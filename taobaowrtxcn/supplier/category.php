
<!-- $Id: category_list.htm 17019 2010-01-29 10:10:34Z liuhui $ -->
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>管理中心 - 商品分类 </title>
<meta name="robots" content="noindex, nofollow">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" /> 
<link href="styles/general.css" rel="stylesheet" type="text/css" />
<link href="styles/main.css" rel="stylesheet" type="text/css" /> 
<script type="text/javascript" src="http://api.map.baidu.com/api?v=1.3"></script>
<script type="text/javascript" src="js/common.js"></script><script type="text/javascript" src="../js/jquery-1.6.2.min.js"></script><script type="text/javascript" src="js/jquery.json.js"></script><script type="text/javascript" src="../js/transport.js"></script>


<script language="JavaScript">
<!--
// 这里把JS用到的所有语言都赋值到这里
var process_request = "正在处理您的请求...";
var todolist_caption = "记事本";
var todolist_autosave = "自动保存";
var todolist_save = "保存";
var todolist_clear = "清除";
var todolist_confirm_save = "是否将更改保存到记事本？";
var todolist_confirm_clear = "是否清空内容？";
var catname_empty = "分类名称不能为空!";
var unit_empyt = "数量单位不能为空!";
var is_leafcat = "您选定的分类是一个末级分类。\r\n新分类的上级分类不能是一个末级分类";
var not_leafcat = "您选定的分类不是一个末级分类。\r\n商品的分类转移只能在末级分类之间才可以操作。";
var filter_attr_not_repeated = "筛选属性不可重复";
var filter_attr_not_selected = "请选择筛选属性";
//-->
</script>
</head>
<body>
<h1>
<span class="action-span"><a href="category.php?act=add">添加分类</a></span>
<span class="action-span1"><a href="index.php?act=main">欢迎您，test（商家管理中心）</a> </span><span id="search_id" class="action-span1"> - 商品分类 </span>
<div style="clear:both"></div>
</h1>
<script type="text/javascript" src="../js/utils.js"></script><script type="text/javascript" src="js/listtable.js"></script><script type="text/javascript" src="js/jquery-1.6.2.min.js"></script><!-- 商品分类搜索 -->
<!-- $Id: category_search.htm 2015-11-02 langlibin $ -->
<div class="form-div">
    <form action="javascript:search_category()" name="searchForm">
        <img src="images/icon_search.gif" width="26" height="22" border="0" alt="SEARCH" />
        搜索商品分类 <input type="text" name="cat_name" size="20" />
        <input type="submit" value=" 搜索 " class="button" />
    </form>
</div>
<script language="JavaScript">
    function search_category()
    {
        listTable.filter['cat_name'] = Utils.trim(document.forms['searchForm'].elements['cat_name'].value);
        listTable.filter['page'] = 1;
        listTable.loadList();
    }
</script>
<form method="post" action="" name="listForm">
<!-- start ad position list -->
<div class="list-div" id="listDiv">

<table width="100%" cellspacing="1" cellpadding="2" id="list-table">
  <tr>
    <th>分类名称&nbsp;&nbsp;<a href="javascript:;" onclick="expandAll(this)">[全部收缩]</a></th>
    <th>商品数量</th>
    <th>数量单位</th>
    <th>导航栏</th>
    <th>是否显示</th>
    <th>价格分级</th>
    <th>排序</th>
    <th>操作</th>
  </tr>
    <tr align="center" class="0" id="0_1">
    <td align="left" class="first-cell" >
            <img src="images/menu_minus.gif" id="icon_0_1" width="9" height="9" border="0" style="margin-left:0em" onclick="rowClicked(this)" />
            <span><a href="goods.php?act=list&cat_id=1">进口鲜果</a></span>
        </td>
    <td width="10%">0</td>
    <td width="10%"><span onclick="listTable.edit(this, 'edit_measure_unit', 1)"><!--  -->&nbsp;&nbsp;&nbsp;&nbsp;<!--  --></span></td>
    <td width="10%"><img src="images/yes.gif" onclick="listTable.toggle(this, 'toggle_show_in_nav', 1)" /></td>
    <td width="10%"><img src="images/yes.gif" onclick="listTable.toggle(this, 'toggle_is_show', 1)" /></td>
    <td><span onclick="listTable.edit(this, 'edit_grade', 1)">0</span></td>
    <td width="10%" align="right"><span onclick="listTable.edit(this, 'edit_sort_order', 1)">50</span></td>
    <td width="24%" align="center">
      <a href="category.php?act=move&cat_id=1">转移商品</a> |
      <a href="category.php?act=edit&amp;cat_id=1">编辑</a> |
      <a href="javascript:;" onclick="listTable.remove(1, '您确认要删除这条记录吗?')" title="移除">移除</a>
    </td>
  </tr>
    <tr align="center" class="1" id="1_4">
    <td align="left" class="first-cell" >
            <img src="images/menu_minus.gif" id="icon_1_4" width="9" height="9" border="0" style="margin-left:1em" onclick="rowClicked(this)" />
            <span><a href="goods.php?act=list&cat_id=4">奇异果</a></span>
        </td>
    <td width="10%">4</td>
    <td width="10%"><span onclick="listTable.edit(this, 'edit_measure_unit', 4)"><!--  -->&nbsp;&nbsp;&nbsp;&nbsp;<!--  --></span></td>
    <td width="10%"><img src="images/no.gif" onclick="listTable.toggle(this, 'toggle_show_in_nav', 4)" /></td>
    <td width="10%"><img src="images/yes.gif" onclick="listTable.toggle(this, 'toggle_is_show', 4)" /></td>
    <td><span onclick="listTable.edit(this, 'edit_grade', 4)">0</span></td>
    <td width="10%" align="right"><span onclick="listTable.edit(this, 'edit_sort_order', 4)">50</span></td>
    <td width="24%" align="center">
      <a href="category.php?act=move&cat_id=4">转移商品</a> |
      <a href="category.php?act=edit&amp;cat_id=4">编辑</a> |
      <a href="javascript:;" onclick="listTable.remove(4, '您确认要删除这条记录吗?')" title="移除">移除</a>
    </td>
  </tr>
    <tr align="center" class="1" id="1_5">
    <td align="left" class="first-cell" >
            <img src="images/menu_minus.gif" id="icon_1_5" width="9" height="9" border="0" style="margin-left:1em" onclick="rowClicked(this)" />
            <span><a href="goods.php?act=list&cat_id=5">车厘子</a></span>
        </td>
    <td width="10%">4</td>
    <td width="10%"><span onclick="listTable.edit(this, 'edit_measure_unit', 5)"><!--  -->&nbsp;&nbsp;&nbsp;&nbsp;<!--  --></span></td>
    <td width="10%"><img src="images/no.gif" onclick="listTable.toggle(this, 'toggle_show_in_nav', 5)" /></td>
    <td width="10%"><img src="images/yes.gif" onclick="listTable.toggle(this, 'toggle_is_show', 5)" /></td>
    <td><span onclick="listTable.edit(this, 'edit_grade', 5)">0</span></td>
    <td width="10%" align="right"><span onclick="listTable.edit(this, 'edit_sort_order', 5)">50</span></td>
    <td width="24%" align="center">
      <a href="category.php?act=move&cat_id=5">转移商品</a> |
      <a href="category.php?act=edit&amp;cat_id=5">编辑</a> |
      <a href="javascript:;" onclick="listTable.remove(5, '您确认要删除这条记录吗?')" title="移除">移除</a>
    </td>
  </tr>
    <tr align="center" class="0" id="0_2">
    <td align="left" class="first-cell" >
            <img src="images/menu_minus.gif" id="icon_0_2" width="9" height="9" border="0" style="margin-left:0em" onclick="rowClicked(this)" />
            <span><a href="goods.php?act=list&cat_id=2">国产时令</a></span>
        </td>
    <td width="10%">0</td>
    <td width="10%"><span onclick="listTable.edit(this, 'edit_measure_unit', 2)"><!--  -->&nbsp;&nbsp;&nbsp;&nbsp;<!--  --></span></td>
    <td width="10%"><img src="images/yes.gif" onclick="listTable.toggle(this, 'toggle_show_in_nav', 2)" /></td>
    <td width="10%"><img src="images/yes.gif" onclick="listTable.toggle(this, 'toggle_is_show', 2)" /></td>
    <td><span onclick="listTable.edit(this, 'edit_grade', 2)">0</span></td>
    <td width="10%" align="right"><span onclick="listTable.edit(this, 'edit_sort_order', 2)">50</span></td>
    <td width="24%" align="center">
      <a href="category.php?act=move&cat_id=2">转移商品</a> |
      <a href="category.php?act=edit&amp;cat_id=2">编辑</a> |
      <a href="javascript:;" onclick="listTable.remove(2, '您确认要删除这条记录吗?')" title="移除">移除</a>
    </td>
  </tr>
    <tr align="center" class="1" id="1_7">
    <td align="left" class="first-cell" >
            <img src="images/menu_minus.gif" id="icon_1_7" width="9" height="9" border="0" style="margin-left:1em" onclick="rowClicked(this)" />
            <span><a href="goods.php?act=list&cat_id=7">火龙果</a></span>
        </td>
    <td width="10%">4</td>
    <td width="10%"><span onclick="listTable.edit(this, 'edit_measure_unit', 7)"><!--  -->&nbsp;&nbsp;&nbsp;&nbsp;<!--  --></span></td>
    <td width="10%"><img src="images/no.gif" onclick="listTable.toggle(this, 'toggle_show_in_nav', 7)" /></td>
    <td width="10%"><img src="images/yes.gif" onclick="listTable.toggle(this, 'toggle_is_show', 7)" /></td>
    <td><span onclick="listTable.edit(this, 'edit_grade', 7)">0</span></td>
    <td width="10%" align="right"><span onclick="listTable.edit(this, 'edit_sort_order', 7)">50</span></td>
    <td width="24%" align="center">
      <a href="category.php?act=move&cat_id=7">转移商品</a> |
      <a href="category.php?act=edit&amp;cat_id=7">编辑</a> |
      <a href="javascript:;" onclick="listTable.remove(7, '您确认要删除这条记录吗?')" title="移除">移除</a>
    </td>
  </tr>
    <tr align="center" class="1" id="1_6">
    <td align="left" class="first-cell" >
            <img src="images/menu_minus.gif" id="icon_1_6" width="9" height="9" border="0" style="margin-left:1em" onclick="rowClicked(this)" />
            <span><a href="goods.php?act=list&cat_id=6">苹果</a></span>
        </td>
    <td width="10%">1</td>
    <td width="10%"><span onclick="listTable.edit(this, 'edit_measure_unit', 6)"><!--  -->&nbsp;&nbsp;&nbsp;&nbsp;<!--  --></span></td>
    <td width="10%"><img src="images/no.gif" onclick="listTable.toggle(this, 'toggle_show_in_nav', 6)" /></td>
    <td width="10%"><img src="images/yes.gif" onclick="listTable.toggle(this, 'toggle_is_show', 6)" /></td>
    <td><span onclick="listTable.edit(this, 'edit_grade', 6)">0</span></td>
    <td width="10%" align="right"><span onclick="listTable.edit(this, 'edit_sort_order', 6)">50</span></td>
    <td width="24%" align="center">
      <a href="category.php?act=move&cat_id=6">转移商品</a> |
      <a href="category.php?act=edit&amp;cat_id=6">编辑</a> |
      <a href="javascript:;" onclick="listTable.remove(6, '您确认要删除这条记录吗?')" title="移除">移除</a>
    </td>
  </tr>
    <tr align="center" class="0" id="0_3">
    <td align="left" class="first-cell" >
            <img src="images/menu_minus.gif" id="icon_0_3" width="9" height="9" border="0" style="margin-left:0em" onclick="rowClicked(this)" />
            <span><a href="goods.php?act=list&cat_id=3">海鲜肉类</a></span>
        </td>
    <td width="10%">0</td>
    <td width="10%"><span onclick="listTable.edit(this, 'edit_measure_unit', 3)"><!--  -->&nbsp;&nbsp;&nbsp;&nbsp;<!--  --></span></td>
    <td width="10%"><img src="images/yes.gif" onclick="listTable.toggle(this, 'toggle_show_in_nav', 3)" /></td>
    <td width="10%"><img src="images/yes.gif" onclick="listTable.toggle(this, 'toggle_is_show', 3)" /></td>
    <td><span onclick="listTable.edit(this, 'edit_grade', 3)">0</span></td>
    <td width="10%" align="right"><span onclick="listTable.edit(this, 'edit_sort_order', 3)">50</span></td>
    <td width="24%" align="center">
      <a href="category.php?act=move&cat_id=3">转移商品</a> |
      <a href="category.php?act=edit&amp;cat_id=3">编辑</a> |
      <a href="javascript:;" onclick="listTable.remove(3, '您确认要删除这条记录吗?')" title="移除">移除</a>
    </td>
  </tr>
    <tr align="center" class="1" id="1_8">
    <td align="left" class="first-cell" >
            <img src="images/menu_minus.gif" id="icon_1_8" width="9" height="9" border="0" style="margin-left:1em" onclick="rowClicked(this)" />
            <span><a href="goods.php?act=list&cat_id=8">尝鲜包装</a></span>
        </td>
    <td width="10%">3</td>
    <td width="10%"><span onclick="listTable.edit(this, 'edit_measure_unit', 8)"><!--  -->&nbsp;&nbsp;&nbsp;&nbsp;<!--  --></span></td>
    <td width="10%"><img src="images/no.gif" onclick="listTable.toggle(this, 'toggle_show_in_nav', 8)" /></td>
    <td width="10%"><img src="images/yes.gif" onclick="listTable.toggle(this, 'toggle_is_show', 8)" /></td>
    <td><span onclick="listTable.edit(this, 'edit_grade', 8)">0</span></td>
    <td width="10%" align="right"><span onclick="listTable.edit(this, 'edit_sort_order', 8)">50</span></td>
    <td width="24%" align="center">
      <a href="category.php?act=move&cat_id=8">转移商品</a> |
      <a href="category.php?act=edit&amp;cat_id=8">编辑</a> |
      <a href="javascript:;" onclick="listTable.remove(8, '您确认要删除这条记录吗?')" title="移除">移除</a>
    </td>
  </tr>
    <tr align="center" class="1" id="1_9">
    <td align="left" class="first-cell" >
            <img src="images/menu_minus.gif" id="icon_1_9" width="9" height="9" border="0" style="margin-left:1em" onclick="rowClicked(this)" />
            <span><a href="goods.php?act=list&cat_id=9">家庭套餐</a></span>
        </td>
    <td width="10%">0</td>
    <td width="10%"><span onclick="listTable.edit(this, 'edit_measure_unit', 9)"><!--  -->&nbsp;&nbsp;&nbsp;&nbsp;<!--  --></span></td>
    <td width="10%"><img src="images/no.gif" onclick="listTable.toggle(this, 'toggle_show_in_nav', 9)" /></td>
    <td width="10%"><img src="images/yes.gif" onclick="listTable.toggle(this, 'toggle_is_show', 9)" /></td>
    <td><span onclick="listTable.edit(this, 'edit_grade', 9)">0</span></td>
    <td width="10%" align="right"><span onclick="listTable.edit(this, 'edit_sort_order', 9)">50</span></td>
    <td width="24%" align="center">
      <a href="category.php?act=move&cat_id=9">转移商品</a> |
      <a href="category.php?act=edit&amp;cat_id=9">编辑</a> |
      <a href="javascript:;" onclick="listTable.remove(9, '您确认要删除这条记录吗?')" title="移除">移除</a>
    </td>
  </tr>
  </table>
</div>
</form>


<script language="JavaScript">
<!--

onload = function()
{
  // 开始检查订单
  startCheckOrder();
}

var imgPlus = new Image();
imgPlus.src = "images/menu_plus.gif";

/**
 * 折叠分类列表
 */
function rowClicked(obj)
{
  // 当前图像
  img = obj;
  // 取得上二级tr>td>img对象
  obj = obj.parentNode.parentNode;
  // 整个分类列表表格
  var tbl = document.getElementById("list-table");
  // 当前分类级别
  var lvl = parseInt(obj.className);
  // 是否找到元素
  var fnd = false;
  var sub_display = img.src.indexOf('menu_minus.gif') > 0 ? 'none' : (Browser.isIE) ? 'block' : 'table-row' ;
  // 遍历所有的分类
  for (i = 0; i < tbl.rows.length; i++)
  {
      var row = tbl.rows[i];
      if (row == obj)
      {
          // 找到当前行
          fnd = true;
          //document.getElementById('result').innerHTML += 'Find row at ' + i +"<br/>";
      }
      else
      {
          if (fnd == true)
          {
              var cur = parseInt(row.className);
              var icon = 'icon_' + row.id;
              if (cur > lvl)
              {
                  row.style.display = sub_display;
                  if (sub_display != 'none')
                  {
                      var iconimg = document.getElementById(icon);
                      iconimg.src = iconimg.src.replace('plus.gif', 'minus.gif');
                  }
              }
              else
              {
                  fnd = false;
                  break;
              }
          }
      }
  }

  for (i = 0; i < obj.cells[0].childNodes.length; i++)
  {
      var imgObj = obj.cells[0].childNodes[i];
      if (imgObj.tagName == "IMG" && imgObj.src != 'images/menu_arrow.gif')
      {
          imgObj.src = (imgObj.src == imgPlus.src) ? 'images/menu_minus.gif' : imgPlus.src;
      }
  }
}

/**
 * 展开或折叠所有分类
 * 直接调用了rowClicked()函数，由于其函数内每次都会扫描整张表所以效率会比较低，数据量大会出现卡顿现象
 */
var expand = true;
function expandAll(obj)
{
	
	var selecter;
	
	if(expand)
	{
		// 收缩
		selecter = "img[src*='menu_minus.gif'],img[src*='menu_plus.gif']";
		$(obj).html("[全部展开]");
		$(selecter).parents("tr[class!='0']").hide();
		$(selecter).attr("src", "images/menu_plus.gif");
	}
	else
	{
		// 展开
		selecter = "img[src*='menu_plus.gif'],img[src*='menu_minus.gif']";
		$(obj).html("[全部收缩]");
		$(selecter).parents("tr").show();
		$(selecter).attr("src", "images/menu_minus.gif");
	}
	
	// 标识展开/收缩状态
	expand = !expand;
}
//-->
</script>


<div id="footer">
共执行 5 个查询，用时 0.009001 秒，Gzip 已禁用，内存占用 2.935 MB<br />
版权所有 &copy; 2008-2030 广州网软志成信息科技有限公司，并保留所有权利。</div>
<!-- 新订单提示信息 -->
<div id="popMsg">
  <table cellspacing="0" cellpadding="0" width="100%" bgcolor="#cfdef4" border="0">
  <tr>
    <td style="color: #0f2c8c" width="30" height="24"></td>
    <td style="font-weight: normal; color: #1f336b; padding-top: 4px;padding-left: 4px" valign="center" width="100%"> 新订单通知</td>
    <td style="padding-top: 2px;padding-right:2px" valign="center" align="right" width="19"><span title="关闭" style="cursor: hand;cursor:pointer;color:red;font-size:12px;font-weight:bold;margin-right:4px;" onclick="Message.close()" >×</span><!-- <img title=关闭 style="cursor: hand" onclick=closediv() hspace=3 src="msgclose.jpg"> --></td>
  </tr>
  <tr>
    <td style="padding-right: 1px; padding-bottom: 1px" colspan="3" height="70">
    <div id="popMsgContent">
      <p>您有 <strong style="color:#ff0000" id="spanNewOrder">1</strong> 个新订单以及       <strong style="color:#ff0000" id="spanNewPaid">0</strong> 个新付款的订单</p>
      <p align="center" style="word-break:break-all"><a href="order.php?act=list"><span style="color:#ff0000">点击查看新订单</span></a></p>
    </div>
    </td>
  </tr>
  </table>
</div>

<!--
<embed src="images/online.wav" width="0" height="0" autostart="false" name="msgBeep" id="msgBeep" enablejavascript="true"/>
-->
<object classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000" codebase="http://active.macromedia.com/flash2/cabs/swflash.cab#version=4,0,0,0" id="msgBeep" width="1" height="1">
  <param name="movie" value="images/online.swf">
  <param name="quality" value="high">
  <embed src="images/online.swf" name="msgBeep" id="msgBeep" quality="high" width="0" height="0" type="application/x-shockwave-flash" pluginspage="http://www.macromedia.com/shockwave/download/index.cgi?p1_prod_version=shockwaveflash">
  </embed>
</object>

<script language="JavaScript">
document.onmousemove=function(e)
{
  var obj = Utils.srcElement(e);
  if (typeof(obj.onclick) == 'function' && obj.onclick.toString().indexOf('listTable.edit') != -1)
  {
    obj.title = '点击修改内容';
    obj.style.cssText = 'background: #278296;';
    obj.onmouseout = function(e)
    {
      this.style.cssText = '';
    }
  }
  else if (typeof(obj.href) != 'undefined' && obj.href.indexOf('listTable.sort') != -1)
  {
    obj.title = '点击对列表排序';
  }
}
<!--


var MyTodolist;
function showTodoList(adminid)
{
  if(!MyTodolist)
  {
    var global = $import("../js/global.js","js");
    global.onload = global.onreadystatechange= function()
    {
      if(this.readyState && this.readyState=="loading")return;
      var md5 = $import("js/md5.js","js");
      md5.onload = md5.onreadystatechange= function()
      {
        if(this.readyState && this.readyState=="loading")return;
        var todolist = $import("js/todolist.js","js");
        todolist.onload = todolist.onreadystatechange = function()
        {
          if(this.readyState && this.readyState=="loading")return;
          MyTodolist = new Todolist();
          MyTodolist.show();
        }
      }
    }
  }
  else
  {
    if(MyTodolist.visibility)
    {
      MyTodolist.hide();
    }
    else
    {
      MyTodolist.show();
    }
  }
}

if (Browser.isIE)
{
  onscroll = function()
  {
    //document.getElementById('calculator').style.top = document.body.scrollTop;
    document.getElementById('popMsg').style.top = (document.body.scrollTop + document.body.clientHeight - document.getElementById('popMsg').offsetHeight) + "px";
  }
}

if (document.getElementById("listDiv"))
{
  document.getElementById("listDiv").onmouseover = function(e)
  {
    obj = Utils.srcElement(e);

    if (obj)
    {
      if (obj.parentNode.tagName.toLowerCase() == "tr") row = obj.parentNode;
      else if (obj.parentNode.parentNode.tagName.toLowerCase() == "tr") row = obj.parentNode.parentNode;
      else return;

      for (i = 0; i < row.cells.length; i++)
      {
        if (row.cells[i].tagName != "TH") row.cells[i].style.backgroundColor = '#F4FAFB';
      }
    }

  }

  document.getElementById("listDiv").onmouseout = function(e)
  {
    obj = Utils.srcElement(e);

    if (obj)
    {
      if (obj.parentNode.tagName.toLowerCase() == "tr") row = obj.parentNode;
      else if (obj.parentNode.parentNode.tagName.toLowerCase() == "tr") row = obj.parentNode.parentNode;
      else return;

      for (i = 0; i < row.cells.length; i++)
      {
          if (row.cells[i].tagName != "TH") row.cells[i].style.backgroundColor = '';
      }
    }
  }

  document.getElementById("listDiv").onclick = function(e)
  {
    var obj = Utils.srcElement(e);

    if (obj.tagName == "INPUT" && obj.type == "checkbox")
    {
      if (!document.forms['listForm'])
      {
        return;
      }
      var nodes = document.forms['listForm'].elements;
      var checked = false;

      for (i = 0; i < nodes.length; i++)
      {
        if (nodes[i].checked)
        {
           checked = true;
           break;
         }
      }

      if(document.getElementById("btnSubmit"))
      {
        document.getElementById("btnSubmit").disabled = !checked;
      }
      for (i = 1; i <= 10; i++)
      {
        if (document.getElementById("btnSubmit" + i))
        {
          document.getElementById("btnSubmit" + i).disabled = !checked;
        }
      }
    }
  }

}

//-->
</script>
</body>
</html>

<!-- $Id: category_info.htm 16752 2009-10-20 09:59:38Z wangleisvn $ -->
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>管理中心 - 编辑商品分类 </title>
<meta name="robots" content="noindex, nofollow">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" /> 
<link href="styles/general.css" rel="stylesheet" type="text/css" />
<link href="styles/main.css" rel="stylesheet" type="text/css" /> 
<script type="text/javascript" src="http://api.map.baidu.com/api?v=1.3"></script>
<script type="text/javascript" src="js/common.js"></script><script type="text/javascript" src="../js/jquery-1.6.2.min.js"></script><script type="text/javascript" src="js/jquery.json.js"></script><script type="text/javascript" src="../js/transport.js"></script>


<script language="JavaScript">
<!--
// 这里把JS用到的所有语言都赋值到这里
var process_request = "正在处理您的请求...";
var todolist_caption = "记事本";
var todolist_autosave = "自动保存";
var todolist_save = "保存";
var todolist_clear = "清除";
var todolist_confirm_save = "是否将更改保存到记事本？";
var todolist_confirm_clear = "是否清空内容？";
var catname_empty = "分类名称不能为空!";
var unit_empyt = "数量单位不能为空!";
var is_leafcat = "您选定的分类是一个末级分类。\r\n新分类的上级分类不能是一个末级分类";
var not_leafcat = "您选定的分类不是一个末级分类。\r\n商品的分类转移只能在末级分类之间才可以操作。";
var filter_attr_not_repeated = "筛选属性不可重复";
var filter_attr_not_selected = "请选择筛选属性";
//-->
</script>
</head>
<body>
<h1>
<span class="action-span"><a href="category.php?act=list">商品分类</a></span>
<span class="action-span1"><a href="index.php?act=main">欢迎您，test（商家管理中心）</a> </span><span id="search_id" class="action-span1"> - 编辑商品分类 </span>
<div style="clear:both"></div>
</h1>
<!-- start add new category form -->
<div class="main-div">
  <form action="category.php" method="post" name="theForm" enctype="multipart/form-data" onsubmit="return validate()">
  <table width="100%" id="general-table">
      <tr>
        <td class="label">分类名称:</td>
          <!--代码修改 By  www.wrzc.net Start -->
          <!--
        <td>
          <input type='text' name='cat_name' maxlength="20" value='奇异果' size='27' /> <font color="red">*</font>
        </td>
        -->
          <td><textarea name='cat_name' rows="3" cols="48">奇异果</textarea>
              <font color="red">*</font> <span class="notice-span" style="display:block"  id="noticeCat_name">如果您需要添加多个分类，请在分类之间使用半角逗号分隔。</span></td>
          <!--代码修改 By  www.wrzc.net End -->
      </tr>
      <tr>
        <td class="label">上级分类:</td>
        <td>
          <select name="parent_id">
            <option value="0">顶级分类</option>
            <option value="1" selected='ture'>进口鲜果</option><option value="4" >&nbsp;&nbsp;&nbsp;&nbsp;奇异果</option><option value="5" >&nbsp;&nbsp;&nbsp;&nbsp;车厘子</option><option value="2" >国产时令</option><option value="7" >&nbsp;&nbsp;&nbsp;&nbsp;火龙果</option><option value="6" >&nbsp;&nbsp;&nbsp;&nbsp;苹果</option><option value="3" >海鲜肉类</option><option value="8" >&nbsp;&nbsp;&nbsp;&nbsp;尝鲜包装</option><option value="9" >&nbsp;&nbsp;&nbsp;&nbsp;家庭套餐</option>          </select>
        </td>
      </tr>

      <tr id="measure_unit">
        <td class="label">数量单位:</td>
        <td>
          <input type="text" name='measure_unit' value='' size="12" />
        </td>
      </tr>
      <tr>
        <td class="label">排序:</td>
        <td>
          <input type="text" name='sort_order' value='50' size="15" />
        </td>
      </tr>

      <tr>
        <td class="label">是否显示:</td>
        <td>
          <input type="radio" name="is_show" value="1"  checked="true"/> 是          <input type="radio" name="is_show" value="0"  /> 否        </td>
      </tr>
      <tr>
        <td class="label">是否显示在导航栏:</td>
        <td>
          <input type="radio" name="show_in_nav" value="1" /> 是          <input type="radio" name="show_in_nav" value="0"  checked="true" /> 否        </td>
      </tr>
      <tr>
        <td class="label">设置为首页推荐:</td>
        <td>
          <input type="checkbox" name="cat_recommend[]" value="1" /> 精品          <input type="checkbox" name="cat_recommend[]" value="2"  /> 最新          <input type="checkbox" name="cat_recommend[]" value="3"  checked="true" /> 热门        </td>
      </tr>
	  <tr>
        <td class="label">是否设置为首页推荐:</td>
        <td>
          <input type="radio" name="is_show_cat_pic" value="1"  checked="true"/> 是          <input type="radio" name="is_show_cat_pic" value="0"  /> 否        </td>
      </tr>
	  <tr>
        <td class="label">上传店铺此类广告图:</td>
        <td>
		<input type="file" name="cat_pic" size="35" />
                              <img src="images/no.gif" />
                      </td>
      </tr>
	  <tr>
        <td class="label">店铺广告图链接:</td>
        <td>
		<input type="text" name='cat_pic_url' value='' size="35" />
        </td>
      </tr>
	  <tr>
        <td class="label">首页此类商品显示数量:</td>
        <td>
		<input type="text" name='cat_goods_limit' value='4' size="15" />
        </td>
      </tr>
      <tr>
        <td class="label"><a href="javascript:showNotice('noticeFilterAttr');" title="点击此处查看提示信息"><img src="images/notice.gif" width="16" height="16" border="0" alt="您可以为每一个商品分类指定一个样式表文件。例如文件存放在 themes 目录下则输入：themes/style.css"></a>筛选属性:</td>
        <td>
          <script type="text/javascript">
          var arr = new Array();
          var sel_filter_attr = "请选择筛选属性";
                      arr[1] = new Array();
                                          arr[1][0] = ["频率", 13];
                                                        arr[1][1] = ["屏幕尺寸", 9];
                                                        arr[1][2] = ["分类", 5];
                                                        arr[1][3] = ["颜色", 1];
                                                        arr[1][4] = ["核数", 12];
                                                        arr[1][5] = ["系统", 8];
                                                        arr[1][6] = ["用途", 4];
                                                        arr[1][7] = ["前置摄像头", 11];
                                                        arr[1][8] = ["有效像素", 7];
                                                        arr[1][9] = ["商品产地", 3];
                                                        arr[1][10] = ["分辨率", 14];
                                                        arr[1][11] = ["后置摄像头", 10];
                                                        arr[1][12] = ["液晶屏尺寸", 6];
                                                        arr[1][13] = ["版本", 2];
                                                arr[2] = new Array();
                                          arr[2][0] = ["鞋头", 30];
                                                        arr[2][1] = ["内里材质", 26];
                                                        arr[2][2] = ["闭合方式", 20];
                                                        arr[2][3] = ["版型", 37];
                                                        arr[2][4] = ["颜色", 15];
                                                        arr[2][5] = ["材质", 33];
                                                        arr[2][6] = ["图案", 29];
                                                        arr[2][7] = ["腰型", 25];
                                                        arr[2][8] = ["风格", 18];
                                                        arr[2][9] = ["袖型", 36];
                                                        arr[2][10] = ["袖长", 23];
                                                        arr[2][11] = ["功能", 32];
                                                        arr[2][12] = ["鞋跟形状", 28];
                                                        arr[2][13] = ["制作工艺", 24];
                                                        arr[2][14] = ["帮面材质", 17];
                                                        arr[2][15] = ["领型", 35];
                                                        arr[2][16] = ["裙型", 31];
                                                        arr[2][17] = ["跟高", 27];
                                                        arr[2][18] = ["鞋底材质", 22];
                                                        arr[2][19] = ["尺码", 16];
                                                        arr[2][20] = ["裙长", 34];
                                                arr[3] = new Array();
                                          arr[3][0] = ["产地", 39];
                                                        arr[3][1] = ["适用人群", 38];
                                    
          function changeCat(obj)
          {
            var key = obj.value;
            var sel = window.ActiveXObject ? obj.parentNode.childNodes[4] : obj.parentNode.childNodes[5];
            sel.length = 0;
            sel.options[0] = new Option(sel_filter_attr, 0);
            if (arr[key] == undefined)
            {
              return;
            }
            for (var i= 0; i < arr[key].length ;i++ )
            {
              sel.options[i+1] = new Option(arr[key][i][0], arr[key][i][1]);
            }

          }

          </script>


          <table width="100%" id="tbody-attr" align="center">
                        <tr>
              <td>
                   <a href="javascript:;" onclick="addFilterAttr(this)">[+]</a>
                   <select onChange="changeCat(this)"><option value="0">请选择商品类型</option><option value='1'>手机数码</option><option value='2'>服饰鞋帽</option><option value='3'>化妆品</option><option value='4'>人人</option></select>&nbsp;&nbsp;
                   <select name="filter_attr[]"><option value="0">请选择筛选属性</option></select><br />
              </td>
            </tr>
                                  </table>

          <span class="notice-span" style="display:block"  id="noticeFilterAttr">筛选属性可在当前分类页面进行筛选商品</span>
        </td>
      </tr>
      <tr>
        <td class="label"><a href="javascript:showNotice('noticeGrade');" title="点击此处查看提示信息"><img src="images/notice.gif" width="16" height="16" border="0" alt="您可以为每一个商品分类指定一个样式表文件。例如文件存放在 themes 目录下则输入：themes/style.css"></a>价格区间个数:</td>
        <td>
          <input type="text" name="grade" value="0" size="40" /> <br />
          <span class="notice-span" style="display:block"  id="noticeGrade">该选项表示该分类下商品最低价与最高价之间的划分的等级个数，填0表示不做分级，最多不能超过10个。</span>
        </td>
      </tr>
      <tr>
        <td class="label"><a href="javascript:showNotice('noticeGoodsSN');" title="点击此处查看提示信息"><img src="images/notice.gif" width="16" height="16" border="0" alt="您可以为每一个商品分类指定一个样式表文件。例如文件存放在 themes 目录下则输入：themes/style.css"></a>分类的样式表文件:</td>
        <td>
          <input type="text" name="style" value="" size="40" /> <br />
          <span class="notice-span" style="display:block"  id="noticeGoodsSN">您可以为每一个商品分类指定一个样式表文件。例如文件存放在 themes 目录下则输入：themes/style.css</span>
        </td>
      </tr>
      <tr>
        <td class="label">关键字:</td>
        <td><input type="text" name="keywords" value='' size="50">
        </td>
      </tr>

      <tr>
        <td class="label">分类描述:</td>
        <td>
          <textarea name='cat_desc' rows="6" cols="48"></textarea>
        </td>
      </tr>
      </table>
      <div class="button-div">
        <input type="submit" class="button" value=" 确定 " />
        <input type="reset" class="button" value=" 重置 " />
      </div>
    <input type="hidden" name="act" value="update" />
    <input type="hidden" name="old_cat_name" value="奇异果" />
    <input type="hidden" name="cat_id" value="4" />
  </form>
</div>
<script type="text/javascript" src="../js/utils.js"></script><script type="text/javascript" src="js/validator.js"></script>
<script language="JavaScript">
<!--
document.forms['theForm'].elements['cat_name'].focus();
/**
 * 检查表单输入的数据
 */
function validate()
{
  validator = new Validator("theForm");
  validator.required("cat_name",      catname_empty);
  if (parseInt(document.forms['theForm'].elements['grade'].value) >10 || parseInt(document.forms['theForm'].elements['grade'].value) < 0)
  {
    validator.addErrorMsg('价格分级数量只能是0-10之内的整数');
  }
  return validator.passed();
}
onload = function()
{
  // 开始检查订单
  startCheckOrder();
}

/**
 * 新增一个筛选属性
 */
function addFilterAttr(obj)
{
  var src = obj.parentNode.parentNode;
  var tbl = document.getElementById('tbody-attr');

  var validator  = new Validator('theForm');
  var filterAttr = document.getElementsByName("filter_attr[]");

  if (filterAttr[filterAttr.length-1].selectedIndex == 0)
  {
    validator.addErrorMsg(filter_attr_not_selected);
  }

  for (i = 0; i < filterAttr.length; i++)
  {
    for (j = i + 1; j <filterAttr.length; j++)
    {
      if (filterAttr.item(i).value == filterAttr.item(j).value)
      {
        validator.addErrorMsg(filter_attr_not_repeated);
      }
    }
  }

  if (!validator.passed())
  {
    return false;
  }

  var row  = tbl.insertRow(tbl.rows.length);
  var cell = row.insertCell(-1);
  cell.innerHTML = src.cells[0].innerHTML.replace(/(.*)(addFilterAttr)(.*)(\[)(\+)/i, "$1removeFilterAttr$3$4-");
  filterAttr[filterAttr.length-1].selectedIndex = 0;
}

/**
 * 删除一个筛选属性
 */
function removeFilterAttr(obj)
{
  var row = rowindex(obj.parentNode.parentNode);
  var tbl = document.getElementById('tbody-attr');

  tbl.deleteRow(row);
}
//-->
</script>

<div id="footer">
共执行 9 个查询，用时 0.021001 秒，Gzip 已禁用，内存占用 2.941 MB<br />
版权所有 &copy; 2008-2030 广州网软志成信息科技有限公司，并保留所有权利。</div>
<!-- 新订单提示信息 -->
<div id="popMsg">
  <table cellspacing="0" cellpadding="0" width="100%" bgcolor="#cfdef4" border="0">
  <tr>
    <td style="color: #0f2c8c" width="30" height="24"></td>
    <td style="font-weight: normal; color: #1f336b; padding-top: 4px;padding-left: 4px" valign="center" width="100%"> 新订单通知</td>
    <td style="padding-top: 2px;padding-right:2px" valign="center" align="right" width="19"><span title="关闭" style="cursor: hand;cursor:pointer;color:red;font-size:12px;font-weight:bold;margin-right:4px;" onclick="Message.close()" >×</span><!-- <img title=关闭 style="cursor: hand" onclick=closediv() hspace=3 src="msgclose.jpg"> --></td>
  </tr>
  <tr>
    <td style="padding-right: 1px; padding-bottom: 1px" colspan="3" height="70">
    <div id="popMsgContent">
      <p>您有 <strong style="color:#ff0000" id="spanNewOrder">1</strong> 个新订单以及       <strong style="color:#ff0000" id="spanNewPaid">0</strong> 个新付款的订单</p>
      <p align="center" style="word-break:break-all"><a href="order.php?act=list"><span style="color:#ff0000">点击查看新订单</span></a></p>
    </div>
    </td>
  </tr>
  </table>
</div>

<!--
<embed src="images/online.wav" width="0" height="0" autostart="false" name="msgBeep" id="msgBeep" enablejavascript="true"/>
-->
<object classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000" codebase="http://active.macromedia.com/flash2/cabs/swflash.cab#version=4,0,0,0" id="msgBeep" width="1" height="1">
  <param name="movie" value="images/online.swf">
  <param name="quality" value="high">
  <embed src="images/online.swf" name="msgBeep" id="msgBeep" quality="high" width="0" height="0" type="application/x-shockwave-flash" pluginspage="http://www.macromedia.com/shockwave/download/index.cgi?p1_prod_version=shockwaveflash">
  </embed>
</object>

<script language="JavaScript">
document.onmousemove=function(e)
{
  var obj = Utils.srcElement(e);
  if (typeof(obj.onclick) == 'function' && obj.onclick.toString().indexOf('listTable.edit') != -1)
  {
    obj.title = '点击修改内容';
    obj.style.cssText = 'background: #278296;';
    obj.onmouseout = function(e)
    {
      this.style.cssText = '';
    }
  }
  else if (typeof(obj.href) != 'undefined' && obj.href.indexOf('listTable.sort') != -1)
  {
    obj.title = '点击对列表排序';
  }
}
<!--


var MyTodolist;
function showTodoList(adminid)
{
  if(!MyTodolist)
  {
    var global = $import("../js/global.js","js");
    global.onload = global.onreadystatechange= function()
    {
      if(this.readyState && this.readyState=="loading")return;
      var md5 = $import("js/md5.js","js");
      md5.onload = md5.onreadystatechange= function()
      {
        if(this.readyState && this.readyState=="loading")return;
        var todolist = $import("js/todolist.js","js");
        todolist.onload = todolist.onreadystatechange = function()
        {
          if(this.readyState && this.readyState=="loading")return;
          MyTodolist = new Todolist();
          MyTodolist.show();
        }
      }
    }
  }
  else
  {
    if(MyTodolist.visibility)
    {
      MyTodolist.hide();
    }
    else
    {
      MyTodolist.show();
    }
  }
}

if (Browser.isIE)
{
  onscroll = function()
  {
    //document.getElementById('calculator').style.top = document.body.scrollTop;
    document.getElementById('popMsg').style.top = (document.body.scrollTop + document.body.clientHeight - document.getElementById('popMsg').offsetHeight) + "px";
  }
}

if (document.getElementById("listDiv"))
{
  document.getElementById("listDiv").onmouseover = function(e)
  {
    obj = Utils.srcElement(e);

    if (obj)
    {
      if (obj.parentNode.tagName.toLowerCase() == "tr") row = obj.parentNode;
      else if (obj.parentNode.parentNode.tagName.toLowerCase() == "tr") row = obj.parentNode.parentNode;
      else return;

      for (i = 0; i < row.cells.length; i++)
      {
        if (row.cells[i].tagName != "TH") row.cells[i].style.backgroundColor = '#F4FAFB';
      }
    }

  }

  document.getElementById("listDiv").onmouseout = function(e)
  {
    obj = Utils.srcElement(e);

    if (obj)
    {
      if (obj.parentNode.tagName.toLowerCase() == "tr") row = obj.parentNode;
      else if (obj.parentNode.parentNode.tagName.toLowerCase() == "tr") row = obj.parentNode.parentNode;
      else return;

      for (i = 0; i < row.cells.length; i++)
      {
          if (row.cells[i].tagName != "TH") row.cells[i].style.backgroundColor = '';
      }
    }
  }

  document.getElementById("listDiv").onclick = function(e)
  {
    var obj = Utils.srcElement(e);

    if (obj.tagName == "INPUT" && obj.type == "checkbox")
    {
      if (!document.forms['listForm'])
      {
        return;
      }
      var nodes = document.forms['listForm'].elements;
      var checked = false;

      for (i = 0; i < nodes.length; i++)
      {
        if (nodes[i].checked)
        {
           checked = true;
           break;
         }
      }

      if(document.getElementById("btnSubmit"))
      {
        document.getElementById("btnSubmit").disabled = !checked;
      }
      for (i = 1; i <= 10; i++)
      {
        if (document.getElementById("btnSubmit" + i))
        {
          document.getElementById("btnSubmit" + i).disabled = !checked;
        }
      }
    }
  }

}

//-->
</script><?php
include '../admin/buy.php' ;
?>
</body>
</html>