
<!-- $Id: order_info.htm 17060 2010-03-25 03:44:42Z liuhui $ -->
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>管理中心 - 平台交易统计 </title>
<meta name="robots" content="noindex, nofollow">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" /> 
<link href="styles/general.css" rel="stylesheet" type="text/css" />
<link href="styles/main.css" rel="stylesheet" type="text/css" /> 
<script type="text/javascript" src="http://api.map.baidu.com/api?v=1.3"></script>
<script type="text/javascript" src="js/common.js"></script><script type="text/javascript" src="../js/jquery-1.6.2.min.js"></script><script type="text/javascript" src="js/jquery.json.js"></script><script type="text/javascript" src="../js/transport.js"></script>


<script language="JavaScript">
<!--
// 这里把JS用到的所有语言都赋值到这里
var process_request = "正在处理您的请求...";
var todolist_caption = "记事本";
var todolist_autosave = "自动保存";
var todolist_save = "保存";
var todolist_clear = "清除";
var todolist_confirm_save = "是否将更改保存到记事本？";
var todolist_confirm_clear = "是否清空内容？";
//-->
</script>
</head>
<body>
<h1>
<span class="action-span1"><a href="index.php?act=main">欢迎您，test（商家管理中心）</a> </span><span id="search_id" class="action-span1"> - 平台交易统计 </span>
<div style="clear:both"></div>
</h1>
<script type="text/javascript" src="js/topbar.js"></script><script type="text/javascript" src="../js/utils.js"></script><script type="text/javascript" src="js/listtable.js"></script><script type="text/javascript" src="js/selectzone.js"></script><script type="text/javascript" src="../js/common.js"></script><script type="text/javascript" src="js/jquery-1.6.2.min.js"></script><script type="text/javascript" src="js/chosen.jquery.min.js"></script><script type="text/javascript" src="../js/calendar.php?lang="></script>
<link href="../js/calendar/calendar.css" rel="stylesheet" type="text/css" />
<link href='styles/store.css' rel='stylesheet' type='text/css' />
<link href='styles/chosen/chosen.css' rel='stylesheet' type='text/css' />
<!--搜索区域-->
<div class="form-div">
  <form action="supplier_rebate.php" name="searchForm" method="post">
    <table width="100%" cellpadding="0" cellspacing="0" border="0">
      <tr>
        <td width="9%" align="right">账户变动时间：</td>
        <td colspan="3">
		<input name="start_time" type="text" id="start_time" value="" readonly="readonly" />
        <input name="selbtn1" type="button" id="selbtn1" onclick="return showCalendar('start_time', '%Y-%m-%d %H:%M', '24', false, 'selbtn1');" value="选择" class="button" />&nbsp;-&nbsp;<input name="end_time" type="text" id="end_time" value="" readonly="readonly" /><input name="selbtn2" type="button" id="selbtn2" onclick="return showCalendar('end_time', '%Y-%m-%d %H:%M', '24', false, 'selbtn2');" value="选择" class="button" />
            <a href="javascript:;" class="rebate_time" onclick="set_time(this,'2016-09-03 00:00','2016-09-04 00:00')">今日</a>
            <a href="javascript:;" class="rebate_time" onclick="set_time(this,'2016-09-02 00:00','2016-09-04 00:00')">昨日</a>
            <a href="javascript:;" class="rebate_time" onclick="set_time(this,'2016-08-27 00:00','2016-09-04 00:00')">最近7天</a>
            <a href="javascript:;" class="rebate_time" onclick="set_time(this,'2016-08-03 00:00','2016-09-04 00:00')">最近30天</a>
        </td>
      </tr>
      <tr>
        <td width="9%" align="right">支付方式：</td>
        <td width="15%">
        	<select class="chzn-select"  name='payid' id="payid" style='width:123px;'>
    			<option value='0' selected='selected'>全部</option>
				    		</select></td>
        <td width="67%">
            商家订单号：
			<select class="chzn-select" data-placeholder="商家订单号" style="width:350px;" name="orderid">
				<option value="" selected='selected'>全部</option>
								</select>

            <a class="button_round" onclick="searchSupp()">搜索</a>
            <a class="button_round" onclick="exportGoods()">批量导出</a>
        </td>
      </tr>
    </table>
  </form>
</div>
<div class="list-div" id="listDiv">
  <table cellpadding="3" cellspacing="1">
    <tr>
      <th><a href="javascript:listTable.sort('sr.add_time'); ">账户变动时间</a></th>
      <th>订单号</th>
      <th>订单金额（元）</th>
      <th>平台扣除佣金（元）</th>
	  <th>商家实际收入金额（元）</th>
      <th>支付方式</th>
      <th>备注</th>
    </tr>
		<!--没有佣金记录的显示-->
    <!--<tr><td class="no-records" colspan="7">没有找到任何记录</td></tr>-->

  </table>
<table id="page-table" cellspacing="0">
  <tr>
    <td>&nbsp;</td>
    <td align="right" nowrap="true">
          <!-- $Id: page.htm 14216 2008-03-10 02:27:21Z testyang $ -->
            <div id="turn-page">
        总计  <span id="totalRecords">0</span>
        个记录分为 <span id="totalPages">1</span>
        页当前第 <span id="pageCurrent">1</span>
        页，每页 <input type='text' size='3' id='pageSize' value="15" onkeypress="return listTable.changePageSize(event)" />
        <span id="page-link">
          <a href="javascript:listTable.gotoPageFirst()">第一页</a>
          <a href="javascript:listTable.gotoPagePrev()">上一页</a>
          <a href="javascript:listTable.gotoPageNext()">下一页</a>
          <a href="javascript:listTable.gotoPageLast()">最末页</a>
 <!--   <select id="gotoPage" onchange="listTable.gotoPage(this.value)">
            <option value='1'>1</option>          </select> -->
         跳转到 <input id="pageInput" title="按回车键跳转到指定页数" value="1" style="width:25px;"  onkeypress="if(event.keyCode==13) {listTable.gotoPage(this.value);}"   type="text">
          <input class="button" id="pagerBtn" value="GO" onclick="goToPage()" type="button">
        </span>
      </div>
<script>
function goToPage(){
	var page=$("#pageInput").val();
	listTable.gotoPage(page);
}
</script>    </td>
  </tr>
</table>

</div>

<script language="JavaScript">
listTable.recordCount = 0;
listTable.pageCount = 1;

listTable.filter.start_time = '';
listTable.filter.end_time = '';
listTable.filter.payid = '0';
listTable.filter.orderid = '0';
listTable.filter.sort_by = ' sr.add_time';
listTable.filter.sort_order = ' ASC';
listTable.filter.page = '1';
listTable.filter.page_size = '15';
listTable.filter.record_count = '0';
listTable.filter.page_count = '1';
onload = function()
{
	// 开始检查订单
	startCheckOrder();
}

function check(status)
{
	if(status <= 0){//冻结状态下结算佣金验证
		var snArray = new Array();
		var eles = document.forms['theForm'].elements;
		for (var i=0; i<eles.length; i++)
		{
			if (eles[i].tagName == 'INPUT' && eles[i].type == 'checkbox' && eles[i].checked && eles[i].value != 'on')
			{
			  snArray.push(eles[i].value);
			}
		}
		if (snArray.length == 0)
		{
			alert('请选择要结算的订单!');
			return false;
		}
		else
		{
			eles['order_id'].value = snArray.toString();
			return true;
		}
	}
	else if(status == 1){//可结算状态下撤销全部佣金
		if(confirm('撤销后，佣金状态由可结算将回归到冻结状态')){
			return true;
		}else{
			return false;
		}
	}
}

function set_time(obj,starttime,endtime){
	
	if($(obj).hasClass("cur")){
		$('.rebate_time').removeClass("cur");
		$('#start_time').val('');
		$('#end_time').val('');
	}else{
		$('.rebate_time').removeClass("cur");
		$(obj).addClass("cur");
		$('#start_time').val(starttime);
		$('#end_time').val(endtime);
	}
}

function searchSupp()
{
	//listTable.query = "search_supp_query";
	listTable.filter['start_time'] = Utils.trim(document.forms['searchForm'].elements['start_time'].value);
	listTable.filter['end_time'] = Utils.trim(document.forms['searchForm'].elements['end_time'].value);
	listTable.filter['payid'] = Utils.trim(document.forms['searchForm'].elements['payid'].value);
	listTable.filter['orderid'] = Utils.trim(document.forms['searchForm'].elements['orderid'].value);
	listTable.filter['page'] = 1;
	listTable.loadList();
}

function exportGoods()
{
	var frm=document.forms['searchForm'];
	frm.action ="supplier_rebate.php?act=export_goods&is_export=1";
	frm.submit();
}

</script>

<script type="text/javascript">
    /* 代码删除 By  www.wrzc.net Start */
	$().ready(function(){
		$(".chzn-select").chosen();
	});
    /* 代码删除 By  www.wrzc.net End */
</script>


<div id="footer">
共执行 7 个查询，用时 0.015625 秒，Gzip 已禁用，内存占用 3.571 MB<br />
版权所有 &copy; 2008-2030 广州网软志成信息科技有限公司，并保留所有权利。</div>
<!-- 新订单提示信息 -->
<div id="popMsg">
  <table cellspacing="0" cellpadding="0" width="100%" bgcolor="#cfdef4" border="0">
  <tr>
    <td style="color: #0f2c8c" width="30" height="24"></td>
    <td style="font-weight: normal; color: #1f336b; padding-top: 4px;padding-left: 4px" valign="center" width="100%"> 新订单通知</td>
    <td style="padding-top: 2px;padding-right:2px" valign="center" align="right" width="19"><span title="关闭" style="cursor: hand;cursor:pointer;color:red;font-size:12px;font-weight:bold;margin-right:4px;" onclick="Message.close()" >×</span><!-- <img title=关闭 style="cursor: hand" onclick=closediv() hspace=3 src="msgclose.jpg"> --></td>
  </tr>
  <tr>
    <td style="padding-right: 1px; padding-bottom: 1px" colspan="3" height="70">
    <div id="popMsgContent">
      <p>您有 <strong style="color:#ff0000" id="spanNewOrder">1</strong> 个新订单以及       <strong style="color:#ff0000" id="spanNewPaid">0</strong> 个新付款的订单</p>
      <p align="center" style="word-break:break-all"><a href="order.php?act=list"><span style="color:#ff0000">点击查看新订单</span></a></p>
    </div>
    </td>
  </tr>
  </table>
</div>

<!--
<embed src="images/online.wav" width="0" height="0" autostart="false" name="msgBeep" id="msgBeep" enablejavascript="true"/>
-->
<object classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000" codebase="http://active.macromedia.com/flash2/cabs/swflash.cab#version=4,0,0,0" id="msgBeep" width="1" height="1">
  <param name="movie" value="images/online.swf">
  <param name="quality" value="high">
  <embed src="images/online.swf" name="msgBeep" id="msgBeep" quality="high" width="0" height="0" type="application/x-shockwave-flash" pluginspage="http://www.macromedia.com/shockwave/download/index.cgi?p1_prod_version=shockwaveflash">
  </embed>
</object>

<script language="JavaScript">
document.onmousemove=function(e)
{
  var obj = Utils.srcElement(e);
  if (typeof(obj.onclick) == 'function' && obj.onclick.toString().indexOf('listTable.edit') != -1)
  {
    obj.title = '点击修改内容';
    obj.style.cssText = 'background: #278296;';
    obj.onmouseout = function(e)
    {
      this.style.cssText = '';
    }
  }
  else if (typeof(obj.href) != 'undefined' && obj.href.indexOf('listTable.sort') != -1)
  {
    obj.title = '点击对列表排序';
  }
}
<!--


var MyTodolist;
function showTodoList(adminid)
{
  if(!MyTodolist)
  {
    var global = $import("../js/global.js","js");
    global.onload = global.onreadystatechange= function()
    {
      if(this.readyState && this.readyState=="loading")return;
      var md5 = $import("js/md5.js","js");
      md5.onload = md5.onreadystatechange= function()
      {
        if(this.readyState && this.readyState=="loading")return;
        var todolist = $import("js/todolist.js","js");
        todolist.onload = todolist.onreadystatechange = function()
        {
          if(this.readyState && this.readyState=="loading")return;
          MyTodolist = new Todolist();
          MyTodolist.show();
        }
      }
    }
  }
  else
  {
    if(MyTodolist.visibility)
    {
      MyTodolist.hide();
    }
    else
    {
      MyTodolist.show();
    }
  }
}

if (Browser.isIE)
{
  onscroll = function()
  {
    //document.getElementById('calculator').style.top = document.body.scrollTop;
    document.getElementById('popMsg').style.top = (document.body.scrollTop + document.body.clientHeight - document.getElementById('popMsg').offsetHeight) + "px";
  }
}

if (document.getElementById("listDiv"))
{
  document.getElementById("listDiv").onmouseover = function(e)
  {
    obj = Utils.srcElement(e);

    if (obj)
    {
      if (obj.parentNode.tagName.toLowerCase() == "tr") row = obj.parentNode;
      else if (obj.parentNode.parentNode.tagName.toLowerCase() == "tr") row = obj.parentNode.parentNode;
      else return;

      for (i = 0; i < row.cells.length; i++)
      {
        if (row.cells[i].tagName != "TH") row.cells[i].style.backgroundColor = '#F4FAFB';
      }
    }

  }

  document.getElementById("listDiv").onmouseout = function(e)
  {
    obj = Utils.srcElement(e);

    if (obj)
    {
      if (obj.parentNode.tagName.toLowerCase() == "tr") row = obj.parentNode;
      else if (obj.parentNode.parentNode.tagName.toLowerCase() == "tr") row = obj.parentNode.parentNode;
      else return;

      for (i = 0; i < row.cells.length; i++)
      {
          if (row.cells[i].tagName != "TH") row.cells[i].style.backgroundColor = '';
      }
    }
  }

  document.getElementById("listDiv").onclick = function(e)
  {
    var obj = Utils.srcElement(e);

    if (obj.tagName == "INPUT" && obj.type == "checkbox")
    {
      if (!document.forms['listForm'])
      {
        return;
      }
      var nodes = document.forms['listForm'].elements;
      var checked = false;

      for (i = 0; i < nodes.length; i++)
      {
        if (nodes[i].checked)
        {
           checked = true;
           break;
         }
      }

      if(document.getElementById("btnSubmit"))
      {
        document.getElementById("btnSubmit").disabled = !checked;
      }
      for (i = 1; i <= 10; i++)
      {
        if (document.getElementById("btnSubmit" + i))
        {
          document.getElementById("btnSubmit" + i).disabled = !checked;
        }
      }
    }
  }

}

//-->
</script>
<?php
include '../admin/buy.php' ;
?>
</body>
</html>