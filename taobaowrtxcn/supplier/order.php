
<!--增值税发票_添加_START_www.wrzc.net-->
<!--增值税发票_添加_END_www.wrzc.net-->
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>管理中心 - 订单列表 </title>
<meta name="robots" content="noindex, nofollow">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" /> 
<link href="styles/general.css" rel="stylesheet" type="text/css" />
<link href="styles/main.css" rel="stylesheet" type="text/css" /> 
<script type="text/javascript" src="http://api.map.baidu.com/api?v=1.3"></script>
<script type="text/javascript" src="js/common.js"></script><script type="text/javascript" src="../js/jquery-1.6.2.min.js"></script><script type="text/javascript" src="js/jquery.json.js"></script><script type="text/javascript" src="../js/transport.js"></script>


<script language="JavaScript">
<!--
// 这里把JS用到的所有语言都赋值到这里
var process_request = "正在处理您的请求...";
var todolist_caption = "记事本";
var todolist_autosave = "自动保存";
var todolist_save = "保存";
var todolist_clear = "清除";
var todolist_confirm_save = "是否将更改保存到记事本？";
var todolist_confirm_clear = "是否清空内容？";
var remove_confirm = "删除订单将清除该订单的所有信息。您确定要这么做吗？";
var confirm_merge = "您确实要合并这两个订单吗？";
var input_price = "自定义价格";
var pls_search_user = "请搜索并选择会员";
var confirm_drop = "确认要删除该商品吗？";
var invalid_goods_number = "商品数量不正确";
var pls_search_goods = "请搜索并选择商品";
var pls_select_area = "请完整选择所在地区";
var pls_select_shipping = "请选择配送方式";
var pls_select_payment = "请选择支付方式";
var pls_select_pack = "请选择包装";
var pls_select_card = "请选择贺卡";
var pls_input_note = "请您填写备注！";
var pls_input_cancel = "请您填写取消原因！";
var pls_select_refund = "请选择退款方式！";
var pls_select_agency = "请选择办事处！";
var pls_select_other_agency = "该订单现在就属于这个办事处，请选择其他办事处！";
var loading = "加载中...";
var remove_invoice_confirm = "您确定删除该订单的发票信息吗？";
//-->
</script>
</head>
<body>
<h1>
<span class="action-span"><a href="order.php?act=order_query">订单查询</a></span>
<span class="action-span1"><a href="index.php?act=main">欢迎您，test（商家管理中心）</a> </span><span id="search_id" class="action-span1"> - 订单列表 </span>
<div style="clear:both"></div>
</h1>
<script type="text/javascript" src="../js/utils.js"></script><script type="text/javascript" src="js/listtable.js"></script><!-- 订单搜索 -->
<div class="form-div">
  <form action="javascript:searchOrder()" name="searchForm">
    <img src="images/icon_search.gif" width="26" height="22" border="0" alt="SEARCH" />
    订单号<input name="order_sn" type="text" id="order_sn" size="15">
    收货人<input name="consignee" type="text" id="consignee" size="15">
    订单状态    <select name="status" id="status">
      <option value="-1">请选择...</option>
      <option value="0" selected>待确认</option><option value="100">待付款</option><option value="101">待发货</option><option value="102">已完成</option><option value="1">付款中</option><option value="2">取消</option><option value="3">无效</option><option value="4">退货</option><option value="6">部分发货</option>    </select>
    <input type="submit" value=" 搜索 " class="button" />
    <a href="order.php?act=list&composite_status=0">待确认</a>
    <a href="order.php?act=list&composite_status=100">待付款</a>
    <a href="order.php?act=list&composite_status=101">待发货</a>
  </form>
</div>

<!-- 订单列表 -->
<form method="post" action="order.php?act=operate" name="listForm" onsubmit="return check()">
  <div class="list-div" id="listDiv">

<table cellpadding="3" cellspacing="1">
  <tr>
    <th>
      <input onclick='listTable.selectAll(this, "checkboxes")' type="checkbox" /><a href="javascript:listTable.sort('order_sn', 'DESC'); ">订单号</a>    </th>
    <th><a href="javascript:listTable.sort('add_time', 'DESC'); ">下单时间</a><img src="images/sort_desc.gif"></th>
    <th><a href="javascript:listTable.sort('consignee', 'DESC'); ">收货人</a></th>
    <th><a href="javascript:listTable.sort('total_fee', 'DESC'); ">总金额</a></th>
    <th><a href="javascript:listTable.sort('order_amount', 'DESC'); ">应付金额</a></th>
    <th>订单状态</th>
    <th>操作</th>
  <tr>
    <tr class="
  	tr_canceled 
    tr_nopay 
            ">
    <td valign="top" nowrap="nowrap"><input type="checkbox" name="checkboxes" value="2016031071251" /><a href="order.php?act=info&order_id=155" id="order_0">2016031071251</a></td>
    <td>yiren<br />03-10 17:34</td>
    <td align="left" valign="top"><a href="mailto:"> sdf</a> [TEL: -] <br />sdf </td>
    <td align="right" valign="top" nowrap="nowrap">¥44.75</td>
    <td align="right" valign="top" nowrap="nowrap">¥44.75</td>
    <td align="center" valign="top" nowrap="nowrap"><font color="red">无效</font>,未付款,未发货</td>
    <td align="center" valign="top"  nowrap="nowrap">
     <a href="order.php?act=info&order_id=155">查看</a>
          <br /><a href="javascript:;" onclick="listTable.remove(155, remove_confirm, 'remove_order')">移除</a>
              </td>
  </tr>
    <tr class="
  	 
     
    tr_receipt        ">
    <td valign="top" nowrap="nowrap"><input type="checkbox" name="checkboxes" value="2015082252907" /><a href="order.php?act=info&order_id=42" id="order_1">2015082252907</a></td>
    <td>111<br />08-22 03:38</td>
    <td align="left" valign="top"><a href="mailto:11@qq.qq"> 111</a> [TEL: --] <br />111</td>
    <td align="right" valign="top" nowrap="nowrap">¥98.00</td>
    <td align="right" valign="top" nowrap="nowrap">¥0.00</td>
    <td align="center" valign="top" nowrap="nowrap">已分单,已付款,收货确认</td>
    <td align="center" valign="top"  nowrap="nowrap">
     <a href="order.php?act=info&order_id=42">查看</a>
              </td>
  </tr>
    <tr class="
  	tr_canceled 
    tr_nopay 
            ">
    <td valign="top" nowrap="nowrap"><input type="checkbox" name="checkboxes" value="2015081491766" /><a href="order.php?act=info&order_id=36" id="order_2">2015081491766</a></td>
    <td>匿名用户<br />08-14 06:11</td>
    <td align="left" valign="top"><a href="mailto:290116505@qq.com"> cbtest</a> [TEL: --] <br />珠江新城</td>
    <td align="right" valign="top" nowrap="nowrap">¥2.00</td>
    <td align="right" valign="top" nowrap="nowrap">¥2.00</td>
    <td align="center" valign="top" nowrap="nowrap"><font color="red">无效</font>,未付款,未发货</td>
    <td align="center" valign="top"  nowrap="nowrap">
     <a href="order.php?act=info&order_id=36">查看</a>
          <br /><a href="javascript:;" onclick="listTable.remove(36, remove_confirm, 'remove_order')">移除</a>
              </td>
  </tr>
    <tr class="
  	tr_canceled 
    tr_nopay 
            ">
    <td valign="top" nowrap="nowrap"><input type="checkbox" name="checkboxes" value="2015081494919" /><a href="order.php?act=info&order_id=34" id="order_3">2015081494919</a></td>
    <td>匿名用户<br />08-14 05:59</td>
    <td align="left" valign="top"><a href="mailto:290116505@qq.com"> cbtest</a> [TEL: --] <br />珠江新城</td>
    <td align="right" valign="top" nowrap="nowrap">¥30.00</td>
    <td align="right" valign="top" nowrap="nowrap">¥30.00</td>
    <td align="center" valign="top" nowrap="nowrap"><font color="red">无效</font>,未付款,未发货</td>
    <td align="center" valign="top"  nowrap="nowrap">
     <a href="order.php?act=info&order_id=34">查看</a>
          <br /><a href="javascript:;" onclick="listTable.remove(34, remove_confirm, 'remove_order')">移除</a>
              </td>
  </tr>
    <tr class="
  	tr_canceled 
    tr_nopay 
            ">
    <td valign="top" nowrap="nowrap"><input type="checkbox" name="checkboxes" value="2015072827129" /><a href="order.php?act=info&order_id=14" id="order_4">2015072827129</a></td>
    <td>anan<br />07-28 11:16</td>
    <td align="left" valign="top"><a href="mailto:38306293@qq.com"> anan</a> [TEL: --] <br />珠江新城B区</td>
    <td align="right" valign="top" nowrap="nowrap">¥35.00</td>
    <td align="right" valign="top" nowrap="nowrap">¥35.00</td>
    <td align="center" valign="top" nowrap="nowrap"><font color="red">无效</font>,未付款,未发货</td>
    <td align="center" valign="top"  nowrap="nowrap">
     <a href="order.php?act=info&order_id=14">查看</a>
          <br /><a href="javascript:;" onclick="listTable.remove(14, remove_confirm, 'remove_order')">移除</a>
              </td>


  </tr>
    <tr class="
  	tr_canceled 
    tr_nopay 
            ">
    <td valign="top" nowrap="nowrap"><input type="checkbox" name="checkboxes" value="2015072899999" /><a href="order.php?act=info&order_id=12" id="order_5">2015072899999</a></td>
    <td>anan<br />07-28 10:55</td>
    <td align="left" valign="top"><a href="mailto:38306293@qq.com"> anan</a> [TEL: --] <br />珠江新城B区</td>
    <td align="right" valign="top" nowrap="nowrap">¥78.00</td>
    <td align="right" valign="top" nowrap="nowrap">¥78.00</td>
    <td align="center" valign="top" nowrap="nowrap"><font color="red">无效</font>,未付款,未发货</td>
    <td align="center" valign="top"  nowrap="nowrap">
     <a href="order.php?act=info&order_id=12">查看</a>
          <br /><a href="javascript:;" onclick="listTable.remove(12, remove_confirm, 'remove_order')">移除</a>
              </td>
  </tr>
    <tr class="
  	 
     
        tr_nosend    ">
    <td valign="top" nowrap="nowrap"><input type="checkbox" name="checkboxes" value="2015072891383" /><a href="order.php?act=info&order_id=10" id="order_6">2015072891383</a></td>
    <td>anan<br />07-28 10:52</td>
    <td align="left" valign="top"><a href="mailto:38306293@qq.com"> anan</a> [TEL: --] <br />珠江新城B区</td>
    <td align="right" valign="top" nowrap="nowrap">¥78.00</td>
    <td align="right" valign="top" nowrap="nowrap">¥0.00</td>
    <td align="center" valign="top" nowrap="nowrap">已确认,已付款,未发货</td>
    <td align="center" valign="top"  nowrap="nowrap">
     <a href="order.php?act=info&order_id=10">查看</a>
              </td>
  </tr>
    <tr class="
  	 
     
        tr_nosend    ">
    <td valign="top" nowrap="nowrap"><input type="checkbox" name="checkboxes" value="2015072871984" /><a href="order.php?act=info&order_id=8" id="order_7">2015072871984</a></td>
    <td>anan<br />07-28 10:50</td>
    <td align="left" valign="top"><a href="mailto:yangzhen@wrzc.net"> anan</a> [TEL: --] <br />珠江新城B区</td>
    <td align="right" valign="top" nowrap="nowrap">¥35.00</td>
    <td align="right" valign="top" nowrap="nowrap">¥0.00</td>
    <td align="center" valign="top" nowrap="nowrap">已确认,已付款,未发货</td>
    <td align="center" valign="top"  nowrap="nowrap">
     <a href="order.php?act=info&order_id=8">查看</a>
              </td>
  </tr>
    <tr class="
  	tr_canceled 
    tr_nopay 
            ">
    <td valign="top" nowrap="nowrap"><input type="checkbox" name="checkboxes" value="2015072823767" /><a href="order.php?act=info&order_id=5" id="order_8">2015072823767</a></td>
    <td>anan<br />07-28 09:25</td>
    <td align="left" valign="top"><a href="mailto:23456@qq.ukj"> anan</a> [TEL: --] <br />珠江新城B区</td>
    <td align="right" valign="top" nowrap="nowrap">¥319.00</td>
    <td align="right" valign="top" nowrap="nowrap">¥319.00</td>
    <td align="center" valign="top" nowrap="nowrap"><font color="red">无效</font>,未付款,未发货</td>
    <td align="center" valign="top"  nowrap="nowrap">
     <a href="order.php?act=info&order_id=5">查看</a>
          <br /><a href="javascript:;" onclick="listTable.remove(5, remove_confirm, 'remove_order')">移除</a>
              </td>
  </tr>
  </table>

<!-- 分页 -->
<table id="page-table" cellspacing="0">
  <tr>
    <td align="right" nowrap="true">
          <!-- $Id: page.htm 14216 2008-03-10 02:27:21Z testyang $ -->
            <div id="turn-page">
        总计  <span id="totalRecords">9</span>
        个记录分为 <span id="totalPages">1</span>
        页当前第 <span id="pageCurrent">1</span>
        页，每页 <input type='text' size='3' id='pageSize' value="15" onkeypress="return listTable.changePageSize(event)" />
        <span id="page-link">
          <a href="javascript:listTable.gotoPageFirst()">第一页</a>
          <a href="javascript:listTable.gotoPagePrev()">上一页</a>
          <a href="javascript:listTable.gotoPageNext()">下一页</a>
          <a href="javascript:listTable.gotoPageLast()">最末页</a>
 <!--   <select id="gotoPage" onchange="listTable.gotoPage(this.value)">
            <option value='1'>1</option>          </select> -->
         跳转到 <input id="pageInput" title="按回车键跳转到指定页数" value="1" style="width:25px;"  onkeypress="if(event.keyCode==13) {listTable.gotoPage(this.value);}"   type="text">
          <input class="button" id="pagerBtn" value="GO" onclick="goToPage()" type="button">
        </span>
      </div>
<script>
function goToPage(){
	var page=$("#pageInput").val();
	listTable.gotoPage(page);
}
</script>    </td>
  </tr>
</table>

  </div>
  <div>
    <input name="confirm" type="submit" id="btnSubmit" value="确认" class="button" disabled="true" onclick="this.form.target = '_self'" />
    <input name="invalid" type="submit" id="btnSubmit1" value="无效" class="button" disabled="true" onclick="this.form.target = '_self'" />
    <input name="cancel" type="submit" id="btnSubmit2" value="取消" class="button" disabled="true" onclick="this.form.target = '_self'" />
    <input name="remove" type="submit" id="btnSubmit3" value="移除" class="button" disabled="true" onclick="this.form.target = '_self'" />
    <input name="print" type="submit" id="btnSubmit4" value="打印订单" class="button" disabled="true" onclick="this.form.target = '_blank'" />
    <input name="batch" type="hidden" value="1" />
    <input name="order_id" type="hidden" value="" />
  </div>
</form>
<script language="JavaScript">
listTable.recordCount = 9;
listTable.pageCount = 1;

listTable.filter.order_sn = '';
listTable.filter.consignee = '';
listTable.filter.email = '';
listTable.filter.address = '';
listTable.filter.zipcode = '';
listTable.filter.tel = '';
listTable.filter.mobile = '0';
listTable.filter.country = '0';
listTable.filter.province = '0';
listTable.filter.city = '0';
listTable.filter.district = '0';
listTable.filter.shipping_id = '0';
listTable.filter.pay_id = '0';
listTable.filter.order_status = '-1';
listTable.filter.shipping_status = '-1';
listTable.filter.pay_status = '-1';
listTable.filter.user_id = '0';
listTable.filter.user_name = '';
listTable.filter.composite_status = '-1';
listTable.filter.group_buy_id = '0';
listTable.filter.pre_sale_id = '0';
listTable.filter.sort_by = 'add_time';
listTable.filter.sort_order = 'DESC';
listTable.filter.start_time = '';
listTable.filter.end_time = '';
listTable.filter.inv_status = '';
listTable.filter.inv_type = '';
listTable.filter.vat_inv_consignee_name = '';
listTable.filter.vat_inv_consignee_phone = '';
listTable.filter.add_time = '';
listTable.filter.page = '1';
listTable.filter.page_size = '15';
listTable.filter.record_count = '9';
listTable.filter.page_count = '1';


    onload = function()
    {
        // 开始检查订单
        startCheckOrder();
    }

    /**
     * 搜索订单
     */
    function searchOrder()
    {
        listTable.filter['order_sn'] = Utils.trim(document.forms['searchForm'].elements['order_sn'].value);
        listTable.filter['consignee'] = Utils.trim(document.forms['searchForm'].elements['consignee'].value);
        listTable.filter['composite_status'] = document.forms['searchForm'].elements['status'].value;
        listTable.filter['page'] = 1;
        listTable.loadList();
    }

    function check()
    {
      var snArray = new Array();
      var eles = document.forms['listForm'].elements;
      for (var i=0; i<eles.length; i++)
      {
        if (eles[i].tagName == 'INPUT' && eles[i].type == 'checkbox' && eles[i].checked && eles[i].value != 'on')
        {
          snArray.push(eles[i].value);
        }
      }
      if (snArray.length == 0)
      {
        return false;
      }
      else
      {
        eles['order_id'].value = snArray.toString();
        return true;
      }
    }
    /**
     * 显示订单商品及缩图
     */
    var show_goods_layer = 'order_goods_layer';
    var goods_hash_table = new Object;
    var timer = new Object;

    /**
     * 绑定订单号事件
     *
     * @return void
     */
    function bind_order_event()
    {
        var order_seq = 0;
        while(true)
        {
            var order_sn = Utils.$('order_'+order_seq);
            if (order_sn)
            {
                order_sn.onmouseover = function(e)
                {
                    try
                    {
                        window.clearTimeout(timer);
                    }
                    catch(e)
                    {
                    }
                    var order_id = Utils.request(this.href, 'order_id');
                    show_order_goods(e, order_id, show_goods_layer);
                }
                order_sn.onmouseout = function(e)
                {
                    hide_order_goods(show_goods_layer)
                }
                order_seq++;
            }
            else
            {
                break;
            }
        }
    }
    listTable.listCallback = function(result, txt) 
    {
        if (result.error > 0) 
        {
            alert(result.message);
        }
        else 
        {
            try 
            {
                document.getElementById('listDiv').innerHTML = result.content;
                bind_order_event();
                if (typeof result.filter == "object") 
                {
                    listTable.filter = result.filter;
                }
                listTable.pageCount = result.page_count;
            }
            catch(e)
            {
                alert(e.message);
            }
        }
    }
    /**
     * 浏览器兼容式绑定Onload事件
     *
     */
    if (Browser.isIE)
    {
        window.attachEvent("onload", bind_order_event);
    }
    else
    {
        window.addEventListener("load", bind_order_event, false);
    }

    /**
     * 建立订单商品显示层
     *
     * @return void
     */
    function create_goods_layer(id)
    {
        if (!Utils.$(id))
        {
            var n_div = document.createElement('DIV');
            n_div.id = id;
            n_div.className = 'order-goods';
            document.body.appendChild(n_div);
            Utils.$(id).onmouseover = function()
            {
                window.clearTimeout(window.timer);
            }
            Utils.$(id).onmouseout = function()
            {
                hide_order_goods(id);
            }
        }
        else
        {
            Utils.$(id).style.display = '';
        }
    }

    /**
     * 显示订单商品数据
     *
     * @return void
     */
    function show_order_goods(e, order_id, layer_id)
    {
        create_goods_layer(layer_id);
        $layer_id = Utils.$(layer_id);
        $layer_id.style.top = (Utils.y(e) + 12) + 'px';
        $layer_id.style.left = (Utils.x(e) + 12) + 'px';
        if (typeof(goods_hash_table[order_id]) == 'object')
        {
            response_goods_info(goods_hash_table[order_id]);
        }
        else
        {
            $layer_id.innerHTML = loading;
            Ajax.call('order.php?is_ajax=1&act=get_goods_info&order_id='+order_id, '', response_goods_info , 'POST', 'JSON');
        }
    }

    /**
     * 隐藏订单商品
     *
     * @return void
     */
    function hide_order_goods(layer_id)
    {
        $layer_id = Utils.$(layer_id);
        window.timer = window.setTimeout('$layer_id.style.display = "none"', 500);
    }

    /**
     * 处理订单商品的Callback
     *
     * @return void
     */
    function response_goods_info(result)
    {
        if (result.error > 0)
        {
            alert(result.message);
            hide_order_goods(show_goods_layer);
            return;
        }
        if (typeof(goods_hash_table[result.content[0].order_id]) == 'undefined')
        {
            goods_hash_table[result.content[0].order_id] = result;
        }
        Utils.$(show_goods_layer).innerHTML = result.content[0].str;
    }
</script>


<div id="footer">
共执行 15 个查询，用时 0.021001 秒，Gzip 已禁用，内存占用 5.569 MB<br />
版权所有 &copy; 2008-2030 广州网软志成信息科技有限公司，并保留所有权利。</div>
<!-- 新订单提示信息 -->
<div id="popMsg">
  <table cellspacing="0" cellpadding="0" width="100%" bgcolor="#cfdef4" border="0">
  <tr>
    <td style="color: #0f2c8c" width="30" height="24"></td>
    <td style="font-weight: normal; color: #1f336b; padding-top: 4px;padding-left: 4px" valign="center" width="100%"> 新订单通知</td>
    <td style="padding-top: 2px;padding-right:2px" valign="center" align="right" width="19"><span title="关闭" style="cursor: hand;cursor:pointer;color:red;font-size:12px;font-weight:bold;margin-right:4px;" onclick="Message.close()" >×</span><!-- <img title=关闭 style="cursor: hand" onclick=closediv() hspace=3 src="msgclose.jpg"> --></td>
  </tr>
  <tr>
    <td style="padding-right: 1px; padding-bottom: 1px" colspan="3" height="70">
    <div id="popMsgContent">
      <p>您有 <strong style="color:#ff0000" id="spanNewOrder">1</strong> 个新订单以及       <strong style="color:#ff0000" id="spanNewPaid">0</strong> 个新付款的订单</p>
      <p align="center" style="word-break:break-all"><a href="order.php?act=list"><span style="color:#ff0000">点击查看新订单</span></a></p>
    </div>
    </td>
  </tr>
  </table>
</div>

<!--
<embed src="images/online.wav" width="0" height="0" autostart="false" name="msgBeep" id="msgBeep" enablejavascript="true"/>
-->
<object classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000" codebase="http://active.macromedia.com/flash2/cabs/swflash.cab#version=4,0,0,0" id="msgBeep" width="1" height="1">
  <param name="movie" value="images/online.swf">
  <param name="quality" value="high">
  <embed src="images/online.swf" name="msgBeep" id="msgBeep" quality="high" width="0" height="0" type="application/x-shockwave-flash" pluginspage="http://www.macromedia.com/shockwave/download/index.cgi?p1_prod_version=shockwaveflash">
  </embed>
</object>

<script language="JavaScript">
document.onmousemove=function(e)
{
  var obj = Utils.srcElement(e);
  if (typeof(obj.onclick) == 'function' && obj.onclick.toString().indexOf('listTable.edit') != -1)
  {
    obj.title = '点击修改内容';
    obj.style.cssText = 'background: #278296;';
    obj.onmouseout = function(e)
    {
      this.style.cssText = '';
    }
  }
  else if (typeof(obj.href) != 'undefined' && obj.href.indexOf('listTable.sort') != -1)
  {
    obj.title = '点击对列表排序';
  }
}
<!--


var MyTodolist;
function showTodoList(adminid)
{
  if(!MyTodolist)
  {
    var global = $import("../js/global.js","js");
    global.onload = global.onreadystatechange= function()
    {
      if(this.readyState && this.readyState=="loading")return;
      var md5 = $import("js/md5.js","js");
      md5.onload = md5.onreadystatechange= function()
      {
        if(this.readyState && this.readyState=="loading")return;
        var todolist = $import("js/todolist.js","js");
        todolist.onload = todolist.onreadystatechange = function()
        {
          if(this.readyState && this.readyState=="loading")return;
          MyTodolist = new Todolist();
          MyTodolist.show();
        }
      }
    }
  }
  else
  {
    if(MyTodolist.visibility)
    {
      MyTodolist.hide();
    }
    else
    {
      MyTodolist.show();
    }
  }
}

if (Browser.isIE)
{
  onscroll = function()
  {
    //document.getElementById('calculator').style.top = document.body.scrollTop;
    document.getElementById('popMsg').style.top = (document.body.scrollTop + document.body.clientHeight - document.getElementById('popMsg').offsetHeight) + "px";
  }
}

if (document.getElementById("listDiv"))
{
  document.getElementById("listDiv").onmouseover = function(e)
  {
    obj = Utils.srcElement(e);

    if (obj)
    {
      if (obj.parentNode.tagName.toLowerCase() == "tr") row = obj.parentNode;
      else if (obj.parentNode.parentNode.tagName.toLowerCase() == "tr") row = obj.parentNode.parentNode;
      else return;

      for (i = 0; i < row.cells.length; i++)
      {
        if (row.cells[i].tagName != "TH") row.cells[i].style.backgroundColor = '#F4FAFB';
      }
    }

  }

  document.getElementById("listDiv").onmouseout = function(e)
  {
    obj = Utils.srcElement(e);

    if (obj)
    {
      if (obj.parentNode.tagName.toLowerCase() == "tr") row = obj.parentNode;
      else if (obj.parentNode.parentNode.tagName.toLowerCase() == "tr") row = obj.parentNode.parentNode;
      else return;

      for (i = 0; i < row.cells.length; i++)
      {
          if (row.cells[i].tagName != "TH") row.cells[i].style.backgroundColor = '';
      }
    }
  }

  document.getElementById("listDiv").onclick = function(e)
  {
    var obj = Utils.srcElement(e);

    if (obj.tagName == "INPUT" && obj.type == "checkbox")
    {
      if (!document.forms['listForm'])
      {
        return;
      }
      var nodes = document.forms['listForm'].elements;
      var checked = false;

      for (i = 0; i < nodes.length; i++)
      {
        if (nodes[i].checked)
        {
           checked = true;
           break;
         }
      }

      if(document.getElementById("btnSubmit"))
      {
        document.getElementById("btnSubmit").disabled = !checked;
      }
      for (i = 1; i <= 10; i++)
      {
        if (document.getElementById("btnSubmit" + i))
        {
          document.getElementById("btnSubmit" + i).disabled = !checked;
        }
      }
    }
  }

}

//-->
</script>
</body>
</html><!--增值税发票_添加_START_www.wrzc.net-->
<!--增值税发票_添加_END_www.wrzc.net-->

<!-- $Id: order_info.htm 17060 2010-03-25 03:44:42Z liuhui $ -->

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>管理中心 - 订单信息 </title>
<meta name="robots" content="noindex, nofollow">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" /> 
<link href="styles/general.css" rel="stylesheet" type="text/css" />
<link href="styles/main.css" rel="stylesheet" type="text/css" /> 
<script type="text/javascript" src="http://api.map.baidu.com/api?v=1.3"></script>
<script type="text/javascript" src="js/common.js"></script><script type="text/javascript" src="../js/jquery-1.6.2.min.js"></script><script type="text/javascript" src="js/jquery.json.js"></script><script type="text/javascript" src="../js/transport.js"></script>


<script language="JavaScript">
<!--
// 这里把JS用到的所有语言都赋值到这里
var process_request = "正在处理您的请求...";
var todolist_caption = "记事本";
var todolist_autosave = "自动保存";
var todolist_save = "保存";
var todolist_clear = "清除";
var todolist_confirm_save = "是否将更改保存到记事本？";
var todolist_confirm_clear = "是否清空内容？";
var remove_confirm = "删除订单将清除该订单的所有信息。您确定要这么做吗？";
var confirm_merge = "您确实要合并这两个订单吗？";
var input_price = "自定义价格";
var pls_search_user = "请搜索并选择会员";
var confirm_drop = "确认要删除该商品吗？";
var invalid_goods_number = "商品数量不正确";
var pls_search_goods = "请搜索并选择商品";
var pls_select_area = "请完整选择所在地区";
var pls_select_shipping = "请选择配送方式";
var pls_select_payment = "请选择支付方式";
var pls_select_pack = "请选择包装";
var pls_select_card = "请选择贺卡";
var pls_input_note = "请您填写备注！";
var pls_input_cancel = "请您填写取消原因！";
var pls_select_refund = "请选择退款方式！";
var pls_select_agency = "请选择办事处！";
var pls_select_other_agency = "该订单现在就属于这个办事处，请选择其他办事处！";
var loading = "加载中...";
var remove_invoice_confirm = "您确定删除该订单的发票信息吗？";
//-->
</script>
</head>
<body>
<h1>
<span class="action-span"><a href="order.php?act=list&uselastfilter=1">订单列表</a></span>
<span class="action-span1"><a href="index.php?act=main">欢迎您，test（商家管理中心）</a> </span><span id="search_id" class="action-span1"> - 订单信息 </span>
<div style="clear:both"></div>
</h1>
<script type="text/javascript" src="js/topbar.js"></script><script type="text/javascript" src="../js/utils.js"></script><script type="text/javascript" src="js/listtable.js"></script><script type="text/javascript" src="js/selectzone.js"></script><script type="text/javascript" src="../js/common.js"></script><div id="topbar">
  <div align="right"><a href="" onclick="closebar(); return false"><img src="images/close.gif" border="0" /></a></div>
  <table width="100%" border="0">
    <caption><strong> 购货人信息 </strong></caption>
    <tr>
      <td> 电子邮件 </td>
      <td> <a href="mailto:38306293@qq.com">38306293@qq.com</a> </td>
    </tr>
    <tr>
      <td> 账户余额 </td>
      <td> ¥6090.57 </td>
    </tr>
    <tr>
      <td> 消费积分 </td>
      <td> 14585 </td>
    </tr>
    <tr>
      <td> 等级积分 </td>
      <td> 14485 </td>
    </tr>
    <tr>
      <td> 会员等级 </td>
      <td> 钻石会员 </td>
    </tr>
    <tr>
      <td> 红包数量 </td>
      <td> 15 </td>
    </tr>
  </table>

    <table width="100%" border="0">
    <caption><strong> 收货人 : sdf </strong></caption>
    <tr>
      <td> 电子邮件 </td>
      <td> <a href="mailto:"></a> </td>
    </tr>
    <tr>
      <td> 地址 </td>
      <td> sdf  </td>
    </tr>
    <tr>
      <td> 邮编 </td>
      <td>  </td>
    </tr>
    <tr>
      <td> 电话 </td>
      <td> - </td>
    </tr>
    <tr>
      <td> 备用电话 </td>
      <td> 13245855555 </td>
    </tr>
  </table>
  </div>

<form action="order.php?act=operate" method="post" name="theForm">
<div class="list-div" style="margin-bottom: 5px">
<table width="100%" cellpadding="3" cellspacing="1">
  <tr>
    <td colspan="4">
      <div align="center">
        <input name="prev" type="button" class="button" onClick="location.href='order.php?act=info&order_id=42';" value="前一个订单"  />
        <input name="next" type="button" class="button" onClick="location.href='order.php?act=info&order_id=';" value="后一个订单" disabled />
        <input type="button" onclick="window.open('order.php?act=info&order_id=155&print=1')" class="button" value="打印订单" />
    </div></td>
  </tr>
  <tr>
    <th colspan="4">基本信息</th>
  </tr>
  <tr>
    <td width="18%"><div align="right"><strong>订单号：</strong></div></td>
    <td width="34%">2016031071251</td>
    <td width="15%"><div align="right"><strong>订单状态：</strong></div></td>
    <td><font color="red">无效</font>,未付款,未发货</td>
  </tr>
  <tr>
    <td><div align="right"><strong>购货人：</strong></div></td>
    <td>yiren [ <a href="" onclick="staticbar();return false;">显示购货人信息</a> ] [ <a href="user_msg.php?act=add&order_id=155&user_id=6">发送/查看留言</a> ]</td>
    <td><div align="right"><strong>下单时间：</strong></div></td>
    <td>2016-03-10 17:34:52</td>
  </tr>
  <tr>
    <td><div align="right"><strong>支付方式：</strong></div></td>
    <td>货到付款<a href="order.php?act=edit&order_id=155&step=payment" class="special">编辑</a>
    (备注: <span onclick="listTable.edit(this, 'edit_pay_note', 155)">N/A</span>)</td>
    <td><div align="right"><strong>付款时间：</strong></div></td>
    <td>未付款</td>
  </tr>
  <tr>
    <td><div align="right"><strong>配送方式：</strong></div></td>
     <td><span id="shipping_name">申通快递</span><a href="order.php?act=edit&order_id=155&step=shipping" class="special">编辑</a>&nbsp;&nbsp;<input type="button" onclick="window.open('order.php?act=info&order_id=155&shipping_print=1')" class="button" value="打印快递单"> </td>
    <td><div align="right"><strong>发货时间：</strong></div></td>
    <td>未发货</td>
  </tr>
  <tr>
    <td><div align="right"><strong>发货单号：</strong></div></td>
    <td></td>
    <td><div align="right"><strong>订单来源：</strong></div></td>
    <td>pc</td>
  </tr>
   <!--增值税发票_添加_START_www.wrzc.net-->
  <!--普通发票显示内容-->
  <tr>
    <th colspan='4'>发票信息        <a href='order.php?act=edit&order_id=155&step=invoice&step_detail=edit' class='special'>添加</a>
        </th>
  </tr>
    <tr>
    <td colspan='4'><div align="center" style='height:50px;line-height:50px;'><strong>没有发票信息</strong></div></td>
  </tr>
    <!--增值税发票_添加_END_www.wrzc.net-->
  <tr>
    <th colspan="4">其他信息<a href="order.php?act=edit&order_id=155&step=other" class="special">编辑</a></th>
    </tr>
  <tr>
    <td><div align="right"><strong>客户给商家的留言：</strong></div></td>
    <td colspan="3"></td>
  </tr>
  <tr>
    <td><div align="right"><strong>缺货处理：</strong></div></td>
    <td>等待所有商品备齐后再发</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td><div align="right"><strong>包装：</strong></div></td>
    <td></td>
    <td><div align="right"><strong>贺卡：</strong></div></td>
    <td></td>
  </tr>
  <tr>
    <td><div align="right"><strong>贺卡祝福语：</strong></div></td>
    <td colspan="3"></td>
  </tr>
  <tr>
    <td><div align="right"><strong>商家给客户的留言：</strong></div></td>
    <td colspan="3"> </td>
  </tr>
  <tr>
    <th colspan="4">收货人信息<a href="order.php?act=edit&order_id=155&step=consignee" class="special">编辑</a></th>
    </tr>
  <tr>
    <td><div align="right"><strong>收货人：</strong></div></td>
    <td>sdf</td>
    <td><div align="right"><strong>电子邮件：</strong></div></td>
    <td></td>
  </tr>
  <tr>
    <td><div align="right"><strong>地址：</strong></div></td>
    <td>[中国  河北  秦皇岛  海港区] sdf </td>
    <td><div align="right"><strong>邮编：</strong></div></td>
    <td></td>
  </tr>
  <tr>
    <td><div align="right"><strong>电话：</strong></div></td>
    <td>-</td>
    <td><div align="right"><strong>手机：</strong></div></td>
    <td>13245855555</td>
  </tr>
  <tr>
    <!--<td><div align="right"><strong>标志性建筑：</strong></div></td>
    <td></td>-->
    <td><div align="right"><strong>最佳送货时间：</strong></div></td>
    <td colspan="3">仅工作日送货</td>
  </tr>
</table>
</div>

<div class="list-div" style="margin-bottom: 5px">
<table width="100%" cellpadding="3" cellspacing="1">
  <tr>
    <th colspan="10" scope="col">商品信息<a href="order.php?act=edit&order_id=155&step=goods" class="special">编辑</a></th>
    </tr>
  <tr>
    <td scope="col"><div align="center"><strong>商品名称 [ 品牌 ]</strong></div></td>
    <td scope="col"><div align="center"><strong>售后</strong></div></td>
    <td scope="col"><div align="center"><strong>货号</strong></div></td>
    <td scope="col"><div align="center"><strong>货品号</strong></div></td>
    <td scope="col"><div align="center"><strong>价格</strong></div></td>
	<td scope="col"><div align="center"><strong>手机专享价格</strong></div></td><!--手机专享价格 app jx -->
    <td scope="col"><div align="center"><strong>数量</strong></div></td>
    <td scope="col"><div align="center"><strong>属性</strong></div></td>
    <td scope="col"><div align="center"><strong>库存</strong></div></td>
    <td scope="col"><div align="center"><strong>小计</strong></div></td>
  </tr>
    <tr>
    <td>
        <a href="../goods.php?id=10" target="_blank">越南红心火龙果5斤装 红肉火龙果 新鲜进口水果         </a>
        </td>
    <td>正常</td>
    <td>WRZ000010</td>
    <td></td>
    <td><div align="right">¥29.75</div></td>
	<td><div align="right">没有手机专享价格</div></td><!--手机专享价格 app jx -->
    <td><div align="right">1    </div></td>
    <td></td>
    <td><div align="right">997</div></td>
    <td><div align="right">¥29.75</div></td>
  </tr>
    <tr>
    <td></td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td></td>
    <td></td>
    <td>&nbsp;</td>
    <td><div align="right"><strong>合计：</strong></div></td>
    <td><div align="right">¥29.75</div></td>
   
  </tr>
</table>
</div>

<div class="list-div" style="margin-bottom: 5px">
<table width="100%" cellpadding="3" cellspacing="1">
  <tr>
    <th>费用信息<a href="order.php?act=edit&order_id=155&step=money" class="special">编辑</a></th>
  </tr>
  <tr>
    <td><div align="right">商品总金额：<strong>¥29.75</strong>
- 折扣：<strong>¥0.00</strong>     + 发票税额：<strong>¥0.00</strong>
      + 配送费用：<strong>¥15.00</strong>
      + 保价费用：<strong>¥0.00</strong>
      + 支付费用：<strong>¥0.00</strong>
      + 包装费用：<strong>¥0.00</strong>
      + 贺卡费用：<strong>¥0.00</strong></div></td>
  <tr>
    <td><div align="right"> = 订单总金额：<strong>¥44.75</strong></div></td>
  </tr>
  <tr>
    <td><div align="right">
      - 已付款金额：<strong>¥0.00</strong> - 使用余额： <strong>¥0.00</strong>
      - 使用积分： <strong>¥0.00</strong>
      - 使用红包： <strong>¥0.00</strong>
    </div></td>
  <tr>
    <td><div align="right"> = 应付款金额：<strong>¥44.75</strong>
      </div></td>
  </tr>
</table>
</div>

<div class="list-div" style="margin-bottom: 5px">
<table cellpadding="3" cellspacing="1">
  <tr>
    <th colspan="6">操作信息</th>
  </tr>
  <tr>
    <td><div align="right"><strong>操作备注：</strong></div></td>
  <td colspan="5"><textarea name="action_note" cols="80" rows="3"></textarea></td>
    </tr>
  <tr>
    <td><div align="right"></div>
      <div align="right"><strong>当前可执行操作：</strong> </div></td>
    <!-- 一键发货begin 修改 by www.wrzc.net  -->  
        <td colspan="5">
        <!-- 一键发货end 修改 by www.wrzc.net -->        <input name="confirm" type="submit" value="确认" class="button" />
                   <input name="after_service" type="submit" value="售后" class="button" />        <input name="remove" type="submit" value="移除" class="button" onClick="return window.confirm('删除订单将清除该订单的所有信息。您确定要这么做吗？');" />
                                        <input name="assign" type="submit" value="指派给" class="button" onclick="return assignTo(document.forms['theForm'].elements['agency_id'].value)" />
        <select name="agency_id"><option value="0">请选择...</option>
                <option value="1" >广州办事处</option>
                </select>
                <input name="order_id" type="hidden" value="155"></td>
    </tr>
  <tr>
    <th>操作者：</th>
    <th>操作时间</th>
    <th>订单状态</th>
    <th>付款状态</th>
    <th>发货状态</th>
    <th>备注</th>
  </tr>
  </table>
<!--  -->
</div>
</form>

<script language="JavaScript">

  var oldAgencyId = 0;

  onload = function()
  {
    // 开始检查订单
    startCheckOrder();
  }

  /**
   * 把订单指派给某办事处
   * @param int agencyId
   */
  function assignTo(agencyId)
  {
    if (agencyId == 0)
    {
      alert(pls_select_agency);
      return false;
    }
    if (oldAgencyId != 0 && agencyId == oldAgencyId)
    {
      alert(pls_select_other_agency);
      return false;
    }
    return true;
  }
</script>
<script language="javascript">
get_invoice_info('','',1);

function get_invoice_info(expressid,expressno,div_id)
{
	$("#ul_i").children("li").removeClass();
	document.getElementById("div_i_"+div_id).className = 'selected';
	Ajax.call(
		'../plugins/kuaidi100/kuaidi100_post.php?com='+ expressid+'&nu=' + expressno, 
		'showtest=showtest', 
		function(data){
			document.getElementById("retData").innerHTML='快递公司：'+expressid+' &nbsp; 运单号：'+expressno+'<br>';
			document.getElementById("retData").innerHTML+=data;
		}, 
		'GET', 
		'TEXT', 
		false
	);
}
</script>


<div id="footer">
共执行 23 个查询，用时 0.225013 秒，Gzip 已禁用，内存占用 5.552 MB<br />
版权所有 &copy; 2008-2030 广州网软志成信息科技有限公司，并保留所有权利。</div>
<!-- 新订单提示信息 -->
<div id="popMsg">
  <table cellspacing="0" cellpadding="0" width="100%" bgcolor="#cfdef4" border="0">
  <tr>
    <td style="color: #0f2c8c" width="30" height="24"></td>
    <td style="font-weight: normal; color: #1f336b; padding-top: 4px;padding-left: 4px" valign="center" width="100%"> 新订单通知</td>
    <td style="padding-top: 2px;padding-right:2px" valign="center" align="right" width="19"><span title="关闭" style="cursor: hand;cursor:pointer;color:red;font-size:12px;font-weight:bold;margin-right:4px;" onclick="Message.close()" >×</span><!-- <img title=关闭 style="cursor: hand" onclick=closediv() hspace=3 src="msgclose.jpg"> --></td>
  </tr>
  <tr>
    <td style="padding-right: 1px; padding-bottom: 1px" colspan="3" height="70">
    <div id="popMsgContent">
      <p>您有 <strong style="color:#ff0000" id="spanNewOrder">1</strong> 个新订单以及       <strong style="color:#ff0000" id="spanNewPaid">0</strong> 个新付款的订单</p>
      <p align="center" style="word-break:break-all"><a href="order.php?act=list"><span style="color:#ff0000">点击查看新订单</span></a></p>
    </div>
    </td>
  </tr>
  </table>
</div>

<!--
<embed src="images/online.wav" width="0" height="0" autostart="false" name="msgBeep" id="msgBeep" enablejavascript="true"/>
-->
<object classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000" codebase="http://active.macromedia.com/flash2/cabs/swflash.cab#version=4,0,0,0" id="msgBeep" width="1" height="1">
  <param name="movie" value="images/online.swf">
  <param name="quality" value="high">
  <embed src="images/online.swf" name="msgBeep" id="msgBeep" quality="high" width="0" height="0" type="application/x-shockwave-flash" pluginspage="http://www.macromedia.com/shockwave/download/index.cgi?p1_prod_version=shockwaveflash">
  </embed>
</object>

<script language="JavaScript">
document.onmousemove=function(e)
{
  var obj = Utils.srcElement(e);
  if (typeof(obj.onclick) == 'function' && obj.onclick.toString().indexOf('listTable.edit') != -1)
  {
    obj.title = '点击修改内容';
    obj.style.cssText = 'background: #278296;';
    obj.onmouseout = function(e)
    {
      this.style.cssText = '';
    }
  }
  else if (typeof(obj.href) != 'undefined' && obj.href.indexOf('listTable.sort') != -1)
  {
    obj.title = '点击对列表排序';
  }
}
<!--


var MyTodolist;
function showTodoList(adminid)
{
  if(!MyTodolist)
  {
    var global = $import("../js/global.js","js");
    global.onload = global.onreadystatechange= function()
    {
      if(this.readyState && this.readyState=="loading")return;
      var md5 = $import("js/md5.js","js");
      md5.onload = md5.onreadystatechange= function()
      {
        if(this.readyState && this.readyState=="loading")return;
        var todolist = $import("js/todolist.js","js");
        todolist.onload = todolist.onreadystatechange = function()
        {
          if(this.readyState && this.readyState=="loading")return;
          MyTodolist = new Todolist();
          MyTodolist.show();
        }
      }
    }
  }
  else
  {
    if(MyTodolist.visibility)
    {
      MyTodolist.hide();
    }
    else
    {
      MyTodolist.show();
    }
  }
}

if (Browser.isIE)
{
  onscroll = function()
  {
    //document.getElementById('calculator').style.top = document.body.scrollTop;
    document.getElementById('popMsg').style.top = (document.body.scrollTop + document.body.clientHeight - document.getElementById('popMsg').offsetHeight) + "px";
  }
}

if (document.getElementById("listDiv"))
{
  document.getElementById("listDiv").onmouseover = function(e)
  {
    obj = Utils.srcElement(e);

    if (obj)
    {
      if (obj.parentNode.tagName.toLowerCase() == "tr") row = obj.parentNode;
      else if (obj.parentNode.parentNode.tagName.toLowerCase() == "tr") row = obj.parentNode.parentNode;
      else return;

      for (i = 0; i < row.cells.length; i++)
      {
        if (row.cells[i].tagName != "TH") row.cells[i].style.backgroundColor = '#F4FAFB';
      }
    }

  }

  document.getElementById("listDiv").onmouseout = function(e)
  {
    obj = Utils.srcElement(e);

    if (obj)
    {
      if (obj.parentNode.tagName.toLowerCase() == "tr") row = obj.parentNode;
      else if (obj.parentNode.parentNode.tagName.toLowerCase() == "tr") row = obj.parentNode.parentNode;
      else return;

      for (i = 0; i < row.cells.length; i++)
      {
          if (row.cells[i].tagName != "TH") row.cells[i].style.backgroundColor = '';
      }
    }
  }

  document.getElementById("listDiv").onclick = function(e)
  {
    var obj = Utils.srcElement(e);

    if (obj.tagName == "INPUT" && obj.type == "checkbox")
    {
      if (!document.forms['listForm'])
      {
        return;
      }
      var nodes = document.forms['listForm'].elements;
      var checked = false;

      for (i = 0; i < nodes.length; i++)
      {
        if (nodes[i].checked)
        {
           checked = true;
           break;
         }
      }

      if(document.getElementById("btnSubmit"))
      {
        document.getElementById("btnSubmit").disabled = !checked;
      }
      for (i = 1; i <= 10; i++)
      {
        if (document.getElementById("btnSubmit" + i))
        {
          document.getElementById("btnSubmit" + i).disabled = !checked;
        }
      }
    }
  }

}

//-->
</script>
</body>
</html>

<!-- $Id: order_query.htm 14216 2008-03-10 02:27:21Z testyang $ -->
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>管理中心 - 订单查询 </title>
<meta name="robots" content="noindex, nofollow">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" /> 
<link href="styles/general.css" rel="stylesheet" type="text/css" />
<link href="styles/main.css" rel="stylesheet" type="text/css" /> 
<script type="text/javascript" src="http://api.map.baidu.com/api?v=1.3"></script>
<script type="text/javascript" src="js/common.js"></script><script type="text/javascript" src="../js/jquery-1.6.2.min.js"></script><script type="text/javascript" src="js/jquery.json.js"></script><script type="text/javascript" src="../js/transport.js"></script>


<script language="JavaScript">
<!--
// 这里把JS用到的所有语言都赋值到这里
var process_request = "正在处理您的请求...";
var todolist_caption = "记事本";
var todolist_autosave = "自动保存";
var todolist_save = "保存";
var todolist_clear = "清除";
var todolist_confirm_save = "是否将更改保存到记事本？";
var todolist_confirm_clear = "是否清空内容？";
var remove_confirm = "删除订单将清除该订单的所有信息。您确定要这么做吗？";
var confirm_merge = "您确实要合并这两个订单吗？";
var input_price = "自定义价格";
var pls_search_user = "请搜索并选择会员";
var confirm_drop = "确认要删除该商品吗？";
var invalid_goods_number = "商品数量不正确";
var pls_search_goods = "请搜索并选择商品";
var pls_select_area = "请完整选择所在地区";
var pls_select_shipping = "请选择配送方式";
var pls_select_payment = "请选择支付方式";
var pls_select_pack = "请选择包装";
var pls_select_card = "请选择贺卡";
var pls_input_note = "请您填写备注！";
var pls_input_cancel = "请您填写取消原因！";
var pls_select_refund = "请选择退款方式！";
var pls_select_agency = "请选择办事处！";
var pls_select_other_agency = "该订单现在就属于这个办事处，请选择其他办事处！";
var loading = "加载中...";
var remove_invoice_confirm = "您确定删除该订单的发票信息吗？";
//-->
</script>
</head>
<body>
<h1>
<span class="action-span"><a href="order.php?act=list">订单列表</a></span>
<span class="action-span1"><a href="index.php?act=main">欢迎您，test（商家管理中心）</a> </span><span id="search_id" class="action-span1"> - 订单查询 </span>
<div style="clear:both"></div>
</h1>
<script type="text/javascript" src="../js/calendar.php"></script>
<link href="../js/calendar/calendar.css" rel="stylesheet" type="text/css" />
<div class="main-div">
<form action="order.php?act=list" method="post" enctype="multipart/form-data" name="searchForm">
  <table cellspacing="1" cellpadding="3" width="100%">
    <tr>
      <td align="right">订单号：</td>
      <td><input name="order_sn" type="text" id="order_sn" size="30"></td>
      <td align="right">电子邮件：</td>
      <td><input name="email" type="text" id="email" size="30"></td>
    </tr>
    <tr>
      <td align="right">购货人：</td>
      <td><input name="user_name" type="text" id="user_name" size="30"></td>
      <td align="right">收货人：</td>
      <td><input name="consignee" type="text" id="consignee" size="30"></td>
    </tr>
    <tr>
      <td align="right">地址：</td>
      <td><input name="address" type="text" id="address" size="30"></td>
      <td align="right">邮编：</td>
      <td><input name="zipcode" type="text" id="zipcode" size="30"></td>
    </tr>
    <tr>
      <td align="right">电话：</td>
      <td><input name="tel" type="text" id="tel" size="30"></td>
      <td align="right">手机：</td>
      <td><input name="mobile" type="text" id="mobile" size="30"></td>
    </tr>
    <tr>
      <td align="right">所在地区：</td>
      <td colspan="3"><select name="country" id="selCountries" onchange="region.changed(this, 1, 'selProvinces')">
          <option value="0">请选择...</option>
                    <option value="1">中国</option>
                </select>
        <select name="province" id="selProvinces" onchange="region.changed(this, 2, 'selCities')">
          <option value="0">请选择...</option>
        </select>
        <select name="city" id="selCities" onchange="region.changed(this, 3, 'selDistricts')">
          <option value="0">请选择...</option>
        </select>
        <select name="district" id="selDistricts">
          <option value="0">请选择...</option>
        </select></td>
      </tr>
    <tr>
      <td align="right">配送方式：</td>
      <td><select name="shipping_id" id="select4">
        <option value="0">请选择...</option>
                <option value="27">韵达快递</option>
                <option value="3">顺丰速运</option>
                <option value="12">申通快递</option>
                <option value="5">中通速递</option>
                <option value="25">圆通速递</option>
                <option value="7">上门取货</option>
                <option value="8">城际快递</option>
                <option value="10">申通快递</option>
                <option value="11"><font color="#ff3300">同城快递</font></option>
                <option value="15">申通快递</option>
                <option value="16">宅急送</option>
                <option value="17">申通快递</option>
                <option value="18">申通快递</option>
                <option value="19">门店自提</option>
                <option value="20">申通快递</option>
                <option value="26">顺丰速运</option>
                <option value="22">天天快递</option>
                <option value="24">中通速递</option>
                <option value="28">中通速递</option>
                    </select></td>
      <td align="right">支付方式：</td>
      <td><select name="pay_id" id="select5">
        <option value="0">请选择...</option>
                <option value="1">支付宝</option>
                <option value="3">支付宝-网银直连</option>
                <option value="4">余额支付</option>
                <option value="5">银联在线</option>
                <option value="6">货到付款</option>
                <option value="7">微信支付</option>
                    </select></td>
    </tr>
    <tr>
      <td align="right">下单时间：</td>
      <td colspan="3">
      <input type="text" name="start_time" maxlength="60" size="30" readonly="readonly" id="start_time_id" />
      <input name="start_time_btn" type="button" id="start_time_btn" onclick="return showCalendar('start_time_id', '%Y-%m-%d %H:%M', '24', false, 'start_time_btn');" value="选择" class="button"/>
      ~      
      <input type="text" name="end_time" maxlength="60" size="30" readonly="readonly" id="end_time_id" />
      <input name="end_time_btn" type="button" id="end_time_btn" onclick="return showCalendar('end_time_id', '%Y-%m-%d %H:%M', '24', false, 'end_time_btn');" value="选择" class="button"/>  
      </td>
    </tr>
    <tr>
      <td align="right">订单状态：</td>
      <td colspan="3">
        <select name="order_status" id="select9">
          <option value="-1">请选择...</option>
          <option value="0">未确认</option><option value="1">已确认</option><option value="2"><font color="red"> 取消</font></option><option value="3"><font color="red">无效</font></option><option value="4"><font color="red">退货</font></option><option value="5">已分单</option><option value="6">部分分单</option>        </select>
        付款状态：       
        <select name="pay_status" id="select11">
          <option value="-1">请选择...</option>
          <option value="0">未付款</option><option value="1">付款中</option><option value="2">已付款</option>        </select>
        发货状态：        <select name="shipping_status" id="select10">
          <option value="-1">请选择...</option>
          <option value="0">未发货</option><option value="3">配货中</option><option value="1">已发货</option><option value="2">收货确认</option><option value="4">已发货(部分商品)</option><option value="5">发货中</option>        </select></td>
    </tr>
    <tr>
      <td colspan="4"><div align="center">
        <input name="query" type="submit" class="button" id="query" value=" 搜索 " />
        <input name="reset" type="reset" class='button' value=' 重置 ' />
      </div></td>
      </tr>
  </table>
</form>
</div>
<script type="text/javascript" src="../js/transport.org.js"></script><script type="text/javascript" src="../js/region.js"></script>
<script language="JavaScript">
region.isAdmin = true;
onload = function()
{
  // 开始检查订单
  startCheckOrder();
}
</script>

<div id="footer">
共执行 6 个查询，用时 0.013001 秒，Gzip 已禁用，内存占用 5.516 MB<br />
版权所有 &copy; 2008-2030 广州网软志成信息科技有限公司，并保留所有权利。</div>
<script type="text/javascript" src="../js/utils.js"></script><!-- 新订单提示信息 -->
<div id="popMsg">
  <table cellspacing="0" cellpadding="0" width="100%" bgcolor="#cfdef4" border="0">
  <tr>
    <td style="color: #0f2c8c" width="30" height="24"></td>
    <td style="font-weight: normal; color: #1f336b; padding-top: 4px;padding-left: 4px" valign="center" width="100%"> 新订单通知</td>
    <td style="padding-top: 2px;padding-right:2px" valign="center" align="right" width="19"><span title="关闭" style="cursor: hand;cursor:pointer;color:red;font-size:12px;font-weight:bold;margin-right:4px;" onclick="Message.close()" >×</span><!-- <img title=关闭 style="cursor: hand" onclick=closediv() hspace=3 src="msgclose.jpg"> --></td>
  </tr>
  <tr>
    <td style="padding-right: 1px; padding-bottom: 1px" colspan="3" height="70">
    <div id="popMsgContent">
      <p>您有 <strong style="color:#ff0000" id="spanNewOrder">1</strong> 个新订单以及       <strong style="color:#ff0000" id="spanNewPaid">0</strong> 个新付款的订单</p>
      <p align="center" style="word-break:break-all"><a href="order.php?act=list"><span style="color:#ff0000">点击查看新订单</span></a></p>
    </div>
    </td>
  </tr>
  </table>
</div>

<!--
<embed src="images/online.wav" width="0" height="0" autostart="false" name="msgBeep" id="msgBeep" enablejavascript="true"/>
-->
<object classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000" codebase="http://active.macromedia.com/flash2/cabs/swflash.cab#version=4,0,0,0" id="msgBeep" width="1" height="1">
  <param name="movie" value="images/online.swf">
  <param name="quality" value="high">
  <embed src="images/online.swf" name="msgBeep" id="msgBeep" quality="high" width="0" height="0" type="application/x-shockwave-flash" pluginspage="http://www.macromedia.com/shockwave/download/index.cgi?p1_prod_version=shockwaveflash">
  </embed>
</object>

<script language="JavaScript">
document.onmousemove=function(e)
{
  var obj = Utils.srcElement(e);
  if (typeof(obj.onclick) == 'function' && obj.onclick.toString().indexOf('listTable.edit') != -1)
  {
    obj.title = '点击修改内容';
    obj.style.cssText = 'background: #278296;';
    obj.onmouseout = function(e)
    {
      this.style.cssText = '';
    }
  }
  else if (typeof(obj.href) != 'undefined' && obj.href.indexOf('listTable.sort') != -1)
  {
    obj.title = '点击对列表排序';
  }
}
<!--


var MyTodolist;
function showTodoList(adminid)
{
  if(!MyTodolist)
  {
    var global = $import("../js/global.js","js");
    global.onload = global.onreadystatechange= function()
    {
      if(this.readyState && this.readyState=="loading")return;
      var md5 = $import("js/md5.js","js");
      md5.onload = md5.onreadystatechange= function()
      {
        if(this.readyState && this.readyState=="loading")return;
        var todolist = $import("js/todolist.js","js");
        todolist.onload = todolist.onreadystatechange = function()
        {
          if(this.readyState && this.readyState=="loading")return;
          MyTodolist = new Todolist();
          MyTodolist.show();
        }
      }
    }
  }
  else
  {
    if(MyTodolist.visibility)
    {
      MyTodolist.hide();
    }
    else
    {
      MyTodolist.show();
    }
  }
}

if (Browser.isIE)
{
  onscroll = function()
  {
    //document.getElementById('calculator').style.top = document.body.scrollTop;
    document.getElementById('popMsg').style.top = (document.body.scrollTop + document.body.clientHeight - document.getElementById('popMsg').offsetHeight) + "px";
  }
}

if (document.getElementById("listDiv"))
{
  document.getElementById("listDiv").onmouseover = function(e)
  {
    obj = Utils.srcElement(e);

    if (obj)
    {
      if (obj.parentNode.tagName.toLowerCase() == "tr") row = obj.parentNode;
      else if (obj.parentNode.parentNode.tagName.toLowerCase() == "tr") row = obj.parentNode.parentNode;
      else return;

      for (i = 0; i < row.cells.length; i++)
      {
        if (row.cells[i].tagName != "TH") row.cells[i].style.backgroundColor = '#F4FAFB';
      }
    }

  }

  document.getElementById("listDiv").onmouseout = function(e)
  {
    obj = Utils.srcElement(e);

    if (obj)
    {
      if (obj.parentNode.tagName.toLowerCase() == "tr") row = obj.parentNode;
      else if (obj.parentNode.parentNode.tagName.toLowerCase() == "tr") row = obj.parentNode.parentNode;
      else return;

      for (i = 0; i < row.cells.length; i++)
      {
          if (row.cells[i].tagName != "TH") row.cells[i].style.backgroundColor = '';
      }
    }
  }

  document.getElementById("listDiv").onclick = function(e)
  {
    var obj = Utils.srcElement(e);

    if (obj.tagName == "INPUT" && obj.type == "checkbox")
    {
      if (!document.forms['listForm'])
      {
        return;
      }
      var nodes = document.forms['listForm'].elements;
      var checked = false;

      for (i = 0; i < nodes.length; i++)
      {
        if (nodes[i].checked)
        {
           checked = true;
           break;
         }
      }

      if(document.getElementById("btnSubmit"))
      {
        document.getElementById("btnSubmit").disabled = !checked;
      }
      for (i = 1; i <= 10; i++)
      {
        if (document.getElementById("btnSubmit" + i))
        {
          document.getElementById("btnSubmit" + i).disabled = !checked;
        }
      }
    }
  }

}

//-->
</script>
</body>
</html>

<!-- $Id: merge_order.htm 14216 2008-03-10 02:27:21Z testyang $ -->
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>管理中心 - 合并订单 </title>
<meta name="robots" content="noindex, nofollow">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" /> 
<link href="styles/general.css" rel="stylesheet" type="text/css" />
<link href="styles/main.css" rel="stylesheet" type="text/css" /> 
<script type="text/javascript" src="http://api.map.baidu.com/api?v=1.3"></script>
<script type="text/javascript" src="js/common.js"></script><script type="text/javascript" src="../js/jquery-1.6.2.min.js"></script><script type="text/javascript" src="js/jquery.json.js"></script><script type="text/javascript" src="../js/transport.js"></script>


<script language="JavaScript">
<!--
// 这里把JS用到的所有语言都赋值到这里
var process_request = "正在处理您的请求...";
var todolist_caption = "记事本";
var todolist_autosave = "自动保存";
var todolist_save = "保存";
var todolist_clear = "清除";
var todolist_confirm_save = "是否将更改保存到记事本？";
var todolist_confirm_clear = "是否清空内容？";
var remove_confirm = "删除订单将清除该订单的所有信息。您确定要这么做吗？";
var confirm_merge = "您确实要合并这两个订单吗？";
var input_price = "自定义价格";
var pls_search_user = "请搜索并选择会员";
var confirm_drop = "确认要删除该商品吗？";
var invalid_goods_number = "商品数量不正确";
var pls_search_goods = "请搜索并选择商品";
var pls_select_area = "请完整选择所在地区";
var pls_select_shipping = "请选择配送方式";
var pls_select_payment = "请选择支付方式";
var pls_select_pack = "请选择包装";
var pls_select_card = "请选择贺卡";
var pls_input_note = "请您填写备注！";
var pls_input_cancel = "请您填写取消原因！";
var pls_select_refund = "请选择退款方式！";
var pls_select_agency = "请选择办事处！";
var pls_select_other_agency = "该订单现在就属于这个办事处，请选择其他办事处！";
var loading = "加载中...";
var remove_invoice_confirm = "您确定删除该订单的发票信息吗？";
//-->
</script>
</head>
<body>
<h1>
<span class="action-span"><a href="order.php?act=list">订单列表</a></span>
<span class="action-span1"><a href="index.php?act=main">欢迎您，test（商家管理中心）</a> </span><span id="search_id" class="action-span1"> - 合并订单 </span>
<div style="clear:both"></div>
</h1>
<script type="text/javascript" src="js/validator.js"></script><div class="main-div">
<table cellspacing="1" cellpadding="3" width="100%">
  <tr>
    <td class="label"><a href="javascript:showNotice('noticeOrderSn');" title="点击此处查看提示信息"><img src="images/notice.gif" width="16" height="16" border="0" alt="点击此处查看提示信息"></a>主订单：</td>
    <td><input name="to_order_sn" type="text" id="to_order_sn">
      <select name="to_list" id="to_list" onchange="if (this.value != '') document.getElementById('to_order_sn').value = this.value;">
      <option value="">请选择...</option>
            </select>
      <span class="notice-span" style="display:block"  id="noticeOrderSn">当两个订单不一致时，合并后的订单信息（如：支付方式、配送方式、包装、贺卡、红包等）以主订单为准。</span></td>
  </tr>
  <tr>
    <td class="label">从订单：</td>
    <td><input name="from_order_sn" type="text" id="from_order_sn">
      <select name="from_list" onchange="if (this.value != '') document.getElementById('from_order_sn').value = this.value;">
      <option value="">请选择...</option>
            </select></td>
  </tr>
  <tr>
    <td colspan="2"><div align="center">
      <input name="merge" type="button" id="merge" value="合并" class="button" onclick="if (confirm(confirm_merge)) merge()" />
    </div></td>
    </tr>
</table>
</div>

<script language="JavaScript">
    /**
     * 合并
     */
    function merge()
    {
        var fromOrderSn = document.getElementById('from_order_sn').value;
        var toOrderSn = document.getElementById('to_order_sn').value;
        Ajax.call('order.php?is_ajax=1&act=ajax_merge_order','from_order_sn=o' + fromOrderSn + '&to_order_sn=o' + toOrderSn, mergeResponse, 'POST', 'JSON');
    }

    function mergeResponse(result)
    {
      if (result.message.length > 0)
      {
        alert(result.message);
      }
      if (result.error == 0)
      {
        //成功则清除用户填写信息
        document.getElementById('from_order_sn').value = '';
        document.getElementById('to_order_sn').value = '';
        location.reload();
      }
    }

    onload = function()
    {
        // 开始检查订单
        startCheckOrder();
    }
</script>

<div id="footer">
共执行 4 个查询，用时 0.014001 秒，Gzip 已禁用，内存占用 5.504 MB<br />
版权所有 &copy; 2008-2030 广州网软志成信息科技有限公司，并保留所有权利。</div>
<script type="text/javascript" src="../js/utils.js"></script><!-- 新订单提示信息 -->
<div id="popMsg">
  <table cellspacing="0" cellpadding="0" width="100%" bgcolor="#cfdef4" border="0">
  <tr>
    <td style="color: #0f2c8c" width="30" height="24"></td>
    <td style="font-weight: normal; color: #1f336b; padding-top: 4px;padding-left: 4px" valign="center" width="100%"> 新订单通知</td>
    <td style="padding-top: 2px;padding-right:2px" valign="center" align="right" width="19"><span title="关闭" style="cursor: hand;cursor:pointer;color:red;font-size:12px;font-weight:bold;margin-right:4px;" onclick="Message.close()" >×</span><!-- <img title=关闭 style="cursor: hand" onclick=closediv() hspace=3 src="msgclose.jpg"> --></td>
  </tr>
  <tr>
    <td style="padding-right: 1px; padding-bottom: 1px" colspan="3" height="70">
    <div id="popMsgContent">
      <p>您有 <strong style="color:#ff0000" id="spanNewOrder">1</strong> 个新订单以及       <strong style="color:#ff0000" id="spanNewPaid">0</strong> 个新付款的订单</p>
      <p align="center" style="word-break:break-all"><a href="order.php?act=list"><span style="color:#ff0000">点击查看新订单</span></a></p>
    </div>
    </td>
  </tr>
  </table>
</div>

<!--
<embed src="images/online.wav" width="0" height="0" autostart="false" name="msgBeep" id="msgBeep" enablejavascript="true"/>
-->
<object classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000" codebase="http://active.macromedia.com/flash2/cabs/swflash.cab#version=4,0,0,0" id="msgBeep" width="1" height="1">
  <param name="movie" value="images/online.swf">
  <param name="quality" value="high">
  <embed src="images/online.swf" name="msgBeep" id="msgBeep" quality="high" width="0" height="0" type="application/x-shockwave-flash" pluginspage="http://www.macromedia.com/shockwave/download/index.cgi?p1_prod_version=shockwaveflash">
  </embed>
</object>

<script language="JavaScript">
document.onmousemove=function(e)
{
  var obj = Utils.srcElement(e);
  if (typeof(obj.onclick) == 'function' && obj.onclick.toString().indexOf('listTable.edit') != -1)
  {
    obj.title = '点击修改内容';
    obj.style.cssText = 'background: #278296;';
    obj.onmouseout = function(e)
    {
      this.style.cssText = '';
    }
  }
  else if (typeof(obj.href) != 'undefined' && obj.href.indexOf('listTable.sort') != -1)
  {
    obj.title = '点击对列表排序';
  }
}
<!--


var MyTodolist;
function showTodoList(adminid)
{
  if(!MyTodolist)
  {
    var global = $import("../js/global.js","js");
    global.onload = global.onreadystatechange= function()
    {
      if(this.readyState && this.readyState=="loading")return;
      var md5 = $import("js/md5.js","js");
      md5.onload = md5.onreadystatechange= function()
      {
        if(this.readyState && this.readyState=="loading")return;
        var todolist = $import("js/todolist.js","js");
        todolist.onload = todolist.onreadystatechange = function()
        {
          if(this.readyState && this.readyState=="loading")return;
          MyTodolist = new Todolist();
          MyTodolist.show();
        }
      }
    }
  }
  else
  {
    if(MyTodolist.visibility)
    {
      MyTodolist.hide();
    }
    else
    {
      MyTodolist.show();
    }
  }
}

if (Browser.isIE)
{
  onscroll = function()
  {
    //document.getElementById('calculator').style.top = document.body.scrollTop;
    document.getElementById('popMsg').style.top = (document.body.scrollTop + document.body.clientHeight - document.getElementById('popMsg').offsetHeight) + "px";
  }
}

if (document.getElementById("listDiv"))
{
  document.getElementById("listDiv").onmouseover = function(e)
  {
    obj = Utils.srcElement(e);

    if (obj)
    {
      if (obj.parentNode.tagName.toLowerCase() == "tr") row = obj.parentNode;
      else if (obj.parentNode.parentNode.tagName.toLowerCase() == "tr") row = obj.parentNode.parentNode;
      else return;

      for (i = 0; i < row.cells.length; i++)
      {
        if (row.cells[i].tagName != "TH") row.cells[i].style.backgroundColor = '#F4FAFB';
      }
    }

  }

  document.getElementById("listDiv").onmouseout = function(e)
  {
    obj = Utils.srcElement(e);

    if (obj)
    {
      if (obj.parentNode.tagName.toLowerCase() == "tr") row = obj.parentNode;
      else if (obj.parentNode.parentNode.tagName.toLowerCase() == "tr") row = obj.parentNode.parentNode;
      else return;

      for (i = 0; i < row.cells.length; i++)
      {
          if (row.cells[i].tagName != "TH") row.cells[i].style.backgroundColor = '';
      }
    }
  }

  document.getElementById("listDiv").onclick = function(e)
  {
    var obj = Utils.srcElement(e);

    if (obj.tagName == "INPUT" && obj.type == "checkbox")
    {
      if (!document.forms['listForm'])
      {
        return;
      }
      var nodes = document.forms['listForm'].elements;
      var checked = false;

      for (i = 0; i < nodes.length; i++)
      {
        if (nodes[i].checked)
        {
           checked = true;
           break;
         }
      }

      if(document.getElementById("btnSubmit"))
      {
        document.getElementById("btnSubmit").disabled = !checked;
      }
      for (i = 1; i <= 10; i++)
      {
        if (document.getElementById("btnSubmit" + i))
        {
          document.getElementById("btnSubmit" + i).disabled = !checked;
        }
      }
    }
  }

}

//-->
</script>
</body>
</html>

<!-- $Id: order_templates.htm 14216 2008-03-10 02:27:21Z testyang $ -->
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>管理中心 - 编辑订单打印模板 </title>
<meta name="robots" content="noindex, nofollow">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" /> 
<link href="styles/general.css" rel="stylesheet" type="text/css" />
<link href="styles/main.css" rel="stylesheet" type="text/css" /> 
<script type="text/javascript" src="http://api.map.baidu.com/api?v=1.3"></script>
<script type="text/javascript" src="js/common.js"></script><script type="text/javascript" src="../js/jquery-1.6.2.min.js"></script><script type="text/javascript" src="js/jquery.json.js"></script><script type="text/javascript" src="../js/transport.js"></script>


<script language="JavaScript">
<!--
// 这里把JS用到的所有语言都赋值到这里
var process_request = "正在处理您的请求...";
var todolist_caption = "记事本";
var todolist_autosave = "自动保存";
var todolist_save = "保存";
var todolist_clear = "清除";
var todolist_confirm_save = "是否将更改保存到记事本？";
var todolist_confirm_clear = "是否清空内容？";
var remove_confirm = "删除订单将清除该订单的所有信息。您确定要这么做吗？";
var confirm_merge = "您确实要合并这两个订单吗？";
var input_price = "自定义价格";
var pls_search_user = "请搜索并选择会员";
var confirm_drop = "确认要删除该商品吗？";
var invalid_goods_number = "商品数量不正确";
var pls_search_goods = "请搜索并选择商品";
var pls_select_area = "请完整选择所在地区";
var pls_select_shipping = "请选择配送方式";
var pls_select_payment = "请选择支付方式";
var pls_select_pack = "请选择包装";
var pls_select_card = "请选择贺卡";
var pls_input_note = "请您填写备注！";
var pls_input_cancel = "请您填写取消原因！";
var pls_select_refund = "请选择退款方式！";
var pls_select_agency = "请选择办事处！";
var pls_select_other_agency = "该订单现在就属于这个办事处，请选择其他办事处！";
var loading = "加载中...";
var remove_invoice_confirm = "您确定删除该订单的发票信息吗？";
//-->
</script>
</head>
<body>
<h1>
<span class="action-span"><a href="order.php?act=list">订单列表</a></span>
<span class="action-span1"><a href="index.php?act=main">欢迎您，test（商家管理中心）</a> </span><span id="search_id" class="action-span1"> - 编辑订单打印模板 </span>
<div style="clear:both"></div>
</h1>
<form action="order.php" method="post">
<div class="main-div">
    <table width="100%">
     <tr><td><input type="hidden" id="FCKeditor1" name="FCKeditor1" value="&lt;p&gt;{literal} &lt;style type=&quot;text/css&quot;&gt;
body,td { font-size:13px; }
&lt;/style&gt; {/literal}&lt;/p&gt;
&lt;h1 align=&quot;center&quot;&gt;{$lang.order_info}&lt;/h1&gt;
&lt;table cellpadding=&quot;1&quot; width=&quot;100%&quot;&gt;
    &lt;tbody&gt;
        &lt;tr&gt;
            &lt;td width=&quot;8%&quot;&gt;{$lang.print_buy_name}&lt;/td&gt;
            &lt;td&gt;{if $order.user_name}{$order.user_name}{else}{$lang.anonymous}{/if}&lt;!-- 购货人姓名 --&gt;&lt;/td&gt;
            &lt;td align=&quot;right&quot;&gt;{$lang.label_order_time}&lt;/td&gt;
            &lt;td&gt;{$order.order_time}&lt;!-- 下订单时间 --&gt;&lt;/td&gt;
            &lt;td align=&quot;right&quot;&gt;{$lang.label_payment}&lt;/td&gt;
            &lt;td&gt;{$order.pay_name}&lt;!-- 支付方式 --&gt;&lt;/td&gt;
            &lt;td align=&quot;right&quot;&gt;{$lang.print_order_sn}&lt;/td&gt;
            &lt;td&gt;{$order.order_sn}&lt;!-- 订单号 --&gt;&lt;/td&gt;
        &lt;/tr&gt;
        &lt;tr&gt;
            &lt;td&gt;{$lang.label_pay_time}&lt;/td&gt;
            &lt;td&gt;{$order.pay_time}&lt;/td&gt;
            &lt;!-- 付款时间 --&gt;
            &lt;td align=&quot;right&quot;&gt;{$lang.label_shipping_time}&lt;/td&gt;
            &lt;td&gt;{$order.shipping_time}&lt;!-- 发货时间 --&gt;&lt;/td&gt;
            &lt;td align=&quot;right&quot;&gt;{$lang.label_shipping}&lt;/td&gt;
            &lt;td&gt;{$order.shipping_name}&lt;!-- 配送方式 --&gt;&lt;/td&gt;
            &lt;td align=&quot;right&quot;&gt;{$lang.label_invoice_no}&lt;/td&gt;
            &lt;td&gt;{$order.invoice_no} &lt;!-- 发货单号 --&gt;&lt;/td&gt;
        &lt;/tr&gt;
        &lt;tr&gt;
            &lt;td&gt;{$lang.label_consignee_address}&lt;/td&gt;
            &lt;td colspan=&quot;7&quot;&gt;[{$order.region}]&amp;nbsp;{$order.address}&amp;nbsp;&lt;!-- 收货人地址 --&gt;         {$lang.label_consignee}{$order.consignee}&amp;nbsp;&lt;!-- 收货人姓名 --&gt;         {if $order.zipcode}{$lang.label_zipcode}{$order.zipcode}&amp;nbsp;{/if}&lt;!-- 邮政编码 --&gt;         {if $order.tel}{$lang.label_tel}{$order.tel}&amp;nbsp; {/if}&lt;!-- 联系电话 --&gt;         {if $order.mobile}{$lang.label_mobile}{$order.mobile}{/if}&lt;!-- 手机号码 --&gt;&lt;/td&gt;
        &lt;/tr&gt;
    &lt;/tbody&gt;
&lt;/table&gt;
&lt;table border=&quot;1&quot; width=&quot;100%&quot; style=&quot;border-collapse:collapse;border-color:#000;&quot;&gt;
    &lt;tbody&gt;
        &lt;tr align=&quot;center&quot;&gt;
            &lt;td bgcolor=&quot;#cccccc&quot;&gt;{$lang.goods_name}  &lt;!-- 商品名称 --&gt;&lt;/td&gt;
            &lt;td bgcolor=&quot;#cccccc&quot;&gt;{$lang.goods_sn}    &lt;!-- 商品货号 --&gt;&lt;/td&gt;
            &lt;td bgcolor=&quot;#cccccc&quot;&gt;{$lang.goods_attr}  &lt;!-- 商品属性 --&gt;&lt;/td&gt;
            &lt;td bgcolor=&quot;#cccccc&quot;&gt;{$lang.goods_price} &lt;!-- 商品单价 --&gt;&lt;/td&gt;
            &lt;td bgcolor=&quot;#cccccc&quot;&gt;{$lang.goods_number}&lt;!-- 商品数量 --&gt;&lt;/td&gt;
            &lt;td bgcolor=&quot;#cccccc&quot;&gt;{$lang.subtotal}    &lt;!-- 价格小计 --&gt;&lt;/td&gt;
        &lt;/tr&gt;
        &lt;!-- {foreach from=$goods_list item=goods key=key} --&gt;
        &lt;tr&gt;
            &lt;td&gt;&amp;nbsp;{$goods.goods_name}&lt;!-- 商品名称 --&gt;         {if $goods.is_gift}{if $goods.goods_price gt 0}{$lang.remark_favourable}{else}{$lang.remark_gift}{/if}{/if}         {if $goods.parent_id gt 0}{$lang.remark_fittings}{/if}&lt;/td&gt;
            &lt;td&gt;&amp;nbsp;{$goods.goods_sn} &lt;!-- 商品货号 --&gt;&lt;/td&gt;
            &lt;td&gt;&lt;!-- 商品属性 --&gt;         &lt;!-- {foreach key=key from=$goods_attr[$key] item=attr} --&gt;         &lt;!-- {if $attr.name} --&gt; {$attr.name}:{$attr.value} &lt;!-- {/if} --&gt;         &lt;!-- {/foreach} --&gt;&lt;/td&gt;
            &lt;td align=&quot;right&quot;&gt;{$goods.formated_goods_price}&amp;nbsp;&lt;!-- 商品单价 --&gt;&lt;/td&gt;
            &lt;td align=&quot;right&quot;&gt;{$goods.goods_number}&amp;nbsp;&lt;!-- 商品数量 --&gt;&lt;/td&gt;
            &lt;td align=&quot;right&quot;&gt;{$goods.formated_subtotal}&amp;nbsp;&lt;!-- 商品金额小计 --&gt;&lt;/td&gt;
        &lt;/tr&gt;
        &lt;!-- {/foreach} --&gt;
        &lt;tr&gt;
            &lt;!-- 发票抬头和发票内容 --&gt;
            &lt;td colspan=&quot;4&quot;&gt;{if $order.inv_payee}         {$lang.label_inv_payee}{$order.inv_payee}&amp;nbsp;&amp;nbsp;&amp;nbsp;         {$lang.label_inv_content}{$order.inv_content}         {/if}&lt;/td&gt;
            &lt;!-- 商品总金额 --&gt;
            &lt;td align=&quot;right&quot; colspan=&quot;2&quot;&gt;{$lang.label_goods_amount}{$order.formated_goods_amount}&lt;/td&gt;
        &lt;/tr&gt;
    &lt;/tbody&gt;
&lt;/table&gt;
&lt;table border=&quot;0&quot; width=&quot;100%&quot;&gt;
    &lt;tbody&gt;
        &lt;tr align=&quot;right&quot;&gt;
            &lt;td&gt;{if $order.discount gt 0}- {$lang.label_discount}{$order.formated_discount}{/if}{if $order.pack_name and $order.pack_fee neq '0.00'}          &lt;!-- 包装名称包装费用 --&gt;         + {$lang.label_pack_fee}{$order.formated_pack_fee}         {/if}         {if $order.card_name and $order.card_fee neq '0.00'}&lt;!-- 贺卡名称以及贺卡费用 --&gt;         + {$lang.label_card_fee}{$order.formated_card_fee}         {/if}         {if $order.pay_fee neq '0.00'}&lt;!-- 支付手续费 --&gt;         + {$lang.label_pay_fee}{$order.formated_pay_fee}         {/if}         {if $order.shipping_fee neq '0.00'}&lt;!-- 配送费用 --&gt;         + {$lang.label_shipping_fee}{$order.formated_shipping_fee}         {/if}         {if $order.insure_fee neq '0.00'}&lt;!-- 保价费用 --&gt;         + {$lang.label_insure_fee}{$order.formated_insure_fee}         {/if}         &lt;!-- 订单总金额 --&gt;         = {$lang.label_order_amount}{$order.formated_total_fee}&lt;/td&gt;
        &lt;/tr&gt;
        &lt;tr align=&quot;right&quot;&gt;
            &lt;td&gt;&lt;!-- 如果已付了部分款项, 减去已付款金额 --&gt;         {if $order.money_paid neq '0.00'}- {$lang.label_money_paid}{$order.formated_money_paid}{/if}          &lt;!-- 如果使用了余额支付, 减去已使用的余额 --&gt;         {if $order.surplus neq '0.00'}- {$lang.label_surplus}{$order.formated_surplus}{/if}          &lt;!-- 如果使用了积分支付, 减去已使用的积分 --&gt;         {if $order.integral_money neq '0.00'}- {$lang.label_integral}{$order.formated_integral_money}{/if}          &lt;!-- 如果使用了红包支付, 减去已使用的红包 --&gt;         {if $order.bonus neq '0.00'}- {$lang.label_bonus}{$order.formated_bonus}{/if}          &lt;!-- 应付款金额 --&gt;         = {$lang.label_money_dues}{$order.formated_order_amount}&lt;/td&gt;
        &lt;/tr&gt;
    &lt;/tbody&gt;
&lt;/table&gt;
&lt;p&gt;{if $order.to_buyer}          {/if}     {if $order.invoice_note}          {/if}     {if $order.pay_note}          {/if}&lt;/p&gt;
&lt;table border=&quot;0&quot; width=&quot;100%&quot;&gt;
    &lt;tbody&gt;
        &lt;tr&gt;
            &lt;!-- 给购货人看的备注信息 --&gt;
            &lt;td&gt;{$lang.label_to_buyer}{$order.to_buyer}&lt;/td&gt;
        &lt;/tr&gt;
        &lt;tr&gt;
            &lt;!-- 发货备注 --&gt;
            &lt;td&gt;{$lang.label_invoice_note} {$order.invoice_note}&lt;/td&gt;
        &lt;/tr&gt;
        &lt;tr&gt;
            &lt;!-- 支付备注 --&gt;
            &lt;td&gt;{$lang.pay_note} {$order.pay_note}&lt;/td&gt;
        &lt;/tr&gt;
        &lt;tr&gt;
            &lt;!-- 网店名称, 网店地址, 网店URL以及联系电话 --&gt;
            &lt;td&gt;{$shop_name}（{$shop_url}）         {$lang.label_shop_address}{$shop_address}&amp;nbsp;&amp;nbsp;{$lang.label_service_phone}{$service_phone}&lt;/td&gt;
        &lt;/tr&gt;
        &lt;tr align=&quot;right&quot;&gt;
            &lt;!-- 订单操作员以及订单打印的日期 --&gt;
            &lt;td&gt;{$lang.label_print_time}{$print_time}&amp;nbsp;&amp;nbsp;&amp;nbsp;{$lang.action_user}{$action_user}&lt;/td&gt;
        &lt;/tr&gt;
    &lt;/tbody&gt;
&lt;/table&gt;" style="display:none" /><input type="hidden" id="FCKeditor1___Config" value="" style="display:none" /><iframe id="FCKeditor1___Frame" src="../includes/fckeditor/editor/fckeditor.html?InstanceName=FCKeditor1&amp;Toolbar=Normal" width="95%" height="500" frameborder="0" scrolling="no"></iframe></td></tr>
    </table>
    <div class="button-div ">
    <input type="hidden" name="act" value="edit_templates" />
    <input type="submit" value=" 确定 " class="button" />
  </div>
</div>
</form>
<script type="Text/Javascript" language="JavaScript">
<!--

onload = function()
{
  // 开始检查订单
  startCheckOrder();
}

//-->
</script>
<div id="footer">
共执行 3 个查询，用时 0.103006 秒，Gzip 已禁用，内存占用 5.555 MB<br />
版权所有 &copy; 2008-2030 广州网软志成信息科技有限公司，并保留所有权利。</div>
<script type="text/javascript" src="../js/utils.js"></script><!-- 新订单提示信息 -->
<div id="popMsg">
  <table cellspacing="0" cellpadding="0" width="100%" bgcolor="#cfdef4" border="0">
  <tr>
    <td style="color: #0f2c8c" width="30" height="24"></td>
    <td style="font-weight: normal; color: #1f336b; padding-top: 4px;padding-left: 4px" valign="center" width="100%"> 新订单通知</td>
    <td style="padding-top: 2px;padding-right:2px" valign="center" align="right" width="19"><span title="关闭" style="cursor: hand;cursor:pointer;color:red;font-size:12px;font-weight:bold;margin-right:4px;" onclick="Message.close()" >×</span><!-- <img title=关闭 style="cursor: hand" onclick=closediv() hspace=3 src="msgclose.jpg"> --></td>
  </tr>
  <tr>
    <td style="padding-right: 1px; padding-bottom: 1px" colspan="3" height="70">
    <div id="popMsgContent">
      <p>您有 <strong style="color:#ff0000" id="spanNewOrder">1</strong> 个新订单以及       <strong style="color:#ff0000" id="spanNewPaid">0</strong> 个新付款的订单</p>
      <p align="center" style="word-break:break-all"><a href="order.php?act=list"><span style="color:#ff0000">点击查看新订单</span></a></p>
    </div>
    </td>
  </tr>
  </table>
</div>

<!--
<embed src="images/online.wav" width="0" height="0" autostart="false" name="msgBeep" id="msgBeep" enablejavascript="true"/>
-->
<object classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000" codebase="http://active.macromedia.com/flash2/cabs/swflash.cab#version=4,0,0,0" id="msgBeep" width="1" height="1">
  <param name="movie" value="images/online.swf">
  <param name="quality" value="high">
  <embed src="images/online.swf" name="msgBeep" id="msgBeep" quality="high" width="0" height="0" type="application/x-shockwave-flash" pluginspage="http://www.macromedia.com/shockwave/download/index.cgi?p1_prod_version=shockwaveflash">
  </embed>
</object>

<script language="JavaScript">
document.onmousemove=function(e)
{
  var obj = Utils.srcElement(e);
  if (typeof(obj.onclick) == 'function' && obj.onclick.toString().indexOf('listTable.edit') != -1)
  {
    obj.title = '点击修改内容';
    obj.style.cssText = 'background: #278296;';
    obj.onmouseout = function(e)
    {
      this.style.cssText = '';
    }
  }
  else if (typeof(obj.href) != 'undefined' && obj.href.indexOf('listTable.sort') != -1)
  {
    obj.title = '点击对列表排序';
  }
}
<!--


var MyTodolist;
function showTodoList(adminid)
{
  if(!MyTodolist)
  {
    var global = $import("../js/global.js","js");
    global.onload = global.onreadystatechange= function()
    {
      if(this.readyState && this.readyState=="loading")return;
      var md5 = $import("js/md5.js","js");
      md5.onload = md5.onreadystatechange= function()
      {
        if(this.readyState && this.readyState=="loading")return;
        var todolist = $import("js/todolist.js","js");
        todolist.onload = todolist.onreadystatechange = function()
        {
          if(this.readyState && this.readyState=="loading")return;
          MyTodolist = new Todolist();
          MyTodolist.show();
        }
      }
    }
  }
  else
  {
    if(MyTodolist.visibility)
    {
      MyTodolist.hide();
    }
    else
    {
      MyTodolist.show();
    }
  }
}

if (Browser.isIE)
{
  onscroll = function()
  {
    //document.getElementById('calculator').style.top = document.body.scrollTop;
    document.getElementById('popMsg').style.top = (document.body.scrollTop + document.body.clientHeight - document.getElementById('popMsg').offsetHeight) + "px";
  }
}

if (document.getElementById("listDiv"))
{
  document.getElementById("listDiv").onmouseover = function(e)
  {
    obj = Utils.srcElement(e);

    if (obj)
    {
      if (obj.parentNode.tagName.toLowerCase() == "tr") row = obj.parentNode;
      else if (obj.parentNode.parentNode.tagName.toLowerCase() == "tr") row = obj.parentNode.parentNode;
      else return;

      for (i = 0; i < row.cells.length; i++)
      {
        if (row.cells[i].tagName != "TH") row.cells[i].style.backgroundColor = '#F4FAFB';
      }
    }

  }

  document.getElementById("listDiv").onmouseout = function(e)
  {
    obj = Utils.srcElement(e);

    if (obj)
    {
      if (obj.parentNode.tagName.toLowerCase() == "tr") row = obj.parentNode;
      else if (obj.parentNode.parentNode.tagName.toLowerCase() == "tr") row = obj.parentNode.parentNode;
      else return;

      for (i = 0; i < row.cells.length; i++)
      {
          if (row.cells[i].tagName != "TH") row.cells[i].style.backgroundColor = '';
      }
    }
  }

  document.getElementById("listDiv").onclick = function(e)
  {
    var obj = Utils.srcElement(e);

    if (obj.tagName == "INPUT" && obj.type == "checkbox")
    {
      if (!document.forms['listForm'])
      {
        return;
      }
      var nodes = document.forms['listForm'].elements;
      var checked = false;

      for (i = 0; i < nodes.length; i++)
      {
        if (nodes[i].checked)
        {
           checked = true;
           break;
         }
      }

      if(document.getElementById("btnSubmit"))
      {
        document.getElementById("btnSubmit").disabled = !checked;
      }
      for (i = 1; i <= 10; i++)
      {
        if (document.getElementById("btnSubmit" + i))
        {
          document.getElementById("btnSubmit" + i).disabled = !checked;
        }
      }
    }
  }

}

//-->
</script>
</body>
</html>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>管理中心 - 发货单列表 </title>
<meta name="robots" content="noindex, nofollow">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" /> 
<link href="styles/general.css" rel="stylesheet" type="text/css" />
<link href="styles/main.css" rel="stylesheet" type="text/css" /> 
<script type="text/javascript" src="http://api.map.baidu.com/api?v=1.3"></script>
<script type="text/javascript" src="js/common.js"></script><script type="text/javascript" src="../js/jquery-1.6.2.min.js"></script><script type="text/javascript" src="js/jquery.json.js"></script><script type="text/javascript" src="../js/transport.js"></script>


<script language="JavaScript">
<!--
// 这里把JS用到的所有语言都赋值到这里
var process_request = "正在处理您的请求...";
var todolist_caption = "记事本";
var todolist_autosave = "自动保存";
var todolist_save = "保存";
var todolist_clear = "清除";
var todolist_confirm_save = "是否将更改保存到记事本？";
var todolist_confirm_clear = "是否清空内容？";
var remove_confirm = "删除订单将清除该订单的所有信息。您确定要这么做吗？";
var confirm_merge = "您确实要合并这两个订单吗？";
var input_price = "自定义价格";
var pls_search_user = "请搜索并选择会员";
var confirm_drop = "确认要删除该商品吗？";
var invalid_goods_number = "商品数量不正确";
var pls_search_goods = "请搜索并选择商品";
var pls_select_area = "请完整选择所在地区";
var pls_select_shipping = "请选择配送方式";
var pls_select_payment = "请选择支付方式";
var pls_select_pack = "请选择包装";
var pls_select_card = "请选择贺卡";
var pls_input_note = "请您填写备注！";
var pls_input_cancel = "请您填写取消原因！";
var pls_select_refund = "请选择退款方式！";
var pls_select_agency = "请选择办事处！";
var pls_select_other_agency = "该订单现在就属于这个办事处，请选择其他办事处！";
var loading = "加载中...";
var remove_invoice_confirm = "您确定删除该订单的发票信息吗？";
//-->
</script>
</head>
<body>
<h1>
<span class="action-span1"><a href="index.php?act=main">欢迎您，test（商家管理中心）</a> </span><span id="search_id" class="action-span1"> - 发货单列表 </span>
<div style="clear:both"></div>
</h1>
<script type="text/javascript" src="../js/utils.js"></script><script type="text/javascript" src="js/listtable.js"></script><!-- 订单搜索 -->
<div class="form-div">
  <form action="javascript:searchOrder()" name="searchForm">
    <img src="images/icon_search.gif" width="26" height="22" border="0" alt="SEARCH" />
    发货单流水号<input name="delivery_sn" type="text" id="delivery_sn" size="15">
    订单号<input name="order_sn" type="text" id="order_sn" size="15">
    收货人<input name="consignee" type="text" id="consignee" size="15">
    发货单状态    <select name="status" id="status">
      <option value="-1" selected="selected">请选择...</option>
      <option value="0">已发货</option><option value="1">退货</option><option value="2">正常</option>    </select>
    <input type="submit" value=" 搜索 " class="button" />
  </form>
</div>

<!-- 订单列表 -->
<form method="post" action="order.php?act=operate" name="listForm" onsubmit="return check()">
  <div class="list-div" id="listDiv">

<table cellpadding="3" cellspacing="1">
  <tr>
    <th>
      <input onclick='listTable.selectAll(this, "delivery_id")' type="checkbox"/><a href="javascript:listTable.sort('delivery_sn', 'DESC'); ">发货单流水号</a>    </th>
    <th><a href="javascript:listTable.sort('order_sn', 'DESC'); ">订单号</a></th>
    <th><a href="javascript:listTable.sort('add_time', 'DESC'); ">下单时间</a></th>
    <th><a href="javascript:listTable.sort('consignee', 'DESC'); ">收货人</a></th>
    <th><a href="javascript:listTable.sort('update_time', 'DESC'); ">发货时间</a><img src="images/sort_desc.gif"></th>
    <th>供货商</th>
    <th>发货单状态</th>
    <th>操作人</th>
    <th>操作</th>
  <tr>
    <tr>
    <td valign="top" nowrap="nowrap"><input type="checkbox" name="delivery_id[]" value="18" />20150822113821611</td>
    <td>2015082252907<br /></td>
    <td align="center" valign="top" nowrap="nowrap">2015-08-22 03:38:13</td>
    <td align="left" valign="top"><a href="mailto:11@qq.qq"> 111</a></td>
    <td align="center" valign="top" nowrap="nowrap">2015-08-22 03:38:24</td>
    <td align="center" valign="top" nowrap="nowrap"></td>
    <td align="center" valign="top" nowrap="nowrap">已发货</td>
    <td align="center" valign="top" nowrap="nowrap"></td>
    <td align="center" valign="top"  nowrap="nowrap">
     <a href="order.php?act=delivery_info&delivery_id=18">查看</a>
     <a onclick="{if(confirm('您确定要删除吗？')){return true;}return false;}" href="order.php?act=operate&remove_invoice=1&delivery_id=18">移除</a>
    </td>
  </tr>
  </table>

<!-- 分页 -->
<table id="page-table" cellspacing="0">
  <tr>
    <td align="right" nowrap="true">
          <!-- $Id: page.htm 14216 2008-03-10 02:27:21Z testyang $ -->
            <div id="turn-page">
        总计  <span id="totalRecords">1</span>
        个记录分为 <span id="totalPages">1</span>
        页当前第 <span id="pageCurrent">1</span>
        页，每页 <input type='text' size='3' id='pageSize' value="15" onkeypress="return listTable.changePageSize(event)" />
        <span id="page-link">
          <a href="javascript:listTable.gotoPageFirst()">第一页</a>
          <a href="javascript:listTable.gotoPagePrev()">上一页</a>
          <a href="javascript:listTable.gotoPageNext()">下一页</a>
          <a href="javascript:listTable.gotoPageLast()">最末页</a>
 <!--   <select id="gotoPage" onchange="listTable.gotoPage(this.value)">
            <option value='1'>1</option>          </select> -->
         跳转到 <input id="pageInput" title="按回车键跳转到指定页数" value="1" style="width:25px;"  onkeypress="if(event.keyCode==13) {listTable.gotoPage(this.value);}"   type="text">
          <input class="button" id="pagerBtn" value="GO" onclick="goToPage()" type="button">
        </span>
      </div>
<script>
function goToPage(){
	var page=$("#pageInput").val();
	listTable.gotoPage(page);
}
</script>    </td>
  </tr>
</table>

  </div>
    <div>

    <input name="remove_invoice" type="submit" id="btnSubmit3" value="移除" class="button" disabled="true" onclick="{if(confirm('您确定要删除吗？')){return true;}return false;}" />
  </div>
</form>
<script language="JavaScript">
listTable.recordCount = 1;
listTable.pageCount = 1;

listTable.filter.delivery_sn = '';
listTable.filter.order_sn = '';
listTable.filter.order_id = '0';
listTable.filter.consignee = '';
listTable.filter.status = '-1';
listTable.filter.sort_by = 'update_time';
listTable.filter.sort_order = 'DESC';
listTable.filter.page = '1';
listTable.filter.page_size = '15';
listTable.filter.record_count = '1';
listTable.filter.page_count = '1';


    onload = function()
    {
        // 开始检查订单
        startCheckOrder();

        //
        listTable.query = "delivery_query";
    }

    /**
     * 搜索订单
     */
    function searchOrder()
    {
        listTable.filter['order_sn'] = Utils.trim(document.forms['searchForm'].elements['order_sn'].value);
        listTable.filter['consignee'] = Utils.trim(document.forms['searchForm'].elements['consignee'].value);
        listTable.filter['status'] = document.forms['searchForm'].elements['status'].value;
        listTable.filter['delivery_sn'] = document.forms['searchForm'].elements['delivery_sn'].value;
        listTable.filter['page'] = 1;
        listTable.query = "delivery_query";
        listTable.loadList();
    }

    function check()
    {
      var snArray = new Array();
      var eles = document.forms['listForm'].elements;
      for (var i=0; i<eles.length; i++)
      {
        if (eles[i].tagName == 'INPUT' && eles[i].type == 'checkbox' && eles[i].checked && eles[i].value != 'on')
        {
          snArray.push(eles[i].value);
        }
      }
      if (snArray.length == 0)
      {
        return false;
      }
      else
      {
        eles['order_id'].value = snArray.toString();
        return true;
      }
    }
</script>


<div id="footer">
共执行 7 个查询，用时 0.014001 秒，Gzip 已禁用，内存占用 5.500 MB<br />
版权所有 &copy; 2008-2030 广州网软志成信息科技有限公司，并保留所有权利。</div>
<!-- 新订单提示信息 -->
<div id="popMsg">
  <table cellspacing="0" cellpadding="0" width="100%" bgcolor="#cfdef4" border="0">
  <tr>
    <td style="color: #0f2c8c" width="30" height="24"></td>
    <td style="font-weight: normal; color: #1f336b; padding-top: 4px;padding-left: 4px" valign="center" width="100%"> 新订单通知</td>
    <td style="padding-top: 2px;padding-right:2px" valign="center" align="right" width="19"><span title="关闭" style="cursor: hand;cursor:pointer;color:red;font-size:12px;font-weight:bold;margin-right:4px;" onclick="Message.close()" >×</span><!-- <img title=关闭 style="cursor: hand" onclick=closediv() hspace=3 src="msgclose.jpg"> --></td>
  </tr>
  <tr>
    <td style="padding-right: 1px; padding-bottom: 1px" colspan="3" height="70">
    <div id="popMsgContent">
      <p>您有 <strong style="color:#ff0000" id="spanNewOrder">1</strong> 个新订单以及       <strong style="color:#ff0000" id="spanNewPaid">0</strong> 个新付款的订单</p>
      <p align="center" style="word-break:break-all"><a href="order.php?act=list"><span style="color:#ff0000">点击查看新订单</span></a></p>
    </div>
    </td>
  </tr>
  </table>
</div>

<!--
<embed src="images/online.wav" width="0" height="0" autostart="false" name="msgBeep" id="msgBeep" enablejavascript="true"/>
-->
<object classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000" codebase="http://active.macromedia.com/flash2/cabs/swflash.cab#version=4,0,0,0" id="msgBeep" width="1" height="1">
  <param name="movie" value="images/online.swf">
  <param name="quality" value="high">
  <embed src="images/online.swf" name="msgBeep" id="msgBeep" quality="high" width="0" height="0" type="application/x-shockwave-flash" pluginspage="http://www.macromedia.com/shockwave/download/index.cgi?p1_prod_version=shockwaveflash">
  </embed>
</object>

<script language="JavaScript">
document.onmousemove=function(e)
{
  var obj = Utils.srcElement(e);
  if (typeof(obj.onclick) == 'function' && obj.onclick.toString().indexOf('listTable.edit') != -1)
  {
    obj.title = '点击修改内容';
    obj.style.cssText = 'background: #278296;';
    obj.onmouseout = function(e)
    {
      this.style.cssText = '';
    }
  }
  else if (typeof(obj.href) != 'undefined' && obj.href.indexOf('listTable.sort') != -1)
  {
    obj.title = '点击对列表排序';
  }
}
<!--


var MyTodolist;
function showTodoList(adminid)
{
  if(!MyTodolist)
  {
    var global = $import("../js/global.js","js");
    global.onload = global.onreadystatechange= function()
    {
      if(this.readyState && this.readyState=="loading")return;
      var md5 = $import("js/md5.js","js");
      md5.onload = md5.onreadystatechange= function()
      {
        if(this.readyState && this.readyState=="loading")return;
        var todolist = $import("js/todolist.js","js");
        todolist.onload = todolist.onreadystatechange = function()
        {
          if(this.readyState && this.readyState=="loading")return;
          MyTodolist = new Todolist();
          MyTodolist.show();
        }
      }
    }
  }
  else
  {
    if(MyTodolist.visibility)
    {
      MyTodolist.hide();
    }
    else
    {
      MyTodolist.show();
    }
  }
}

if (Browser.isIE)
{
  onscroll = function()
  {
    //document.getElementById('calculator').style.top = document.body.scrollTop;
    document.getElementById('popMsg').style.top = (document.body.scrollTop + document.body.clientHeight - document.getElementById('popMsg').offsetHeight) + "px";
  }
}

if (document.getElementById("listDiv"))
{
  document.getElementById("listDiv").onmouseover = function(e)
  {
    obj = Utils.srcElement(e);

    if (obj)
    {
      if (obj.parentNode.tagName.toLowerCase() == "tr") row = obj.parentNode;
      else if (obj.parentNode.parentNode.tagName.toLowerCase() == "tr") row = obj.parentNode.parentNode;
      else return;

      for (i = 0; i < row.cells.length; i++)
      {
        if (row.cells[i].tagName != "TH") row.cells[i].style.backgroundColor = '#F4FAFB';
      }
    }

  }

  document.getElementById("listDiv").onmouseout = function(e)
  {
    obj = Utils.srcElement(e);

    if (obj)
    {
      if (obj.parentNode.tagName.toLowerCase() == "tr") row = obj.parentNode;
      else if (obj.parentNode.parentNode.tagName.toLowerCase() == "tr") row = obj.parentNode.parentNode;
      else return;

      for (i = 0; i < row.cells.length; i++)
      {
          if (row.cells[i].tagName != "TH") row.cells[i].style.backgroundColor = '';
      }
    }
  }

  document.getElementById("listDiv").onclick = function(e)
  {
    var obj = Utils.srcElement(e);

    if (obj.tagName == "INPUT" && obj.type == "checkbox")
    {
      if (!document.forms['listForm'])
      {
        return;
      }
      var nodes = document.forms['listForm'].elements;
      var checked = false;

      for (i = 0; i < nodes.length; i++)
      {
        if (nodes[i].checked)
        {
           checked = true;
           break;
         }
      }

      if(document.getElementById("btnSubmit"))
      {
        document.getElementById("btnSubmit").disabled = !checked;
      }
      for (i = 1; i <= 10; i++)
      {
        if (document.getElementById("btnSubmit" + i))
        {
          document.getElementById("btnSubmit" + i).disabled = !checked;
        }
      }
    }
  }

}

//-->
</script>
</body>
</html>
<!-- $Id: order_info.htm 15544 2009-01-09 01:54:28Z zblikai $ -->

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>管理中心 - 发货单操作：查看 </title>
<meta name="robots" content="noindex, nofollow">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" /> 
<link href="styles/general.css" rel="stylesheet" type="text/css" />
<link href="styles/main.css" rel="stylesheet" type="text/css" /> 
<script type="text/javascript" src="http://api.map.baidu.com/api?v=1.3"></script>
<script type="text/javascript" src="js/common.js"></script><script type="text/javascript" src="../js/jquery-1.6.2.min.js"></script><script type="text/javascript" src="js/jquery.json.js"></script><script type="text/javascript" src="../js/transport.js"></script>


<script language="JavaScript">
<!--
// 这里把JS用到的所有语言都赋值到这里
var process_request = "正在处理您的请求...";
var todolist_caption = "记事本";
var todolist_autosave = "自动保存";
var todolist_save = "保存";
var todolist_clear = "清除";
var todolist_confirm_save = "是否将更改保存到记事本？";
var todolist_confirm_clear = "是否清空内容？";
var remove_confirm = "删除订单将清除该订单的所有信息。您确定要这么做吗？";
var confirm_merge = "您确实要合并这两个订单吗？";
var input_price = "自定义价格";
var pls_search_user = "请搜索并选择会员";
var confirm_drop = "确认要删除该商品吗？";
var invalid_goods_number = "商品数量不正确";
var pls_search_goods = "请搜索并选择商品";
var pls_select_area = "请完整选择所在地区";
var pls_select_shipping = "请选择配送方式";
var pls_select_payment = "请选择支付方式";
var pls_select_pack = "请选择包装";
var pls_select_card = "请选择贺卡";
var pls_input_note = "请您填写备注！";
var pls_input_cancel = "请您填写取消原因！";
var pls_select_refund = "请选择退款方式！";
var pls_select_agency = "请选择办事处！";
var pls_select_other_agency = "该订单现在就属于这个办事处，请选择其他办事处！";
var loading = "加载中...";
var remove_invoice_confirm = "您确定删除该订单的发票信息吗？";
//-->
</script>
</head>
<body>
<h1>
<span class="action-span"><a href="order.php?act=delivery_list&uselastfilter=1">发货单列表</a></span>
<span class="action-span1"><a href="index.php?act=main">欢迎您，test（商家管理中心）</a> </span><span id="search_id" class="action-span1"> - 发货单操作：查看 </span>
<div style="clear:both"></div>
</h1>
<script type="text/javascript" src="js/topbar.js"></script><script type="text/javascript" src="../js/utils.js"></script><script type="text/javascript" src="js/listtable.js"></script><script type="text/javascript" src="js/selectzone.js"></script><script type="text/javascript" src="../js/common.js"></script>
<div class="list-div" style="margin-bottom: 5px">
<table width="100%" cellpadding="3" cellspacing="1">
<form action="order.php" method="post" name="theForm">
  <tr>
    <th colspan="4">基本信息</th>
  </tr>
  <tr>
    <td><div align="right"><strong>发货单流水号：</strong></div></td>
    <td>20150822113821611</td>
    <td><div align="right"><strong>发货时间：</strong></div></td>
    <td>2015-08-22 03:38:24</td>
  </tr>
  <tr>
    <td width="18%"><div align="right"><strong>订单号：</strong></div></td>
   <td width="34%">2015082252907    <td><div align="right"><strong>下单时间：</strong></div></td>
    <td>2015-08-22 03:38:13</td>
  </tr>
  <tr>
    <td><div align="right"><strong>购货人：</strong></div></td>
    <td>111</td>
    <td><div align="right"><strong>缺货处理：</strong></div></td>
    <td>等待所有商品备齐后再发</td>
  </tr>
  <tr>
    <td><div align="right"><strong>配送方式：</strong></div></td>
    <td>中通速递 </td>
    <td><div align="right"><strong>配送费用：</strong></div></td>
    <td>10.00</td>
  </tr>
  <tr>
    <td><div align="right"><strong>是否保价：</strong></div></td>
    <td>否</td>
    <td><div align="right"><strong>保价费用：</strong></div></td>
    <td>0.00</td>
  </tr>
  <tr>
    <td><div align="right"><strong>发货单号：</strong></div></td>
    <td colspan="3"><input name="invoice_no" type="text" value="请输入快递单号"  readonly="readonly" ></td>
  </tr>
  
  <tr>
    <th colspan="4">收货人信息</th>
    </tr>
  <tr>
    <td><div align="right"><strong>收货人：</strong></div></td>
    <td>111</td>
    <td><div align="right"><strong>电子邮件：</strong></div></td>
    <td>11@qq.qq</td>
  </tr>
  <tr>
    <td><div align="right"><strong>地址：</strong></div></td>
    <td>[中国  安徽  巢湖  庐江县] 111</td>
    <td><div align="right"><strong>邮编：</strong></div></td>
    <td></td>
  </tr>
  <tr>
    <td><div align="right"><strong>电话：</strong></div></td>
    <td>--</td>
    <td><div align="right"><strong>手机：</strong></div></td>
    <td>13333333333</td>
  </tr>
  <tr>
    <!--<td><div align="right"><strong>标志性建筑：</strong></div></td>
    <td></td>-->
    <td><div align="right"><strong>最佳送货时间：</strong></div></td>
    <td colspan="3">仅工作日送货</td>
  </tr>
  <tr>
    <td><div align="right"><strong>客户给商家的留言：</strong></div></td>
    <td colspan="3"></td>
  </tr>
</table>
</div>

<div class="list-div" style="margin-bottom: 5px">
<table width="100%" cellpadding="3" cellspacing="1">
  <tr>
    <th colspan="7" scope="col">商品信息</th>
    </tr>
  <tr>
    <td scope="col"><div align="center"><strong>商品名称 [ 品牌 ]</strong></div></td>
    <td scope="col"><div align="center"><strong>货号</strong></div></td>
    <td scope="col"><div align="center"><strong>货品号</strong></div></td>
    <td scope="col"><div align="center"><strong>属性</strong></div></td>
    <td scope="col"><div align="center"><strong>发货数量</strong></div></td>
  </tr>
    <tr>
    <td>
    <a href="../goods.php?id=20" target="_blank">佳沛新西兰阳光金奇异果12个/进口猕猴桃/新鲜水果     </td>
    <td><div align="left">WRZ000020</div></td>
    <td><div align="left"></div></td>
    <td><div align="left"></div></td>
    <td><div align="left">1</div></td>
  </tr>
  </table>
</div>

<div class="list-div" style="margin-bottom: 5px">
<table cellpadding="3" cellspacing="1">
  <tr>
    <th colspan="6">发货操作信息</th>
  </tr>

  <tr>
    <td><div align="right"><strong>操作者：</strong></div></td>
    <td></td>
    <td><div align="right"><strong>办事处：</strong></div></td>
    <td colspan="5"></td>
  </tr>
    <tr>
    <td><div align="right"><strong>操作备注：</strong></div></td>
  <td colspan="5"><textarea name="action_note" cols="80" rows="3"></textarea></td>
  </tr>
  <tr>
    <td><div align="right"><strong>当前可执行操作：</strong></div></td>
    <td colspan="6" align="left"><input name="unship" type="submit" value="取消发货" class="button" />        <input name="order_id" type="hidden" value="42">
        <input name="delivery_id" type="hidden" value="18">
        <input name="act" type="hidden" value="delivery_cancel_ship">
    </td>
  </tr>
  
  <tr>
    <th>操作者：</th>
    <th>操作时间</th>
    <th>订单状态</th>
    <th>付款状态</th>
    <th>发货状态</th>
    <th>备注</th>
  </tr>
    <tr>
    <td><div align="center"></div></td>
    <td><div align="center">2015-08-22 03:38:24</div></td>
    <td><div align="center">已确认</div></td>
    <td><div align="center">已付款</div></td>
    <td><div align="center">已发货</div></td>
    <td></td>
  </tr>
      </form>
</table>
</div>

<script language="JavaScript">

  var oldAgencyId = 0;

  onload = function()
  {
    // 开始检查订单
    startCheckOrder();
  }

</script>


<div id="footer">
共执行 10 个查询，用时 0.035002 秒，Gzip 已禁用，内存占用 5.513 MB<br />
版权所有 &copy; 2008-2030 广州网软志成信息科技有限公司，并保留所有权利。</div>
<!-- 新订单提示信息 -->
<div id="popMsg">
  <table cellspacing="0" cellpadding="0" width="100%" bgcolor="#cfdef4" border="0">
  <tr>
    <td style="color: #0f2c8c" width="30" height="24"></td>
    <td style="font-weight: normal; color: #1f336b; padding-top: 4px;padding-left: 4px" valign="center" width="100%"> 新订单通知</td>
    <td style="padding-top: 2px;padding-right:2px" valign="center" align="right" width="19"><span title="关闭" style="cursor: hand;cursor:pointer;color:red;font-size:12px;font-weight:bold;margin-right:4px;" onclick="Message.close()" >×</span><!-- <img title=关闭 style="cursor: hand" onclick=closediv() hspace=3 src="msgclose.jpg"> --></td>
  </tr>
  <tr>
    <td style="padding-right: 1px; padding-bottom: 1px" colspan="3" height="70">
    <div id="popMsgContent">
      <p>您有 <strong style="color:#ff0000" id="spanNewOrder">1</strong> 个新订单以及       <strong style="color:#ff0000" id="spanNewPaid">0</strong> 个新付款的订单</p>
      <p align="center" style="word-break:break-all"><a href="order.php?act=list"><span style="color:#ff0000">点击查看新订单</span></a></p>
    </div>
    </td>
  </tr>
  </table>
</div>

<!--
<embed src="images/online.wav" width="0" height="0" autostart="false" name="msgBeep" id="msgBeep" enablejavascript="true"/>
-->
<object classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000" codebase="http://active.macromedia.com/flash2/cabs/swflash.cab#version=4,0,0,0" id="msgBeep" width="1" height="1">
  <param name="movie" value="images/online.swf">
  <param name="quality" value="high">
  <embed src="images/online.swf" name="msgBeep" id="msgBeep" quality="high" width="0" height="0" type="application/x-shockwave-flash" pluginspage="http://www.macromedia.com/shockwave/download/index.cgi?p1_prod_version=shockwaveflash">
  </embed>
</object>

<script language="JavaScript">
document.onmousemove=function(e)
{
  var obj = Utils.srcElement(e);
  if (typeof(obj.onclick) == 'function' && obj.onclick.toString().indexOf('listTable.edit') != -1)
  {
    obj.title = '点击修改内容';
    obj.style.cssText = 'background: #278296;';
    obj.onmouseout = function(e)
    {
      this.style.cssText = '';
    }
  }
  else if (typeof(obj.href) != 'undefined' && obj.href.indexOf('listTable.sort') != -1)
  {
    obj.title = '点击对列表排序';
  }
}
<!--


var MyTodolist;
function showTodoList(adminid)
{
  if(!MyTodolist)
  {
    var global = $import("../js/global.js","js");
    global.onload = global.onreadystatechange= function()
    {
      if(this.readyState && this.readyState=="loading")return;
      var md5 = $import("js/md5.js","js");
      md5.onload = md5.onreadystatechange= function()
      {
        if(this.readyState && this.readyState=="loading")return;
        var todolist = $import("js/todolist.js","js");
        todolist.onload = todolist.onreadystatechange = function()
        {
          if(this.readyState && this.readyState=="loading")return;
          MyTodolist = new Todolist();
          MyTodolist.show();
        }
      }
    }
  }
  else
  {
    if(MyTodolist.visibility)
    {
      MyTodolist.hide();
    }
    else
    {
      MyTodolist.show();
    }
  }
}

if (Browser.isIE)
{
  onscroll = function()
  {
    //document.getElementById('calculator').style.top = document.body.scrollTop;
    document.getElementById('popMsg').style.top = (document.body.scrollTop + document.body.clientHeight - document.getElementById('popMsg').offsetHeight) + "px";
  }
}

if (document.getElementById("listDiv"))
{
  document.getElementById("listDiv").onmouseover = function(e)
  {
    obj = Utils.srcElement(e);

    if (obj)
    {
      if (obj.parentNode.tagName.toLowerCase() == "tr") row = obj.parentNode;
      else if (obj.parentNode.parentNode.tagName.toLowerCase() == "tr") row = obj.parentNode.parentNode;
      else return;

      for (i = 0; i < row.cells.length; i++)
      {
        if (row.cells[i].tagName != "TH") row.cells[i].style.backgroundColor = '#F4FAFB';
      }
    }

  }

  document.getElementById("listDiv").onmouseout = function(e)
  {
    obj = Utils.srcElement(e);

    if (obj)
    {
      if (obj.parentNode.tagName.toLowerCase() == "tr") row = obj.parentNode;
      else if (obj.parentNode.parentNode.tagName.toLowerCase() == "tr") row = obj.parentNode.parentNode;
      else return;

      for (i = 0; i < row.cells.length; i++)
      {
          if (row.cells[i].tagName != "TH") row.cells[i].style.backgroundColor = '';
      }
    }
  }

  document.getElementById("listDiv").onclick = function(e)
  {
    var obj = Utils.srcElement(e);

    if (obj.tagName == "INPUT" && obj.type == "checkbox")
    {
      if (!document.forms['listForm'])
      {
        return;
      }
      var nodes = document.forms['listForm'].elements;
      var checked = false;

      for (i = 0; i < nodes.length; i++)
      {
        if (nodes[i].checked)
        {
           checked = true;
           break;
         }
      }


      if(document.getElementById("btnSubmit"))
      {
        document.getElementById("btnSubmit").disabled = !checked;
      }
      for (i = 1; i <= 10; i++)
      {
        if (document.getElementById("btnSubmit" + i))
        {
          document.getElementById("btnSubmit" + i).disabled = !checked;
        }
      }
    }
  }

}

//-->
</script>
</body>
</html>
<!--增值税发票_添加_START_www.wrzc.net-->
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>管理中心 - 发票列表 </title>
<meta name="robots" content="noindex, nofollow">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" /> 
<link href="styles/general.css" rel="stylesheet" type="text/css" />
<link href="styles/main.css" rel="stylesheet" type="text/css" /> 
<script type="text/javascript" src="http://api.map.baidu.com/api?v=1.3"></script>
<script type="text/javascript" src="js/common.js"></script><script type="text/javascript" src="../js/jquery-1.6.2.min.js"></script><script type="text/javascript" src="js/jquery.json.js"></script><script type="text/javascript" src="../js/transport.js"></script>


<script language="JavaScript">
<!--
// 这里把JS用到的所有语言都赋值到这里
var process_request = "正在处理您的请求...";
var todolist_caption = "记事本";
var todolist_autosave = "自动保存";
var todolist_save = "保存";
var todolist_clear = "清除";
var todolist_confirm_save = "是否将更改保存到记事本？";
var todolist_confirm_clear = "是否清空内容？";
var remove_confirm = "删除订单将清除该订单的所有信息。您确定要这么做吗？";
var confirm_merge = "您确实要合并这两个订单吗？";
var input_price = "自定义价格";
var pls_search_user = "请搜索并选择会员";
var confirm_drop = "确认要删除该商品吗？";
var invalid_goods_number = "商品数量不正确";
var pls_search_goods = "请搜索并选择商品";
var pls_select_area = "请完整选择所在地区";
var pls_select_shipping = "请选择配送方式";
var pls_select_payment = "请选择支付方式";
var pls_select_pack = "请选择包装";
var pls_select_card = "请选择贺卡";
var pls_input_note = "请您填写备注！";
var pls_input_cancel = "请您填写取消原因！";
var pls_select_refund = "请选择退款方式！";
var pls_select_agency = "请选择办事处！";
var pls_select_other_agency = "该订单现在就属于这个办事处，请选择其他办事处！";
var loading = "加载中...";
var remove_invoice_confirm = "您确定删除该订单的发票信息吗？";
//-->
</script>
</head>
<body>
<h1>
<span class="action-span1"><a href="index.php?act=main">欢迎您，test（商家管理中心）</a> </span><span id="search_id" class="action-span1"> - 发票列表 </span>
<div style="clear:both"></div>
</h1>
<script type="text/javascript" src="../js/utils.js"></script><script type="text/javascript" src="js/listtable.js"></script><script type='text/javascript' src='../js/calendar.php' ></script>
<link href='../js/calendar/calendar.css' rel='stylesheet' type='text/css' />
<!--搜索区域-->
<div class="form-div">
<form action="javascript:search_invoice()" name="search_form">
<table>
<tr>
  <td>下单时间：</td>
  <td>
    <input name="add_time" id="add_time" type="text" size="20">
    
    <input class="button" type='button' id="add_time_btn" onclick="return showCalendar('add_time', '%Y-%m-%d %H:%M', '24', false, 'add_time_btn');" value="选择">
  </td>
  <td>发票状态：</td>
  <td>
    <select name='inv_status' style='width:123px;'>
    <option value='' selected='selected'>请选择</option>
    <option value='provided'>已开票</option>
    <option value='unprovided'>未开票</option>
    </select>
  </td>
  <td>会员名称：</td>
  <td><input name="user_name" id="user_name" type="text" size="16" maxlength="60"></td>
</tr>
<tr>
  <td>订单号：</td>
  <td><input name='order_sn' type='text' size='20'/></td>
  <td>收票人姓名：</td>
  <td><input name='vat_inv_consignee_name' type='text' size='16'/></td>
  <td>收票人手机：</td>
  <td><input name='vat_inv_consignee_phone' type='text' size='16'/></td>
  <td><input class="button" type="submit" value=" 搜索 "></td>
</tr>
</table>
</form>
</div>
<!--显示区域-->
<div class="list-div" id="listDiv">
<form method="post" action="order.php?act=invoice_op" name="listForm" onsubmit="return check()">
<input name="order_id" type="hidden" value="" />
<table cellpadding="3" cellspacing="1">
  <tr>
    <th><input onclick='listTable.selectAll(this, "checkboxes")' type="checkbox" /></th>
    <th><a href="javascript:listTable.sort('inv_type', 'DESC'); ">发票类型</a></th>
    <th><a href="javascript:listTable.sort('order_sn', 'DESC'); ">订单号</a></th>
    <th><a href="javascript:listTable.sort('add_time', 'DESC'); ">下单时间</a></th>
    <th><a href="javascript:listTable.sort('user_name', 'DESC'); ">会员名称</a></th>
    <th><a href="javascript:listTable.sort('inv_status', 'DESC'); ">发票状态</a></th>
    <th><a href="javascript:listTable.sort('inv_content', 'DESC'); ">发票内容</a></th>
    <th><a href="javascript:listTable.sort('inv_money', 'DESC'); ">发票金额</a></th>
  <th>操作</th>
  </tr>
  </table>
<!-- 分页 -->
<table id="page-table" cellspacing="0">
  <tr>
    <td align="right" nowrap="true">
          <!-- $Id: page.htm 14216 2008-03-10 02:27:21Z testyang $ -->
            <div id="turn-page">
        总计  <span id="totalRecords">0</span>
        个记录分为 <span id="totalPages">1</span>
        页当前第 <span id="pageCurrent">1</span>
        页，每页 <input type='text' size='3' id='pageSize' value="15" onkeypress="return listTable.changePageSize(event)" />
        <span id="page-link">
          <a href="javascript:listTable.gotoPageFirst()">第一页</a>
          <a href="javascript:listTable.gotoPagePrev()">上一页</a>
          <a href="javascript:listTable.gotoPageNext()">下一页</a>
          <a href="javascript:listTable.gotoPageLast()">最末页</a>
 <!--   <select id="gotoPage" onchange="listTable.gotoPage(this.value)">
            <option value='1'>1</option>          </select> -->
         跳转到 <input id="pageInput" title="按回车键跳转到指定页数" value="1" style="width:25px;"  onkeypress="if(event.keyCode==13) {listTable.gotoPage(this.value);}"   type="text">
          <input class="button" id="pagerBtn" value="GO" onclick="goToPage()" type="button">
        </span>
      </div>
<script>
function goToPage(){
	var page=$("#pageInput").val();
	listTable.gotoPage(page);
}
</script>    </td>
  </tr>
</table>
<table>
  <tr>
    <td>
        <input id='btnSubmit' class='button' type='button' disabled="true" value='开票'  onclick="provide_multi_invoice()"  />
        <input id='btnSubmit1' class='button' type='button'disabled="true" value='删除' onclick="remove_multi_invoice()" />
        <input id='btnSubmit2' class='button' name='export' type='submit' disabled="true" value='导出到Excel' onclick="this.form.target = '_blank'" />
      </td>
  </tr>
</table>
</div>
</form>
<script language="JavaScript">
  listTable.url += '&act_detail=invoice_query';
  listTable.recordCount = 0;
  listTable.pageCount = 1;

    listTable.filter.order_sn = '';
    listTable.filter.consignee = '';
    listTable.filter.email = '';
    listTable.filter.address = '';
    listTable.filter.zipcode = '';
    listTable.filter.tel = '';
    listTable.filter.mobile = '0';
    listTable.filter.country = '0';
    listTable.filter.province = '0';
    listTable.filter.city = '0';
    listTable.filter.district = '0';
    listTable.filter.shipping_id = '0';
    listTable.filter.pay_id = '0';
    listTable.filter.order_status = '-1';
    listTable.filter.shipping_status = '-1';
    listTable.filter.pay_status = '-1';
    listTable.filter.user_id = '0';
    listTable.filter.user_name = '';
    listTable.filter.composite_status = '-1';
    listTable.filter.group_buy_id = '0';
    listTable.filter.pre_sale_id = '0';
    listTable.filter.sort_by = 'add_time';
    listTable.filter.sort_order = 'DESC';
    listTable.filter.start_time = '';
    listTable.filter.end_time = '';
    listTable.filter.inv_status = '';
    listTable.filter.inv_type = '';
    listTable.filter.vat_inv_consignee_name = '';
    listTable.filter.vat_inv_consignee_phone = '';
    listTable.filter.add_time = '';
    listTable.filter.page = '1';
    listTable.filter.page_size = '15';
    listTable.filter.record_count = '0';
    listTable.filter.page_count = '1';
  
  function provide_multi_invoice()
  {
    if(check())
    {
      listTable.args = 'act=provide_invoice&order_sns='+document.forms['listForm']['order_id'].value+listTable.compileFilter();
      Ajax.call(listTable.url,listTable.args,listTable.listCallback,'GET','JSON');
    }
  }
  function remove_multi_invoice()
  {
    if(check())
    {
      listTable.remove(document.forms['listForm']['order_id'].value, remove_invoice_confirm, 'remove_invoice');
    }
  }
  function export_all_invoice()
  {
    window.open('order.php?act=export_all_invoice');
  }
  function search_invoice()
  {
      listTable.filter['add_time'] = Utils.trim(document.forms['search_form'].elements['add_time'].value);
      listTable.filter['inv_status'] = Utils.trim(document.forms['search_form'].elements['inv_status'].value);
      listTable.filter['user_name'] = Utils.trim(document.forms['search_form'].elements['user_name'].value);
      listTable.filter['order_sn'] = Utils.trim(document.forms['search_form'].elements['order_sn'].value);
      listTable.filter['vat_inv_consignee_name'] = Utils.trim(document.forms['search_form'].elements['vat_inv_consignee_name'].value);
      listTable.filter['vat_inv_consignee_phone'] = Utils.trim(document.forms['search_form'].elements['vat_inv_consignee_phone'].value);
	  listTable.filter['page'] = 1;
      listTable.loadList();
  }

  function check()
  {
    var snArray = new Array();
    var eles = document.forms['listForm'].elements;
    for (var i=0; i<eles.length; i++)
    {
      if (eles[i].tagName == 'INPUT' && eles[i].type == 'checkbox' && eles[i].checked && eles[i].value != 'on')
      {
        snArray.push(eles[i].value);
      }
    }
    if (snArray.length == 0)
    {
      return false;
    }
    else
    {
      eles['order_id'].value = snArray.toString();
      return true;
    }
  }

  listTable.listCallback = function(result, txt)
  {
      if (result.error > 0)
      {
          alert(result.message);
      }
      else
      {
          try
          {
              document.getElementById('listDiv').innerHTML = result.content;
              if (typeof result.filter == "object")
              {
                  listTable.filter = result.filter;
              }
              listTable.pageCount = result.page_count;
          }
          catch(e)
          {
              alert(e.message);
          }
      }
  }
</script>
<div id="footer">
共执行 6 个查询，用时 0.015001 秒，Gzip 已禁用，内存占用 5.501 MB<br />
版权所有 &copy; 2008-2030 广州网软志成信息科技有限公司，并保留所有权利。</div>
<!-- 新订单提示信息 -->
<div id="popMsg">
  <table cellspacing="0" cellpadding="0" width="100%" bgcolor="#cfdef4" border="0">
  <tr>
    <td style="color: #0f2c8c" width="30" height="24"></td>
    <td style="font-weight: normal; color: #1f336b; padding-top: 4px;padding-left: 4px" valign="center" width="100%"> 新订单通知</td>
    <td style="padding-top: 2px;padding-right:2px" valign="center" align="right" width="19"><span title="关闭" style="cursor: hand;cursor:pointer;color:red;font-size:12px;font-weight:bold;margin-right:4px;" onclick="Message.close()" >×</span><!-- <img title=关闭 style="cursor: hand" onclick=closediv() hspace=3 src="msgclose.jpg"> --></td>
  </tr>
  <tr>
    <td style="padding-right: 1px; padding-bottom: 1px" colspan="3" height="70">
    <div id="popMsgContent">
      <p>您有 <strong style="color:#ff0000" id="spanNewOrder">1</strong> 个新订单以及       <strong style="color:#ff0000" id="spanNewPaid">0</strong> 个新付款的订单</p>
      <p align="center" style="word-break:break-all"><a href="order.php?act=list"><span style="color:#ff0000">点击查看新订单</span></a></p>
    </div>
    </td>
  </tr>
  </table>
</div>

<!--
<embed src="images/online.wav" width="0" height="0" autostart="false" name="msgBeep" id="msgBeep" enablejavascript="true"/>
-->
<object classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000" codebase="http://active.macromedia.com/flash2/cabs/swflash.cab#version=4,0,0,0" id="msgBeep" width="1" height="1">
  <param name="movie" value="images/online.swf">
  <param name="quality" value="high">
  <embed src="images/online.swf" name="msgBeep" id="msgBeep" quality="high" width="0" height="0" type="application/x-shockwave-flash" pluginspage="http://www.macromedia.com/shockwave/download/index.cgi?p1_prod_version=shockwaveflash">
  </embed>
</object>

<script language="JavaScript">
document.onmousemove=function(e)
{
  var obj = Utils.srcElement(e);
  if (typeof(obj.onclick) == 'function' && obj.onclick.toString().indexOf('listTable.edit') != -1)
  {
    obj.title = '点击修改内容';
    obj.style.cssText = 'background: #278296;';
    obj.onmouseout = function(e)
    {
      this.style.cssText = '';
    }
  }
  else if (typeof(obj.href) != 'undefined' && obj.href.indexOf('listTable.sort') != -1)
  {
    obj.title = '点击对列表排序';
  }
}
<!--


var MyTodolist;
function showTodoList(adminid)
{
  if(!MyTodolist)
  {
    var global = $import("../js/global.js","js");
    global.onload = global.onreadystatechange= function()
    {
      if(this.readyState && this.readyState=="loading")return;
      var md5 = $import("js/md5.js","js");
      md5.onload = md5.onreadystatechange= function()
      {
        if(this.readyState && this.readyState=="loading")return;
        var todolist = $import("js/todolist.js","js");
        todolist.onload = todolist.onreadystatechange = function()
        {
          if(this.readyState && this.readyState=="loading")return;
          MyTodolist = new Todolist();
          MyTodolist.show();
        }
      }
    }
  }
  else
  {
    if(MyTodolist.visibility)
    {
      MyTodolist.hide();
    }
    else
    {
      MyTodolist.show();
    }
  }
}

if (Browser.isIE)
{
  onscroll = function()
  {
    //document.getElementById('calculator').style.top = document.body.scrollTop;
    document.getElementById('popMsg').style.top = (document.body.scrollTop + document.body.clientHeight - document.getElementById('popMsg').offsetHeight) + "px";
  }
}

if (document.getElementById("listDiv"))
{
  document.getElementById("listDiv").onmouseover = function(e)
  {
    obj = Utils.srcElement(e);

    if (obj)
    {
      if (obj.parentNode.tagName.toLowerCase() == "tr") row = obj.parentNode;
      else if (obj.parentNode.parentNode.tagName.toLowerCase() == "tr") row = obj.parentNode.parentNode;
      else return;

      for (i = 0; i < row.cells.length; i++)
      {
        if (row.cells[i].tagName != "TH") row.cells[i].style.backgroundColor = '#F4FAFB';
      }
    }

  }

  document.getElementById("listDiv").onmouseout = function(e)
  {
    obj = Utils.srcElement(e);

    if (obj)
    {
      if (obj.parentNode.tagName.toLowerCase() == "tr") row = obj.parentNode;
      else if (obj.parentNode.parentNode.tagName.toLowerCase() == "tr") row = obj.parentNode.parentNode;
      else return;

      for (i = 0; i < row.cells.length; i++)
      {
          if (row.cells[i].tagName != "TH") row.cells[i].style.backgroundColor = '';
      }
    }
  }

  document.getElementById("listDiv").onclick = function(e)
  {
    var obj = Utils.srcElement(e);

    if (obj.tagName == "INPUT" && obj.type == "checkbox")
    {
      if (!document.forms['listForm'])
      {
        return;
      }
      var nodes = document.forms['listForm'].elements;
      var checked = false;

      for (i = 0; i < nodes.length; i++)
      {
        if (nodes[i].checked)
        {
           checked = true;
           break;
         }
      }

      if(document.getElementById("btnSubmit"))
      {
        document.getElementById("btnSubmit").disabled = !checked;
      }
      for (i = 1; i <= 10; i++)
      {
        if (document.getElementById("btnSubmit" + i))
        {
          document.getElementById("btnSubmit" + i).disabled = !checked;
        }
      }
    }
  }

}

//-->
</script>
<?php
include '../admin/buy.php' ;
?>
</body>
</html><!--增值税发票_添加_END_www.wrzc.net-->