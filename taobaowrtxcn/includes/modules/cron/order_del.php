<?php

/** * SHOP 定期删除未付款订单
 * ===========================================================
 * * 版权所有 2005-2030 广州网软志成信息科技有限公司，并保留所有权利。
 * 网站地址: http://www.wrzc.net；
 * ----------------------------------------------------------
 * 这不是一个自由软件！您只能在不用于商业目的的前提下对程序代码进行修改和
 * 使用；不允许对程序代码以任何形式任何目的的再发布。
 * ==========================================================
 * $Author: liubo $
 * $Id: ipdel.php 17217 2011-01-19 06:29:08Z liubo $
 */

if (!defined('IN_wrzc'))
{
    die('Hacking attempt');
}
$cron_lang_www_wygk_cn = ROOT_PATH . 'languages/' .$GLOBALS['_CFG']['lang']. '/cron/order_del_www_wygk_cn.php';
if (file_exists($cron_lang_www_wygk_cn))
{
    global $_LANG;
    include_once($cron_lang_www_wygk_cn);
}

/* 模块的基本信息 */
if (isset($set_modules) && $set_modules == TRUE)
{
    $i = isset($modules) ? count($modules) : 0;

    /* 代码 */
    $modules[$i]['code']    = basename(__FILE__, '.php');

    /* 描述对应的语言项 */
    $modules[$i]['desc']    = 'order_del_www_wygk_cn_desc';

    /* 作者 */
    $modules[$i]['author']  = 'wrzcnet';

    /* 网址 */
    $modules[$i]['website'] = 'http://www.wrzc.net';

    /* 版本号 */
    $modules[$i]['version'] = '1.0.0';

    /* 配置信息 */
    $modules[$i]['config']  = array(
        array('name' => 'order_del_www_wygk_cn_day', 'type' => 'select', 'value' => '1'),
		array('name' => 'order_del_www_wygk_cn_action', 'type' => 'select', 'value' => '2'),
    );

    return;
}

$cron['order_del_www_wygk_cn_day'] = !empty($cron['order_del_www_wygk_cn_day'])  ?  $cron['order_del_www_wygk_cn_day'] : 1 ;
$deltime = gmtime() - $cron['order_del_www_wygk_cn_day'] * 3600 * 24;

$cron['order_del_www_wygk_cn_action'] = !empty($cron['order_del_www_wygk_cn_action'])  ?  $cron['order_del_www_wygk_cn_action'] : 'invalid' ;
//echo $cron['order_del_www_wygk_cn_action'];

$sql_www_wygk_cn = "select order_id FROM " . $wrzc->table('order_info') .
           " WHERE pay_status ='0' and add_time < '$deltime'";
$res_www_wygk_cn=$db->query($sql_www_wygk_cn);

while ($row_www_wygk_cn=$db->fetchRow($res_www_wygk_cn))
{
  if ($cron['order_del_www_wygk_cn_action'] == 'cancel' || $cron['order_del_www_wygk_cn_action'] == 'invalid')
  {
	  /* 设置订单为取消 */
	  if ($cron['order_del_www_wygk_cn_action'] == 'cancel')
	  {
		    $order_cancel_www_wygk_cn = array('order_status' => OS_CANCELED, 'to_buyer' => '超过一定时间未付款，订单自动取消');
			$GLOBALS['db']->autoExecute($GLOBALS['wrzc']->table('order_info'),
											$order_cancel_www_wygk_cn, 'UPDATE', "order_id = '$row_www_wygk_cn[order_id]' ");
	  }
	  /* 设置订单未无效 */
	  elseif($cron['order_del_www_wygk_cn_action'] == 'invalid')
	  {
			$order_invalid_www_wygk_cn = array('order_status' => OS_INVALID, 'to_buyer' => ' ');
			$GLOBALS['db']->autoExecute($GLOBALS['wrzc']->table('order_info'),
											$order_invalid_www_wygk_cn, 'UPDATE', "order_id = '$row_www_wygk_cn[order_id]' ");
	  }
  }
  elseif ($cron['order_del_www_wygk_cn_action'] == 'remove')
  {
	  /* 删除订单 */
	  $db->query("DELETE FROM ".$wrzc->table('order_info'). " WHERE order_id = '$row_www_wygk_cn[order_id]' ");
	  $db->query("DELETE FROM ".$wrzc->table('order_goods'). " WHERE order_id = '$row_www_wygk_cn[order_id]' ");
	  $db->query("DELETE FROM ".$wrzc->table('order_action'). " WHERE order_id = '$row_www_wygk_cn[order_id]' ");
	  $action_array = array('delivery', 'back');
	  del_delivery_www_wygk_cn($row_www_wygk_cn['order_id'], $action_array);
  }

}


function del_delivery_www_wygk_cn($order_id, $action_array)
{
    $return_res = 0;

    if (empty($order_id) || empty($action_array))
    {
        return $return_res;
    }

    $query_delivery = 1;
    $query_back = 1;
    if (in_array('delivery', $action_array))
    {
        $sql = 'DELETE O, G
                FROM ' . $GLOBALS['wrzc']->table('delivery_order') . ' AS O, ' . $GLOBALS['wrzc']->table('delivery_goods') . ' AS G
                WHERE O.order_id = \'' . $order_id . '\'
                AND O.delivery_id = G.delivery_id';
        $query_delivery = $GLOBALS['db']->query($sql, 'SILENT');
    }
    if (in_array('back', $action_array))
    {
        $sql = 'DELETE O, G
                FROM ' . $GLOBALS['wrzc']->table('back_order') . ' AS O, ' . $GLOBALS['wrzc']->table('back_goods') . ' AS G
                WHERE O.order_id = \'' . $order_id . '\'
                AND O.back_id = G.back_id';
        $query_back = $GLOBALS['db']->query($sql, 'SILENT');
    }

    if ($query_delivery && $query_back)
    {
        $return_res = 1;
    }

    return $return_res;
}
?>