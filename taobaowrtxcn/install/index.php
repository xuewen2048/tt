<?php
error_reporting(E_ERROR | E_WARNING | E_PARSE);
set_magic_quotes_runtime(0);


//相关函数

function remove_remarks($sql)
    {
        $i = 0;

        while ($i < strlen($sql)) {

            if ($sql[$i] == '#' && ($i == 0 || $sql[$i-1] == "\n")) {
                $j = 1;
                while ($sql[$i+$j] != "\n") {
                    $j++;
                    if ($j+$i > strlen($sql)) {
                        break;
                    }
                } 
                $sql = substr($sql, 0, $i) . substr($sql, $i+$j);
            } 
            $i++;
        } 

        return $sql;
} 


function split_sql_file($sql, $delimiter)
    {
        $sql               = trim($sql);
        $char              = '';
        $last_char         = '';
        $ret               = array();
        $string_start      = '';
        $in_string         = FALSE;
        $escaped_backslash = FALSE;

        for ($i = 0; $i < strlen($sql); ++$i) {
            $char = $sql[$i];

            // if delimiter found, add the parsed part to the returned array
            if ($char == $delimiter && !$in_string) {
                $ret[]     = substr($sql, 0, $i);
                $sql       = substr($sql, $i + 1);
                $i         = 0;
                $last_char = '';
            }

            if ($in_string) {
                // We are in a string, first check for escaped backslashes
                if ($char == '\\') {
                    if ($last_char != '\\') {
                        $escaped_backslash = FALSE;
                    } else {
                        $escaped_backslash = !$escaped_backslash;
                    }
                }
                // then check for not escaped end of strings except for
                // backquotes than cannot be escaped
                if (($char == $string_start)
                    && ($char == '`' || !(($last_char == '\\') && !$escaped_backslash))) {
                    $in_string    = FALSE;
                    $string_start = '';
                }
            } else {
                // we are not in a string, check for start of strings
                if (($char == '"') || ($char == '\'') || ($char == '`')) {
                    $in_string    = TRUE;
                    $string_start = $char;
                }
            }
            $last_char = $char;
        } // end for

        // add any rest to the returned array
        if (!empty($sql)) {
            $ret[] = $sql;
        }
        return $ret;
    }

function seld($t,$v){
	
	if($t==$v){
		return " selected ";
	}else{
		return " ";
	}

}
?>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<link id="style_sheet" href="css.css" type="text/css" rel="stylesheet">
<title>安装向导 - Powered by wygkcnsoft</title>
<script>
function CheckForm (src, step) {
	if (src.command.value == "goprev") return true;
	if (step == "1" && src.skip.value != "allow") {

		return true;

	}  else if (step == "3") {
		
		if (src.dbname.value == "") {
			alert ("请输入数据库名");
			src.dbname.focus ();
			return false;
		}
		if (src.dbuser.value == "") {
			alert ("请输入数据库用户名");
			src.dbuser.focus ();
			return false;
		}
		if (src.dbpwd.value == "") {
			alert ("请输入数据库密码");
			src.dbpwd.focus ();
			return false;
		}
	} 
	return true;
}
</script>



<style>

BODY {color:#666666;font-size:14px;line-height:150%;}  
TD,A,P {color:#666666;font-size:14px;line-height:150%;}
input {font-size: 12px;}
SELECT {font-size: 12px;}
A:link {TEXT-DECORATION: none;}
A:visited {TEXT-DECORATION: none;}
A:active {TEXT-DECORATION: none;}
A:hover {TEXT-DECORATION: underline;color:#ff6600}
td.font_13_w {
	font-size: 14px; color: #ffffff; font-weight:bold;
}

</style>
</head>
<?php
$step = 0;


if ($_POST['command'] == "gonext") {
	if ($_POST['nextstep'] == 0) {
		$donext = true;

	}
if ( $_POST['nextstep'] == 1 )
	{
				$donext = true;
			
	}
	if ($_POST['nextstep'] == 2) $donext = true;
	if ($_POST['nextstep'] == 3) {
		
		$connect =  @mysql_connect($_POST['dbhost'],$_POST['dbuser'],$_POST['dbpwd']);

		$dbhost=$_POST['dbhost'];
		$dbuser=$_POST['dbuser'];
		$dbpwd=$_POST['dbpwd'];
		$dbname=$_POST['dbname'];
		$language=$_POST['language'];

		$sLan=substr($language,0,5);
		$dbcharset=substr($language,6);

		if($dbcharset=="utf8"){
			$charset="utf-8";
		}else{
			$charset=$dbcharset;
		}
		
		

		if (!$connect) {
			$error_msg = "<font color=red style='font-size: 14px; font-weight:normal;'>数据库服务器连接失败。请检查输入信息是否正确！</font>";
			$donext    = false;
			
		} else {

				$caninstall=1;

				$mysqlvision=substr(mysql_get_server_info(),0,4);
				
				if($mysqlvision<3.23){
					$error_msg = "<font color=red style='font-size: 14px; font-weight:normal;'>Mysql数据库版本不可低于3.23</font>";
					$donext    = false;
					$caninstall=0;
					
				}

				if($mysqlvision>=4.1){
					mysql_query("SET character_set_connection=$dbcharset, character_set_results=$dbcharset, character_set_client=$dbcharset");
				}


		
				//安装数据库
				$table_num=0;
				$dbname=$_POST['dbname'];

if (mysql_get_server_info()<5.0) exit("安装失败，请使用mysql5以上版本");
	if (mysql_get_server_info() > '4.1')
	{
		mysql_query("CREATE DATABASE IF NOT EXISTS `{$dbname}` DEFAULT CHARACTER SET  `{$dbcharset}` ", $connect);
	}
	else
	{
		mysql_query("CREATE DATABASE IF NOT EXISTS `{$dbname}`", $connect);
	}
	mysql_query("CREATE DATABASE IF NOT EXISTS `{$dbname}`;",$connect);


				if($mysqlvision<4.1){
					$sql_file="./db/b2b2cwygkcn.sql";
				}else{
					$sql_file="./db/b2b2cwygkcn.sql";
				
				}



				$view_bookmark = 0;

				if (file_exists($sql_file)) {

					$sql_query = fread(fopen($sql_file, 'r'), filesize($sql_file));
					
					if (get_magic_quotes_runtime() == 1) {
						$sql_query = stripslashes($sql_query);
					}

				}else{
				
					$error_msg = "<font color=red style='font-size: 14px; font-weight:normal;'>数据库导入文件".$sql_file."不存在</font>";
					$donext    = false;
					$caninstall=0;

				}

				if($caninstall=="1"){

					$sql_query = trim($sql_query);


					if ($sql_query != '') {
						$sql_query    = remove_remarks($sql_query);
						$pieces       = split_sql_file($sql_query, ';');
						$pieces_count = count($pieces);

					
					
						if (mysql_select_db($dbname, $connect)) {
							for ($i = 0; $i < $pieces_count; $i++) {
								$a_sql_query = trim($pieces[$i]);
								if (!empty($a_sql_query) && $a_sql_query[0] != '#') {
									$result = mysql_query($a_sql_query);
									if ($result == FALSE) { 
										$error_msg = "<font color=red style='font-size: 14px; font-weight:normal;'>数据库导入失败！</font>";
										$donext    = false;
									
									}
								}
							} 
						}else{
							$error_msg = "<font color=red style='font-size: 14px; font-weight:normal;'>不可使用数据库，请检查数据库是否存在、用户是否有权访问该数据库！</font>";
							$donext    = false;
							$caninstall=0;

						}
					
				} 

				if($caninstall=="1"){
					$result=mysql_list_tables($dbname,$connect);
					
					$i = 0;
					while ($i < mysql_num_rows ($result)) {
						$tb_names[$i] = mysql_tablename ($result, $i);
						$table_name.="导入数据表 ".$tb_names[$i] . "&nbsp;&nbsp; .........OK<BR>";
						$i++;
					}

					$table_num=$i;


					//修改配置文件

					$ConFile="../data/config.php";
					$SysConfigFile="./inc.php";

					
					$siteurl="http://".$_SERVER["HTTP_HOST"]."/";

					$filestr = fread(fopen($SysConfigFile, 'r'),30000);
					$filestr=str_replace(" ","",$filestr);
					$filestr=str_replace("DefaultWaStarPath","",$filestr);
                    $filestr=str_replace("DefaultDbHost",$dbhost,$filestr);
					$filestr=str_replace("DefaultDbName",$dbname,$filestr);
					$filestr=str_replace("DefaultDbUser",$dbuser,$filestr);
					$filestr=str_replace("DefaultDbPass",$dbpwd,$filestr);	
					fwrite(fopen($ConFile,"w"),$filestr,30000);
					
$fp = @fopen('../data/install.lock', 'wb+');
		fwrite($fp, 'OK');
		fclose($fp);
					$step_content .= "<table width=\"100%\"  border=\"0\" cellspacing=\"0\" cellpadding=\"4\"><input type=\"hidden\" name=\"db_host\" value=\"" . $_POST['dbhost'] . "\"><input type=\"hidden\" name=\"db_name\" value=\"" . $_POST['dbname'] . "\"><input name=\"db_user\" type=\"hidden\" value=\"" . $_POST['dbuser'] . "\"><input name=\"db_pwd\" type=\"hidden\" value=\"" . $_POST['dbpwd'] . "\">";
					$step_content .= "<tr><td style=\"font-size: 15px; color: #003399; font-weight:bold;\">第 <strong>5</strong> 步：导入系统初始数据 <br><hr size=\"1\" width=\"98%\" align=\"left\">" . $error_msg . "</td></tr>";
					$step_content .= "<tr><td style=\"font-size: 14px; color: #999999;\">" . $table_name . "</td></tr>";
					$step_content .= "<tr><td style=\"font-size: 14px; color: #999999; line-height: 150%\">" . $table_num . " 个数据表已导入</td></tr>";
					$step_content .= "<tr><td style=\"font-size: 14px; color: #999999;\"><hr size=\"1\" width=\"98%\" align=\"left\"></td></tr>";
					$step_content .= "</table>";
				
					$donext = true;
				}
			}
		}
	}
	if ($_POST['nextstep'] == 4)  $donext = true;


	if ($_POST['nextstep'] == 5){
		
		echo "<script>window.location.href = 'admin/'</script>";
	}
	
	if($donext){
		$step = $_POST['nextstep'] + 1;
		
	}else{
		$step = $_POST['nextstep'];
		
		
	}

	
	

} else if ($_POST['command'] == "goprev"){
	
	$step = $_POST['nextstep'] - 1;
}

if ($step != 0) {
	$prev = "<input id=\"ddd\" type=\"submit\" value=\"<< 上一步\" onClick=\"command.value='goprev'\">";
	$next = "<input id=\"subm\" type=\"submit\" value=\">> 下一步\" onClick=\"command.value='gonext'\">";
}
if ($step == 0) {
	$next    = "<input id=\"subm\" type=\"submit\" value=\"接受协议\" onClick=\"command.value='gonext'\" style='border:1px outset'><br>";
	$unagree = "<input type=\"button\" value=\"不接受协议\" onClick=\"window.location.href='http://www.wrzc.net'\" style='border:1px outset'>";

	$step_caption = array (
		"0" => array ("bgcolor" => "#dddddd", "fontcolor" => "#999999", "tag" => "->"),
		"1" => array ("bgcolor" => "#244A6C", "fontcolor" => "#ffffff", "tag" => ""),
		"2" => array ("bgcolor" => "#244A6C", "fontcolor" => "#ffffff", "tag" => ""),
		"3" => array ("bgcolor" => "#244A6C", "fontcolor" => "#ffffff", "tag" => ""),
		"4" => array ("bgcolor" => "#244A6C", "fontcolor" => "#ffffff", "tag" => ""),
		"5" => array ("bgcolor" => "#244A6C", "fontcolor" => "#ffffff", "tag" => "")
	);
	$protocol_con = "名称：网软志成B2B2C多用户电商平台系统全开源，仿京东商城，仿淘宝网，仿天猫商城，仿一号店商城，仿苏宁易购商城，仿国美商城源码模板[推荐]
版本：电脑版商城+手机版商城+手机APP客户端+微信商城+微信登录+平台自营+商家入驻+三级分销+附近店铺o2o+佣金返利+店铺手机端管理+食品生鲜+店铺街+团购+预售+拍卖+礼包+积分商城+优惠活动+电商资讯+家用电器+红包

网软志成B2B2C多用户电商平台系统，是目前国内最领先，最完善的电商管理平台标准化产品，供应商入驻模式，商家独立管理中心、买家SNS模式用户中心，以及平台佣金结算、售后服务系统等更完善的诠释了电子商务在现今及未来的发展模式。全面支持iOS\ Android\WAP\微信等移动平台，系统支持虚拟产品的销售，集成O2O模式。全新的技术架构、全新的UI设计、丰富的促销体系、系统支持MySQL及Oracle数据库、商家助手桌面软件。各项新功能从根源上解决了电商企业普遍存在的推广、招商、盈利等问题。供应商店铺与平台店铺共存模式，极大的丰富了平台商品，是企业开展平台类型电商业务的最佳选择。
客服QQ:38306293    客服QQ:417586492 
客服QQ:657248708   客服QQ:290116505
客服QQ:314730679   客服QQ:394223545
客服QQ:469648611   客服QQ:454882888
e-mail:38306293@qq.com
电话总机:020-34506590,020-34700400,020-34709708
传真:020-34709708
手机号同时也是微信号:13527894748,13719472523
官方网址:
http://www.wrzc.net
地址:广东省广州市天河区国家软件产业基地8栋502
邮编:510000

	";
	$protocol_con=str_replace("\n","<br>",$protocol_con);
	$step_content .= "<table width=\"100%\"  border=\"0\" cellspacing=\"0\" cellpadding=\"5\">";
	$step_content .= "<tr><td style=\"font-size: 15px; color: #003399; font-weight:bold;\">第 <strong>1</strong> 步：软件介绍和安装协议<br><hr size=\"1\" width=\"98%\" align=\"left\"></td></tr>";
	$step_content .= "<tr><td style='font-size:14px;color:#666666;line-height:20px'>" . $protocol_con . "</td></tr>";
	$step_content .= "</table>";
} else if ($step == 1) {
	$step_caption = array (
		"0" => array ("bgcolor" => "#244A6C", "fontcolor" => "#ffffff", "tag" => ""),
		"1" => array ("bgcolor" => "#dddddd", "fontcolor" => "#999999", "tag" => "->"),
		"2" => array ("bgcolor" => "#244A6C", "fontcolor" => "#ffffff", "tag" => ""),
		"3" => array ("bgcolor" => "#244A6C", "fontcolor" => "#ffffff", "tag" => ""),
		"4" => array ("bgcolor" => "#244A6C", "fontcolor" => "#ffffff", "tag" => ""),
		"5" => array ("bgcolor" => "#244A6C", "fontcolor" => "#ffffff", "tag" => "")
			);
	$year = "<option value=''></option>"; 
	$month = "<option value=''></option>";
	$day = "<option value=''></option>";
	for ($y = 1930; $y <= 1996; $y ++) {
		
		$year .= "<option value=\"" . $y . "\"" . $y_selected . ">" . $y . "</option>"; 
	}
	for ($m = 1; $m <= 12; $m ++) {
		
		$month .= "<option value=\"" . $m . "\"" . $m_selected . ">" . $m . "</option>"; 
	}
	for ($d = 1; $d <= 31; $d ++) {
		
		$day .= "<option value=\"" . $d . "\"" . $d_selected . ">" . $d . "</option>"; 
	}
	
	if($_POST['nextstep']!=1){
		$error_msg = "<font style=\"font-size: 14px; color: #666666;\">注：以下信息用于获取软件试用授权，同时在软件支持论坛注册用户</font>";
	}
	
	$step_content .= "<table width=\"100%\"  border=\"0\" cellspacing=\"0\" cellpadding=\"3\">";
	$step_content .= "<tr><input type=\"hidden\" name=\"skip\" value=\"" . $skip_step . "\"><td colspan=\"2\" style=\"font-size: 15px; color: #003399; font-weight:bold;\">第 <strong>2</strong> 步：填写用户信息<br><hr size=\"1\" width=\"98%\" align=\"left\"></td><tr><td colspan=2 height=36>". $error_msg . "</td></tr></tr>";
	$step_content .= "<tr><td align=\"right\" style=\"font-size: 14px; color: #666666;\">用 户 名</td><td style=\"font-size: 12px;\"><input type=\"text\" name=\"username\" value='".$_POST[username]."'><select name=\"usersex\"><option value=\"1\">先生</option><option value=\"2\">女士/小姐</option></select> 支持论坛登录名 [ 必填 ] </td></tr>";
	$step_content .= "<tr><td align=\"right\" style=\"font-size: 14px; color: #666666;\">登录密码</td><td style=\"font-size: 12px;\"><input name=\"password\" type=\"password\" size=\"32\"  value='".$_POST[password]."'> 支持论坛登录密码 [ 必填 ] </td></tr>";
	$step_content .= "<tr><td align=\"right\" style=\"font-size: 14px; color: #666666;\">公司名称</td><td style=\"font-size: 12px;\"><input name=\"usercompany\" type=\"text\" size=\"32\"  value='".$_POST[usercompany]."'> 个人请填写姓名 [ 必填 ]</td></tr>";
	$step_content .= "<tr><td align=\"right\" style=\"font-size: 14px; color: #666666;\">联 系 人</td><td style=\"font-size: 12px;\"><input name=\"name\" type=\"text\" size=\"32\"  value='".$_POST[name]."'> 请填写真实姓名 [ 必填 ]</td></tr>";
	$step_content .= "<tr><td align=\"right\" style=\"font-size: 14px; color: #666666;\">电子邮箱</td><td style=\"font-size: 12px;\"><input name=\"useremail\" type=\"text\" size=\"32\"  value='".$_POST[useremail]."'> 请正确填写 [ 必填 ] </td></tr>";
	$step_content .= "<tr><td align=\"right\" style=\"font-size: 14px; color: #666666;\">联系电话</td><td style=\"font-size: 12px;\"><input name=\"usertelphone\" type=\"text\"  value='".$_POST[usertelphone]."'> 建议填写方便联系 [ 可不填 ]</td></tr>";
	$step_content .= "<tr><td align=\"right\" style=\"font-size: 14px; color: #666666;\">网站URL</td><td style=\"font-size: 12px;\"><input name=\"userwebsite\" type=\"text\" size=\"32\"  value='".$_POST[userwebsite]."' > 建议填写 [ 可不填 ]</td></tr>";
	$step_content .= "<tr><td align=\"right\" style=\"font-size: 14px; color: #666666;\">您的生日</td><td style=\"font-size: 12px;\"><select name=\"year\">" . $year . "</select> 年 <select name=\"month\">" . $month . "</select> 月 <select name=\"day\">" . $day . "</select> 日 建议填写 [ 可不填 ]</td></tr>";
	$step_content .= "<tr><td align=\"right\" style=\"font-size: 14px; color: #666666;\">QQ号码</td><td style=\"font-size: 12px;\"><input name=\"userqq\" type=\"text\"  value='".$_POST[userqq]."'> 建议填写方便联系 [ 可不填 ]</td></tr>";
	$step_content .= "<tr><td colspan=\"2\" style=\"font-size: 14px; color: #999999;\"><hr size=\"1\" width=\"98%\" align=\"left\"></td></tr>";
	$step_content .= "</table>";
} else if ($step == 2) {
	$step_caption = array (
		"0" => array ("bgcolor" => "#244A6C", "fontcolor" => "#ffffff", "tag" => ""),
		"1" => array ("bgcolor" => "#244A6C", "fontcolor" => "#ffffff", "tag" => ""),
		"2" => array ("bgcolor" => "#dddddd", "fontcolor" => "#999999", "tag" => "->"),
		"3" => array ("bgcolor" => "#244A6C", "fontcolor" => "#ffffff", "tag" => ""),
		"4" => array ("bgcolor" => "#244A6C", "fontcolor" => "#ffffff", "tag" => ""),
		"5" => array ("bgcolor" => "#244A6C", "fontcolor" => "#ffffff", "tag" => "")
	);
	

	//php版本检测
	$php_version = substr(PHP_VERSION,0,3)+0;
	if ($php_version < 4.2  ){
		$phpmsg = "<font color=red style='font-size: 14px; '>× (建议使用PHP4.2.x至4.4.x版本)</font>";
		$noinstall      = "<script>document.all['subm'].disabled = true;</script>";
	}else{
		$phpmsg = "<font color=#666666 style='font-size: 14px; '>√</font>";
	}
	


	

	//config文件权限检测
	if (!is_writable("../data/config.php")) {
		$confdir = "不可写";
		$confmsg = "<font color=red style='font-size: 14px; '>× (请设置sub/config.php文件为可读写，如666)</font>";
		$noinstall = "<script>document.all['subm'].disabled = true;</script>";

	}else{
		$confdir = "可读写";
		$confmsg = "<font color=#666666 style='font-size: 14px; '>√</font>";

	}





	$step_content .= "<table width=\"100%\"  border=\"0\" cellspacing=\"0\" cellpadding=\"3\">";
	$step_content .= "<tr><td style=\"font-size: 15px; color: #003399; font-weight:bold;\">第 <strong>3</strong> 步：检测服务器环境<br><hr size=\"1\" width=\"98%\" align=\"left\">" . $error_msg . "</td></tr>";
	$step_content .= "<tr><td style=\"font-size: 14px; color: #999999;\">操作系统：" . PHP_OS . "  ...√</td></tr>";
	$step_content .= "<tr><td style=\"font-size: 14px; color: #999999;\">服务器软件：" . $_ENV['SERVER_SOFTWARE'] . "  ...√</td></tr>";
	$step_content .= "<tr><td style=\"font-size: 14px; color: #999999;\">PHP版本：" . $php_version . "  ...".$phpmsg."</td></tr>";
	$step_content .= "<tr><td style=\"font-size: 14px; color: #999999;\">设置文件权限： ".$confdir."  ..." . $confmsg. "</td></tr>";
	$step_content .= "</table>";

} else if ($step == 3) {
	$step_caption = array (
		"0" => array ("bgcolor" => "#244A6C", "fontcolor" => "#ffffff", "tag" => ""),
		"1" => array ("bgcolor" => "#244A6C", "fontcolor" => "#ffffff", "tag" => ""),
		"2" => array ("bgcolor" => "#244A6C", "fontcolor" => "#ffffff", "tag" => ""),
		"3" => array ("bgcolor" => "#dddddd", "fontcolor" => "#999999", "tag" => "->"),
		"4" => array ("bgcolor" => "#244A6C", "fontcolor" => "#ffffff", "tag" => ""),
		"5" => array ("bgcolor" => "#244A6C", "fontcolor" => "#ffffff", "tag" => "")
	);
	

	
	
	$step_content .= "<table width=\"100%\"  border=\"0\" cellspacing=\"0\" cellpadding=\"3\">";
	$step_content .= "<tr><td colspan=\"2\" style=\"font-size: 15px; color: #003399; font-weight:bold;\">第 <strong>4</strong> 步：语言和数据库参数设置<br><hr size=\"1\" width=\"98%\" align=\"left\">" . $error_msg . "</td></tr>";
	$step_content .= "<tr><td align=\"right\" style=\"font-size: 14px; color: #000000;\">选择语言版本</td><td>
	<select name='language'>
 
  <option value='zh_cn_utf8' ".seld($language,"zh_cn_utf8").">简体中文(UTF-8)</option>
  
</select> *</td></tr>";
	$step_content .= "<tr><td align=\"right\" style=\"font-size: 14px; color: #000000;\">数据库服务器</td><td><input name=\"dbhost\" type=\"text\" value=\"localhost\" size=\"20\"> *数据库的地址，如填写空间商分配好的数据库地址，本机可不修改</td></tr>";
	$step_content .= "<tr><td align=\"right\" style=\"font-size: 14px; color: #000000;\">数据库名称</td><td><input name=\"dbname\" type=\"text\" size=\"20\" value='".$_POST[dbname]."'> *数据库名称，如填写空间商分配好的数据库名</td></tr>";
	$step_content .= "<tr><td align=\"right\" style=\"font-size: 14px; color: #000000;\">数据库用户</td><td><input name=\"dbuser\" type=\"text\" size=\"20\" value='".$_POST[dbuser]."'> *数据库的用户名，如填写空间商分配好的用户名</td></tr>";
	$step_content .= "<tr><td align=\"right\" style=\"font-size: 14px; color: #000000;\">数据库密码</td><td><input name=\"dbpwd\" type=\"text\" size=\"20\" value='".$_POST[dbpwd]."'> *数据库的密码，如填写空间商分配好的密码</td></tr>";
	$step_content .= "<tr><td colspan=\"2\" style=\"font-size: 14px; color: #999999;\"><hr size=\"1\" width=\"98%\" align=\"left\"></td></tr>";
	$step_content .= "</table>";
} else if ($step == 4) {
	$step_caption = array (
		"0" => array ("bgcolor" => "#244A6C", "fontcolor" => "#ffffff", "tag" => ""),
		"1" => array ("bgcolor" => "#244A6C", "fontcolor" => "#ffffff", "tag" => ""),
		"2" => array ("bgcolor" => "#244A6C", "fontcolor" => "#ffffff", "tag" => ""),
		"3" => array ("bgcolor" => "#244A6C", "fontcolor" => "#ffffff", "tag" => ""),
		"4" => array ("bgcolor" => "#dddddd", "fontcolor" => "#999999", "tag" => "->"),
		"5" => array ("bgcolor" => "#244A6C", "fontcolor" => "#ffffff", "tag" => "")
	);
}  else if ($step == 5) {
	$step_caption = array (
		"0" => array ("bgcolor" => "#244A6C", "fontcolor" => "#ffffff", "tag" => ""),
		"1" => array ("bgcolor" => "#244A6C", "fontcolor" => "#ffffff", "tag" => ""),
		"2" => array ("bgcolor" => "#244A6C", "fontcolor" => "#ffffff", "tag" => ""),
		"3" => array ("bgcolor" => "#244A6C", "fontcolor" => "#ffffff", "tag" => ""),
		"4" => array ("bgcolor" => "#244A6C", "fontcolor" => "#ffffff", "tag" => ""),
		"5" => array ("bgcolor" => "#244A6C", "fontcolor" => "#ffffff", "tag" => "")
	);
	$prev ="";
	$next          = "第一步：请点击&nbsp;&nbsp;<input id=\"sys\" type=\"button\" value=\"管理登录\" onClick=\"window.open('../admin','_blank')\">&nbsp;&nbsp;&nbsp;用默认的帐号，密码，输入验证码后，登录后台管理。<br><br>第二步：请点击&nbsp;&nbsp;&nbsp;<input id=\"sys\" type=\"button\" value=\"清空缓存\" onClick=\"window.open('../admin/index.php?act=clear_cache','_blank')\">&nbsp;&nbsp;&nbsp;以清空站点缓存，如打开的页面还是后台登录界面，则没有清空成功，请直接在后台的右上角，找到“清空缓存”，并点击执行清空站点缓存。（这一步非常重要！）<br><br>第三步：请点击&nbsp;&nbsp;&nbsp;<input id=\"subm\" type=\"button\" value=\"查看网站\" onClick=\"window.open('../','_blank')\">&nbsp;&nbsp;&nbsp;即可查看你安装的商城。";
	$step_content .= "<table width=\"100%\"  border=\"0\" cellspacing=\"0\" cellpadding=\"4\">";
	$step_content .= "<tr><td style=\"font-size: 15px; color: #003399; font-weight:bold;\">第 <strong>7</strong> 步：安装完成<br><hr width=\"98%\" size=\"1\" align=\"left\"></td></tr>";
	$step_content .= "<tr><td style=\"font-size: 14px; color: #666666; line-height: 130%\">系统安装完成！<br><br>请按以下提示的三步操作，最后请删除 [ install ] 目录<br><br></td></tr>";
	$step_content .= "</table>";
}
?>
<body topmargin="0" leftmargin="0" bgcolor="#396DA5">
<center>
  <table width="100%" height="100%"  border="0" cellpadding="0" cellspacing="0">
    <tr>
      <td width="229" valign="top" align="center"> 
        <table width="100%"  border="0" cellpadding="5" cellspacing="0">
          <tr> 
            <td align="center"><img src="images/installlogo.gif" width="229" height="75"></td>
          </tr>
          <tr> 
            <td height="130" valign="top" style="font-size:12px;color:#ffffff">&nbsp; 
            </td>
          </tr>
          <tr> 
            <td align="right" bgcolor="<?php echo $step_caption[0]["bgcolor"]?>" class="font_13_w" style="border-top: 1 solid #ffffff; border-bottom: 1 solid #ffffff; border-right: 3 solid #ffcc00; color: <?php echo $step_caption[0]["fontcolor"]?>"><?php echo $step_caption[0]["tag"];?>&nbsp;软件介绍和安装协议</td>
          </tr>
          <tr> 
            <td align="right" bgcolor="<?php echo $step_caption[1]["bgcolor"]?>" class="font_13_w" style="border-right: 3 solid #ffcc00; color: <?php echo $step_caption[1]["fontcolor"]?>"><?php echo $step_caption[1]["tag"];?>&nbsp;填写用户信息</td>
          </tr>
          <tr> 
            <td align="right" bgcolor="<?php echo $step_caption[2]["bgcolor"]?>" class="font_13_w" style="border-top: 1 solid #ffffff; border-right: 3 solid #ffcc00; color: <?php echo $step_caption[2]["fontcolor"]?>"><?php echo $step_caption[2]["tag"];?>&nbsp;检测服务器环境</td>
          </tr>
          <tr> 
            <td align="right" bgcolor="<?php echo $step_caption[3]["bgcolor"]?>" class="font_13_w" style="border-top: 1 solid #ffffff; border-right: 3 solid #ffcc00; color: <?php echo $step_caption[3]["fontcolor"]?>"><?php echo $step_caption[3]["tag"];?>&nbsp;设置语言和数据库参数</td>
          </tr>
          <tr> 
            <td align="right" bgcolor="<?php echo $step_caption[4]["bgcolor"]?>" class="font_13_w" style="border-top: 1 solid #ffffff; border-bottom: 1 solid #ffffff; border-right: 3 solid #ffcc00; color: <?php echo $step_caption[4]["fontcolor"]?>"><?php echo $step_caption[4]["tag"];?>&nbsp;导入系统初始数据</td>
          </tr>
                   <tr> 
            <td align="right" bgcolor="<?php echo $step_caption[5]["bgcolor"]?>" class="font_13_w" style="border-top: 1 solid #ffffff; border-bottom: 1 solid #ffffff; border-right: 3 solid #ffcc00; color: <?php echo $step_caption[5]["fontcolor"]?>"><?php echo $step_caption[5]["tag"];?>&nbsp;安装完成</td>
          </tr>
        </table>
        <br>
        <br>
        <table width="68%" border="0" cellspacing="0" cellpadding="0" align="center">
          <tr> 
            <td><a href="http://www.wrzc.net" target="_blank"><img src="images/medisoft.gif" width="229" height="75" border="0"></a></td>
          </tr>
        </table>
      </td>
      <td valign="top" bgcolor="#FFFFFF"><table width="100%"  border="0" cellpadding="0" cellspacing="0">
          <tr valign="top"> 
            <td height="90"><img src="images/top1.gif" width="764" height="73"></td>
        </tr>
      </table>
        <table width="100%"  border="0" cellspacing="0" cellpadding="0">
		<form name="form" action="" method="post" onSubmit="return CheckForm (this, '<?php echo $step;?>');">
		  <tr>
            <td style="padding-left: 10px; padding-top: 5px; padding-bottom: 5px;" align="left"><?php echo $step_content;?></td>
          </tr>
          <tr>
            <td style="padding-left: 10px;"><?php echo $prev;?>
              <?php echo $unagree . "&nbsp;" . $next . "&nbsp;" . $download;?>
              <input type="hidden" name="nextstep" value="<?php echo $step;?>">
              <input type="hidden" name="command" value="gonext">
              <input type="hidden" name="alertmsg" value="<?php echo $msg;?>">
              <input type="hidden" name="user_email" value="<?php echo $_POST['user_email'];?>">
              </td>
          </tr>
		  </form>
        </table></td>
    </tr>
  </table>
</center>
</body>
</html>
<?php
echo $noinstall;
?>