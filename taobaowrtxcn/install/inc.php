<?php
// database host
$db_host   = "DefaultDbHost";

// database name
$db_name   = "DefaultDbName";

// database username
$db_user   = "DefaultDbUser";

// database password
$db_pass   = "DefaultDbPass";

// table prefix
$prefix    = "wrzcnet_";

$timezone    = "UTC";

$cookie_path    = "/";

$cookie_domain    = "";

$session = "1440";

define('EC_CHARSET','utf-8');

if(!defined('ADMIN_PATH'))
{
define('ADMIN_PATH','admin');
}

define('AUTH_KEY', 'this is a key');

define('OLD_AUTH_KEY', '');

define('API_TIME', '2016-08-03 02:55:06');

?>
