<?php

/** * SHOP 财付通支付插件语言文件
 * ============================================================================
 * 版权所有 2005-2030 广州网软志成信息科技有限公司，并保留所有权利。
 * 网站地址: http://www.wrzc.net；
 * ============================================================================
 * $Author: liubo $
 * $Id: tenpay.php 17217 2011-01-19 06:29:08Z liubo $
 */

global $_LANG;

$_LANG['tenpay']            = '财付通';
$_LANG['tenpay_desc']       = '<b>财付通（www.tenpay.com） - 腾讯旗下在线支付平台，通过国家权威安全认证，支持各大银行网上支付，免支付手续费。</b><br /><a href="http://union.tenpay.com/mch/mch_register.shtml?sp_suggestuser=" target="_blank">立即免费申请：单笔费率1%</a><br /><a href="http://union.tenpay.com/set_meal_charge/?referrer=1442037873" target="_blank">立即购买包量套餐：折算后单笔费率0.6-1%</a>';
$_LANG['tenpay_account']    = '财付通商户号';
$_LANG['tenpay_key']        = '财付通密钥';
$_LANG['magic_string']      = '自定义签名';
$_LANG['pay_button']        = '去支付';
$_LANG['account_voucher']   = '会员帐户充值';
$_LANG['shop_order_sn']     = '商城订单号';
$_LANG['penpay_register']     = "<a href='http://union.tenpay.com/mch/mch_register.shtml?sp_suggestuser=' target=\"_blank\">立即免费申请：单笔费率1%</a>";


?>