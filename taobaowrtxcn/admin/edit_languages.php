
<!-- $Id: language_list.htm 14216 2008-03-10 02:27:21Z testyang $ -->

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>管理中心 - 语言项编辑 </title>
<meta name="robots" content="noindex, nofollow">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link href="styles/general.css" rel="stylesheet" type="text/css" />
<link href="styles/main.css" rel="stylesheet" type="text/css" />
<link href="styles/chosen/chosen.css" rel="stylesheet" type="text/css">
<script type="text/javascript" src="js/jquery-1.6.2.min.js"></script><script type="text/javascript" src="js/chosen.jquery.min.js"></script><script type="text/javascript" src="js/jquery.json.js"></script><script type="text/javascript" src="../js/transport.js"></script><script type="text/javascript" src="js/common.js"></script><script language="JavaScript">
<!--
// 这里把JS用到的所有语言都赋值到这里
var process_request = "正在处理您的请求...";
var todolist_caption = "记事本";
var todolist_autosave = "自动保存";
var todolist_save = "保存";
var todolist_clear = "清除";
var todolist_confirm_save = "是否将更改保存到记事本？";
var todolist_confirm_clear = "是否清空内容？";
var keyword_empty_error = "请输入您要编辑的语言关键字!\n通过搜索, 列出与此关键字相关的语言项列表";
//-->
</script>
</head>
<body>

<div id="menu_list" onmouseover="show_popup()" onmouseout="hide_popup()">
<ul>
<li><a href="goods.php?act=add" target="main_frame">添加新商品</a></li>
<li><a href="category.php?act=add" target="main_frame">添加商品分类</a></li>
<li><a href="order.php?act=add" target="main_frame">添加订单</a></li>
<li><a href="article.php?act=add" target="main_frame">添加新文章</a></li>
<li><a href="users.php?act=add" target="main_frame">添加会员</a></li>
</ul>
</div>
<script>
function show_popup(){
frmBody = parent.document.getElementById('frame-body');
if (frmBody.cols == "37, 12, *")
{
parent.main_frame.document.getElementById('menu_list').style.left = '195px';
}
else
{
parent.main_frame.document.getElementById('menu_list').style.left = '40px';
}
parent.main_frame.document.getElementById('menu_list').style.display = 'block';
}
function hide_popup(){

parent.main_frame.document.getElementById('menu_list').style.display = 'none';
}
</script>
<h1>
<span class="action-span1"><a href="index.php?act=main">管理中心</a> </span><span id="search_id" class="action-span1"> - 语言项编辑 </span>
<div style="clear:both"></div>
</h1>

<div class="form-div">
  <form name="searchForm" action="edit_languages.php" method="post" onSubmit="return validate();">
    <select name="lang_file"><option value="calendar" selected>calendar.php - </option><option value="common">common.php - 公共语言包</option><option value="demo">demo.php - </option><option value="shopping_flow">shopping_flow.php - 购物流程语言包</option><option value="user">user.php - 会员中心语言包</option></select>
    &nbsp;&nbsp;&nbsp;
    输入语言项关键字：<input type="text" name="keyword" size="30" />
    <input type="submit" value=" 搜索 " class="button" /> <input type="hidden" name="act" value="list" />
  </form>
</div>
<div>
<ul style="padding:0; margin: 0; list-style-type:none; color: #CC0000;">
  </ul>
</div>

<form method="post" action="edit_languages.php">
<div class="list-div" id="listDiv">
<table width="100%" cellspacing="1" cellpadding="2" id="list-table">
  <tr>
    <th>语言项名称</th>
    <th>语言项值</th>
  </tr>
   <tr>
    <td width="30%" align="left" class="first-cell">
    $_LANG['booking_goods_name']<input type="hidden" name="item_id[]" value="$_LANG['booking_goods_name']" />
    </td>
    <td width="70%">
      <input type="text" name="item_content[]" value="订购商品名" size="60" />
    </td>
  </tr>
  <tr style="display:none">
    <td width="30%" align="left" class="first-cell">&nbsp;</td>
    <td width="70%">
      <input type="hidden" name="item[]" value="$_LANG['booking_goods_name'] = '订购商品名';" size="60"/>
    </td>
  </tr>
    <tr>
    <td width="30%" align="left" class="first-cell">
    $_LANG['booking_success']<input type="hidden" name="item_id[]" value="$_LANG['booking_success']" />
    </td>
    <td width="70%">
      <input type="text" name="item_content[]" value="您的商品订购已经成功提交！" size="60" />
    </td>
  </tr>
  <tr style="display:none">
    <td width="30%" align="left" class="first-cell">&nbsp;</td>
    <td width="70%">
      <input type="hidden" name="item[]" value="$_LANG['booking_success'] = '您的商品订购已经成功提交！';" size="60"/>
    </td>
  </tr>
    <tr>
    <td width="30%" align="left" class="first-cell">
    $_LANG['booking_rec_exist']<input type="hidden" name="item_id[]" value="$_LANG['booking_rec_exist']" />
    </td>
    <td width="70%">
      <input type="text" name="item_content[]" value="此商品您已经进行过缺货登记了！" size="60" />
    </td>
  </tr>
  <tr style="display:none">
    <td width="30%" align="left" class="first-cell">&nbsp;</td>
    <td width="70%">
      <input type="hidden" name="item[]" value="$_LANG['booking_rec_exist'] = '此商品您已经进行过缺货登记了！';" size="60"/>
    </td>
  </tr>
    <tr>
    <td width="30%" align="left" class="first-cell">
    $_LANG['no_goods_id']<input type="hidden" name="item_id[]" value="$_LANG['no_goods_id']" />
    </td>
    <td width="70%">
      <input type="text" name="item_content[]" value="请指定商品ID" size="60" />
    </td>
  </tr>
  <tr style="display:none">
    <td width="30%" align="left" class="first-cell">&nbsp;</td>
    <td width="70%">
      <input type="hidden" name="item[]" value="$_LANG['no_goods_id'] = '请指定商品ID';" size="60"/>
    </td>
  </tr>
    <tr>
    <td width="30%" align="left" class="first-cell">
    $_LANG['booking_js']['booking_amount_empty']<input type="hidden" name="item_id[]" value="$_LANG['booking_js']['booking_amount_empty']" />
    </td>
    <td width="70%">
      <input type="text" name="item_content[]" value="请输入您要订购的商品数量！" size="60" />
    </td>
  </tr>
  <tr style="display:none">
    <td width="30%" align="left" class="first-cell">&nbsp;</td>
    <td width="70%">
      <input type="hidden" name="item[]" value="$_LANG['booking_js']['booking_amount_empty'] = '请输入您要订购的商品数量！';" size="60"/>
    </td>
  </tr>
    <tr>
    <td width="30%" align="left" class="first-cell">
    $_LANG['goods_price']<input type="hidden" name="item_id[]" value="$_LANG['goods_price']" />
    </td>
    <td width="70%">
      <input type="text" name="item_content[]" value="商品价格" size="60" />
    </td>
  </tr>
  <tr style="display:none">
    <td width="30%" align="left" class="first-cell">&nbsp;</td>
    <td width="70%">
      <input type="hidden" name="item[]" value="$_LANG['goods_price'] = '商品价格';" size="60"/>
    </td>
  </tr>
    <tr>
    <td width="30%" align="left" class="first-cell">
    $_LANG['shopping_money']<input type="hidden" name="item_id[]" value="$_LANG['shopping_money']" />
    </td>
    <td width="70%">
      <input type="text" name="item_content[]" value="商品总价" size="60" />
    </td>
  </tr>
  <tr style="display:none">
    <td width="30%" align="left" class="first-cell">&nbsp;</td>
    <td width="70%">
      <input type="hidden" name="item[]" value="$_LANG['shopping_money'] = '商品总价';" size="60"/>
    </td>
  </tr>
    <tr>
    <td width="30%" align="left" class="first-cell">
    $_LANG['del_attention']<input type="hidden" name="item_id[]" value="$_LANG['del_attention']" />
    </td>
    <td width="70%">
      <input type="text" name="item_content[]" value="确认取消此商品的关注么？" size="60" />
    </td>
  </tr>
  <tr style="display:none">
    <td width="30%" align="left" class="first-cell">&nbsp;</td>
    <td width="70%">
      <input type="hidden" name="item[]" value="$_LANG['del_attention'] = '确认取消此商品的关注么？';" size="60"/>
    </td>
  </tr>
    <tr>
    <td width="30%" align="left" class="first-cell">
    $_LANG['add_to_attention']<input type="hidden" name="item_id[]" value="$_LANG['add_to_attention']" />
    </td>
    <td width="70%">
      <input type="text" name="item_content[]" value="确定将此商品加入关注列表么？" size="60" />
    </td>
  </tr>
  <tr style="display:none">
    <td width="30%" align="left" class="first-cell">&nbsp;</td>
    <td width="70%">
      <input type="hidden" name="item[]" value="$_LANG['add_to_attention'] = '确定将此商品加入关注列表么？';" size="60"/>
    </td>
  </tr>
    <tr>
    <td width="30%" align="left" class="first-cell">
    $_LANG['label_need_image']<input type="hidden" name="item_id[]" value="$_LANG['label_need_image']" />
    </td>
    <td width="70%">
      <input type="text" name="item_content[]" value="是否显示商品图片：" size="60" />
    </td>
  </tr>
  <tr style="display:none">
    <td width="30%" align="left" class="first-cell">&nbsp;</td>
    <td width="70%">
      <input type="hidden" name="item[]" value="$_LANG['label_need_image'] = '是否显示商品图片：';" size="60"/>
    </td>
  </tr>
    <tr>
    <td width="30%" align="left" class="first-cell">
    $_LANG['label_goods_num']<input type="hidden" name="item_id[]" value="$_LANG['label_goods_num']" />
    </td>
    <td width="70%">
      <input type="text" name="item_content[]" value="显示商品数量：" size="60" />
    </td>
  </tr>
  <tr style="display:none">
    <td width="30%" align="left" class="first-cell">&nbsp;</td>
    <td width="70%">
      <input type="hidden" name="item[]" value="$_LANG['label_goods_num'] = '显示商品数量：';" size="60"/>
    </td>
  </tr>
    <tr>
    <td width="30%" align="left" class="first-cell">
    $_LANG['label_arrange']<input type="hidden" name="item_id[]" value="$_LANG['label_arrange']" />
    </td>
    <td width="70%">
      <input type="text" name="item_content[]" value="选择商品排列方式：" size="60" />
    </td>
  </tr>
  <tr style="display:none">
    <td width="30%" align="left" class="first-cell">&nbsp;</td>
    <td width="70%">
      <input type="hidden" name="item[]" value="$_LANG['label_arrange'] = '选择商品排列方式：';" size="60"/>
    </td>
  </tr>
    <tr>
    <td width="30%" align="left" class="first-cell">
    $_LANG['goods_num_must_be_int']<input type="hidden" name="item_id[]" value="$_LANG['goods_num_must_be_int']" />
    </td>
    <td width="70%">
      <input type="text" name="item_content[]" value="商品数量应该是整数" size="60" />
    </td>
  </tr>
  <tr style="display:none">
    <td width="30%" align="left" class="first-cell">&nbsp;</td>
    <td width="70%">
      <input type="hidden" name="item[]" value="$_LANG['goods_num_must_be_int'] = '商品数量应该是整数';" size="60"/>
    </td>
  </tr>
    <tr>
    <td width="30%" align="left" class="first-cell">
    $_LANG['goods_num_must_over_0']<input type="hidden" name="item_id[]" value="$_LANG['goods_num_must_over_0']" />
    </td>
    <td width="70%">
      <input type="text" name="item_content[]" value="商品数量应该大于0" size="60" />
    </td>
  </tr>
  <tr style="display:none">
    <td width="30%" align="left" class="first-cell">&nbsp;</td>
    <td width="70%">
      <input type="hidden" name="item[]" value="$_LANG['goods_num_must_over_0'] = '商品数量应该大于0';" size="60"/>
    </td>
  </tr>
    <tr>
    <td width="30%" align="left" class="first-cell">
    $_LANG['return_to_cart_success']<input type="hidden" name="item_id[]" value="$_LANG['return_to_cart_success']" />
    </td>
    <td width="70%">
      <input type="text" name="item_content[]" value="订单中商品已经成功加入购物车中" size="60" />
    </td>
  </tr>
  <tr style="display:none">
    <td width="30%" align="left" class="first-cell">&nbsp;</td>
    <td width="70%">
      <input type="hidden" name="item[]" value="$_LANG['return_to_cart_success'] = '订单中商品已经成功加入购物车中';" size="60"/>
    </td>
  </tr>
    <tr>
    <td colspan="2">
      <div align="center">
        <input type="hidden" name="act" value="edit" />
        <input type="hidden" name="file_path" value="../languages/zh_cn/user.php" />
        <input type="hidden" name="keyword" value="商品" />
        <input type="submit" value="确认修改" class="button" />
&nbsp;&nbsp;&nbsp;
        <input type="reset" value="还 原" class="button" />
      </div></td>
    </tr>
  <tr>
    <td colspan="2"><strong>说明：留空将不会更新语言项</strong></td>
    </tr>
  
</table>
</div>
</form>


<script type="text/javascript" language="JavaScript">
<!--

onload = function()
{
    document.forms['searchForm'].elements['keyword'].focus();
}

function validate()
{
    var frm     = document.forms['searchForm'];
    var keyword = frm.elements['keyword'].value;
    if (keyword.length == 0)
    {
        alert(keyword_empty_error);

        return false;
    }
    return true;
}
//-->
</script>

<div id="footer">
共执行 1 个查询，用时 0.013000 秒，Gzip 已禁用，内存占用 2.463 MB<br />
版权所有 &copy; 2008-2030 广州网软志成信息科技有限公司，并保留所有权利。</div>
<script type="text/javascript" src="../js/utils.js"></script><!-- 新订单提示信息 -->
<div id="popMsg">
  <table cellspacing="0" cellpadding="0" width="100%" bgcolor="#cfdef4" border="0">
  <tr>
    <td style="color: #0f2c8c" width="30" height="24"></td>
    <td style="font-weight: normal; color: #1f336b; padding-top: 4px;padding-left: 4px" valign="center" width="100%"> 新订单通知</td>
    <td style="padding-top: 2px;padding-right:2px" valign="center" align="right" width="19"><span title="关闭" style="cursor: hand;cursor:pointer;color:red;font-size:12px;font-weight:bold;margin-right:4px;" onclick="Message.close()" >×</span><!-- <img title=关闭 style="cursor: hand" onclick=closediv() hspace=3 src="msgclose.jpg"> --></td>
  </tr>
  <tr>
    <td style="padding-right: 1px; padding-bottom: 1px" colspan="3" height="70">
    <div id="popMsgContent">
      <p>您有 <strong style="color:#ff0000" id="spanNewOrder">1</strong> 个新订单以及       <strong style="color:#ff0000" id="spanNewPaid">0</strong> 个新付款的订单</p>
      <p align="center" style="word-break:break-all"><a href="order.php?act=list"><span style="color:#ff0000">点击查看新订单</span></a></p>
    </div>
    </td>
  </tr>
  </table>
</div>

<!--
<embed src="images/online.wav" width="0" height="0" autostart="false" name="msgBeep" id="msgBeep" enablejavascript="true"/>
-->
<object classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000" codebase="http://active.macromedia.com/flash2/cabs/swflash.cab#version=4,0,0,0" id="msgBeep" width="1" height="1">
  <param name="movie" value="images/online.swf">
  <param name="quality" value="high">
  <embed src="images/online.swf" name="msgBeep" id="msgBeep" quality="high" width="0" height="0" type="application/x-shockwave-flash" pluginspage="http://www.macromedia.com/shockwave/download/index.cgi?p1_prod_version=shockwaveflash">
  </embed>
</object>

<script language="JavaScript">
document.onmousemove=function(e)
{
  var obj = Utils.srcElement(e);
  if (typeof(obj.onclick) == 'function' && obj.onclick.toString().indexOf('listTable.edit') != -1)
  {
    obj.title = '点击修改内容';
    obj.style.cssText = 'background: #278296;';
    obj.onmouseout = function(e)
    {
      this.style.cssText = '';
    }
  }
  else if (typeof(obj.href) != 'undefined' && obj.href.indexOf('listTable.sort') != -1)
  {
    obj.title = '点击对列表排序';
  }
}
<!--


var MyTodolist;
function showTodoList(adminid)
{
  if(!MyTodolist)
  {
    var global = $import("../js/global.js","js");
    global.onload = global.onreadystatechange= function()
    {
      if(this.readyState && this.readyState=="loading")return;
      var md5 = $import("js/md5.js","js");
      md5.onload = md5.onreadystatechange= function()
      {
        if(this.readyState && this.readyState=="loading")return;
        var todolist = $import("js/todolist.js","js");
        todolist.onload = todolist.onreadystatechange = function()
        {
          if(this.readyState && this.readyState=="loading")return;
          MyTodolist = new Todolist();
          MyTodolist.show();
        }
      }
    }
  }
  else
  {
    if(MyTodolist.visibility)
    {
      MyTodolist.hide();
    }
    else
    {
      MyTodolist.show();
    }
  }
}

if (Browser.isIE)
{
  onscroll = function()
  {
    //document.getElementById('calculator').style.top = document.body.scrollTop;
    document.getElementById('popMsg').style.top = (document.body.scrollTop + document.body.clientHeight - document.getElementById('popMsg').offsetHeight) + "px";
  }
}

if (document.getElementById("listDiv"))
{
  document.getElementById("listDiv").onmouseover = function(e)
  {
    obj = Utils.srcElement(e);

    if (obj)
    {
      if (obj.parentNode.tagName.toLowerCase() == "tr") row = obj.parentNode;
      else if (obj.parentNode.parentNode.tagName.toLowerCase() == "tr") row = obj.parentNode.parentNode;
      else return;

      for (i = 0; i < row.cells.length; i++)
      {
        if (row.cells[i].tagName != "TH") row.cells[i].style.backgroundColor = '#F4FAFB';
      }
    }

  }

  document.getElementById("listDiv").onmouseout = function(e)
  {
    obj = Utils.srcElement(e);

    if (obj)
    {
      if (obj.parentNode.tagName.toLowerCase() == "tr") row = obj.parentNode;
      else if (obj.parentNode.parentNode.tagName.toLowerCase() == "tr") row = obj.parentNode.parentNode;
      else return;

      for (i = 0; i < row.cells.length; i++)
      {
          if (row.cells[i].tagName != "TH") row.cells[i].style.backgroundColor = '';
      }
    }
  }

  document.getElementById("listDiv").onclick = function(e)
  {
    var obj = Utils.srcElement(e);

    if (obj.tagName == "INPUT" && obj.type == "checkbox")
    {
      if (!document.forms['listForm'])
      {
        return;
      }
      var nodes = document.forms['listForm'].elements;
      var checked = false;

      for (i = 0; i < nodes.length; i++)
      {
        if (nodes[i].checked)
        {
           checked = true;
           break;
         }
      }

      if(document.getElementById("btnSubmit"))
      {
        document.getElementById("btnSubmit").disabled = !checked;
      }
      for (i = 1; i <= 10; i++)
      {
        if (document.getElementById("btnSubmit" + i))
        {
          document.getElementById("btnSubmit" + i).disabled = !checked;
        }
      }
    }
  }

}

//-->
</script><?php
include 'buy.php' ;
?>
</body>
</html>