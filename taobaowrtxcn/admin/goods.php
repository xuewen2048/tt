
<!-- $Id: goods_list.htm 17126 2010-04-23 10:30:26Z liuhui $ -->

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>管理中心 - 商品列表 </title>
<meta name="robots" content="noindex, nofollow">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link href="styles/general.css" rel="stylesheet" type="text/css" />
<link href="styles/main.css" rel="stylesheet" type="text/css" />
<link href="styles/chosen/chosen.css" rel="stylesheet" type="text/css">
<script type="text/javascript" src="js/jquery-1.6.2.min.js"></script><script type="text/javascript" src="js/chosen.jquery.min.js"></script><script type="text/javascript" src="js/jquery.json.js"></script><script type="text/javascript" src="../js/transport.js"></script><script type="text/javascript" src="js/common.js"></script><script language="JavaScript">
<!--
// 这里把JS用到的所有语言都赋值到这里
var process_request = "正在处理您的请求...";
var todolist_caption = "记事本";
var todolist_autosave = "自动保存";
var todolist_save = "保存";
var todolist_clear = "清除";
var todolist_confirm_save = "是否将更改保存到记事本？";
var todolist_confirm_clear = "是否清空内容？";
var goods_name_not_null = "商品名称不能为空。";
var goods_cat_not_null = "商品分类必须选择。";
var category_cat_not_null = "分类名称不能为空";
var brand_cat_not_null = "品牌名称不能为空";
var goods_cat_not_leaf = "您选择的商品分类不是底级分类，请选择底级分类。";
var shop_price_not_null = "本店售价不能为空。";
var shop_price_not_number = "本店售价不是数值。";
var select_please = "请选择...";
var button_add = "添加";
var button_del = "删除";
var spec_value_not_null = "规格不能为空";
var spec_price_not_number = "加价不是数字";
var market_price_not_number = "市场价格不是数字";
var goods_number_not_int = "商品库存不是整数";
var warn_number_not_int = "库存警告不是整数";
var promote_not_lt = "促销开始日期不能大于结束日期";
var promote_start_not_null = "促销开始时间不能为空";
var promote_end_not_null = "促销结束时间不能为空";
var drop_img_confirm = "您确实要删除该图片吗？";
var batch_no_on_sale = "您确实要将选定的商品下架吗？";
var batch_trash_confirm = "您确实要把选中的商品放入回收站吗？";
var go_category_page = "本页数据将丢失，确认要去商品分类页添加分类吗？";
var go_brand_page = "本页数据将丢失，确认要去商品品牌页添加品牌吗？";
var volume_num_not_null = "请输入优惠数量";
var volume_num_not_number = "优惠数量不是数字";
var volume_price_not_null = "请输入优惠价格";
var volume_price_not_number = "优惠价格不是数字";
var cancel_color = "无样式";
//-->
</script>
</head>
<body>

<div id="menu_list" onmouseover="show_popup()" onmouseout="hide_popup()">
<ul>
<li><a href="goods.php?act=add" target="main_frame">添加新商品</a></li>
<li><a href="category.php?act=add" target="main_frame">添加商品分类</a></li>
<li><a href="order.php?act=add" target="main_frame">添加订单</a></li>
<li><a href="article.php?act=add" target="main_frame">添加新文章</a></li>
<li><a href="users.php?act=add" target="main_frame">添加会员</a></li>
</ul>
</div>
<script>
function show_popup(){
frmBody = parent.document.getElementById('frame-body');
if (frmBody.cols == "37, 12, *")
{
parent.main_frame.document.getElementById('menu_list').style.left = '195px';
}
else
{
parent.main_frame.document.getElementById('menu_list').style.left = '40px';
}
parent.main_frame.document.getElementById('menu_list').style.display = 'block';
}
function hide_popup(){

parent.main_frame.document.getElementById('menu_list').style.display = 'none';
}
</script>
<h1>
<span class="action-span"><a href="goods.php?act=add">添加新商品</a></span>
<span class="action-span1"><a href="index.php?act=main">管理中心</a> </span><span id="search_id" class="action-span1"> - 商品列表 </span>
<div style="clear:both"></div>
</h1>
<script type="text/javascript" src="../js/utils.js"></script><script type="text/javascript" src="js/listtable.js"></script>
<!-- 商品搜索 -->
<!-- $Id: goods_search.htm 16790 2009-11-10 08:56:15Z wangleisvn $ -->
<link href="styles/zTree/zTreeStyle.css" rel="stylesheet" type="text/css" />
<link href="styles/chosen/chosen.css" rel="stylesheet" type="text/css">
<script type="text/javascript" src="js/jquery.ztree.all-3.5.min.js"></script><script type="text/javascript" src="js/category_selecter.js"></script><div class="form-div">
    <form action="javascript:searchGoods()" name="searchForm">
        <img src="images/icon_search.gif" width="26" height="22" border="0" alt="SEARCH" />
                <!-- 分类 -->
        <input type="text" id="cat_name" name="cat_name" nowvalue="" value="" >
        <input type="hidden" id="cat_id" name="cat_id" value="">
        <!-- 品牌 -->
        <select  class="chzn-select" name="brand_id"><option value="0">所有品牌</option><option value="54">缪诗</option><option value="68">格力</option><option value="69">老板</option><option value="70">西门子</option><option value="71">格兰仕</option><option value="72">海信</option><option value="73">伊莱克斯</option><option value="74">艾力斯特</option><option value="75">博洋家纺</option><option value="76">富安娜</option><option value="77">爱仕达</option><option value="78">罗莱</option><option value="67">美的</option><option value="66">海尔</option><option value="55">卓诗尼</option><option value="56">七匹狼</option><option value="57">佐丹奴</option><option value="58">达芙妮</option><option value="59">她他/tata</option><option value="60">曼妮芬（ManniForm）</option><option value="61">伊芙丽</option><option value="62">稻草人</option><option value="63">斯提亚</option><option value="64">袋鼠</option><option value="65">爱华仕</option><option value="79">安睡宝</option><option value="80">溢彩年华</option><option value="94">王老吉</option><option value="95">可口可乐</option><option value="96">贝古贝古</option><option value="97">皇家宝贝</option><option value="98">呵宝童车</option><option value="99">合生元</option><option value="100">美赞臣</option><option value="101">帮宝适</option><option value="102">抱抱熊</option><option value="103">巴拉巴拉</option><option value="104">青蛙王子</option><option value="93">统一</option><option value="92">加多宝</option><option value="81">慧乐家</option><option value="82">天堂伞</option><option value="83">水星家纺</option><option value="84">全有家居</option><option value="85">五粮液</option><option value="86">泸州老窖</option><option value="87">洋河</option><option value="88">郎酒</option><option value="89">锐澳</option><option value="90">雪花</option><option value="91">哈尔滨</option><option value="105">雀氏</option><option value="1">资生堂</option><option value="15">韩束</option><option value="16">卡姿兰</option><option value="17">珀莱雅</option><option value="18">兰芝</option><option value="19">碧欧泉</option><option value="20">小米</option><option value="21">摩托罗拉</option><option value="22">中兴</option><option value="23">朵唯</option><option value="24">htc</option><option value="25">华为</option><option value="14">高丝</option><option value="13">SK-ll</option><option value="2">CK</option><option value="3">Disney</option><option value="4">雅诗兰黛</option><option value="5">相宜本草</option><option value="6">Dior</option><option value="7">爱丽</option><option value="8">雅顿</option><option value="9">狮王</option><option value="10">高丝洁</option><option value="11">MISS FACE</option><option value="12">姬芮</option><option value="26">oppo</option><option value="27">金立</option><option value="42">君乐宝</option><option value="43">光明</option><option value="44">三元</option><option value="45">百草味</option><option value="46">三只松鼠</option><option value="47">口水娃</option><option value="48">楼兰密语</option><option value="49">西域美农</option><option value="50">糖糖屋</option><option value="51">享爱.</option><option value="52">猫人</option><option value="40">蒙牛</option><option value="39">海底捞</option><option value="28">LG</option><option value="29">苹果</option><option value="30">三星</option><option value="31">乐檬</option><option value="32">努比亚</option><option value="41">伊利</option><option value="34">肯德基</option><option value="35">麦当劳</option><option value="36">小肥羊</option><option value="37">小尾羊</option><option value="38">必胜客</option><option value="53">茵曼（INMAN）</option></select>
        <!-- 推荐 -->
                        <select class="chzn-select" name="intro_type"><option value="0">全部</option>
            <option value="is_best">精品</option><option value="is_new">新品</option><option value="is_hot">热销</option><option value="is_promote">特价</option><option value="all_type">全部推荐</option>        </select>
                                <!-- 上架 -->
                <select class="chzn-select" name="is_on_sale"><option value=''>全部</option><option value="1">上架</option><option value="0">下架</option></select>
                        <!-- 关键字 -->
        关键字 <input type="text" name="keyword" size="15" />
        <input type="submit" value=" 搜索 " class="button" />
                 <input type="button" value="批量导出" class="button" onclick="batch_export()" />
                 
    </form>
</div>


<script language="JavaScript">

    $().ready(function(){
       $(".chzn-select").chosen();
       $(".chzn-container").click(function(){
     	$("#menuContent_cat_id").hide();
    	})
		$('.chzn-container-single:last').css('width','60px');
    });

    function searchGoods()
    {
        
                listTable.filter['cat_id'] = document.forms['searchForm'].elements['cat_id'].value;
        listTable.filter['brand_id'] = document.forms['searchForm'].elements['brand_id'].value;
                    listTable.filter['intro_type'] = document.forms['searchForm'].elements['intro_type'].value;
                                                listTable.filter['is_on_sale'] = document.forms['searchForm'].elements['is_on_sale'].value;
                                            
                        listTable.filter['keyword'] = Utils.trim(document.forms['searchForm'].elements['keyword'].value);

                        listTable.filter['page'] = 1;

                        listTable.loadList();
                    }
</script>

<script type="text/javascript">
    $().ready(function(){
        // $("#cat_name")为获取分类名称的jQuery对象，可根据实际情况修改
        // $("#cat_id")为获取分类ID的jQuery对象，可根据实际情况修改
        // ""为被选中的商品分类编号，无则设置为null或者不写此参数或者为空字符串
        $.ajaxCategorySelecter($("#cat_name"), $("#cat_id"), "");
    });

    function searchSuppliers()
    {
        var filter = new Object;
        filter.keyword = document.forms['searchForm'].elements['search_suppliers'].value;
        Ajax.call('goods.php?is_ajax=1&act=search_suppliers', filter, searchSuppliersResponse, 'GET', 'JSON')
    }

    function searchSuppliersResponse(result)
    {
    	
        var frm = document.forms['searchForm'];
        var sel = frm.elements['suppliers_id'];

        if (result.error == 0)
        {
            /* 清除 options */
            sel.length = 0;

            /* 创建 options */
            var suppliers = result.content;
            if (suppliers)
            {
                for (i = 0; i < suppliers.length; i++)
                {
                    var opt = document.createElement("OPTION");
                    opt.value = suppliers[i].supplier_id;
                    opt.text  = suppliers[i].supplier_name;
                    sel.options.add(opt);
                }
            }
            else
            {
                var opt = document.createElement("OPTION");
                opt.value = 0;
                opt.text  = search_is_null;
                sel.options.add(opt);
            }
        }

        if (result.message.length > 0)
        {
            alert(result.message);
        }
    }

    function batch_export()
    {
        // 分类
        var cat_id = document.forms['searchForm'].elements['cat_id'].value;
        // 品牌
        var brand_id = document.forms['searchForm'].elements['brand_id'].value;
        // 入驻商
        if (typeof(document.forms['searchForm'].elements['suppliers_id']) == "undefined")
        {
            var suppliers_exists = 0;
            var suppliers_id = 0;
        }
        else
        {
            var suppliers_exists = 1;
            var suppliers_id = document.forms['searchForm'].elements['suppliers_id'].value;
        }
        // 审核状态
        if (typeof(document.forms['searchForm'].elements['supplier_status']) == "undefined")
        {
            var supplier_status = '';
        }
        else
        {
            var supplier_status = document.forms['searchForm'].elements['supplier_status'].value;
        }
        // 推荐
        if (typeof(document.forms['searchForm'].elements['intro_type']) == "undefined")
        {
            var intro_type = '';
        }
        else
        {
            var intro_type = document.forms['searchForm'].elements['intro_type'].value;
        }
        // 上架
        var is_on_sale = document.forms['searchForm'].elements['is_on_sale'].value;
        // 搜索关键字
        var keyword = Utils.trim(document.forms['searchForm'].elements['keyword'].value);
        return location.href='goods.php?act=export&cat_id='+cat_id+'&brand_id='
                +brand_id+'&suppliers_id='+suppliers_id+'&supplier_status='+supplier_status+
                '&intro_type='+intro_type+'&is_on_sale='+is_on_sale+'&keyword='+keyword+'&suppliers_exists='+suppliers_exists;
    }
</script><!-- 商品列表 -->
<form method="post" action="" name="listForm" onsubmit="return confirmSubmit(this)">
  <!-- start goods list -->
  <div class="list-div" id="listDiv">
<table cellpadding="3" cellspacing="1">
  <tr>
    <th>
      <input onclick='listTable.selectAll(this, "checkboxes")' type="checkbox" />
      <a href="javascript:listTable.sort('goods_id'); ">编号</a><img src="images/sort_desc.gif"/>    </th>
	    <th><a href="javascript:listTable.sort('goods_name'); ">商品名称</a></th>
    <th><a href="javascript:listTable.sort('goods_sn'); ">货号</a></th>
    <th><a href="javascript:listTable.sort('shop_price'); ">价格</a></th>
	<th><a href="javascript:listTable.sort('exclusive'); ">手机专享价</a></th><!--手机专享价格   app  jx-->
    <th><a href="javascript:listTable.sort('is_on_sale'); ">上架</a></th>
    <th><a href="javascript:listTable.sort('is_best'); ">精品</a></th>
    <th><a href="javascript:listTable.sort('is_new'); ">新品</a></th>
    <th><a href="javascript:listTable.sort('is_hot'); ">热销</a></th>
	    <th><a href="javascript:listTable.sort('sort_order'); ">推荐排序</a></th>
        <th><a href="javascript:listTable.sort('goods_number'); ">库存</a></th>
        <th>标签</th> <!-- 晒单插件 增加 by www.wrzc.net -->
    <th>操作</th>
  <tr>
    <tr>
    <td><input type="checkbox" name="checkboxes[]" value="283" />283</td>
	    <td class="first-cell" style=""><span onclick="listTable.edit(this, 'edit_goods_name', 283)">好事达家用梯子四步梯加厚梯子折叠梯移动扶梯人字梯2766</span></td>
    <td><span onclick="listTable.edit(this, 'edit_goods_sn', 283)">WRZ000283</span></td>
    <td align="right"><span onclick="listTable.edit(this, 'edit_goods_price', 283)">2.00
    </span></td>
	<!-- 手机专享价格   app  jx  开始-->
	<td align="right"><span onclick="listTable.edit(this, 'edit_exclusive', 283)">-1
    </span></td>
	<!-- 手机专享价格  app  jx   结束 -->
    <td align="center"><img src="images/yes.gif" onclick="listTable.toggle(this, 'toggle_on_sale', 283)" /></td>
    <td align="center"><img src="images/no.gif" onclick="listTable.toggle(this, 'toggle_best', 283)" /></td>
    <td align="center"><img src="images/yes.gif" onclick="listTable.toggle(this, 'toggle_new', 283)" /></td>
    <td align="center"><img src="images/yes.gif" onclick="listTable.toggle(this, 'toggle_hot', 283)" /></td>
	    <td align="center"><span onclick="listTable.edit(this, 'edit_sort_order', 283)">100</span></td>
        <td align="right"><span onclick="listTable.edit(this, 'edit_goods_number', 283)">111</span></td>
        <td align="center"><a href="goods_tag.php?act=list&goods_id=283">标签</a></td> <!-- 晒单插件 增加 by www.wrzc.net -->
    <td align="center">
      <a href="../goods.php?id=283" target="_blank" title="查看"><img src="images/icon_view.gif" width="16" height="16" border="0" /></a>
      <a href="goods.php?act=edit&goods_id=283&extension_code=" title="编辑"><img src="images/icon_edit.gif" width="16" height="16" border="0" /></a>
      <a href="goods.php?act=copy&goods_id=283&extension_code=" title="复制"><img src="images/icon_copy.gif" width="16" height="16" border="0" /></a>
      <a href="javascript:;" onclick="listTable.remove(283, '您确实要把该商品放入回收站吗？')" title="回收站"><img src="images/icon_trash.gif" width="16" height="16" border="0" /></a>
      <a href="goods.php?act=product_list&goods_id=283" title="货品列表"><img src="images/icon_docs.gif" width="16" height="16" border="0" /></a>            <a href="getTaoBaoGoods.php?gid=283"><img src="images/comment_icon.png" border="0" width="21" height="18" /></a>
    </td>
  </tr>
    <tr>
    <td><input type="checkbox" name="checkboxes[]" value="282" />282</td>
	    <td class="first-cell" style=""><span onclick="listTable.edit(this, 'edit_goods_name', 282)">超B级锁芯防伪升级版 防盗门锁芯 防锡纸开门37.5+32.5=70mm</span></td>
    <td><span onclick="listTable.edit(this, 'edit_goods_sn', 282)">WRZ000282</span></td>
    <td align="right"><span onclick="listTable.edit(this, 'edit_goods_price', 282)">129.00
    </span></td>
	<!-- 手机专享价格   app  jx  开始-->
	<td align="right"><span onclick="listTable.edit(this, 'edit_exclusive', 282)">-1
    </span></td>
	<!-- 手机专享价格  app  jx   结束 -->
    <td align="center"><img src="images/yes.gif" onclick="listTable.toggle(this, 'toggle_on_sale', 282)" /></td>
    <td align="center"><img src="images/no.gif" onclick="listTable.toggle(this, 'toggle_best', 282)" /></td>
    <td align="center"><img src="images/yes.gif" onclick="listTable.toggle(this, 'toggle_new', 282)" /></td>
    <td align="center"><img src="images/yes.gif" onclick="listTable.toggle(this, 'toggle_hot', 282)" /></td>
	    <td align="center"><span onclick="listTable.edit(this, 'edit_sort_order', 282)">100</span></td>
        <td align="right"><span onclick="listTable.edit(this, 'edit_goods_number', 282)">1</span></td>
        <td align="center"><a href="goods_tag.php?act=list&goods_id=282">标签</a></td> <!-- 晒单插件 增加 by www.wrzc.net -->
    <td align="center">
      <a href="../goods.php?id=282" target="_blank" title="查看"><img src="images/icon_view.gif" width="16" height="16" border="0" /></a>
      <a href="goods.php?act=edit&goods_id=282&extension_code=" title="编辑"><img src="images/icon_edit.gif" width="16" height="16" border="0" /></a>
      <a href="goods.php?act=copy&goods_id=282&extension_code=" title="复制"><img src="images/icon_copy.gif" width="16" height="16" border="0" /></a>
      <a href="javascript:;" onclick="listTable.remove(282, '您确实要把该商品放入回收站吗？')" title="回收站"><img src="images/icon_trash.gif" width="16" height="16" border="0" /></a>
      <img src="images/empty.gif" width="16" height="16" border="0" />            <a href="getTaoBaoGoods.php?gid=282"><img src="images/comment_icon.png" border="0" width="21" height="18" /></a>
    </td>
  </tr>
    <tr>
    <td><input type="checkbox" name="checkboxes[]" value="281" />281</td>
	    <td class="first-cell" style=""><span onclick="listTable.edit(this, 'edit_goods_name', 281)">卡贝 不锈钢户门吸15CM加长特长墙吸地碰门挡强磁现代家用五金 拉丝不锈钢</span></td>
    <td><span onclick="listTable.edit(this, 'edit_goods_sn', 281)">WRZ000281</span></td>
    <td align="right"><span onclick="listTable.edit(this, 'edit_goods_price', 281)">39.00
    </span></td>
	<!-- 手机专享价格   app  jx  开始-->
	<td align="right"><span onclick="listTable.edit(this, 'edit_exclusive', 281)">-1
    </span></td>
	<!-- 手机专享价格  app  jx   结束 -->
    <td align="center"><img src="images/yes.gif" onclick="listTable.toggle(this, 'toggle_on_sale', 281)" /></td>
    <td align="center"><img src="images/no.gif" onclick="listTable.toggle(this, 'toggle_best', 281)" /></td>
    <td align="center"><img src="images/yes.gif" onclick="listTable.toggle(this, 'toggle_new', 281)" /></td>
    <td align="center"><img src="images/yes.gif" onclick="listTable.toggle(this, 'toggle_hot', 281)" /></td>
	    <td align="center"><span onclick="listTable.edit(this, 'edit_sort_order', 281)">100</span></td>
        <td align="right"><span onclick="listTable.edit(this, 'edit_goods_number', 281)">1</span></td>
        <td align="center"><a href="goods_tag.php?act=list&goods_id=281">标签</a></td> <!-- 晒单插件 增加 by www.wrzc.net -->
    <td align="center">
      <a href="../goods.php?id=281" target="_blank" title="查看"><img src="images/icon_view.gif" width="16" height="16" border="0" /></a>
      <a href="goods.php?act=edit&goods_id=281&extension_code=" title="编辑"><img src="images/icon_edit.gif" width="16" height="16" border="0" /></a>
      <a href="goods.php?act=copy&goods_id=281&extension_code=" title="复制"><img src="images/icon_copy.gif" width="16" height="16" border="0" /></a>
      <a href="javascript:;" onclick="listTable.remove(281, '您确实要把该商品放入回收站吗？')" title="回收站"><img src="images/icon_trash.gif" width="16" height="16" border="0" /></a>
      <img src="images/empty.gif" width="16" height="16" border="0" />            <a href="getTaoBaoGoods.php?gid=281"><img src="images/comment_icon.png" border="0" width="21" height="18" /></a>
    </td>
  </tr>
    <tr>
    <td><input type="checkbox" name="checkboxes[]" value="280" />280</td>
	    <td class="first-cell" style=""><span onclick="listTable.edit(this, 'edit_goods_name', 280)">宝雕 欧式双舌静音象牙白室内房门锁M87457 象牙白宝雕</span></td>
    <td><span onclick="listTable.edit(this, 'edit_goods_sn', 280)">WRZ000280</span></td>
    <td align="right"><span onclick="listTable.edit(this, 'edit_goods_price', 280)">99.00
    </span></td>
	<!-- 手机专享价格   app  jx  开始-->
	<td align="right"><span onclick="listTable.edit(this, 'edit_exclusive', 280)">-1
    </span></td>
	<!-- 手机专享价格  app  jx   结束 -->
    <td align="center"><img src="images/yes.gif" onclick="listTable.toggle(this, 'toggle_on_sale', 280)" /></td>
    <td align="center"><img src="images/no.gif" onclick="listTable.toggle(this, 'toggle_best', 280)" /></td>
    <td align="center"><img src="images/yes.gif" onclick="listTable.toggle(this, 'toggle_new', 280)" /></td>
    <td align="center"><img src="images/yes.gif" onclick="listTable.toggle(this, 'toggle_hot', 280)" /></td>
	    <td align="center"><span onclick="listTable.edit(this, 'edit_sort_order', 280)">100</span></td>
        <td align="right"><span onclick="listTable.edit(this, 'edit_goods_number', 280)">1</span></td>
        <td align="center"><a href="goods_tag.php?act=list&goods_id=280">标签</a></td> <!-- 晒单插件 增加 by www.wrzc.net -->
    <td align="center">
      <a href="../goods.php?id=280" target="_blank" title="查看"><img src="images/icon_view.gif" width="16" height="16" border="0" /></a>
      <a href="goods.php?act=edit&goods_id=280&extension_code=" title="编辑"><img src="images/icon_edit.gif" width="16" height="16" border="0" /></a>
      <a href="goods.php?act=copy&goods_id=280&extension_code=" title="复制"><img src="images/icon_copy.gif" width="16" height="16" border="0" /></a>
      <a href="javascript:;" onclick="listTable.remove(280, '您确实要把该商品放入回收站吗？')" title="回收站"><img src="images/icon_trash.gif" width="16" height="16" border="0" /></a>
      <img src="images/empty.gif" width="16" height="16" border="0" />            <a href="getTaoBaoGoods.php?gid=280"><img src="images/comment_icon.png" border="0" width="21" height="18" /></a>
    </td>
  </tr>
    <tr>
    <td><input type="checkbox" name="checkboxes[]" value="279" />279</td>
	    <td class="first-cell" style=""><span onclick="listTable.edit(this, 'edit_goods_name', 279)">固特 304不锈钢门吸 墙地吸 3408</span></td>
    <td><span onclick="listTable.edit(this, 'edit_goods_sn', 279)">WRZ000279</span></td>
    <td align="right"><span onclick="listTable.edit(this, 'edit_goods_price', 279)">28.00
    </span></td>
	<!-- 手机专享价格   app  jx  开始-->
	<td align="right"><span onclick="listTable.edit(this, 'edit_exclusive', 279)">-1
    </span></td>
	<!-- 手机专享价格  app  jx   结束 -->
    <td align="center"><img src="images/yes.gif" onclick="listTable.toggle(this, 'toggle_on_sale', 279)" /></td>
    <td align="center"><img src="images/no.gif" onclick="listTable.toggle(this, 'toggle_best', 279)" /></td>
    <td align="center"><img src="images/yes.gif" onclick="listTable.toggle(this, 'toggle_new', 279)" /></td>
    <td align="center"><img src="images/yes.gif" onclick="listTable.toggle(this, 'toggle_hot', 279)" /></td>
	    <td align="center"><span onclick="listTable.edit(this, 'edit_sort_order', 279)">100</span></td>
        <td align="right"><span onclick="listTable.edit(this, 'edit_goods_number', 279)">1</span></td>
        <td align="center"><a href="goods_tag.php?act=list&goods_id=279">标签</a></td> <!-- 晒单插件 增加 by www.wrzc.net -->
    <td align="center">
      <a href="../goods.php?id=279" target="_blank" title="查看"><img src="images/icon_view.gif" width="16" height="16" border="0" /></a>
      <a href="goods.php?act=edit&goods_id=279&extension_code=" title="编辑"><img src="images/icon_edit.gif" width="16" height="16" border="0" /></a>
      <a href="goods.php?act=copy&goods_id=279&extension_code=" title="复制"><img src="images/icon_copy.gif" width="16" height="16" border="0" /></a>
      <a href="javascript:;" onclick="listTable.remove(279, '您确实要把该商品放入回收站吗？')" title="回收站"><img src="images/icon_trash.gif" width="16" height="16" border="0" /></a>
      <img src="images/empty.gif" width="16" height="16" border="0" />            <a href="getTaoBaoGoods.php?gid=279"><img src="images/comment_icon.png" border="0" width="21" height="18" /></a>
    </td>
  </tr>
    <tr>
    <td><input type="checkbox" name="checkboxes[]" value="278" />278</td>
	    <td class="first-cell" style=""><span onclick="listTable.edit(this, 'edit_goods_name', 278)">西门子开关插座面板 悦动白墙壁二位双控带荧光开关</span></td>
    <td><span onclick="listTable.edit(this, 'edit_goods_sn', 278)">WRZ000278</span></td>
    <td align="right"><span onclick="listTable.edit(this, 'edit_goods_price', 278)">21.60
    </span></td>
	<!-- 手机专享价格   app  jx  开始-->
	<td align="right"><span onclick="listTable.edit(this, 'edit_exclusive', 278)">-1
    </span></td>
	<!-- 手机专享价格  app  jx   结束 -->
    <td align="center"><img src="images/yes.gif" onclick="listTable.toggle(this, 'toggle_on_sale', 278)" /></td>
    <td align="center"><img src="images/no.gif" onclick="listTable.toggle(this, 'toggle_best', 278)" /></td>
    <td align="center"><img src="images/yes.gif" onclick="listTable.toggle(this, 'toggle_new', 278)" /></td>
    <td align="center"><img src="images/yes.gif" onclick="listTable.toggle(this, 'toggle_hot', 278)" /></td>
	    <td align="center"><span onclick="listTable.edit(this, 'edit_sort_order', 278)">100</span></td>
        <td align="right"><span onclick="listTable.edit(this, 'edit_goods_number', 278)">86</span></td>
        <td align="center"><a href="goods_tag.php?act=list&goods_id=278">标签</a></td> <!-- 晒单插件 增加 by www.wrzc.net -->
    <td align="center">
      <a href="../goods.php?id=278" target="_blank" title="查看"><img src="images/icon_view.gif" width="16" height="16" border="0" /></a>
      <a href="goods.php?act=edit&goods_id=278&extension_code=" title="编辑"><img src="images/icon_edit.gif" width="16" height="16" border="0" /></a>
      <a href="goods.php?act=copy&goods_id=278&extension_code=" title="复制"><img src="images/icon_copy.gif" width="16" height="16" border="0" /></a>
      <a href="javascript:;" onclick="listTable.remove(278, '您确实要把该商品放入回收站吗？')" title="回收站"><img src="images/icon_trash.gif" width="16" height="16" border="0" /></a>
      <img src="images/empty.gif" width="16" height="16" border="0" />            <a href="getTaoBaoGoods.php?gid=278"><img src="images/comment_icon.png" border="0" width="21" height="18" /></a>
    </td>
  </tr>
    <tr>
    <td><input type="checkbox" name="checkboxes[]" value="277" />277</td>
	    <td class="first-cell" style=""><span onclick="listTable.edit(this, 'edit_goods_name', 277)">西门子开关插座面板 悦动白墙壁一开双控带荧光面板</span></td>
    <td><span onclick="listTable.edit(this, 'edit_goods_sn', 277)">WRZ000277</span></td>
    <td align="right"><span onclick="listTable.edit(this, 'edit_goods_price', 277)">16.00
    </span></td>
	<!-- 手机专享价格   app  jx  开始-->
	<td align="right"><span onclick="listTable.edit(this, 'edit_exclusive', 277)">-1
    </span></td>
	<!-- 手机专享价格  app  jx   结束 -->
    <td align="center"><img src="images/yes.gif" onclick="listTable.toggle(this, 'toggle_on_sale', 277)" /></td>
    <td align="center"><img src="images/no.gif" onclick="listTable.toggle(this, 'toggle_best', 277)" /></td>
    <td align="center"><img src="images/yes.gif" onclick="listTable.toggle(this, 'toggle_new', 277)" /></td>
    <td align="center"><img src="images/yes.gif" onclick="listTable.toggle(this, 'toggle_hot', 277)" /></td>
	    <td align="center"><span onclick="listTable.edit(this, 'edit_sort_order', 277)">100</span></td>
        <td align="right"><span onclick="listTable.edit(this, 'edit_goods_number', 277)">1</span></td>
        <td align="center"><a href="goods_tag.php?act=list&goods_id=277">标签</a></td> <!-- 晒单插件 增加 by www.wrzc.net -->
    <td align="center">
      <a href="../goods.php?id=277" target="_blank" title="查看"><img src="images/icon_view.gif" width="16" height="16" border="0" /></a>
      <a href="goods.php?act=edit&goods_id=277&extension_code=" title="编辑"><img src="images/icon_edit.gif" width="16" height="16" border="0" /></a>
      <a href="goods.php?act=copy&goods_id=277&extension_code=" title="复制"><img src="images/icon_copy.gif" width="16" height="16" border="0" /></a>
      <a href="javascript:;" onclick="listTable.remove(277, '您确实要把该商品放入回收站吗？')" title="回收站"><img src="images/icon_trash.gif" width="16" height="16" border="0" /></a>
      <img src="images/empty.gif" width="16" height="16" border="0" />            <a href="getTaoBaoGoods.php?gid=277"><img src="images/comment_icon.png" border="0" width="21" height="18" /></a>
    </td>
  </tr>
    <tr>
    <td><input type="checkbox" name="checkboxes[]" value="276" />276</td>
	    <td class="first-cell" style=""><span onclick="listTable.edit(this, 'edit_goods_name', 276)">鸿雁 六位+三位 ZDP61G3T3/D 3m+ZDP31G3/D 1.5m</span></td>
    <td><span onclick="listTable.edit(this, 'edit_goods_sn', 276)">WRZ000276</span></td>
    <td align="right"><span onclick="listTable.edit(this, 'edit_goods_price', 276)">39.00
    </span></td>
	<!-- 手机专享价格   app  jx  开始-->
	<td align="right"><span onclick="listTable.edit(this, 'edit_exclusive', 276)">-1
    </span></td>
	<!-- 手机专享价格  app  jx   结束 -->
    <td align="center"><img src="images/yes.gif" onclick="listTable.toggle(this, 'toggle_on_sale', 276)" /></td>
    <td align="center"><img src="images/no.gif" onclick="listTable.toggle(this, 'toggle_best', 276)" /></td>
    <td align="center"><img src="images/yes.gif" onclick="listTable.toggle(this, 'toggle_new', 276)" /></td>
    <td align="center"><img src="images/yes.gif" onclick="listTable.toggle(this, 'toggle_hot', 276)" /></td>
	    <td align="center"><span onclick="listTable.edit(this, 'edit_sort_order', 276)">100</span></td>
        <td align="right"><span onclick="listTable.edit(this, 'edit_goods_number', 276)">1</span></td>
        <td align="center"><a href="goods_tag.php?act=list&goods_id=276">标签</a></td> <!-- 晒单插件 增加 by www.wrzc.net -->
    <td align="center">
      <a href="../goods.php?id=276" target="_blank" title="查看"><img src="images/icon_view.gif" width="16" height="16" border="0" /></a>
      <a href="goods.php?act=edit&goods_id=276&extension_code=" title="编辑"><img src="images/icon_edit.gif" width="16" height="16" border="0" /></a>
      <a href="goods.php?act=copy&goods_id=276&extension_code=" title="复制"><img src="images/icon_copy.gif" width="16" height="16" border="0" /></a>
      <a href="javascript:;" onclick="listTable.remove(276, '您确实要把该商品放入回收站吗？')" title="回收站"><img src="images/icon_trash.gif" width="16" height="16" border="0" /></a>
      <img src="images/empty.gif" width="16" height="16" border="0" />            <a href="getTaoBaoGoods.php?gid=276"><img src="images/comment_icon.png" border="0" width="21" height="18" /></a>
    </td>
  </tr>
    <tr>
    <td><input type="checkbox" name="checkboxes[]" value="275" />275</td>
	    <td class="first-cell" style=""><span onclick="listTable.edit(this, 'edit_goods_name', 275)">水动乐柠檬味600ml</span></td>
    <td><span onclick="listTable.edit(this, 'edit_goods_sn', 275)">WRZ000275</span></td>
    <td align="right"><span onclick="listTable.edit(this, 'edit_goods_price', 275)">3.90
    </span></td>
	<!-- 手机专享价格   app  jx  开始-->
	<td align="right"><span onclick="listTable.edit(this, 'edit_exclusive', 275)">-1
    </span></td>
	<!-- 手机专享价格  app  jx   结束 -->
    <td align="center"><img src="images/yes.gif" onclick="listTable.toggle(this, 'toggle_on_sale', 275)" /></td>
    <td align="center"><img src="images/no.gif" onclick="listTable.toggle(this, 'toggle_best', 275)" /></td>
    <td align="center"><img src="images/yes.gif" onclick="listTable.toggle(this, 'toggle_new', 275)" /></td>
    <td align="center"><img src="images/yes.gif" onclick="listTable.toggle(this, 'toggle_hot', 275)" /></td>
	    <td align="center"><span onclick="listTable.edit(this, 'edit_sort_order', 275)">100</span></td>
        <td align="right"><span onclick="listTable.edit(this, 'edit_goods_number', 275)">1</span></td>
        <td align="center"><a href="goods_tag.php?act=list&goods_id=275">标签</a></td> <!-- 晒单插件 增加 by www.wrzc.net -->
    <td align="center">
      <a href="../goods.php?id=275" target="_blank" title="查看"><img src="images/icon_view.gif" width="16" height="16" border="0" /></a>
      <a href="goods.php?act=edit&goods_id=275&extension_code=" title="编辑"><img src="images/icon_edit.gif" width="16" height="16" border="0" /></a>
      <a href="goods.php?act=copy&goods_id=275&extension_code=" title="复制"><img src="images/icon_copy.gif" width="16" height="16" border="0" /></a>
      <a href="javascript:;" onclick="listTable.remove(275, '您确实要把该商品放入回收站吗？')" title="回收站"><img src="images/icon_trash.gif" width="16" height="16" border="0" /></a>
      <img src="images/empty.gif" width="16" height="16" border="0" />            <a href="getTaoBaoGoods.php?gid=275"><img src="images/comment_icon.png" border="0" width="21" height="18" /></a>
    </td>
  </tr>
    <tr>
    <td><input type="checkbox" name="checkboxes[]" value="274" />274</td>
	    <td class="first-cell" style=""><span onclick="listTable.edit(this, 'edit_goods_name', 274)">可口可乐 碳酸饮料 汽水 1.25L</span></td>
    <td><span onclick="listTable.edit(this, 'edit_goods_sn', 274)">WRZ000274</span></td>
    <td align="right"><span onclick="listTable.edit(this, 'edit_goods_price', 274)">4.20
    </span></td>
	<!-- 手机专享价格   app  jx  开始-->
	<td align="right"><span onclick="listTable.edit(this, 'edit_exclusive', 274)">-1
    </span></td>
	<!-- 手机专享价格  app  jx   结束 -->
    <td align="center"><img src="images/yes.gif" onclick="listTable.toggle(this, 'toggle_on_sale', 274)" /></td>
    <td align="center"><img src="images/no.gif" onclick="listTable.toggle(this, 'toggle_best', 274)" /></td>
    <td align="center"><img src="images/yes.gif" onclick="listTable.toggle(this, 'toggle_new', 274)" /></td>
    <td align="center"><img src="images/yes.gif" onclick="listTable.toggle(this, 'toggle_hot', 274)" /></td>
	    <td align="center"><span onclick="listTable.edit(this, 'edit_sort_order', 274)">100</span></td>
        <td align="right"><span onclick="listTable.edit(this, 'edit_goods_number', 274)">1</span></td>
        <td align="center"><a href="goods_tag.php?act=list&goods_id=274">标签</a></td> <!-- 晒单插件 增加 by www.wrzc.net -->
    <td align="center">
      <a href="../goods.php?id=274" target="_blank" title="查看"><img src="images/icon_view.gif" width="16" height="16" border="0" /></a>
      <a href="goods.php?act=edit&goods_id=274&extension_code=" title="编辑"><img src="images/icon_edit.gif" width="16" height="16" border="0" /></a>
      <a href="goods.php?act=copy&goods_id=274&extension_code=" title="复制"><img src="images/icon_copy.gif" width="16" height="16" border="0" /></a>
      <a href="javascript:;" onclick="listTable.remove(274, '您确实要把该商品放入回收站吗？')" title="回收站"><img src="images/icon_trash.gif" width="16" height="16" border="0" /></a>
      <img src="images/empty.gif" width="16" height="16" border="0" />            <a href="getTaoBaoGoods.php?gid=274"><img src="images/comment_icon.png" border="0" width="21" height="18" /></a>
    </td>
  </tr>
    <tr>
    <td><input type="checkbox" name="checkboxes[]" value="273" />273</td>
	    <td class="first-cell" style=""><span onclick="listTable.edit(this, 'edit_goods_name', 273)">康师傅优悦水550ml*24瓶（整箱）</span></td>
    <td><span onclick="listTable.edit(this, 'edit_goods_sn', 273)">WRZ000273</span></td>
    <td align="right"><span onclick="listTable.edit(this, 'edit_goods_price', 273)">19.90
    </span></td>
	<!-- 手机专享价格   app  jx  开始-->
	<td align="right"><span onclick="listTable.edit(this, 'edit_exclusive', 273)">-1
    </span></td>
	<!-- 手机专享价格  app  jx   结束 -->
    <td align="center"><img src="images/yes.gif" onclick="listTable.toggle(this, 'toggle_on_sale', 273)" /></td>
    <td align="center"><img src="images/no.gif" onclick="listTable.toggle(this, 'toggle_best', 273)" /></td>
    <td align="center"><img src="images/yes.gif" onclick="listTable.toggle(this, 'toggle_new', 273)" /></td>
    <td align="center"><img src="images/yes.gif" onclick="listTable.toggle(this, 'toggle_hot', 273)" /></td>
	    <td align="center"><span onclick="listTable.edit(this, 'edit_sort_order', 273)">100</span></td>
        <td align="right"><span onclick="listTable.edit(this, 'edit_goods_number', 273)">1</span></td>
        <td align="center"><a href="goods_tag.php?act=list&goods_id=273">标签</a></td> <!-- 晒单插件 增加 by www.wrzc.net -->
    <td align="center">
      <a href="../goods.php?id=273" target="_blank" title="查看"><img src="images/icon_view.gif" width="16" height="16" border="0" /></a>
      <a href="goods.php?act=edit&goods_id=273&extension_code=" title="编辑"><img src="images/icon_edit.gif" width="16" height="16" border="0" /></a>
      <a href="goods.php?act=copy&goods_id=273&extension_code=" title="复制"><img src="images/icon_copy.gif" width="16" height="16" border="0" /></a>
      <a href="javascript:;" onclick="listTable.remove(273, '您确实要把该商品放入回收站吗？')" title="回收站"><img src="images/icon_trash.gif" width="16" height="16" border="0" /></a>
      <img src="images/empty.gif" width="16" height="16" border="0" />            <a href="getTaoBaoGoods.php?gid=273"><img src="images/comment_icon.png" border="0" width="21" height="18" /></a>
    </td>
  </tr>
    <tr>
    <td><input type="checkbox" name="checkboxes[]" value="272" />272</td>
	    <td class="first-cell" style=""><span onclick="listTable.edit(this, 'edit_goods_name', 272)">江鼎 白酒衡水老白干大青花50度高度白酒500ML</span></td>
    <td><span onclick="listTable.edit(this, 'edit_goods_sn', 272)">WRZ000272</span></td>
    <td align="right"><span onclick="listTable.edit(this, 'edit_goods_price', 272)">118.00
    </span></td>
	<!-- 手机专享价格   app  jx  开始-->
	<td align="right"><span onclick="listTable.edit(this, 'edit_exclusive', 272)">-1
    </span></td>
	<!-- 手机专享价格  app  jx   结束 -->
    <td align="center"><img src="images/yes.gif" onclick="listTable.toggle(this, 'toggle_on_sale', 272)" /></td>
    <td align="center"><img src="images/no.gif" onclick="listTable.toggle(this, 'toggle_best', 272)" /></td>
    <td align="center"><img src="images/yes.gif" onclick="listTable.toggle(this, 'toggle_new', 272)" /></td>
    <td align="center"><img src="images/yes.gif" onclick="listTable.toggle(this, 'toggle_hot', 272)" /></td>
	    <td align="center"><span onclick="listTable.edit(this, 'edit_sort_order', 272)">100</span></td>
        <td align="right"><span onclick="listTable.edit(this, 'edit_goods_number', 272)">1</span></td>
        <td align="center"><a href="goods_tag.php?act=list&goods_id=272">标签</a></td> <!-- 晒单插件 增加 by www.wrzc.net -->
    <td align="center">
      <a href="../goods.php?id=272" target="_blank" title="查看"><img src="images/icon_view.gif" width="16" height="16" border="0" /></a>
      <a href="goods.php?act=edit&goods_id=272&extension_code=" title="编辑"><img src="images/icon_edit.gif" width="16" height="16" border="0" /></a>
      <a href="goods.php?act=copy&goods_id=272&extension_code=" title="复制"><img src="images/icon_copy.gif" width="16" height="16" border="0" /></a>
      <a href="javascript:;" onclick="listTable.remove(272, '您确实要把该商品放入回收站吗？')" title="回收站"><img src="images/icon_trash.gif" width="16" height="16" border="0" /></a>
      <img src="images/empty.gif" width="16" height="16" border="0" />            <a href="getTaoBaoGoods.php?gid=272"><img src="images/comment_icon.png" border="0" width="21" height="18" /></a>
    </td>
  </tr>
    <tr>
    <td><input type="checkbox" name="checkboxes[]" value="271" />271</td>
	    <td class="first-cell" style=""><span onclick="listTable.edit(this, 'edit_goods_name', 271)">江口醇 方形蓝色马六甲 52度500ml*2瓶装 高度浓香型国产白酒 纯粮酒水特价包邮</span></td>
    <td><span onclick="listTable.edit(this, 'edit_goods_sn', 271)">WRZ000271</span></td>
    <td align="right"><span onclick="listTable.edit(this, 'edit_goods_price', 271)">198.00
    </span></td>
	<!-- 手机专享价格   app  jx  开始-->
	<td align="right"><span onclick="listTable.edit(this, 'edit_exclusive', 271)">-1
    </span></td>
	<!-- 手机专享价格  app  jx   结束 -->
    <td align="center"><img src="images/yes.gif" onclick="listTable.toggle(this, 'toggle_on_sale', 271)" /></td>
    <td align="center"><img src="images/yes.gif" onclick="listTable.toggle(this, 'toggle_best', 271)" /></td>
    <td align="center"><img src="images/yes.gif" onclick="listTable.toggle(this, 'toggle_new', 271)" /></td>
    <td align="center"><img src="images/yes.gif" onclick="listTable.toggle(this, 'toggle_hot', 271)" /></td>
	    <td align="center"><span onclick="listTable.edit(this, 'edit_sort_order', 271)">100</span></td>
        <td align="right"><span onclick="listTable.edit(this, 'edit_goods_number', 271)">1</span></td>
        <td align="center"><a href="goods_tag.php?act=list&goods_id=271">标签</a></td> <!-- 晒单插件 增加 by www.wrzc.net -->
    <td align="center">
      <a href="../goods.php?id=271" target="_blank" title="查看"><img src="images/icon_view.gif" width="16" height="16" border="0" /></a>
      <a href="goods.php?act=edit&goods_id=271&extension_code=" title="编辑"><img src="images/icon_edit.gif" width="16" height="16" border="0" /></a>
      <a href="goods.php?act=copy&goods_id=271&extension_code=" title="复制"><img src="images/icon_copy.gif" width="16" height="16" border="0" /></a>
      <a href="javascript:;" onclick="listTable.remove(271, '您确实要把该商品放入回收站吗？')" title="回收站"><img src="images/icon_trash.gif" width="16" height="16" border="0" /></a>
      <img src="images/empty.gif" width="16" height="16" border="0" />            <a href="getTaoBaoGoods.php?gid=271"><img src="images/comment_icon.png" border="0" width="21" height="18" /></a>
    </td>
  </tr>
    <tr>
    <td><input type="checkbox" name="checkboxes[]" value="270" />270</td>
	    <td class="first-cell" style=""><span onclick="listTable.edit(this, 'edit_goods_name', 270)">五粮液 45度小酒版 水晶礼盒装 浓香型白酒 官方授权 酒厂直供 中国名酒 250ML</span></td>
    <td><span onclick="listTable.edit(this, 'edit_goods_sn', 270)">WRZ000270</span></td>
    <td align="right"><span onclick="listTable.edit(this, 'edit_goods_price', 270)">278.00
    </span></td>
	<!-- 手机专享价格   app  jx  开始-->
	<td align="right"><span onclick="listTable.edit(this, 'edit_exclusive', 270)">-1
    </span></td>
	<!-- 手机专享价格  app  jx   结束 -->
    <td align="center"><img src="images/yes.gif" onclick="listTable.toggle(this, 'toggle_on_sale', 270)" /></td>
    <td align="center"><img src="images/no.gif" onclick="listTable.toggle(this, 'toggle_best', 270)" /></td>
    <td align="center"><img src="images/yes.gif" onclick="listTable.toggle(this, 'toggle_new', 270)" /></td>
    <td align="center"><img src="images/yes.gif" onclick="listTable.toggle(this, 'toggle_hot', 270)" /></td>
	    <td align="center"><span onclick="listTable.edit(this, 'edit_sort_order', 270)">100</span></td>
        <td align="right"><span onclick="listTable.edit(this, 'edit_goods_number', 270)">1</span></td>
        <td align="center"><a href="goods_tag.php?act=list&goods_id=270">标签</a></td> <!-- 晒单插件 增加 by www.wrzc.net -->
    <td align="center">
      <a href="../goods.php?id=270" target="_blank" title="查看"><img src="images/icon_view.gif" width="16" height="16" border="0" /></a>
      <a href="goods.php?act=edit&goods_id=270&extension_code=" title="编辑"><img src="images/icon_edit.gif" width="16" height="16" border="0" /></a>
      <a href="goods.php?act=copy&goods_id=270&extension_code=" title="复制"><img src="images/icon_copy.gif" width="16" height="16" border="0" /></a>
      <a href="javascript:;" onclick="listTable.remove(270, '您确实要把该商品放入回收站吗？')" title="回收站"><img src="images/icon_trash.gif" width="16" height="16" border="0" /></a>
      <img src="images/empty.gif" width="16" height="16" border="0" />            <a href="getTaoBaoGoods.php?gid=270"><img src="images/comment_icon.png" border="0" width="21" height="18" /></a>
    </td>
  </tr>
    <tr>
    <td><input type="checkbox" name="checkboxes[]" value="269" />269</td>
	    <td class="first-cell" style=""><span onclick="listTable.edit(this, 'edit_goods_name', 269)">酒仙网 大成·澜爵2009珍藏版品丽珠干红葡萄酒750ml 红酒 国产酒水</span></td>
    <td><span onclick="listTable.edit(this, 'edit_goods_sn', 269)">WRZ000269</span></td>
    <td align="right"><span onclick="listTable.edit(this, 'edit_goods_price', 269)">39.00
    </span></td>
	<!-- 手机专享价格   app  jx  开始-->
	<td align="right"><span onclick="listTable.edit(this, 'edit_exclusive', 269)">-1
    </span></td>
	<!-- 手机专享价格  app  jx   结束 -->
    <td align="center"><img src="images/yes.gif" onclick="listTable.toggle(this, 'toggle_on_sale', 269)" /></td>
    <td align="center"><img src="images/no.gif" onclick="listTable.toggle(this, 'toggle_best', 269)" /></td>
    <td align="center"><img src="images/yes.gif" onclick="listTable.toggle(this, 'toggle_new', 269)" /></td>
    <td align="center"><img src="images/yes.gif" onclick="listTable.toggle(this, 'toggle_hot', 269)" /></td>
	    <td align="center"><span onclick="listTable.edit(this, 'edit_sort_order', 269)">100</span></td>
        <td align="right"><span onclick="listTable.edit(this, 'edit_goods_number', 269)">1</span></td>
        <td align="center"><a href="goods_tag.php?act=list&goods_id=269">标签</a></td> <!-- 晒单插件 增加 by www.wrzc.net -->
    <td align="center">
      <a href="../goods.php?id=269" target="_blank" title="查看"><img src="images/icon_view.gif" width="16" height="16" border="0" /></a>
      <a href="goods.php?act=edit&goods_id=269&extension_code=" title="编辑"><img src="images/icon_edit.gif" width="16" height="16" border="0" /></a>
      <a href="goods.php?act=copy&goods_id=269&extension_code=" title="复制"><img src="images/icon_copy.gif" width="16" height="16" border="0" /></a>
      <a href="javascript:;" onclick="listTable.remove(269, '您确实要把该商品放入回收站吗？')" title="回收站"><img src="images/icon_trash.gif" width="16" height="16" border="0" /></a>
      <img src="images/empty.gif" width="16" height="16" border="0" />            <a href="getTaoBaoGoods.php?gid=269"><img src="images/comment_icon.png" border="0" width="21" height="18" /></a>
    </td>
  </tr>
  </table>
<!-- end goods list -->

<!-- 分页 -->
<table id="page-table" cellspacing="0">
  <tr>
    <td align="right" nowrap="true">
          <!-- $Id: page.htm 14216 2008-03-10 02:27:21Z testyang $ -->
            <div id="turn-page">
        总计  <span id="totalRecords">149</span>
        个记录分为 <span id="totalPages">10</span>
        页当前第 <span id="pageCurrent">1</span>
        页，每页 <input type='text' size='3' id='pageSize' value="15" onkeypress="return listTable.changePageSize(event)" />
        <span id="page-link">
          <a href="javascript:listTable.gotoPageFirst()">第一页</a>
          <a href="javascript:listTable.gotoPagePrev()">上一页</a>
          <a href="javascript:listTable.gotoPageNext()">下一页</a>
          <a href="javascript:listTable.gotoPageLast()">最末页</a>
        <!--   <select id="gotoPage" onchange="listTable.gotoPage(this.value)">
            <option value='1'>1</option><option value='2'>2</option><option value='3'>3</option><option value='4'>4</option><option value='10'>10</option>          </select> -->
         跳转到 <input id="pageInput" title="按回车键跳转到指定页数" value="1" style="width:25px;"  onkeypress="if(event.keyCode==13) {listTable.gotoPage(this.value);}"   type="text">
          <input class="button" id="pagerBtn" value="GO" onclick="goToPage()" type="button">
        </span>
      </div>
<script>
function goToPage(){
	var page=$("#pageInput").val();
	listTable.gotoPage(page);
}
</script>    </td>
  </tr>
</table>

</div>

<div>
	操作：
  <input type="hidden" name="act" value="batch" />
  <select name="type" id="selAction" onchange="change_this(this.value)">
    <option value="">请选择...</option>
        <option value="trash">回收站</option>
    <option value="on_sale">上架</option>
    <option value="not_on_sale">下架</option>
    <option value="best">精品</option>
    <option value="not_best">取消精品</option>
    <option value="new">新品</option>
    <option value="not_new">取消新品</option>
    <option value="hot">热销</option>
    <option value="not_hot">取消热销</option>
    <option value="move_to">转移到分类</option>
	    <option value="suppliers_move_to">转移到供货商</option>
	    
  </select>
  <select name="target_cat" style="display:none" onchange="changeAction()">
    <option value="0">请选择...</option><option value="5" >家用电器</option><option value="182" >&nbsp;&nbsp;&nbsp;&nbsp;五金家装</option><option value="218" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;家具五金</option><option value="219" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;电工电料</option><option value="216" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;手动工具</option><option value="220" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;监控安防</option><option value="217" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;厨卫五金</option><option value="215" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;电动工具</option><option value="179" >&nbsp;&nbsp;&nbsp;&nbsp;生活电器</option><option value="193" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;加湿器</option><option value="197" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;饮水机</option><option value="194" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;吸尘器</option><option value="198" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;其它生活电器</option><option value="195" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;挂烫机/熨斗</option><option value="192" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;净化器</option><option value="191" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;电风扇</option><option value="196" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;取暖电器</option><option value="180" >&nbsp;&nbsp;&nbsp;&nbsp;厨房电器</option><option value="206" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;其它厨房电器</option><option value="203" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;电炖锅</option><option value="200" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;微波炉</option><option value="204" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;果蔬解毒机</option><option value="201" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;电磁炉</option><option value="205" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;养生壶/煎药壶</option><option value="202" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;电饼铛/烧烤盘</option><option value="199" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;电饭煲</option><option value="181" >&nbsp;&nbsp;&nbsp;&nbsp;个护健康</option><option value="209" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;按摩椅</option><option value="213" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;计步器/脂肪检测</option><option value="210" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;足浴盆</option><option value="207" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;剃须刀</option><option value="214" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;其它健康电器</option><option value="211" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;健康秤/厨房秤</option><option value="208" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;电吹风</option><option value="212" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;血糖仪</option><option value="178" >&nbsp;&nbsp;&nbsp;&nbsp;大家电</option><option value="185" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;洗衣机</option><option value="189" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;消毒柜/洗碗机</option><option value="186" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;家庭影院</option><option value="183" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;平板电视</option><option value="190" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;冷柜/冰吧</option><option value="187" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;烟机/灶具</option><option value="184" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;空调冰箱</option><option value="188" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;热水器</option><option value="4" >手机、数码、通信</option><option value="147" >&nbsp;&nbsp;&nbsp;&nbsp;智能设备</option><option value="169" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;体感车</option><option value="166" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;智能眼镜</option><option value="167" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;运动跟踪器</option><option value="164" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;智能手环</option><option value="168" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;智能家居</option><option value="165" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;智能手表</option><option value="144" >&nbsp;&nbsp;&nbsp;&nbsp;热卖手机</option><option value="153" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;联通4G</option><option value="150" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;小米特供</option><option value="154" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;电信4G</option><option value="151" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;魅族手机</option><option value="148" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;三星盖乐世</option><option value="155" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;移动4G</option><option value="152" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;华为荣耀</option><option value="149" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;iPhone</option><option value="145" >&nbsp;&nbsp;&nbsp;&nbsp;手机配件</option><option value="163" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;保护套</option><option value="160" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;创意配件</option><option value="157" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;移动电源</option><option value="161" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;手机饰品</option><option value="158" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;蓝牙耳机</option><option value="162" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;手机耳机</option><option value="159" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;充电器</option><option value="156" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;电池</option><option value="146" >&nbsp;&nbsp;&nbsp;&nbsp;数码影音</option><option value="176" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;数码相框</option><option value="173" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;运动相机</option><option value="170" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;数码相机</option><option value="177" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;影棚器材</option><option value="174" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;摄像机</option><option value="171" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;单反相机</option><option value="175" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;户外器材</option><option value="172" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;拍立得</option><option value="358" >电脑、办公</option><option value="6" >家居、家具、家装、厨具</option><option value="310" >&nbsp;&nbsp;&nbsp;&nbsp;家装软饰</option><option value="348" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;墙贴/装饰贴</option><option value="345" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;帘艺隔断</option><option value="342" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;桌布/罩件</option><option value="349" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;摆件花瓶</option><option value="346" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;相框/照片墙</option><option value="343" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;地毯地垫</option><option value="347" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;装饰字画</option><option value="344" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;沙发垫套/椅垫</option><option value="307" >&nbsp;&nbsp;&nbsp;&nbsp;家具</option><option value="322" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;餐厅家具</option><option value="326" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;沙发</option><option value="323" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;书房家具</option><option value="320" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;卧室家具</option><option value="327" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;鞋架/衣帽架</option><option value="324" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;储物家具</option><option value="321" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;客厅家具</option><option value="325" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;阳台/户外</option><option value="311" >&nbsp;&nbsp;&nbsp;&nbsp;生活日用</option><option value="354" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;洗晒/熨烫</option><option value="351" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;雨伞雨具</option><option value="355" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;净化除味</option><option value="352" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;浴室用品</option><option value="353" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;缝纫/针织用品</option><option value="350" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;收纳用品</option><option value="306" >&nbsp;&nbsp;&nbsp;&nbsp;家纺</option><option value="319" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;窗帘/窗纱</option><option value="316" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;床单被罩</option><option value="313" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;被子</option><option value="317" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;毛巾浴巾</option><option value="314" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;蚊帐</option><option value="318" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;床垫/床褥</option><option value="315" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;凉席</option><option value="312" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;床品套件</option><option value="308" >&nbsp;&nbsp;&nbsp;&nbsp;厨具</option><option value="332" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;餐具</option><option value="329" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;刀剪菜板</option><option value="333" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;茶具/咖啡具</option><option value="330" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;厨房配件</option><option value="331" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;水具酒具</option><option value="328" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;烹饪锅具</option><option value="309" >&nbsp;&nbsp;&nbsp;&nbsp;灯具</option><option value="338" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;落地灯</option><option value="335" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;吸顶灯</option><option value="339" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;应急灯/手电</option><option value="336" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;筒灯射灯</option><option value="340" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;装饰灯</option><option value="337" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;LED灯</option><option value="334" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;台灯</option><option value="341" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;吊灯</option><option value="2" >男装、女装、内衣、珠宝</option><option value="57" >&nbsp;&nbsp;&nbsp;&nbsp;男装馆</option><option value="83" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;休闲短裤</option><option value="80" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;风衣</option><option value="77" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;休闲裤</option><option value="84" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;POLO衫</option><option value="81" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;针织衫</option><option value="78" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;牛仔裤</option><option value="82" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;长袖衬衫</option><option value="79" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nnbsp;夹克</option><option value="55" >&nbsp;&nbsp;&nbsp;&nbsp;女装馆</option><option value="67" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;短外套</option><option value="64" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;时尚套装</option><option value="61" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;连衣裙</option><option value="68" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;防晒衫</option><option value="65" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;复古旗袍</option><option value="62" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;连体裤</option><option value="66" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;牛仔裤</option><option value="63" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;棉麻T恤</option><option value="58" >&nbsp;&nbsp;&nbsp;&nbsp;户外鞋服</option><option value="89" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;迷彩裤</option><option value="86" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;篮球鞋</option><option value="90" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;沙滩鞋</option><option value="87" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;帆布鞋</option><option value="91" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;钓鱼服</option><option value="88" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;羽毛球鞋</option><option value="85" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;跑步鞋</option><option value="92" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;登山鞋</option><option value="59" >&nbsp;&nbsp;&nbsp;&nbsp;女鞋馆</option><option value="99" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;乐福鞋</option><option value="96" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;坡跟单鞋</option><option value="93" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;高跟凉拖</option><option value="100" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;松糕鞋</option><option value="97" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;浅口单鞋</option><option value="94" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;平底鞋</option><option value="98" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;帆布鞋</option><option value="95" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;高跟鞋</option><option value="56" >&nbsp;&nbsp;&nbsp;&nbsp;内衣馆</option><option value="73" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;男士内裤</option><option value="70" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;薄款文胸</option><option value="74" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;夏季睡衣</option><option value="71" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;无钢圈文胸</option><option value="75" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;性感睡衣</option><option value="72" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;女士内裤</option><option value="69" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;聚拢文胸</option><option value="76" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;瘦腿袜</option><option value="60" >&nbsp;&nbsp;&nbsp;&nbsp;箱包馆</option><option value="105" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;男士钱包</option><option value="102" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;手提女包</option><option value="106" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;旅行箱</option><option value="103" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;女士钱包</option><option value="107" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;拉杆箱</option><option value="104" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;男士双肩</option><option value="101" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;单肩女包</option><option value="108" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;拉杆包</option><option value="3" >个护化妆、清洁用品</option><option value="113" >&nbsp;&nbsp;&nbsp;&nbsp;香水彩妆</option><option value="137" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;底妆</option><option value="141" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;美甲</option><option value="138" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;腮红</option><option value="142" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;美容工具</option><option value="139" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;眼部</option><option value="136" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;香水</option><option value="143" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;套装</option><option value="140" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;唇部</option><option value="112" >&nbsp;&nbsp;&nbsp;&nbsp;口腔护理</option><option value="134" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;漱口水</option><option value="135" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;套装</option><option value="132" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;牙膏/牙粉</option><option value="133" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;牙刷/牙线</option><option value="109" >&nbsp;&nbsp;&nbsp;&nbsp;面部护肤</option><option value="118" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;套装</option><option value="115" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;护肤</option><option value="116" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;面膜</option><option value="117" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;剃须</option><option value="114" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;清洁</option><option value="110" >&nbsp;&nbsp;&nbsp;&nbsp;洗发护发</option><option value="121" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;染发</option><option value="122" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;造型</option><option value="119" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;洗发</option><option value="123" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;假发</option><option value="120" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;护发</option><option value="124" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;套装</option><option value="111" >&nbsp;&nbsp;&nbsp;&nbsp;身体护肤</option><option value="131" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;套装</option><option value="128" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;手足</option><option value="125" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;沐浴</option><option value="129" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;纤体塑形</option><option value="126" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;润肤</option><option value="130" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 美胸</option><option value="127" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;颈部</option><option value="7" >酒类饮料</option><option value="273" >&nbsp;&nbsp;&nbsp;&nbsp;饮料饮品</option><option value="289" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;果蔬汁</option><option value="296" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;植物蛋白饮料</option><option value="293" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;纯牛奶</option><option value="290" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;茶饮料</option><option value="294" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;酸奶</option><option value="291" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;碳酸饮料</option><option value="295" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;风味奶</option><option value="292" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;功能饮料</option><option value="274" >&nbsp;&nbsp;&nbsp;&nbsp;茗茶</option><option value="305" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;其他茶</option><option value="302" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;红茶</option><option value="299" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;普洱</option><option value="303" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;花果茶</option><option value="300" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;龙井</option><option value="304" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;养生茶</option><option value="301" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;绿茶</option><option value="298" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;铁观音</option><option value="271" >&nbsp;&nbsp;&nbsp;&nbsp;酒水</option><option value="280" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;养生酒</option><option value="277" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;啤酒</option><option value="281" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;预调酒</option><option value="278" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;葡萄酒/果酒</option><option value="279" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;黄酒/米酒</option><option value="276" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;白酒</option><option value="272" >&nbsp;&nbsp;&nbsp;&nbsp;冲调饮品</option><option value="286" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;奶茶</option><option value="283" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;成人奶粉</option><option value="287" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;麦片谷物</option><option value="284" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;豆浆/豆奶粉</option><option value="297" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;果味冲调</option><option value="288" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;咖啡</option><option value="285" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;茶叶</option><option value="282" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;蜂蜜</option><option value="359" >鞋靴、箱包、钟表、奢侈品</option><option value="360" >运动户外</option><option value="361" >汽车、汽车用品</option><option value="8" >母婴、玩具乐器</option><option value="225" >&nbsp;&nbsp;&nbsp;&nbsp;车床/床品</option><option value="257" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;婴儿床</option><option value="261" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;睡袋/抱被</option><option value="258" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;餐椅</option><option value="255" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;安全座椅</option><option value="262" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;凉席/蚊帐</option><option value="259" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;三轮车</option><option value="256" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;手推车</option><option value="260" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;儿童家具</option><option value="222" >&nbsp;&nbsp;&nbsp;&nbsp;营养/辅食</option><option value="238" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;清火开胃</option><option value="235" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;面食类</option><option value="239" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;钙铁锌</option><option value="236" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;宝宝零食</option><option value="233" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;米粉</option><option value="240" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;益生菌</option><option value="237" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;DHA</option><option value="234" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;果汁/泥</option><option value="226" >&nbsp;&nbsp;&nbsp;&nbsp;孕妈专区</option><option value="270" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;孕妇内裤</option><option value="267" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;妈咪包</option><option value="264" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;打底裤</option><option value="268" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;收腹带</option><option value="265" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;防辐射服</option><option value="269" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;哺乳文胸</option><option value="266" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;腰凳</option><option value="263" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;孕妇裙</option><option value="223" >&nbsp;&nbsp;&nbsp;&nbsp;孕婴洗护</option><option value="241" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;洗衣液/皂</option><option value="245" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;爽身粉</option><option value="242" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;宝宝沐浴</option><option value="246" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;奶瓶清洗</option><option value="243" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;儿童防晒</option><option value="247" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;孕妇护肤</option><option value="244" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;防蚊/驱蚊</option><option value="224" >&nbsp;&nbsp;&nbsp;&nbsp;喂养用品</option><option value="254" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;防溢乳垫</option><option value="251" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;水壶/水杯</option><option value="248" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;奶嘴奶瓶</option><option value="252" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;吸奶器</option><option value="249" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;驱蚊用品</option><option value="253" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;理发器</option><option value="250" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;浴室用品</option><option value="221" >&nbsp;&nbsp;&nbsp;&nbsp;孕婴奶粉</option><option value="232" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;3段</option><option value="229" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;pre段</option><option value="230" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;1段</option><option value="227" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;特配奶粉</option><option value="231" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;2段</option><option value="228" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;孕妈奶粉</option><option value="1" >食品、酒类、生鲜、特产</option><option value="14" >&nbsp;&nbsp;&nbsp;&nbsp;进口水果</option><option value="20" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;奇异果猕猴桃</option><option value="17" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;芒果桃李</option><option value="21" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;凤梨蓝莓</option><option value="18" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;龙眼荔枝</option><option value="15" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;释迦芭乐</option><option value="22" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;榴莲山竹</option><option value="19" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;提子葡萄</option><option value="16" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;樱桃车厘子</option><option value="13" >&nbsp;&nbsp;&nbsp;&nbsp;糖果巧克力</option><option value="24" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;巧克力</option><option value="28" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;奶糖</option><option value="25" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;口香糖</option><option value="29" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;QQ糖</option><option value="26" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;棒棒糖</option><option value="30" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;果冻</option><option value="27" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;软糖</option><option value="9" >&nbsp;&nbsp;&nbsp;&nbsp;牛奶乳品</option><option value="37" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;全脂奶</option><option value="34" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;酸奶</option><option value="31" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;常温奶</option><option value="38" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;成人奶粉</option><option value="35" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;豆奶</option><option value="32" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;乳饮料</option><option value="36" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;低脂奶</option><option value="33" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;儿童奶</option><option value="10" >&nbsp;&nbsp;&nbsp;&nbsp;坚果炒货</option><option value="40" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;夏威夷果</option><option value="44" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;瓜子</option><option value="41" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;碧根果</option><option value="45" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;花生</option><option value="42" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;开心果</option><option value="39" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;核桃</option><option value="46" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;杏仁</option><option value="43" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;腰果</option><option value="12" >&nbsp;&nbsp;&nbsp;&nbsp;蜜饯果干</option><option value="53" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;橄榄</option><option value="50" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;芒果干</option><option value="47" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;红枣</option><option value="54" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;其他</option><option value="51" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;香蕉干</option><option value="48" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;莓类</option><option value="52" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;山楂片</option><option value="49" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;葡萄干</option><option value="362" >营养保健</option><option value="363" >图书、音像、电子书</option><option value="364" >彩票、旅行、充值、票务</option><option value="365" >理财、众筹、白条、保险</option>  </select>
	  <!--二级主菜单：转移供货商-->
  <select name="suppliers_id" style="display:none" onchange="changeAction()">
    <option value="-1">请选择...</option>
    <option value="0">转移到网店</option>
          <option value="1">发的撒饭</option>
      </select>
  <!--end!-->
	  
    <input type="hidden" name="extension_code" value="" />
    <input type="hidden" value=" 确定 " id="btnSubmit" name="btnSubmit" class="button" disabled="true" />
</div>
</form>

<script type="text/javascript">
  listTable.recordCount = 149;
  listTable.pageCount = 10;

    listTable.filter.cat_id = '0';
    listTable.filter.intro_type = '';
    listTable.filter.is_promote = '0';
    listTable.filter.stock_warning = '0';
    listTable.filter.brand_id = '0';
    listTable.filter.keyword = '';
    listTable.filter.suppliers_id = '';
    listTable.filter.is_on_sale = '';
    listTable.filter.sort_by = 'goods_id';
    listTable.filter.sort_order = 'DESC';
    listTable.filter.extension_code = '';
    listTable.filter.is_delete = '0';
    listTable.filter.real_goods = '1';
    listTable.filter.supp = '0';
    listTable.filter.record_count = '149';
    listTable.filter.page_size = '15';
    listTable.filter.page = '1';
    listTable.filter.page_count = '10';
    listTable.filter.start = '0';
  
  
  onload = function()
  {
    startCheckOrder(); // 开始检查订单
    document.forms['listForm'].reset();
  }

  
  function change_this(val)
  {
    var frm = document.forms['listForm'];

    // 切换分类列表的显示
    frm.elements['target_cat'].style.display = frm.elements['type'].value == 'move_to' ? '' : 'none';
			
			frm.elements['suppliers_id'].style.display = frm.elements['type'].value == 'suppliers_move_to' ? '' : 'none';
	
	if (val != "move_to" && val != "suppliers_move_to")
	{
		changeAction();
	}
  }
  
  /**
   * @param: bool ext 其他条件：用于转移分类
   */
  function confirmSubmit(frm, ext)
  {
      if (frm.elements['type'].value == 'trash')
      {
          return confirm(batch_trash_confirm);
      }
      else if (frm.elements['type'].value == 'not_on_sale')
      {
          return confirm(batch_no_on_sale);
      }
      else if (frm.elements['type'].value == 'move_to')
      {
          ext = (ext == undefined) ? true : ext;
          return ext && frm.elements['target_cat'].value != 0;
      }
      else if (frm.elements['type'].value == '')
      {
          return false;
      }
      else
      {
          return true;
      }
  }

  function changeAction()
  {	
      var frm = document.forms['listForm'];

      if (!document.getElementById('btnSubmit').disabled &&
          confirmSubmit(frm))
      {
          frm.submit();
      }
  }

</script>
<div id="footer">
共执行 23 个查询，用时 0.044002 秒，Gzip 已禁用，内存占用 7.134 MB<br />
版权所有 &copy; 2008-2030 广州网软志成信息科技有限公司，并保留所有权利。</div>
<!-- 新订单提示信息 -->
<div id="popMsg">
  <table cellspacing="0" cellpadding="0" width="100%" bgcolor="#cfdef4" border="0">
  <tr>
    <td style="color: #0f2c8c" width="30" height="24"></td>
    <td style="font-weight: normal; color: #1f336b; padding-top: 4px;padding-left: 4px" valign="center" width="100%"> 新订单通知</td>
    <td style="padding-top: 2px;padding-right:2px" valign="center" align="right" width="19"><span title="关闭" style="cursor: hand;cursor:pointer;color:red;font-size:12px;font-weight:bold;margin-right:4px;" onclick="Message.close()" >×</span><!-- <img title=关闭 style="cursor: hand" onclick=closediv() hspace=3 src="msgclose.jpg"> --></td>
  </tr>
  <tr>
    <td style="padding-right: 1px; padding-bottom: 1px" colspan="3" height="70">
    <div id="popMsgContent">
      <p>您有 <strong style="color:#ff0000" id="spanNewOrder">1</strong> 个新订单以及       <strong style="color:#ff0000" id="spanNewPaid">0</strong> 个新付款的订单</p>
      <p align="center" style="word-break:break-all"><a href="order.php?act=list"><span style="color:#ff0000">点击查看新订单</span></a></p>
    </div>
    </td>
  </tr>
  </table>
</div>

<!--
<embed src="images/online.wav" width="0" height="0" autostart="false" name="msgBeep" id="msgBeep" enablejavascript="true"/>
-->
<object classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000" codebase="http://active.macromedia.com/flash2/cabs/swflash.cab#version=4,0,0,0" id="msgBeep" width="1" height="1">
  <param name="movie" value="images/online.swf">
  <param name="quality" value="high">
  <embed src="images/online.swf" name="msgBeep" id="msgBeep" quality="high" width="0" height="0" type="application/x-shockwave-flash" pluginspage="http://www.macromedia.com/shockwave/download/index.cgi?p1_prod_version=shockwaveflash">
  </embed>
</object>

<script language="JavaScript">
document.onmousemove=function(e)
{
  var obj = Utils.srcElement(e);
  if (typeof(obj.onclick) == 'function' && obj.onclick.toString().indexOf('listTable.edit') != -1)
  {
    obj.title = '点击修改内容';
    obj.style.cssText = 'background: #278296;';
    obj.onmouseout = function(e)
    {
      this.style.cssText = '';
    }
  }
  else if (typeof(obj.href) != 'undefined' && obj.href.indexOf('listTable.sort') != -1)
  {
    obj.title = '点击对列表排序';
  }
}
<!--


var MyTodolist;
function showTodoList(adminid)
{
  if(!MyTodolist)
  {
    var global = $import("../js/global.js","js");
    global.onload = global.onreadystatechange= function()
    {
      if(this.readyState && this.readyState=="loading")return;
      var md5 = $import("js/md5.js","js");
      md5.onload = md5.onreadystatechange= function()
      {
        if(this.readyState && this.readyState=="loading")return;
        var todolist = $import("js/todolist.js","js");
        todolist.onload = todolist.onreadystatechange = function()
        {
          if(this.readyState && this.readyState=="loading")return;
          MyTodolist = new Todolist();
          MyTodolist.show();
        }
      }
    }
  }
  else
  {
    if(MyTodolist.visibility)
    {
      MyTodolist.hide();
    }
    else
    {
      MyTodolist.show();
    }
  }
}

if (Browser.isIE)
{
  onscroll = function()
  {
    //document.getElementById('calculator').style.top = document.body.scrollTop;
    document.getElementById('popMsg').style.top = (document.body.scrollTop + document.body.clientHeight - document.getElementById('popMsg').offsetHeight) + "px";
  }
}

if (document.getElementById("listDiv"))
{
  document.getElementById("listDiv").onmouseover = function(e)
  {
    obj = Utils.srcElement(e);

    if (obj)
    {
      if (obj.parentNode.tagName.toLowerCase() == "tr") row = obj.parentNode;
      else if (obj.parentNode.parentNode.tagName.toLowerCase() == "tr") row = obj.parentNode.parentNode;
      else return;

      for (i = 0; i < row.cells.length; i++)
      {
        if (row.cells[i].tagName != "TH") row.cells[i].style.backgroundColor = '#F4FAFB';
      }
    }

  }

  document.getElementById("listDiv").onmouseout = function(e)
  {
    obj = Utils.srcElement(e);

    if (obj)
    {
      if (obj.parentNode.tagName.toLowerCase() == "tr") row = obj.parentNode;
      else if (obj.parentNode.parentNode.tagName.toLowerCase() == "tr") row = obj.parentNode.parentNode;
      else return;

      for (i = 0; i < row.cells.length; i++)
      {
          if (row.cells[i].tagName != "TH") row.cells[i].style.backgroundColor = '';
      }
    }
  }

  document.getElementById("listDiv").onclick = function(e)
  {
    var obj = Utils.srcElement(e);

    if (obj.tagName == "INPUT" && obj.type == "checkbox")
    {
      if (!document.forms['listForm'])
      {
        return;
      }
      var nodes = document.forms['listForm'].elements;
      var checked = false;

      for (i = 0; i < nodes.length; i++)
      {
        if (nodes[i].checked)
        {
           checked = true;
           break;
         }
      }

      if(document.getElementById("btnSubmit"))
      {
        document.getElementById("btnSubmit").disabled = !checked;
      }
      for (i = 1; i <= 10; i++)
      {
        if (document.getElementById("btnSubmit" + i))
        {
          document.getElementById("btnSubmit" + i).disabled = !checked;
        }
      }
    }
  }

}

//-->
</script>
</body>
</html>

<!-- $Id: goods_info.htm 17126 2010-04-23 10:30:26Z liuhui $ -->
<!-- 修改 by www.wrzc.net 百度编辑器 begin -->

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>管理中心 - 编辑商品信息 </title>
<meta name="robots" content="noindex, nofollow">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link href="styles/general.css" rel="stylesheet" type="text/css" />
<link href="styles/main.css" rel="stylesheet" type="text/css" />
<!-- 修改 by www.wrzc.net 百度编辑器 begin -->
<script type="text/javascript" src="js/jquery.js"></script><script type="text/javascript" src="js/jquery.json.js"></script><script type="text/javascript" src="js/transport_bd.js"></script><script type="text/javascript" src="js/common.js"></script><!-- 修改 by www.wrzc.net 百度编辑器 end -->
<script language="JavaScript">
<!--
// 这里把JS用到的所有语言都赋值到这里
var process_request = "正在处理您的请求...";
var todolist_caption = "记事本";
var todolist_autosave = "自动保存";
var todolist_save = "保存";
var todolist_clear = "清除";
var todolist_confirm_save = "是否将更改保存到记事本？";
var todolist_confirm_clear = "是否清空内容？";
var goods_name_not_null = "商品名称不能为空。";
var goods_cat_not_null = "商品分类必须选择。";
var category_cat_not_null = "分类名称不能为空";
var brand_cat_not_null = "品牌名称不能为空";
var goods_cat_not_leaf = "您选择的商品分类不是底级分类，请选择底级分类。";
var shop_price_not_null = "本店售价不能为空。";
var shop_price_not_number = "本店售价不是数值。";
var select_please = "请选择...";
var button_add = "添加";
var button_del = "删除";
var spec_value_not_null = "规格不能为空";
var spec_price_not_number = "加价不是数字";
var market_price_not_number = "市场价格不是数字";
var goods_number_not_int = "商品库存不是整数";
var warn_number_not_int = "库存警告不是整数";
var promote_not_lt = "促销开始日期不能大于结束日期";
var promote_start_not_null = "促销开始时间不能为空";
var promote_end_not_null = "促销结束时间不能为空";
var drop_img_confirm = "您确实要删除该图片吗？";
var batch_no_on_sale = "您确实要将选定的商品下架吗？";
var batch_trash_confirm = "您确实要把选中的商品放入回收站吗？";
var go_category_page = "本页数据将丢失，确认要去商品分类页添加分类吗？";
var go_brand_page = "本页数据将丢失，确认要去商品品牌页添加品牌吗？";
var volume_num_not_null = "请输入优惠数量";
var volume_num_not_number = "优惠数量不是数字";
var volume_price_not_null = "请输入优惠价格";
var volume_price_not_number = "优惠价格不是数字";
var cancel_color = "无样式";
//-->
</script>
</head>
<body>

<div id="menu_list"  onmouseover="show_popup()" onmouseout="hide_popup()">
<ul>
<li><a href="goods.php?act=add" target="main_frame">添加新商品</a></li>
<li><a href="category.php?act=add" target="main_frame">添加商品分类</a></li>
<li><a href="order.php?act=add" target="main_frame">添加订单</a></li>
<li><a href="article.php?act=add" target="main_frame">添加新文章</a></li>
<li><a href="users.php?act=add" target="main_frame">添加会员</a></li>
</ul>
</div>
<script>
function show_popup(){
frmBody = parent.document.getElementById('frame-body');
if (frmBody.cols == "37, 12, *")
{
parent.main_frame.document.getElementById('menu_list').style.left = '195px';
}
else
{
parent.main_frame.document.getElementById('menu_list').style.left = '40px';
}
parent.main_frame.document.getElementById('menu_list').style.display = 'block';
}
function hide_popup(){

parent.main_frame.document.getElementById('menu_list').style.display = 'none';
}
</script>
<h1>
<span class="action-span"><a href="goods.php?act=list&uselastfilter=1">商品列表</a></span>
<span class="action-span1"><a href="index.php?act=main">管理中心</a> </span><span id="search_id" class="action-span1"> - 编辑商品信息 </span>
<div style="clear:both"></div>
</h1>
 <script type="text/javascript" src="../js/utils.js"></script><script type="text/javascript" src="js/selectzone_bd.js"></script><script type="text/javascript" src="js/colorselector.js"></script><!-- 修改 by www.wrzc.net 百度编辑器 end -->
<script type="text/javascript" src="../js/calendar.php?lang="></script>
<link href="../js/calendar/calendar.css" rel="stylesheet" type="text/css" />
<!-- 代码增加 By  www.wrzc.net 促销商品时间精确到时分 Start -->
<script type="text/javascript" src="../js/My97DatePicker/WdatePicker.js"></script>
<!-- 代码增加 By  www.wrzc.net 促销商品时间精确到时分 End -->
<!-- zTree Style -->
<link href="styles/zTree/zTreeStyle.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="js/jquery.ztree.all-3.5.min.js"></script><script type="text/javascript" src="js/category_selecter.js"></script> 
<!-- start goods form -->
<div class="tab-div">
	<!-- tab bar -->
	<div id="tabbar-div">
		<p>
			<span class="tab-front" id="general-tab">通用信息</span>
			<span class="tab-back" id="detail-tab">详细描述</span>
			<span class="tab-back" id="mix-tab">其他信息</span>
						<span class="tab-back" id="properties-tab">商品属性</span>
						<span class="tab-back" id="gallery-tab">商品相册</span>
			<span class="tab-back" id="linkgoods-tab">关联商品</span>
						<span class="tab-back" id="groupgoods-tab">配件</span>
						<span class="tab-back" id="article-tab" style="display:none">关联文章</span>
		</p>
	</div>

	<!-- tab body -->
	<div id="tabbody-div">
		<form enctype="multipart/form-data" action="" method="post" name="theForm">
      		<!-- 最大文件限制 -->
			<input type="hidden" name="MAX_FILE_SIZE" value="2097152" />
      		<!-- 通用信息 -->
			<table width="100%" id="general-table" align="center">
				<tr>
					<td class="label">商品名称：</td>
					<td>
						<input type="text" name="goods_name" value="好事达家用梯子四步梯加厚梯子折叠梯移动扶梯人字梯2766" size="20" />
						<!--<div style="background-color:;float:left;margin-left:2px;" id="font_color" onclick="ColorSelecter.Show(this);">
							<img src="images/color_selecter.gif" style="margin-top:-1px;" />
						</div>
						<input type="hidden" id="goods_name_color" name="goods_name_color" value="" />
						&nbsp;
						<select name="goods_name_style">
							<option value="">字体样式</option>

							<option value="strong">加粗</option><option value="em">斜体</option><option value="u">下划线</option><option value="strike">删除线</option>
						</select>-->
						<span class="require-field">*</span>					</td>
				</tr>
				<tr>
					<td class="label">
						<a href="javascript:showNotice('noticeGoodsSN');" title="点击此处查看提示信息">
							<img src="images/notice.gif" width="16" height="16" border="0" alt="点击此处查看提示信息">
						</a>
						商品货号：					</td>
					<td>
						<input type="text" name="goods_sn" value="WRZ000283" size="20" onblur="checkGoodsSn(this.value,'283')" />
						<span id="goods_sn_notice"></span>
						<br />
						<span class="notice-span" style="display:block"  id="noticeGoodsSN">如果您不输入商品货号，系统将自动生成一个唯一的货号。</span>
					</td>
				</tr>
				<tr>
					<td class="label">商品分类：</td>
					<td>
						<input type="text" id="cat_name" name="cat_name" nowvalue="182" value="五金家装">
						<input type="hidden" id="cat_id" name="cat_id" value="182">
												<a href="javascript:void(0)" onclick="rapidCatAdd()" title="添加分类" class="special">添加分类</a>
						<span id="category_add" style="display:none;">
							<input class="text" size="10" name="addedCategoryName" />
							<a href="javascript:void(0)" onclick="addCategory()" title=" 确定 " class="special"> 确定 </a>
							<a href="javascript:void(0)" onclick="return goCatPage()" title="分类管理" class="special">分类管理</a>
							<a href="javascript:void(0)" onclick="hideCatDiv()" title="隐藏" class="special">&lt;&lt;</a>
						</span>
						 <span class="require-field">*</span>						<script type="text/javascript">
                		$().ready(function(){
							// $("#cat_name")为获取分类名称的jQuery对象，可根据实际情况修改
							// $("#cat_id")为获取分类ID的jQuery对象，可根据实际情况修改
							// "182"为被选中的商品分类编号，无则设置为null或者不写此参数或者为空字符串
							$.ajaxCategorySelecter($("#cat_name"), $("#cat_id"), "182");
						});
            			</script>
					</td>
				</tr>
				<tr>
					<td class="label">扩展分类：</td>
					<td>
						<input type="button" value="添加" onclick="addOtherCat(this.parentNode)" class="button" />
												<select name="other_cat[]" onchange="hideCatDiv()" ><option value="0">请选择...</option><option value="5" >家用电器</option><option value="182" >&nbsp;&nbsp;&nbsp;&nbsp;五金家装</option><option value="218" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;家具五金</option><option value="219" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;电工电料</option><option value="216" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;手动工具</option><option value="220" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;监控安防</option><option value="217" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;厨卫五金</option><option value="215" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;电动工具</option><option value="179" >&nbsp;&nbsp;&nbsp;&nbsp;生活电器</option><option value="193" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;加湿器</option><option value="197" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;饮水机</option><option value="194" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;吸尘器</option><option value="198" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;其它生活电器</option><option value="195" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;挂烫机/熨斗</option><option value="192" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;净化器</option><option value="191" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;电风扇</option><option value="196" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;取暖电器</option><option value="180" >&nbsp;&nbsp;&nbsp;&nbsp;厨房电器</option><option value="206" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;其它厨房电器</option><option value="203" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;电炖锅</option><option value="200" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;微波炉</option><option value="204" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;果蔬解毒机</option><option value="201" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;电磁炉</option><option value="205" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;养生壶/煎药壶</option><option value="202" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;电饼铛/烧烤盘</option><option value="199" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;电饭煲</option><option value="181" >&nbsp;&nbsp;&nbsp;&nbsp;个护健康</option><option value="209" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;按摩椅</option><option value="213" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;计步器/脂肪检测</option><option value="210" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;足浴盆</option><option value="207" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;剃须刀</option><option value="214" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;其它健康电器</option><option value="211" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;健康秤/厨房秤</option><option value="208" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;电吹风</option><option value="212" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;血糖仪</option><option value="178" >&nbsp;&nbsp;&nbsp;&nbsp;大家电</option><option value="185" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;洗衣机</option><option value="189" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;消毒柜/洗碗机</option><option value="186" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;家庭影院</option><option value="183" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;平板电视</option><option value="190" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;冷柜/冰吧</option><option value="187" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;烟机/灶具</option><option value="184" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;空调冰箱</option><option value="188" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;热水器</option><option value="4" >手机、数码、通信</option><option value="147" >&nbsp;&nbsp;&nbsp;&nbsp;智能设备</option><option value="169" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;体感车</option><option value="166" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;智能眼镜</option><option value="167" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;运动跟踪器</option><option value="164" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;智能手环</option><option value="168" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;智能家居</option><option value="165" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;智能手表</option><option value="144" >&nbsp;&nbsp;&nbsp;&nbsp;热卖手机</option><option value="153" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;联通4G</option><option value="150" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;小米特供</option><option value="154" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;电信4G</option><option value="151" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;魅族手机</option><option value="148" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;三星盖乐世</option><option value="155" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;移动4G</option><option value="152" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;华为荣耀</option><option value="149" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;iPhone</option><option value="145" >&nbsp;&nbsp;&nbsp;&nbsp;手机配件</option><option value="163" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;保护套</option><option value="160" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;创意配件</option><option value="157" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;移动电源</option><option value="161" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;手机饰品</option><option value="158" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;蓝牙耳机</option><option value="162" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;手机耳机</option><option value="159" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;充电器</option><option value="156" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;电池</option><option value="146" >&nbsp;&nbsp;&nbsp;&nbsp;数码影音</option><option value="176" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;数码相框</option><option value="173" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;运动相机</option><option value="170" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;数码相机</option><option value="177" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;影棚器材</option><option value="174" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;摄像机</option><option value="171" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;单反相机</option><option value="175" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;户外器材</option><option value="172" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;拍立得</option><option value="358" >电脑、办公</option><option value="6" >家居、家具、家装、厨具</option><option value="310" >&nbsp;&nbsp;&nbsp;&nbsp;家装软饰</option><option value="348" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;墙贴/装饰贴</option><option value="345" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;帘艺隔断</option><option value="342" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;桌布/罩件</option><option value="349" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;摆件花瓶</option><option value="346" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;相框/照片墙</option><option value="343" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;地毯地垫</option><option value="347" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;装饰字画</option><option value="344" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;沙发垫套/椅垫</option><option value="307" >&nbsp;&nbsp;&nbsp;&nbsp;家具</option><option value="322" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;餐厅家具</option><option value="326" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;沙发</option><option value="323" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;书房家具</option><option value="320" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;卧室家具</option><option value="327" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;鞋架/衣帽架</option><option value="324" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;储物家具</option><option value="321" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;客厅家具</option><option value="325" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;阳台/户外</option><option value="311" >&nbsp;&nbsp;&nbsp;&nbsp;生活日用</option><option value="354" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;洗晒/熨烫</option><option value="351" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;雨伞雨具</option><option value="355" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;净化除味</option><option value="352" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;浴室用品</option><option value="353" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;缝纫/针织用品</option><option value="350" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;收纳用品</option><option value="306" >&nbsp;&nbsp;&nbsp;&nbsp;家纺</option><option value="319" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;窗帘/窗纱</option><option value="316" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;床单被罩</option><option value="313" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;被子</option><option value="317" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;毛巾浴巾</option><option value="314" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;蚊帐</option><option value="318" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;床垫/床褥</option><option value="315" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;凉席</option><option value="312" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;床品套件</option><option value="308" >&nbsp;&nbsp;&nbsp;&nbsp;厨具</option><option value="332" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;餐具</option><option value="329" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;刀剪菜板</option><option value="333" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;茶具/咖啡具</option><option value="330" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;厨房配件</option><option value="331" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;水具酒具</option><option value="328" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;烹饪锅具</option><option value="309" >&nbsp;&nbsp;&nbsp;&nbsp;灯具</option><option value="338" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;落地灯</option><option value="335" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;吸顶灯</option><option value="339" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;应急灯/手电</option><option value="336" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;筒灯射灯</option><option value="340" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;装饰灯</option><option value="337" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;LED灯</option><option value="334" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;台灯</option><option value="341" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;吊灯</option><option value="2" >男装、女装、内衣、珠宝</option><option value="57" >&nbsp;&nbsp;&nbsp;&nbsp;男装馆</option><option value="83" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;休闲短裤</option><option value="80" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;风衣</option><option value="77" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;休闲裤</option><option value="84" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;POLO衫</option><option value="81" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;针织衫</option><option value="78" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;牛仔裤</option><option value="82" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;长袖衬衫</option><option value="79" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;夹克</option><option value="55" >&nbsp;&nbsp;&nbsp;&nbsp;女装馆</option><option value="67" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;短外套</option><option value="64" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;时尚套装</option><option value="61" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;连衣裙</option><option value="68" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;防晒衫</option><option value="65" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;复古旗袍</option><option value="62" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;连体裤</option><option value="66" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;牛仔裤</option><option value="63" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;棉麻T恤</option><option value="58" >&nbsp;&nbsp;&nbsp;&nbsp;户外鞋服</option><option value="89" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;迷彩裤</option><option value="86" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;篮球鞋</option><option value="90" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;沙滩鞋</option><option value="87" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;帆布鞋</option><option value="91" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;钓鱼服</option><option value="88" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;羽毛球鞋</option><option value="85" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;跑步鞋</option><option value="92" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;登山鞋</option><option value="59" >&nbsp;&nbsp;&nbsp;&nbsp;女鞋馆</option><option value="99" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;乐福鞋</option><option value="96" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;坡跟单鞋</option><option value="93" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;高跟凉拖</option><option value="100" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;松糕鞋</option><option value="97" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;浅口单鞋</option><option value="94" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;平底鞋</option><option value="98" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;帆布鞋</option><option value="95" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;高跟鞋</option><option value="56" >&nbsp;&nbsp;&nbsp;&nbsp;内衣馆</option><option value="73" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;男士内裤</option><option value="70" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;薄款文胸</option><option value="74" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;夏季睡衣</option><option value="71" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;无钢圈文胸</option><option value="75" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;性感睡衣</option><option value="72" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;女士内裤</option><option value="69" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;聚拢文胸</option><option value="76" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;瘦腿袜</option><option value="60" >&nbsp;&nbsp;&nbsp;&nbsp;箱包馆</option><option value="105" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;男士钱包</option><option value="102" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;手提女包</option><option value="106" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;旅行箱</option><option value="103" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;女士钱包</option><option value="107" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;拉杆箱</option><option value="104" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;男士双肩</option><option value="101" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;单肩女包</option><option value="108" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;拉杆包</option><option value="3" >个护化妆、清洁用品</option><option value="113" >&nbsp;&nbsp;&nbsp;&nbsp;香水彩妆</option><option value="137" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;底妆</option><option value="141" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;美甲</option><option value="138" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;腮红</option><option value="142" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;美容工具</option><option value="139" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;眼部</option><option value="136" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;香水</option><option value="143" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;套装</option><option value="140" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;唇部</option><option value="112" >&nbsp;&nbsp;&nbsp;&nbsp;口腔护理</option><option value="134" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;漱口水</option><option value="135" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;套装</option><option value="132" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;牙膏/牙粉</option><option value="133" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;牙刷/牙线</option><option value="109" >&nbsp;&nbsp;&nbsp;&nbsp;面部护肤</option><option value="118" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;套装</option><option value="115" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;护肤</option><option value="116" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;面膜</option><option value="117" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;剃须</option><option value="114" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;清洁</option><option value="110" >&nbsp;&nbsp;&nbsp;&nbsp;洗发护发</option><option value="121" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;染发</option><option value="122" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;造型</option><option value="119" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;洗发</option><option value="123" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;假发</option><option value="120" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;护发</option><option value="124" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;套装</option><option value="111" >&nbsp;&nbsp;&nbsp;&nbsp;身体护肤</option><option value="131" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;套装</option><option value="128" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;手足</option><option value="125" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;沐浴</option><option value="129" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;纤体塑形</option><option value="126" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;润肤</option><option value="130" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 美胸</option><option value="127" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;颈部</option><option value="7" >酒类饮料</option><option value="273" >&nbsp;&nbsp;&nbsp;&nbsp;饮料饮品</option><option value="289" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;果蔬汁</option><option value="296" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;植物蛋白饮料</option><option value="293" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;纯牛奶</option><option value="290" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;茶饮料</option><option value="294" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;酸奶</option><option value="291" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;碳酸饮料</option><option value="295" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;风味奶</option><option value="292" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;功能饮料</option><option value="274" >&nbsp;&nbsp;&nbsp;&nbsp;茗茶</option><option value="305" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;其他茶</option><option value="302" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;红茶</option><option value="299" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;普洱</option><option value="303" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;花果茶</option><option value="300" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;龙井</option><option value="304" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;养生茶</option><option value="301" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;绿茶</option><option value="298" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;铁观音</option><option value="271" >&nbsp;&nbsp;&nbsp;&nbsp;酒水</option><option value="280" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;养生酒</option><option value="277" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;啤酒</option><option value="281" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;预调酒</option><option value="278" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;葡萄酒/果酒</option><option value="279" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;黄酒/米酒</option><option value="276" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;白酒</option><option value="272" >&nbsp;&nbsp;&nbsp;&nbsp;冲调饮品</option><option value="286" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;奶茶</option><option value="283" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;成人奶粉</option><option value="287" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;麦片谷物</option><option value="284" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;豆浆/豆奶粉</option><option value="297" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;果味冲调</option><option value="288" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;咖啡</option><option value="285" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;茶叶</option><option value="282" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;蜂蜜</option><option value="359" >鞋靴、箱包、钟表、奢侈品</option><option value="360" >运动户外</option><option value="361" >汽车、汽车用品</option><option value="8" >母婴、玩具乐器</option><option value="225" >&nbsp;&nbsp;&nbsp;&nbsp;车床/床品</option><option value="257" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;婴儿床</option><option value="261" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;睡袋/抱被</option><option value="258" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;餐椅</option><option value="255" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;安全座椅</option><option value="262" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;凉席/蚊帐</option><option value="259" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;三轮车</option><option value="256" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;手推车</option><option value="260" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;儿童家具</option><option value="222" >&nbsp;&nbsp;&nbsp;&nbsp;营养/辅食</option><option value="238" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;清火开胃</option><option value="235" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;面食类</option><option value="239" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;钙铁锌</option><option value="236" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;宝宝零食</option><option value="233" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;米粉</option><option value="240" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;益生菌</option><option value="237" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;DHA</option><option value="234" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;果汁/泥</option><option value="226" >&nbsp;&nbsp;&nbsp;&nbsp;孕妈专区</option><option value="270" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;孕妇内裤</option><option value="267" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;妈咪包</option><option value="264" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;打底裤</option><option value="268" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;收腹带</option><option value="265" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;防辐射服</option><option value="269" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;哺乳文胸</option><option value="266" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;腰凳</option><option value="263" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;孕妇裙</option><option value="223" >&nbsp;&nbsp;&nbsp;&nbsp;孕婴洗护</option><option value="241" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;洗衣液/皂</option><option value="245" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;爽身粉</option><option value="242" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;宝宝沐浴</option><option value="246" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;奶瓶清洗</option><option value="243" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;儿童防晒</option><option value="247" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;孕妇护肤</option><option value="244" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;防蚊/驱蚊</option><option value="224" >&nbsp;&nbsp;&nbsp;&nbsp;喂养用品</option><option value="254" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;防溢乳垫</option><option value="251" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;水壶/水杯</option><option value="248" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;奶嘴奶瓶</option><option value="252" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;吸奶器</option><option value="249" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;驱蚊用品</option><option value="253" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;理发器</option><option value="250" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;浴室用品</option><option value="221" >&nbsp;&nbsp;&nbsp;&nbsp;孕婴奶粉</option><option value="232" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;3段</option><option value="229" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;pre段</option><option value="230" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;1段</option><option value="227" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;特配奶粉</option><option value="231" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;2段</option><option value="228" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;孕妈奶粉</option><option value="1" >食品、酒类、生鲜、特产</option><option value="14" >&nbsp;&nbsp;&nbsp;&nbsp;进口水果</option><option value="20" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;奇异果猕猴桃</option><option value="17" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;芒果桃李</option><option value="21" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;凤梨蓝莓</option><option value="18" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;龙眼荔枝</option><option value="15" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;释迦芭乐</option><option value="22" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;榴莲山竹</option><option value="19" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;提子葡萄</option><option value="16" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;樱桃车厘子</option><option value="13" >&nbsp;&nbsp;&nbsp;&nbsp;糖果巧克力</option><option value="24" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;巧克力</option><option value="28" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;奶糖</option><option value="25" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;口香糖</option><option value="29" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;QQ糖</option><option value="26" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;棒棒糖</option><option value="30" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;果冻</option><option value="27" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;软糖</option><option value="9" >&nbsp;&nbsp;&nbsp;&nbsp;牛奶乳品</option><option value="37" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;全脂奶</option><option value="34" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;酸奶</option><option value="31" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;常温奶</option><option value="38" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;成人奶粉</option><option value="35" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;豆奶</option><option value="32" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;乳饮料</option><option value="36" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;低脂奶</option><option value="33" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;儿童奶</option><option value="10" >&nbsp;&nbsp;&nbsp;&nbsp;坚果炒货</option><option value="40" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;夏威夷果</option><option value="44" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;瓜子</option><option value="41" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;碧根果</option><option value="45" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;花生</option><option value="42" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;开心果</option><option value="39" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;核桃</option><option value="46" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;杏仁</option><option value="43" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;腰果</option><option value="12" >&nbsp;&nbsp;&nbsp;&nbsp;蜜饯果干</option><option value="53" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;橄榄</option><option value="50" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;芒果干</option><option value="47" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;红枣</option><option value="54" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;其他</option><option value="51" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;香蕉干</option><option value="48" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;莓类</option><option value="52" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;山楂片</option><option value="49" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;葡萄干</option><option value="362" >营养保健</option><option value="363" >图书、音像、电子书</option><option value="364" >彩票、旅行、充值、票务</option><option value="365" >理财、众筹、白条、保险</option></select>
					</td>
				</tr>
				<tr>
					<td class="label">商品品牌：</td>
					<td>
						<!-- 代码修改_start_derek20150129admin_goods  www.wrzc.net -->

						<input id="brand_search" name="brand_search" type="text" value="请输入……" onclick="onC_search()" onblur="onB_search()" oninput="onK_search(this.value)" />
						<input id="brand_search_bf" name="brand_search_bf" type="hidden" value="" />
						<input id="brand_search_jt" name="brand_search_jt" type="hidden" value="0" />
						<script language="javascript">
			//文本框点击事件
            function onC_search()
			{
				if (document.getElementById("brand_search").value == "请输入……"){
					document.getElementById("brand_search").value = "";
				}
				document.getElementById("brand_content").style.display = "block";
				$("div[id^='@']>div").css('display','block');
			}
			//失去焦点事件
            function onB_search()
			{
				if (document.getElementById("brand_search").value == ""){
					document.getElementById("brand_search").value = document.getElementById("brand_search_bf").value;
				}
				//鼠标离开判断搜索框中名称与选中的名称是否一致，不一致则清空
				if($("#brand_search").val() != $("#brand_name").val()){
					if($("#brand_search_bf").val() != ''){
						$("#brand_search").val($("#brand_search_bf").val());
						$("#brand_id").val($("#brand_search_jt").val());
						$("#brand_name").val($("#brand_search_bf").val());
					}else{
						$("#brand_search").val("");
						$("#brand_id").val(0);
						$("#brand_name").val("");
					}
				}
			}
			//输入事件
			function onK_search(w)
			{
				if (w != "")
				{
					$("div[id^='@']>div").css('display','none');
					$("div[id^='@']>div[id*='"+w+"']").css('display','block');
				}
				else
					$("div[id^='@']>div").css('display','block');
			}
            </script>

						<!-- 代码修改_end_derek20150129admin_goods  www.wrzc.net -->
												<a href="javascript:void(0)" title="添加品牌" onclick="rapidBrandAdd()" class="special">添加品牌</a>
						<span id="brand_add" style="display:none;">
							<input class="text" size="15" name="addedBrandName" />
							<a href="javascript:void(0)" onclick="addBrand()" class="special"> 确定 </a>
							<a href="javascript:void(0)" onclick="return goBrandPage()" title="品牌管理" class="special">品牌管理</a>
							<a href="javascript:void(0)" onclick="hideBrandDiv()" title="隐藏" class="special">&lt;&lt;</a>
						</span>
						
						<!-- 代码增加_start_derek20150129admin_goods  www.wrzc.net -->

						<div id="brand_content" style="margin-top:5px; margin-bottom:10px; display:none">
							<div style="float:left; overflow-y:scroll; width:420px; height:120px; border:#CCC 1px solid">
								<div id="xin_brand" style="display:none">新增品牌：</div>
								<table width="400" border="0" cellspacing="0" cellpadding="0">
									<!--  -->
									<tr>
										<td style="padding:5px 10px">A-G</td>
										<td style="padding:5px 10px">H-K</td>
										<td style="padding:5px 10px">L-S</td>
										<td style="padding:5px 10px">T-Z</td>
										<td style="padding:5px 10px">0-9</td>
									</tr>
									<tr>
										<td valign="top" style="padding-left:10px">
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->

											<div id="@68">
												<div id="geli格力">
													<a href="javascript:go_brand_id(68,'格力')">格力</a>
												</div>
											</div>

											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->

											<div id="@71">
												<div id="gelanshi格兰仕">
													<a href="javascript:go_brand_id(71,'格兰仕')">格兰仕</a>
												</div>
											</div>

											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->

											<div id="@74">
												<div id="ailisite艾力斯特">
													<a href="javascript:go_brand_id(74,'艾力斯特')">艾力斯特</a>
												</div>
											</div>

											<!--  -->
											<!--  -->
											<!--  -->

											<div id="@75">
												<div id="boyangjiafang博洋家纺">
													<a href="javascript:go_brand_id(75,'博洋家纺')">博洋家纺</a>
												</div>
											</div>

											<!--  -->
											<!--  -->
											<!--  -->

											<div id="@76">
												<div id="fuanna富安娜">
													<a href="javascript:go_brand_id(76,'富安娜')">富安娜</a>
												</div>
											</div>

											<!--  -->
											<!--  -->
											<!--  -->

											<div id="@77">
												<div id="aishida爱仕达">
													<a href="javascript:go_brand_id(77,'爱仕达')">爱仕达</a>
												</div>
											</div>

											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->

											<div id="@58">
												<div id="dani达芙妮">
													<a href="javascript:go_brand_id(58,'达芙妮')">达芙妮</a>
												</div>
											</div>

											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->

											<div id="@62">
												<div id="daocaoren稻草人">
													<a href="javascript:go_brand_id(62,'稻草人')">稻草人</a>
												</div>
											</div>

											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->

											<div id="@64">
												<div id="daishu袋鼠">
													<a href="javascript:go_brand_id(64,'袋鼠')">袋鼠</a>
												</div>
											</div>

											<!--  -->
											<!--  -->
											<!--  -->

											<div id="@65">
												<div id="aihuashi爱华仕">
													<a href="javascript:go_brand_id(65,'爱华仕')">爱华仕</a>
												</div>
											</div>

											<!--  -->
											<!--  -->
											<!--  -->

											<div id="@79">
												<div id="anshuibao安睡宝">
													<a href="javascript:go_brand_id(79,'安睡宝')">安睡宝</a>
												</div>
											</div>

											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->

											<div id="@96">
												<div id="beigubeigu贝古贝古">
													<a href="javascript:go_brand_id(96,'贝古贝古')">贝古贝古</a>
												</div>
											</div>

											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->

											<div id="@101">
												<div id="bangbaoshi帮宝适">
													<a href="javascript:go_brand_id(101,'帮宝适')">帮宝适</a>
												</div>
											</div>

											<!--  -->
											<!--  -->
											<!--  -->

											<div id="@102">
												<div id="baobaoxiong抱抱熊">
													<a href="javascript:go_brand_id(102,'抱抱熊')">抱抱熊</a>
												</div>
											</div>

											<!--  -->
											<!--  -->
											<!--  -->

											<div id="@103">
												<div id="balabala巴拉巴拉">
													<a href="javascript:go_brand_id(103,'巴拉巴拉')">巴拉巴拉</a>
												</div>
											</div>

											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->

											<div id="@19">
												<div id="biouquan碧欧泉">
													<a href="javascript:go_brand_id(19,'碧欧泉')">碧欧泉</a>
												</div>
											</div>

											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->

											<div id="@23">
												<div id="duowei朵唯">
													<a href="javascript:go_brand_id(23,'朵唯')">朵唯</a>
												</div>
											</div>

											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->

											<div id="@14">
												<div id="gaosi高丝">
													<a href="javascript:go_brand_id(14,'高丝')">高丝</a>
												</div>
											</div>

											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->

											<div id="@2">
												<div id="ckCK">
													<a href="javascript:go_brand_id(2,'CK')">CK</a>
												</div>
											</div>

											<!--  -->
											<!--  -->
											<!--  -->

											<div id="@3">
												<div id="disneyDisney">
													<a href="javascript:go_brand_id(3,'Disney')">Disney</a>
												</div>
											</div>

											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->

											<div id="@6">
												<div id="diorDior">
													<a href="javascript:go_brand_id(6,'Dior')">Dior</a>
												</div>
											</div>

											<!--  -->
											<!--  -->
											<!--  -->

											<div id="@7">
												<div id="aili爱丽">
													<a href="javascript:go_brand_id(7,'爱丽')">爱丽</a>
												</div>
											</div>

											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->

											<div id="@10">
												<div id="gaosijie高丝洁">
													<a href="javascript:go_brand_id(10,'高丝洁')">高丝洁</a>
												</div>
											</div>

											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->

											<div id="@43">
												<div id="guangming光明">
													<a href="javascript:go_brand_id(43,'光明')">光明</a>
												</div>
											</div>

											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->

											<div id="@45">
												<div id="baicaowei百草味">
													<a href="javascript:go_brand_id(45,'百草味')">百草味</a>
												</div>
											</div>

											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->

											<div id="@38">
												<div id="bishengke必胜客">
													<a href="javascript:go_brand_id(38,'必胜客')">必胜客</a>
												</div>
											</div>

											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
										</td>
										<td valign="top" style="padding-left:10px">
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->

											<div id="@72">
												<div id="haixin海信">
													<a href="javascript:go_brand_id(72,'海信')">海信</a>
												</div>
											</div>

											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->

											<div id="@66">
												<div id="haier海尔">
													<a href="javascript:go_brand_id(66,'海尔')">海尔</a>
												</div>
											</div>

											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->

											<div id="@95">
												<div id="kekoukele可口可乐">
													<a href="javascript:go_brand_id(95,'可口可乐')">可口可乐</a>
												</div>
											</div>

											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->

											<div id="@97">
												<div id="huangjiabaobei皇家宝贝">
													<a href="javascript:go_brand_id(97,'皇家宝贝')">皇家宝贝</a>
												</div>
											</div>

											<!--  -->
											<!--  -->
											<!--  -->

											<div id="@98">
												<div id="hebaotongche呵宝童车">
													<a href="javascript:go_brand_id(98,'呵宝童车')">呵宝童车</a>
												</div>
											</div>

											<!--  -->
											<!--  -->
											<!--  -->

											<div id="@99">
												<div id="heshengyuan合生元">
													<a href="javascript:go_brand_id(99,'合生元')">合生元</a>
												</div>
											</div>

											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->

											<div id="@92">
												<div id="jiaduobao加多宝">
													<a href="javascript:go_brand_id(92,'加多宝')">加多宝</a>
												</div>
											</div>

											<!--  -->
											<!--  -->
											<!--  -->

											<div id="@81">
												<div id="huilejia慧乐家">
													<a href="javascript:go_brand_id(81,'慧乐家')">慧乐家</a>
												</div>
											</div>

											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->

											<div id="@91">
												<div id="haerbin哈尔滨">
													<a href="javascript:go_brand_id(91,'哈尔滨')">哈尔滨</a>
												</div>
											</div>

											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->

											<div id="@15">
												<div id="hanshu韩束">
													<a href="javascript:go_brand_id(15,'韩束')">韩束</a>
												</div>
											</div>

											<!--  -->
											<!--  -->
											<!--  -->

											<div id="@16">
												<div id="kazilan卡姿兰">
													<a href="javascript:go_brand_id(16,'卡姿兰')">卡姿兰</a>
												</div>
											</div>

											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->

											<div id="@24">
												<div id="htchtc">
													<a href="javascript:go_brand_id(24,'htc')">htc</a>
												</div>
											</div>

											<!--  -->
											<!--  -->
											<!--  -->

											<div id="@25">
												<div id="huawei华为">
													<a href="javascript:go_brand_id(25,'华为')">华为</a>
												</div>
											</div>

											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->

											<div id="@12">
												<div id="ji姬芮">
													<a href="javascript:go_brand_id(12,'姬芮')">姬芮</a>
												</div>
											</div>

											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->

											<div id="@27">
												<div id="jinli金立">
													<a href="javascript:go_brand_id(27,'金立')">金立</a>
												</div>
											</div>

											<!--  -->
											<!--  -->
											<!--  -->

											<div id="@42">
												<div id="junlebao君乐宝">
													<a href="javascript:go_brand_id(42,'君乐宝')">君乐宝</a>
												</div>
											</div>

											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->

											<div id="@47">
												<div id="koushuiwa口水娃">
													<a href="javascript:go_brand_id(47,'口水娃')">口水娃</a>
												</div>
											</div>

											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->

											<div id="@39">
												<div id="haidilao海底捞">
													<a href="javascript:go_brand_id(39,'海底捞')">海底捞</a>
												</div>
											</div>

											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->

											<div id="@34">
												<div id="kendeji肯德基">
													<a href="javascript:go_brand_id(34,'肯德基')">肯德基</a>
												</div>
											</div>

											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
										</td>
										<td valign="top" style="padding-left:10px">
											<!--  -->
											<!--  -->

											<div id="@54">
												<div id="shi缪诗">
													<a href="javascript:go_brand_id(54,'缪诗')">缪诗</a>
												</div>
											</div>

											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->

											<div id="@69">
												<div id="laoban老板">
													<a href="javascript:go_brand_id(69,'老板')">老板</a>
												</div>
											</div>

											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->

											<div id="@78">
												<div id="luolai罗莱">
													<a href="javascript:go_brand_id(78,'罗莱')">罗莱</a>
												</div>
											</div>

											<!--  -->
											<!--  -->
											<!--  -->

											<div id="@67">
												<div id="meide美的">
													<a href="javascript:go_brand_id(67,'美的')">美的</a>
												</div>
											</div>

											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->

											<div id="@56">
												<div id="qipilang七匹狼">
													<a href="javascript:go_brand_id(56,'七匹狼')">七匹狼</a>
												</div>
											</div>

											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->

											<div id="@60">
												<div id="mannifenmanniform曼妮芬（ManniForm）">
													<a href="javascript:go_brand_id(60,'曼妮芬（ManniForm）')">曼妮芬（ManniForm）</a>
												</div>
											</div>

											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->

											<div id="@63">
												<div id="sitiya斯提亚">
													<a href="javascript:go_brand_id(63,'斯提亚')">斯提亚</a>
												</div>
											</div>

											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->

											<div id="@100">
												<div id="meizanchen美赞臣">
													<a href="javascript:go_brand_id(100,'美赞臣')">美赞臣</a>
												</div>
											</div>

											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->

											<div id="@104">
												<div id="qingwawangzi青蛙王子">
													<a href="javascript:go_brand_id(104,'青蛙王子')">青蛙王子</a>
												</div>
											</div>

											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->

											<div id="@83">
												<div id="shuixingjiafang水星家纺">
													<a href="javascript:go_brand_id(83,'水星家纺')">水星家纺</a>
												</div>
											</div>

											<!--  -->
											<!--  -->
											<!--  -->

											<div id="@84">
												<div id="quanyoujiaju全有家居">
													<a href="javascript:go_brand_id(84,'全有家居')">全有家居</a>
												</div>
											</div>

											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->

											<div id="@88">
												<div id="langjiu郎酒">
													<a href="javascript:go_brand_id(88,'郎酒')">郎酒</a>
												</div>
											</div>

											<!--  -->
											<!--  -->
											<!--  -->

											<div id="@89">
												<div id="ruiao锐澳">
													<a href="javascript:go_brand_id(89,'锐澳')">锐澳</a>
												</div>
											</div>

											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->

											<div id="@105">
												<div id="queshi雀氏">
													<a href="javascript:go_brand_id(105,'雀氏')">雀氏</a>
												</div>
											</div>

											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->

											<div id="@17">
												<div id="laiya珀莱雅">
													<a href="javascript:go_brand_id(17,'珀莱雅')">珀莱雅</a>
												</div>
											</div>

											<!--  -->
											<!--  -->
											<!--  -->

											<div id="@18">
												<div id="lanzhi兰芝">
													<a href="javascript:go_brand_id(18,'兰芝')">兰芝</a>
												</div>
											</div>

											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->

											<div id="@21">
												<div id="motuoluola摩托罗拉">
													<a href="javascript:go_brand_id(21,'摩托罗拉')">摩托罗拉</a>
												</div>
											</div>

											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->

											<div id="@13">
												<div id="skllSK-ll">
													<a href="javascript:go_brand_id(13,'SK-ll')">SK-ll</a>
												</div>
											</div>

											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->

											<div id="@9">
												<div id="shiwang狮王">
													<a href="javascript:go_brand_id(9,'狮王')">狮王</a>
												</div>
											</div>

											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->

											<div id="@11">
												<div id="missfaceMISS FACE">
													<a href="javascript:go_brand_id(11,'MISS FACE')">MISS FACE</a>
												</div>
											</div>

											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->

											<div id="@26">
												<div id="oppooppo">
													<a href="javascript:go_brand_id(26,'oppo')">oppo</a>
												</div>
											</div>

											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->

											<div id="@44">
												<div id="sanyuan三元">
													<a href="javascript:go_brand_id(44,'三元')">三元</a>
												</div>
											</div>

											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->

											<div id="@46">
												<div id="sanzhisongshu三只松鼠">
													<a href="javascript:go_brand_id(46,'三只松鼠')">三只松鼠</a>
												</div>
											</div>

											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->

											<div id="@48">
												<div id="loulanmiyu楼兰密语">
													<a href="javascript:go_brand_id(48,'楼兰密语')">楼兰密语</a>
												</div>
											</div>

											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->

											<div id="@52">
												<div id="maoren猫人">
													<a href="javascript:go_brand_id(52,'猫人')">猫人</a>
												</div>
											</div>

											<!--  -->
											<!--  -->
											<!--  -->

											<div id="@40">
												<div id="mengniu蒙牛">
													<a href="javascript:go_brand_id(40,'蒙牛')">蒙牛</a>
												</div>
											</div>

											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->

											<div id="@28">
												<div id="lgLG">
													<a href="javascript:go_brand_id(28,'LG')">LG</a>
												</div>
											</div>

											<!--  -->
											<!--  -->
											<!--  -->

											<div id="@29">
												<div id="pingguo苹果">
													<a href="javascript:go_brand_id(29,'苹果')">苹果</a>
												</div>
											</div>

											<!--  -->
											<!--  -->
											<!--  -->

											<div id="@30">
												<div id="sanxing三星">
													<a href="javascript:go_brand_id(30,'三星')">三星</a>
												</div>
											</div>

											<!--  -->
											<!--  -->
											<!--  -->

											<div id="@31">
												<div id="lemeng乐檬">
													<a href="javascript:go_brand_id(31,'乐檬')">乐檬</a>
												</div>
											</div>

											<!--  -->
											<!--  -->
											<!--  -->

											<div id="@32">
												<div id="nubiya努比亚">
													<a href="javascript:go_brand_id(32,'努比亚')">努比亚</a>
												</div>
											</div>

											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->

											<div id="@35">
												<div id="maidanglao麦当劳">
													<a href="javascript:go_brand_id(35,'麦当劳')">麦当劳</a>
												</div>
											</div>

											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
										</td>
										<td valign="top" style="padding-left:10px">
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->

											<div id="@70">
												<div id="ximenzi西门子">
													<a href="javascript:go_brand_id(70,'西门子')">西门子</a>
												</div>
											</div>

											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->

											<div id="@73">
												<div id="yilaikesi伊莱克斯">
													<a href="javascript:go_brand_id(73,'伊莱克斯')">伊莱克斯</a>
												</div>
											</div>

											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->

											<div id="@55">
												<div id="zhuoshini卓诗尼">
													<a href="javascript:go_brand_id(55,'卓诗尼')">卓诗尼</a>
												</div>
											</div>

											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->

											<div id="@57">
												<div id="zuodannu佐丹奴">
													<a href="javascript:go_brand_id(57,'佐丹奴')">佐丹奴</a>
												</div>
											</div>

											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->

											<div id="@59">
												<div id="tatatata她他/tata">
													<a href="javascript:go_brand_id(59,'她他/tata')">她他/tata</a>
												</div>
											</div>

											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->

											<div id="@61">
												<div id="yili伊芙丽">
													<a href="javascript:go_brand_id(61,'伊芙丽')">伊芙丽</a>
												</div>
											</div>

											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->

											<div id="@80">
												<div id="yicainianhua溢彩年华">
													<a href="javascript:go_brand_id(80,'溢彩年华')">溢彩年华</a>
												</div>
											</div>

											<!--  -->
											<!--  -->
											<!--  -->

											<div id="@94">
												<div id="wanglaoji王老吉">
													<a href="javascript:go_brand_id(94,'王老吉')">王老吉</a>
												</div>
											</div>

											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->

											<div id="@93">
												<div id="tongyi统一">
													<a href="javascript:go_brand_id(93,'统一')">统一</a>
												</div>
											</div>

											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->

											<div id="@82">
												<div id="tiantangsan天堂伞">
													<a href="javascript:go_brand_id(82,'天堂伞')">天堂伞</a>
												</div>
											</div>

											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->

											<div id="@85">
												<div id="wuliangye五粮液">
													<a href="javascript:go_brand_id(85,'五粮液')">五粮液</a>
												</div>
											</div>

											<!--  -->
											<!--  -->
											<!--  -->

											<div id="@86">
												<div id="zhoulaojiao泸州老窖">
													<a href="javascript:go_brand_id(86,'泸州老窖')">泸州老窖</a>
												</div>
											</div>

											<!--  -->
											<!--  -->
											<!--  -->

											<div id="@87">
												<div id="yanghe洋河">
													<a href="javascript:go_brand_id(87,'洋河')">洋河</a>
												</div>
											</div>

											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->

											<div id="@90">
												<div id="xuehua雪花">
													<a href="javascript:go_brand_id(90,'雪花')">雪花</a>
												</div>
											</div>

											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->

											<div id="@1">
												<div id="zishengtang资生堂">
													<a href="javascript:go_brand_id(1,'资生堂')">资生堂</a>
												</div>
											</div>

											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->

											<div id="@20">
												<div id="xiaomi小米">
													<a href="javascript:go_brand_id(20,'小米')">小米</a>
												</div>
											</div>

											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->

											<div id="@22">
												<div id="zhongxing中兴">
													<a href="javascript:go_brand_id(22,'中兴')">中兴</a>
												</div>
											</div>

											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->

											<div id="@4">
												<div id="yashilan雅诗兰黛">
													<a href="javascript:go_brand_id(4,'雅诗兰黛')">雅诗兰黛</a>
												</div>
											</div>

											<!--  -->
											<!--  -->
											<!--  -->

											<div id="@5">
												<div id="xiangyibencao相宜本草">
													<a href="javascript:go_brand_id(5,'相宜本草')">相宜本草</a>
												</div>
											</div>

											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->

											<div id="@8">
												<div id="yadun雅顿">
													<a href="javascript:go_brand_id(8,'雅顿')">雅顿</a>
												</div>
											</div>

											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->

											<div id="@49">
												<div id="xiyumeinong西域美农">
													<a href="javascript:go_brand_id(49,'西域美农')">西域美农</a>
												</div>
											</div>

											<!--  -->
											<!--  -->
											<!--  -->

											<div id="@50">
												<div id="tangtangwu糖糖屋">
													<a href="javascript:go_brand_id(50,'糖糖屋')">糖糖屋</a>
												</div>
											</div>

											<!--  -->
											<!--  -->
											<!--  -->

											<div id="@51">
												<div id="xiangai享爱.">
													<a href="javascript:go_brand_id(51,'享爱.')">享爱.</a>
												</div>
											</div>

											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->

											<div id="@41">
												<div id="yili伊利">
													<a href="javascript:go_brand_id(41,'伊利')">伊利</a>
												</div>
											</div>

											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->

											<div id="@36">
												<div id="xiaofeiyang小肥羊">
													<a href="javascript:go_brand_id(36,'小肥羊')">小肥羊</a>
												</div>
											</div>

											<!--  -->
											<!--  -->
											<!--  -->

											<div id="@37">
												<div id="xiaoweiyang小尾羊">
													<a href="javascript:go_brand_id(37,'小尾羊')">小尾羊</a>
												</div>
											</div>

											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->

											<div id="@53">
												<div id="yinmaninman茵曼（INMAN）">
													<a href="javascript:go_brand_id(53,'茵曼（INMAN）')">茵曼（INMAN）</a>
												</div>
											</div>

											<!--  -->
											<!--  -->
										</td>
										<td valign="top" style="padding-left:10px">
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
											<!--  -->
										</td>
									</tr>
									<!--  -->
								</table>
							</div>
							<div style="padding:106px 0px 0px 426px">
								<a href="javascript:no_look_brand_content()">收起列表↑</a>
							</div>
						</div>

						<!--  -->

						<input type="hidden" name="brand_id" id="brand_id" value="0" />
						<input type="hidden" name="brand_name" id="brand_name" value="" />

						<!--  -->
						<script language="javascript">
            function go_brand_id(id,name)
			{
				document.getElementById("brand_id").value = id;
				document.getElementById("brand_search").value = name;
				document.getElementById("brand_name").value = name;
				document.getElementById("brand_content").style.display = "none";
			}
			function no_look_brand_content()
			{
				document.getElementById("brand_content").style.display = "none";
			}
            </script>

						<!-- 代码增加_end_derek20150129admin_goods  www.wrzc.net -->
					</td>
				</tr>
								<tr>
					<td class="label">选择供货商：</td>
					<td>
						<select name="suppliers_id" id="suppliers_id">
							<option value="0">不指定供货商属于本店商品</option>

							<option value="1">发的撒饭</option>
						</select>
					</td>
				</tr>
								<tr>
					<td class="label">本店售价：</td>
					<td>
						<input type="text" name="shop_price" value="2.00" size="20" onblur="priceSetted()" />
						<input type="button" value="按市场价计算" onclick="marketPriceSetted()" />
						<span class="require-field">*</span>					</td>
				</tr>
				<!-- 手机专享价格  app  jx -->
			   <tr>
				<td class="label">手机专享价：</td>
				<td><input type="text" name="exclusive" value="-1" size="20" />
				 <br />
				<span class="notice-span" style="display:block"  id="noticeUserPrice">手机专享价格为-1的时候表示没有手机专享价格</span>
				</td>
			  </tr>
			  <!-- 手机专享价格  app jx -->
								<tr>
					<td class="label">
						<a href="javascript:showNotice('noticeUserPrice');" title="点击此处查看提示信息">
							<img src="images/notice.gif" width="16" height="16" border="0" alt="点击此处查看提示信息">
						</a>
						会员价格：					</td>
					<td>
						 普通会员						<span id="nrank_1"></span>
						<input type="text" id="rank_1" name="user_price[]" value="-1" onkeyup="if(parseInt(this.value)<-1){this.value='-1';};set_price_note(1)" size="8" />
						<input type="hidden" name="user_rank[]" value="1" />
						 铜牌会员						<span id="nrank_2"></span>
						<input type="text" id="rank_2" name="user_price[]" value="-1" onkeyup="if(parseInt(this.value)<-1){this.value='-1';};set_price_note(2)" size="8" />
						<input type="hidden" name="user_rank[]" value="2" />
						 金牌会员						<span id="nrank_3"></span>
						<input type="text" id="rank_3" name="user_price[]" value="-1" onkeyup="if(parseInt(this.value)<-1){this.value='-1';};set_price_note(3)" size="8" />
						<input type="hidden" name="user_rank[]" value="3" />
						 钻石会员						<span id="nrank_4"></span>
						<input type="text" id="rank_4" name="user_price[]" value="-1" onkeyup="if(parseInt(this.value)<-1){this.value='-1';};set_price_note(4)" size="8" />
						<input type="hidden" name="user_rank[]" value="4" />
												<br />
						<span class="notice-span" style="display:block"  id="noticeUserPrice">会员价格为-1时表示会员价格按会员等级折扣率计算。你也可以为每个等级指定一个固定价格</span>
					</td>
				</tr>
				
        		<!--商品优惠价格-->
				<tr>
					<td class="label">
						<a href="javascript:showNotice('volumePrice');" title="点击此处查看提示信息">
							<img src="images/notice.gif" width="16" height="16" border="0" alt="点击此处查看提示信息">
						</a>
						商品优惠价格：					</td>
					<td>
						<table width="100%" id="tbody-volume" align="center">
														<tr>
								<td>
																		<a href="javascript:;" onclick="addVolumePrice(this)">[+]</a>
									 优惠数量									<input type="text" name="volume_number[]" size="8" value="5" />
									优惠价格									<input type="text" name="volume_price[]" size="8" value="2.20" />
								</td>
							</tr>
														<tr>
								<td>
																		<a href="javascript:;" onclick="removeVolumePrice(this)">[-]</a>
									 优惠数量									<input type="text" name="volume_number[]" size="8" value="8" />
									优惠价格									<input type="text" name="volume_price[]" size="8" value="2.00" />
								</td>
							</tr>
														<tr>
								<td>
																		<a href="javascript:;" onclick="removeVolumePrice(this)">[-]</a>
									 优惠数量									<input type="text" name="volume_number[]" size="8" value="10" />
									优惠价格									<input type="text" name="volume_price[]" size="8" value="1.70" />
								</td>
							</tr>
													</table>
						<span class="notice-span" style="display:block"  id="volumePrice">购买数量达到优惠数量时享受的优惠价格</span>
					</td>
				</tr>
        		<!--商品优惠价格 end -->

				<tr>
					<td class="label">市场售价：</td>
					<td>
						<input type="text" name="market_price" value="2.40" size="20" />
						<input type="button" value="取整数" onclick="integral_market_price()" />
					</td>
				</tr>
				<tr>
					<td class="label">
						<a href="javascript:showNotice('giveIntegral');" title="点击此处查看提示信息">
							<img src="images/notice.gif" width="16" height="16" border="0" alt="点击此处查看提示信息">
						</a>
						赠送消费积分数：					</td>
					<td>
						<input type="text" name="give_integral" value="-1" size="20" />
						<br />
						<span class="notice-span" style="display:block"  id="giveIntegral">购买该商品时赠送消费积分数,-1表示按商品价格赠送</span>
					</td>
				</tr>
				<tr>
					<td class="label">
						<a href="javascript:showNotice('rankIntegral');" title="点击此处查看提示信息">
							<img src="images/notice.gif" width="16" height="16" border="0" alt="点击此处查看提示信息">
						</a>
						赠送等级积分数：					</td>
					<td>
						<input type="text" name="rank_integral" value="-1" size="20" />
						<br />
						<span class="notice-span" style="display:block"  id="rankIntegral">购买该商品时赠送等级积分数,-1表示按商品价格赠送</span>
					</td>
				</tr>
				<!-- <tr>
					<td class="label">
						<a href="javascript:showNotice('noticPoints');" title="点击此处查看提示信息">
							<img src="images/notice.gif" width="16" height="16" border="0" alt="点击此处查看提示信息">
						</a>
						积分购买金额：					</td>
					<td>
						<input type="text" name="integral" value="0" size="20" onblur="parseint_integral()" ;/>
						<br />
						<span class="notice-span" style="display:block"  id="noticPoints">(此处需填写金额)购买该商品时最多可以使用积分的金额</span>
					</td>
				</tr> -->
				<tr>
					<td class="label">
						<label for="is_promote">
							<input type="checkbox" id="is_promote" name="is_promote" value="1"  onclick="handlePromote(this.checked);" />
							促销价：						</label>
					</td>
					<td id="promote_3">
						<input type="text" id="promote_1" name="promote_price" value="0.00" size="20" />
					</td>
				</tr>

				<tr id="promote_4">
					<td class="label" id="promote_5">促销日期：</td>
                    <!-- 代码修改 By  www.wrzc.net 促销商品时间精确到时分 Start -->
                    <!--
                             <td id="promote_6">
                                 <input name="promote_start_date" type="text" id="promote_start_date" size="12" value='' readonly="readonly" />
                                 <input name="selbtn1" type="button" id="selbtn1" onclick="return showCalendar('promote_start_date', '%Y-%m-%d', false, false, 'selbtn1');" value="选择" class="button" />
                                 -
                                 <input name="promote_end_date" type="text" id="promote_end_date" size="12" value='' readonly="readonly" />
                                 <input name="selbtn2" type="button" id="selbtn2" onclick="return showCalendar('promote_end_date', '%Y-%m-%d', false, false, 'selbtn2');" value="选择" class="button" />
                             </td>
                         </tr>
                    -->
                    <td id="promote_6">
                        <input name="promote_start_date" type="text" id="promote_start_date" size="12" value='' readonly="readonly" onfocus="WdatePicker({dateFmt:'yyyy-MM-dd HH:mm'})" />
                        -
                        <input name="promote_end_date" type="text" id="promote_end_date" size="12" value='' readonly="readonly" onfocus="WdatePicker({dateFmt:'yyyy-MM-dd HH:mm'})" />
                    </td>
                    <!-- 代码修改 By  www.wrzc.net 促销商品时间精确到时分 End -->


				<tr>
					<td class="label">
						<label for="is_buy">
							<input type="checkbox" id="is_buy" name="is_buy" value="1" checked="checked"  onclick="handleBuy(this.checked);" />
							限购数量：
						</label>
					</td>
					<td id="promote_3">
						<input type="text" id="buymax" name="buymax" value="2" size="20" />
						<br />
						<span class="notice-span" style="display:block"  id="giveIntegral">表示限购日期内，每个用户最多只能购买多少件。0：表示不限购</span>
					</td>
				</tr>
				<tr id="promote_4">
					<td class="label">限购日期：</td>
					<td>
						<input name="buymax_start_date" type="text" id="buymax_start_date" size="12" value='2016-02-25' readonly="readonly" />
						<input name="selbtn3" type="button" id="selbtn3" onclick="return showCalendar('buymax_start_date', '%Y-%m-%d', false, false, 'selbtn3');" value="选择" class="button" />
						-
						<input name="buymax_end_date" type="text" id="buymax_end_date" size="12" value='2016-02-29' readonly="readonly" />
						<input name="selbtn4" type="button" id="selbtn4" onclick="return showCalendar('buymax_end_date', '%Y-%m-%d', false, false, 'selbtn4');" value="选择" class="button" />
					</td>
				</tr>

				<tr>
					<td class="label" id="promote_5">分成金额：</td>
					<td id="promote_7">
						<input type="text" id="promote_1" name="cost_price" value="0.00" size="20" />
						<br />
						<span class="notice-span">分成金额为客户购买本商品，其推荐人能够通过分成获得的金额基数</span>
					</td>

				</tr>

				<tr>
					<td class="label">上传商品图片：</td>
					<td>
						<input type="file" name="goods_img" size="35" />
												<a href="goods.php?act=show_image&img_url=images/201603/goods_img/_P_1457590880024.jpg" target="_blank">
							<img src="images/yes.gif" border="0" />
						</a>
												<br />
						<input type="text" size="40" value="商品图片外部URL" style="color:#aaa;" onfocus="if (this.value == '商品图片外部URL'){this.value='http://';this.style.color='#000';}" name="goods_img_url" />
					</td>
				</tr>
				<tr id="auto_thumb_1">
					<td class="label">上传商品缩略图：</td>
					<td id="auto_thumb_3">
						<input type="file" name="goods_thumb" size="35" />
												<a href="goods.php?act=show_image&img_url=images/201603/thumb_img/_thumb_P_1457590880591.jpg" target="_blank">
							<img src="images/yes.gif" border="0" />
						</a>
												<br />
						<input type="text" size="40" value="商品缩略图外部URL" style="color:#aaa;" onfocus="if (this.value == '商品缩略图外部URL'){this.value='http://';this.style.color='#000';}" name="goods_thumb_url" />
												<br />
						<label for="auto_thumb">
							<input type="checkbox" id="auto_thumb" name="auto_thumb" checked="true" value="1" onclick="handleAutoThumb(this.checked)" />
							自动生成商品缩略图						</label>
											</td>
				</tr>
				<!--代码增加_start byjdy-->
				<tr>
					<td class="label">
						<font color=#ff3300>是否显示在频道首页：</font>
					</td>
					<td>
						<input type="checkbox" name="is_catindex" value="1"  />
						对于二级或三级分类下商品，打勾表示在频道首页显示，否则不显示。
					</td>
				</tr>
				<!--代码增加_end byjdy-->
				
				<!---->
				
			</table>

      		<!-- 详细描述 -->
			<table width="100%" id="detail-table" style="display:none">
				<tr>
					<td>
    <script type="text/javascript" charset="utf-8" 

src="../includes/ueditor/ueditor.config.js"></script>
    <script type="text/javascript" charset="utf-8" 

src="../includes/ueditor/ueditor.all.js"></script>
    <textarea name="goods_desc" id="goods_desc" style="width:100%;">&lt;p&gt;品牌：野外生存&lt;/p&gt;&lt;p&gt;袖长：长袖&lt;/p&gt;&lt;p&gt;适用对象：男&lt;/p&gt;&lt;p&gt;产地：中国&lt;/p&gt;&lt;p&gt;材质：棉&lt;/p&gt;&lt;p&gt;尺码：S,M,L,XL,XXL,XXXL&lt;/p&gt;&lt;p&gt;板型：宽松&lt;/p&gt;&lt;p&gt;颜色：男款长短2穿黑色,男款长短2穿军绿,男款长短2穿迷彩&lt;/p&gt;&lt;p&gt;市场价：198&lt;/p&gt;&lt;p&gt;适用季节：冬季,春季,夏季&lt;/p&gt;&lt;p&gt;功能：透气,耐磨&lt;/p&gt;&lt;p&gt;价格区间：51-100元&lt;/p&gt;&lt;img src=https://img.alicdn.com/imgextra/i2/140582949/TB2qSiKcVXXXXXxXpXXXXXXXXXX_!!140582949.jpg&gt;&lt;img src=https://img.alicdn.com/imgextra/i2/140582949/TB2V8OHcVXXXXaKXpXXXXXXXXXX_!!140582949.jpg&gt;&lt;img src=https://img.alicdn.com/imgextra/i2/140582949/TB2k1qScVXXXXbwXXXXXXXXXXXX_!!140582949.jpg&gt;&lt;img src=https://img.alicdn.com/imgextra/i2/140582949/TB2mZeJcVXXXXaxXpXXXXXXXXXX_!!140582949.jpg&gt;&lt;img src=https://img.alicdn.com/imgextra/i4/140582949/TB296iHcVXXXXbmXpXXXXXXXXXX_!!140582949.jpg&gt;&lt;img src=https://img.alicdn.com/imgextra/i1/140582949/TB2_meKcVXXXXXUXpXXXXXXXXXX_!!140582949.jpg&gt;&lt;img src=https://img.alicdn.com/imgextra/i3/140582949/TB2t3eQcVXXXXccXXXXXXXXXXXX_!!140582949.jpg&gt;&lt;img src=https://img.alicdn.com/imgextra/i2/140582949/TB2QNuKcVXXXXXZXpXXXXXXXXXX_!!140582949.jpg&gt;&lt;img src=https://img.alicdn.com/imgextra/i3/140582949/TB2bSKMcVXXXXXiXpXXXXXXXXXX_!!140582949.jpg&gt;&lt;img src=https://img.alicdn.com/imgextra/i2/140582949/TB2nzaTcVXXXXbzXXXXXXXXXXXX_!!140582949.jpg&gt;&lt;img src=https://img.alicdn.com/imgextra/i1/140582949/TB2fciVcVXXXXbhXXXXXXXXXXXX_!!140582949.jpg&gt;&lt;img src=https://img.alicdn.com/imgextra/i2/140582949/TB208O2cVXXXXXaXXXXXXXXXXXX_!!140582949.jpg&gt;&lt;img src=https://img.alicdn.com/imgextra/i4/140582949/TB29aWMcVXXXXXnXpXXXXXXXXXX_!!140582949.jpg&gt;&lt;img src=https://img.alicdn.com/imgextra/i1/140582949/TB2eWqEcVXXXXb2XpXXXXXXXXXX_!!140582949.jpg&gt;&lt;img src=https://img.alicdn.com/imgextra/i1/140582949/TB2okqLcVXXXXXrXpXXXXXXXXXX_!!140582949.jpg&gt;&lt;img src=https://img.alicdn.com/imgextra/i2/140582949/TB2lW5RcVXXXXcjXXXXXXXXXXXX_!!140582949.jpg&gt;&lt;img src=https://img.alicdn.com/imgextra/i4/140582949/TB2ACOLcVXXXXXzXpXXXXXXXXXX_!!140582949.jpg&gt;&lt;img src=https://img.alicdn.com/imgextra/i4/140582949/TB2wn9HcVXXXXaJXpXXXXXXXXXX_!!140582949.jpg&gt;&lt;img src=https://img.alicdn.com/imgextra/i2/140582949/TB269mMcVXXXXXnXpXXXXXXXXXX_!!140582949.jpg&gt;&lt;img src=https://img.alicdn.com/imgextra/i4/140582949/TB2c9aKcVXXXXXCXpXXXXXXXXXX_!!140582949.jpg&gt;&lt;img src=https://img.alicdn.com/imgextra/i1/140582949/TB2JfSScVXXXXbAXXXXXXXXXXXX_!!140582949.jpg&gt;&lt;img src=https://img.alicdn.com/imgextra/i2/140582949/TB2mPm0cVXXXXaaXXXXXXXXXXXX_!!140582949.jpg&gt;&lt;img src=https://img.alicdn.com/imgextra/i4/140582949/TB2nkWTcVXXXXbzXXXXXXXXXXXX_!!140582949.jpg&gt;&lt;img src=https://img.alicdn.com/imgextra/i3/140582949/TB2IYm0cVXXXXacXXXXXXXXXXXX_!!140582949.jpg&gt;&lt;img src=https://img.alicdn.com/imgextra/i2/140582949/TB2ZGyIcVXXXXa2XpXXXXXXXXXX_!!140582949.jpg&gt;&lt;img src=https://img.alicdn.com/imgextra/i3/140582949/TB2v_K1cVXXXXXAXXXXXXXXXXXX_!!140582949.jpg&gt;&lt;img src=https://img.alicdn.com/imgextra/i2/140582949/TB2KYCWcVXXXXbXXXXXXXXXXXXX_!!140582949.jpg&gt;&lt;img src=https://img.alicdn.com/imgextra/i4/140582949/TB2PO5WcVXXXXa1XXXXXXXXXXXX_!!140582949.jpg&gt;&lt;img src=https://img.alicdn.com/imgextra/i4/140582949/TB2z4WNcVXXXXcIXXXXXXXXXXXX_!!140582949.jpg&gt;&lt;img src=https://img.alicdn.com/imgextra/i3/140582949/TB2eJyJcVXXXXaxXpXXXXXXXXXX_!!140582949.jpg&gt;&lt;img src=https://img.alicdn.com/imgextra/i3/140582949/TB2UnuIcVXXXXanXpXXXXXXXXXX_!!140582949.jpg&gt;&lt;img src=https://img.alicdn.com/imgextra/i2/140582949/TB2bxuTcVXXXXbIXXXXXXXXXXXX_!!140582949.jpg&gt;&lt;img src=https://img.alicdn.com/imgextra/i3/140582949/TB2AqqOcVXXXXc1XXXXXXXXXXXX_!!140582949.jpg&gt;&lt;img src=https://img.alicdn.com/imgextra/i3/140582949/TB23aiNcVXXXXXuXpXXXXXXXXXX_!!140582949.jpg&gt;&lt;img src=https://img.alicdn.com/imgextra/i1/140582949/TB22xCTcVXXXXbvXXXXXXXXXXXX_!!140582949.jpg&gt;&lt;img src=https://img.alicdn.com/imgextra/i3/140582949/TB21YuFcVXXXXb4XpXXXXXXXXXX_!!140582949.jpg&gt;&lt;img src=https://img.alicdn.com/imgextra/i4/140582949/TB2YEuiaFXXXXcMXpXXXXXXXXXX_!!140582949.jpg&gt;&lt;img src=https://img.alicdn.com/imgextra/i4/140582949/TB2905KcVXXXXXPXpXXXXXXXXXX_!!140582949.jpg&gt;&lt;img src=https://img.alicdn.com/imgextra/i4/140582949/TB29byYcVXXXXaJXXXXXXXXXXXX_!!140582949.jpg&gt;&lt;img src=https://img.alicdn.com/imgextra/i2/140582949/TB2W8iRcVXXXXb4XXXXXXXXXXXX_!!140582949.jpg&gt;&lt;img src=https://img.alicdn.com/imgextra/i3/140582949/TB2nemKcVXXXXXRXpXXXXXXXXXX_!!140582949.jpg&gt;&lt;img src=https://img.alicdn.com/imgextra/i1/140582949/TB23R5JcVXXXXX3XpXXXXXXXXXX_!!140582949.jpg&gt;&lt;img src=https://img.alicdn.com/imgextra/i3/140582949/TB2V.SPcVXXXXcgXXXXXXXXXXXX_!!140582949.jpg&gt;&lt;img src=https://img.alicdn.com/imgextra/i1/140582949/TB2NbyYcVXXXXazXXXXXXXXXXXX_!!140582949.jpg&gt;&lt;img src=https://img.alicdn.com/imgextra/i4/140582949/TB2YJK2cVXXXXXdXXXXXXXXXXXX_!!140582949.jpg&gt;&lt;img src=https://img.alicdn.com/imgextra/i2/140582949/TB2zWCKcVXXXXacXpXXXXXXXXXX_!!140582949.jpg&gt;&lt;img src=https://img.alicdn.com/imgextra/i4/140582949/TB2BSOScVXXXXbUXXXXXXXXXXXX_!!140582949.jpg&gt;&lt;img src=https://img.alicdn.com/imgextra/i4/140582949/TB2O1mWcVXXXXa0XXXXXXXXXXXX_!!140582949.jpg&gt;</textarea>
    <script type="text/javascript">
    UE.getEditor("goods_desc",{
    theme:"default", //皮肤
    lang:"zh-cn",    //语言
    initialFrameWidth:1000,  //初始化编辑器宽度,默认650
    initialFrameHeight:350  //初始化编辑器高度,默认180
    });
    </script></td>
				</tr>
			</table>

      		<!-- 其他信息 -->
			<table width="90%" id="mix-table" style="display:none" align="center">
								<tr>
					<td class="label">商品重量：</td>
					<td>
						<input type="text" name="goods_weight" value="" size="20" />
						<select name="weight_unit"> <option value="1">千克</option><option value="0.001" selected>克</option>						</select>
					</td>
				</tr>
				 				<tr>
					<td class="label">
						<a href="javascript:showNotice('noticeStorage');" title="点击此处查看提示信息">
							<img src="images/notice.gif" width="16" height="16" border="0" alt="点击此处查看提示信息">
						</a>
						商品库存数量：					</td>
					<!--            <td><input type="text" name="goods_number" value="111" size="20" readonly="readonly" /><br />-->
					<td>
                        
                        <!--
						<input type="text" name="goods_number" value="111" size="20" />
						-->
                        <input type="text" name="goods_number" value="111" size="20"  style="color: #666" readonly  />
                        
						<br />
						<span class="notice-span" style="display:block"  id="noticeStorage">库存在商品为虚货或商品存在货品时为不可编辑状态，库存数值取决于其虚货数量或货品数量</span>
					</td>
				</tr>
				<tr>
					<td class="label">库存警告数量：</td>
					<td>
						<input type="text" name="warn_number" value="1" size="20" />
					</td>
				</tr>
				                <tr>
					<td class="label">虚拟销量</td>
					<td>
						<input type="text" name="ghost_count" value="0" size="20" />
					</td>
				</tr>
				<tr>
					<td class="label">加入推荐：</td>
					<td>
						<input type="checkbox" name="is_best" value="1"  />
						精品						<input type="checkbox" name="is_new" value="1" checked="checked"  />
						新品						<input type="checkbox" name="is_hot" value="1" checked="checked"  />
						热销					</td>
				</tr>
				<tr id="alone_sale_1">
					<td class="label" id="alone_sale_2">上架：</td>
					<td id="alone_sale_3">
						<input type="checkbox" name="is_on_sale" value="1" checked="checked"  />
						打勾表示允许销售，否则不允许销售。					</td>
				</tr>
				<tr>
					<td class="label">能作为普通商品销售：</td>
					<td>
						<input type="checkbox" name="is_alone_sale" value="1" checked="checked"  />
						打勾表示能作为普通商品销售，否则只能作为配件或赠品销售。					</td>
				</tr>
				<tr>
					<td class="label">是否为免运费商品：</td>
					<td>
						<input type="checkbox" name="is_shipping" value="1"  />
						打勾表示此商品不会产生运费花销，否则按照正常运费计算。					</td>
				</tr>
				<tr>
					<td class="label">商品关键词：</td>
					<td>
						<input type="text" name="keywords" value="" size="40" />
						用空格分隔					</td>
				</tr>
				<tr>
					<td class="label">商品简单描述：</td>
					<td>
						<textarea name="goods_brief" cols="40" rows="3"></textarea>
					</td>
				</tr>
				<tr>
					<td class="label">
						<a href="javascript:showNotice('noticeSellerNote');" title="点击此处查看提示信息">
							<img src="images/notice.gif" width="16" height="16" border="0" alt="点击此处查看提示信息">
						</a>
						商家备注：					</td>
					<td>
						<textarea name="seller_note" cols="40" rows="3"></textarea>
						<br />
						<span class="notice-span" style="display:block"  id="noticeSellerNote">仅供商家自己看的信息</span>
					</td>
				</tr>
			</table>

      		<!-- 属性与规格 --> 
						<table width="100%" id="properties-table" style="display:none" align="center">
				<tr>
					<td class="label">
						<a href="javascript:showNotice('noticeGoodsType');" title="点击此处查看提示信息">
							<img src="images/notice.gif" width="16" height="16" border="0" alt="点击此处查看提示信息">
						</a>
						商品类型：					</td>
					<td>
						<select name="goods_type" onchange="getAttrList(283)">
							<option value="0">请选择商品类型</option>

							<option value='1'>手机数码</option><option value='2' selected="true">服饰鞋帽</option><option value='3'>化妆品</option><option value='4'>人人</option>
						</select>
						<br />
						<span class="notice-span" style="display:block"  id="noticeGoodsType">请选择商品的所属类型，进而完善此商品的属性</span>
					</td>
				</tr>
				<tr>
					<td id="tbody-goodsAttr" colspan="2" style="padding:0"><table width="100%" id="attrTable"><tr><td class='label'>帮面材质：</td><td><input type='hidden' name='attr_id_list[]' value='17' txm='0' class='ctxm_0' /><select class=attr_num_17 name="attr_value_list[]" ><option value="">请选择...</option><option value="头层牛皮" selected="selected">头层牛皮</option><option value="塑胶">塑胶</option><option value="PU">PU</option><option value="网布">网布</option><option value="混合材质">混合材质</option></select>  <input type="hidden" name="attr_price_list[]" value="0" /></td></tr><tr><td class='label'>风格：</td><td><input type='hidden' name='attr_id_list[]' value='18' txm='0' class='ctxm_0' /><select class=attr_num_18 name="attr_value_list[]" ><option value="">请选择...</option><option value="淑女">淑女</option><option value="优雅" selected="selected">优雅</option><option value="性感">性感</option><option value="休闲">休闲</option><option value="甜美">甜美</option><option value="欧美">欧美</option><option value="OL">OL</option><option value="复古">复古</option><option value="文艺">文艺</option><option value="学院">学院</option><option value="韩范">韩范</option><option value="名媛">名媛</option><option value="简约">简约</option></select>  <input type="hidden" name="attr_price_list[]" value="0" /></td></tr><tr><td class='label'>闭合方式：</td><td><input type='hidden' name='attr_id_list[]' value='20' txm='0' class='ctxm_0' /><select class=attr_num_20 name="attr_value_list[]" ><option value="">请选择...</option><option value="一字式扣带">一字式扣带</option><option value="套脚" selected="selected">套脚</option><option value="系带">系带</option><option value="侧拉链">侧拉链</option><option value="套筒">套筒</option><option value="后拉链">后拉链</option><option value="前拉链">前拉链</option></select>  <input type="hidden" name="attr_price_list[]" value="0" /></td></tr><tr><td class='label'>鞋底材质：</td><td><input type='hidden' name='attr_id_list[]' value='22' txm='0' class='ctxm_0' /><select class=attr_num_22 name="attr_value_list[]" ><option value="">请选择...</option><option value="橡胶">橡胶</option><option value="橡胶发泡" selected="selected">橡胶发泡</option><option value="聚氨酯">聚氨酯</option></select>  <input type="hidden" name="attr_price_list[]" value="0" /></td></tr><tr><td class='label'>袖长：</td><td><input type='hidden' name='attr_id_list[]' value='23' txm='0' class='ctxm_0' /><select class=attr_num_23 name="attr_value_list[]" ><option value="">请选择...</option><option value="短袖">短袖</option><option value="无袖" selected="selected">无袖</option><option value="长袖">长袖</option><option value="五分袖">五分袖</option><option value="七分袖">七分袖</option><option value="九分袖">九分袖</option></select>  <input type="hidden" name="attr_price_list[]" value="0" /></td></tr><tr><td class='label'>制作工艺：</td><td><input type='hidden' name='attr_id_list[]' value='24' txm='0' class='ctxm_0' /><select class=attr_num_24 name="attr_value_list[]" ><option value="">请选择...</option><option value="缝制鞋" selected="selected">缝制鞋</option><option value="硫化鞋">硫化鞋</option><option value="胶粘鞋">胶粘鞋</option><option value="注压鞋">注压鞋</option></select>  <input type="hidden" name="attr_price_list[]" value="0" /></td></tr><tr><td class='label'>腰型：</td><td><input type='hidden' name='attr_id_list[]' value='25' txm='0' class='ctxm_0' /><select class=attr_num_25 name="attr_value_list[]" ><option value="">请选择...</option><option value="中腰">中腰</option><option value="高腰">高腰</option><option value="宽松腰">宽松腰</option><option value="松紧腰" selected="selected">松紧腰</option><option value="低腰">低腰</option><option value="超低腰">超低腰</option></select>  <input type="hidden" name="attr_price_list[]" value="0" /></td></tr><tr><td class='label'>内里材质：</td><td><input type='hidden' name='attr_id_list[]' value='26' txm='0' class='ctxm_0' /><select class=attr_num_26 name="attr_value_list[]" ><option value="">请选择...</option><option value="头层牛皮">头层牛皮</option><option value="头层猪皮">头层猪皮</option><option value="人造长毛绒">人造长毛绒</option><option value="兔毛">兔毛</option><option value="PU">PU</option><option value="超细纤维">超细纤维</option><option value="网纱">网纱</option></select>  <input type="hidden" name="attr_price_list[]" value="0" /></td></tr><tr><td class='label'>跟高：</td><td><input type='hidden' name='attr_id_list[]' value='27' txm='0' class='ctxm_0' /><select class=attr_num_27 name="attr_value_list[]" ><option value="">请选择...</option><option value="中跟">中跟</option><option value="高跟">高跟</option><option value="低跟">低跟</option><option value="平跟">平跟</option><option value="超高跟">超高跟</option></select>  <input type="hidden" name="attr_price_list[]" value="0" /></td></tr><tr><td class='label'>鞋跟形状：</td><td><input type='hidden' name='attr_id_list[]' value='28' txm='0' class='ctxm_0' /><select class=attr_num_28 name="attr_value_list[]" ><option value="">请选择...</option><option value="平跟">平跟</option><option value="粗跟">粗跟</option><option value="方跟">方跟</option><option value="坡跟">坡跟</option><option value="内增高">内增高</option><option value="细跟">细跟</option><option value="马蹄跟">马蹄跟</option><option value="松糕底">松糕底</option><option value="酒杯跟">酒杯跟</option></select>  <input type="hidden" name="attr_price_list[]" value="0" /></td></tr><tr><td class='label'>图案：</td><td><input type='hidden' name='attr_id_list[]' value='29' txm='0' class='ctxm_0' /><select class=attr_num_29 name="attr_value_list[]" ><option value="">请选择...</option><option value="纯色">纯色</option><option value="条纹">条纹</option><option value="植物花卉">植物花卉</option><option value="圆点">圆点</option><option value="碎花">碎花</option><option value="抽象图案">抽象图案</option><option value="其他">其他</option></select>  <input type="hidden" name="attr_price_list[]" value="0" /></td></tr><tr><td class='label'>鞋头：</td><td><input type='hidden' name='attr_id_list[]' value='30' txm='0' class='ctxm_0' /><select class=attr_num_30 name="attr_value_list[]" ><option value="">请选择...</option><option value="圆头">圆头</option><option value="尖头">尖头</option><option value="方头">方头</option><option value="鱼嘴">鱼嘴</option></select>  <input type="hidden" name="attr_price_list[]" value="0" /></td></tr><tr><td class='label'>裙型：</td><td><input type='hidden' name='attr_id_list[]' value='31' txm='0' class='ctxm_0' /><select class=attr_num_31 name="attr_value_list[]" ><option value="">请选择...</option><option value="A字裙">A字裙</option><option value="大摆型">大摆型</option><option value="不规则裙">不规则裙</option><option value="公主裙">公主裙</option><option value="一步裙">一步裙</option><option value="百褶裙">百褶裙</option><option value="铅笔裙">铅笔裙</option><option value="荷叶边裙">荷叶边裙</option><option value="灯笼裙">灯笼裙</option><option value="蛋糕裙">蛋糕裙</option></select>  <input type="hidden" name="attr_price_list[]" value="0" /></td></tr><tr><td class='label'>功能：</td><td><input type='hidden' name='attr_id_list[]' value='32' txm='0' class='ctxm_0' /><input name="attr_value_list[]" type="text" value="" size="40" />  <input type="hidden" name="attr_price_list[]" value="0" /></td></tr><tr><td class='label'>材质：</td><td><input type='hidden' name='attr_id_list[]' value='33' txm='0' class='ctxm_0' /><input name="attr_value_list[]" type="text" value="" size="40" />  <input type="hidden" name="attr_price_list[]" value="0" /></td></tr><tr><td class='label'>裙长：</td><td><input type='hidden' name='attr_id_list[]' value='34' txm='0' class='ctxm_0' /><input name="attr_value_list[]" type="text" value="" size="40" />  <input type="hidden" name="attr_price_list[]" value="0" /></td></tr><tr><td class='label'>领型：</td><td><input type='hidden' name='attr_id_list[]' value='35' txm='0' class='ctxm_0' /><input name="attr_value_list[]" type="text" value="" size="40" />  <input type="hidden" name="attr_price_list[]" value="0" /></td></tr><tr><td class='label'>袖型：</td><td><input type='hidden' name='attr_id_list[]' value='36' txm='0' class='ctxm_0' /><input name="attr_value_list[]" type="text" value="" size="40" />  <input type="hidden" name="attr_price_list[]" value="0" /></td></tr><tr><td class='label'>版型：</td><td><input type='hidden' name='attr_id_list[]' value='37' txm='0' class='ctxm_0' /><input name="attr_value_list[]" type="text" value="" size="40" />  <input type="hidden" name="attr_price_list[]" value="0" /></td></tr><tr><td class='label'><a href='javascript:;' onclick='addSpec(this)'>[+]</a>颜色：</td><td><input type='hidden' name='attr_id_list[]' value='15' txm='0' class='ctxm_0' /><select class=attr_num_15 name="attr_value_list[]" ><option value="">请选择...</option><option value="白色">白色</option><option value="黑色">黑色</option><option value="粉色" selected="selected">粉色</option><option value="红色">红色</option><option value="灰色">灰色</option><option value="棕色">棕色</option><option value="蓝色">蓝色</option><option value="米色">米色</option><option value="黄色">黄色</option><option value="玫红">玫红</option><option value="金色">金色</option><option value="银色">银色</option><option value="紫色">紫色</option></select> 属性价格 <input type="text" name="attr_price_list[]" value="" size="5" maxlength="10" /></td></tr><tr><td class='label'><a href='javascript:;' onclick='addSpec(this)'>[+]</a>尺码：</td><td><input type='hidden' name='attr_id_list[]' value='16' txm='0' class='ctxm_0' /><select class=attr_num_16 name="attr_value_list[]" ><option value="">请选择...</option><option value="S">S</option><option value="M">M</option><option value="L">L</option><option value="XL">XL</option><option value="XXL">XXL</option><option value="35.5">35.5</option><option value="36">36</option><option value="36.5">36.5</option><option value="37">37</option><option value="37.5">37.5</option><option value="38">38</option><option value="38.5">38.5</option><option value="39">39</option><option value="39.5">39.5</option><option value="40">40</option><option value="40.5" selected="selected">40.5</option><option value="41">41</option><option value="41.5">41.5</option><option value="42">42</option><option value="42.5">42.5</option><option value="43">43</option><option value="43.5">43.5</option><option value="44">44</option><option value="45">45</option></select> 属性价格 <input type="text" name="attr_price_list[]" value="" size="5" maxlength="10" /></td></tr></table><div id="input_txm"></div></td>
				</tr>
			</table>
			
			<!--代码修改_start By www.wygk.cn  将 商品相册 这部分代码完全修改成下面这样-->
			<table width="100%" id="gallery-table" style="display:none" align="center">
				<!-- 图片列表 -->
				<tr>
					<td>
						<style>
.attr-color-div {
	width: 100%;
	background: #BBDDE5;
	text-indent: 10px;
	height: 26px;
	padding-top: 1px;
}

.attr-front {
	background: #CCFF99;
	line-height: 24px;
	font-weight: bold;
	padding: 6px 15px 6px 18px;
}

.attr-back {
	color: #FF0000;
	font-weight: bold;
	line-height: 24px;
	padding: 6px 15px 6px 18px;
	border-right: 1px solid #FFF;
}
</style>
												<script>
			
			function changeCurrentColor(n)
			{
			for(i=1;i<=2;i++)
			{
				document.getElementById("color_" + i).className = "attr-back";
			}
			document.getElementById("color_" + n).className = "attr-front";
			}
			
			</script>
						<font color=#ff3300>请选择商品颜色</font>
						（点击下面不同颜色切换到该颜色对应的相册）
						<br>
						<br>
						<div class="attr-color-div">
														<span class="attr-front" id="color_1">
								<a href="attr_img_upload.php?goods_id=283&goods_attr_id=327" onclick="javascript:changeCurrentColor(1)" target="attr_upload">粉色</a>
							</span>
														<span class="attr-back" id="color_2">
								<a href="attr_img_upload.php?goods_id=283&goods_attr_id=" onclick="javascript:changeCurrentColor(2)" target="attr_upload">通用</a>
							</span>
													</div>
						<iframe name="attr_upload" src="attr_img_upload.php?goods_id=283&goods_attr_id=327" frameborder=1 scrolling=no width="100%" height="auto" style="min-height:630px; border:1px #eee solid; margin-top:5px;"> </iframe>
					</td>
				</tr>
				<tr>
					<td>&nbsp;</td>
				</tr>
			</table>

			<!--代码修改_end By www.wygk.cn-->

			<!-- 鍏宠仈鍟嗗搧 -->
			<table width="90%" id="linkgoods-table" style="display:none" align="center">
        		<!-- 商品搜索 -->
				<tr>
					<td colspan="3">
						<img src="images/icon_search.gif" width="26" height="22" border="0" alt="SEARCH" />
						<select name="cat_id1">
							<option value="0">所有分类<option value="5" >家用电器</option><option value="182" selected='ture'>&nbsp;&nbsp;&nbsp;&nbsp;五金家装</option><option value="218" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;家具五金</option><option value="219" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;电工电料</option><option value="216" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;手动工具</option><option value="220" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;监控安防</option><option value="217" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;厨卫五金</option><option value="215" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;电动工具</option><option value="179" >&nbsp;&nbsp;&nbsp;&nbsp;生活电器</option><option value="193" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;加湿器</option><option value="197" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;饮水机</option><option value="194" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;吸尘器</option><option value="198" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;其它生活电器</option><option value="195" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;挂烫机/熨斗</option><option value="192" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;净化器</option><option value="191" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;电风扇</option><option value="196" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;取暖电器</option><option value="180" >&nbsp;&nbsp;&nbsp;&nbsp;厨房电器</option><option value="206" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;其它厨房电器</option><option value="203" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;电炖锅</option><option value="200" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;微波炉</option><option value="204" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;果蔬解毒机</option><option value="201" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;电磁炉</option><option value="205" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;养生壶/煎药壶</option><option value="202" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;电饼铛/烧烤盘</option><option value="199" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;电饭煲</option><option value="181" >&nbsp;&nbsp;&nbsp;&nbsp;个护健康</option><option value="209" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;按摩椅</option><option value="213" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;计步器/脂肪检测</option><option value="210" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;足浴盆</option><option value="207" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;剃须刀</option><option value="214" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;其它健康电器</option><option value="211" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;健康秤/厨房秤</option><option value="208" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;电吹风</option><option value="212" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;血糖仪</option><option value="178" >&nbsp;&nbsp;&nbsp;&nbsp;大家电</option><option value="185" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;洗衣机</option><option value="189" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;消毒柜/洗碗机</option><option value="186" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;家庭影院</option><option value="183" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;平板电视</option><option value="190" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;冷柜/冰吧</option><option value="187" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;烟机/灶具</option><option value="184" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;空调冰箱</option><option value="188" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;热水器</option><option value="4" >手机、数码、通信</option><option value="147" >&nbsp;&nbsp;&nbsp;&nbsp;智能设备</option><option value="169" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;体感车</option><option value="166" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;智能眼镜</option><option value="167" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;运动跟踪器</option><option value="164" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;智能手环</option><option value="168" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;智能家居</option><option value="165" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;智能手表</option><option value="144" >&nbsp;&nbsp;&nbsp;&nbsp;热卖手机</option><option value="153" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;联通4G</option><option value="150" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;小米特供</option><option value="154" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;电信4G</option><option value="151" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;魅族手机</option><option value="148" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;三星盖乐世</option><option value="155" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;移动4G</option><option value="152" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;华为荣耀</option><option value="149" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;iPhone</option><option value="145" >&nbsp;&nbsp;&nbsp;&nbsp;手机配件</option><option value="163" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;保护套</option><option value="160" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;创意配件</option><option value="157" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;移动电源</option><option value="161" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;手机饰品</option><option value="158" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;蓝牙耳机</option><option value="162" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;手机耳机</option><option value="159" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;充电器</option><option value="156" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;电池</option><option value="146" >&nbsp;&nbsp;&nbsp;&nbsp;数码影音</option><option value="176" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;数码相框</option><option value="173" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsnbsp;运动相机</option><option value="170" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;数码相机</option><option value="177" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;影棚器材</option><option value="174" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;摄像机</option><option value="171" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;单反相机</option><option value="175" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;户外器材</option><option value="172" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;拍立得</option><option value="358" >电脑、办公</option><option value="6" >家居、家具、家装、厨具</option><option value="310" >&nbsp;&nbsp;&nbsp;&nbsp;家装软饰</option><option value="348" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;墙贴/装饰贴</option><option value="345" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;帘艺隔断</option><option value="342" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;桌布/罩件</option><option value="349" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;摆件花瓶</option><option value="346" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;相框/照片墙</option><option value="343" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;地毯地垫</option><option value="347" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;装饰字画</option><option value="344" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;沙发垫套/椅垫</option><option value="307" >&nbsp;&nbsp;&nbsp;&nbsp;家具</option><option value="322" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;餐厅家具</option><option value="326" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;沙发</option><option value="323" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;书房家具</option><option value="320" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;卧室家具</option><option value="327" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;鞋架/衣帽架</option><option value="324" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;储物家具</option><option value="321" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;客厅家具</option><option value="325" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;阳台/户外</option><option value="311" >&nbsp;&nbsp;&nbsp;&nbsp;生活日用</option><option value="354" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;洗晒/熨烫</option><option value="351" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;雨伞雨具</option><option value="355" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;净化除味</option><option value="352" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;浴室用品</option><option value="353" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;缝纫/针织用品</option><option value="350" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;收纳用品</option><option value="306" >&nbsp;&nbsp;&nbsp;&nbsp;家纺</option><option value="319" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;窗帘/窗纱</option><option value="316" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;床单被罩</option><option value="313" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;被子</option><option value="317" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;毛巾浴巾</option><option value="314" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;蚊帐</option><option value="318" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;床垫/床褥</option><option value="315" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;凉席</option><option value="312" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;床品套件</option><option value="308" >&nbsp;&nbsp;&nbsp;&nbsp;厨具</option><option value="332" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;餐具</option><option value="329" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;刀剪菜板</option><option value="333" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;茶具/咖啡具</option><option value="330" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;厨房配件</option><option value="331" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;水具酒具</option><option value="328" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;烹饪锅具</option><option value="309" >&nbsp;&nbsp;&nbsp;&nbsp;灯具</option><option value="338" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;落地灯</option><option value="335" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;吸顶灯</option><option value="339" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;应急灯/手电</option><option value="336" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;筒灯射灯</option><option value="340" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;装饰灯</option><option value="337" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;LED灯</option><option value="334" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;台灯</option><option value="341" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;吊灯</option><option value="2" >男装、女装、内衣、珠宝</option><option value="57" >&nbsp;&nbsp;&nbsp;&nbsp;男装馆</option><option value="83" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;休闲短裤</option><option value="80" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;风衣</option><option value="77" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;休闲裤</option><option value="84" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;POLO衫</option><option value="81" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;针织衫</option><option value="78" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;牛仔裤</option><option value="82" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;长袖衬衫</option><option value="79" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;夹克</option><option value="55" >&nbsp;&nbsp;&nbsp;&nbsp;女装馆</option><option value="67" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;短外套</option><option value="64" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;时尚套装</option><option value="61" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;连衣裙</option><option value="68" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;防晒衫</option><option value="65" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;复古旗袍</option><option value="62" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;连体裤</option><option value="66" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;牛仔裤</option><option value="63" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;棉麻T恤</option><option value="58" >&nbsp;&nbsp;&nbsp;&nbsp;户外鞋服</option><option value="89" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;迷彩裤</option><option value="86" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;篮球鞋</option><option value="90" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;沙滩鞋</option><option value="87" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;帆布鞋</option><option value="91" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;钓鱼服</option><option value="88" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;羽毛球鞋</option><option value="85" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;跑步鞋</option><option value="92" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;登山鞋</option><option value="59" >&nbsp;&nbsp;&nbsp;&nbsp;女鞋馆</option><option value="99" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;乐福鞋</option><option value="96" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;坡跟单鞋</option><option value="93" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;高跟凉拖</option><option value="100" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;松糕鞋</option><option value="97" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;浅口单鞋</option><option value="94" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;平底鞋</option><option value="98" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;帆布鞋</option><option value="95" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;高跟鞋</option><option value="56" >&nbsp;&nbsp;&nbsp;&nbsp;内衣馆</option><option value="73" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;男士内裤</option><option value="70" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;薄款文胸</option><option value="74" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;夏季睡衣</option><option value="71" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;无钢圈文胸</option><option value="75" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;性感睡衣</option><option value="72" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;女士内裤</option><option value="69" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;聚拢文胸</option><option value="76" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;瘦腿袜</option><option value="60" >&nbsp;&nbsp;&nbsp;&nbsp;箱包馆</option><option value="105" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;男士钱包</option><option value="102" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;手提女包</option><option value="106" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;旅行箱</option><option value="103" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;女士钱包</option><option value="107" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;拉杆箱</option><option value="104" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;男士双肩</option><option value="101" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;单肩女包</option><option value="108" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;拉杆包</option><option value="3" >个护化妆、清洁用品</option><option value="113" >&nbsp;&nbsp;&nbsp;&nbsp;香水彩妆</option><option value="137" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;底妆</option><option value="141" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;美甲</option><option value="138" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;腮红</option><option value="142" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;美容工具</option><option value="139" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;眼部</option><option value="136" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;香水</option><option value="143" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;套装</option><option value="140" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;唇部</option><option value="112" >&nbsp;&nbsp;&nbsp;&nbsp;口腔护理</option><option value="134" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;漱口水</option><option value="135" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;套装</option><option value="132" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;牙膏/牙粉</option><option value="133" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;牙刷/牙线</option><option value="109" >&nbsp;&nbsp;&nbsp;&nbsp;面部护肤</option><option value="118" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;套装</option><option value="115" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;护肤</option><option value="116" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;面膜</option><option value="117" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;剃须</option><option value="114" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;清洁</option><option value="110" >&nbsp;&nbsp;&nbsp;&nbsp;洗发护发</option><option value="121" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;染发</option><option value="122" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;造型</option><option value="119" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;洗发</option><option value="123" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;假发</option><option value="120" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;护发</option><option value="124" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;套装</option><option value="111" >&nbsp;&nbsp;&nbsp;&nbsp;身体护肤</option><option value="131" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;套装</option><option value="128" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;手足</option><option value="125" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;沐浴</option><option value="129" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;纤体塑形</option><option value="126" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;润肤</option><option value="130" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 美胸</option><option value="127" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;颈部</option><option value="7" >酒类饮料</option><option value="273" >&nbsp;&nbsp;&nbsp;&nbsp;饮料饮品</option><option value="289" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;果蔬汁</option><option value="296" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;植物蛋白饮料</option><option value="293" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;纯牛奶</option><option value="290" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;茶饮料</option><option value="294" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;酸奶</option><option value="291" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;碳酸饮料</option><option value="295" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;风味奶</option><option value="292" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;功能饮料</option><option value="274" >&nbsp;&nbsp;&nbsp;&nbsp;茗茶</option><option value="305" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;其他茶</option><option value="302" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;红茶</option><option value="299" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;普洱</option><option value="303" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;花果茶</option><option value="300" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;龙井</option><option value="304" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;养生茶</option><option value="301" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;绿茶</option><option value="298" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;铁观音</option><option value="271" >&nbsp;&nbsp;&nbsp;&nbsp;酒水</option><option value="280" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;养生酒</option><option value="277" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;啤酒</option><option value="281" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;预调酒</option><option value="278" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;葡萄酒/果酒</option><option value="279" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;黄酒/米酒</option><option value="276" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;白酒</option><option value="272" >&nbsp;&nbsp;&nbsp;&nbsp;冲调饮品</option><option value="286" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;奶茶</option><option value="283" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;成人奶粉</option><option value="287" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;麦片谷物</option><option value="284" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;豆浆/豆奶粉</option><option value="297" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;果味冲调</option><option value="288" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;咖啡</option><option value="285" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;茶叶</option><option value="282" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;蜂蜜</option><option value="359" >鞋靴、箱包、钟表、奢侈品</option><option value="360" >运动户外</option><option value="361" >汽车、汽车用品</option><option value="8" >母婴、玩具乐器</option><option value="225" >&nbsp;&nbsp;&nbsp;&nbsp;车床/床品</option><option value="257" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;婴儿床</option><option value="261" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;睡袋/抱被</option><option value="258" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;餐椅</option><option value="255" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;安全座椅</option><option value="262" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;凉席/蚊帐</option><option value="259" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;三轮车</option><option value="256" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;手推车</option><option value="260" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;儿童家具</option><option value="222" >&nbsp;&nbsp;&nbsp;&nbsp;营养/辅食</option><option value="238" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;清火开胃</option><option value="235" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;面食类</option><option value="239" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;钙铁锌</option><option value="236" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;宝宝零食</option><option value="233" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;米粉</option><option value="240" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;益生菌</option><option value="237" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;DHA</option><option value="234" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;果汁/泥</option><option value="226" >&nbsp;&nbsp;&nbsp;&nbsp;孕妈专区</option><option value="270" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;孕妇内裤</option><option value="267" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;妈咪包</option><option value="264" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;打底裤</option><option value="268" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;收腹带</option><option value="265" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;防辐射服</option><option value="269" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;哺乳文胸</option><option value="266" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;腰凳</option><option value="263" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;孕妇裙</option><option value="223" >&nbsp;&nbsp;&nbsp;&nbsp;孕婴洗护</option><option value="241" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;洗衣液/皂</option><option value="245" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;爽身粉</option><option value="242" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;宝宝沐浴</option><option value="246" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;奶瓶清洗</option><option value="243" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;儿童防晒</option><option value="247" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;孕妇护肤</option><option value="244" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;防蚊/驱蚊</option><option value="224" >&nbsp;&nbsp;&nbsp;&nbsp;喂养用品</option><option value="254" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;防溢乳垫</option><option value="251" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;水壶/水杯</option><option value="248" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;奶嘴奶瓶</option><option value="252" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;吸奶器</option><option value="249" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;驱蚊用品</option><option value="253" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;理发器</option><option value="250" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;浴室用品</option><option value="221" >&nbsp;&nbsp;&nbsp;&nbsp;孕婴奶粉</option><option value="232" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;3段</option><option value="229" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;pre段</option><option value="230" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;1段</option><option value="227" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;特配奶粉</option><option value="231" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;2段</option><option value="228" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;孕妈奶粉</option><option value="1" >食品、酒类、生鲜、特产</option><option value="14" >&nbsp;&nbsp;&nbsp;&nbsp;进口水果</option><option value="20" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;奇异果猕猴桃</option><option value="17" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;芒果桃李</option><option value="21" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;凤梨蓝莓</option><option value="18" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;龙眼荔枝</option><option value="15" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;释迦芭乐</option><option value="22" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;榴莲山竹</option><option value="19" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;提子葡萄</option><option value="16" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;樱桃车厘子</option><option value="13" >&nbsp;&nbsp;&nbsp;&nbsp;糖果巧克力</option><option value="24" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;巧克力</option><option value="28" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;奶糖</option><option value="25" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;口香糖</option><option value="29" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;QQ糖</option><option value="26" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;棒棒糖</option><option value="30" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;果冻</option><option value="27" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;软糖</option><option value="9" >&nbsp;&nbsp;&nbsp;&nbsp;牛奶乳品</option><option value="37" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;全脂奶</option><option value="34" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;酸奶</option><option value="31" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;常温奶</option><option value="38" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;成人奶粉</option><option value="35" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;豆奶</option><option value="32" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;乳饮料</option><option value="36" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;低脂奶</option><option value="33" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;儿童奶</option><option value="10" >&nbsp;&nbsp;&nbsp;&nbsp;坚果炒货</option><option value="40" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;夏威夷果</option><option value="44" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;瓜子</option><option value="41" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;碧根果</option><option value="45" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;花生</option><option value="42" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;开心果</option><option value="39" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;核桃</option><option value="46" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;杏仁</option><option value="43" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;腰果</option><option value="12" >&nbsp;&nbsp;&nbsp;&nbsp;蜜饯果干</option><option value="53" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;橄榄</option><option value="50" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;芒果干</option><option value="47" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;红枣</option><option value="54" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;其他</option><option value="51" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;香蕉干</option><option value="48" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;莓类</option><option value="52" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;山楂片</option><option value="49" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;葡萄干</option><option value="362" >营养保健</option><option value="363" >图书、音像、电子书</option><option value="364" >彩票、旅行、充值、票务</option><option value="365" >理财、众筹、白条、保险</option>						</select>
						<select name="brand_id1">
							<option value="0">所有品牌<option value="54">缪诗</option><option value="68">格力</option><option value="69">老板</option><option value="70">西门子</option><option value="71">格兰仕</option><option value="72">海信</option><option value="73">伊莱克斯</option><option value="74">艾力斯特</option><option value="75">博洋家纺</option><option value="76">富安娜</option><option value="77">爱仕达</option><option value="78">罗莱</option><option value="67">美的</option><option value="66">海尔</option><option value="55">卓诗尼</option><option value="56">七匹狼</option><option value="57">佐丹奴</option><option value="58">达芙妮</option><option value="59">她他/tata</option><option value="60">曼妮芬（ManniForm）</option><option value="61">伊芙丽</option><option value="62">稻草人</option><option value="63">斯提亚</option><option value="64">袋鼠</option><option value="65">爱华仕</option><option value="79">安睡宝</option><option value="80">溢彩年华</option><option value="94">王老吉</option><option value="95">可口可乐</option><option value="96">贝古贝古</option><option value="97">皇家宝贝</option><option value="98">呵宝童车</option><option value="99">合生元</option><option value="100">美赞臣</option><option value="101">帮宝适</option><option value="102">抱抱熊</option><option value="103">巴拉巴拉</option><option value="104">青蛙王子</option><option value="93">统一</option><option value="92">加多宝</option><option value="81">慧乐家</option><option value="82">天堂伞</option><option value="83">水星家纺</option><option value="84">全有家居</option><option value="85">五粮液</option><option value="86">泸州老窖</option><option value="87">洋河</option><option value="88">郎酒</option><option value="89">锐澳</option><option value="90">雪花</option><option value="91">哈尔滨</option><option value="105">雀氏</option><option value="1">资生堂</option><option value="15">韩束</option><option value="16">卡姿兰</option><option value="17">珀莱雅</option><option value="18">兰芝</option><option value="19">碧欧泉</option><option value="20">小米</option><option value="21">摩托罗拉</option><option value="22">中兴</option><option value="23">朵唯</option><option value="24">htc</option><option value="25">华为</option><option value="14">高丝</option><option value="13">SK-ll</option><option value="2">CK</option><option value="3">Disney</option><option value="4">雅诗兰黛</option><option value="5">相宜本草</option><option value="6">Dior</option><option value="7">爱丽</option><option value="8">雅顿</option><option value="9">狮王</option><option value="10">高丝洁</option><option value="11">MISS FACE</option><option value="12">姬芮</option><option value="26">oppo</option><option value="27">金立</option><option value="42">君乐宝</option><option value="43">光明</option><option value="44">三元</option><option value="45">百草味</option><option value="46">三只松鼠</option><option value="47">口水娃</option><option value="48">楼兰密语</option><option value="49">西域美农</option><option value="50">糖糖屋</option><option value="51">享爱.</option><option value="52">猫人</option><option value="40">蒙牛</option><option value="39">海底捞</option><option value="28">LG</option><option value="29">苹果</option><option value="30">三星</option><option value="31">乐檬</option><option value="32">努比亚</option><option value="41">伊利</option><option value="34">肯德基</option><option value="35">麦当劳</option><option value="36">小肥羊</option><option value="37">小尾羊</option><option value="38">必胜客</option><option value="53">茵曼（INMAN）</option>						</select>
						<input type="text" name="keyword1" />
						<input type="button" value=" 搜索 " class="button" onclick="searchGoods(sz1, 'cat_id1','brand_id1','keyword1')" />
					</td>
				</tr>
        		<!-- 商品列表 -->
				<tr>
					<th>可选商品</th>
					<th>操作</th>
					<th>跟该商品关联的商品</th>
				</tr>
				<tr>
					<td width="42%">
						<select name="source_select1" size="20" style="width:100%" ondblclick="sz1.addItem(false, 'add_link_goods', goodsId, this.form.elements['is_single'][0].checked)" multiple="true">
						</select>
					</td>
					<td align="center">
						<p>
							<input name="is_single" type="radio" value="1" checked="checked" />
							单向关联							<br />
							<input name="is_single" type="radio" value="0" />
							双向关联						</p>
						<p>
							<input type="button" value=">>" onclick="sz1.addItem(true, 'add_link_goods', goodsId, this.form.elements['is_single'][0].checked)" class="button" />
						</p>
						<p>
							<input type="button" value=">" onclick="sz1.addItem(false, 'add_link_goods', goodsId, this.form.elements['is_single'][0].checked)" class="button" />
						</p>
						<p>
							<input type="button" value="<" onclick="sz1.dropItem(false, 'drop_link_goods', goodsId, elements['is_single'][0].checked)" class="button" />
						</p>
						<p>
							<input type="button" value="<<" onclick="sz1.dropItem(true, 'drop_link_goods', goodsId, elements['is_single'][0].checked)" class="button" />
						</p>
					</td>
					<td width="42%">
						<select name="target_select1" size="20" style="width:100%" multiple ondblclick="sz1.dropItem(false, 'drop_link_goods', goodsId, elements['is_single'][0].checked)">

							
						</select>
					</td>
				</tr>
			</table>

      		<!-- 配件 -->
			<table width="90%" id="groupgoods-table" style="display:none" align="center">
        		<!-- 商品搜索 -->
				<tr>
					<td colspan="3">
						<img src="images/icon_search.gif" width="26" height="22" border="0" alt="SEARCH" />
						<select name="cat_id2">
							<option value="0">所有分类<option value="5" >家用电器</option><option value="182" selected='ture'>&nbsp;&nbsp;&nbsp;&nbsp;五金家装</option><option value="218" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;家具五金</option><option value="219" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;电工电料</option><option value="216" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;手动工具</option><option value="220" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;监控安防</option><option value="217" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;厨卫五金</option><option value="215" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;电动工具</option><option value="179" >&nbsp;&nbsp;&nbsp;&nbsp;生活电器</option><option value="193" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;加湿器</option><option value="197" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;饮水机</option><option value="194" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;吸尘器</option><option value="198" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;其它生活电器</option><option value="195" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;挂烫机/熨斗</option><option value="192" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;净化器</option><option value="191" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;电风扇</option><option value="196" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;取暖电器</option><option value="180" >&nbsp;&nbsp;&nbsp;&nbsp;厨房电器</option><option value="206" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;其它厨房电器</option><option value="203" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;电炖锅</option><option value="200" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;微波炉</option><option value="204" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;果蔬解毒机</option><option value="201" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;电磁炉</option><option value="205" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;养生壶/煎药壶</option><option value="202" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;电饼铛/烧烤盘</option><option value="199" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;电饭煲</option><option value="181" >&nbsp;&nbsp;&nbsp;&nbsp;个护健康</option><option value="209" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;按摩椅</option><option value="213" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;计步器/脂肪检测</option><option value="210" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;足浴盆</option><option value="207" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;剃须刀</option><option value="214" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;其它健康电器</option><option value="211" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;健康秤/厨房秤</option><option value="208" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;电吹风</option><option value="212" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;血糖仪</option><option value="178" >&nbsp;&nbsp;&nbsp;&nbsp;大家电</option><option value="185" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;洗衣机</option><option value="189" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;消毒柜/洗碗机</option><option value="186" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;家庭影院</option><option value="183" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;平板电视</option><option value="190" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;冷柜/冰吧</option><option value="187" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;烟机/灶具</option><option value="184" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;空调冰箱</option><option value="188" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;热水器</option><option value="4" >手机、数码、通信</option><option value="147" >&nbsp;&nbsp;&nbsp;&nbsp;智能设备</option><option value="169" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;体感车</option><option value="166" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;智能眼镜</option><option value="167" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;运动跟踪器</option><option value="164" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;智能手环</option><option value="168" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;智能家居</option><option value="165" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;智能手表</option><option value="144" >&nbsp;&nbsp;&nbsp;&nbsp;热卖手机</option><option value="153" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;联通4G</option><option value="150" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;小米特供</option><option value="154" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;电信4G</option><option value="151" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;魅族手机</option><option value="148" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;三星盖乐世</option><option value="155" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;移动4G</option><option value="152" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;华为荣耀</option><option value="149" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;iPhone</option><option value="145" >&nbsp;&nbsp;&nbsp;&nbsp;手机配件</option><option value="163" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;保护套</option><option value="160" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;创意配件</option><option value="157" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;移动电源</option><option value="161" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;手机饰品</option><option value="158" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;蓝牙耳机</option><option value="162" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;手机耳机</option><option value="159" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;充电器</option><option value="156" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;电池</option><option value="146" >&nbsp;&nbsp;&nbsp;&nbsp;数码影音</option><option value="176" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;数码相框</option><option value="173" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;运动相机</option><option value="170" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;数码相机</option><option value="177" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;影棚器材</option><option value="174" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;摄像机</option><option value="171" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;单反相机</option><option value="175" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;户外器材</option><option value="172" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;拍立得</option><option value="358" >电脑、办公</option><option value="6" >家居、家具、家装、厨具</option><option value="310" >&nbsp;&nbsp;&nbsp;&nbsp;家装软饰</option><option value="348" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;墙贴/装饰贴</option><option value="345" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;帘艺隔断</option><option value="342" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;桌布/罩件</option><option value="349" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;摆件花瓶</option><option value="346" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;相框/照片墙</option><option value="343" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;地毯地垫</option><option value="347" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;装饰字画</option><option value="344" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;沙发垫套/椅垫</option><option value="307" >&nbsp;&nbsp;&nbsp;&nbsp;家具</option><option value="322" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;餐厅家具</option><option value="326" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;沙发</option><option value="323" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;书房家具</option><option value="320" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;卧室家具</option><option value="327" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;鞋架/衣帽架</option><option value="324" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;储物家具</option><option value="321" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;客厅家具</option><option value="325" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;阳台/户外</option><option value="311" >&nbsp;&nbsp;&nbsp;&nbsp;生活日用</option><option value="354" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;洗晒/熨烫</option><option value="351" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;雨伞雨具</option><option value="355" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;净化除味</option><option value="352" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;浴室用品</option><option value="353" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;缝纫/针织用品</option><option value="350" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;收纳用品</option><option value="306" >&nbsp;&nbsp;&nbsp;&nbsp;家纺</option><option value="319" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;窗帘/窗纱</option><option value="316" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;床单被罩</option><option value="313" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;被子</option><option value="317" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;毛巾浴巾</option><option value="314" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;蚊帐</option><option value="318" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;床垫/床褥</option><option value="315" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;凉席</option><option value="312" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;床品套件</option><option value="308" >&nbsp;&nbsp;&nbsp;&nbsp;厨具</option><option value="332" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;餐具</option><option value="329" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;刀剪菜板</option><option value="333" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;茶具/咖啡具</option><option value="330" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;厨房配件</option><option value="331" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;水具酒具</option><option value="328" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;烹饪锅具</option><option value="309" >&nbsp;&nbsp;&nbsp;&nbsp;灯具</option><option value="338" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;落地灯</option><option value="335" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;吸顶灯</option><option value="339" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;应急灯/手电</option><option value="336" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;筒灯射灯</option><option value="340" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;装饰灯</option><option value="337" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;LED灯</option><option value="334" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;台灯</option><option value="341" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;吊灯</option><option value="2" >男装、女装、内衣、珠宝</option><option value="57" >&nbsp;&nbsp;&nbsp;&nbsp;男装馆</option><option value="83" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;休闲短裤</option><option value="80" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;风衣</option><option value="77" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;休闲裤</option><option value="84" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;POLO衫</option><option value="81" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;针织衫</option><option value="78" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;牛仔裤</option><option value="82" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;长袖衬衫</option><option value="79" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;夹克</option><option value="55" >&nbsp;&nbsp;&nbsp;&nbsp;女装馆</option><option value="67" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;短外套</option><option value="64" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;时尚套装</option><option value="61" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;连衣裙</option><option value="68" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;防晒衫</option><option value="65" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;复古旗袍</option><option value="62" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;连体裤</option><option value="66" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;牛仔裤</option><option value="63" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;棉麻T恤</option><option value="58" >&nbsp;&nbsp;&nbsp;&nbsp;户外鞋服</option><option value="89" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;迷彩裤</option><option value="86" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;篮球鞋</option><option value="90" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;沙滩鞋</option><option value="87" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;帆布鞋</option><option value="91" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;钓鱼服</option><option value="88" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;羽毛球鞋</option><option value="85" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;跑步鞋</option><option value="92" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;登山鞋</option><option value="59" >&nbsp;&nbsp;&nbsp;&nbsp;女鞋馆</option><option value="99" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;乐福鞋</option><option value="96" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;坡跟单鞋</option><option value="93" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;高跟凉拖</option><option value="100" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;松糕鞋</option><option value="97" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;浅口单鞋</option><option value="94" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;平底鞋</option><option value="98" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;帆布鞋</option><option value="95" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;高跟鞋</option><option value="56" >&nbsp;&nbsp;&nbsp;&nbsp;内衣馆</option><option value="73" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;男士内裤</option><option value="70" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;薄款文胸</option><option value="74" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;夏季睡衣</option><option value="71" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;无钢圈文胸</option><option value="75" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;性感睡衣</option><option value="72" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;女士内裤</option><option value="69" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;聚拢文胸</option><option value="76" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;瘦腿袜</option><option value="60" >&nbsp;&nbsp;&nbsp;&nbsp;箱包馆</option><option value="105" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;男士钱包</option><option value="102" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;手提女包</option><option value="106" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;旅行箱</option><option value="103" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;女士钱包</option><option value="107" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;拉杆箱</option><option value="104" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;男士双肩</option><option value="101" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;单肩女包</option><option value="108" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;拉杆包</option><option value="3" >个护化妆、清洁用品</option><option value="113" >&nbsp;&nbsp;&nbsp;&nbsp;香水彩妆</option><option value="137" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;底妆</option><option value="141" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;美甲</option><option value="138" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;腮红</option><option value="142" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;美容工具</option><option value="139" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;眼部</option><option value="136" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;香水</option><option value="143" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;套装</option><option value="140" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;唇部</option><option value="112" >&nbsp;&nbsp;&nbsp;&nbsp;口腔护理</option><option value="134" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;漱口水</option><option value="135" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;套装</option><option value="132" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;牙膏/牙粉</option><option value="133" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;牙刷/牙线</option><option value="109" >&nbsp;&nbsp;&nbsp;&nbsp;面部护肤</option><option value="118" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;套装</option><option value="115" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;护肤</option><option value="116" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;面膜</option><option value="117" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;剃须</option><option value="114" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;清洁</option><option value="110" >&nbsp;&nbsp;&nbsp;&nbsp;洗发护发</option><option value="121" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;染发</option><option value="122" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;造型</option><option value="119" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;洗发</option><option value="123" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;假发</option><option value="120" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;护发</option><option value="124" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;套装</option><option value="111" >&nbsp;&nbsp;&nbsp;&nbsp;身体护肤</option><option value="131" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;套装</option><option value="128" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;手足</option><option value="125" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;沐浴</option><option value="129" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;纤体塑形</option><option value="126" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;润肤</option><option value="130" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 美胸</option><option value="127" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;颈部</option><option value="7" >酒类饮料</option><option value="273" >&nbsp;&nbsp;&nbsp;&nbsp;饮料饮品</option><option value="289" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;果蔬汁</option><option value="296" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;植物蛋白饮料</option><option value="293" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;纯牛奶</option><option value="290" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;茶饮料</option><option value="294" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;酸奶</option><option value="291" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;碳酸饮料</option><option value="295" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;风味奶</option><option value="292" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;功能饮料</option><option value="274" >&nbsp;&nbsp;&nbsp;&nbsp;茗茶</option><option value="305" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;其他茶</option><option value="302" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;红茶</option><option value="299" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;普洱</option><option value="303" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;花果茶</option><option value="300" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;龙井</option><option value="304" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;养生茶</option><option value="301" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;绿茶</option><option value="298" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;铁观音</option><option value="271" >&nbsp;&nbsp;&nbsp;&nbsp;酒水</option><option value="280" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;养生酒</option><option value="277" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;啤酒</option><option value="281" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;预调酒</option><option value="278" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;葡萄酒/果酒</option><option value="279" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;黄酒/米酒</option><option value="276" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;白酒</option><option value="272" >&nbsp;&nbsp;&nbsp;&nbsp;冲调饮品</option><option value="286" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;奶茶</option><option value="283" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;成人奶粉</option><option value="287" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;麦片谷物</option><option value="284" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;豆浆/豆奶粉</option><option value="297" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;果味冲调</option><option value="288" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;咖啡</option><option value="285" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;茶叶</option><option value="282" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;蜂蜜</option><option value="359" >鞋靴、箱包、钟表、奢侈品</option><option value="360" >运动户外</option><option value="361" >汽车、汽车用品</option><option value="8" >母婴、玩具乐器</option><option value="225" >&nbsp;&nbsp;&nbsp;&nbsp;车床/床品</option><option value="257" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;婴儿床</option><option value="261" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;睡袋/抱被</option><option value="258" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;餐椅</option><option value="255" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;安全座椅</option><option value="262" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;凉席/蚊帐</option><option value="259" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;三轮车</option><option value="256" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;手推车</option><option value="260" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;儿童家具</option><option value="222" >&nbsp;&nbsp;&nbsp;&nbsp;营养/辅食</option><option value="238" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;清火开胃</option><option value="235" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;面食类</option><option value="239" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;钙铁锌</option><option value="236" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;宝宝零食</option><option value="233" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;米粉</option><option value="240" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;益生菌</option><option value="237" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;DHA</option><option value="234" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;果汁/泥</option><option value="226" >&nbsp;&nbsp;&nbsp;&nbsp;孕妈专区</option><option value="270" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;孕妇内裤</option><option value="267" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;妈咪包</option><option value="264" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;打底裤</option><option value="268" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;收腹带</option><option value="265" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;防辐射服</option><option value="269" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;哺乳文胸</option><option value="266" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;腰凳</option><option value="263" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;孕妇裙</option><option value="223" >&nbsp;&nbsp;&nbsp;&nbsp;孕婴洗护</option><option value="241" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;洗衣液/皂</option><option value="245" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsnbsp;爽身粉</option><option value="242" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;宝宝沐浴</option><option value="246" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;奶瓶清洗</option><option value="243" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;儿童防晒</option><option value="247" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;孕妇护肤</option><option value="244" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;防蚊/驱蚊</option><option value="224" >&nbsp;&nbsp;&nbsp;&nbsp;喂养用品</option><option value="254" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;防溢乳垫</option><option value="251" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;水壶/水杯</option><option value="248" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;奶嘴奶瓶</option><option value="252" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;吸奶器</option><option value="249" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;驱蚊用品</option><option value="253" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;理发器</option><option value="250" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;浴室用品</option><option value="221" >&nbsp;&nbsp;&nbsp;&nbsp;孕婴奶粉</option><option value="232" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;3段</option><option value="229" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;pre段</option><option value="230" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;1段</option><option value="227" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;特配奶粉</option><option value="231" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;2段</option><option value="228" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;孕妈奶粉</option><option value="1" >食品、酒类、生鲜、特产</option><option value="14" >&nbsp;&nbsp;&nbsp;&nbsp;进口水果</option><option value="20" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;奇异果猕猴桃</option><option value="17" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;芒果桃李</option><option value="21" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;凤梨蓝莓</option><option value="18" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;龙眼荔枝</option><option value="15" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;释迦芭乐</option><option value="22" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;榴莲山竹</option><option value="19" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;提子葡萄</option><option value="16" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;樱桃车厘子</option><option value="13" >&nbsp;&nbsp;&nbsp;&nbsp;糖果巧克力</option><option value="24" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;巧克力</option><option value="28" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;奶糖</option><option value="25" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;口香糖</option><option value="29" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;QQ糖</option><option value="26" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;棒棒糖</option><option value="30" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;果冻</option><option value="27" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;软糖</option><option value="9" >&nbsp;&nbsp;&nbsp;&nbsp;牛奶乳品</option><option value="37" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;全脂奶</option><option value="34" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;酸奶</option><option value="31" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;常温奶</option><option value="38" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;成人奶粉</option><option value="35" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;豆奶</option><option value="32" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;乳饮料</option><option value="36" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;低脂奶</option><option value="33" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;儿童奶</option><option value="10" >&nbsp;&nbsp;&nbsp;&nbsp;坚果炒货</option><option value="40" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;夏威夷果</option><option value="44" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;瓜子</option><option value="41" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;碧根果</option><option value="45" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;花生</option><option value="42" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;开心果</option><option value="39" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;核桃</option><option value="46" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;杏仁</option><option value="43" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;腰果</option><option value="12" >&nbsp;&nbsp;&nbsp;&nbsp;蜜饯果干</option><option value="53" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;橄榄</option><option value="50" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;芒果干</option><option value="47" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;红枣</option><option value="54" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;其他</option><option value="51" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;香蕉干</option><option value="48" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;莓类</option><option value="52" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;山楂片</option><option value="49" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;葡萄干</option><option value="362" >营养保健</option><option value="363" >图书、音像、电子书</option><option value="364" >彩票、旅行、充值、票务</option><option value="365" >理财、众筹、白条、保险</option>						</select>
						<select name="brand_id2">
							<option value="0">所有品牌<option value="54">缪诗</option><option value="68">格力</option><option value="69">老板</option><option value="70">西门子</option><option value="71">格兰仕</option><option value="72">海信</option><option value="73">伊莱克斯</option><option value="74">艾力斯特</option><option value="75">博洋家纺</option><option value="76">富安娜</option><option value="77">爱仕达</option><option value="78">罗莱</option><option value="67">美的</option><option value="66">海尔</option><option value="55">卓诗尼</option><option value="56">七匹狼</option><option value="57">佐丹奴</option><option value="58">达芙妮</option><option value="59">她他/tata</option><option value="60">曼妮芬（ManniForm）</option><option value="61">伊芙丽</option><option value="62">稻草人</option><option value="63">斯提亚</option><option value="64">袋鼠</option><option value="65">爱华仕</option><option value="79">安睡宝</option><option value="80">溢彩年华</option><option value="94">王老吉</option><option value="95">可口可乐</option><option value="96">贝古贝古</option><option value="97">皇家宝贝</option><option value="98">呵宝童车</option><option value="99">合生元</option><option value="100">美赞臣</option><option value="101">帮宝适</option><option value="102">抱抱熊</option><option value="103">巴拉巴拉</option><option value="104">青蛙王子</option><option value="93">统一</option><option value="92">加多宝</option><option value="81">慧乐家</option><option value="82">天堂伞</option><option value="83">水星家纺</option><option value="84">全有家居</option><option value="85">五粮液</option><option value="86">泸州老窖</option><option value="87">洋河</option><option value="88">郎酒</option><option value="89">锐澳</option><option value="90">雪花</option><option value="91">哈尔滨</option><option value="105">雀氏</option><option value="1">资生堂</option><option value="15">韩束</option><option value="16">卡姿兰</option><option value="17">珀莱雅</option><option value="18">兰芝</option><option value="19">碧欧泉</option><option value="20">小米</option><option value="21">摩托罗拉</option><option value="22">中兴</option><option value="23">朵唯</option><option value="24">htc</option><option value="25">华为</option><option value="14">高丝</option><option value="13">SK-ll</option><option value="2">CK</option><option value="3">Disney</option><option value="4">雅诗兰黛</option><option value="5">相宜本草</option><option value="6">Dior</option><option value="7">爱丽</option><option value="8">雅顿</option><option value="9">狮王</option><option value="10">高丝洁</option><option value="11">MISS FACE</option><option value="12">姬芮</option><option value="26">oppo</option><option value="27">金立</option><option value="42">君乐宝</option><option value="43">光明</option><option value="44">三元</option><option value="45">百草味</option><option value="46">三只松鼠</option><option value="47">口水娃</option><option value="48">楼兰密语</option><option value="49">西域美农</option><option value="50">糖糖屋</option><option value="51">享爱.</option><option value="52">猫人</option><option value="40">蒙牛</option><option value="39">海底捞</option><option value="28">LG</option><option value="29">苹果</option><option value="30">三星</option><option value="31">乐檬</option><option value="32">努比亚</option><option value="41">伊利</option><option value="34">肯德基</option><option value="35">麦当劳</option><option value="36">小肥羊</option><option value="37">小尾羊</option><option value="38">必胜客</option><option value="53">茵曼（INMAN）</option>						</select>
						<input type="text" name="keyword2" />
						<input type="button" value=" 搜索 " onclick="searchGoods(sz2, 'cat_id2', 'brand_id2', 'keyword2')" class="button" />
					</td>
				</tr>
        <!-- 商品列表 -->
				<tr>
					<th>可选商品</th>
					<th>操作</th>
					<th>该商品的配件</th>
				</tr>
				<tr>
					<td width="42%">
						<select name="source_select2" size="20" style="width:100%" onchange="sz2.priceObj.value = this.options[this.selectedIndex].id" ondblclick="sz2.addItem(false, 'add_group_goods', goodsId, this.form.elements['price2'].value)">
						</select>
					</td>
					<td align="center">
						<p>
							价格							<br />
							<input onkeyup="this.value=this.value.replace(/[^0-9.]/g,'')" name="price2" type="text" size="6" style="min-width:40px;width:40px;"/>
						</p>
						<p>
							<input type="button" value=">" onclick="sz2.addItem(false, 'add_group_goods', goodsId, this.form.elements['price2'].value)" class="button" />
						</p>
						<p>
							<input type="button" value="<" onclick="sz2.dropItem(false, 'drop_group_goods', goodsId, elements['is_single'][0].checked)" class="button" />
						</p>
						<p>
							<input type="button" value="<<" onclick="sz2.dropItem(true, 'drop_group_goods', goodsId, elements['is_single'][0].checked)" class="button" />
						</p>
					</td>
					<td width="42%">
						<select name="target_select2" size="20" style="width:100%" multiple ondblclick="sz2.dropItem(false, 'drop_group_goods', goodsId, elements['is_single'][0].checked)">

							
						</select>
					</td>
				</tr>
			</table>

			<!-- 鍏宠仈鏂囩珷 -->
			<table width="90%" id="article-table" style="display:none" align="center">
				<!-- 鏂囩珷鎼滅储 -->
				<tr>
					<td colspan="3">
						<img src="images/icon_search.gif" width="26" height="22" border="0" alt="SEARCH" />
						文章标题						<input type="text" name="article_title" />
						<input type="button" value=" 搜索 " onclick="searchArticle()" class="button" />
					</td>
				</tr>
				<!-- 鏂囩珷鍒楄〃 -->
				<tr>
					<th>可选文章</th>
					<th>操作</th>
					<th>跟该商品关联的文章</th>
				</tr>
				<tr>
					<td width="45%">
						<select name="source_select3" size="20" style="width:100%" multiple ondblclick="sz3.addItem(false, 'add_goods_article', goodsId, this.form.elements['price2'].value)">
						</select>
					</td>
					<td align="center">
						<p>
							<input type="button" value=">>" onclick="sz3.addItem(true, 'add_goods_article', goodsId, this.form.elements['price2'].value)" class="button" />
						</p>
						<p>
							<input type="button" value=">" onclick="sz3.addItem(false, 'add_goods_article', goodsId, this.form.elements['price2'].value)" class="button" />
						</p>
						<p>
							<input type="button" value="<" onclick="sz3.dropItem(false, 'drop_goods_article', goodsId, elements['is_single'][0].checked)" class="button" />
						</p>
						<p>
							<input type="button" value="<<" onclick="sz3.dropItem(true, 'drop_goods_article', goodsId, elements['is_single'][0].checked)" class="button" />
						</p>
					</td>
					<td width="45%">
						<select name="target_select3" size="20" style="width:100%" multiple ondblclick="sz3.dropItem(false, 'drop_goods_article', goodsId, elements['is_single'][0].checked)">

							
						</select>
					</td>
				</tr>
			</table>
			<div class="button-div">
				<input type="hidden" name="goods_id" value="283" />
				
				<!--修改代码_start By www.wygk.cn 主要是给这两个INPUT各自增加了一个ID， id="goods_info_submit"  id="goods_info_reset" -->
				<input id="goods_info_submit" type="submit" value=" 确定 " class="button" />
				<input id="goods_info_reset" type="reset" value=" 重置 " class="button" />
				<!--修改代码_end By www.wygk.cn-->

			</div>
			<input type="hidden" name="act" value="update" />
		</form>
	</div>
</div>
<!-- end goods form -->
<script type="text/javascript" src="js/validator.js"></script><script type="text/javascript" src="js/tab.js"></script><script language="JavaScript">
  var goodsId = '283';
  var elements = document.forms['theForm'].elements;
  var sz1 = new SelectZone(1, elements['source_select1'], elements['target_select1']);
  var sz2 = new SelectZone(2, elements['source_select2'], elements['target_select2'], elements['price2']);
  var sz3 = new SelectZone(1, elements['source_select3'], elements['target_select3']);
  var marketPriceRate = 1.2;
  var integralPercent = 1;

  
  onload = function()
  {
      handlePromote(document.forms['theForm'].elements['is_promote'].checked);

      if (document.forms['theForm'].elements['auto_thumb'])
      {
          handleAutoThumb(document.forms['theForm'].elements['auto_thumb'].checked);
      }

      // 检查新订单
      startCheckOrder();
      
            set_price_note(1);
            set_price_note(2);
            set_price_note(3);
            set_price_note(4);
            
      document.forms['theForm'].reset();
  }

  function validate(goods_id)
  {
      var validator = new Validator('theForm');
      var goods_sn  = document.forms['theForm'].elements['goods_sn'].value;

      validator.required('goods_name', goods_name_not_null);
      if (document.forms['theForm'].elements['cat_id'].value == 0)
      {
          validator.addErrorMsg(goods_cat_not_null);
      }

      checkVolumeData("1",validator);

      validator.required('shop_price', shop_price_not_null);
      validator.isNumber('shop_price', shop_price_not_number, true);
      validator.isNumber('market_price', market_price_not_number, false);
      if (document.forms['theForm'].elements['is_promote'].checked)
      {
          validator.required('promote_start_date', promote_start_not_null);
          validator.required('promote_end_date', promote_end_not_null);
          validator.islt('promote_start_date', 'promote_end_date', promote_not_lt);
      }

      if (document.forms['theForm'].elements['goods_number'] != undefined)
      {
          validator.isInt('goods_number', goods_number_not_int, false);
          validator.isInt('warn_number', warn_number_not_int, false);
      }

      var callback = function(res)
     {
      if (res.error > 0)
      {
        alert("您输入的货号已存在，请换一个");
      }
      else
      {
         if(validator.passed())
         {
         document.forms['theForm'].submit();
         }
      }
  }
    Ajax.call('goods.php?is_ajax=1&act=check_goods_sn', "goods_sn=" + goods_sn + "&goods_id=" + goods_id, callback, "GET", "JSON");
}

  /**
   * 切换商品类型
   */
  function getAttrList(goodsId)
  {
      var selGoodsType = document.forms['theForm'].elements['goods_type'];

      if (selGoodsType != undefined)
      {
          var goodsType = selGoodsType.options[selGoodsType.selectedIndex].value;

          Ajax.call('goods.php?is_ajax=1&act=get_attr', 'goods_id=' + goodsId + "&goods_type=" + goodsType, setAttrList, "GET", "JSON");
      }
  }
function array_search_value(arrayinfo,value){
	for(i in arrayinfo){
		if(arrayinfo[i] == value){
			return false;
		}
	}
	return true;
}

  ///条形码选择传值

function getType(txm,id,value,good_id)
{
	
	var txm = txm;
	var cid = id;//所选属性的上级ID
	var val = value;//选中的值
	var goodid = good_id;//商品id
	var parm = new Array();
	var j = 0;
	$('.ctxm_'+txm).each(function(k,v){
		if(array_search_value(parm,v.value)){
			parm[j] = v.value;
			j++;
		}
	})
	
	var par_str = '';
	var parm_key = '';
	var parm_value = '';
	for(i in parm){
		parm_key = '&attr_'+parm[i]+'='; 
		parm_value = '';
		$('.attr_num_'+parm[i]).each(function(key,value){
			if(value.value !=''){
				parm_value += value.value+',';
			}
		})
		par_str += parm_key+parm_value;
	}
	
	Ajax.call('goods.php?is_ajax=1&act=get_txm', 'goods_id=' + goodid + "&id=" + id + par_str , chu, "GET", "JSON");
	
	return;
}
/*条形码数据返回*/
function chu (result)
{
	var opanel = document.getElementById("input_txm");
	var zhi = result.content;
	opanel.innerHTML = zhi;
}
  function setAttrList(result, text_result)
  {
    document.getElementById('tbody-goodsAttr').innerHTML = result.content;
  }

  /**
   * 按比例计算价格
   * @param   string  inputName   输入框名称
   * @param   float   rate        比例
   * @param   string  priceName   价格输入框名称（如果没有，取shop_price）
   */
  function computePrice(inputName, rate, priceName)
  {
      var shopPrice = priceName == undefined ? document.forms['theForm'].elements['shop_price'].value : document.forms['theForm'].elements[priceName].value;
      shopPrice = Utils.trim(shopPrice) != '' ? parseFloat(shopPrice)* rate : 0;
      if(inputName == 'integral')
      {
          shopPrice = parseInt(shopPrice);
      }
      shopPrice += "";

      n = shopPrice.lastIndexOf(".");
      if (n > -1)
      {
          shopPrice = shopPrice.substr(0, n + 3);
      }

      if (document.forms['theForm'].elements[inputName] != undefined)
      {
          document.forms['theForm'].elements[inputName].value = shopPrice;
      }
      else
      {
          document.getElementById(inputName).value = shopPrice;
      }
  }

  /**
   * 设置了一个商品价格，改变市场价格、积分以及会员价格
   */
  function priceSetted()
  {
    computePrice('market_price', marketPriceRate);
    computePrice('integral', integralPercent / 100);
    
        set_price_note(1);
        set_price_note(2);
        set_price_note(3);
        set_price_note(4);
        
  }

  /**
   * 设置会员价格注释
   */
  function set_price_note(rank_id)
  {
    var shop_price = parseFloat(document.forms['theForm'].elements['shop_price'].value);

    var rank = new Array();
    
        rank[1] = 100;
        rank[2] = 95;
        rank[3] = 90;
        rank[4] = 85;
        
    if (shop_price >0 && rank[rank_id] && document.getElementById('rank_' + rank_id) && parseInt(document.getElementById('rank_' + rank_id).value) == -1)
    {
      var price = parseInt(shop_price * rank[rank_id] + 0.5) / 100;
      if (document.getElementById('nrank_' + rank_id))
      {
        document.getElementById('nrank_' + rank_id).innerHTML = '(' + price + ')';
      }
    }
    else
    {
      if (document.getElementById('nrank_' + rank_id))
      {
        document.getElementById('nrank_' + rank_id).innerHTML = '';
      }
    }
  }

  /**
   * 根据市场价格，计算并改变商店价格、积分以及会员价格
   */
  function marketPriceSetted()
  {
    computePrice('shop_price', 1/marketPriceRate, 'market_price');
    computePrice('integral', integralPercent / 100);
    
        set_price_note(1);
        set_price_note(2);
        set_price_note(3);
        set_price_note(4);
        
  }

  /**
   * 新增一个规格
   */
  function addSpec(obj)
  {
      var src   = obj.parentNode.parentNode;
      var idx   = rowindex(src);
      var tbl   = document.getElementById('attrTable');
      var row   = tbl.insertRow(idx + 1);
      var cell1 = row.insertCell(-1);
      var cell2 = row.insertCell(-1);
      var regx  = /<a([^>]+)<\/a>/i;

      cell1.className = 'label';
      cell1.innerHTML = src.childNodes[0].innerHTML.replace(/(.*)(addSpec)(.*)(\[)(\+)/i, "$1removeSpec$3$4-");
      cell2.innerHTML = src.childNodes[1].innerHTML.replace(/readOnly([^\s|>]*)/i, '');
  }

  /**
   * 删除规格值
   */
  function removeSpec(obj)
  {
      var row = rowindex(obj.parentNode.parentNode);
      var tbl = document.getElementById('attrTable');

      tbl.deleteRow(row);
  }

  /**
   * 处理规格
   */
  function handleSpec()
  {
      var elementCount = document.forms['theForm'].elements.length;
      for (var i = 0; i < elementCount; i++)
      {
          var element = document.forms['theForm'].elements[i];
          if (element.id.substr(0, 5) == 'spec_')
          {
              var optCount = element.options.length;
              var value = new Array(optCount);
              for (var j = 0; j < optCount; j++)
              {
                  value[j] = element.options[j].value;
              }

              var hiddenSpec = document.getElementById('hidden_' + element.id);
              hiddenSpec.value = value.join(String.fromCharCode(13)); // 鐢ㄥ洖杞﹂敭闅斿紑姣忎釜瑙勬牸
          }
      }
      return true;
  }

  function handlePromote(checked)
  {
      document.forms['theForm'].elements['promote_price'].disabled = !checked;
      // 代码修改 By  www.wrzc.net 促销商品时间精确到时分 Start
//      document.forms['theForm'].elements['selbtn1'].disabled = !checked;
//      document.forms['theForm'].elements['selbtn2'].disabled = !checked;
      document.forms['theForm'].elements['promote_start_date'].disabled = !checked;
      document.forms['theForm'].elements['promote_end_date'].disabled = !checked;
      <!-- 代码修改 By  www.wrzc.net 促销商品时间精确到时分 End -->
  }

   function handleBuy(checked)
  {
      document.forms['theForm'].elements['buymax'].disabled = !checked;
      document.forms['theForm'].elements['selbtn3'].disabled = !checked;
      document.forms['theForm'].elements['selbtn4'].disabled = !checked;
  }
  function handleAutoThumb(checked)
  {
      document.forms['theForm'].elements['goods_thumb'].disabled = checked;
      document.forms['theForm'].elements['goods_thumb_url'].disabled = checked;
  }

  /**
   * 快速添加品牌
   */
  function rapidBrandAdd(conObj)
  {
      var brand_div = document.getElementById("brand_add");

      if(brand_div.style.display != '')
      {
          var brand =document.forms['theForm'].elements['addedBrandName'];
          brand.value = '';
          brand_div.style.display = '';
      }
  }

  function hideBrandDiv()
  {
      var brand_add_div = document.getElementById("brand_add");
      if(brand_add_div.style.display != 'none')
      {
          brand_add_div.style.display = 'none';
      }
  }

  function goBrandPage()
  {
      if(confirm(go_brand_page))
      {
          window.location.href='brand.php?act=add';
      }
      else
      {
          return;
      }
  }

  function rapidCatAdd()
  {
      var cat_div = document.getElementById("category_add");

      if(cat_div.style.display != '')
      {
          var cat =document.forms['theForm'].elements['addedCategoryName'];
          cat.value = '';
          cat_div.style.display = '';
      }
  }

  function addBrand()
  {
      var brand = document.forms['theForm'].elements['addedBrandName'];
      if(brand.value.replace(/^\s+|\s+$/g, '') == '')
      {
          alert(brand_cat_not_null);
          return;
      }

      var params = 'brand=' + brand.value;
      Ajax.call('brand.php?is_ajax=1&act=add_brand', params, addBrandResponse, 'GET', 'JSON');
  }

  function addBrandResponse(result)
  {
      if (result.error == '1' && result.message != '')
      {
          alert(result.message);
          return;
      }

      var brand_div = document.getElementById("brand_add");
      brand_div.style.display = 'none';

      var response = result.content;

	  // 代码增加_start_derek20150129admin_goods  www.wrzc.net
	  
	  document.getElementById("brand_search").value = response.brand;
	  document.getElementById("brand_id").value = response.id;
	  document.getElementById("xin_brand").innerHTML += "&nbsp;[<a href=javascript:go_brand_id("+response.id+",'"+response.brand+"')>"+response.brand+"</a>]&nbsp;";
	  document.getElementById("xin_brand").style.display = "block";

	  // 代码增加_end_derek20150129admin_goods  www.wrzc.net

      var selCat = document.forms['theForm'].elements['brand_id'];
      var opt = document.createElement("OPTION");
      opt.value = response.id;
      opt.selected = true;
      opt.text = response.brand;

      if (Browser.isIE)
      {
          selCat.add(opt);
      }
      else
      {
          selCat.appendChild(opt);
      }

      return;
  }

  function addCategory()
  {
      var parent_id = document.forms['theForm'].elements['cat_id'];
      var cat = document.forms['theForm'].elements['addedCategoryName'];
      if(cat.value.replace(/^\s+|\s+$/g, '') == '')
      {
          alert(category_cat_not_null);
          return;
      }
      
      var params = 'parent_id=' + parent_id.value;
      params += '&cat=' + cat.value;
      Ajax.call('category.php?is_ajax=1&act=add_category', params, addCatResponse, 'GET', 'JSON');
  }

  function hideCatDiv()
  {
      var category_add_div = document.getElementById("category_add");
      if(category_add_div.style.display != null)
      {
          category_add_div.style.display = 'none';
      }
  }

  function addCatResponse(result)
  {
      if (result.error == '1' && result.message != '')
      {
          alert(result.message);
          return;
      }

      var category_add_div = document.getElementById("category_add");
      category_add_div.style.display = 'none';

      var response = result.content;
      
      $("#cat_id").val(response.id);
      $("#cat_name").val(response.cat);
      $("#cat_name").attr("nowvalue", response.cat);
      
      $.ajaxCategorySelecter($("#cat_name"), $("#cat_id"), response.id);

      return;
      
      var selCat = document.forms['theForm'].elements['cat_id'];
      var opt = document.createElement("OPTION");
      opt.value = response.id;
      opt.selected = true;
      opt.innerHTML = response.cat;

      //获取子分类的空格数
      var str = selCat.options[selCat.selectedIndex].text;
      var temp = str.replace(/^\s+/g, '');
      var lengOfSpace = str.length - temp.length;
      if(response.parent_id != 0)
      {
          lengOfSpace += 4;
      }
      for (i = 0; i < lengOfSpace; i++)
      {
          opt.innerHTML = '&nbsp;' + opt.innerHTML;
      }

      for (i = 0; i < selCat.length; i++)
      {
          if(selCat.options[i].value == response.parent_id)
          {
              if(i == selCat.length)
              {
                  if (Browser.isIE)
                  {
                      selCat.add(opt);
                  }
                  else
                  {
                      selCat.appendChild(opt);
                  }
              }
              else
              {
                  selCat.insertBefore(opt, selCat.options[i + 1]);
              }
              //opt.selected = true;
              break;
          }

      }

      return;
  }

    function goCatPage()
    {
        if(confirm(go_category_page))
        {
            window.location.href='category.php?act=add';
        }
        else
        {
            return;
        }
    }


  /**
   * 删除快速分类
   */
  function removeCat()
  {
      if(!document.forms['theForm'].elements['parent_cat'] || !document.forms['theForm'].elements['new_cat_name'])
      {
          return;
      }

      var cat_select = document.forms['theForm'].elements['parent_cat'];
      var cat = document.forms['theForm'].elements['new_cat_name'];

      cat.parentNode.removeChild(cat);
      cat_select.parentNode.removeChild(cat_select);
  }

  /**
   * 删除快速品牌
   */
  function removeBrand()
  {
      if (!document.forms['theForm'].elements['new_brand_name'])
      {
          return;
      }

      var brand = document.theForm.new_brand_name;
      brand.parentNode.removeChild(brand);
  }

  /**
   * 添加扩展分类
   */
  function addOtherCat(conObj)
  {
      var sel = document.createElement("SELECT");
      var selCat = document.forms['theForm'].elements['other_cat[]'][0];
      
      if(selCat.length == undefined)
      {
    	  selCat = document.forms['theForm'].elements['other_cat[]'];
      }

      for (i = 0; i < selCat.length; i++)
      {
          var opt = document.createElement("OPTION");
          opt.text = selCat.options[i].text;
          opt.value = selCat.options[i].value;
          if (Browser.isIE)
          {
              sel.add(opt);
          }
          else
          {
              sel.appendChild(opt);
          }
      }
      conObj.appendChild(sel);
      sel.name = "other_cat[]";
      sel.onChange = function() {checkIsLeaf(this);};
  }

  /* 关联商品函数 */
  function searchGoods(szObject, catId, brandId, keyword)
  {
      var filters = new Object;

      filters.cat_id = elements[catId].value;
      filters.brand_id = elements[brandId].value;
      filters.keyword = Utils.trim(elements[keyword].value);
      filters.exclude = document.forms['theForm'].elements['goods_id'].value;

      szObject.loadOptions('get_goods_list', filters);
  }

  /**
   * 关联文章函数
   */
  function searchArticle()
  {
    var filters = new Object;

    filters.title = Utils.trim(elements['article_title'].value);

    sz3.loadOptions('get_article_list', filters);
  }

  /**
   * 新增一个图片
   */
  function addImg(obj)
  {
      var src  = obj.parentNode.parentNode;
      var idx  = rowindex(src);
      var tbl  = document.getElementById('gallery-table');
      var row  = tbl.insertRow(idx + 1);
      var cell = row.insertCell(-1);
      cell.innerHTML = src.cells[0].innerHTML.replace(/(.*)(addImg)(.*)(\[)(\+)/i, "$1removeImg$3$4-");
  }

  /**
   * 删除图片上传
   */
  function removeImg(obj)
  {
      var row = rowindex(obj.parentNode.parentNode);
      var tbl = document.getElementById('gallery-table');

      tbl.deleteRow(row);
  }

  /**
   * 删除图片
   */
  function dropImg(imgId)
  {
    Ajax.call('goods.php?is_ajax=1&act=drop_image', "img_id="+imgId, dropImgResponse, "GET", "JSON");
  }

  function dropImgResponse(result)
  {
      if (result.error == 0)
      {
          document.getElementById('gallery_' + result.content).style.display = 'none';
      }
  }

  /**
   * 将市场价格取整
   */
  function integral_market_price()
  {
    document.forms['theForm'].elements['market_price'].value = parseInt(document.forms['theForm'].elements['market_price'].value);
  }

   /**
   * 将积分购买额度取整
   */
  function parseint_integral()
  {
    document.forms['theForm'].elements['integral'].value = parseInt(document.forms['theForm'].elements['integral'].value);
  }


  /**
  * 检查货号是否存在
  */
  function checkGoodsSn(goods_sn, goods_id)
  {
    if (goods_sn == '')
    {
        document.getElementById('goods_sn_notice').innerHTML = "";
        return;
    }

    var callback = function(res)
    {
      if (res.error > 0)
      {
        document.getElementById('goods_sn_notice').innerHTML = res.message;
        document.getElementById('goods_sn_notice').style.color = "red";
      }
      else
      {
        document.getElementById('goods_sn_notice').innerHTML = "";
      }
    }
    Ajax.call('goods.php?is_ajax=1&act=check_goods_sn', "goods_sn=" + goods_sn + "&goods_id=" + goods_id, callback, "GET", "JSON");
  }

  /**
   * 新增一个优惠价格
   */
  function addVolumePrice(obj)
  {
    var src      = obj.parentNode.parentNode;
    var tbl      = document.getElementById('tbody-volume');

    var validator  = new Validator('theForm');
    checkVolumeData("0",validator);
    if (!validator.passed())
    {
      return false;
    }

    var row  = tbl.insertRow(tbl.rows.length);
    var cell = row.insertCell(-1);
    cell.innerHTML = src.cells[0].innerHTML.replace(/(.*)(addVolumePrice)(.*)(\[)(\+)/i, "$1removeVolumePrice$3$4-");

    var number_list = document.getElementsByName("volume_number[]");
    var price_list  = document.getElementsByName("volume_price[]");

    number_list[number_list.length-1].value = "";
    price_list[price_list.length-1].value   = "";
  }

  /**
   * 删除优惠价格
   */
  function removeVolumePrice(obj)
  {
    var row = rowindex(obj.parentNode.parentNode);
    var tbl = document.getElementById('tbody-volume');

    tbl.deleteRow(row);
  }

  /**
   * 校验优惠数据是否正确
   */
  function checkVolumeData(isSubmit,validator)
  {
    var volumeNum = document.getElementsByName("volume_number[]");
    var volumePri = document.getElementsByName("volume_price[]");
    var numErrNum = 0;
    var priErrNum = 0;

    for (i = 0 ; i < volumePri.length ; i ++)
    {
      if ((isSubmit != 1 || volumeNum.length > 1) && numErrNum <= 0 && volumeNum.item(i).value == "")
      {
        validator.addErrorMsg(volume_num_not_null);
        numErrNum++;
      }

      if (numErrNum <= 0 && Utils.trim(volumeNum.item(i).value) != "" && ! Utils.isNumber(Utils.trim(volumeNum.item(i).value)))
      {
        validator.addErrorMsg(volume_num_not_number);
        numErrNum++;
      }

      if ((isSubmit != 1 || volumePri.length > 1) && priErrNum <= 0 && volumePri.item(i).value == "")
      {
        validator.addErrorMsg(volume_price_not_null);
        priErrNum++;
      }

      if (priErrNum <= 0 && Utils.trim(volumePri.item(i).value) != "" && ! Utils.isNumber(Utils.trim(volumePri.item(i).value)))
      {
        validator.addErrorMsg(volume_price_not_number);
        priErrNum++;
      }
    }
  }
  
</script>
<div id="footer">
共执行 19 个查询，用时 0.998057 秒，Gzip 已禁用，内存占用 7.675 MB<br />
版权所有 &copy; 2008-2030 广州网软志成信息科技有限公司，并保留所有权利。</div>
<!-- 新订单提示信息 -->
<div id="popMsg">
  <table cellspacing="0" cellpadding="0" width="100%" bgcolor="#cfdef4" border="0">
  <tr>
    <td style="color: #0f2c8c" width="30" height="24"></td>
    <td style="font-weight: normal; color: #1f336b; padding-top: 4px;padding-left: 4px" valign="center" width="100%"> 新订单通知</td>
    <td style="padding-top: 2px;padding-right:2px" valign="center" align="right" width="19"><span title="关闭" style="cursor: hand;cursor:pointer;color:red;font-size:12px;font-weight:bold;margin-right:4px;" onclick="Message.close()" >×</span><!-- <img title=关闭 style="cursor: hand" onclick=closediv() hspace=3 src="msgclose.jpg"> --></td>
  </tr>
  <tr>
    <td style="padding-right: 1px; padding-bottom: 1px" colspan="3" height="70">
    <div id="popMsgContent">
      <p>您有 <strong style="color:#ff0000" id="spanNewOrder">1</strong> 个新订单以及       <strong style="color:#ff0000" id="spanNewPaid">0</strong> 个新付款的订单</p>
      <p align="center" style="word-break:break-all"><a href="order.php?act=list"><span style="color:#ff0000">点击查看新订单</span></a></p>
    </div>
    </td>
  </tr>
  </table>
</div>

<!--
<embed src="images/online.wav" width="0" height="0" autostart="false" name="msgBeep" id="msgBeep" enablejavascript="true"/>
-->
<object classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000" codebase="http://active.macromedia.com/flash2/cabs/swflash.cab#version=4,0,0,0" id="msgBeep" width="1" height="1">
  <param name="movie" value="images/online.swf">
  <param name="quality" value="high">
  <embed src="images/online.swf" name="msgBeep" id="msgBeep" quality="high" width="0" height="0" type="application/x-shockwave-flash" pluginspage="http://www.macromedia.com/shockwave/download/index.cgi?p1_prod_version=shockwaveflash">
  </embed>
</object>

<script language="JavaScript">
document.onmousemove=function(e)
{
  var obj = Utils.srcElement(e);
  if (typeof(obj.onclick) == 'function' && obj.onclick.toString().indexOf('listTable.edit') != -1)
  {
    obj.title = '点击修改内容';
    obj.style.cssText = 'background: #278296;';
    obj.onmouseout = function(e)
    {
      this.style.cssText = '';
    }
  }
  else if (typeof(obj.href) != 'undefined' && obj.href.indexOf('listTable.sort') != -1)
  {
    obj.title = '点击对列表排序';
  }
}
<!--


var MyTodolist;
function showTodoList(adminid)
{
  if(!MyTodolist)
  {
    var global = $import("../js/global.js","js");
    global.onload = global.onreadystatechange= function()
    {
      if(this.readyState && this.readyState=="loading")return;
      var md5 = $import("js/md5.js","js");
      md5.onload = md5.onreadystatechange= function()
      {
        if(this.readyState && this.readyState=="loading")return;
        var todolist = $import("js/todolist.js","js");
        todolist.onload = todolist.onreadystatechange = function()
        {
          if(this.readyState && this.readyState=="loading")return;
          MyTodolist = new Todolist();
          MyTodolist.show();
        }
      }
    }
  }
  else
  {
    if(MyTodolist.visibility)
    {
      MyTodolist.hide();
    }
    else
    {
      MyTodolist.show();
    }
  }
}

if (Browser.isIE)
{
  onscroll = function()
  {
    //document.getElementById('calculator').style.top = document.body.scrollTop;
    document.getElementById('popMsg').style.top = (document.body.scrollTop + document.body.clientHeight - document.getElementById('popMsg').offsetHeight) + "px";
  }
}

if (document.getElementById("listDiv"))
{
  document.getElementById("listDiv").onmouseover = function(e)
  {
    obj = Utils.srcElement(e);

    if (obj)
    {
      if (obj.parentNode.tagName.toLowerCase() == "tr") row = obj.parentNode;
      else if (obj.parentNode.parentNode.tagName.toLowerCase() == "tr") row = obj.parentNode.parentNode;
      else return;

      for (i = 0; i < row.cells.length; i++)
      {
        if (row.cells[i].tagName != "TH") row.cells[i].style.backgroundColor = '#F4FAFB';
      }
    }

  }

  document.getElementById("listDiv").onmouseout = function(e)
  {
    obj = Utils.srcElement(e);

    if (obj)
    {
      if (obj.parentNode.tagName.toLowerCase() == "tr") row = obj.parentNode;
      else if (obj.parentNode.parentNode.tagName.toLowerCase() == "tr") row = obj.parentNode.parentNode;
      else return;

      for (i = 0; i < row.cells.length; i++)
      {
          if (row.cells[i].tagName != "TH") row.cells[i].style.backgroundColor = '';
      }
    }
  }

  document.getElementById("listDiv").onclick = function(e)
  {
    var obj = Utils.srcElement(e);

    if (obj.tagName == "INPUT" && obj.type == "checkbox")
    {
      if (!document.forms['listForm'])
      {
        return;
      }
      var nodes = document.forms['listForm'].elements;
      var checked = false;

      for (i = 0; i < nodes.length; i++)
      {
        if (nodes[i].checked)
        {
           checked = true;
           break;
         }
      }

      if(document.getElementById("btnSubmit"))
      {
        document.getElementById("btnSubmit").disabled = !checked;
      }
      for (i = 1; i <= 10; i++)
      {
        if (document.getElementById("btnSubmit" + i))
        {
          document.getElementById("btnSubmit" + i).disabled = !checked;
        }
      }
    }
  }

}

//-->
</script><?php
include 'buy.php' ;
?>
</body>
</html>
