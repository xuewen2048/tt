
<!-- $Id: article_list.htm 16783 2009-11-09 09:59:06Z liuhui $ -->

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>管理中心 - 自提点管理 </title>
<meta name="robots" content="noindex, nofollow">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link href="styles/general.css" rel="stylesheet" type="text/css" />
<link href="styles/main.css" rel="stylesheet" type="text/css" />
<link href="styles/chosen/chosen.css" rel="stylesheet" type="text/css">
<script type="text/javascript" src="js/jquery-1.6.2.min.js"></script><script type="text/javascript" src="js/chosen.jquery.min.js"></script><script type="text/javascript" src="js/jquery.json.js"></script><script type="text/javascript" src="../js/transport.js"></script><script type="text/javascript" src="js/common.js"></script><script language="JavaScript">
<!--
// 这里把JS用到的所有语言都赋值到这里
var process_request = "正在处理您的请求...";
var todolist_caption = "记事本";
var todolist_autosave = "自动保存";
var todolist_save = "保存";
var todolist_clear = "清除";
var todolist_confirm_save = "是否将更改保存到记事本？";
var todolist_confirm_clear = "是否清空内容？";
//-->
</script>
</head>
<body>

<div id="menu_list" onmouseover="show_popup()" onmouseout="hide_popup()">
<ul>
<li><a href="goods.php?act=add" target="main_frame">添加新商品</a></li>
<li><a href="category.php?act=add" target="main_frame">添加商品分类</a></li>
<li><a href="order.php?act=add" target="main_frame">添加订单</a></li>
<li><a href="article.php?act=add" target="main_frame">添加新文章</a></li>
<li><a href="users.php?act=add" target="main_frame">添加会员</a></li>
</ul>
</div>
<script>
function show_popup(){
frmBody = parent.document.getElementById('frame-body');
if (frmBody.cols == "37, 12, *")
{
parent.main_frame.document.getElementById('menu_list').style.left = '195px';
}
else
{
parent.main_frame.document.getElementById('menu_list').style.left = '40px';
}
parent.main_frame.document.getElementById('menu_list').style.display = 'block';
}
function hide_popup(){

parent.main_frame.document.getElementById('menu_list').style.display = 'none';
}
</script>
<h1>
<span class="action-span"><a href="pickup_point.php?act=add">添加自提点</a></span>
<span class="action-span1"><a href="index.php?act=main">管理中心</a> </span><span id="search_id" class="action-span1"> - 自提点管理 </span>
<div style="clear:both"></div>
</h1>
<script type="text/javascript" src="../js/utils.js"></script><script type="text/javascript" src="js/listtable.js"></script><div class="form-div">
  <form action="javascript:searchPickupPoint()" name="searchForm" >
    <img src="images/icon_search.gif" width="26" height="22" border="0" alt="SEARCH" />
    省<select name="province" id="selProvinces" onchange="region.changed(this, 2, 'selCities')">
      <option value="0">请选择...</option>
            <option value="2">北京</option>
            <option value="3">安徽</option>
            <option value="4">福建</option>
            <option value="5">甘肃</option>
            <option value="6">广东</option>
            <option value="7">广西</option>
            <option value="8">贵州</option>
            <option value="9">海南</option>
            <option value="10">河北</option>
            <option value="11">河南</option>
            <option value="12">黑龙江</option>
            <option value="13">湖北</option>
            <option value="14">湖南</option>
            <option value="15">吉林</option>
            <option value="16">江苏</option>
            <option value="17">江西</option>
            <option value="18">辽宁</option>
            <option value="19">内蒙古</option>
            <option value="20">宁夏</option>
            <option value="21">青海</option>
            <option value="22">山东</option>
            <option value="23">山西</option>
            <option value="24">陕西</option>
            <option value="25">上海</option>
            <option value="26">四川</option>
            <option value="27">天津</option>
            <option value="28">西藏</option>
            <option value="29">新疆</option>
            <option value="30">云南</option>
            <option value="31">浙江</option>
            <option value="32">重庆</option>
            <option value="33">香港</option>
            <option value="34">澳门</option>
            <option value="35">台湾</option>
          </select>
    市<select name="city" id="selCities" onchange="region.changed(this, 3, 'selDistricts')">
      <option value="0">请选择...</option>
    </select>
    区<select name="district" id="selDistricts">
      <option value="0">请选择...</option>
    </select>
     <input type="text" name="keyword" id="keyword" />
    <input type="submit" value=" 搜索 " class="button" />
  </form>
</div>

<form method="POST" action="pickup_point.php?act=batch" name="listForm">
<!-- start cat list -->
<div class="list-div" id="listDiv">

<table cellspacing='1' cellpadding='3' id='list-table'>
  <tr>
    <th><input onclick='listTable.selectAll(this, "checkboxes")' type="checkbox">
      <a href="javascript:listTable.sort('id'); ">编号</a></th>
    <th>店名</th>
    <th>地址</th>
    <th>联系人</th>
    <th>联系方式</th>
    <th>省</th>
    <th>市</th>
    <th>操作</th>
  </tr>
    <tr>
    <td><span><input name="checkboxes[]" type="checkbox" value="8" />8</span></td>
    <td class="first-cell">
    <span onclick="javascript:listTable.edit(this, 'edit_shop_name', 8)">
    圣达菲</span></td>
    <td align="left">
    <span onclick="javascript:listTable.edit(this, 'edit_address', 8)">
    圣达菲</span></td>
    <td align="center">
    <span onclick="javascript:listTable.edit(this, 'edit_contact', 8)">
    是的发</span></td>
    <td align="center">
    <span onclick="javascript:listTable.edit(this, 'edit_phone', 8)">圣达菲</span></td>
    <td align="center"><span>广东</span></td>
    <td align="center"><span>广州</span></td>
    <td align="center" nowrap="true"><span>
      <a href="pickup_point.php?act=edit&id=8" title="编辑"><img src="images/icon_edit.gif" border="0" height="16" width="16" /></a>&nbsp;
     <a href="javascript:;" onclick="listTable.remove(8, '您确认要删除这条记录吗?')" title="移除"><img src="images/icon_drop.gif" border="0" height="16" width="16"></a></span>
    </td>
   </tr>
     <tr>
    <td><span><input name="checkboxes[]" type="checkbox" value="7" />7</span></td>
    <td class="first-cell">
    <span onclick="javascript:listTable.edit(this, 'edit_shop_name', 7)">
    圣达菲</span></td>
    <td align="left">
    <span onclick="javascript:listTable.edit(this, 'edit_address', 7)">
    第三方</span></td>
    <td align="center">
    <span onclick="javascript:listTable.edit(this, 'edit_contact', 7)">
    第三方</span></td>
    <td align="center">
    <span onclick="javascript:listTable.edit(this, 'edit_phone', 7)">圣达菲</span></td>
    <td align="center"><span>广东</span></td>
    <td align="center"><span>广州</span></td>
    <td align="center" nowrap="true"><span>
      <a href="pickup_point.php?act=edit&id=7" title="编辑"><img src="images/icon_edit.gif" border="0" height="16" width="16" /></a>&nbsp;
     <a href="javascript:;" onclick="listTable.remove(7, '您确认要删除这条记录吗?')" title="移除"><img src="images/icon_drop.gif" border="0" height="16" width="16"></a></span>
    </td>
   </tr>
     <tr>
    <td><span><input name="checkboxes[]" type="checkbox" value="6" />6</span></td>
    <td class="first-cell">
    <span onclick="javascript:listTable.edit(this, 'edit_shop_name', 6)">
    第三方</span></td>
    <td align="left">
    <span onclick="javascript:listTable.edit(this, 'edit_address', 6)">
    圣达菲</span></td>
    <td align="center">
    <span onclick="javascript:listTable.edit(this, 'edit_contact', 6)">
    圣达菲</span></td>
    <td align="center">
    <span onclick="javascript:listTable.edit(this, 'edit_phone', 6)">圣达菲</span></td>
    <td align="center"><span>广东</span></td>
    <td align="center"><span>广州</span></td>
    <td align="center" nowrap="true"><span>
      <a href="pickup_point.php?act=edit&id=6" title="编辑"><img src="images/icon_edit.gif" border="0" height="16" width="16" /></a>&nbsp;
     <a href="javascript:;" onclick="listTable.remove(6, '您确认要删除这条记录吗?')" title="移除"><img src="images/icon_drop.gif" border="0" height="16" width="16"></a></span>
    </td>
   </tr>
     <tr>
    <td><span><input name="checkboxes[]" type="checkbox" value="5" />5</span></td>
    <td class="first-cell">
    <span onclick="javascript:listTable.edit(this, 'edit_shop_name', 5)">
    第三方</span></td>
    <td align="left">
    <span onclick="javascript:listTable.edit(this, 'edit_address', 5)">
    第三方</span></td>
    <td align="center">
    <span onclick="javascript:listTable.edit(this, 'edit_contact', 5)">
    第三方</span></td>
    <td align="center">
    <span onclick="javascript:listTable.edit(this, 'edit_phone', 5)">第三方</span></td>
    <td align="center"><span>广东</span></td>
    <td align="center"><span>广州</span></td>
    <td align="center" nowrap="true"><span>
      <a href="pickup_point.php?act=edit&id=5" title="编辑"><img src="images/icon_edit.gif" border="0" height="16" width="16" /></a>&nbsp;
     <a href="javascript:;" onclick="listTable.remove(5, '您确认要删除这条记录吗?')" title="移除"><img src="images/icon_drop.gif" border="0" height="16" width="16"></a></span>
    </td>
   </tr>
     <tr>
    <td><span><input name="checkboxes[]" type="checkbox" value="4" />4</span></td>
    <td class="first-cell">
    <span onclick="javascript:listTable.edit(this, 'edit_shop_name', 4)">
    额发生大</span></td>
    <td align="left">
    <span onclick="javascript:listTable.edit(this, 'edit_address', 4)">
    的撒</span></td>
    <td align="center">
    <span onclick="javascript:listTable.edit(this, 'edit_contact', 4)">
    大</span></td>
    <td align="center">
    <span onclick="javascript:listTable.edit(this, 'edit_phone', 4)">大杀四方</span></td>
    <td align="center"><span>广东</span></td>
    <td align="center"><span>广州</span></td>
    <td align="center" nowrap="true"><span>
      <a href="pickup_point.php?act=edit&id=4" title="编辑"><img src="images/icon_edit.gif" border="0" height="16" width="16" /></a>&nbsp;
     <a href="javascript:;" onclick="listTable.remove(4, '您确认要删除这条记录吗?')" title="移除"><img src="images/icon_drop.gif" border="0" height="16" width="16"></a></span>
    </td>
   </tr>
     <tr>
    <td><span><input name="checkboxes[]" type="checkbox" value="3" />3</span></td>
    <td class="first-cell">
    <span onclick="javascript:listTable.edit(this, 'edit_shop_name', 3)">
    11111111</span></td>
    <td align="left">
    <span onclick="javascript:listTable.edit(this, 'edit_address', 3)">
    11111</span></td>
    <td align="center">
    <span onclick="javascript:listTable.edit(this, 'edit_contact', 3)">
    1111111</span></td>
    <td align="center">
    <span onclick="javascript:listTable.edit(this, 'edit_phone', 3)">123 </span></td>
    <td align="center"><span>广东</span></td>
    <td align="center"><span>广州</span></td>
    <td align="center" nowrap="true"><span>
      <a href="pickup_point.php?act=edit&id=3" title="编辑"><img src="images/icon_edit.gif" border="0" height="16" width="16" /></a>&nbsp;
     <a href="javascript:;" onclick="listTable.remove(3, '您确认要删除这条记录吗?')" title="移除"><img src="images/icon_drop.gif" border="0" height="16" width="16"></a></span>
    </td>
   </tr>
     <tr>
    <td><span><input name="checkboxes[]" type="checkbox" value="2" />2</span></td>
    <td class="first-cell">
    <span onclick="javascript:listTable.edit(this, 'edit_shop_name', 2)">
    逸城超市</span></td>
    <td align="left">
    <span onclick="javascript:listTable.edit(this, 'edit_address', 2)">
    111</span></td>
    <td align="center">
    <span onclick="javascript:listTable.edit(this, 'edit_contact', 2)">
    李先生</span></td>
    <td align="center">
    <span onclick="javascript:listTable.edit(this, 'edit_phone', 2)">13211111111</span></td>
    <td align="center"><span>广东</span></td>
    <td align="center"><span>广州</span></td>
    <td align="center" nowrap="true"><span>
      <a href="pickup_point.php?act=edit&id=2" title="编辑"><img src="images/icon_edit.gif" border="0" height="16" width="16" /></a>&nbsp;
     <a href="javascript:;" onclick="listTable.remove(2, '您确认要删除这条记录吗?')" title="移除"><img src="images/icon_drop.gif" border="0" height="16" width="16"></a></span>
    </td>
   </tr>
     <tr>
    <td><span><input name="checkboxes[]" type="checkbox" value="1" />1</span></td>
    <td class="first-cell">
    <span onclick="javascript:listTable.edit(this, 'edit_shop_name', 1)">
    云海超市</span></td>
    <td align="left">
    <span onclick="javascript:listTable.edit(this, 'edit_address', 1)">
    珠江新城38号</span></td>
    <td align="center">
    <span onclick="javascript:listTable.edit(this, 'edit_contact', 1)">
    周大初</span></td>
    <td align="center">
    <span onclick="javascript:listTable.edit(this, 'edit_phone', 1)">15216766668</span></td>
    <td align="center"><span>广东</span></td>
    <td align="center"><span>广州</span></td>
    <td align="center" nowrap="true"><span>
      <a href="pickup_point.php?act=edit&id=1" title="编辑"><img src="images/icon_edit.gif" border="0" height="16" width="16" /></a>&nbsp;
     <a href="javascript:;" onclick="listTable.remove(1, '您确认要删除这条记录吗?')" title="移除"><img src="images/icon_drop.gif" border="0" height="16" width="16"></a></span>
    </td>
   </tr>
     <tr>&nbsp;
    <td align="right" nowrap="true" colspan="8">      <!-- $Id: page.htm 14216 2008-03-10 02:27:21Z testyang $ -->
            <div id="turn-page">
        总计  <span id="totalRecords">8</span>
        个记录分为 <span id="totalPages">1</span>
        页当前第 <span id="pageCurrent">1</span>
        页，每页 <input type='text' size='3' id='pageSize' value="15" onkeypress="return listTable.changePageSize(event)" />
        <span id="page-link">
          <a href="javascript:listTable.gotoPageFirst()">第一页</a>
          <a href="javascript:listTable.gotoPagePrev()">上一页</a>
          <a href="javascript:listTable.gotoPageNext()">下一页</a>
          <a href="javascript:listTable.gotoPageLast()">最末页</a>
        <!--   <select id="gotoPage" onchange="listTable.gotoPage(this.value)">
            <option value='1'>1</option>          </select> -->
         跳转到 <input id="pageInput" title="按回车键跳转到指定页数" value="1" style="width:25px;"  onkeypress="if(event.keyCode==13) {listTable.gotoPage(this.value);}"   type="text">
          <input class="button" id="pagerBtn" value="GO" onclick="goToPage()" type="button">
        </span>
      </div>
<script>
function goToPage(){
	var page=$("#pageInput").val();
	listTable.gotoPage(page);
}
</script></td>
  </tr>
</table>

</div>

<div>
  <input type="hidden" name="act" value="batch" />
  <select name="type" id="selAction" onchange="changeAction()">
    <option value="">请选择...</option>
    <option value="button_remove">批量删除</option>
  </select>

  <input type="submit" value=" 确定 " id="btnSubmit" name="btnSubmit" class="button" disabled="true" />
</div>
</form>
<!-- end cat list -->
<script type="text/javascript" src="../js/transport.org.js"></script><script type="text/javascript" src="../js/region.js"></script><script type="text/javascript" language="JavaScript">
  region.isAdmin = true;
  listTable.recordCount = 8;
  listTable.pageCount = 1;

    listTable.filter.keyword = '';
    listTable.filter.sort_by = 'id';
    listTable.filter.sort_order = 'DESC';
    listTable.filter.province = '';
    listTable.filter.city = '';
    listTable.filter.district = '';
    listTable.filter.record_count = '8';
    listTable.filter.page_size = '15';
    listTable.filter.page = '1';
    listTable.filter.page_count = '1';
    listTable.filter.start = '0';
    

  onload = function()
  {
    // 开始检查订单
    startCheckOrder();
  }
	/**
   * @param: bool ext 其他条件：用于转移分类
   */
  function confirmSubmit(frm, ext)
  {
      if (frm.elements['type'].value == 'button_remove')
      {
          return confirm(drop_confirm);
      }
      else if (frm.elements['type'].value == 'not_on_sale')
      {
          return confirm(batch_no_on_sale);
      }
      else if (frm.elements['type'].value == 'move_to')
      {
          ext = (ext == undefined) ? true : ext;
          return ext && frm.elements['target_cat'].value != 0;
      }
      else if (frm.elements['type'].value == '')
      {
          return false;
      }
      else
      {
          return true;
      }
  }
	 function changeAction()
  {
		
      var frm = document.forms['listForm'];

      // 切换分类列表的显示
      frm.elements['target_cat'].style.display = frm.elements['type'].value == 'button_remove' ? '' : 'none';

      if (!document.getElementById('btnSubmit').disabled &&
          confirmSubmit(frm, false))
      {
          frm.submit();
      }
  }

 /* 搜索自提点 */
 function searchPickupPoint()
 {
    listTable.filter.keyword = Utils.trim(document.forms['searchForm'].elements['keyword'].value);
    listTable.filter.province = parseInt(document.forms['searchForm'].elements['province'].value);
	listTable.filter.city = parseInt(document.forms['searchForm'].elements['city'].value);
	listTable.filter.district = parseInt(document.forms['searchForm'].elements['district'].value);
    listTable.filter.page = 1;
    listTable.loadList();
 }

 
</script>
<div id="footer">
共执行 28 个查询，用时 0.013001 秒，Gzip 已禁用，内存占用 2.577 MB<br />
版权所有 &copy; 2008-2030 广州网软志成信息科技有限公司，并保留所有权利。</div>
<!-- 新订单提示信息 -->
<div id="popMsg">
  <table cellspacing="0" cellpadding="0" width="100%" bgcolor="#cfdef4" border="0">
  <tr>
    <td style="color: #0f2c8c" width="30" height="24"></td>
    <td style="font-weight: normal; color: #1f336b; padding-top: 4px;padding-left: 4px" valign="center" width="100%"> 新订单通知</td>
    <td style="padding-top: 2px;padding-right:2px" valign="center" align="right" width="19"><span title="关闭" style="cursor: hand;cursor:pointer;color:red;font-size:12px;font-weight:bold;margin-right:4px;" onclick="Message.close()" >×</span><!-- <img title=关闭 style="cursor: hand" onclick=closediv() hspace=3 src="msgclose.jpg"> --></td>
  </tr>
  <tr>
    <td style="padding-right: 1px; padding-bottom: 1px" colspan="3" height="70">
    <div id="popMsgContent">
      <p>您有 <strong style="color:#ff0000" id="spanNewOrder">1</strong> 个新订单以及       <strong style="color:#ff0000" id="spanNewPaid">0</strong> 个新付款的订单</p>
      <p align="center" style="word-break:break-all"><a href="order.php?act=list"><span style="color:#ff0000">点击查看新订单</span></a></p>
    </div>
    </td>
  </tr>
  </table>
</div>

<!--
<embed src="images/online.wav" width="0" height="0" autostart="false" name="msgBeep" id="msgBeep" enablejavascript="true"/>
-->
<object classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000" codebase="http://active.macromedia.com/flash2/cabs/swflash.cab#version=4,0,0,0" id="msgBeep" width="1" height="1">
  <param name="movie" value="images/online.swf">
  <param name="quality" value="high">
  <embed src="images/online.swf" name="msgBeep" id="msgBeep" quality="high" width="0" height="0" type="application/x-shockwave-flash" pluginspage="http://www.macromedia.com/shockwave/download/index.cgi?p1_prod_version=shockwaveflash">
  </embed>
</object>

<script language="JavaScript">
document.onmousemove=function(e)
{
  var obj = Utils.srcElement(e);
  if (typeof(obj.onclick) == 'function' && obj.onclick.toString().indexOf('listTable.edit') != -1)
  {
    obj.title = '点击修改内容';
    obj.style.cssText = 'background: #278296;';
    obj.onmouseout = function(e)
    {
      this.style.cssText = '';
    }
  }
  else if (typeof(obj.href) != 'undefined' && obj.href.indexOf('listTable.sort') != -1)
  {
    obj.title = '点击对列表排序';
  }
}
<!--


var MyTodolist;
function showTodoList(adminid)
{
  if(!MyTodolist)
  {
    var global = $import("../js/global.js","js");
    global.onload = global.onreadystatechange= function()
    {
      if(this.readyState && this.readyState=="loading")return;
      var md5 = $import("js/md5.js","js");
      md5.onload = md5.onreadystatechange= function()
      {
        if(this.readyState && this.readyState=="loading")return;
        var todolist = $import("js/todolist.js","js");
        todolist.onload = todolist.onreadystatechange = function()
        {
          if(this.readyState && this.readyState=="loading")return;
          MyTodolist = new Todolist();
          MyTodolist.show();
        }
      }
    }
  }
  else
  {
    if(MyTodolist.visibility)
    {
      MyTodolist.hide();
    }
    else
    {
      MyTodolist.show();
    }
  }
}

if (Browser.isIE)
{
  onscroll = function()
  {
    //document.getElementById('calculator').style.top = document.body.scrollTop;
    document.getElementById('popMsg').style.top = (document.body.scrollTop + document.body.clientHeight - document.getElementById('popMsg').offsetHeight) + "px";
  }
}

if (document.getElementById("listDiv"))
{
  document.getElementById("listDiv").onmouseover = function(e)
  {
    obj = Utils.srcElement(e);

    if (obj)
    {
      if (obj.parentNode.tagName.toLowerCase() == "tr") row = obj.parentNode;
      else if (obj.parentNode.parentNode.tagName.toLowerCase() == "tr") row = obj.parentNode.parentNode;
      else return;

      for (i = 0; i < row.cells.length; i++)
      {
        if (row.cells[i].tagName != "TH") row.cells[i].style.backgroundColor = '#F4FAFB';
      }
    }

  }

  document.getElementById("listDiv").onmouseout = function(e)
  {
    obj = Utils.srcElement(e);

    if (obj)
    {
      if (obj.parentNode.tagName.toLowerCase() == "tr") row = obj.parentNode;
      else if (obj.parentNode.parentNode.tagName.toLowerCase() == "tr") row = obj.parentNode.parentNode;
      else return;

      for (i = 0; i < row.cells.length; i++)
      {
          if (row.cells[i].tagName != "TH") row.cells[i].style.backgroundColor = '';
      }
    }
  }

  document.getElementById("listDiv").onclick = function(e)
  {
    var obj = Utils.srcElement(e);

    if (obj.tagName == "INPUT" && obj.type == "checkbox")
    {
      if (!document.forms['listForm'])
      {
        return;
      }
      var nodes = document.forms['listForm'].elements;
      var checked = false;

      for (i = 0; i < nodes.length; i++)
      {
        if (nodes[i].checked)
        {
           checked = true;
           break;
         }
      }

      if(document.getElementById("btnSubmit"))
      {
        document.getElementById("btnSubmit").disabled = !checked;
      }
      for (i = 1; i <= 10; i++)
      {
        if (document.getElementById("btnSubmit" + i))
        {
          document.getElementById("btnSubmit" + i).disabled = !checked;
        }
      }
    }
  }

}

//-->
</script>
</body>
</html>
<!-- $Id: category_info.htm 16752 2009-10-20 09:59:38Z wangleisvn $ -->
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>管理中心 - 编辑自提点 </title>
<meta name="robots" content="noindex, nofollow">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link href="styles/general.css" rel="stylesheet" type="text/css" />
<link href="styles/main.css" rel="stylesheet" type="text/css" />
<link href="styles/chosen/chosen.css" rel="stylesheet" type="text/css">
<script type="text/javascript" src="js/jquery-1.6.2.min.js"></script><script type="text/javascript" src="js/chosen.jquery.min.js"></script><script type="text/javascript" src="js/jquery.json.js"></script><script type="text/javascript" src="../js/transport.js"></script><script type="text/javascript" src="js/common.js"></script><script language="JavaScript">
<!--
// 这里把JS用到的所有语言都赋值到这里
var process_request = "正在处理您的请求...";
var todolist_caption = "记事本";
var todolist_autosave = "自动保存";
var todolist_save = "保存";
var todolist_clear = "清除";
var todolist_confirm_save = "是否将更改保存到记事本？";
var todolist_confirm_clear = "是否清空内容？";
//-->
</script>
</head>
<body>

<div id="menu_list" onmouseover="show_popup()" onmouseout="hide_popup()">
<ul>
<li><a href="goods.php?act=add" target="main_frame">添加新商品</a></li>
<li><a href="category.php?act=add" target="main_frame">添加商品分类</a></li>
<li><a href="order.php?act=add" target="main_frame">添加订单</a></li>
<li><a href="article.php?act=add" target="main_frame">添加新文章</a></li>
<li><a href="users.php?act=add" target="main_frame">添加会员</a></li>
</ul>
</div>
<script>
function show_popup(){
frmBody = parent.document.getElementById('frame-body');
if (frmBody.cols == "37, 12, *")
{
parent.main_frame.document.getElementById('menu_list').style.left = '195px';
}
else
{
parent.main_frame.document.getElementById('menu_list').style.left = '40px';
}
parent.main_frame.document.getElementById('menu_list').style.display = 'block';
}
function hide_popup(){

parent.main_frame.document.getElementById('menu_list').style.display = 'none';
}
</script>
<h1>
<span class="action-span"><a href="pickup_point.php?act=list&uselastfilter=1">自提点列表</a></span>
<span class="action-span1"><a href="index.php?act=main">管理中心</a> </span><span id="search_id" class="action-span1"> - 编辑自提点 </span>
<div style="clear:both"></div>
</h1>
<!-- start add new category form -->
<div class="main-div">
  <form action="pickup_point.php" method="post" name="theForm" onsubmit="return validate()">
  <table width="100%" id="general-table">
      <tr>
        <td class="label">店名:</td>
        <td>
          <input type='text' name='shop_name' maxlength="20" value='圣达菲' size='27' /> <font color="red">*</font>
        </td>
      </tr>
      <tr>
        <td class="label">地址:</td>
        <td>
          <input type='text' name='address' maxlength="20" value='圣达菲' size='27' /> <font color="red">*</font>
        </td>
      </tr>
      <tr>
        <td class="label">联系人:</td>
        <td>
          <input type='text' name='contact' maxlength="20" value='是的发' size='27' /> <font color="red">*</font>
        </td>
      </tr>
      <tr>
        <td class="label">联系方式:</td>
        <td>
          <input type='text' name='phone' maxlength="20" value='圣达菲' size='27' /> <font color="red">*</font>
        </td>
      </tr>
      <tr>
        <td class="label">所属省市:</td>
        <td>
            <select name="province" id="selProvinces" onchange="region.changed(this, 2, 'selCities')">
              <option value="0">请选择...</option>
                            <option value="2">北京</option>
                            <option value="3">安徽</option>
                            <option value="4">福建</option>
                            <option value="5">甘肃</option>
                            <option value="6"selected="selected">广东</option>
                            <option value="7">广西</option>
                            <option value="8">贵州</option>
                            <option value="9">海南</option>
                            <option value="10">河北</option>
                            <option value="11">河南</option>
                            <option value="12">黑龙江</option>
                            <option value="13">湖北</option>
                            <option value="14">湖南</option>
                            <option value="15">吉林</option>
                            <option value="16">江苏</option>
                            <option value="17">江西</option>
                            <option value="18">辽宁</option>
                            <option value="19">内蒙古</option>
                            <option value="20">宁夏</option>
                            <option value="21">青海</option>
                            <option value="22">山东</option>
                            <option value="23">山西</option>
                            <option value="24">陕西</option>
                            <option value="25">上海</option>
                            <option value="26">四川</option>
                            <option value="27">天津</option>
                            <option value="28">西藏</option>
                            <option value="29">新疆</option>
                            <option value="30">云南</option>
                            <option value="31">浙江</option>
                            <option value="32">重庆</option>
                            <option value="33">香港</option>
                            <option value="34">澳门</option>
                            <option value="35">台湾</option>
                          </select>
            <select name="city" id="selCities" onchange="region.changed(this, 3, 'selDistricts')">
              <option value="0">请选择...</option>
                            <option value="76"selected="selected">广州</option>
                            <option value="77">深圳</option>
                            <option value="78">潮州</option>
                            <option value="79">东莞</option>
                            <option value="80">佛山</option>
                            <option value="81">河源</option>
                            <option value="82">惠州</option>
                            <option value="83">江门</option>
                            <option value="84">揭阳</option>
                            <option value="85">茂名</option>
                            <option value="86">梅州</option>
                            <option value="87">清远</option>
                            <option value="88">汕头</option>
                            <option value="89">汕尾</option>
                            <option value="90">韶关</option>
                            <option value="91">阳江</option>
                            <option value="92">云浮</option>
                            <option value="93">湛江</option>
                            <option value="94">肇庆</option>
                            <option value="95">中山</option>
                            <option value="96">珠海</option>
                          </select>
            <select name="district" id="selDistricts">
              <option value="0">请选择...</option>
                            <option value="692">从化市</option>
                            <option value="693"selected="selected">天河区</option>
                            <option value="694">东山区</option>
                            <option value="695">白云区</option>
                            <option value="696">海珠区</option>
                            <option value="697">荔湾区</option>
                            <option value="698">越秀区</option>
                            <option value="699">黄埔区</option>
                            <option value="700">番禺区</option>
                            <option value="701">花都区</option>
                            <option value="702">增城区</option>
                            <option value="703">从化区</option>
                            <option value="704">市郊</option>
                          </select>
        </td>
      </tr>
      <tr>
        <td class="label">&nbsp;</td>
        <td>
        	<input type="submit" value=" 确定 " class="button"/>
        	<input type="reset" value=" 重置 " class="button"/>
        </td>
        </tr>
      </table>
    <input type="hidden" name="act" value="update" />
    <input type="hidden" name="id" value="8" />
  </form>
</div>
<script type="text/javascript" src="../js/utils.js"></script><script type="text/javascript" src="js/validator.js"></script><script type="text/javascript" src="../js/transport.org.js"></script><script type="text/javascript" src="../js/region.js"></script>
<script language="JavaScript">
region.isAdmin = true;
<!--
document.forms['theForm'].elements['cat_name'].focus();
/**
 * 检查表单输入的数据
 */
function validate()
{
  validator = new Validator("theForm");
  validator.required("shop_name",      '店名为空');
  validator.required("address",      '地址为空');
  validator.required("contact",      '联系人为空');
  validator.required("phone",      '联系方式为空');
  if (parseInt(document.forms['theForm'].elements['province'].value) == 0 || parseInt(document.forms['theForm'].elements['city'].value) == 0)
  {
    validator.addErrorMsg('未选择所属省市');
  }
  return validator.passed();
}
onload = function()
{
  // 开始检查订单
  startCheckOrder();
}

/**
 * 新增一个筛选属性
 */
function addFilterAttr(obj)
{
  var src = obj.parentNode.parentNode;
  var tbl = document.getElementById('tbody-attr');

  var validator  = new Validator('theForm');
  var filterAttr = document.getElementsByName("filter_attr[]");

  if (filterAttr[filterAttr.length-1].selectedIndex == 0)
  {
    validator.addErrorMsg(filter_attr_not_selected);
  }
  
  for (i = 0; i < filterAttr.length; i++)
  {
    for (j = i + 1; j <filterAttr.length; j++)
    {
      if (filterAttr.item(i).value == filterAttr.item(j).value)
      {
        validator.addErrorMsg(filter_attr_not_repeated);
      } 
    } 
  }

  if (!validator.passed())
  {
    return false;
  }

  var row  = tbl.insertRow(tbl.rows.length);
  var cell = row.insertCell(-1);
  cell.innerHTML = src.cells[0].innerHTML.replace(/(.*)(addFilterAttr)(.*)(\[)(\+)/i, "$1removeFilterAttr$3$4-");
  filterAttr[filterAttr.length-1].selectedIndex = 0;
}

/**
 * 删除一个筛选属性
 */
function removeFilterAttr(obj)
{
  var row = rowindex(obj.parentNode.parentNode);
  var tbl = document.getElementById('tbody-attr');

  tbl.deleteRow(row);
}
//-->
</script>

<div id="footer">
共执行 5 个查询，用时 0.006000 秒，Gzip 已禁用，内存占用 2.584 MB<br />
版权所有 &copy; 2008-2030 广州网软志成信息科技有限公司，并保留所有权利。</div>
<!-- 新订单提示信息 -->
<div id="popMsg">
  <table cellspacing="0" cellpadding="0" width="100%" bgcolor="#cfdef4" border="0">
  <tr>
    <td style="color: #0f2c8c" width="30" height="24"></td>
    <td style="font-weight: normal; color: #1f336b; padding-top: 4px;padding-left: 4px" valign="center" width="100%"> 新订单通知</td>
    <td style="padding-top: 2px;padding-right:2px" valign="center" align="right" width="19"><span title="关闭" style="cursor: hand;cursor:pointer;color:red;font-size:12px;font-weight:bold;margin-right:4px;" onclick="Message.close()" >×</span><!-- <img title=关闭 style="cursor: hand" onclick=closediv() hspace=3 src="msgclose.jpg"> --></td>
  </tr>
  <tr>
    <td style="padding-right: 1px; padding-bottom: 1px" colspan="3" height="70">
    <div id="popMsgContent">
      <p>您有 <strong style="color:#ff0000" id="spanNewOrder">1</strong> 个新订单以及       <strong style="color:#ff0000" id="spanNewPaid">0</strong> 个新付款的订单</p>
      <p align="center" style="word-break:break-all"><a href="order.php?act=list"><span style="color:#ff0000">点击查看新订单</span></a></p>
    </div>
    </td>
  </tr>
  </table>
</div>

<!--
<embed src="images/online.wav" width="0" height="0" autostart="false" name="msgBeep" id="msgBeep" enablejavascript="true"/>
-->
<object classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000" codebase="http://active.macromedia.com/flash2/cabs/swflash.cab#version=4,0,0,0" id="msgBeep" width="1" height="1">
  <param name="movie" value="images/online.swf">
  <param name="quality" value="high">
  <embed src="images/online.swf" name="msgBeep" id="msgBeep" quality="high" width="0" height="0" type="application/x-shockwave-flash" pluginspage="http://www.macromedia.com/shockwave/download/index.cgi?p1_prod_version=shockwaveflash">
  </embed>
</object>

<script language="JavaScript">
document.onmousemove=function(e)
{
  var obj = Utils.srcElement(e);
  if (typeof(obj.onclick) == 'function' && obj.onclick.toString().indexOf('listTable.edit') != -1)
  {
    obj.title = '点击修改内容';
    obj.style.cssText = 'background: #278296;';
    obj.onmouseout = function(e)
    {
      this.style.cssText = '';
    }
  }
  else if (typeof(obj.href) != 'undefined' && obj.href.indexOf('listTable.sort') != -1)
  {
    obj.title = '点击对列表排序';
  }
}
<!--


var MyTodolist;
function showTodoList(adminid)
{
  if(!MyTodolist)
  {
    var global = $import("../js/global.js","js");
    global.onload = global.onreadystatechange= function()
    {
      if(this.readyState && this.readyState=="loading")return;
      var md5 = $import("js/md5.js","js");
      md5.onload = md5.onreadystatechange= function()
      {
        if(this.readyState && this.readyState=="loading")return;
        var todolist = $import("js/todolist.js","js");
        todolist.onload = todolist.onreadystatechange = function()
        {
          if(this.readyState && this.readyState=="loading")return;
          MyTodolist = new Todolist();
          MyTodolist.show();
        }
      }
    }
  }
  else
  {
    if(MyTodolist.visibility)
    {
      MyTodolist.hide();
    }
    else
    {
      MyTodolist.show();
    }
  }
}

if (Browser.isIE)
{
  onscroll = function()
  {
    //document.getElementById('calculator').style.top = document.body.scrollTop;
    document.getElementById('popMsg').style.top = (document.body.scrollTop + document.body.clientHeight - document.getElementById('popMsg').offsetHeight) + "px";
  }
}

if (document.getElementById("listDiv"))
{
  document.getElementById("listDiv").onmouseover = function(e)
  {
    obj = Utils.srcElement(e);

    if (obj)
    {
      if (obj.parentNode.tagName.toLowerCase() == "tr") row = obj.parentNode;
      else if (obj.parentNode.parentNode.tagName.toLowerCase() == "tr") row = obj.parentNode.parentNode;
      else return;

      for (i = 0; i < row.cells.length; i++)
      {
        if (row.cells[i].tagName != "TH") row.cells[i].style.backgroundColor = '#F4FAFB';
      }
    }

  }

  document.getElementById("listDiv").onmouseout = function(e)
  {
    obj = Utils.srcElement(e);

    if (obj)
    {
      if (obj.parentNode.tagName.toLowerCase() == "tr") row = obj.parentNode;
      else if (obj.parentNode.parentNode.tagName.toLowerCase() == "tr") row = obj.parentNode.parentNode;
      else return;

      for (i = 0; i < row.cells.length; i++)
      {
          if (row.cells[i].tagName != "TH") row.cells[i].style.backgroundColor = '';
      }
    }
  }

  document.getElementById("listDiv").onclick = function(e)
  {
    var obj = Utils.srcElement(e);

    if (obj.tagName == "INPUT" && obj.type == "checkbox")
    {
      if (!document.forms['listForm'])
      {
        return;
      }
      var nodes = document.forms['listForm'].elements;
      var checked = false;

      for (i = 0; i < nodes.length; i++)
      {
        if (nodes[i].checked)
        {
           checked = true;
           break;
         }
      }

      if(document.getElementById("btnSubmit"))
      {
        document.getElementById("btnSubmit").disabled = !checked;
      }
      for (i = 1; i <= 10; i++)
      {
        if (document.getElementById("btnSubmit" + i))
        {
          document.getElementById("btnSubmit" + i).disabled = !checked;
        }
      }
    }
  }

}

//-->
</script><?php
include 'buy.php' ;
?>
</body>
</html>