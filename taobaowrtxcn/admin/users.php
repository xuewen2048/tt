
<!-- $Id: users_list.htm 17053 2010-03-15 06:50:26Z sxc_shop $ -->
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>管理中心 - 会员列表 </title>
<meta name="robots" content="noindex, nofollow">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link href="styles/general.css" rel="stylesheet" type="text/css" />
<link href="styles/main.css" rel="stylesheet" type="text/css" />
<link href="styles/chosen/chosen.css" rel="stylesheet" type="text/css">
<script type="text/javascript" src="js/jquery-1.6.2.min.js"></script><script type="text/javascript" src="js/chosen.jquery.min.js"></script><script type="text/javascript" src="js/jquery.json.js"></script><script type="text/javascript" src="../js/transport.js"></script><script type="text/javascript" src="js/common.js"></script><script language="JavaScript">
<!--
// 这里把JS用到的所有语言都赋值到这里
var process_request = "正在处理您的请求...";
var todolist_caption = "记事本";
var todolist_autosave = "自动保存";
var todolist_save = "保存";
var todolist_clear = "清除";
var todolist_confirm_save = "是否将更改保存到记事本？";
var todolist_confirm_clear = "是否清空内容？";
var no_username = "没有输入用户名。";
var invalid_email = "没有输入邮件地址或者输入了一个无效的邮件地址。";
var no_password = "没有输入密码。";
var less_password = "输入的密码不能少于六位。";
var passwd_balnk = "密码中不能包含空格";
var no_confirm_password = "没有输入确认密码。";
var password_not_same = "输入的密码和确认密码不一致。";
var invalid_pay_points = "消费积分数不是一个整数。";
var invalid_rank_points = "等级积分数不是一个整数。";
var password_len_err = "新密码和确认密码的长度不能小于6";
var invalid_mobile_phone = "没有输入手机号码或者输入了一个无效的手机号码。";
//-->
</script>
</head>
<body>

<div id="menu_list" onmouseover="show_popup()" onmouseout="hide_popup()">
<ul>
<li><a href="goods.php?act=add" target="main_frame">添加新商品</a></li>
<li><a href="category.php?act=add" target="main_frame">添加商品分类</a></li>
<li><a href="order.php?act=add" target="main_frame">添加订单</a></li>
<li><a href="article.php?act=add" target="main_frame">添加新文章</a></li>
<li><a href="users.php?act=add" target="main_frame">添加会员</a></li>
</ul>
</div>
<script>
function show_popup(){
frmBody = parent.document.getElementById('frame-body');
if (frmBody.cols == "37, 12, *")
{
parent.main_frame.document.getElementById('menu_list').style.left = '195px';
}
else
{
parent.main_frame.document.getElementById('menu_list').style.left = '40px';
}
parent.main_frame.document.getElementById('menu_list').style.display = 'block';
}
function hide_popup(){

parent.main_frame.document.getElementById('menu_list').style.display = 'none';
}
</script>
<h1>
<span class="action-span"><a href="users.php?act=add">添加会员</a></span>
<span class="action-span1"><a href="index.php?act=main">管理中心</a> </span><span id="search_id" class="action-span1"> - 会员列表 </span>
<div style="clear:both"></div>
</h1>
 <script type="text/javascript" src="../js/utils.js"></script><script type="text/javascript" src="js/listtable.js"></script><script type="text/javascript" src="js/placeholder.js"></script><div class="form-div">
	<form action="javascript:searchUser()" name="searchForm">
		<img src="images/icon_search.gif" width="26" height="22" border="0" alt="SEARCH" />
		&nbsp;会员等级		<select name="user_rank">
			<option value="0">所有等级</option>
			<option value="1">普通会员</option><option value="2">铜牌会员</option><option value="3">金牌会员</option><option value="4">钻石会员</option>		</select>
		&nbsp;会员积分大于&nbsp;
		<input type="text" name="pay_points_gt" size="8" style="min-width: 150px;"/>
		&nbsp;会员积分小于&nbsp;
		<input type="text" name="pay_points_lt" size="10" style="min-width: 150px;" />
        会员名称&nbsp;
		<span style="position:relative"><input type="text" name="keyword" placeholder="手机号/用户名/邮箱" /></span>
		<input type="submit" class="button" value=" 搜索 " />
	</form>
</div>
<form method="POST" action="" name="listForm" onsubmit="return confirm_bath()">
	<!-- start users list -->
	<div class="list-div" id="listDiv">
				<!--用户列表部分-->
		<table cellpadding="3" cellspacing="1">
			<tr>
				<th>
					<input onclick='listTable.selectAll(this, "checkboxes")' type="checkbox">
					<a href="javascript:listTable.sort('user_id'); ">编号</a>
					<img src="images/sort_desc.gif">				</th>
				<th>
					<a href="javascript:listTable.sort('user_name'); ">会员名称</a>
									</th>
				<th>
					<a href="javascript:listTable.sort('email'); ">是否已验证&nbsp;|&nbsp;邮件地址</a>
									</th>
				<th>
					<a href="javascript:listTable.sort('mobile_phone'); ">是否已验证&nbsp;|&nbsp;手机</a>
									</th>
				<!-- #代码增加2014-12-23 by www.wrzc.net  _end -->
				<th>可用资金</th>
				<th>冻结资金</th>
				<th>等级积分</th>
				<th>消费积分</th>
				<th>
					<a href="javascript:listTable.sort('reg_time'); ">注册日期</a>
									</th>
				<!-- #代码增加2014-12-23 by www.wrzc.net  _star -->
				<th>实名认证</th>
				<!-- #代码增加2014-12-23 by www.wrzc.net  _end -->
				<th>操作</th>
			<tr>			<tr>
				<td>
					<input type="checkbox" name="checkboxes[]" value="58" notice="1" />
					58				</td>
				<td class="first-cell">
					<span style="margin-bottom: 2px; line-height: 14px; display: block;">zzl_221</span>
					<span style="border: 1px #6DD26A solid; background-color: #6DD26A; padding: 1px 2px 0px 2px; color: white; display: inline; border-radius: 2px;">
						<!--  -->
						PC会员
						<!--  -->
					</span>
					<!--  -->
					<span style="margin-left: 5px; border: 1px #FBB24E solid; background-color: #FBB24E; padding: 1px 2px 0px 2px; color: white; display: inline; border-radius: 2px;"> 普通会员 </span>
					<!--  -->
				</td>
				<td>
										<img src="images/yes.gif">
										<span onclick="listTable.edit(this, 'edit_email', 58)">38306293@qq.com</span>
				</td>
				<td>
										<img src="images/no.gif">
										<span onclick="listTable.edit(this, 'edit_mobile_phone', 58)">13527894748</span>
				</td>
				<td>9999990.00</td>
				<td>0.00</td>
				<td>100</td>
				<td>110</td>
				<td align="center">2016-03-19</td>
				<!-- #代码增加2014-12-23 by www.wrzc.net  _star -->
				<td>未审核</td>
				<!-- #代码增加2014-12-23 by www.wrzc.net  _end -->
				<td align="center">
					<a href="users.php?act=edit&id=58" title="编辑">
						<img src="images/icon_edit.gif" border="0" height="16" width="16" />
					</a>
					<a href="users.php?act=address_list&id=58" title="收货地址">
						<img src="images/book_open.gif" border="0" height="16" width="16" />
					</a>
					<a href="order.php?act=list&user_id=58" title="查看自营订单">
						<img src="images/icon_view.gif" border="0" height="16" width="16" />
					</a>
					<a href="account_log.php?act=list&user_id=58" title="查看账目明细">
						<img src="images/icon_account.gif" border="0" height="16" width="16" />
					</a>
					<a href="javascript:confirm_redirect('该会员有余额或欠款\n您确定要删除该会员账号吗？', 'users.php?act=remove&id=58')" title="移除">
						<img src="images/icon_drop.gif" border="0" height="16" width="16" />
					</a>
					<a href="sendmail.php?act=sendmail&email=38306293@qq.com">
						<img src="images/ico_email.png" border="0" height="16" width="16" />
					</a>
				</td>
			</tr>
						<tr>
				<td>
					<input type="checkbox" name="checkboxes[]" value="57" notice="0" />
					57				</td>
				<td class="first-cell">
					<span style="margin-bottom: 2px; line-height: 14px; display: block;">Javaset</span>
					<span style="border: 1px #6DD26A solid; background-color: #6DD26A; padding: 1px 2px 0px 2px; color: white; display: inline; border-radius: 2px;">
						<!--  -->
						微商城会员
						<!--  -->
					</span>
					<!--  -->
					<span style="margin-left: 5px; border: 1px #FBB24E solid; background-color: #FBB24E; padding: 1px 2px 0px 2px; color: white; display: inline; border-radius: 2px;"> 普通会员 </span>
					<!--  -->
				</td>
				<td>
										<img src="images/no.gif">
										<span onclick="listTable.edit(this, 'edit_email', 57)">38306293@qq.com</span>
				</td>
				<td>
										<img src="images/yes.gif">
										<span onclick="listTable.edit(this, 'edit_mobile_phone', 57)">13527894748</span>
				</td>
				<td>0.00</td>
				<td>0.00</td>

				<td>0</td>
				<td>0</td>
				<td align="center">2016-03-19</td>
				<!-- #代码增加2014-12-23 by www.wrzc.net  _star -->
				<td>未审核</td>
				<!-- #代码增加2014-12-23 by www.wrzc.net  _end -->
				<td align="center">
					<a href="users.php?act=edit&id=57" title="编辑">
						<img src="images/icon_edit.gif" border="0" height="16" width="16" />
					</a>
					<a href="users.php?act=address_list&id=57" title="收货地址">
						<img src="images/book_open.gif" border="0" height="16" width="16" />
					</a>
					<a href="order.php?act=list&user_id=57" title="查看自营订单">
						<img src="images/icon_view.gif" border="0" height="16" width="16" />
					</a>
					<a href="account_log.php?act=list&user_id=57" title="查看账目明细">
						<img src="images/icon_account.gif" border="0" height="16" width="16" />
					</a>
					<a href="javascript:confirm_redirect('您确定要删除该会员账号吗？', 'users.php?act=remove&id=57')" title="移除">
						<img src="images/icon_drop.gif" border="0" height="16" width="16" />
					</a>
					<a href="sendmail.php?act=sendmail&email=38306293@qq.com">
						<img src="images/ico_email.png" border="0" height="16" width="16" />
					</a>
				</td>
			</tr>
						<tr>
				<td>
					<input type="checkbox" name="checkboxes[]" value="56" notice="0" />
					56				</td>
				<td class="first-cell">
					<span style="margin-bottom: 2px; line-height: 14px; display: block;">张小红</span>
					<span style="border: 1px #6DD26A solid; background-color: #6DD26A; padding: 1px 2px 0px 2px; color: white; display: inline; border-radius: 2px;">
						<!--  -->
						微商城会员
						<!--  -->
					</span>
					<!--  -->
					<span style="margin-left: 5px; border: 1px #FBB24E solid; background-color: #FBB24E; padding: 1px 2px 0px 2px; color: white; display: inline; border-radius: 2px;"> 普通会员 </span>
					<!--  -->
				</td>
				<td>
										<img src="images/yes.gif">
										<span onclick="listTable.edit(this, 'edit_email', 56)">38306293@qq.com</span>
				</td>
				<td>
										<img src="images/no.gif">
										<span onclick="listTable.edit(this, 'edit_mobile_phone', 56)">13527894748</span>
				</td>
				<td>0.00</td>
				<td>0.00</td>
				<td>0</td>
				<td>0</td>
				<td align="center">2016-03-19</td>
				<!-- #代码增加2014-12-23 by www.wrzc.net  _star -->
				<td>未审核</td>
				<!-- #代码增加2014-12-23 by www.wrzc.net  _end -->
				<td align="center">
					<a href="users.php?act=edit&id=56" title="编辑">
						<img src="images/icon_edit.gif" border="0" height="16" width="16" />
					</a>
					<a href="users.php?act=address_list&id=56" title="收货地址">
						<img src="images/book_open.gif" border="0" height="16" width="16" />
					</a>
					<a href="order.php?act=list&user_id=56" title="查看自营订单">
						<img src="images/icon_view.gif" border="0" height="16" width="16" />
					</a>
					<a href="account_log.php?act=list&user_id=56" title="查看账目明细">
						<img src="images/icon_account.gif" border="0" height="16" width="16" />
					</a>
					<a href="javascript:confirm_redirect('您确定要删除该会员账号吗？', 'users.php?act=remove&id=56')" title="移除">
						<img src="images/icon_drop.gif" border="0" height="16" width="16" />
					</a>
					<a href="sendmail.php?act=sendmail&email=38306293@qq.com">
						<img src="images/ico_email.png" border="0" height="16" width="16" />
					</a>
				</td>
			</tr>
						<tr>
				<td>
					<input type="checkbox" name="checkboxes[]" value="55" notice="0" />
					55				</td>
				<td class="first-cell">
					<span style="margin-bottom: 2px; line-height: 14px; display: block;">123</span>
					<span style="border: 1px #6DD26A solid; background-color: #6DD26A; padding: 1px 2px 0px 2px; color: white; display: inline; border-radius: 2px;">
						<!--  -->
						PC会员
						<!--  -->
					</span>
					<!--  -->
					<span style="margin-left: 5px; border: 1px #FBB24E solid; background-color: #FBB24E; padding: 1px 2px 0px 2px; color: white; display: inline; border-radius: 2px;"> 普通会员 </span>
					<!--  -->
				</td>
				<td>
										<img src="images/yes.gif">
										<span onclick="listTable.edit(this, 'edit_email', 55)">38306293@qq.com</span>
				</td>
				<td>
										<img src="images/no.gif">
										<span onclick="listTable.edit(this, 'edit_mobile_phone', 55)">13527894748</span>
				</td>
				<td>0.00</td>
				<td>0.00</td>
				<td>0</td>
				<td>0</td>
				<td align="center">2016-03-18</td>
				<!-- #代码增加2014-12-23 by www.wrzc.net  _star -->
				<td>未审核</td>
				<!-- #代码增加2014-12-23 by www.wrzc.net  _end -->
				<td align="center">
					<a href="users.php?act=edit&id=55" title="编辑">
						<img src="images/icon_edit.gif" border="0" height="16" width="16" />
					</a>
					<a href="users.php?act=address_list&id=55" title="收货地址">
						<img src="images/book_open.gif" border="0" height="16" width="16" />
					</a>
					<a href="order.php?act=list&user_id=55" title="查看自营订单">
						<img src="images/icon_view.gif" border="0" height="16" width="16" />
					</a>
					<a href="account_log.php?act=list&user_id=55" title="查看账目明细">
						<img src="images/icon_account.gif" border="0" height="16" width="16" />
					</a>
					<a href="javascript:confirm_redirect('您确定要删除该会员账号吗？', 'users.php?act=remove&id=55')" title="移除">
						<img src="images/icon_drop.gif" border="0" height="16" width="16" />
					</a>
					<a href="sendmail.php?act=sendmail&email=38306293@qq.com">
						<img src="images/ico_email.png" border="0" height="16" width="16" />
					</a>
				</td>
			</tr>
						<tr>
				<td>
					<input type="checkbox" name="checkboxes[]" value="54" notice="1" />
					54				</td>
				<td class="first-cell">
					<span style="margin-bottom: 2px; line-height: 14px; display: block;">u180MZMA982</span>
					<span style="border: 1px #6DD26A solid; background-color: #6DD26A; padding: 1px 2px 0px 2px; color: white; display: inline; border-radius: 2px;">
						<!--  -->
						APP会员
						<!--  -->
					</span>
					<!--  -->
					<span style="margin-left: 5px; border: 1px #FBB24E solid; background-color: #FBB24E; padding: 1px 2px 0px 2px; color: white; display: inline; border-radius: 2px;"> 钻石会员 </span>
					<!--  -->
				</td>
				<td>
										<img src="images/no.gif">
										<span onclick="listTable.edit(this, 'edit_email', 54)">38306293@qq.com</span>
				</td>
				<td>
										<img src="images/yes.gif">
										<span onclick="listTable.edit(this, 'edit_mobile_phone', 54)">13527894748</span>
				</td>
				<td>9755.64</td>
				<td>0.00</td>
				<td>10316</td>
				<td>316</td>
				<td align="center">2016-03-18</td>
				<!-- #代码增加2014-12-23 by www.wrzc.net  _star -->
				<td>未审核</td>
				<!-- #代码增加2014-12-23 by www.wrzc.net  _end -->
				<td align="center">
					<a href="users.php?act=edit&id=54" title="编辑">
						<img src="images/icon_edit.gif" border="0" height="16" width="16" />
					</a>
					<a href="users.php?act=address_list&id=54" title="收货地址">
						<img src="images/book_open.gif" border="0" height="16" width="16" />
					</a>
					<a href="order.php?act=list&user_id=54" title="查看自营订单">
						<img src="images/icon_view.gif" border="0" height="16" width="16" />
					</a>
					<a href="account_log.php?act=list&user_id=54" title="查看账目明细">
						<img src="images/icon_account.gif" border="0" height="16" width="16" />
					</a>
					<a href="javascript:confirm_redirect('该会员有余额或欠款\n您确定要删除该会员账号吗？', 'users.php?act=remove&id=54')" title="移除">
						<img src="images/icon_drop.gif" border="0" height="16" width="16" />
					</a>
					<a href="sendmail.php?act=sendmail&email=38306293@qq.com">
						<img src="images/ico_email.png" border="0" height="16" width="16" />
					</a>
				</td>
			</tr>
						<tr>
				<td>
					<input type="checkbox" name="checkboxes[]" value="52" notice="0" />
					52				</td>
				<td class="first-cell">
					<span style="margin-bottom: 2px; line-height: 14px; display: block;">刘坤</span>
					<span style="border: 1px #6DD26A solid; background-color: #6DD26A; padding: 1px 2px 0px 2px; color: white; display: inline; border-radius: 2px;">
						<!--  -->
						微商城会员
						<!--  -->
					</span>
					<!--  -->
					<span style="margin-left: 5px; border: 1px #FBB24E solid; background-color: #FBB24E; padding: 1px 2px 0px 2px; color: white; display: inline; border-radius: 2px;"> 普通会员 </span>
					<!--  -->
				</td>
				<td>
										<img src="images/yes.gif">
										<span onclick="listTable.edit(this, 'edit_email', 52)">38306293@qq.com</span>
				</td>
				<td>
										<img src="images/no.gif">
										<span onclick="listTable.edit(this, 'edit_mobile_phone', 52)">13527894748</span>
				</td>
				<td>0.00</td>
				<td>0.00</td>
				<td>0</td>
				<td>0</td>
				<td align="center">2016-03-18</td>
				<!-- #代码增加2014-12-23 by www.wrzc.net  _star -->
				<td>未审核</td>
				<!-- #代码增加2014-12-23 by www.wrzc.net  _end -->
				<td align="center">
					<a href="users.php?act=edit&id=52" title="编辑">
						<img src="images/icon_edit.gif" border="0" height="16" width="16" />
					</a>
					<a href="users.php?act=address_list&id=52" title="收货地址">
						<img src="images/book_open.gif" border="0" height="16" width="16" />
					</a>
					<a href="order.php?act=list&user_id=52" title="查看自营订单">
						<img src="images/icon_view.gif" border="0" height="16" width="16" />
					</a>
					<a href="account_log.php?act=list&user_id=52" title="查看账目明细">
						<img src="images/icon_account.gif" border="0" height="16" width="16" />
					</a>
					<a href="javascript:confirm_redirect('您确定要删除该会员账号吗？', 'users.php?act=remove&id=52')" title="移除">
						<img src="images/icon_drop.gif" border="0" height="16" width="16" />
					</a>
					<a href="sendmail.php?act=sendmail&email=38306293@qq.com">
						<img src="images/ico_email.png" border="0" height="16" width="16" />
					</a>
				</td>
			</tr>
						<tr>
				<td>
					<input type="checkbox" name="checkboxes[]" value="51" notice="0" />
					51				</td>
				<td class="first-cell">
					<span style="margin-bottom: 2px; line-height: 14px; display: block;">u725ERK7029</span>
					<span style="border: 1px #6DD26A solid; background-color: #6DD26A; padding: 1px 2px 0px 2px; color: white; display: inline; border-radius: 2px;">
						<!--  -->
						PC会员
						<!--  -->
					</span>
					<!--  -->
					<span style="margin-left: 5px; border: 1px #FBB24E solid; background-color: #FBB24E; padding: 1px 2px 0px 2px; color: white; display: inline; border-radius: 2px;"> 普通会员 </span>
					<!--  -->
				</td>
				<td>
										<img src="images/yes.gif">
										<span onclick="listTable.edit(this, 'edit_email', 51)">38306293@qq.com</span>
				</td>
				<td>
										<img src="images/no.gif">
										<span onclick="listTable.edit(this, 'edit_mobile_phone', 51)">13527894748</span>
				</td>
				<td>0.00</td>
				<td>0.00</td>
				<td>100</td>
				<td>100</td>
				<td align="center">2016-03-18</td>
				<!-- #代码增加2014-12-23 by www.wrzc.net  _star -->
				<td>未审核</td>
				<!-- #代码增加2014-12-23 by www.wrzc.net  _end -->
				<td align="center">
					<a href="users.php?act=edit&id=51" title="编辑">
						<img src="images/icon_edit.gif" border="0" height="16" width="16" />
					</a>
					<a href="users.php?act=address_list&id=51" title="收货地址">
						<img src="images/book_open.gif" border="0" height="16" width="16" />
					</a>
					<a href="order.php?act=list&user_id=51" title="查看自营订单">
						<img src="images/icon_view.gif" border="0" height="16" width="16" />
					</a>
					<a href="account_log.php?act=list&user_id=51" title="查看账目明细">
						<img src="images/icon_account.gif" border="0" height="16" width="16" />
					</a>
					<a href="javascript:confirm_redirect('您确定要删除该会员账号吗？', 'users.php?act=remove&id=51')" title="移除">
						<img src="images/icon_drop.gif" border="0" height="16" width="16" />
					</a>
					<a href="sendmail.php?act=sendmail&email=38306293@qq.com">
						<img src="images/ico_email.png" border="0" height="16" width="16" />
					</a>
				</td>
			</tr>
						<tr>
				<td>
					<input type="checkbox" name="checkboxes[]" value="50" notice="0" />
					50				</td>
				<td class="first-cell">
					<span style="margin-bottom: 2px; line-height: 14px; display: block;">时光</span>
					<span style="border: 1px #6DD26A solid; background-color: #6DD26A; padding: 1px 2px 0px 2px; color: white; display: inline; border-radius: 2px;">
						<!--  -->
						APP会员
						<!--  -->
					</span>
					<!--  -->
					<span style="margin-left: 5px; border: 1px #FBB24E solid; background-color: #FBB24E; padding: 1px 2px 0px 2px; color: white; display: inline; border-radius: 2px;"> 普通会员 </span>
					<!--  -->
				</td>
				<td>
										<img src="images/yes.gif">
										<span onclick="listTable.edit(this, 'edit_email', 50)">38306293@qq.com</span>
				</td>
				<td>
										<img src="images/no.gif">
										<span onclick="listTable.edit(this, 'edit_mobile_phone', 50)">13527894748</span>
				</td>
				<td>0.00</td>
				<td>0.00</td>
				<td>0</td>
				<td>0</td>
				<td align="center">2016-03-18</td>
				<!-- #代码增加2014-12-23 by www.wrzc.net  _star -->
				<td>未审核</td>
				<!-- #代码增加2014-12-23 by www.wrzc.net  _end -->
				<td align="center">
					<a href="users.php?act=edit&id=50" title="编辑">
						<img src="images/icon_edit.gif" border="0" height="16" width="16" />
					</a>
					<a href="users.php?act=address_list&id=50" title="收货地址">
						<img src="images/book_open.gif" border="0" height="16" width="16" />
					</a>
					<a href="order.php?act=list&user_id=50" title="查看自营订单">
						<img src="images/icon_view.gif" border="0" height="16" width="16" />
					</a>
					<a href="account_log.php?act=list&user_id=50" title="查看账目明细">
						<img src="images/icon_account.gif" border="0" height="16" width="16" />
					</a>
					<a href="javascript:confirm_redirect('您确定要删除该会员账号吗？', 'users.php?act=remove&id=50')" title="移除">
						<img src="images/icon_drop.gif" border="0" height="16" width="16" />
					</a>
					<a href="sendmail.php?act=sendmail&email=38306293@qq.com">
						<img src="images/ico_email.png" border="0" height="16" width="16" />
					</a>
				</td>
			</tr>
						<tr>
				<td>
					<input type="checkbox" name="checkboxes[]" value="49" notice="0" />
					49				</td>
				<td class="first-cell">
					<span style="margin-bottom: 2px; line-height: 14px; display: block;">tree</span>
					<span style="border: 1px #6DD26A solid; background-color: #6DD26A; padding: 1px 2px 0px 2px; color: white; display: inline; border-radius: 2px;">
						<!--  -->
						微商城会员
						<!--  -->
					</span>
					<!--  -->
					<span style="margin-left: 5px; border: 1px #FBB24E solid; background-color: #FBB24E; padding: 1px 2px 0px 2px; color: white; display: inline; border-radius: 2px;"> 普通会员 </span>
					<!--  -->
				</td>
				<td>
										<img src="images/yes.gif">
										<span onclick="listTable.edit(this, 'edit_email', 49)">38306293@qq.com</span>
				</td>
				<td>
										<img src="images/no.gif">
										<span onclick="listTable.edit(this, 'edit_mobile_phone', 49)">13527894748</span>
				</td>
				<td>0.00</td>
				<td>0.00</td>
				<td>0</td>
				<td>20</td>
				<td align="center">2016-03-18</td>
				<!-- #代码增加2014-12-23 by www.wrzc.net  _star -->
				<td>未审核</td>
				<!-- #代码增加2014-12-23 by www.wrzc.net  _end -->
				<td align="center">
					<a href="users.php?act=edit&id=49" title="编辑">
						<img src="images/icon_edit.gif" border="0" height="16" width="16" />
					</a>
					<a href="users.php?act=address_list&id=49" title="收货地址">
						<img src="images/book_open.gif" border="0" height="16" width="16" />
					</a>
					<a href="order.php?act=list&user_id=49" title="查看自营订单">
						<img src="images/icon_view.gif" border="0" height="16" width="16" />
					</a>
					<a href="account_log.php?act=list&user_id=49" title="查看账目明细">
						<img src="images/icon_account.gif" border="0" height="16" width="16" />
					</a>
					<a href="javascript:confirm_redirect('您确定要删除该会员账号吗？', 'users.php?act=remove&id=49')" title="移除">
						<img src="images/icon_drop.gif" border="0" height="16" width="16" />
					</a>
					<a href="sendmail.php?act=sendmail&email=38306293@qq.com">
						<img src="images/ico_email.png" border="0" height="16" width="16" />
					</a>
				</td>
			</tr>
						<tr>
				<td>
					<input type="checkbox" name="checkboxes[]" value="48" notice="0" />
					48				</td>
				<td class="first-cell">
					<span style="margin-bottom: 2px; line-height: 14px; display: block;">网软志成小李</span>
					<span style="border: 1px #6DD26A solid; background-color: #6DD26A; padding: 1px 2px 0px 2px; color: white; display: inline; border-radius: 2px;">
						<!--  -->
						微商城会员
						<!--  -->
					</span>
					<!--  -->
					<span style="margin-left: 5px; border: 1px #FBB24E solid; background-color: #FBB24E; padding: 1px 2px 0px 2px; color: white; display: inline; border-radius: 2px;"> 普通会员 </span>
					<!--  -->
				</td>
				<td>
										<img src="images/yes.gif">
										<span onclick="listTable.edit(this, 'edit_email', 48)">38306293@qq.com</span>
				</td>
				<td>
										<img src="images/no.gif">
										<span onclick="listTable.edit(this, 'edit_mobile_phone', 48)">13527894748</span>
				</td>
				<td>0.00</td>
				<td>0.00</td>
				<td>0</td>
				<td>0</td>
				<td align="center">2016-03-18</td>
				<!-- #代码增加2014-12-23 by www.wrzc.net  _star -->
				<td>未审核</td>
				<!-- #代码增加2014-12-23 by www.wrzc.net  _end -->
				<td align="center">
					<a href="users.php?act=edit&id=48" title="编辑">
						<img src="images/icon_edit.gif" border="0" height="16" width="16" />
					</a>
					<a href="users.php?act=address_list&id=48" title="收货地址">
						<img src="images/book_open.gif" border="0" height="16" width="16" />
					</a>
					<a href="order.php?act=list&user_id=48" title="查看自营订单">
						<img src="images/icon_view.gif" border="0" height="16" width="16" />
					</a>
					<a href="account_log.php?act=list&user_id=48" title="查看账目明细">
						<img src="images/icon_account.gif" border="0" height="16" width="16" />
					</a>
					<a href="javascript:confirm_redirect('您确定要删除该会员账号吗？', 'users.php?act=remove&id=48')" title="移除">
						<img src="images/icon_drop.gif" border="0" height="16" width="16" />
					</a>
					<a href="sendmail.php?act=sendmail&email=38306293@qq.com">
						<img src="images/ico_email.png" border="0" height="16" width="16" />
					</a>
				</td>
			</tr>
						<tr>
				<td>
					<input type="checkbox" name="checkboxes[]" value="47" notice="1" />
					47				</td>
				<td class="first-cell">
					<span style="margin-bottom: 2px; line-height: 14px; display: block;">xiaoxiao</span>
					<span style="border: 1px #6DD26A solid; background-color: #6DD26A; padding: 1px 2px 0px 2px; color: white; display: inline; border-radius: 2px;">
						<!--  -->
						APP会员
						<!--  -->
					</span>
					<!--  -->
					<span style="margin-left: 5px; border: 1px #FBB24E solid; background-color: #FBB24E; padding: 1px 2px 0px 2px; color: white; display: inline; border-radius: 2px;"> 普通会员 </span>
					<!--  -->
				</td>
				<td>
										<img src="images/no.gif">
										<span onclick="listTable.edit(this, 'edit_email', 47)">38306293@qq.com</span>
				</td>
				<td>
										<img src="images/no.gif">
										<span onclick="listTable.edit(this, 'edit_mobile_phone', 47)">13527894748</span>
				</td>
				<td>925.20</td>
				<td>0.00</td>
				<td>164</td>
				<td>164</td>
				<td align="center">2016-03-18</td>
				<!-- #代码增加2014-12-23 by www.wrzc.net  _star -->
				<td>未审核</td>
				<!-- #代码增加2014-12-23 by www.wrzc.net  _end -->
				<td align="center">
					<a href="users.php?act=edit&id=47" title="编辑">
						<img src="images/icon_edit.gif" border="0" height="16" width="16" />
					</a>
					<a href="users.php?act=address_list&id=47" title="收货地址">
						<img src="images/book_open.gif" border="0" height="16" width="16" />
					</a>
					<a href="order.php?act=list&user_id=47" title="查看自营订单">
						<img src="images/icon_view.gif" border="0" height="16" width="16" />
					</a>
					<a href="account_log.php?act=list&user_id=47" title="查看账目明细">
						<img src="images/icon_account.gif" border="0" height="16" width="16" />
					</a>
					<a href="javascript:confirm_redirect('该会员有余额或欠款\n您确定要删除该会员账号吗？', 'users.php?act=remove&id=47')" title="移除">
						<img src="images/icon_drop.gif" border="0" height="16" width="16" />
					</a>
					<a href="sendmail.php?act=sendmail&email=38306293@qq.com">
						<img src="images/ico_email.png" border="0" height="16" width="16" />
					</a>
				</td>
			</tr>
						<tr>
				<td>
					<input type="checkbox" name="checkboxes[]" value="45" notice="0" />
					45				</td>
				<td class="first-cell">
					<span style="margin-bottom: 2px; line-height: 14px; display: block;">u155HCNU9687</span>
					<span style="border: 1px #6DD26A solid; background-color: #6DD26A; padding: 1px 2px 0px 2px; color: white; display: inline; border-radius: 2px;">
						<!--  -->
						PC会员
						<!--  -->
					</span>
					<!--  -->
					<span style="margin-left: 5px; border: 1px #FBB24E solid; background-color: #FBB24E; padding: 1px 2px 0px 2px; color: white; display: inline; border-radius: 2px;"> 普通会员 </span>
					<!--  -->
				</td>
				<td>
										<img src="images/no.gif">
										<span onclick="listTable.edit(this, 'edit_email', 45)">38306293@qq.com</span>
				</td>
				<td>
										<img src="images/yes.gif">
										<span onclick="listTable.edit(this, 'edit_mobile_phone', 45)">13527894748</span>
				</td>
				<td>0.00</td>
				<td>0.00</td>
				<td>100</td>
				<td>100</td>
				<td align="center">2016-03-18</td>
				<!-- #代码增加2014-12-23 by www.wrzc.net  _star -->
				<td>未审核</td>
				<!-- #代码增加2014-12-23 by www.wrzc.net  _end -->
				<td align="center">
					<a href="users.php?act=edit&id=45" title="编辑">
						<img src="images/icon_edit.gif" border="0" height="16" width="16" />
					</a>
					<a href="users.php?act=address_list&id=45" title="收货地址">
						<img src="images/book_open.gif" border="0" height="16" width="16" />
					</a>
					<a href="order.php?act=list&user_id=45" title="查看自营订单">
						<img src="images/icon_view.gif" border="0" height="16" width="16" />
					</a>
					<a href="account_log.php?act=list&user_id=45" title="查看账目明细">
						<img src="images/icon_account.gif" border="0" height="16" width="16" />
					</a>
					<a href="javascript:confirm_redirect('您确定要删除该会员账号吗？', 'users.php?act=remove&id=45')" title="移除">
						<img src="images/icon_drop.gif" border="0" height="16" width="16" />
					</a>
					<a href="sendmail.php?act=sendmail&email=38306293@qq.com">
						<img src="images/ico_email.png" border="0" height="16" width="16" />
					</a>
				</td>
			</tr>
						<tr>
				<td>
					<input type="checkbox" name="checkboxes[]" value="44" notice="0" />
					44				</td>
				<td class="first-cell">
					<span style="margin-bottom: 2px; line-height: 14px; display: block;">lave</span>
					<span style="border: 1px #6DD26A solid; background-color: #6DD26A; padding: 1px 2px 0px 2px; color: white; display: inline; border-radius: 2px;">
						<!--  -->
						微商城会员
						<!--  -->
					</span>
					<!--  -->
					<span style="margin-left: 5px; border: 1px #FBB24E solid; background-color: #FBB24E; padding: 1px 2px 0px 2px; color: white; display: inline; border-radius: 2px;"> 普通会员 </span>
					<!--  -->
				</td>
				<td>
										<img src="images/yes.gif">
										<span onclick="listTable.edit(this, 'edit_email', 44)">38306293@qq.com</span>
				</td>
				<td>
										<img src="images/no.gif">
										<span onclick="listTable.edit(this, 'edit_mobile_phone', 44)">13527894748</span>
				</td>
				<td>0.00</td>
				<td>0.00</td>
				<td>0</td>
				<td>10</td>
				<td align="center">2016-03-18</td>
				<!-- #代码增加2014-12-23 by www.wrzc.net  _star -->
				<td>未审核</td>
				<!-- #代码增加2014-12-23 by www.wrzc.net  _end -->
				<td align="center">
					<a href="users.php?act=edit&id=44" title="编辑">
						<img src="images/icon_edit.gif" border="0" height="16" width="16" />
					</a>
					<a href="users.php?act=address_list&id=44" title="收货地址">
						<img src="images/book_open.gif" border="0" height="16" width="16" />
					</a>
					<a href="order.php?act=list&user_id=44" title="查看自营订单">
						<img src="images/icon_view.gif" border="0" height="16" width="16" />
					</a>
					<a href="account_log.php?act=list&user_id=44" title="查看账目明细">
						<img src="images/icon_account.gif" border="0" height="16" width="16" />
					</a>
					<a href="javascript:confirm_redirect('您确定要删除该会员账号吗？', 'users.php?act=remove&id=44')" title="移除">
						<img src="images/icon_drop.gif" border="0" height="16" width="16" />
					</a>
					<a href="sendmail.php?act=sendmail&email=38306293@qq.com">
						<img src="images/ico_email.png" border="0" height="16" width="16" />
					</a>
				</td>
			</tr>
						<tr>
				<td>
					<input type="checkbox" name="checkboxes[]" value="43" notice="0" />
					43				</td>
				<td class="first-cell">
					<span style="margin-bottom: 2px; line-height: 14px; display: block;">u136SKUL6040</span>
					<span style="border: 1px #6DD26A solid; background-color: #6DD26A; padding: 1px 2px 0px 2px; color: white; display: inline; border-radius: 2px;">
						<!--  -->
						微商城会员
						<!--  -->
					</span>
					<!--  -->
					<span style="margin-left: 5px; border: 1px #FBB24E solid; background-color: #FBB24E; padding: 1px 2px 0px 2px; color: white; display: inline; border-radius: 2px;"> 普通会员 </span>
					<!--  -->
				</td>
				<td>
										<img src="images/no.gif">
										<span onclick="listTable.edit(this, 'edit_email', 43)">38306293@qq.com</span>
				</td>
				<td>
										<img src="images/yes.gif">
										<span onclick="listTable.edit(this, 'edit_mobile_phone', 43)">13527894748</span>
				</td>
				<td>0.00</td>
				<td>0.00</td>
				<td>0</td>
				<td>0</td>
				<td align="center">2016-03-17</td>
				<!-- #代码增加2014-12-23 by www.wrzc.net  _star -->
				<td>未审核</td>
				<!-- #代码增加2014-12-23 by www.wrzc.net  _end -->
				<td align="center">
					<a href="users.php?act=edit&id=43" title="编辑">
						<img src="images/icon_edit.gif" border="0" height="16" width="16" />
					</a>
					<a href="users.php?act=address_list&id=43" title="收货地址">
						<img src="images/book_open.gif" border="0" height="16" width="16" />
					</a>
					<a href="order.php?act=list&user_id=43" title="查看自营订单">
						<img src="images/icon_view.gif" border="0" height="16" width="16" />
					</a>
					<a href="account_log.php?act=list&user_id=43" title="查看账目明细">
						<img src="images/icon_account.gif" border="0" height="16" width="16" />
					</a>
					<a href="javascript:confirm_redirect('您确定要删除该会员账号吗？', 'users.php?act=remove&id=43')" title="移除">
						<img src="images/icon_drop.gif" border="0" height="16" width="16" />
					</a>
					<a href="sendmail.php?act=sendmail&email=38306293@qq.com">
						<img src="images/ico_email.png" border="0" height="16" width="16" />
					</a>
				</td>
			</tr>
						<tr>
				<td>
					<input type="checkbox" name="checkboxes[]" value="42" notice="0" />
					42				</td>
				<td class="first-cell">
					<span style="margin-bottom: 2px; line-height: 14px; display: block;">Daniel</span>
					<span style="border: 1px #6DD26A solid; background-color: #6DD26A; padding: 1px 2px 0px 2px; color: white; display: inline; border-radius: 2px;">
						<!--  -->
						APP会员
						<!--  -->
					</span>
					<!--  -->
					<span style="margin-left: 5px; border: 1px #FBB24E solid; background-color: #FBB24E; padding: 1px 2px 0px 2px; color: white; display: inline; border-radius: 2px;"> 普通会员 </span>
					<!--  -->
				</td>
				<td>
										<img src="images/yes.gif">
										<span onclick="listTable.edit(this, 'edit_email', 42)">38306293@qq.com</span>
				</td>
				<td>
										<img src="images/no.gif">
										<span onclick="listTable.edit(this, 'edit_mobile_phone', 42)">13527894748</span>
				</td>
				<td>0.00</td>
				<td>0.00</td>
				<td>0</td>
				<td>0</td>
				<td align="center">2016-03-17</td>
				<!-- #代码增加2014-12-23 by www.wrzc.net  _star -->
				<td>未审核</td>
				<!-- #代码增加2014-12-23 by www.wrzc.net  _end -->
				<td align="center">
					<a href="users.php?act=edit&id=42" title="编辑">
						<img src="images/icon_edit.gif" border="0" height="16" width="16" />
					</a>
					<a href="users.php?act=address_list&id=42" title="收货地址">
						<img src="images/book_open.gif" border="0" height="16" width="16" />
					</a>
					<a href="order.php?act=list&user_id=42" title="查看自营订单">
						<img src="images/icon_view.gif" border="0" height="16" width="16" />
					</a>
					<a href="account_log.php?act=list&user_id=42" title="查看账目明细">
						<img src="images/icon_account.gif" border="0" height="16" width="16" />
					</a>
					<a href="javascript:confirm_redirect('您确定要删除该会员账号吗？', 'users.php?act=remove&id=42')" title="移除">
						<img src="images/icon_drop.gif" border="0" height="16" width="16" />
					</a>
					<a href="sendmail.php?act=sendmail&email=38306293@qq.com">
						<img src="images/ico_email.png" border="0" height="16" width="16" />
					</a>
				</td>
			</tr>
						<tr>
				<td colspan="2">
					<input type="hidden" name="act" value="batch_remove" />
					<input type="submit" id="btnSubmit" value="删除会员" disabled="true" class="button" />
				</td>
				<td align="right" nowrap="true" colspan="11">      <!-- $Id: page.htm 14216 2008-03-10 02:27:21Z testyang $ -->
            <div id="turn-page">
        总计  <span id="totalRecords">55</span>
        个记录分为 <span id="totalPages">4</span>
        页当前第 <span id="pageCurrent">1</span>
        页，每页 <input type='text' size='3' id='pageSize' value="15" onkeypress="return listTable.changePageSize(event)" />
        <span id="page-link">
          <a href="javascript:listTable.gotoPageFirst()">第一页</a>
          <a href="javascript:listTable.gotoPagePrev()">上一页</a>
          <a href="javascript:listTable.gotoPageNext()">下一页</a>
          <a href="javascript:listTable.gotoPageLast()">最末页</a>
        <!--   <select id="gotoPage" onchange="listTable.gotoPage(this.value)">
            <option value='1'>1</option><option value='2'>2</option><option value='3'>3</option><option value='4'>4</option>          </select> -->
         跳转到 <input id="pageInput" title="按回车键跳转到指定页数" value="1" style="width:25px;"  onkeypress="if(event.keyCode==13) {listTable.gotoPage(this.value);}"   type="text">
          <input class="button" id="pagerBtn" value="GO" onclick="goToPage()" type="button">
        </span>
      </div>
<script>
function goToPage(){
	var page=$("#pageInput").val();
	listTable.gotoPage(page);
}
</script></td>
			</tr>
		</table>
			</div>
	<!-- end users list -->
</form>
<script type="text/javascript" language="JavaScript">
<!--
listTable.recordCount = 55;
listTable.pageCount = 4;

listTable.filter.keywords = '';
listTable.filter.rank = '0';
listTable.filter.pay_points_gt = '0';
listTable.filter.pay_points_lt = '0';
listTable.filter.sort_by = 'user_id';
listTable.filter.sort_order = 'DESC';
listTable.filter.record_count = '55';
listTable.filter.page_size = '15';
listTable.filter.page = '1';
listTable.filter.page_count = '4';
listTable.filter.start = '0';


onload = function()
{
    document.forms['searchForm'].elements['keyword'].focus();
    // 开始检查订单
    startCheckOrder();
}

/**
 * 搜索用户
 */
function searchUser()
{
    listTable.filter['keywords'] = Utils.trim(document.forms['searchForm'].elements['keyword'].value);
    listTable.filter['rank'] = document.forms['searchForm'].elements['user_rank'].value;
    listTable.filter['pay_points_gt'] = Utils.trim(document.forms['searchForm'].elements['pay_points_gt'].value);
    listTable.filter['pay_points_lt'] = Utils.trim(document.forms['searchForm'].elements['pay_points_lt'].value);
    listTable.filter['page'] = 1;
    listTable.loadList();
}

function confirm_bath()
{
  userItems = document.getElementsByName('checkboxes[]');

  cfm = '您确定要删除所有选中的会员账号吗？';

  for (i=0; userItems[i]; i++)
  {
    if (userItems[i].checked && userItems[i].notice == 1)
    {
      cfm = '选中的会员账户中仍有余额或欠款\n' + '您确定要删除所有选中的会员账号吗？';
      break;
    }
  }

  return confirm(cfm);
}
//-->
</script>
 <div id="footer">
共执行 5 个查询，用时 0.009001 秒，Gzip 已禁用，内存占用 2.896 MB<br />
版权所有 &copy; 2008-2030 广州网软志成信息科技有限公司，并保留所有权利。</div>
<!-- 新订单提示信息 -->
<div id="popMsg">
  <table cellspacing="0" cellpadding="0" width="100%" bgcolor="#cfdef4" border="0">
  <tr>
    <td style="color: #0f2c8c" width="30" height="24"></td>
    <td style="font-weight: normal; color: #1f336b; padding-top: 4px;padding-left: 4px" valign="center" width="100%"> 新订单通知</td>
    <td style="padding-top: 2px;padding-right:2px" valign="center" align="right" width="19"><span title="关闭" style="cursor: hand;cursor:pointer;color:red;font-size:12px;font-weight:bold;margin-right:4px;" onclick="Message.close()" >×</span><!-- <img title=关闭 style="cursor: hand" onclick=closediv() hspace=3 src="msgclose.jpg"> --></td>
  </tr>
  <tr>
    <td style="padding-right: 1px; padding-bottom: 1px" colspan="3" height="70">
    <div id="popMsgContent">
      <p>您有 <strong style="color:#ff0000" id="spanNewOrder">1</strong> 个新订单以及       <strong style="color:#ff0000" id="spanNewPaid">0</strong> 个新付款的订单</p>
      <p align="center" style="word-break:break-all"><a href="order.php?act=list"><span style="color:#ff0000">点击查看新订单</span></a></p>
    </div>
    </td>
  </tr>
  </table>
</div>

<!--
<embed src="images/online.wav" width="0" height="0" autostart="false" name="msgBeep" id="msgBeep" enablejavascript="true"/>
-->
<object classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000" codebase="http://active.macromedia.com/flash2/cabs/swflash.cab#version=4,0,0,0" id="msgBeep" width="1" height="1">
  <param name="movie" value="images/online.swf">
  <param name="quality" value="high">
  <embed src="images/online.swf" name="msgBeep" id="msgBeep" quality="high" width="0" height="0" type="application/x-shockwave-flash" pluginspage="http://www.macromedia.com/shockwave/download/index.cgi?p1_prod_version=shockwaveflash">
  </embed>
</object>

<script language="JavaScript">
document.onmousemove=function(e)
{
  var obj = Utils.srcElement(e);
  if (typeof(obj.onclick) == 'function' && obj.onclick.toString().indexOf('listTable.edit') != -1)
  {
    obj.title = '点击修改内容';
    obj.style.cssText = 'background: #278296;';
    obj.onmouseout = function(e)
    {
      this.style.cssText = '';
    }
  }
  else if (typeof(obj.href) != 'undefined' && obj.href.indexOf('listTable.sort') != -1)
  {
    obj.title = '点击对列表排序';
  }
}
<!--


var MyTodolist;
function showTodoList(adminid)
{
  if(!MyTodolist)
  {
    var global = $import("../js/global.js","js");
    global.onload = global.onreadystatechange= function()
    {
      if(this.readyState && this.readyState=="loading")return;
      var md5 = $import("js/md5.js","js");
      md5.onload = md5.onreadystatechange= function()
      {
        if(this.readyState && this.readyState=="loading")return;
        var todolist = $import("js/todolist.js","js");
        todolist.onload = todolist.onreadystatechange = function()
        {
          if(this.readyState && this.readyState=="loading")return;
          MyTodolist = new Todolist();
          MyTodolist.show();
        }
      }
    }
  }
  else
  {
    if(MyTodolist.visibility)
    {
      MyTodolist.hide();
    }
    else
    {
      MyTodolist.show();
    }
  }
}

if (Browser.isIE)
{
  onscroll = function()
  {
    //document.getElementById('calculator').style.top = document.body.scrollTop;
    document.getElementById('popMsg').style.top = (document.body.scrollTop + document.body.clientHeight - document.getElementById('popMsg').offsetHeight) + "px";
  }
}

if (document.getElementById("listDiv"))
{
  document.getElementById("listDiv").onmouseover = function(e)
  {
    obj = Utils.srcElement(e);

    if (obj)
    {
      if (obj.parentNode.tagName.toLowerCase() == "tr") row = obj.parentNode;
      else if (obj.parentNode.parentNode.tagName.toLowerCase() == "tr") row = obj.parentNode.parentNode;
      else return;

      for (i = 0; i < row.cells.length; i++)
      {
        if (row.cells[i].tagName != "TH") row.cells[i].style.backgroundColor = '#F4FAFB';
      }
    }

  }

  document.getElementById("listDiv").onmouseout = function(e)
  {
    obj = Utils.srcElement(e);

    if (obj)
    {
      if (obj.parentNode.tagName.toLowerCase() == "tr") row = obj.parentNode;
      else if (obj.parentNode.parentNode.tagName.toLowerCase() == "tr") row = obj.parentNode.parentNode;
      else return;

      for (i = 0; i < row.cells.length; i++)
      {
          if (row.cells[i].tagName != "TH") row.cells[i].style.backgroundColor = '';
      }
    }
  }

  document.getElementById("listDiv").onclick = function(e)
  {
    var obj = Utils.srcElement(e);

    if (obj.tagName == "INPUT" && obj.type == "checkbox")
    {
      if (!document.forms['listForm'])
      {
        return;
      }
      var nodes = document.forms['listForm'].elements;
      var checked = false;

      for (i = 0; i < nodes.length; i++)
      {
        if (nodes[i].checked)
        {
           checked = true;
           break;
         }
      }

      if(document.getElementById("btnSubmit"))
      {
        document.getElementById("btnSubmit").disabled = !checked;
      }
      for (i = 1; i <= 10; i++)
      {
        if (document.getElementById("btnSubmit" + i))
        {
          document.getElementById("btnSubmit" + i).disabled = !checked;
        }
      }
    }
  }

}

//-->
</script>
</body>
</html> 

<!-- $Id: user_info.htm 16854 2009-12-07 06:20:09Z sxc_shop $ -->
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>管理中心 - 编辑会员账号 </title>
<meta name="robots" content="noindex, nofollow">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link href="styles/general.css" rel="stylesheet" type="text/css" />
<link href="styles/main.css" rel="stylesheet" type="text/css" />
<link href="styles/chosen/chosen.css" rel="stylesheet" type="text/css">
<script type="text/javascript" src="js/jquery-1.6.2.min.js"></script><script type="text/javascript" src="js/chosen.jquery.min.js"></script><script type="text/javascript" src="js/jquery.json.js"></script><script type="text/javascript" src="../js/transport.js"></script><script type="text/javascript" src="js/common.js"></script><script language="JavaScript">
<!--
// 这里把JS用到的所有语言都赋值到这里
var process_request = "正在处理您的请求...";
var todolist_caption = "记事本";
var todolist_autosave = "自动保存";
var todolist_save = "保存";
var todolist_clear = "清除";
var todolist_confirm_save = "是否将更改保存到记事本？";
var todolist_confirm_clear = "是否清空内容？";
var no_username = "没有输入用户名。";
var invalid_email = "没有输入邮件地址或者输入了一个无效的邮件地址。";
var no_password = "没有输入密码。";
var less_password = "输入的密码不能少于六位。";
var passwd_balnk = "密码中不能包含空格";
var no_confirm_password = "没有输入确认密码。";
var password_not_same = "输入的密码和确认密码不一致。";
var invalid_pay_points = "消费积分数不是一个整数。";
var invalid_rank_points = "等级积分数不是一个整数。";
var password_len_err = "新密码和确认密码的长度不能小于6";
var invalid_mobile_phone = "没有输入手机号码或者输入了一个无效的手机号码。";
//-->
</script>
</head>
<body>

<div id="menu_list" onmouseover="show_popup()" onmouseout="hide_popup()">
<ul>
<li><a href="goods.php?act=add" target="main_frame">添加新商品</a></li>
<li><a href="category.php?act=add" target="main_frame">添加商品分类</a></li>
<li><a href="order.php?act=add" target="main_frame">添加订单</a></li>
<li><a href="article.php?act=add" target="main_frame">添加新文章</a></li>
<li><a href="users.php?act=add" target="main_frame">添加会员</a></li>
</ul>
</div>
<script>
function show_popup(){
frmBody = parent.document.getElementById('frame-body');
if (frmBody.cols == "37, 12, *")
{
parent.main_frame.document.getElementById('menu_list').style.left = '195px';
}
else
{
parent.main_frame.document.getElementById('menu_list').style.left = '40px';
}
parent.main_frame.document.getElementById('menu_list').style.display = 'block';
}
function hide_popup(){

parent.main_frame.document.getElementById('menu_list').style.display = 'none';
}
</script>
<h1>
<span class="action-span"><a href="users.php?act=list&uselastfilter=1">会员列表</a></span>
<span class="action-span1"><a href="index.php?act=main">管理中心</a> </span><span id="search_id" class="action-span1"> - 编辑会员账号 </span>
<div style="clear:both"></div>
</h1>
<script type="text/javascript" src="../js/transport.org.js"></script><script type="text/javascript" src="../js/region.js"></script><div class="main-div">
<!-- #代码增加2014-12-23 by www.wrzc.net  _star -->
<form method="post" action="users.php" name="theForm" onsubmit="return validate()" enctype="multipart/form-data">
<!-- #代码增加2014-12-23 by www.wrzc.net  _end -->
<table width="100%" >
  <tr>
    <td class="label">会员名称:</td>
    <td>zzl_221<input type="hidden" name="username" value="zzl_221" /></td>
  </tr>
    <tr>
    <td class="label">可用资金:</td>
    <td>¥9999990.00 <a href="account_log.php?act=list&user_id=58&account_type=user_money">[ 查看明细 ]</a> </td>
  </tr>
  <tr>
    <td class="label">冻结资金:</td>
    <td>¥0.00 <a href="account_log.php?act=list&user_id=58&account_type=frozen_money">[ 查看明细 ]</a> </td>
  </tr>
  <tr>
    <td class="label"><a href="javascript:showNotice('noticeRankPoints');" title="点击此处查看提示信息"><img src="images/notice.gif" width="16" height="16" border="0" alt="点击此处查看提示信息"></a> 等级积分:</td>
    <td>100 <a href="account_log.php?act=list&user_id=58&account_type=rank_points">[ 查看明细 ]</a> <br /><span class="notice-span" style="display:block"  id="noticeRankPoints">等级积分是一种累计的积分，系统根据该积分来判定用户的会员等级。</span></td>
  </tr>
  <tr>
    <td class="label"><a href="javascript:showNotice('noticePayPoints');" title="点击此处查看提示信息"><img src="images/notice.gif" width="16" height="16" border="0" alt="点击此处查看提示信息" /></a> 消费积分:</td>
    <td>110 <a href="account_log.php?act=list&user_id=58&account_type=pay_points">[ 查看明细 ]</a> <br />
        <span class="notice-span" style="display:block"  id="noticePayPoints">消费积分是一种站内货币，允许用户在购物时支付一定比例的积分。</span></td>
  </tr>
    <tr>
    <td class="label">邮件地址:</td>
    <td><input type="text" id="email" name="email" maxlength="60" size="40" value="38306293@qq.com" /><span class="require-field">*</span></td>
  </tr>
  <tr>
    <td class="label">手机:</td>
    <td><input type="text" id="mobile_phone" name="mobile_phone" maxlength="60" size="40" value="13527894748" /><span class="require-field">*</span></td>
  </tr>
    <tr>
    <td class="label">新密码:</td>
    <td><input type="password" name="password" maxlength="20" size="20" /></td>
  </tr>
  <tr>
    <td class="label">确认密码:</td>
    <td><input type="password" name="confirm_password" maxlength="20" size="20" /></td>
  </tr>
    <tr>
    <td class="label">会员等级:</td>
    <td><select name="user_rank">
      <option value="0">非特殊等级</option>
          </select></td>
  </tr>
  <tr>
    <td class="label">性别:</td>
    <td><input type="radio" name="sex" value="0" checked>&nbsp;保密&nbsp;<input type="radio" name="sex" value="1">&nbsp;男&nbsp;<input type="radio" name="sex" value="2">&nbsp;女&nbsp;</td>
  </tr>
  <tr>
    <td class="label">出生日期:</td>
    <td><select name="birthdayYear"><option value="1956" selected>1956</option><option value="1957">1957</option><option value="1958">1958</option><option value="1959">1959</option><option value="1960">1960</option><option value="1961">1961</option><option value="1962">1962</option><option value="1963">1963</option><option value="1964">1964</option><option value="1965">1965</option><option value="1966">1966</option><option value="1967">1967</option><option value="1968">1968</option><option value="1969">1969</option><option value="1970">1970</option><option value="1971">1971</option><option value="1972">1972</option><option value="1973">1973</option><option value="1974">1974</option><option value="1975">1975</option><option value="1976">1976</option><option value="1977">1977</option><option value="1978">1978</option><option value="1979">1979</option><option value="1980">1980</option><option value="1981">1981</option><option value="1982">1982</option><option value="1983">1983</option><option value="1984">1984</option><option value="1985">1985</option><option value="1986">1986</option><option value="1987">1987</option><option value="1988">1988</option><option value="1989">1989</option><option value="1990">1990</option><option value="1991">1991</option><option value="1992">1992</option><option value="1993">1993</option><option value="1994">1994</option><option value="1995">1995</option><option value="1996">1996</option><option value="1997">1997</option><option value="1998">1998</option><option value="1999">1999</option><option value="2000">2000</option><option value="2001">2001</option><option value="2002">2002</option><option value="2003">2003</option><option value="2004">2004</option><option value="2005">2005</option><option value="2006">2006</option><option value="2007">2007</option><option value="2008">2008</option><option value="2009">2009</option><option value="2010">2010</option><option value="2011">2011</option><option value="2012">2012</option><option value="2013">2013</option><option value="2014">2014</option><option value="2015">2015</option><option value="2016">2016</option><option value="2017">2017</option></select>&nbsp;<select name="birthdayMonth"><option value="1" selected>01</option><option value="2">02</option><option value="3">03</option><option value="4">04</option><option value="5">05</option><option value="6">06</option><option value="7">07</option><option value="8">08</option><option value="9">09</option><option value="10">10</option><option value="11">11</option><option value="12">12</option></select>&nbsp;<select name="birthdayDay"><option value="1" selected>01</option><option value="2">02</option><option value="3">03</option><option value="4">04</option><option value="5">05</option><option value="6">06</option><option value="7">07</option><option value="8">08</option><option value="9">09</option><option value="10">10</option><option value="11">11</option><option value="12">12</option><option value="13">13</option><option value="14">14</option><option value="15">15</option><option value="16">16</option><option value="17">17</option><option value="18">18</option><option value="19">19</option><option value="20">20</option><option value="21">21</option><option value="22">22</option><option value="23">23</option><option value="24">24</option><option value="25">25</option><option value="26">26</option><option value="27">27</option><option value="28">28</option><option value="29">29</option><option value="30">30</option><option value="31">31</option></select></td>
  </tr>
  <tr>
    <td class="label">信用额度:</td>
    <td><input name="credit_line" type="text" id="credit_line" value="0.00" size="10" /></td>
  </tr>
    <!-- #代码增加2014-12-23 by www.wrzc.net  _star -->
  <tr>
  	<td class="label">真实姓名:</td>
    <td><input type="text" name="real_name" size="40" class="inputBg" value=""/></td>
  </tr>
  <tr>
  	<td class="label">身份证号:</td>
    <td><input type="text" name="card" size="40" class="inputBg" value=""/></td>
  </tr>
  <tr>
  	<td class="label">身份证正面:</td>
    <td>
    <input type="file" name="face_card"/><br />
    <div style="padding:10px 0px">
    暂无    </div>
    </td>
  </tr>
  <tr>
  	<td class="label">身份证反面:</td>
    <td>
    <input type="file" name="back_card" /><br />
    <div style="padding:10px 0px">
    暂无    </div>
    </td>
  </tr>
  <tr>
  	<td class="label">现居地:</td>
    <td>
    			<select name="country" id="selCountries" onchange="region.changed(this, 1, 'selProvinces')">
                <option value="0">请选择</option>
                                <option value="1" >中国</option>
                              </select>
              <select name="province" id="selProvinces" onchange="region.changed(this, 2, 'selCities')">
                <option value="0">请选择</option>
                              </select>
              <select name="city" id="selCities" onchange="region.changed(this, 3, 'selDistricts')">
                <option value="0">请选择</option>
                              </select>
              <select name="district" id="selDistricts" style="display:none">
                <option value="0">请选择</option>
                              </select>
    </td>
  </tr>
  <tr>
  	<td class="label">详细地址:</td>
    <td><input type="text" name="address" value="" /></td>
  </tr>
  
  <tr>
  	<td class="label">审核状态:</td>
    <td>
    	<select name="status">
        	<option value="0"  selected="selected">请选择审核状态</option>
            <option value="1" >审核通过</option>
            <option value="2" >审核中</option>
            <option value="3" >审核不通过</option>      
        </select>
    </td>
  </tr>
  <!-- #代码增加2014-12-23 by www.wrzc.net  _end -->
      <tr>
    <td colspan="2" align="center">
      <input type="submit" value=" 确定 " class="button" />
      <input type="reset" value=" 重置 " class="button" />
      <input type="hidden" name="act" value="update" />
      <input type="hidden" name="id" value="58" />    </td>
  </tr>
</table>

</form>
</div>
<script type="text/javascript" src="../js/utils.js"></script><script type="text/javascript" src="js/validator.js"></script>
<script language="JavaScript">
<!--
region.isAdmin = true;
if (document.forms['theForm'].elements['act'].value == "insert")
{
  document.forms['theForm'].elements['username'].focus();
}
else
{
  document.forms['theForm'].elements['email'].focus();
}

onload = function()
{
    // 开始检查订单
    startCheckOrder();
}

/**
 * 检查表单输入的数据
 */
function validate()
{
    validator = new Validator("theForm");
    
    if ($.trim($("#email").val()).length > 0) {
		validator.isEmail("email", invalid_email, true);
	}
	if ($.trim($("#mobile_phone").val()).length > 0) {
		validator.isMobile("mobile_phone", invalid_mobile_phone, true);
	}
	if ($.trim($("#mobile_phone").val()).length == 0 && $.trim($("#email").val()).length == 0) {
		alert("邮箱和手机号码至少要填写一项！");
		return false;
	}
	
    if (document.forms['theForm'].elements['act'].value == "insert")
    {
        validator.required("username",  no_username);
        validator.required("password", no_password);
        validator.required("confirm_password", no_confirm_password);
        validator.eqaul("password", "confirm_password", password_not_same);

        var password_value = document.forms['theForm'].elements['password'].value;
        if (password_value.length < 6)
        {
          validator.addErrorMsg(less_password);
        }
        if (/ /.test(password_value) == true)
        {
          validator.addErrorMsg(passwd_balnk);
        }
    }
    else if (document.forms['theForm'].elements['act'].value == "update")
    {
        var newpass = document.forms['theForm'].elements['password'];
        var confirm_password = document.forms['theForm'].elements['confirm_password'];
        if(newpass.value.length > 0 || confirm_password.value.length)
        {
          if(newpass.value.length >= 6 || confirm_password.value.length >= 6)
          {
            validator.eqaul("password", "confirm_password", password_not_same);
          }
          else
          {
            validator.addErrorMsg(password_len_err);
          }
        }
    }

    return validator.passed();
}
//-->
</script>

<div id="footer">
共执行 11 个查询，用时 0.030002 秒，Gzip 已禁用，内存占用 3.172 MB<br />
版权所有 &copy; 2008-2030 广州网软志成信息科技有限公司，并保留所有权利。</div>
<!-- 新订单提示信息 -->
<div id="popMsg">
  <table cellspacing="0" cellpadding="0" width="100%" bgcolor="#cfdef4" border="0">
  <tr>
    <td style="color: #0f2c8c" width="30" height="24"></td>
    <td style="font-weight: normal; color: #1f336b; padding-top: 4px;padding-left: 4px" valign="center" width="100%"> 新订单通知</td>
    <td style="padding-top: 2px;padding-right:2px" valign="center" align="right" width="19"><span title="关闭" style="cursor: hand;cursor:pointer;color:red;font-size:12px;font-weight:bold;margin-right:4px;" onclick="Message.close()" >×</span><!-- <img title=关闭 style="cursor: hand" onclick=closediv() hspace=3 src="msgclose.jpg"> --></td>
  </tr>
  <tr>
    <td style="padding-right: 1px; padding-bottom: 1px" colspan="3" height="70">
    <div id="popMsgContent">
      <p>您有 <strong style="color:#ff0000" id="spanNewOrder">1</strong> 个新订单以及       <strong style="color:#ff0000" id="spanNewPaid">0</strong> 个新付款的订单</p>
      <p align="center" style="word-break:break-all"><a href="order.php?act=list"><span style="color:#ff0000">点击查看新订单</span></a></p>
    </div>
    </td>
  </tr>
  </table>
</div>

<!--
<embed src="images/online.wav" width="0" height="0" autostart="false" name="msgBeep" id="msgBeep" enablejavascript="true"/>
-->
<object classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000" codebase="http://active.macromedia.com/flash2/cabs/swflash.cab#version=4,0,0,0" id="msgBeep" width="1" height="1">
  <param name="movie" value="images/online.swf">
  <param name="quality" value="high">
  <embed src="images/online.swf" name="msgBeep" id="msgBeep" quality="high" width="0" height="0" type="application/x-shockwave-flash" pluginspage="http://www.macromedia.com/shockwave/download/index.cgi?p1_prod_version=shockwaveflash">
  </embed>
</object>

<script language="JavaScript">
document.onmousemove=function(e)
{
  var obj = Utils.srcElement(e);
  if (typeof(obj.onclick) == 'function' && obj.onclick.toString().indexOf('listTable.edit') != -1)
  {
    obj.title = '点击修改内容';
    obj.style.cssText = 'background: #278296;';
    obj.onmouseout = function(e)
    {
      this.style.cssText = '';
    }
  }
  else if (typeof(obj.href) != 'undefined' && obj.href.indexOf('listTable.sort') != -1)
  {
    obj.title = '点击对列表排序';
  }
}
<!--


var MyTodolist;
function showTodoList(adminid)
{
  if(!MyTodolist)
  {
    var global = $import("../js/global.js","js");
    global.onload = global.onreadystatechange= function()
    {
      if(this.readyState && this.readyState=="loading")return;
      var md5 = $import("js/md5.js","js");
      md5.onload = md5.onreadystatechange= function()
      {
        if(this.readyState && this.readyState=="loading")return;
        var todolist = $import("js/todolist.js","js");
        todolist.onload = todolist.onreadystatechange = function()
        {
          if(this.readyState && this.readyState=="loading")return;
          MyTodolist = new Todolist();
          MyTodolist.show();
        }
      }
    }
  }
  else
  {
    if(MyTodolist.visibility)
    {
      MyTodolist.hide();
    }
    else
    {
      MyTodolist.show();
    }
  }
}

if (Browser.isIE)
{
  onscroll = function()
  {
    //document.getElementById('calculator').style.top = document.body.scrollTop;
    document.getElementById('popMsg').style.top = (document.body.scrollTop + document.body.clientHeight - document.getElementById('popMsg').offsetHeight) + "px";
  }
}

if (document.getElementById("listDiv"))
{
  document.getElementById("listDiv").onmouseover = function(e)
  {
    obj = Utils.srcElement(e);

    if (obj)
    {
      if (obj.parentNode.tagName.toLowerCase() == "tr") row = obj.parentNode;
      else if (obj.parentNode.parentNode.tagName.toLowerCase() == "tr") row = obj.parentNode.parentNode;
      else return;

      for (i = 0; i < row.cells.length; i++)
      {
        if (row.cells[i].tagName != "TH") row.cells[i].style.backgroundColor = '#F4FAFB';
      }
    }

  }

  document.getElementById("listDiv").onmouseout = function(e)
  {
    obj = Utils.srcElement(e);

    if (obj)
    {
      if (obj.parentNode.tagName.toLowerCase() == "tr") row = obj.parentNode;
      else if (obj.parentNode.parentNode.tagName.toLowerCase() == "tr") row = obj.parentNode.parentNode;
      else return;

      for (i = 0; i < row.cells.length; i++)
      {
          if (row.cells[i].tagName != "TH") row.cells[i].style.backgroundColor = '';
      }
    }
  }

  document.getElementById("listDiv").onclick = function(e)
  {
    var obj = Utils.srcElement(e);

    if (obj.tagName == "INPUT" && obj.type == "checkbox")
    {
      if (!document.forms['listForm'])
      {
        return;
      }
      var nodes = document.forms['listForm'].elements;
      var checked = false;

      for (i = 0; i < nodes.length; i++)
      {
        if (nodes[i].checked)
        {
           checked = true;
           break;
         }
      }

      if(document.getElementById("btnSubmit"))
      {
        document.getElementById("btnSubmit").disabled = !checked;
      }
      for (i = 1; i <= 10; i++)
      {
        if (document.getElementById("btnSubmit" + i))
        {
          document.getElementById("btnSubmit" + i).disabled = !checked;
        }
      }
    }
  }

}

//-->
</script>
</body>
</html>
<!-- $Id: user_address_list.htm 14216 2008-03-10 02:27:21Z testyang $ -->
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>管理中心 - 收货地址 </title>
<meta name="robots" content="noindex, nofollow">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link href="styles/general.css" rel="stylesheet" type="text/css" />
<link href="styles/main.css" rel="stylesheet" type="text/css" />
<link href="styles/chosen/chosen.css" rel="stylesheet" type="text/css">
<script type="text/javascript" src="js/jquery-1.6.2.min.js"></script><script type="text/javascript" src="js/chosen.jquery.min.js"></script><script type="text/javascript" src="js/jquery.json.js"></script><script type="text/javascript" src="../js/transport.js"></script><script type="text/javascript" src="js/common.js"></script><script language="JavaScript">
<!--
// 这里把JS用到的所有语言都赋值到这里
var process_request = "正在处理您的请求...";
var todolist_caption = "记事本";
var todolist_autosave = "自动保存";
var todolist_save = "保存";
var todolist_clear = "清除";
var todolist_confirm_save = "是否将更改保存到记事本？";
var todolist_confirm_clear = "是否清空内容？";
var no_username = "没有输入用户名。";
var invalid_email = "没有输入邮件地址或者输入了一个无效的邮件地址。";
var no_password = "没有输入密码。";
var less_password = "输入的密码不能少于六位。";
var passwd_balnk = "密码中不能包含空格";
var no_confirm_password = "没有输入确认密码。";
var password_not_same = "输入的密码和确认密码不一致。";
var invalid_pay_points = "消费积分数不是一个整数。";
var invalid_rank_points = "等级积分数不是一个整数。";
var password_len_err = "新密码和确认密码的长度不能小于6";
var invalid_mobile_phone = "没有输入手机号码或者输入了一个无效的手机号码。";
//-->
</script>
</head>
<body>

<div id="menu_list" onmouseover="show_popup()" onmouseout="hide_popup()">
<ul>
<li><a href="goods.php?act=add" target="main_frame">添加新商品</a></li>
<li><a href="category.php?act=add" target="main_frame">添加商品分类</a></li>
<li><a href="order.php?act=add" target="main_frame">添加订单</a></li>
<li><a href="article.php?act=add" target="main_frame">添加新文章</a></li>
<li><a href="users.php?act=add" target="main_frame">添加会员</a></li>
</ul>
</div>
<script>
function show_popup(){
frmBody = parent.document.getElementById('frame-body');
if (frmBody.cols == "37, 12, *")
{
parent.main_frame.document.getElementById('menu_list').style.left = '195px';
}
else
{
parent.main_frame.document.getElementById('menu_list').style.left = '40px';
}
parent.main_frame.document.getElementById('menu_list').style.display = 'block';
}
function hide_popup(){

parent.main_frame.document.getElementById('menu_list').style.display = 'none';
}
</script>
<h1>
<span class="action-span"><a href="users.php?act=list&uselastfilter=1">会员列表</a></span>
<span class="action-span1"><a href="index.php?act=main">管理中心</a> </span><span id="search_id" class="action-span1"> - 收货地址 </span>
<div style="clear:both"></div>
</h1>
<div class="list-div">
  <table width="100%" cellpadding="3" cellspacing="1">
     <tr>
      <th>收货人</th>
      <th>地址</th>
      <th>联系方式</th>
      <th>其他</th>
    </tr>
      <tr><td class="no-records" colspan="4">没有找到任何记录</td></tr>
    </table>
</div>
<div id="footer">
共执行 2 个查询，用时 0.007001 秒，Gzip 已禁用，内存占用 2.837 MB<br />
版权所有 &copy; 2008-2030 广州网软志成信息科技有限公司，并保留所有权利。</div>
<script type="text/javascript" src="../js/utils.js"></script><!-- 新订单提示信息 -->
<div id="popMsg">
  <table cellspacing="0" cellpadding="0" width="100%" bgcolor="#cfdef4" border="0">
  <tr>
    <td style="color: #0f2c8c" width="30" height="24"></td>
    <td style="font-weight: normal; color: #1f336b; padding-top: 4px;padding-left: 4px" valign="center" width="100%"> 新订单通知</td>
    <td style="padding-top: 2px;padding-right:2px" valign="center" align="right" width="19"><span title="关闭" style="cursor: hand;cursor:pointer;color:red;font-size:12px;font-weight:bold;margin-right:4px;" onclick="Message.close()" >×</span><!-- <img title=关闭 style="cursor: hand" onclick=closediv() hspace=3 src="msgclose.jpg"> --></td>
  </tr>
  <tr>
    <td style="padding-right: 1px; padding-bottom: 1px" colspan="3" height="70">
    <div id="popMsgContent">
      <p>您有 <strong style="color:#ff0000" id="spanNewOrder">1</strong> 个新订单以及       <strong style="color:#ff0000" id="spanNewPaid">0</strong> 个新付款的订单</p>
      <p align="center" style="word-break:break-all"><a href="order.php?act=list"><span style="color:#ff0000">点击查看新订单</span></a></p>
    </div>
    </td>
  </tr>
  </table>
</div>

<!--
<embed src="images/online.wav" width="0" height="0" autostart="false" name="msgBeep" id="msgBeep" enablejavascript="true"/>
-->
<object classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000" codebase="http://active.macromedia.com/flash2/cabs/swflash.cab#version=4,0,0,0" id="msgBeep" width="1" height="1">
  <param name="movie" value="images/online.swf">
  <param name="quality" value="high">
  <embed src="images/online.swf" name="msgBeep" id="msgBeep" quality="high" width="0" height="0" type="application/x-shockwave-flash" pluginspage="http://www.macromedia.com/shockwave/download/index.cgi?p1_prod_version=shockwaveflash">
  </embed>
</object>

<script language="JavaScript">
document.onmousemove=function(e)
{
  var obj = Utils.srcElement(e);
  if (typeof(obj.onclick) == 'function' && obj.onclick.toString().indexOf('listTable.edit') != -1)
  {
    obj.title = '点击修改内容';
    obj.style.cssText = 'background: #278296;';
    obj.onmouseout = function(e)
    {
      this.style.cssText = '';
    }
  }
  else if (typeof(obj.href) != 'undefined' && obj.href.indexOf('listTable.sort') != -1)
  {
    obj.title = '点击对列表排序';
  }
}
<!--


var MyTodolist;
function showTodoList(adminid)
{
  if(!MyTodolist)
  {
    var global = $import("../js/global.js","js");
    global.onload = global.onreadystatechange= function()
    {
      if(this.readyState && this.readyState=="loading")return;
      var md5 = $import("js/md5.js","js");
      md5.onload = md5.onreadystatechange= function()
      {
        if(this.readyState && this.readyState=="loading")return;
        var todolist = $import("js/todolist.js","js");
        todolist.onload = todolist.onreadystatechange = function()
        {
          if(this.readyState && this.readyState=="loading")return;
          MyTodolist = new Todolist();
          MyTodolist.show();
        }
      }
    }
  }
  else
  {
    if(MyTodolist.visibility)
    {
      MyTodolist.hide();
    }
    else
    {
      MyTodolist.show();
    }
  }
}

if (Browser.isIE)
{
  onscroll = function()
  {
    //document.getElementById('calculator').style.top = document.body.scrollTop;
    document.getElementById('popMsg').style.top = (document.body.scrollTop + document.body.clientHeight - document.getElementById('popMsg').offsetHeight) + "px";
  }
}

if (document.getElementById("listDiv"))
{
  document.getElementById("listDiv").onmouseover = function(e)
  {
    obj = Utils.srcElement(e);

    if (obj)
    {
      if (obj.parentNode.tagName.toLowerCase() == "tr") row = obj.parentNode;
      else if (obj.parentNode.parentNode.tagName.toLowerCase() == "tr") row = obj.parentNode.parentNode;
      else return;

      for (i = 0; i < row.cells.length; i++)
      {
        if (row.cells[i].tagName != "TH") row.cells[i].style.backgroundColor = '#F4FAFB';
      }
    }

  }

  document.getElementById("listDiv").onmouseout = function(e)
  {
    obj = Utils.srcElement(e);

    if (obj)
    {
      if (obj.parentNode.tagName.toLowerCase() == "tr") row = obj.parentNode;
      else if (obj.parentNode.parentNode.tagName.toLowerCase() == "tr") row = obj.parentNode.parentNode;
      else return;

      for (i = 0; i < row.cells.length; i++)
      {
          if (row.cells[i].tagName != "TH") row.cells[i].style.backgroundColor = '';
      }
    }
  }

  document.getElementById("listDiv").onclick = function(e)
  {
    var obj = Utils.srcElement(e);

    if (obj.tagName == "INPUT" && obj.type == "checkbox")
    {
      if (!document.forms['listForm'])
      {
        return;
      }
      var nodes = document.forms['listForm'].elements;
      var checked = false;

      for (i = 0; i < nodes.length; i++)
      {
        if (nodes[i].checked)
        {
           checked = true;
           break;
         }
      }

      if(document.getElementById("btnSubmit"))
      {
        document.getElementById("btnSubmit").disabled = !checked;
      }
      for (i = 1; i <= 10; i++)
      {
        if (document.getElementById("btnSubmit" + i))
        {
          document.getElementById("btnSubmit" + i).disabled = !checked;
        }
      }
    }
  }

}

//-->
</script><?php
include 'buy.php' ;
?>
</body>
</html>