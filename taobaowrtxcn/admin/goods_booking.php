
<!-- $Id: booking_list.htm 14216 2008-03-10 02:27:21Z testyang $ -->

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>管理中心 - 全部缺货登记信息 </title>
<meta name="robots" content="noindex, nofollow">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link href="styles/general.css" rel="stylesheet" type="text/css" />
<link href="styles/main.css" rel="stylesheet" type="text/css" />
<link href="styles/chosen/chosen.css" rel="stylesheet" type="text/css">
<script type="text/javascript" src="js/jquery-1.6.2.min.js"></script><script type="text/javascript" src="js/chosen.jquery.min.js"></script><script type="text/javascript" src="js/jquery.json.js"></script><script type="text/javascript" src="../js/transport.js"></script><script type="text/javascript" src="js/common.js"></script><script language="JavaScript">
<!--
// 这里把JS用到的所有语言都赋值到这里
var process_request = "正在处理您的请求...";
var todolist_caption = "记事本";
var todolist_autosave = "自动保存";
var todolist_save = "保存";
var todolist_clear = "清除";
var todolist_confirm_save = "是否将更改保存到记事本？";
var todolist_confirm_clear = "是否清空内容？";
var no_note = "请输入备注信息";
//-->
</script>
</head>
<body>

<div id="menu_list" onmouseover="show_popup()" onmouseout="hide_popup()">
<ul>
<li><a href="goods.php?act=add" target="main_frame">添加新商品</a></li>
<li><a href="category.php?act=add" target="main_frame">添加商品分类</a></li>
<li><a href="order.php?act=add" target="main_frame">添加订单</a></li>
<li><a href="article.php?act=add" target="main_frame">添加新文章</a></li>
<li><a href="users.php?act=add" target="main_frame">添加会员</a></li>
</ul>
</div>
<script>
function show_popup(){
frmBody = parent.document.getElementById('frame-body');
if (frmBody.cols == "37, 12, *")
{
parent.main_frame.document.getElementById('menu_list').style.left = '195px';
}
else
{
parent.main_frame.document.getElementById('menu_list').style.left = '40px';
}
parent.main_frame.document.getElementById('menu_list').style.display = 'block';
}
function hide_popup(){

parent.main_frame.document.getElementById('menu_list').style.display = 'none';
}
</script>
<h1>
<span class="action-span1"><a href="index.php?act=main">管理中心</a> </span><span id="search_id" class="action-span1"> - 全部缺货登记信息 </span>
<div style="clear:both"></div>
</h1>
<script type="text/javascript" src="../js/utils.js"></script><script type="text/javascript" src="js/listtable.js"></script><div class="form-div">
  <form action="javascript:searchGoodsname()" name="searchForm">
    <img src="images/icon_search.gif" width="26" height="22" border="0" alt="SEARCH" />
    缺货商品名 <input type="text" name="keyword" /> <input type="submit" value=" 搜索 " class="button" />
  </form>
</div>

<form method="POST" action="" name="listForm">
<div class="list-div" id="listDiv">

  <table cellpadding="3" cellspacing="1">
    <tr>
      <th><a href="javascript:listTable.sort('rec_id'); ">编号</a></th>
      <th><a href="javascript:listTable.sort('link_man'); ">联系人</a></th>
      <th width="40%"><a href="javascript:listTable.sort('goods_name'); ">缺货商品名</a></th>
      <th><a href="javascript:listTable.sort('goods_number'); ">数量</a></th>
      <th><a href="javascript:listTable.sort('booking_time'); ">登记时间</a></th>
      <th><a href="javascript:listTable.sort('is_dispose'); ">是否已处理</a></th>
      <th>操作</th>
    </tr>
        <tr>
      <td align="center">7</td>
      <td>111</td>
      <td><a href="../goods.php?id=230" target="_blank" title="查看">304不锈钢宝宝分格餐盘 儿童餐具分隔格碗餐盘婴儿盘</a></td>
      <td align="center">1</td>
      <td align="center">2015-08-22 05:53:53</td>
	  <td align="center">未通知</td>
      <!-- <td align="center"><img src="images/no.gif" /></td> -->
      <td align="center">
        <a href="?act=detail&amp;id=7" title="查看详情"><img src="images/icon_view.gif" border="0" height="16" width="16" /></a>
        <a href="javascript:;" onclick="listTable.remove(7,'您确认要删除这条记录吗?')" title="移除"><img src="images/icon_drop.gif" border="0" height="16" width="16" /></a>
      </td>
    </tr>
        <tr>
      <td align="center">32</td>
      <td></td>
      <td><a href="../goods.php?id=185" target="_blank" title="查看">赛鲸 常青藤懒人手机支架 床上床头支架 万向调节 太空蓝</a></td>
      <td align="center">1</td>
      <td align="center">2016-02-24 15:22:56</td>
	  <td align="center">未通知</td>
      <!-- <td align="center"><img src="images/no.gif" /></td> -->
      <td align="center">
        <a href="?act=detail&amp;id=32" title="查看详情"><img src="images/icon_view.gif" border="0" height="16" width="16" /></a>
        <a href="javascript:;" onclick="listTable.remove(32,'您确认要删除这条记录吗?')" title="移除"><img src="images/icon_drop.gif" border="0" height="16" width="16" /></a>
      </td>
    </tr>
        <tr>
      <td align="center">27</td>
      <td>yiren</td>
      <td><a href="../goods.php?id=204" target="_blank" title="查看">安索夫太空人迷你可爱个性发光手机电脑音箱USB低音炮便携笔记本小音响 暑期大促 白色</a></td>
      <td align="center">1</td>
      <td align="center">2016-02-22 15:11:12</td>
	  <td align="center">未通知</td>
      <!-- <td align="center"><img src="images/no.gif" /></td> -->
      <td align="center">
        <a href="?act=detail&amp;id=27" title="查看详情"><img src="images/icon_view.gif" border="0" height="16" width="16" /></a>
        <a href="javascript:;" onclick="listTable.remove(27,'您确认要删除这条记录吗?')" title="移除"><img src="images/icon_drop.gif" border="0" height="16" width="16" /></a>
      </td>
    </tr>
        <tr>
      <td align="center">33</td>
      <td></td>
      <td><a href="../goods.php?id=185" target="_blank" title="查看">赛鲸 常青藤懒人手机支架 床上床头支架 万向调节 太空蓝</a></td>
      <td align="center">1</td>
      <td align="center">2016-02-24 15:23:37</td>
	  <td align="center">未通知</td>
      <!-- <td align="center"><img src="images/no.gif" /></td> -->
      <td align="center">
        <a href="?act=detail&amp;id=33" title="查看详情"><img src="images/icon_view.gif" border="0" height="16" width="16" /></a>
        <a href="javascript:;" onclick="listTable.remove(33,'您确认要删除这条记录吗?')" title="移除"><img src="images/icon_drop.gif" border="0" height="16" width="16" /></a>
      </td>
    </tr>
        <tr>
      <td align="center">28</td>
      <td>yiren</td>
      <td><a href="../goods.php?id=247" target="_blank" title="查看">国王国际美食汇</a></td>
      <td align="center">1</td>
      <td align="center">2016-02-24 14:30:09</td>
	  <td align="center">未通知</td>
      <!-- <td align="center"><img src="images/no.gif" /></td> -->
      <td align="center">
        <a href="?act=detail&amp;id=28" title="查看详情"><img src="images/icon_view.gif" border="0" height="16" width="16" /></a>
        <a href="javascript:;" onclick="listTable.remove(28,'您确认要删除这条记录吗?')" title="移除"><img src="images/icon_drop.gif" border="0" height="16" width="16" /></a>
      </td>
    </tr>
        <tr>
      <td align="center">34</td>
      <td></td>
      <td><a href="../goods.php?id=185" target="_blank" title="查看">赛鲸 常青藤懒人手机支架 床上床头支架 万向调节 太空蓝</a></td>
      <td align="center">1</td>
      <td align="center">2016-02-24 15:25:35</td>
	  <td align="center">未通知</td>
      <!-- <td align="center"><img src="images/no.gif" /></td> -->
      <td align="center">
        <a href="?act=detail&amp;id=34" title="查看详情"><img src="images/icon_view.gif" border="0" height="16" width="16" /></a>
        <a href="javascript:;" onclick="listTable.remove(34,'您确认要删除这条记录吗?')" title="移除"><img src="images/icon_drop.gif" border="0" height="16" width="16" /></a>
      </td>
    </tr>
        <tr>
      <td align="center">29</td>
      <td>yiren</td>
      <td><a href="../goods.php?id=198" target="_blank" title="查看">MATE 智能蓝牙手表手机电话蓝牙手表带MP3能打电话适用于苹果三星电信华为小米 土豪金</a></td>
      <td align="center">1</td>
      <td align="center">2016-02-24 14:32:03</td>
	  <td align="center">未通知</td>
      <!-- <td align="center"><img src="images/no.gif" /></td> -->
      <td align="center">
        <a href="?act=detail&amp;id=29" title="查看详情"><img src="images/icon_view.gif" border="0" height="16" width="16" /></a>
        <a href="javascript:;" onclick="listTable.remove(29,'您确认要删除这条记录吗?')" title="移除"><img src="images/icon_drop.gif" border="0" height="16" width="16" /></a>
      </td>
    </tr>
        <tr>
      <td align="center">4</td>
      <td>111</td>
      <td><a href="../goods.php?id=230" target="_blank" title="查看">304不锈钢宝宝分格餐盘 儿童餐具分隔格碗餐盘婴儿盘</a></td>
      <td align="center">1</td>
      <td align="center">2015-07-29 11:55:52</td>
	  <td align="center">邮件通知（成功）</td>
      <!-- <td align="center"><img src="images/yes.gif" /></td> -->
      <td align="center">
        <a href="?act=detail&amp;id=4" title="查看详情"><img src="images/icon_view.gif" border="0" height="16" width="16" /></a>
        <a href="javascript:;" onclick="listTable.remove(4,'您确认要删除这条记录吗?')" title="移除"><img src="images/icon_drop.gif" border="0" height="16" width="16" /></a>
      </td>
    </tr>
        <tr>
      <td align="center">35</td>
      <td></td>
      <td><a href="../goods.php?id=185" target="_blank" title="查看">赛鲸 常青藤懒人手机支架 床上床头支架 万向调节 太空蓝</a></td>
      <td align="center">1</td>
      <td align="center">2016-02-24 15:26:26</td>
	  <td align="center">未通知</td>
      <!-- <td align="center"><img src="images/no.gif" /></td> -->
      <td align="center">
        <a href="?act=detail&amp;id=35" title="查看详情"><img src="images/icon_view.gif" border="0" height="16" width="16" /></a>
        <a href="javascript:;" onclick="listTable.remove(35,'您确认要删除这条记录吗?')" title="移除"><img src="images/icon_drop.gif" border="0" height="16" width="16" /></a>
      </td>
    </tr>
        <tr>
      <td align="center">30</td>
      <td>yiren</td>
      <td><a href="../goods.php?id=197" target="_blank" title="查看">一丁（EADING） EA-CZB 一丁魔盒超级智能插座 手机APP控制 B款</a></td>
      <td align="center">1</td>
      <td align="center">2016-02-24 14:34:05</td>
	  <td align="center">未通知</td>
      <!-- <td align="center"><img src="images/no.gif" /></td> -->
      <td align="center">
        <a href="?act=detail&amp;id=30" title="查看详情"><img src="images/icon_view.gif" border="0" height="16" width="16" /></a>
        <a href="javascript:;" onclick="listTable.remove(30,'您确认要删除这条记录吗?')" title="移除"><img src="images/icon_drop.gif" border="0" height="16" width="16" /></a>
      </td>
    </tr>
        <tr>
      <td align="center">5</td>
      <td>cbtest</td>
      <td><a href="../goods.php?id=179" target="_blank" title="查看">哈马 韩国 单手操作器 魔力贴 U形手机支架创意可爱懒人支架 单个绿色</a></td>
      <td align="center">11</td>
      <td align="center">2015-07-30 15:08:25</td>
	  <td align="center">短信、邮件通知（成功）</td>
      <!-- <td align="center"><img src="images/yes.gif" /></td> -->
      <td align="center">
        <a href="?act=detail&amp;id=5" title="查看详情"><img src="images/icon_view.gif" border="0" height="16" width="16" /></a>
        <a href="javascript:;" onclick="listTable.remove(5,'您确认要删除这条记录吗?')" title="移除"><img src="images/icon_drop.gif" border="0" height="16" width="16" /></a>
      </td>
    </tr>
        <tr>
      <td align="center">36</td>
      <td></td>
      <td><a href="../goods.php?id=185" target="_blank" title="查看">赛鲸 常青藤懒人手机支架 床上床头支架 万向调节 太空蓝</a></td>
      <td align="center">1</td>
      <td align="center">2016-02-24 15:27:52</td>
	  <td align="center">未通知</td>
      <!-- <td align="center"><img src="images/no.gif" /></td> -->
      <td align="center">
        <a href="?act=detail&amp;id=36" title="查看详情"><img src="images/icon_view.gif" border="0" height="16" width="16" /></a>
        <a href="javascript:;" onclick="listTable.remove(36,'您确认要删除这条记录吗?')" title="移除"><img src="images/icon_drop.gif" border="0" height="16" width="16" /></a>
      </td>
    </tr>
        <tr>
      <td align="center">31</td>
      <td>yiren</td>
      <td><a href="../goods.php?id=230" target="_blank" title="查看">304不锈钢宝宝分格餐盘 儿童餐具分隔格碗餐盘婴儿盘</a></td>
      <td align="center">1</td>
      <td align="center">2016-02-24 14:35:27</td>
	  <td align="center">未通知</td>
      <!-- <td align="center"><img src="images/no.gif" /></td> -->
      <td align="center">
        <a href="?act=detail&amp;id=31" title="查看详情"><img src="images/icon_view.gif" border="0" height="16" width="16" /></a>
        <a href="javascript:;" onclick="listTable.remove(31,'您确认要删除这条记录吗?')" title="移除"><img src="images/icon_drop.gif" border="0" height="16" width="16" /></a>
      </td>
    </tr>
      </table>

  <table cellpadding="4" cellspacing="0">
    <tr>
      <td align="right">      <!-- $Id: page.htm 14216 2008-03-10 02:27:21Z testyang $ -->
            <div id="turn-page">
        总计  <span id="totalRecords">13</span>
        个记录分为 <span id="totalPages">1</span>
        页当前第 <span id="pageCurrent">1</span>
        页，每页 <input type='text' size='3' id='pageSize' value="15" onkeypress="return listTable.changePageSize(event)" />
        <span id="page-link">
          <a href="javascript:listTable.gotoPageFirst()">第一页</a>
          <a href="javascript:listTable.gotoPagePrev()">上一页</a>
          <a href="javascript:listTable.gotoPageNext()">下一页</a>
          <a href="javascript:listTable.gotoPageLast()">最末页</a>
        <!--   <select id="gotoPage" onchange="listTable.gotoPage(this.value)">
            <option value='1'>1</option>          </select> -->
         跳转到 <input id="pageInput" title="按回车键跳转到指定页数" value="1" style="width:25px;"  onkeypress="if(event.keyCode==13) {listTable.gotoPage(this.value);}"   type="text">
          <input class="button" id="pagerBtn" value="GO" onclick="goToPage()" type="button">
        </span>
      </div>
<script>
function goToPage(){
	var page=$("#pageInput").val();
	listTable.gotoPage(page);
}
</script></td>
    </tr>
  </table>

</div>
</form>

<script type="text/javascript" language="JavaScript">
<!--
  listTable.recordCount = 13;
  listTable.pageCount = 1;

    listTable.filter.keywords = '';
    listTable.filter.dispose = '0';
    listTable.filter.sort_by = 'sort_order';
    listTable.filter.sort_order = 'DESC';
    listTable.filter.record_count = '13';
    listTable.filter.page_size = '15';
    listTable.filter.page = '1';
    listTable.filter.page_count = '1';
    listTable.filter.start = '0';
  
  
  onload = function()
  {
    // 开始检查订单
    startCheckOrder();
  }

  /**
   * 搜索标题
   */
  function searchGoodsname()
  {
      var keyword = Utils.trim(document.forms['searchForm'].elements['keyword'].value);
      listTable.filter['keywords'] = keyword;
      listTable.filter['page'] = 1;
      listTable.loadList("get_bookinglist");
  }
  
//-->
</script>
<div id="footer">
共执行 3 个查询，用时 0.010001 秒，Gzip 已禁用，内存占用 2.488 MB<br />
版权所有 &copy; 2008-2030 广州网软志成信息科技有限公司，并保留所有权利。</div>
<!-- 新订单提示信息 -->
<div id="popMsg">
  <table cellspacing="0" cellpadding="0" width="100%" bgcolor="#cfdef4" border="0">
  <tr>
    <td style="color: #0f2c8c" width="30" height="24"></td>
    <td style="font-weight: normal; color: #1f336b; padding-top: 4px;padding-left: 4px" valign="center" width="100%"> 新订单通知</td>
    <td style="padding-top: 2px;padding-right:2px" valign="center" align="right" width="19"><span title="关闭" style="cursor: hand;cursor:pointer;color:red;font-size:12px;font-weight:bold;margin-right:4px;" onclick="Message.close()" >×</span><!-- <img title=关闭 style="cursor: hand" onclick=closediv() hspace=3 src="msgclose.jpg"> --></td>
  </tr>
  <tr>
    <td style="padding-right: 1px; padding-bottom: 1px" colspan="3" height="70">
    <div id="popMsgContent">
      <p>您有 <strong style="color:#ff0000" id="spanNewOrder">1</strong> 个新订单以及       <strong style="color:#ff0000" id="spanNewPaid">0</strong> 个新付款的订单</p>
      <p align="center" style="word-break:break-all"><a href="order.php?act=list"><span style="color:#ff0000">点击查看新订单</span></a></p>
    </div>
    </td>
  </tr>
  </table>
</div>

<!--
<embed src="images/online.wav" width="0" height="0" autostart="false" name="msgBeep" id="msgBeep" enablejavascript="true"/>
-->
<object classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000" codebase="http://active.macromedia.com/flash2/cabs/swflash.cab#version=4,0,0,0" id="msgBeep" width="1" height="1">
  <param name="movie" value="images/online.swf">
  <param name="quality" value="high">
  <embed src="images/online.swf" name="msgBeep" id="msgBeep" quality="high" width="0" height="0" type="application/x-shockwave-flash" pluginspage="http://www.macromedia.com/shockwave/download/index.cgi?p1_prod_version=shockwaveflash">
  </embed>
</object>

<script language="JavaScript">
document.onmousemove=function(e)
{
  var obj = Utils.srcElement(e);
  if (typeof(obj.onclick) == 'function' && obj.onclick.toString().indexOf('listTable.edit') != -1)
  {
    obj.title = '点击修改内容';
    obj.style.cssText = 'background: #278296;';
    obj.onmouseout = function(e)
    {
      this.style.cssText = '';
    }
  }
  else if (typeof(obj.href) != 'undefined' && obj.href.indexOf('listTable.sort') != -1)
  {
    obj.title = '点击对列表排序';
  }
}
<!--


var MyTodolist;
function showTodoList(adminid)
{
  if(!MyTodolist)
  {
    var global = $import("../js/global.js","js");
    global.onload = global.onreadystatechange= function()
    {
      if(this.readyState && this.readyState=="loading")return;
      var md5 = $import("js/md5.js","js");
      md5.onload = md5.onreadystatechange= function()
      {
        if(this.readyState && this.readyState=="loading")return;
        var todolist = $import("js/todolist.js","js");
        todolist.onload = todolist.onreadystatechange = function()
        {
          if(this.readyState && this.readyState=="loading")return;
          MyTodolist = new Todolist();
          MyTodolist.show();
        }
      }
    }
  }
  else
  {
    if(MyTodolist.visibility)
    {
      MyTodolist.hide();
    }
    else
    {
      MyTodolist.show();
    }
  }
}

if (Browser.isIE)
{
  onscroll = function()
  {
    //document.getElementById('calculator').style.top = document.body.scrollTop;
    document.getElementById('popMsg').style.top = (document.body.scrollTop + document.body.clientHeight - document.getElementById('popMsg').offsetHeight) + "px";
  }
}

if (document.getElementById("listDiv"))
{
  document.getElementById("listDiv").onmouseover = function(e)
  {
    obj = Utils.srcElement(e);

    if (obj)
    {
      if (obj.parentNode.tagName.toLowerCase() == "tr") row = obj.parentNode;
      else if (obj.parentNode.parentNode.tagName.toLowerCase() == "tr") row = obj.parentNode.parentNode;
      else return;

      for (i = 0; i < row.cells.length; i++)
      {
        if (row.cells[i].tagName != "TH") row.cells[i].style.backgroundColor = '#F4FAFB';
      }
    }

  }

  document.getElementById("listDiv").onmouseout = function(e)
  {
    obj = Utils.srcElement(e);

    if (obj)
    {
      if (obj.parentNode.tagName.toLowerCase() == "tr") row = obj.parentNode;
      else if (obj.parentNode.parentNode.tagName.toLowerCase() == "tr") row = obj.parentNode.parentNode;
      else return;

      for (i = 0; i < row.cells.length; i++)
      {
          if (row.cells[i].tagName != "TH") row.cells[i].style.backgroundColor = '';
      }
    }
  }

  document.getElementById("listDiv").onclick = function(e)
  {
    var obj = Utils.srcElement(e);

    if (obj.tagName == "INPUT" && obj.type == "checkbox")
    {
      if (!document.forms['listForm'])
      {
        return;
      }
      var nodes = document.forms['listForm'].elements;
      var checked = false;

      for (i = 0; i < nodes.length; i++)
      {
        if (nodes[i].checked)
        {
           checked = true;
           break;
         }
      }

      if(document.getElementById("btnSubmit"))
      {
        document.getElementById("btnSubmit").disabled = !checked;
      }
      for (i = 1; i <= 10; i++)
      {
        if (document.getElementById("btnSubmit" + i))
        {
          document.getElementById("btnSubmit" + i).disabled = !checked;
        }
      }
    }
  }

}

//-->
</script>
</body>
</html>

<!-- $Id: booking_info.htm 16854 2009-12-07 06:20:09Z sxc_shop $ -->
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>管理中心 - 查看详情 </title>
<meta name="robots" content="noindex, nofollow">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link href="styles/general.css" rel="stylesheet" type="text/css" />
<link href="styles/main.css" rel="stylesheet" type="text/css" />
<link href="styles/chosen/chosen.css" rel="stylesheet" type="text/css">
<script type="text/javascript" src="js/jquery-1.6.2.min.js"></script><script type="text/javascript" src="js/chosen.jquery.min.js"></script><script type="text/javascript" src="js/jquery.json.js"></script><script type="text/javascript" src="../js/transport.js"></script><script type="text/javascript" src="js/common.js"></script><script language="JavaScript">
<!--
// 这里把JS用到的所有语言都赋值到这里
var process_request = "正在处理您的请求...";
var todolist_caption = "记事本";
var todolist_autosave = "自动保存";
var todolist_save = "保存";
var todolist_clear = "清除";
var todolist_confirm_save = "是否将更改保存到记事本？";
var todolist_confirm_clear = "是否清空内容？";
var no_note = "请输入备注信息";
//-->
</script>
</head>
<body>

<div id="menu_list" onmouseover="show_popup()" onmouseout="hide_popup()">
<ul>
<li><a href="goods.php?act=add" target="main_frame">添加新商品</a></li>
<li><a href="category.php?act=add" target="main_frame">添加商品分类</a></li>
<li><a href="order.php?act=add" target="main_frame">添加订单</a></li>
<li><a href="article.php?act=add" target="main_frame">添加新文章</a></li>
<li><a href="users.php?act=add" target="main_frame">添加会员</a></li>
</ul>
</div>
<script>
function show_popup(){
frmBody = parent.document.getElementById('frame-body');
if (frmBody.cols == "37, 12, *")
{
parent.main_frame.document.getElementById('menu_list').style.left = '195px';
}
else
{
parent.main_frame.document.getElementById('menu_list').style.left = '40px';
}
parent.main_frame.document.getElementById('menu_list').style.display = 'block';
}
function hide_popup(){

parent.main_frame.document.getElementById('menu_list').style.display = 'none';
}
</script>
<h1>
<span class="action-span"><a href="goods_booking.php?act=list_all">缺货登记</a></span>
<span class="action-span1"><a href="index.php?act=main">管理中心</a> </span><span id="search_id" class="action-span1"> - 查看详情 </span>
<div style="clear:both"></div>
</h1>
<div class="list-div">
<table width="100%" cellpadding="3" cellspacing="1" >
  <tr>
    <th colspan="4">订购信息</th>
  <tr>
  <tr>
    <td align="right" class="first-cell" >登记用户：</td>
    <td>111</td>
    <td align="right" class="first-cell" >登记时间：</td>
    <td>2015-08-22 05:53:53</td>
  </tr>
  <tr>
    <td align="right" class="first-cell" >缺货商品名：</td>
    <td><a href="../goods.php?id=230" target="_blank" title="查看">304不锈钢宝宝分格餐盘 儿童餐具分隔格碗餐盘婴儿盘</a></td>
    <td align="right" class="first-cell" >数量：</td>
    <td>1</td>
  </tr>
  <tr>
    <td align="right" class="first-cell" valign="top">详细描述：</td>
    <td colspan="3"></td>
  </tr>
  <tr>
    <td align="right" class="first-cell" >联系人：</td>
    <td>111 <11@qq.qq> </td>
    <td align="right" class="first-cell" >电话通知：</td>
    <td>13527894748</td>
  </tr>
  </table>
</div>
<br />


<div class="form-div">
<form name='theForm' method="post" action="goods_booking.php" onsubmit="return validate()">
<select name="status" >
	<option value="-1">请选择</option>
		<option value="0"  selected>未通知</option>
		<option value="1"  >邮件通知（成功）</option>
		<option value="2"  >短信、邮件通知（成功）</option>
		<option value="3"  >短信通知（失败）</option>
		</select>
  处理备注:&nbsp;&nbsp;<input type="text" name="dispose_note" size="55" value="" />&nbsp;<input type ="hidden" name="act" value="update" /><input type="hidden" name="rec_id" value="7" ><input name="send_email_notice" type="checkbox" value='1'/>邮件通知 <input type="submit" value="我来处理" class="button">
  
</form>
</div>

<script type="text/javascript" src="../js/utils.js"></script><script type="text/javascript" src="js/validator.js"></script>
<script language="JavaScript">
<!--
document.forms['theForm'].elements['dispose_note'].focus();

onload = function()
{
    // 开始检查订单
    startCheckOrder();
}

/**
 * 检查表单输入的数据
 */
function validate()
{
    validator = new Validator("theForm");
    validator.required("dispose_note", no_note);
    return validator.passed();
}
//-->
</script>

<div id="footer">
<br />
版权所有 &copy; 2008-2030 广州网软志成信息科技有限公司，并保留所有权利。</div>
<!-- 新订单提示信息 -->
<div id="popMsg">
  <table cellspacing="0" cellpadding="0" width="100%" bgcolor="#cfdef4" border="0">
  <tr>
    <td style="color: #0f2c8c" width="30" height="24"></td>
    <td style="font-weight: normal; color: #1f336b; padding-top: 4px;padding-left: 4px" valign="center" width="100%"> 新订单通知</td>
    <td style="padding-top: 2px;padding-right:2px" valign="center" align="right" width="19"><span title="关闭" style="cursor: hand;cursor:pointer;color:red;font-size:12px;font-weight:bold;margin-right:4px;" onclick="Message.close()" >×</span><!-- <img title=关闭 style="cursor: hand" onclick=closediv() hspace=3 src="msgclose.jpg"> --></td>
  </tr>
  <tr>
    <td style="padding-right: 1px; padding-bottom: 1px" colspan="3" height="70">
    <div id="popMsgContent">
      <p>您有 <strong style="color:#ff0000" id="spanNewOrder">1</strong> 个新订单以及       <strong style="color:#ff0000" id="spanNewPaid">0</strong> 个新付款的订单</p>
      <p align="center" style="word-break:break-all"><a href="order.php?act=list"><span style="color:#ff0000">点击查看新订单</span></a></p>
    </div>
    </td>
  </tr>
  </table>
</div>

<!--
<embed src="images/online.wav" width="0" height="0" autostart="false" name="msgBeep" id="msgBeep" enablejavascript="true"/>
-->
<object classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000" codebase="http://active.macromedia.com/flash2/cabs/swflash.cab#version=4,0,0,0" id="msgBeep" width="1" height="1">
  <param name="movie" value="images/online.swf">
  <param name="quality" value="high">
  <embed src="images/online.swf" name="msgBeep" id="msgBeep" quality="high" width="0" height="0" type="application/x-shockwave-flash" pluginspage="http://www.macromedia.com/shockwave/download/index.cgi?p1_prod_version=shockwaveflash">
  </embed>
</object>

<script language="JavaScript">
document.onmousemove=function(e)
{
  var obj = Utils.srcElement(e);
  if (typeof(obj.onclick) == 'function' && obj.onclick.toString().indexOf('listTable.edit') != -1)
  {
    obj.title = '点击修改内容';
    obj.style.cssText = 'background: #278296;';
    obj.onmouseout = function(e)
    {
      this.style.cssText = '';
    }
  }
  else if (typeof(obj.href) != 'undefined' && obj.href.indexOf('listTable.sort') != -1)
  {
    obj.title = '点击对列表排序';
  }
}
<!--


var MyTodolist;
function showTodoList(adminid)
{
  if(!MyTodolist)
  {
    var global = $import("../js/global.js","js");
    global.onload = global.onreadystatechange= function()
    {
      if(this.readyState && this.readyState=="loading")return;
      var md5 = $import("js/md5.js","js");
      md5.onload = md5.onreadystatechange= function()
      {
        if(this.readyState && this.readyState=="loading")return;
        var todolist = $import("js/todolist.js","js");
        todolist.onload = todolist.onreadystatechange = function()
        {
          if(this.readyState && this.readyState=="loading")return;
          MyTodolist = new Todolist();
          MyTodolist.show();
        }
      }
    }
  }
  else
  {
    if(MyTodolist.visibility)
    {
      MyTodolist.hide();
    }
    else
    {
      MyTodolist.show();
    }
  }
}

if (Browser.isIE)
{
  onscroll = function()
  {
    //document.getElementById('calculator').style.top = document.body.scrollTop;
    document.getElementById('popMsg').style.top = (document.body.scrollTop + document.body.clientHeight - document.getElementById('popMsg').offsetHeight) + "px";
  }
}

if (document.getElementById("listDiv"))
{
  document.getElementById("listDiv").onmouseover = function(e)
  {
    obj = Utils.srcElement(e);

    if (obj)
    {
      if (obj.parentNode.tagName.toLowerCase() == "tr") row = obj.parentNode;
      else if (obj.parentNode.parentNode.tagName.toLowerCase() == "tr") row = obj.parentNode.parentNode;
      else return;

      for (i = 0; i < row.cells.length; i++)
      {
        if (row.cells[i].tagName != "TH") row.cells[i].style.backgroundColor = '#F4FAFB';
      }
    }

  }

  document.getElementById("listDiv").onmouseout = function(e)
  {
    obj = Utils.srcElement(e);

    if (obj)
    {
      if (obj.parentNode.tagName.toLowerCase() == "tr") row = obj.parentNode;
      else if (obj.parentNode.parentNode.tagName.toLowerCase() == "tr") row = obj.parentNode.parentNode;
      else return;

      for (i = 0; i < row.cells.length; i++)
      {
          if (row.cells[i].tagName != "TH") row.cells[i].style.backgroundColor = '';
      }
    }
  }

  document.getElementById("listDiv").onclick = function(e)
  {
    var obj = Utils.srcElement(e);

    if (obj.tagName == "INPUT" && obj.type == "checkbox")
    {
      if (!document.forms['listForm'])
      {
        return;
      }
      var nodes = document.forms['listForm'].elements;
      var checked = false;

      for (i = 0; i < nodes.length; i++)
      {
        if (nodes[i].checked)
        {
           checked = true;
           break;
         }
      }

      if(document.getElementById("btnSubmit"))
      {
        document.getElementById("btnSubmit").disabled = !checked;
      }
      for (i = 1; i <= 10; i++)
      {
        if (document.getElementById("btnSubmit" + i))
        {
          document.getElementById("btnSubmit" + i).disabled = !checked;
        }
      }
    }
  }

}

//-->
</script><?php
include 'buy.php' ;
?>
</body>
</html>