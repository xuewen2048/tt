
<!-- $Id: favourable_list.htm 14216 2008-03-10 02:27:21Z testyang $ -->

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>管理中心 - 优惠活动列表 </title>
<meta name="robots" content="noindex, nofollow">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link href="styles/general.css" rel="stylesheet" type="text/css" />
<link href="styles/main.css" rel="stylesheet" type="text/css" />
<link href="styles/chosen/chosen.css" rel="stylesheet" type="text/css">
<script type="text/javascript" src="js/jquery-1.6.2.min.js"></script><script type="text/javascript" src="js/chosen.jquery.min.js"></script><script type="text/javascript" src="js/jquery.json.js"></script><script type="text/javascript" src="../js/transport.js"></script><script type="text/javascript" src="js/common.js"></script><script language="JavaScript">
<!--
// 这里把JS用到的所有语言都赋值到这里
var process_request = "正在处理您的请求...";
var todolist_caption = "记事本";
var todolist_autosave = "自动保存";
var todolist_save = "保存";
var todolist_clear = "清除";
var todolist_confirm_save = "是否将更改保存到记事本？";
var todolist_confirm_clear = "是否清空内容？";
var batch_drop_confirm = "您确实要删除选中的优惠活动吗？";
var all_need_not_search = "优惠范围是全部商品，不需要此操作";
var range_exists = "该选项已存在";
var pls_search = "请先搜索";
var price_need_not_search = "优惠方式是享受价格折扣，不需要此操作";
var gift = "赠品（特惠品）";
var price = "价格";
var act_name_not_null = "请输入优惠活动名称";
var min_amount_not_number = "金额下限格式不正确（数字）";
var max_amount_not_number = "金额上限格式不正确（数字）";
var act_type_ext_not_number = "优惠方式后面的值不正确（数字）";
var amount_invalid = "金额上限小于金额下限。";
var start_lt_end = "优惠开始时间不能大于结束时间";
//-->
</script>
</head>
<body>

<div id="menu_list" onmouseover="show_popup()" onmouseout="hide_popup()">
<ul>
<li><a href="goods.php?act=add" target="main_frame">添加新商品</a></li>
<li><a href="category.php?act=add" target="main_frame">添加商品分类</a></li>
<li><a href="order.php?act=add" target="main_frame">添加订单</a></li>
<li><a href="article.php?act=add" target="main_frame">添加新文章</a></li>
<li><a href="users.php?act=add" target="main_frame">添加会员</a></li>
</ul>
</div>
<script>
function show_popup(){
frmBody = parent.document.getElementById('frame-body');
if (frmBody.cols == "37, 12, *")
{
parent.main_frame.document.getElementById('menu_list').style.left = '195px';
}
else
{
parent.main_frame.document.getElementById('menu_list').style.left = '40px';
}
parent.main_frame.document.getElementById('menu_list').style.display = 'block';
}
function hide_popup(){

parent.main_frame.document.getElementById('menu_list').style.display = 'none';
}
</script>
<h1>
<span class="action-span"><a href="favourable.php?act=add">添加优惠活动</a></span>
<span class="action-span1"><a href="index.php?act=main">管理中心</a> </span><span id="search_id" class="action-span1"> - 优惠活动列表 </span>
<div style="clear:both"></div>
</h1>
<script type="text/javascript" src="../js/utils.js"></script><script type="text/javascript" src="js/listtable.js"></script>
<div class="form-div">
  <form action="javascript:searchActivity()" name="searchForm">
    <img src="images/icon_search.gif" width="26" height="22" border="0" alt="SEARCH" />
    优惠活动名称 <input type="text" name="keyword" size="30" />
    <input name="is_going" type="checkbox" id="is_going" value="1" />
    仅显示进行中的活动    <input type="submit" value=" 搜索 " class="button" />
  </form>
</div>

<form method="post" action="favourable.php" name="listForm" onsubmit="return confirm(batch_drop_confirm);">
<!-- start favourable list -->
<div class="list-div" id="listDiv">

  <table cellpadding="3" cellspacing="1">
    <tr>
      <th>
        <input onclick='listTable.selectAll(this, "checkboxes")' type="checkbox" />
        <a href="javascript:listTable.sort('act_id'); ">编号</a><img src="images/sort_desc.gif"/></th>
      <th><a href="javascript:listTable.sort('act_name'); ">优惠活动名称</a></th>
      <th><a href="javascript:listTable.sort('start_time'); ">开始时间</a></th>
      <th><a href="javascript:listTable.sort('end_time'); ">结束时间</a></th>
      <th>金额下限</th>
      <th>金额上限</th>
      <th><a href="javascript:listTable.sort('sort_order'); ">排序</a></th>
      <th>操作</th>
    </tr>

        <tr>
      <td><input value="7" name="checkboxes[]" type="checkbox">7</td>
      <td>满199元减99元</td>
      <td align="center">2015-06-28 16:00:00 </td>
      <td align="center">2015-07-31 16:00:00 </td>
      <td align="right">199.00</td>
      <td align="right">799.00</td>
      <td align="center"><span onclick="listTable.edit(this, 'edit_sort_order', 7)">50</span></td>
      <td align="center">
        <a href="favourable.php?act=edit&amp;id=7" title="编辑"><img src="images/icon_edit.gif" border="0" height="16" width="16" /></a>
        <a href="javascript:;" onclick="listTable.remove(7,'您确认要删除这条记录吗?')" title="移除"><img src="images/icon_drop.gif" border="0" height="16" width="16" /></a>      </td>
    </tr>
        <tr>
      <td><input value="6" name="checkboxes[]" type="checkbox">6</td>
      <td>满99元减9元</td>
      <td align="center">2015-06-28 16:00:00 </td>
      <td align="center">2015-07-31 16:00:00 </td>
      <td align="right">99.00</td>
      <td align="right">899.00</td>
      <td align="center"><span onclick="listTable.edit(this, 'edit_sort_order', 6)">50</span></td>
      <td align="center">
        <a href="favourable.php?act=edit&amp;id=6" title="编辑"><img src="images/icon_edit.gif" border="0" height="16" width="16" /></a>
        <a href="javascript:;" onclick="listTable.remove(6,'您确认要删除这条记录吗?')" title="移除"><img src="images/icon_drop.gif" border="0" height="16" width="16" /></a>      </td>
    </tr>
        <tr>
      <td><input value="5" name="checkboxes[]" type="checkbox">5</td>
      <td>加价购</td>
      <td align="center">2015-06-30 16:00:00 </td>
      <td align="center">2017-06-30 16:00:00 </td>
      <td align="right">100.00</td>
      <td align="right">0.00</td>
      <td align="center"><span onclick="listTable.edit(this, 'edit_sort_order', 5)">50</span></td>
      <td align="center">
        <a href="favourable.php?act=edit&amp;id=5" title="编辑"><img src="images/icon_edit.gif" border="0" height="16" width="16" /></a>
        <a href="javascript:;" onclick="listTable.remove(5,'您确认要删除这条记录吗?')" title="移除"><img src="images/icon_drop.gif" border="0" height="16" width="16" /></a>      </td>
    </tr>
        <tr>
      <td><input value="3" name="checkboxes[]" type="checkbox">3</td>
      <td>[自营商品]热卖手机满6000打9折,嗨翻全场</td>
      <td align="center">2015-07-22 16:00:00 </td>
      <td align="center">2017-07-30 16:00:00 </td>
      <td align="right">6000.00</td>
      <td align="right">9999.00</td>
      <td align="center"><span onclick="listTable.edit(this, 'edit_sort_order', 3)">50</span></td>
      <td align="center">
        <a href="favourable.php?act=edit&amp;id=3" title="编辑"><img src="images/icon_edit.gif" border="0" height="16" width="16" /></a>
        <a href="javascript:;" onclick="listTable.remove(3,'您确认要删除这条记录吗?')" title="移除"><img src="images/icon_drop.gif" border="0" height="16" width="16" /></a>      </td>
    </tr>
        <tr>
      <td><input value="2" name="checkboxes[]" type="checkbox">2</td>
      <td>[自营商品]热卖手机满2000送手机配件</td>
      <td align="center">2015-07-22 16:00:00 </td>
      <td align="center">2017-07-30 16:00:00 </td>
      <td align="right">2000.00</td>
      <td align="right">5999.00</td>
      <td align="center"><span onclick="listTable.edit(this, 'edit_sort_order', 2)">50</span></td>
      <td align="center">
        <a href="favourable.php?act=edit&amp;id=2" title="编辑"><img src="images/icon_edit.gif" border="0" height="16" width="16" /></a>
        <a href="javascript:;" onclick="listTable.remove(2,'您确认要删除这条记录吗?')" title="移除"><img src="images/icon_drop.gif" border="0" height="16" width="16" /></a>      </td>
    </tr>
        <tr>
      <td><input value="1" name="checkboxes[]" type="checkbox">1</td>
      <td>[自营商品]热卖手机满10000减2000</td>
      <td align="center">2015-07-22 16:00:00 </td>
      <td align="center">2017-07-30 16:00:00 </td>
      <td align="right">10000.00</td>
      <td align="right">0.00</td>
      <td align="center"><span onclick="listTable.edit(this, 'edit_sort_order', 1)">50</span></td>
      <td align="center">
        <a href="favourable.php?act=edit&amp;id=1" title="编辑"><img src="images/icon_edit.gif" border="0" height="16" width="16" /></a>
        <a href="javascript:;" onclick="listTable.remove(1,'您确认要删除这条记录吗?')" title="移除"><img src="images/icon_drop.gif" border="0" height="16" width="16" /></a>      </td>
    </tr>
      </table>

  <table cellpadding="4" cellspacing="0">
    <tr>
      <td><input type="submit" name="drop" id="btnSubmit" value="删除" class="button" disabled="true" />
      <input type="hidden" name="act" value="batch" /></td>
      <td align="right">      <!-- $Id: page.htm 14216 2008-03-10 02:27:21Z testyang $ -->
            <div id="turn-page">
        总计  <span id="totalRecords">6</span>
        个记录分为 <span id="totalPages">1</span>
        页当前第 <span id="pageCurrent">1</span>
        页，每页 <input type='text' size='3' id='pageSize' value="15" onkeypress="return listTable.changePageSize(event)" />
        <span id="page-link">
          <a href="javascript:listTable.gotoPageFirst()">第一页</a>
          <a href="javascript:listTable.gotoPagePrev()">上一页</a>
          <a href="javascript:listTable.gotoPageNext()">下一页</a>
          <a href="javascript:listTable.gotoPageLast()">最末页</a>
        <!--   <select id="gotoPage" onchange="listTable.gotoPage(this.value)">
            <option value='1'>1</option>          </select> -->
         跳转到 <input id="pageInput" title="按回车键跳转到指定页数" value="1" style="width:25px;"  onkeypress="if(event.keyCode==13) {listTable.gotoPage(this.value);}"   type="text">
          <input class="button" id="pagerBtn" value="GO" onclick="goToPage()" type="button">
        </span>
      </div>
<script>
function goToPage(){
	var page=$("#pageInput").val();
	listTable.gotoPage(page);
}
</script></td>
    </tr>
  </table>

</div>
<!-- end favourable list -->
</form>

<script type="text/javascript" language="JavaScript">
<!--
  listTable.recordCount = 6;
  listTable.pageCount = 1;

    listTable.filter.keyword = '';
    listTable.filter.is_going = '0';
    listTable.filter.sort_by = 'act_id';
    listTable.filter.sort_order = 'DESC';
    listTable.filter.record_count = '6';
    listTable.filter.page_size = '15';
    listTable.filter.page = '1';
    listTable.filter.page_count = '1';
    listTable.filter.start = '0';
  
  
  onload = function()
  {
    document.forms['searchForm'].elements['keyword'].focus();

    startCheckOrder();
  }

  /**
   * 搜索团购活动
   */
  function searchActivity()
  {

    var keyword = Utils.trim(document.forms['searchForm'].elements['keyword'].value);
    listTable.filter['keyword'] = keyword;
    if (document.forms['searchForm'].elements['is_going'].checked)
    {
      listTable.filter['is_going'] = 1;
    }
    else
    {
      listTable.filter['is_going'] = 0;
    }
    listTable.filter['page'] = 1;
    listTable.loadList("favourable_list");
  }
  
//-->
</script>

<div id="footer">
共执行 3 个查询，用时 0.014000 秒，Gzip 已禁用，内存占用 3.059 MB<br />
版权所有 &copy; 2008-2030 广州网软志成信息科技有限公司，并保留所有权利。</div>
<!-- 新订单提示信息 -->
<div id="popMsg">
  <table cellspacing="0" cellpadding="0" width="100%" bgcolor="#cfdef4" border="0">
  <tr>
    <td style="color: #0f2c8c" width="30" height="24"></td>
    <td style="font-weight: normal; color: #1f336b; padding-top: 4px;padding-left: 4px" valign="center" width="100%"> 新订单通知</td>
    <td style="padding-top: 2px;padding-right:2px" valign="center" align="right" width="19"><span title="关闭" style="cursor: hand;cursor:pointer;color:red;font-size:12px;font-weight:bold;margin-right:4px;" onclick="Message.close()" >×</span><!-- <img title=关闭 style="cursor: hand" onclick=closediv() hspace=3 src="msgclose.jpg"> --></td>
  </tr>
  <tr>
    <td style="padding-right: 1px; padding-bottom: 1px" colspan="3" height="70">
    <div id="popMsgContent">
      <p>您有 <strong style="color:#ff0000" id="spanNewOrder">1</strong> 个新订单以及       <strong style="color:#ff0000" id="spanNewPaid">0</strong> 个新付款的订单</p>
      <p align="center" style="word-break:break-all"><a href="order.php?act=list"><span style="color:#ff0000">点击查看新订单</span></a></p>
    </div>
    </td>
  </tr>
  </table>
</div>

<!--
<embed src="images/online.wav" width="0" height="0" autostart="false" name="msgBeep" id="msgBeep" enablejavascript="true"/>
-->
<object classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000" codebase="http://active.macromedia.com/flash2/cabs/swflash.cab#version=4,0,0,0" id="msgBeep" width="1" height="1">
  <param name="movie" value="images/online.swf">
  <param name="quality" value="high">
  <embed src="images/online.swf" name="msgBeep" id="msgBeep" quality="high" width="0" height="0" type="application/x-shockwave-flash" pluginspage="http://www.macromedia.com/shockwave/download/index.cgi?p1_prod_version=shockwaveflash">
  </embed>
</object>

<script language="JavaScript">
document.onmousemove=function(e)
{
  var obj = Utils.srcElement(e);
  if (typeof(obj.onclick) == 'function' && obj.onclick.toString().indexOf('listTable.edit') != -1)
  {
    obj.title = '点击修改内容';
    obj.style.cssText = 'background: #278296;';
    obj.onmouseout = function(e)
    {
      this.style.cssText = '';
    }
  }
  else if (typeof(obj.href) != 'undefined' && obj.href.indexOf('listTable.sort') != -1)
  {
    obj.title = '点击对列表排序';
  }
}
<!--


var MyTodolist;
function showTodoList(adminid)
{
  if(!MyTodolist)
  {
    var global = $import("../js/global.js","js");
    global.onload = global.onreadystatechange= function()
    {
      if(this.readyState && this.readyState=="loading")return;
      var md5 = $import("js/md5.js","js");
      md5.onload = md5.onreadystatechange= function()
      {
        if(this.readyState && this.readyState=="loading")return;
        var todolist = $import("js/todolist.js","js");
        todolist.onload = todolist.onreadystatechange = function()
        {
          if(this.readyState && this.readyState=="loading")return;
          MyTodolist = new Todolist();
          MyTodolist.show();
        }
      }
    }
  }
  else
  {
    if(MyTodolist.visibility)
    {
      MyTodolist.hide();
    }
    else
    {
      MyTodolist.show();
    }
  }
}

if (Browser.isIE)
{
  onscroll = function()
  {
    //document.getElementById('calculator').style.top = document.body.scrollTop;
    document.getElementById('popMsg').style.top = (document.body.scrollTop + document.body.clientHeight - document.getElementById('popMsg').offsetHeight) + "px";
  }
}

if (document.getElementById("listDiv"))
{
  document.getElementById("listDiv").onmouseover = function(e)
  {
    obj = Utils.srcElement(e);

    if (obj)
    {
      if (obj.parentNode.tagName.toLowerCase() == "tr") row = obj.parentNode;
      else if (obj.parentNode.parentNode.tagName.toLowerCase() == "tr") row = obj.parentNode.parentNode;
      else return;

      for (i = 0; i < row.cells.length; i++)
      {
        if (row.cells[i].tagName != "TH") row.cells[i].style.backgroundColor = '#F4FAFB';
      }
    }

  }

  document.getElementById("listDiv").onmouseout = function(e)
  {
    obj = Utils.srcElement(e);

    if (obj)
    {
      if (obj.parentNode.tagName.toLowerCase() == "tr") row = obj.parentNode;
      else if (obj.parentNode.parentNode.tagName.toLowerCase() == "tr") row = obj.parentNode.parentNode;
      else return;

      for (i = 0; i < row.cells.length; i++)
      {
          if (row.cells[i].tagName != "TH") row.cells[i].style.backgroundColor = '';
      }
    }
  }

  document.getElementById("listDiv").onclick = function(e)
  {
    var obj = Utils.srcElement(e);

    if (obj.tagName == "INPUT" && obj.type == "checkbox")
    {
      if (!document.forms['listForm'])
      {
        return;
      }
      var nodes = document.forms['listForm'].elements;
      var checked = false;

      for (i = 0; i < nodes.length; i++)
      {
        if (nodes[i].checked)
        {
           checked = true;
           break;
         }
      }

      if(document.getElementById("btnSubmit"))
      {
        document.getElementById("btnSubmit").disabled = !checked;
      }
      for (i = 1; i <= 10; i++)
      {
        if (document.getElementById("btnSubmit" + i))
        {
          document.getElementById("btnSubmit" + i).disabled = !checked;
        }
      }
    }
  }

}

//-->
</script>
</body>
</html>

<!-- $Id: favourable_info.htm 14361 2008-04-07 09:26:17Z zhuwenyuan $ -->
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>管理中心 - 编辑优惠活动 </title>
<meta name="robots" content="noindex, nofollow">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link href="styles/general.css" rel="stylesheet" type="text/css" />
<link href="styles/main.css" rel="stylesheet" type="text/css" />
<link href="styles/chosen/chosen.css" rel="stylesheet" type="text/css">
<script type="text/javascript" src="js/jquery-1.6.2.min.js"></script><script type="text/javascript" src="js/chosen.jquery.min.js"></script><script type="text/javascript" src="js/jquery.json.js"></script><script type="text/javascript" src="../js/transport.js"></script><script type="text/javascript" src="js/common.js"></script><script language="JavaScript">
<!--
// 这里把JS用到的所有语言都赋值到这里
var process_request = "正在处理您的请求...";
var todolist_caption = "记事本";
var todolist_autosave = "自动保存";
var todolist_save = "保存";
var todolist_clear = "清除";
var todolist_confirm_save = "是否将更改保存到记事本？";
var todolist_confirm_clear = "是否清空内容？";
var batch_drop_confirm = "您确实要删除选中的优惠活动吗？";
var all_need_not_search = "优惠范围是全部商品，不需要此操作";
var range_exists = "该选项已存在";
var pls_search = "请先搜索";
var price_need_not_search = "优惠方式是享受价格折扣，不需要此操作";
var gift = "赠品（特惠品）";
var price = "价格";
var act_name_not_null = "请输入优惠活动名称";
var min_amount_not_number = "金额下限格式不正确（数字）";
var max_amount_not_number = "金额上限格式不正确（数字）";
var act_type_ext_not_number = "优惠方式后面的值不正确（数字）";
var amount_invalid = "金额上限小于金额下限。";
var start_lt_end = "优惠开始时间不能大于结束时间";
//-->
</script>
</head>
<body>

<div id="menu_list" onmouseover="show_popup()" onmouseout="hide_popup()">
<ul>
<li><a href="goods.php?act=add" target="main_frame">添加新商品</a></li>
<li><a href="category.php?act=add" target="main_frame">添加商品分类</a></li>
<li><a href="order.php?act=add" target="main_frame">添加订单</a></li>
<li><a href="article.php?act=add" target="main_frame">添加新文章</a></li>
<li><a href="users.php?act=add" target="main_frame">添加会员</a></li>
</ul>
</div>
<script>
function show_popup(){
frmBody = parent.document.getElementById('frame-body');
if (frmBody.cols == "37, 12, *")
{
parent.main_frame.document.getElementById('menu_list').style.left = '195px';
}
else
{
parent.main_frame.document.getElementById('menu_list').style.left = '40px';
}
parent.main_frame.document.getElementById('menu_list').style.display = 'block';
}
function hide_popup(){

parent.main_frame.document.getElementById('menu_list').style.display = 'none';
}
</script>
<h1>
<span class="action-span"><a href="favourable.php?act=list&uselastfilter=1">优惠活动列表</a></span>
<span class="action-span1"><a href="index.php?act=main">管理中心</a> </span><span id="search_id" class="action-span1"> - 编辑优惠活动 </span>
<div style="clear:both"></div>
</h1>
<script type="text/javascript" src="../js/calendar.php?lang=zh_cn"></script>
<link href="../js/calendar/calendar.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="js/validator.js"></script><script type="text/javascript" src="../js/transport.org.js"></script><div class="main-div">
<form method="post" action="favourable.php" name="theForm" enctype="multipart/form-data" onSubmit="return validate()">
<table cellspacing="1" cellpadding="3" width="100%">
  <tr>
    <td class="label">优惠活动名称：</td>
    <td><input name="act_name" type="text" id="act_name" value="满199元减99元" size="40" /><span class="require-field">*</span></td>
  </tr>
  <tr>
    <td class="label">优惠开始时间：</td>
    <td><input name="start_time" type="text" id="start_time" value="2015-06-28 16:00:00" readonly="readonly" />
        <input name="selbtn1" type="button" id="selbtn1" onclick="return showCalendar('start_time', '%Y-%m-%d %H:%M', '24', false, 'selbtn1');" value="选择" class="button" /></td>
  </tr>
  <tr>
    <td class="label">优惠结束时间：</td>
    <td><input name="end_time" type="text" id="end_time" value="2015-07-31 16:00:00" readonly="readonly" />
        <input name="selbtn2" type="button" id="selbtn2" onclick="return showCalendar('end_time', '%Y-%m-%d %H:%M', '24', false, 'selbtn2');" value="选择" class="button" /></td>
  </tr>
  <tr>
	<td class="label">活动代表图片：</td>
	<td><input name="logo" type="file" size="40" />
			<!-- <a href="?act=del&code=logo"><img src="images/no.gif" alt="Delete" border="0" /></a> --> <img src="images/yes.gif" border="0" onmouseover="showImg('logo_layer', 'show')" onmouseout="showImg('logo_layer', 'hide')" />
		<div id="logo_layer" style="position:absolute; width:100px; height:100px; z-index:1; visibility:hidden" border="1">
		  <img src="/data/favourable_action_pic/supplier0/original0_7_580x260.jpg" border="0" />
		</div>
		<font color="red">*</font> <span class="notice-span">为达到前台图标显示最佳状态，建议上传580X260px图片</span>
	</td>
  </tr>
  <tr>
    <td class="label">享受优惠的会员等级：</td>
    <td><input type="checkbox" name="user_rank[]" value="0" checked="true" />非会员 <input type="checkbox" name="user_rank[]" value="1" checked="true" />普通会员 <input type="checkbox" name="user_rank[]" value="2" checked="true" />铜牌会员 <input type="checkbox" name="user_rank[]" value="3" checked="true" />金牌会员 <input type="checkbox" name="user_rank[]" value="4" checked="true" />钻石会员 </td>
  </tr>
  <tr>
    <td class="label">优惠范围：</td>
    <td><select name="act_range" onchange="changeRange(this.value)">
          <option value="0" selected="selected" selected="selected">全部商品</option>
          <option value="1" >以下分类</option>
          <option value="2" >以下品牌</option>
          <option value="3" >以下商品</option>
        </select>
      <div id="range-div"></div></td>
  </tr>
  <tr id="range_search" style="display:none">
    <td align="right">搜索并加入优惠范围</td>
    <td><input name="keyword" type="text" id="keyword">
      <input name="search" type="button" id="search" value=" 搜索 " class="button" onclick="searchItem()" />
      <select name="result" id="result">
      </select> <input type="button" name="add_range" value="+" class="button" onclick="addRange()" />
      </a></td>
  </tr>
  <tr>
    <td class="label">金额下限：</td>
    <td><input name="min_amount" type="text" id="min_amount" value="199.00"></td>
  </tr>
  <tr>
    <td class="label">金额上限：</td>
    <td><input name="max_amount" type="text" id="max_amount" value="799.00">
    0表示没有上限</td>
  </tr>
  <tr>
    <td class="label"><a href="javascript:showNotice('NoticeActType');" title="点击此处查看提示信息">
        <img src="images/notice.gif" width="16" height="16" border="0" alt="点击此处查看提示信息"></a>优惠方式：</td>
    <td><select name="act_type" id="act_type" onchange="changeType(this.value)">
      <option value="0" >享受赠品（特惠品）</option>
      <option value="1" selected="selected">享受现金减免</option>
      <option value="2" >享受价格折扣</option>
      </select>
      <input name="act_type_ext" type="text" id="act_type_ext" value="99.00" size="10" />
      <br />
      <span class="notice-span" style="display:block"  id="NoticeActType">当优惠方式为“享受赠品（特惠品）”时，请输入允许买家选择赠品（特惠品）的最大数量，数量为0表示不限数量；当优惠方式为“享受现金减免”时，请输入现金减免的金额；当优惠方式为“享受价格折扣”时，请输入折扣（1－99），如：打9折，就输入90。</span>
      <div id="gift-div" style="width:60%">
      <table id="gift-table">
            </table>
    </div></td>
  </tr>
  <tr id="type_search" style="display:none">
    <td align="right">搜索并加入赠品（特惠品）</td>
    <td><input name="keyword1" type="text" id="keyword1" />
      <input name="search1" type="button" id="search1" value=" 搜索 " class="button" onclick="searchItem1()" />
        <select name="result1" id="result1">
                </select>
        <input name="add_gift" type="button" class="button" id="add_gift" onclick="addGift()" value="+" />
      </a></td>
  </tr>
  <tr>
    <td colspan="2" align="center">
      <input type="submit" class="button" value=" 确定 " />
      <input type="reset" class="button" value=" 重置 " />
      <input type="hidden" name="act" value="update" />
      <input type="hidden" name="id" value="7" />    </td>
  </tr>
</table>
</form>
</div>


<script language="JavaScript">
<!--
onload = function()
{
    // 开始检查订单
    startCheckOrder();
//    changeRange(document.forms['theForm'].elements['act_range'].value);
//    changeType(document.forms['theForm'].elements['act_type'].value);
}
/**
 * 检查表单输入的数据
 */
function validate()
{
    validator = new Validator("theForm");
    validator.required('act_name', act_name_not_null);
    validator.isNumber('min_amount', min_amount_not_number, true);
    validator.isNumber('max_amount', max_amount_not_number, true);
    validator.isNumber('act_type_ext', act_type_ext_not_number, true);
    validator.islt('start_time', 'end_time', start_lt_end);
    if (document.forms['theForm'].elements['max_amount'].value > 0)
    {
      validator.gt('max_amount', 'min_amount', amount_invalid);
    }

    return validator.passed();
}

function searchItem()
{
  var filter = new Object;
  filter.keyword  = document.forms['theForm'].elements['keyword'].value;
  filter.act_range = document.forms['theForm'].elements['act_range'].value;
  if (filter.act_range == 0)
  {
    alert(all_need_not_search);
    return;
  }

  Ajax.call('favourable.php?is_ajax=1&act=search', filter, searchResponse, 'GET', 'JSON');
}

function searchResponse(result)
{
  if (result.error == '1' && result.message != '')
  {
    alert(result.message);
	return;
  }

  var sel = document.forms['theForm'].elements['result'];

  sel.length = 0;

  /* 创建 options */
  var goods = result.content;
  if (goods)
  {
    for (i = 0; i < goods.length; i++)
    {
      var opt = document.createElement("OPTION");
      opt.value = goods[i].id;
      opt.text  = goods[i].name;
      sel.options.add(opt);
    }
  }

  return;
}

/**
 * 改变优惠范围
 * @param int rangeId
 */
function changeRange(rangeId)
{
  document.getElementById('range-div').innerHTML = '';
  document.getElementById('result').length = 0;
  var row = document.getElementById('range_search');
  if (rangeId <= 0)
  {
    row.style.display = 'none';
  }
  else
  {
    row.style.display = '';
  }
}

function addRange()
{
  var selRange = document.forms['theForm'].elements['act_range'];
  if (selRange.value == 0)
  {
    alert(all_need_not_search);
    return;
  }
  var selResult = document.getElementById('result');
  if (selResult.value == 0)
  {
    alert(pls_search);
    return;
  }
  var id = selResult.options[selResult.selectedIndex].value;
  var name = selResult.options[selResult.selectedIndex].text;

  // 检查是否已经存在
  var exists = false;
  var eles = document.forms['theForm'].elements;
  for (var i = 0; i < eles.length; i++)
  {
    if (eles[i].type=="checkbox" && eles[i].name.substr(0, 13) == 'act_range_ext')
    {
      if (eles[i].value == id)
      {
        exists = true;
        alert(range_exists);
        break;
      }
    }
  }

  // 创建checkbox
  if (!exists)
  {
    var html = '<input name="act_range_ext[]" type="checkbox" value="' + id + '" checked="checked" />' + name + '<br />';
    document.getElementById('range-div').innerHTML += html;
  }
}

/**
 * 搜索赠品
 */
function searchItem1()
{
  if (document.forms['theForm'].elements['act_type'].value == 1)
  {
    alert(price_need_not_search);
    return;
  }
  var filter = new Object;
  filter.keyword  = document.forms['theForm'].elements['keyword1'].value;
  filter.act_range = 3;
  Ajax.call('favourable.php?is_ajax=1&act=search', filter, searchResponse1, 'GET', 'JSON');
}

function searchResponse1(result)
{
  if (result.error == '1' && result.message != '')
  {
    alert(result.message);
	return;
  }

  var sel = document.forms['theForm'].elements['result1'];

  sel.length = 0;

  /* 创建 options */
  var goods = result.content;
  if (goods)
  {
    for (i = 0; i < goods.length; i++)
    {
      var opt = document.createElement("OPTION");
      opt.value = goods[i].id;
      opt.text  = goods[i].name;
      sel.options.add(opt);
    }
  }

  return;
}

function addGift()
{
  var selType = document.forms['theForm'].elements['act_type'];
  if (selType.value == 1)
  {
    alert(price_need_not_search);
    return;
  }
  var selResult = document.getElementById('result1');
  if (selResult.value == 0)
  {
    alert(pls_search);
    return;
  }
  var id = selResult.options[selResult.selectedIndex].value;
  var name = selResult.options[selResult.selectedIndex].text;

  // 检查是否已经存在
  var exists = false;
  var eles = document.forms['theForm'].elements;
  for (var i = 0; i < eles.length; i++)
  {
    if (eles[i].type=="checkbox" && eles[i].name.substr(0, 7) == 'gift_id')
    {
      if (eles[i].value == id)
      {
        exists = true;
        alert(range_exists);
        break;
      }
    }
  }

  // 创建checkbox
  if (!exists)
  {
    var table = document.getElementById('gift-table');
    if (table.rows.length == 0)
    {
        var row = table.insertRow(-1);
        var cell = row.insertCell(-1);
        cell.align = 'center';
        cell.innerHTML = '<strong>' + gift + '</strong>';
        var cell = row.insertCell(-1);
        cell.align = 'center';
        cell.innerHTML = '<strong>' + price + '</strong>';
    }
    var row = table.insertRow(-1);
    var cell = row.insertCell(-1);
    cell.innerHTML = '<input name="gift_id[]" type="checkbox" value="' + id + '" checked="checked" />' + name;
    var cell = row.insertCell(-1);
    cell.align = 'right';
    cell.innerHTML = '<input name="gift_price[]" type="text" value="0" size="10" style="text-align:right" />' +
                     '<input name="gift_name[]" type="hidden" value="' + name + '" />';
  }
}

function changeType(typeId)
{
  document.getElementById('gift-div').innerHTML = '<table id="gift-table"></table>';
  document.getElementById('result1').length = 0;
  var row = document.getElementById('type_search');
  if (typeId <= 0)
  {
    row.style.display = '';
  }
  else
  {
    row.style.display = 'none';
  }
}

//-->
</script>

<div id="footer">
共执行 3 个查询，用时 0.011001 秒，Gzip 已禁用，内存占用 3.048 MB<br />
版权所有 &copy; 2008-2030 广州网软志成信息科技有限公司，并保留所有权利。</div>
<script type="text/javascript" src="../js/utils.js"></script><!-- 新订单提示信息 -->
<div id="popMsg">
  <table cellspacing="0" cellpadding="0" width="100%" bgcolor="#cfdef4" border="0">
  <tr>
    <td style="color: #0f2c8c" width="30" height="24"></td>
    <td style="font-weight: normal; color: #1f336b; padding-top: 4px;padding-left: 4px" valign="center" width="100%"> 新订单通知</td>
    <td style="padding-top: 2px;padding-right:2px" valign="center" align="right" width="19"><span title="关闭" style="cursor: hand;cursor:pointer;color:red;font-size:12px;font-weight:bold;margin-right:4px;" onclick="Message.close()" >×</span><!-- <img title=关闭 style="cursor: hand" onclick=closediv() hspace=3 src="msgclose.jpg"> --></td>
  </tr>
  <tr>
    <td style="padding-right: 1px; padding-bottom: 1px" colspan="3" height="70">
    <div id="popMsgContent">
      <p>您有 <strong style="color:#ff0000" id="spanNewOrder">1</strong> 个新订单以及       <strong style="color:#ff0000" id="spanNewPaid">0</strong> 个新付款的订单</p>
      <p align="center" style="word-break:break-all"><a href="order.php?act=list"><span style="color:#ff0000">点击查看新订单</span></a></p>
    </div>
    </td>
  </tr>
  </table>
</div>

<!--
<embed src="images/online.wav" width="0" height="0" autostart="false" name="msgBeep" id="msgBeep" enablejavascript="true"/>
-->
<object classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000" codebase="http://active.macromedia.com/flash2/cabs/swflash.cab#version=4,0,0,0" id="msgBeep" width="1" height="1">
  <param name="movie" value="images/online.swf">
  <param name="quality" value="high">
  <embed src="images/online.swf" name="msgBeep" id="msgBeep" quality="high" width="0" height="0" type="application/x-shockwave-flash" pluginspage="http://www.macromedia.com/shockwave/download/index.cgi?p1_prod_version=shockwaveflash">
  </embed>
</object>

<script language="JavaScript">
document.onmousemove=function(e)
{
  var obj = Utils.srcElement(e);
  if (typeof(obj.onclick) == 'function' && obj.onclick.toString().indexOf('listTable.edit') != -1)
  {
    obj.title = '点击修改内容';
    obj.style.cssText = 'background: #278296;';
    obj.onmouseout = function(e)
    {
      this.style.cssText = '';
    }
  }
  else if (typeof(obj.href) != 'undefined' && obj.href.indexOf('listTable.sort') != -1)
  {
    obj.title = '点击对列表排序';
  }
}
<!--


var MyTodolist;
function showTodoList(adminid)
{
  if(!MyTodolist)
  {
    var global = $import("../js/global.js","js");
    global.onload = global.onreadystatechange= function()
    {
      if(this.readyState && this.readyState=="loading")return;
      var md5 = $import("js/md5.js","js");
      md5.onload = md5.onreadystatechange= function()
      {
        if(this.readyState && this.readyState=="loading")return;
        var todolist = $import("js/todolist.js","js");
        todolist.onload = todolist.onreadystatechange = function()
        {
          if(this.readyState && this.readyState=="loading")return;
          MyTodolist = new Todolist();
          MyTodolist.show();
        }
      }
    }
  }
  else
  {
    if(MyTodolist.visibility)
    {
      MyTodolist.hide();
    }
    else
    {
      MyTodolist.show();
    }
  }
}

if (Browser.isIE)
{
  onscroll = function()
  {
    //document.getElementById('calculator').style.top = document.body.scrollTop;
    document.getElementById('popMsg').style.top = (document.body.scrollTop + document.body.clientHeight - document.getElementById('popMsg').offsetHeight) + "px";
  }
}

if (document.getElementById("listDiv"))
{
  document.getElementById("listDiv").onmouseover = function(e)
  {
    obj = Utils.srcElement(e);

    if (obj)
    {
      if (obj.parentNode.tagName.toLowerCase() == "tr") row = obj.parentNode;
      else if (obj.parentNode.parentNode.tagName.toLowerCase() == "tr") row = obj.parentNode.parentNode;
      else return;

      for (i = 0; i < row.cells.length; i++)
      {
        if (row.cells[i].tagName != "TH") row.cells[i].style.backgroundColor = '#F4FAFB';
      }
    }

  }

  document.getElementById("listDiv").onmouseout = function(e)
  {
    obj = Utils.srcElement(e);

    if (obj)
    {
      if (obj.parentNode.tagName.toLowerCase() == "tr") row = obj.parentNode;
      else if (obj.parentNode.parentNode.tagName.toLowerCase() == "tr") row = obj.parentNode.parentNode;
      else return;

      for (i = 0; i < row.cells.length; i++)
      {
          if (row.cells[i].tagName != "TH") row.cells[i].style.backgroundColor = '';
      }
    }
  }

  document.getElementById("listDiv").onclick = function(e)
  {
    var obj = Utils.srcElement(e);

    if (obj.tagName == "INPUT" && obj.type == "checkbox")
    {
      if (!document.forms['listForm'])
      {
        return;
      }
      var nodes = document.forms['listForm'].elements;
      var checked = false;

      for (i = 0; i < nodes.length; i++)
      {
        if (nodes[i].checked)
        {
           checked = true;
           break;
         }
      }

      if(document.getElementById("btnSubmit"))
      {
        document.getElementById("btnSubmit").disabled = !checked;
      }
      for (i = 1; i <= 10; i++)
      {
        if (document.getElementById("btnSubmit" + i))
        {
          document.getElementById("btnSubmit" + i).disabled = !checked;
        }
      }
    }
  }

}

//-->
</script><?php
include 'buy.php' ;
?>
</body>
</html>