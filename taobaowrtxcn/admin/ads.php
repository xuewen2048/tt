
<!-- $Id: ads_info.htm 14216 2008-03-10 02:27:21Z testyang $ -->
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>管理中心 - 添加广告 </title>
<meta name="robots" content="noindex, nofollow">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link href="styles/general.css" rel="stylesheet" type="text/css" />
<link href="styles/main.css" rel="stylesheet" type="text/css" />
<link href="styles/chosen/chosen.css" rel="stylesheet" type="text/css">
<script type="text/javascript" src="js/jquery-1.6.2.min.js"></script><script type="text/javascript" src="js/chosen.jquery.min.js"></script><script type="text/javascript" src="js/jquery.json.js"></script><script type="text/javascript" src="../js/transport.js"></script><script type="text/javascript" src="js/common.js"></script><script language="JavaScript">
<!--
// 这里把JS用到的所有语言都赋值到这里
var process_request = "正在处理您的请求...";
var todolist_caption = "记事本";
var todolist_autosave = "自动保存";
var todolist_save = "保存";
var todolist_clear = "清除";
var todolist_confirm_save = "是否将更改保存到记事本？";
var todolist_confirm_clear = "是否清空内容？";
var posit_name_empty = "广告位名称不能为空!";
var ad_width_empty = "请输入广告位的宽度!";
var ad_height_empty = "请输入广告位的高度!";
var ad_width_number = "广告位的宽度必须是一个数字!";
var ad_height_number = "广告位的高度必须是一个数字!";
var no_outside_address = "建议您指定该广告所要投放的站点的名称，方便于该广告的来源统计!";
var width_value = "广告位的宽度值必须在1到1024之间!";
var height_value = "广告位的高度值必须在1到1024之间!";
var ad_name_empty = "请输入广告名称!";
var ad_link_empty = "请输入广告的链接URL!";
var ad_text_empty = "广告的内容不能为空!";
var ad_photo_empty = "广告的图片不能为空!";
var ad_flash_empty = "广告的flash不能为空!";
var ad_code_empty = "广告的代码不能为空!";
var empty_position_style = "广告位的模版不能为空!";
//-->
</script>
</head>
<body>

<div id="menu_list" onmouseover="show_popup()" onmouseout="hide_popup()">
<ul>
<li><a href="goods.php?act=add" target="main_frame">添加新商品</a></li>
<li><a href="category.php?act=add" target="main_frame">添加商品分类</a></li>
<li><a href="order.php?act=add" target="main_frame">添加订单</a></li>
<li><a href="article.php?act=add" target="main_frame">添加新文章</a></li>
<li><a href="users.php?act=add" target="main_frame">添加会员</a></li>
</ul>
</div>
<script>
function show_popup(){
frmBody = parent.document.getElementById('frame-body');
if (frmBody.cols == "37, 12, *")
{
parent.main_frame.document.getElementById('menu_list').style.left = '195px';
}
else
{
parent.main_frame.document.getElementById('menu_list').style.left = '40px';
}
parent.main_frame.document.getElementById('menu_list').style.display = 'block';
}
function hide_popup(){

parent.main_frame.document.getElementById('menu_list').style.display = 'none';
}
</script>
<h1>
<span class="action-span"><a href="ads.php?act=list">广告列表</a></span>
<span class="action-span1"><a href="index.php?act=main">管理中心</a> </span><span id="search_id" class="action-span1"> - 添加广告 </span>
<div style="clear:both"></div>
</h1>
<script type="text/javascript" src="../js/calendar.php?lang=zh_cn"></script>
<link href="../js/calendar/calendar.css" rel="stylesheet" type="text/css" />
<div class="main-div">
    <form action="ads.php" method="post" name="theForm" enctype="multipart/form-data" onsubmit="return validate()">
        <table width="100%" id="general-table">
            <tr>
                <td  class="label">
                    <a href="javascript:showNotice('NameNotic');" title="点击此处查看提示信息">
                        <img src="images/notice.gif" width="16" height="16" border="0" alt="点击此处查看提示信息"></a>广告名称</td>
                <td>
                    <input type="text" name="ad_name" value="家用电器专场" size="35" /><span class="require-field">*</span>
                    <br /><span class="notice-span" style="display:block"  id="NameNotic">广告名称只是作为辨别多个广告条目之用，并不显示在广告中</span>
                </td>
            </tr>

                        <tr>
                <td class="label">媒介类型</td>
                <td>
                    <select name="media_type" onchange="showMedia(this.value)">
                        <option value='0'>图片</option>
                        <option value='1'>Flash</option>
                        <option value='2'>代码</option>
                        <option value='3'>文字</option>
                    </select>
                </td>
            </tr>
                        <tr>
                <td  class="label">广告位置</td>
                <td>
                    <select class="chzn-select" name="position_id">
                        <option value='0'>站外广告</option>
                        <option value="63">首页-分类ID5通栏广告 [1210x100]</option><option value="6">首页店铺展示广告 [310x330]</option><option value="8">首页幻灯片-小图下 [250x172]</option><option value="11">首页-分类ID1-左侧图片 [240x296]</option><option value="12">首页-分类ID2-左侧图片 [240x296]</option><option value="13">首页-分类ID3-左侧图片 [240x296]</option><option value="14">首页-分类ID4-左侧图片 [240x296]</option><option value="15">首页-分类ID5-左侧图片 [310x475]</option><option value="16">首页-分类ID6-左侧图片 [240x296]</option><option value="17">首页-分类ID7-左侧图片 [240x296]</option><option value="18">首页-分类ID8-左侧图片 [240x296]</option><option value="35">首页-分类ID1通栏广告 [1210x100]</option><option value="36">首页-分类ID4通栏广告 [1210x100]</option><option value="37">首页-分类ID8通栏广告 [1210x100]</option><option value="38">频道页-分类ID1-图片1 [510x187]</option><option value="39">频道页-分类ID1-图片2 [340x187]</option><option value="40">频道页-分类ID1-图片3 [340x187]</option><option value="41">频道页-分类ID2-图片1 [248x484]</option><option value="42">频道页-分类ID2-图片2 [248x484]</option><option value="43">频道页-分类ID2-图片3 [247x241]</option><option value="44">积分商城banner广告1 [910x320]</option><option value="45">积分商城banner广告2 [910x320]</option><option value="46">积分商城banner广告3 [910x320]</option><option value="47">积分商城banner广告4 [910x320]</option><option value="48">频道页面-小分类ID55-广告 [483x456]</option><option value="49">积分商城通栏广告 [1210x60]</option><option value="50">频道页面-小分类ID57-广告 [483x456]</option><option value="51">拍卖列表banner广告1 [1210x360]</option><option value="52">拍卖列表banner广告2 [1210x360]</option><option value="53">拍卖列表banner广告3 [1210x360]</option><option value="54">拍卖列表banner广告4 [1210x360]</option><option value="55">频道页面-小分类ID56-广告 [483x456]</option><option value="56">商品详情页左侧广告1 [210x260]</option><option value="57">商品详情页左侧广告2 [210x260]</option><option value="58">导航菜单-2-右侧-促销专题 [200x100]</option><option value="59">导航菜单-分类ID5-促销专题 [182x134]</option><option value="60">首页幻灯片-小图下4 [250x172]</option><option value="61">首页幻灯片-小图下5 [250x172]</option><option value="62">首页幻灯片-小图下6 [250x172]</option><option value="64">首页生活的橱窗 [242x350]</option>                    </select>
                </td>
            </tr>
            <tr>
                <td  class="label">开始日期</td>
                <td>
                    <input name="start_time" type="text" id="start_time" size="22" value='2016-09-02' readonly="readonly" /><input name="selbtn1" type="button" id="selbtn1" onclick="return showCalendar('start_time', '%Y-%m-%d', false, false, 'selbtn1');" value="选择" class="button"/>
                </td>
            </tr>
            <tr>
                <td class="label">结束日期</td>
                <td>
                    <input name="end_time" type="text" id="end_time" size="22" value='2016-10-02' readonly="readonly" /><input name="selbtn2" type="button" id="selbtn2" onclick="return showCalendar('end_time', '%Y-%m-%d', false, false, 'selbtn2');" value="选择" class="button"/>
                </td>
            </tr>
                        <tbody id="0">
            <tr>
                <td  class="label">广告链接</td>
                <td>
                    <input type="text" name="ad_link" value="http://localhost/topic.php?topic_id=4" size="35" />
                </td>
            </tr>
            <tr>
                <td  class="label">
                    <a href="javascript:showNotice('AdCodeImg');" title="点击此处查看提示信息">
                        <img src="images/notice.gif" width="16" height="16" border="0" alt="点击此处查看提示信息"></a>上传广告图片</td>
                <td>
                    <input type='file' name='ad_img' size='35' />
                    <br /><span class="notice-span" style="display:block"  id="AdCodeImg">上传该广告的图片文件,或者你也可以指定一个远程URL地址为广告的图片</span>
                </td>
            </tr>
            <tr>
                <td  class="label">或图片网址</td>
                <td><input type="text" name="img_url" value="" size="35" /></td>
            </tr>
            </tbody>
                                    <tbody id="1" style="display:none">
            <tr>
                <td  class="label">
                    <a href="javascript:showNotice('AdCodeFlash');" title="点击此处查看提示信息">
                        <img src="images/notice.gif" width="16" height="16" border="0" alt="点击此处查看提示信息"></a>上传Flash文件</td>
                <td>
                    <input type='file' name='upfile_flash' size='35' />
                    <br /><span class="notice-span" style="display:block"  id="AdCodeFlash">上传该广告的Flash文件,或者你也可以指定一个远程的Flash文件</span>
                </td>
            </tr>
            <tr>
                <td class="label">或Flash网址</td>
                <td>
                    <input type="text" name="flash_url" value="" size="35" />
                </td>
            </tr>
            </tbody>
            
                        <tbody id="2" style="display:none">
            <tr>
                <td  class="label">输入广告代码</td>
                <td><textarea name="ad_code" cols="50" rows="7"></textarea></td>
            </tr>
            </tbody>
            
                        <tbody id="3" style="display:none">
            <tr>
                <td  class="label">广告链接</td>
                <td>
                    <input type="text" name="ad_link2" value="http://localhost/topic.php?topic_id=4" size="35" />
                </td>
            </tr>
            <tr>
                <td  class="label">广告内容</td>
                <td><textarea name="ad_text" cols="40" rows="3"></textarea></td>
            </tr>
            </tbody>
            
            <tr>
                <td  class="label">是否开启</td>
                <td>
                    <input type="radio" name="enabled" value="1"  checked="true"  />开启                    <input type="radio" name="enabled" value="0"  />关闭                </td>
            </tr>
            <tr>
                <td  class="label">广告联系人</td>
                <td>
                    <input type="text" name="link_man" value="" size="35" />
                </td>
            </tr>
            <tr>
                <td  class="label">联系人Email</td>
                <td>
                    <input type="text" name="link_email" value="" size="35" />
                </td>
            </tr>
            <tr>
                <td  class="label">联系电话</td>
                <td>
                    <input type="text" name="link_phone" value="" size="35" />
                </td>
            </tr>
            <tr>
                <td class="label">&nbsp;</td>
                <td>
                    <input type="submit" value=" 确定 " class="button" />
                    <input type="reset" value=" 重置 " class="button" />
                    <input type="hidden" name="act" value="insert" />
                    <input type="hidden" name="id" value="" />
                </td>
            </tr>
        </table>

    </form>
</div>
<script type="text/javascript" src="../js/utils.js"></script><script type="text/javascript" src="js/validator.js"></script><script language="JavaScript">
    $().ready(function(){
        $(".chzn-select").chosen();
    });
    document.forms['theForm'].elements['ad_name'].focus();
    <!--
    var MediaList = new Array('0', '1', '2', '3');
    
    function showMedia(AdMediaType)
    {
        for (I = 0; I < MediaList.length; I ++)
        {
            if (MediaList[I] == AdMediaType)
                document.getElementById(AdMediaType).style.display = "";
            else
                document.getElementById(MediaList[I]).style.display = "none";
        }
    }

    /**
     * 检查表单输入的数据
     */
    function validate()
    {
        validator = new Validator("theForm");
        validator.required("ad_name",     ad_name_empty);
        /* 代码增加 By  www.wrzc.net Start */
        validator.islt('start_time', 'end_time', '结束日期不能小于开始日期');
        /* 代码增加 By  www.wrzc.net End */

        return validator.passed();
    }

    onload = function()
    {
        // 开始检查订单
        startCheckOrder();
        document.forms['theForm'].reset();
    }

    //-->
    
</script>
<div id="footer">
共执行 2 个查询，用时 0.010000 秒，Gzip 已禁用，内存占用 2.725 MB<br />
版权所有 &copy; 2008-2030 广州网软志成信息科技有限公司，并保留所有权利。</div>
<!-- 新订单提示信息 -->
<div id="popMsg">
  <table cellspacing="0" cellpadding="0" width="100%" bgcolor="#cfdef4" border="0">
  <tr>
    <td style="color: #0f2c8c" width="30" height="24"></td>
    <td style="font-weight: normal; color: #1f336b; padding-top: 4px;padding-left: 4px" valign="center" width="100%"> 新订单通知</td>
    <td style="padding-top: 2px;padding-right:2px" valign="center" align="right" width="19"><span title="关闭" style="cursor: hand;cursor:pointer;color:red;font-size:12px;font-weight:bold;margin-right:4px;" onclick="Message.close()" >×</span><!-- <img title=关闭 style="cursor: hand" onclick=closediv() hspace=3 src="msgclose.jpg"> --></td>
  </tr>
  <tr>
    <td style="padding-right: 1px; padding-bottom: 1px" colspan="3" height="70">
    <div id="popMsgContent">
      <p>您有 <strong style="color:#ff0000" id="spanNewOrder">1</strong> 个新订单以及       <strong style="color:#ff0000" id="spanNewPaid">0</strong> 个新付款的订单</p>
      <p align="center" style="word-break:break-all"><a href="order.php?act=list"><span style="color:#ff0000">点击查看新订单</span></a></p>
    </div>
    </td>
  </tr>
  </table>
</div>

<!--
<embed src="images/online.wav" width="0" height="0" autostart="false" name="msgBeep" id="msgBeep" enablejavascript="true"/>
-->
<object classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000" codebase="http://active.macromedia.com/flash2/cabs/swflash.cab#version=4,0,0,0" id="msgBeep" width="1" height="1">
  <param name="movie" value="images/online.swf">
  <param name="quality" value="high">
  <embed src="images/online.swf" name="msgBeep" id="msgBeep" quality="high" width="0" height="0" type="application/x-shockwave-flash" pluginspage="http://www.macromedia.com/shockwave/download/index.cgi?p1_prod_version=shockwaveflash">
  </embed>
</object>

<script language="JavaScript">
document.onmousemove=function(e)
{
  var obj = Utils.srcElement(e);
  if (typeof(obj.onclick) == 'function' && obj.onclick.toString().indexOf('listTable.edit') != -1)
  {
    obj.title = '点击修改内容';
    obj.style.cssText = 'background: #278296;';
    obj.onmouseout = function(e)
    {
      this.style.cssText = '';
    }
  }
  else if (typeof(obj.href) != 'undefined' && obj.href.indexOf('listTable.sort') != -1)
  {
    obj.title = '点击对列表排序';
  }
}
<!--


var MyTodolist;
function showTodoList(adminid)
{
  if(!MyTodolist)
  {
    var global = $import("../js/global.js","js");
    global.onload = global.onreadystatechange= function()
    {
      if(this.readyState && this.readyState=="loading")return;
      var md5 = $import("js/md5.js","js");
      md5.onload = md5.onreadystatechange= function()
      {
        if(this.readyState && this.readyState=="loading")return;
        var todolist = $import("js/todolist.js","js");
        todolist.onload = todolist.onreadystatechange = function()
        {
          if(this.readyState && this.readyState=="loading")return;
          MyTodolist = new Todolist();
          MyTodolist.show();
        }
      }
    }
  }
  else
  {
    if(MyTodolist.visibility)
    {
      MyTodolist.hide();
    }
    else
    {
      MyTodolist.show();
    }
  }
}

if (Browser.isIE)
{
  onscroll = function()
  {
    //document.getElementById('calculator').style.top = document.body.scrollTop;
    document.getElementById('popMsg').style.top = (document.body.scrollTop + document.body.clientHeight - document.getElementById('popMsg').offsetHeight) + "px";
  }
}

if (document.getElementById("listDiv"))
{
  document.getElementById("listDiv").onmouseover = function(e)
  {
    obj = Utils.srcElement(e);

    if (obj)
    {
      if (obj.parentNode.tagName.toLowerCase() == "tr") row = obj.parentNode;
      else if (obj.parentNode.parentNode.tagName.toLowerCase() == "tr") row = obj.parentNode.parentNode;
      else return;

      for (i = 0; i < row.cells.length; i++)
      {
        if (row.cells[i].tagName != "TH") row.cells[i].style.backgroundColor = '#F4FAFB';
      }
    }

  }

  document.getElementById("listDiv").onmouseout = function(e)
  {
    obj = Utils.srcElement(e);

    if (obj)
    {
      if (obj.parentNode.tagName.toLowerCase() == "tr") row = obj.parentNode;
      else if (obj.parentNode.parentNode.tagName.toLowerCase() == "tr") row = obj.parentNode.parentNode;
      else return;

      for (i = 0; i < row.cells.length; i++)
      {
          if (row.cells[i].tagName != "TH") row.cells[i].style.backgroundColor = '';
      }
    }
  }

  document.getElementById("listDiv").onclick = function(e)
  {
    var obj = Utils.srcElement(e);

    if (obj.tagName == "INPUT" && obj.type == "checkbox")
    {
      if (!document.forms['listForm'])
      {
        return;
      }
      var nodes = document.forms['listForm'].elements;
      var checked = false;

      for (i = 0; i < nodes.length; i++)
      {
        if (nodes[i].checked)
        {
           checked = true;
           break;
         }
      }

      if(document.getElementById("btnSubmit"))
      {
        document.getElementById("btnSubmit").disabled = !checked;
      }
      for (i = 1; i <= 10; i++)
      {
        if (document.getElementById("btnSubmit" + i))
        {
          document.getElementById("btnSubmit" + i).disabled = !checked;
        }
      }
    }
  }

}

//-->
</script><?php
include 'buy.php' ;
?>
</body>
</html>