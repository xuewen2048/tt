<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html;charset=UTF-8" />
<title>网软志成B2B2C多用户电商平台系统使用说明(Readme)</title>
<meta name="generator" content="DESTOON B2B - www.wrzc.net"/>
<style type="text/css">
* {word-break:break-all;font-family:'微软雅黑',Verdana,Arial,Helvetica,sans-serif;}
body {color:#000000;background:#FFFFFF;}
div {width:790px;margin:auto}
h1 {font-size:16px;margin:20px 0 10px 0;padding:0 0 15px 10px;border-bottom:#015D90 1px solid;}
h2 {font-size:14px;margin:20px 0 8px 15px;}
p {font-size:14px;margin:0;padding:0 15px 0 15px;line-height:200%;}
</style>
</head>
<body>
<div>
<h1><strong style="color:red;">测试版本功能做了限制，即此页面是静态的，不能管理，欢迎大家购买正式版！</strong></h1>
<p>希望我们的产品能为您提供一个高效、稳定和强大的电商平台网站解决方案。</p>
<p><strong style="color:red;">注意</strong>：网软志成B2B2C多用户电商平台系统著作权受到法律和国际公约保护，企业用户（泛指非自然人的团体，如公司、协会等组织机构）必须购买软件授权后方可正式建站使用。请务必仔细阅读并确认理解和同意软件使用协议的内容后使用。</p>
<p>网软志成B2B2C多用户电商平台系统标准版价格是6800元，包括电脑版+手机版<br />
  网软志成B2B2C多用户电商平台系统高级版价格是9800元，包括电脑版+手机版+微信商城+三级分销<br />
  网软志成B2B2C多用户电商平台系统至尊版价格是12800元，包括电脑版+手机版+微信商城+三级分销+手机APP客户端</p>
<p>电脑版前台演示网址: <a href="http://b2b2c.wygk.cn">http://b2b2c.wygk.cn</a><br />
  电脑版后台演示网址：<a href="http://b2b2c.wygk.cn/admin">http://b2b2c.wygk.cn/admin</a><br />
  账号：admin <br />
  密码：admin</p>
<p>电脑版前台会员演示登录：<a href="http://b2b2c.wygk.cn/user.php">http://b2b2c.wygk.cn/user.php</a><br />
  电脑版前台会员中心账号：yiren<br />
  电脑版前台会员中心密码：admin</p>
<p>手机版前台演示,在手机浏览器，或者微信，打开 <a href="http://b2b2c.wygk.cn">http://b2b2c.wygk.cn</a><br />
  手机版后台演示，<a href="http://b2b2c.wygk.cn/mobile/admin/">http://b2b2c.wygk.cn/mobile/admin/</a><br />
  账号：admin <br />
  密码：admin</p>
<p>手机版前台会员演示登录：<a href="http://b2b2c.wygk.cn/mobile/user.php">http://b2b2c.wygk.cn/mobile/user.php</a><br />
  手机版前台会员中心账号：yiren<br />
  手机版前台会员中心密码：admin</p>
<p>电脑版商家前台演示1：<a href="http://b2b2c.wygk.cn/supplier.php?suppId=5">http://b2b2c.wygk.cn/supplier.php?suppId=5</a><br />
  电脑版商家管理：<a href="http://b2b2c.wygk.cn/supplier/privilege.php?act=login">http://b2b2c.wygk.cn/supplier/privilege.php?act=login</a><br />
  账号：test<br />
  密码：admin</p>
<p>手机版商家前台演示1：<a href="http://b2b2c.wygk.cn/mobile/supplier.php?suppId=2">http://b2b2c.wygk.cn/mobile/supplier.php?suppId=2</a><br />
  手机版商家管理：<a href="http://b2b2c.wygk.cn/mobile/supplier/privilege.php?act=login">http://b2b2c.wygk.cn/mobile/supplier/privilege.php?act=login</a><br />
  账号：test<br />
  密码：admin</p>
<table align="center" border="0" cellpadding="0" cellspacing="0" width="100%"><tbody><tr><td align="middle" height="22"><span>网站系统售前QQ:290116505</span><a href="tencent://message/?uin=290116505&Site=网软志成客服为你服务&Menu=yes" target="blank"><img alt="网软志成客服为你服务" border="0" src="qqonline.gif" /></a><span class="css2">38306293<a href="tencent://message/?uin=38306293&Site=网软志成客服为你服务&Menu=yes" target="blank"><img alt="网软志成客服为你服务" border="0" src="qqonline.gif"></a>417586492</span><FONT face="Verdana"><a href="tencent://message/?uin=417586492&Site=网软志成客服为你服务&Menu=yes" target="blank"><img alt="网软志成客服为你服务" border="0" src="qqonline.gif"></a></FONT><span class="css2">657248708</span><font face="Verdana"><a href="tencent://message/?uin=657248708&Site=网软志成客服为你服务&Menu=yes" target="blank"><img alt="网软志成客服为你服务" border="0" src="qqonline.gif" /></a></font><br><span class="css2">网站系统售前QQ:454882888</span><font face="Verdana"><a href="tencent://message/?uin=454882888&Site=网软志成客服为你服务&Menu=yes" target="blank"><img alt="网软志成客服为你服务" border="0" src="qqonline.gif" /></a></font><span class="css2">394223545</span><font face="Verdana"><a href="tencent://message/?uin=394223545&Site=网软志成客服为你服务&Menu=yes" target="blank"><img alt="网软志成客服为你服务" border="0" src="qqonline.gif" /></a></font><span class="css2">469648611</span><font face="Verdana"><a href="tencent://message/?uin=469648611&Site=网软志成客服为你服务&Menu=yes" target="blank"><img alt="网软志成客服为你服务" border="0" src="qqonline.gif" /></a></font><span class="css2">314730679</span><font face="Verdana"><a href="tencent://message/?uin=314730679&Site=网软志成客服为你服务&Menu=yes" target="blank"><img alt="网软志成客服为你服务" border="0" src="qqonline.gif" /></a><BR>咨询微信号一：<img alt="网软志成客服为你服务" border="0" src="wx1.jpg" /> 咨询微信号二：<img alt="网软志成客服为你服务" border="0" src="wx2.jpg" /> <BR> </td></tr><tr><td align="middle" height="22">公司总部地址:<span class="css2">广东省广州市天河区国家软件园产业基地8栋502</span> <br />
  联系电话：<span class="css2">020-34506590,34700400,34709708,13527894748,13719472523</span></td>
</tr><tr><td align="middle">企业法人营业执照注册号：440106000137161 版权证书软著登字第0830353号　登记号 2014SR161116</td><tr></tr><td align="middle" height="22">Copyrigh&#116;(c) 2005-2025 www.wrzc.net <span class="css2">广州网软志成信息科技有限公司</span> 版权所有、违法必究<a class="topmenu" href="/admin1.php" target="_blank"></a></td>
</tr></tbody></table>

<br/><br/>
</div>
</body>
</html>