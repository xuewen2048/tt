
<!--增值税发票_添加_START_www.wrzc.net-->
<!--增值税发票_添加_END_www.wrzc.net-->
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>管理中心 - 订单列表 </title>
<meta name="robots" content="noindex, nofollow">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link href="styles/general.css" rel="stylesheet" type="text/css" />
<link href="styles/main.css" rel="stylesheet" type="text/css" />
<link href="styles/chosen/chosen.css" rel="stylesheet" type="text/css">
<script type="text/javascript" src="js/jquery-1.6.2.min.js"></script><script type="text/javascript" src="js/chosen.jquery.min.js"></script><script type="text/javascript" src="js/jquery.json.js"></script><script type="text/javascript" src="../js/transport.js"></script><script type="text/javascript" src="js/common.js"></script><script language="JavaScript">
<!--
// 这里把JS用到的所有语言都赋值到这里
var process_request = "正在处理您的请求...";
var todolist_caption = "记事本";
var todolist_autosave = "自动保存";
var todolist_save = "保存";
var todolist_clear = "清除";
var todolist_confirm_save = "是否将更改保存到记事本？";
var todolist_confirm_clear = "是否清空内容？";
var remove_confirm = "删除订单将清除该订单的所有信息。您确定要这么做吗？";
var confirm_merge = "您确实要合并这两个订单吗？";
var input_price = "自定义价格";
var pls_search_user = "请搜索并选择会员";
var confirm_drop = "确认要删除该商品吗？";
var invalid_goods_number = "商品数量不正确";
var pls_search_goods = "请搜索并选择商品";
var pls_select_area = "请完整选择所在地区";
var pls_select_shipping = "请选择配送方式";
var pls_select_payment = "请选择支付方式";
var pls_select_pack = "请选择包装";
var pls_select_card = "请选择贺卡";
var pls_input_note = "请您填写备注！";
var pls_input_cancel = "请您填写取消原因！";
var pls_select_refund = "请选择退款方式！";
var pls_select_agency = "请选择办事处！";
var pls_select_other_agency = "该订单现在就属于这个办事处，请选择其他办事处！";
var loading = "加载中...";
var remove_invoice_confirm = "您确定删除该订单的发票信息吗？";
//-->
</script>
</head>
<body>

<div id="menu_list" onmouseover="show_popup()" onmouseout="hide_popup()">
<ul>
<li><a href="goods.php?act=add" target="main_frame">添加新商品</a></li>
<li><a href="category.php?act=add" target="main_frame">添加商品分类</a></li>
<li><a href="order.php?act=add" target="main_frame">添加订单</a></li>
<li><a href="article.php?act=add" target="main_frame">添加新文章</a></li>
<li><a href="users.php?act=add" target="main_frame">添加会员</a></li>
</ul>
</div>
<script>
function show_popup(){
frmBody = parent.document.getElementById('frame-body');
if (frmBody.cols == "37, 12, *")
{
parent.main_frame.document.getElementById('menu_list').style.left = '195px';
}
else
{
parent.main_frame.document.getElementById('menu_list').style.left = '40px';
}
parent.main_frame.document.getElementById('menu_list').style.display = 'block';
}
function hide_popup(){

parent.main_frame.document.getElementById('menu_list').style.display = 'none';
}
</script>
<h1>
<span class="action-span"><a href="order.php?act=order_query">订单查询</a></span>
<span class="action-span1"><a href="index.php?act=main">管理中心</a> </span><span id="search_id" class="action-span1"> - 订单列表 </span>
<div style="clear:both"></div>
</h1>
<script type="text/javascript" src="../js/utils.js"></script><script type="text/javascript" src="js/listtable.js"></script><!-- 订单搜索 -->
<div class="form-div">
  <form action="javascript:searchOrder()" name="searchForm">
    <img src="images/icon_search.gif" width="26" height="22" border="0" alt="SEARCH" />
    订单号<input name="order_sn" type="text" id="order_sn" size="15">
    收货人<input name="consignee" type="text" id="consignee" size="15">
    订单状态    <select name="status" id="status">
      <option value="-1">请选择...</option>
      <option value="0" selected>待确认</option><option value="100">待付款</option><option value="101">待发货</option><option value="102">已完成</option><option value="1">付款中</option><option value="2">取消</option><option value="3">无效</option><option value="4">退货</option><option value="6">部分发货</option>    </select>
	    	<!-- 代码增加_start   By www.wygk.cn -->
    订单类型    <select name="order_type">
    	<option value="0">请选择...</option>
        <option value="1">一般订单</option>
        <option value="2">自提订单</option>
    </select>
	<!-- 代码增加_end   By www.wygk.cn -->
    <input type="submit" value=" 搜索 " class="button" />
    <a href="order.php?act=list&supp=0&composite_status=0">待确认</a>
    <a href="order.php?act=list&supp=0&composite_status=100">待付款</a>
    <a href="order.php?act=list&supp=0&composite_status=101">待发货</a>
  </form>
</div>

<!-- 订单列表 -->
<form method="post" action="order.php?act=operate" name="listForm" onsubmit="return check()">
  <div class="list-div" id="listDiv">

<table cellpadding="3" cellspacing="1">
  <tr>
    <th>
      <input onclick='listTable.selectAll(this, "checkboxes")' type="checkbox" /><a href="javascript:listTable.sort('order_sn', 'DESC'); ">订单号</a>    </th>
	    <!-- 代码增加_start   By www.wygk.cn -->
    <th>订单类型</th>
	<!-- 代码增加_end   By www.wygk.cn -->
    <th><a href="javascript:listTable.sort('add_time', 'DESC'); ">下单时间</a><img src="images/sort_desc.gif"></th>
    <th><a href="javascript:listTable.sort('consignee', 'DESC'); ">收货人</a></th>
    <th><a href="javascript:listTable.sort('total_fee', 'DESC'); ">总金额</a></th>
    <th><a href="javascript:listTable.sort('order_amount', 'DESC'); ">应付金额</a></th>
	<th>订单来源</th>
    <th>订单状态</th>
    <th>操作</th>
  <tr>
    <tr class="
  	 
     
        tr_nosend    ">
    <td valign="top" nowrap="nowrap"><input type="checkbox" name="checkboxes" value="2016031622459" /><a href="order.php?act=info&order_id=163" id="order_0">2016031622459</a></td>
	    <!-- 代码增加_start   By www.wygk.cn -->
	<td>自提订单</td>
	<!-- 代码增加_end   By www.wygk.cn -->
    <td>hhh<br />03-16 16:57</td>
    <td align="left" valign="top"><a href="mailto:"> test</a> <br />test</td>
    <td align="right" valign="top" nowrap="nowrap">¥114.00</td>
    <td align="right" valign="top" nowrap="nowrap">¥0.00</td>
	<td align="right" valign="top" nowrap="nowrap">app</td>
    <td align="center" valign="top" nowrap="nowrap">已确认,已付款,未发货</td>
    <td align="center" valign="top"  nowrap="nowrap">
     <a href="order.php?act=info&order_id=163">查看</a>
              </td>
  </tr>
    <tr class="
  	tr_canceled 
    tr_nopay 
            ">
    <td valign="top" nowrap="nowrap"><input type="checkbox" name="checkboxes" value="2016031638792" /><a href="order.php?act=info&order_id=162" id="order_1">2016031638792</a></td>
	    <!-- 代码增加_start   By www.wygk.cn -->
	<td>自提订单</td>
	<!-- 代码增加_end   By www.wygk.cn -->
    <td>hhh<br />03-16 16:52</td>
    <td align="left" valign="top"><a href="mailto:"> test</a> <br />test</td>
    <td align="right" valign="top" nowrap="nowrap">¥99.00</td>
    <td align="right" valign="top" nowrap="nowrap">¥99.00</td>
	<td align="right" valign="top" nowrap="nowrap">app</td>
    <td align="center" valign="top" nowrap="nowrap"><font color="red">无效</font>,未付款,已发货</td>
    <td align="center" valign="top"  nowrap="nowrap">
     <a href="order.php?act=info&order_id=162">查看</a>
          <br /><a href="javascript:;" onclick="listTable.remove(162, remove_confirm, 'remove_order')">移除</a>
              </td>
  </tr>
    <tr class="
  	tr_canceled 
    tr_nopay 
            ">
    <td valign="top" nowrap="nowrap"><input type="checkbox" name="checkboxes" value="2016030936707" /><a href="order.php?act=info&order_id=154" id="order_2">2016030936707</a></td>
	    <!-- 代码增加_start   By www.wygk.cn -->
	<td>自提订单</td>
	<!-- 代码增加_end   By www.wygk.cn -->
    <td>yiren<br />03-09 08:50</td>
    <td align="left" valign="top"><a href="mailto:"> sdf</a> [TEL: -] <br />sdf </td>
    <td align="right" valign="top" nowrap="nowrap">¥84.13</td>
    <td align="right" valign="top" nowrap="nowrap">¥84.13</td>
	<td align="right" valign="top" nowrap="nowrap">pc</td>
    <td align="center" valign="top" nowrap="nowrap"><font color="red">无效</font>,未付款,未发货</td>
    <td align="center" valign="top"  nowrap="nowrap">
     <a href="order.php?act=info&order_id=154">查看</a>
          <br /><a href="javascript:;" onclick="listTable.remove(154, remove_confirm, 'remove_order')">移除</a>
              </td>
  </tr>
    <tr class="
  	tr_canceled 
    tr_nopay 
            ">
    <td valign="top" nowrap="nowrap"><input type="checkbox" name="checkboxes" value="2016030842353" /><a href="order.php?act=info&order_id=153" id="order_3">2016030842353</a></td>
	    <!-- 代码增加_start   By www.wygk.cn -->
	<td>自提订单</td>
	<!-- 代码增加_end   By www.wygk.cn -->
    <td>yiren<br />03-08 15:00</td>
    <td align="left" valign="top"><a href="mailto:"> sdf</a> [TEL: -] <br />sdf </td>
    <td align="right" valign="top" nowrap="nowrap">¥83.30</td>
    <td align="right" valign="top" nowrap="nowrap">¥83.30</td>
	<td align="right" valign="top" nowrap="nowrap">pc</td>
    <td align="center" valign="top" nowrap="nowrap"><font color="red">无效</font>,未付款,未发货</td>
    <td align="center" valign="top"  nowrap="nowrap">
     <a href="order.php?act=info&order_id=153">查看</a>
          <br /><a href="javascript:;" onclick="listTable.remove(153, remove_confirm, 'remove_order')">移除</a>
              </td>
  </tr>
    <tr class="
  	tr_canceled 
     
            tr_back">
    <td valign="top" nowrap="nowrap"><input type="checkbox" name="checkboxes" value="2016022590434" /><a href="order.php?act=info&order_id=151" id="order_4">2016022590434</a></td>
	    <!-- 代码增加_start   By www.wygk.cn -->
	<td>一般订单</td>
	<!-- 代码增加_end   By www.wygk.cn -->
    <td>yiren<br />02-25 20:43</td>
    <td align="left" valign="top"><a href="mailto:"> 123</a> [TEL: -] <br />圣达菲</td>
    <td align="right" valign="top" nowrap="nowrap">¥99.13</td>
    <td align="right" valign="top" nowrap="nowrap">¥0.00</td>
	<td align="right" valign="top" nowrap="nowrap">pc</td>
    <td align="center" valign="top" nowrap="nowrap"><font color="red"> 取消</font>,已付款,未发货</td>
    <td align="center" valign="top"  nowrap="nowrap">
     <a href="order.php?act=info&order_id=151">查看</a>
          <br /><a href="javascript:;" onclick="listTable.remove(151, remove_confirm, 'remove_order')">移除</a>
               <br /><span style="color:#F00">退款,系统自动取消</span>
         </td>
  </tr>
    <tr class="
  	 
     
        tr_nosend    ">
    <td valign="top" nowrap="nowrap"><input type="checkbox" name="checkboxes" value="2016022516450" /><a href="order.php?act=info&order_id=150" id="order_5">2016022516450</a></td>
	    <!-- 代码增加_start   By www.wygk.cn -->
	<td>一般订单</td>
	<!-- 代码增加_end   By www.wygk.cn -->
    <td>yiren<br />02-25 20:27</td>
    <td align="left" valign="top"><a href="mailto:"> 123</a> [TEL: -] <br />圣达菲</td>
    <td align="right" valign="top" nowrap="nowrap">¥3888.75</td>
    <td align="right" valign="top" nowrap="nowrap">¥0.00</td>
	<td align="right" valign="top" nowrap="nowrap">pc</td>
    <td align="center" valign="top" nowrap="nowrap">已确认,已付款,未发货</td>
    <td align="center" valign="top"  nowrap="nowrap">
     <a href="order.php?act=info&order_id=150">查看</a>
              </td>
  </tr>
    <tr class="
  	tr_canceled 
    tr_nopay 
            ">
    <td valign="top" nowrap="nowrap"><input type="checkbox" name="checkboxes" value="2016022598233" /><a href="order.php?act=info&order_id=149" id="order_6">2016022598233</a></td>
	    <!-- 代码增加_start   By www.wygk.cn -->
	<td>一般订单</td>
	<!-- 代码增加_end   By www.wygk.cn -->
    <td>yiren<br />02-25 20:22</td>
    <td align="left" valign="top"><a href="mailto:"> 123</a> [TEL: -] <br />圣达菲</td>
    <td align="right" valign="top" nowrap="nowrap">¥142.92</td>
    <td align="right" valign="top" nowrap="nowrap">¥139.92</td>
	<td align="right" valign="top" nowrap="nowrap">pc</td>
    <td align="center" valign="top" nowrap="nowrap"><font color="red">无效</font>,未付款,未发货</td>
    <td align="center" valign="top"  nowrap="nowrap">
     <a href="order.php?act=info&order_id=149">查看</a>
          <br /><a href="javascript:;" onclick="listTable.remove(149, remove_confirm, 'remove_order')">移除</a>
              </td>
  </tr>
    <tr class="
  	tr_canceled 
    tr_nopay 
            ">
    <td valign="top" nowrap="nowrap"><input type="checkbox" name="checkboxes" value="2016022537343" /><a href="order.php?act=info&order_id=148" id="order_7">2016022537343</a></td>
	    <!-- 代码增加_start   By www.wygk.cn -->
	<td>一般订单</td>
	<!-- 代码增加_end   By www.wygk.cn -->
    <td>yiren<br />02-25 20:21</td>
    <td align="left" valign="top"><a href="mailto:"> 123</a> [TEL: -] <br />圣达菲</td>
    <td align="right" valign="top" nowrap="nowrap">¥141.65</td>
    <td align="right" valign="top" nowrap="nowrap">¥141.65</td>
	<td align="right" valign="top" nowrap="nowrap">pc</td>
    <td align="center" valign="top" nowrap="nowrap"><font color="red">无效</font>,未付款,未发货</td>
    <td align="center" valign="top"  nowrap="nowrap">
     <a href="order.php?act=info&order_id=148">查看</a>
          <br /><a href="javascript:;" onclick="listTable.remove(148, remove_confirm, 'remove_order')">移除</a>
              </td>
  </tr>
    <tr class="
  	tr_canceled 
    tr_nopay 
            ">
    <td valign="top" nowrap="nowrap"><input type="checkbox" name="checkboxes" value="2016022587052" /><a href="order.php?act=info&order_id=147" id="order_8">2016022587052</a></td>
	    <!-- 代码增加_start   By www.wygk.cn -->
	<td>一般订单</td>
	<!-- 代码增加_end   By www.wygk.cn -->
    <td>yiren<br />02-25 09:15</td>
    <td align="left" valign="top"><a href="mailto:18039485645@163.com"> sdf</a> [TEL: -] <br />sdf dsf 考虑到进水阀链接咖啡机</td>
    <td align="right" valign="top" nowrap="nowrap">¥864.15</td>
    <td align="right" valign="top" nowrap="nowrap">¥15.00</td>
	<td align="right" valign="top" nowrap="nowrap">pc</td>
    <td align="center" valign="top" nowrap="nowrap"><font color="red">无效</font>,未付款,未发货</td>
    <td align="center" valign="top"  nowrap="nowrap">
     <a href="order.php?act=info&order_id=147">查看</a>
          <br /><a href="javascript:;" onclick="listTable.remove(147, remove_confirm, 'remove_order')">移除</a>
              </td>
  </tr>
    <tr class="
  	tr_canceled 
    tr_nopay 
            ">
    <td valign="top" nowrap="nowrap"><input type="checkbox" name="checkboxes" value="2016022595140" /><a href="order.php?act=info&order_id=146" id="order_9">2016022595140</a></td>
	    <!-- 代码增加_start   By www.wygk.cn -->
	<td>自提订单</td>
	<!-- 代码增加_end   By www.wygk.cn -->
    <td>yiren<br />02-25 08:59</td>
    <td align="left" valign="top"><a href="mailto:18039485645@163.com"> sdf</a> [TEL: -] <br />sdf dsf 考虑到进水阀链接咖啡机</td>
    <td align="right" valign="top" nowrap="nowrap">¥169.15</td>
    <td align="right" valign="top" nowrap="nowrap">¥169.15</td>
	<td align="right" valign="top" nowrap="nowrap">pc</td>
    <td align="center" valign="top" nowrap="nowrap"><font color="red">无效</font>,未付款,未发货</td>
    <td align="center" valign="top"  nowrap="nowrap">
     <a href="order.php?act=info&order_id=146">查看</a>
          <br /><a href="javascript:;" onclick="listTable.remove(146, remove_confirm, 'remove_order')">移除</a>
              </td>
  </tr>
    <tr class="
  	tr_canceled 
    tr_nopay 
            ">
    <td valign="top" nowrap="nowrap"><input type="checkbox" name="checkboxes" value="2016022461530" /><a href="order.php?act=info&order_id=145" id="order_10">2016022461530</a></td>
	    <!-- 代码增加_start   By www.wygk.cn -->
	<td>自提订单</td>
	<!-- 代码增加_end   By www.wygk.cn -->
    <td>yiren<br />02-24 21:32</td>
    <td align="left" valign="top"><a href="mailto:18039485645@163.com"> sdf</a> [TEL: -] <br />sdf dsf 考虑到进水阀链接咖啡机</td>
    <td align="right" valign="top" nowrap="nowrap">¥169.15</td>
    <td align="right" valign="top" nowrap="nowrap">¥169.15</td>
	<td align="right" valign="top" nowrap="nowrap">pc</td>
    <td align="center" valign="top" nowrap="nowrap"><font color="red">无效</font>,未付款,未发货</td>
    <td align="center" valign="top"  nowrap="nowrap">
     <a href="order.php?act=info&order_id=145">查看</a>
          <br /><a href="javascript:;" onclick="listTable.remove(145, remove_confirm, 'remove_order')">移除</a>
              </td>
  </tr>
    <tr class="
  	tr_canceled 
    tr_nopay 
            ">
    <td valign="top" nowrap="nowrap"><input type="checkbox" name="checkboxes" value="2016022474077" /><a href="order.php?act=info&order_id=144" id="order_11">2016022474077</a></td>
	    <!-- 代码增加_start   By www.wygk.cn -->
	<td>自提订单</td>
	<!-- 代码增加_end   By www.wygk.cn -->
    <td>yiren<br />02-24 17:23</td>
    <td align="left" valign="top"><a href="mailto:18039485645@163.com"> sdf</a> [TEL: -] <br />sdf dsf 考虑到进水阀链接咖啡机</td>
    <td align="right" valign="top" nowrap="nowrap">¥169.15</td>
    <td align="right" valign="top" nowrap="nowrap">¥169.15</td>
	<td align="right" valign="top" nowrap="nowrap">pc</td>
    <td align="center" valign="top" nowrap="nowrap"><font color="red">无效</font>,未付款,未发货</td>
    <td align="center" valign="top"  nowrap="nowrap">
     <a href="order.php?act=info&order_id=144">查看</a>
          <br /><a href="javascript:;" onclick="listTable.remove(144, remove_confirm, 'remove_order')">移除</a>
              </td>
  </tr>
    <tr class="
  	 
     
        tr_nosend    ">
    <td valign="top" nowrap="nowrap"><input type="checkbox" name="checkboxes" value="2016022401774" /><a href="order.php?act=info&order_id=143" id="order_12">2016022401774</a></td>
	    <!-- 代码增加_start   By www.wygk.cn -->
	<td>自提订单</td>
	<!-- 代码增加_end   By www.wygk.cn -->
    <td>yiren<br />02-24 13:15</td>
    <td align="left" valign="top"><a href="mailto:18039485645@163.com"> sdf</a> [TEL: -] <br />sdf dsf 考虑到进水阀链接咖啡机</td>
    <td align="right" valign="top" nowrap="nowrap">¥67.15</td>
    <td align="right" valign="top" nowrap="nowrap">¥0.00</td>
	<td align="right" valign="top" nowrap="nowrap">pc</td>
    <td align="center" valign="top" nowrap="nowrap">已确认,已付款,未发货</td>
    <td align="center" valign="top"  nowrap="nowrap">
     <a href="order.php?act=info&order_id=143">查看</a>
              </td>
  </tr>
    <tr class="
  	 
     
        tr_nosend    ">
    <td valign="top" nowrap="nowrap"><input type="checkbox" name="checkboxes" value="2016022497287" /><a href="order.php?act=info&order_id=142" id="order_13">2016022497287</a></td>
	    <!-- 代码增加_start   By www.wygk.cn -->
	<td>一般订单</td>
	<!-- 代码增加_end   By www.wygk.cn -->
    <td>yiren<br />02-24 13:13</td>
    <td align="left" valign="top"><a href="mailto:18039485645@163.com"> sdf</a> [TEL: -] <br />sdf dsf 考虑到进水阀链接咖啡机</td>
    <td align="right" valign="top" nowrap="nowrap">¥67.15</td>
    <td align="right" valign="top" nowrap="nowrap">¥0.00</td>
	<td align="right" valign="top" nowrap="nowrap">pc</td>
    <td align="center" valign="top" nowrap="nowrap">已确认,已付款,未发货</td>
    <td align="center" valign="top"  nowrap="nowrap">
     <a href="order.php?act=info&order_id=142">查看</a>
              </td>
  </tr>
    <tr class="
  	tr_canceled 
    tr_nopay 
            ">
    <td valign="top" nowrap="nowrap"><input type="checkbox" name="checkboxes" value="2016022457490" /><a href="order.php?act=info&order_id=141" id="order_14">2016022457490</a></td>
	    <!-- 代码增加_start   By www.wygk.cn -->
	<td>一般订单</td>
	<!-- 代码增加_end   By www.wygk.cn -->
    <td>yiren<br />02-24 13:10</td>
    <td align="left" valign="top"><a href="mailto:18039485645@163.com"> sdf</a> [TEL: -] <br />sdf dsf 考虑到进水阀链接咖啡机</td>
    <td align="right" valign="top" nowrap="nowrap">¥236.30</td>
    <td align="right" valign="top" nowrap="nowrap">¥236.30</td>
	<td align="right" valign="top" nowrap="nowrap">pc</td>
    <td align="center" valign="top" nowrap="nowrap"><font color="red">无效</font>,未付款,未发货</td>
    <td align="center" valign="top"  nowrap="nowrap">
     <a href="order.php?act=info&order_id=141">查看</a>
          <br /><a href="javascript:;" onclick="listTable.remove(141, remove_confirm, 'remove_order')">移除</a>
              </td>
  </tr>
  </table>

<!-- 分页 -->
<table id="page-table" cellspacing="0">
  <tr>
    <td align="right" nowrap="true">
          <!-- $Id: page.htm 14216 2008-03-10 02:27:21Z testyang $ -->
            <div id="turn-page">
        总计  <span id="totalRecords">52</span>
        个记录分为 <span id="totalPages">4</span>
        页当前第 <span id="pageCurrent">1</span>
        页，每页 <input type='text' size='3' id='pageSize' value="15" onkeypress="return listTable.changePageSize(event)" />
        <span id="page-link">
          <a href="javascript:listTable.gotoPageFirst()">第一页</a>
          <a href="javascript:listTable.gotoPagePrev()">上一页</a>
          <a href="javascript:listTable.gotoPageNext()">下一页</a>
          <a href="javascript:listTable.gotoPageLast()">最末页</a>
        <!--   <select id="gotoPage" onchange="listTable.gotoPage(this.value)">
            <option value='1'>1</option><option value='2'>2</option><option value='3'>3</option><option value='4'>4</option>          </select> -->
         跳转到 <input id="pageInput" title="按回车键跳转到指定页数" value="1" style="width:25px;"  onkeypress="if(event.keyCode==13) {listTable.gotoPage(this.value);}"   type="text">
          <input class="button" id="pagerBtn" value="GO" onclick="goToPage()" type="button">
        </span>
      </div>
<script>
function goToPage(){
	var page=$("#pageInput").val();
	listTable.gotoPage(page);
}
</script>    </td>
  </tr>
</table>

  </div>
  <div>
    <input name="confirm" type="submit" id="btnSubmit" value="确认" class="button" disabled="true" onclick="this.form.target = '_self'" />
    <input name="invalid" type="submit" id="btnSubmit1" value="无效" class="button" disabled="true" onclick="this.form.target = '_self'" />
    <input name="cancel" type="submit" id="btnSubmit2" value="取消" class="button" disabled="true" onclick="this.form.target = '_self'" />
    <input name="remove" type="submit" id="btnSubmit3" value="移除" class="button" disabled="true" onclick="this.form.target = '_self'" />
    <input name="print" type="submit" id="btnSubmit4" value="打印订单" class="button" disabled="true" onclick="this.form.target = '_blank'" />
    <input name="batch" type="hidden" value="1" />
    <input name="order_id" type="hidden" value="" />
  </div>
</form>
<script language="JavaScript">
listTable.recordCount = 52;
listTable.pageCount = 4;

listTable.filter.order_sn = '';
listTable.filter.consignee = '';
listTable.filter.email = '';
listTable.filter.address = '';
listTable.filter.zipcode = '';
listTable.filter.tel = '';
listTable.filter.mobile = '0';
listTable.filter.country = '0';
listTable.filter.province = '0';
listTable.filter.city = '0';
listTable.filter.district = '0';
listTable.filter.shipping_id = '0';
listTable.filter.pay_id = '0';
listTable.filter.order_status = '-1';
listTable.filter.shipping_status = '-1';
listTable.filter.pay_status = '-1';
listTable.filter.user_id = '0';
listTable.filter.user_name = '';
listTable.filter.composite_status = '-1';
listTable.filter.group_buy_id = '0';
listTable.filter.pre_sale_id = '0';
listTable.filter.sort_by = 'add_time';
listTable.filter.sort_order = 'DESC';
listTable.filter.start_time = '';
listTable.filter.end_time = '';
listTable.filter.order_type = '0';
listTable.filter.supp = '0';
listTable.filter.suppid = '0';
listTable.filter.inv_status = '';
listTable.filter.inv_type = '';
listTable.filter.vat_inv_consignee_name = '';
listTable.filter.vat_inv_consignee_phone = '';
listTable.filter.add_time = '';
listTable.filter.page = '1';
listTable.filter.page_size = '15';
listTable.filter.record_count = '52';
listTable.filter.page_count = '4';


    onload = function()
    {
        // 开始检查订单
        startCheckOrder();
    }

    /**
     * 搜索订单
     */
    function searchOrder()
    {
        listTable.filter['order_sn'] = Utils.trim(document.forms['searchForm'].elements['order_sn'].value);
        listTable.filter['consignee'] = Utils.trim(document.forms['searchForm'].elements['consignee'].value);
        listTable.filter['composite_status'] = document.forms['searchForm'].elements['status'].value;
				<!-- 代码增加_start   By www.wygk.cn -->
		listTable.filter['order_type'] = document.forms['searchForm'].elements['order_type'].value;
		<!-- 代码增加_end   By www.wygk.cn -->
        listTable.filter['page'] = 1;
        listTable.loadList();
    }

    function check()
    {
      var snArray = new Array();
      var eles = document.forms['listForm'].elements;
      for (var i=0; i<eles.length; i++)
      {
        if (eles[i].tagName == 'INPUT' && eles[i].type == 'checkbox' && eles[i].checked && eles[i].value != 'on')
        {
          snArray.push(eles[i].value);
        }
      }
      if (snArray.length == 0)
      {
        return false;
      }
      else
      {
        eles['order_id'].value = snArray.toString();
        return true;
      }
    }
    /**
     * 显示订单商品及缩图
     */
    var show_goods_layer = 'order_goods_layer';
    var goods_hash_table = new Object;
    var timer = new Object;

    /**
     * 绑定订单号事件
     *
     * @return void
     */
    function bind_order_event()
    {
        var order_seq = 0;
        while(true)
        {
            var order_sn = Utils.$('order_'+order_seq);
            if (order_sn)
            {
                order_sn.onmouseover = function(e)
                {
                    try
                    {
                        window.clearTimeout(timer);
                    }
                    catch(e)
                    {
                    }
                    var order_id = Utils.request(this.href, 'order_id');
                    show_order_goods(e, order_id, show_goods_layer);
                }
                order_sn.onmouseout = function(e)
                {
                    hide_order_goods(show_goods_layer)
                }
                order_seq++;
            }
            else
            {
                break;
            }
        }
    }
    listTable.listCallback = function(result, txt) 
    {
        if (result.error > 0) 
        {
            alert(result.message);
        }
        else 
        {
            try 
            {
                document.getElementById('listDiv').innerHTML = result.content;
                bind_order_event();
                if (typeof result.filter == "object") 
                {
                    listTable.filter = result.filter;
                }
                listTable.pageCount = result.page_count;
            }
            catch(e)
            {
                alert(e.message);
            }
        }
    }
    /**
     * 浏览器兼容式绑定Onload事件
     *
     */
    if (Browser.isIE)
    {
        window.attachEvent("onload", bind_order_event);
    }
    else
    {
        window.addEventListener("load", bind_order_event, false);
    }

    /**
     * 建立订单商品显示层
     *
     * @return void
     */
    function create_goods_layer(id)
    {
        if (!Utils.$(id))
        {
            var n_div = document.createElement('DIV');
            n_div.id = id;
            n_div.className = 'order-goods';
            document.body.appendChild(n_div);
            Utils.$(id).onmouseover = function()
            {
                window.clearTimeout(window.timer);
            }
            Utils.$(id).onmouseout = function()
            {
                hide_order_goods(id);
            }
        }
        else
        {
            Utils.$(id).style.display = '';
        }
    }

    /**
     * 显示订单商品数据
     *
     * @return void
     */
    function show_order_goods(e, order_id, layer_id)
    {
        create_goods_layer(layer_id);
        $layer_id = Utils.$(layer_id);
        $layer_id.style.top = (Utils.y(e) + 12) + 'px';
        $layer_id.style.left = (Utils.x(e) + 12) + 'px';
        if (typeof(goods_hash_table[order_id]) == 'object')
        {
            response_goods_info(goods_hash_table[order_id]);
        }
        else
        {
            $layer_id.innerHTML = loading;
            Ajax.call('order.php?is_ajax=1&act=get_goods_info&order_id='+order_id, '', response_goods_info , 'POST', 'JSON');
        }
    }

    /**
     * 隐藏订单商品
     *
     * @return void
     */
    function hide_order_goods(layer_id)
    {
        $layer_id = Utils.$(layer_id);
        window.timer = window.setTimeout('$layer_id.style.display = "none"', 500);
    }


    /**
     * 处理订单商品的Callback
     *
     * @return void
     */
    function response_goods_info(result)
    {
        if (result.error > 0)
        {
            alert(result.message);
            hide_order_goods(show_goods_layer);
            return;
        }
        if (typeof(goods_hash_table[result.content[0].order_id]) == 'undefined')
        {
            goods_hash_table[result.content[0].order_id] = result;
        }
        Utils.$(show_goods_layer).innerHTML = result.content[0].str;
    }
</script>


<div id="footer">
共执行 19 个查询，用时 0.026001 秒，Gzip 已禁用，内存占用 5.987 MB<br />
版权所有 &copy; 2008-2030 广州网软志成信息科技有限公司，并保留所有权利。</div>
<!-- 新订单提示信息 -->
<div id="popMsg">
  <table cellspacing="0" cellpadding="0" width="100%" bgcolor="#cfdef4" border="0">
  <tr>
    <td style="color: #0f2c8c" width="30" height="24"></td>
    <td style="font-weight: normal; color: #1f336b; padding-top: 4px;padding-left: 4px" valign="center" width="100%"> 新订单通知</td>
    <td style="padding-top: 2px;padding-right:2px" valign="center" align="right" width="19"><span title="关闭" style="cursor: hand;cursor:pointer;color:red;font-size:12px;font-weight:bold;margin-right:4px;" onclick="Message.close()" >×</span><!-- <img title=关闭 style="cursor: hand" onclick=closediv() hspace=3 src="msgclose.jpg"> --></td>
  </tr>
  <tr>
    <td style="padding-right: 1px; padding-bottom: 1px" colspan="3" height="70">
    <div id="popMsgContent">
      <p>您有 <strong style="color:#ff0000" id="spanNewOrder">1</strong> 个新订单以及       <strong style="color:#ff0000" id="spanNewPaid">0</strong> 个新付款的订单</p>
      <p align="center" style="word-break:break-all"><a href="order.php?act=list"><span style="color:#ff0000">点击查看新订单</span></a></p>
    </div>
    </td>
  </tr>
  </table>
</div>

<!--
<embed src="images/online.wav" width="0" height="0" autostart="false" name="msgBeep" id="msgBeep" enablejavascript="true"/>
-->
<object classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000" codebase="http://active.macromedia.com/flash2/cabs/swflash.cab#version=4,0,0,0" id="msgBeep" width="1" height="1">
  <param name="movie" value="images/online.swf">
  <param name="quality" value="high">
  <embed src="images/online.swf" name="msgBeep" id="msgBeep" quality="high" width="0" height="0" type="application/x-shockwave-flash" pluginspage="http://www.macromedia.com/shockwave/download/index.cgi?p1_prod_version=shockwaveflash">
  </embed>
</object>

<script language="JavaScript">
document.onmousemove=function(e)
{
  var obj = Utils.srcElement(e);
  if (typeof(obj.onclick) == 'function' && obj.onclick.toString().indexOf('listTable.edit') != -1)
  {
    obj.title = '点击修改内容';
    obj.style.cssText = 'background: #278296;';
    obj.onmouseout = function(e)
    {
      this.style.cssText = '';
    }
  }
  else if (typeof(obj.href) != 'undefined' && obj.href.indexOf('listTable.sort') != -1)
  {
    obj.title = '点击对列表排序';
  }
}
<!--


var MyTodolist;
function showTodoList(adminid)
{
  if(!MyTodolist)
  {
    var global = $import("../js/global.js","js");
    global.onload = global.onreadystatechange= function()
    {
      if(this.readyState && this.readyState=="loading")return;
      var md5 = $import("js/md5.js","js");
      md5.onload = md5.onreadystatechange= function()
      {
        if(this.readyState && this.readyState=="loading")return;
        var todolist = $import("js/todolist.js","js");
        todolist.onload = todolist.onreadystatechange = function()
        {
          if(this.readyState && this.readyState=="loading")return;
          MyTodolist = new Todolist();
          MyTodolist.show();
        }
      }
    }
  }
  else
  {
    if(MyTodolist.visibility)
    {
      MyTodolist.hide();
    }
    else
    {
      MyTodolist.show();
    }
  }
}

if (Browser.isIE)
{
  onscroll = function()
  {
    //document.getElementById('calculator').style.top = document.body.scrollTop;
    document.getElementById('popMsg').style.top = (document.body.scrollTop + document.body.clientHeight - document.getElementById('popMsg').offsetHeight) + "px";
  }
}

if (document.getElementById("listDiv"))
{
  document.getElementById("listDiv").onmouseover = function(e)
  {
    obj = Utils.srcElement(e);

    if (obj)
    {
      if (obj.parentNode.tagName.toLowerCase() == "tr") row = obj.parentNode;
      else if (obj.parentNode.parentNode.tagName.toLowerCase() == "tr") row = obj.parentNode.parentNode;
      else return;

      for (i = 0; i < row.cells.length; i++)
      {
        if (row.cells[i].tagName != "TH") row.cells[i].style.backgroundColor = '#F4FAFB';
      }
    }

  }

  document.getElementById("listDiv").onmouseout = function(e)
  {
    obj = Utils.srcElement(e);

    if (obj)
    {
      if (obj.parentNode.tagName.toLowerCase() == "tr") row = obj.parentNode;
      else if (obj.parentNode.parentNode.tagName.toLowerCase() == "tr") row = obj.parentNode.parentNode;
      else return;

      for (i = 0; i < row.cells.length; i++)
      {
          if (row.cells[i].tagName != "TH") row.cells[i].style.backgroundColor = '';
      }
    }
  }

  document.getElementById("listDiv").onclick = function(e)
  {
    var obj = Utils.srcElement(e);

    if (obj.tagName == "INPUT" && obj.type == "checkbox")
    {
      if (!document.forms['listForm'])
      {
        return;
      }
      var nodes = document.forms['listForm'].elements;
      var checked = false;

      for (i = 0; i < nodes.length; i++)
      {
        if (nodes[i].checked)
        {
           checked = true;
           break;
         }
      }

      if(document.getElementById("btnSubmit"))
      {
        document.getElementById("btnSubmit").disabled = !checked;
      }
      for (i = 1; i <= 10; i++)
      {
        if (document.getElementById("btnSubmit" + i))
        {
          document.getElementById("btnSubmit" + i).disabled = !checked;
        }
      }
    }
  }

}

//-->
</script>
</body>
</html><!--增值税发票_添加_START_www.wrzc.net-->
<!--增值税发票_添加_END_www.wrzc.net-->

<!-- $Id: order_query.htm 14216 2008-03-10 02:27:21Z testyang $ -->
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>管理中心 - 订单查询 </title>
<meta name="robots" content="noindex, nofollow">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link href="styles/general.css" rel="stylesheet" type="text/css" />
<link href="styles/main.css" rel="stylesheet" type="text/css" />
<link href="styles/chosen/chosen.css" rel="stylesheet" type="text/css">
<script type="text/javascript" src="js/jquery-1.6.2.min.js"></script><script type="text/javascript" src="js/chosen.jquery.min.js"></script><script type="text/javascript" src="js/jquery.json.js"></script><script type="text/javascript" src="../js/transport.js"></script><script type="text/javascript" src="js/common.js"></script><script language="JavaScript">
<!--
// 这里把JS用到的所有语言都赋值到这里
var process_request = "正在处理您的请求...";
var todolist_caption = "记事本";
var todolist_autosave = "自动保存";
var todolist_save = "保存";
var todolist_clear = "清除";
var todolist_confirm_save = "是否将更改保存到记事本？";
var todolist_confirm_clear = "是否清空内容？";
var remove_confirm = "删除订单将清除该订单的所有信息。您确定要这么做吗？";
var confirm_merge = "您确实要合并这两个订单吗？";
var input_price = "自定义价格";
var pls_search_user = "请搜索并选择会员";
var confirm_drop = "确认要删除该商品吗？";
var invalid_goods_number = "商品数量不正确";
var pls_search_goods = "请搜索并选择商品";
var pls_select_area = "请完整选择所在地区";
var pls_select_shipping = "请选择配送方式";
var pls_select_payment = "请选择支付方式";
var pls_select_pack = "请选择包装";
var pls_select_card = "请选择贺卡";
var pls_input_note = "请您填写备注！";
var pls_input_cancel = "请您填写取消原因！";
var pls_select_refund = "请选择退款方式！";
var pls_select_agency = "请选择办事处！";
var pls_select_other_agency = "该订单现在就属于这个办事处，请选择其他办事处！";
var loading = "加载中...";
var remove_invoice_confirm = "您确定删除该订单的发票信息吗？";
//-->
</script>
</head>
<body>

<div id="menu_list" onmouseover="show_popup()" onmouseout="hide_popup()">
<ul>
<li><a href="goods.php?act=add" target="main_frame">添加新商品</a></li>
<li><a href="category.php?act=add" target="main_frame">添加商品分类</a></li>
<li><a href="order.php?act=add" target="main_frame">添加订单</a></li>
<li><a href="article.php?act=add" target="main_frame">添加新文章</a></li>
<li><a href="users.php?act=add" target="main_frame">添加会员</a></li>
</ul>
</div>
<script>
function show_popup(){
frmBody = parent.document.getElementById('frame-body');
if (frmBody.cols == "37, 12, *")
{
parent.main_frame.document.getElementById('menu_list').style.left = '195px';
}
else
{
parent.main_frame.document.getElementById('menu_list').style.left = '40px';
}
parent.main_frame.document.getElementById('menu_list').style.display = 'block';
}
function hide_popup(){

parent.main_frame.document.getElementById('menu_list').style.display = 'none';
}
</script>
<h1>
<span class="action-span"><a href="order.php?act=list">订单列表</a></span>
<span class="action-span1"><a href="index.php?act=main">管理中心</a> </span><span id="search_id" class="action-span1"> - 订单查询 </span>
<div style="clear:both"></div>
</h1>
<script type="text/javascript" src="../js/calendar.php"></script>
<link href="../js/calendar/calendar.css" rel="stylesheet" type="text/css" />
<div class="main-div">
<form action="order.php?act=list" method="post" enctype="multipart/form-data" name="searchForm">
  <table cellspacing="1" cellpadding="3" width="100%">
    <tr>
      <td align="right">订单号：</td>
      <td><input name="order_sn" type="text" id="order_sn" size="30"></td>
      <td align="right">电子邮件：</td>
      <td><input name="email" type="text" id="email" size="30"></td>
    </tr>
    <tr>
      <td align="right">购货人：</td>
      <td><input name="user_name" type="text" id="user_name" size="30"></td>
      <td align="right">收货人：</td>
      <td><input name="consignee" type="text" id="consignee" size="30"></td>
    </tr>
    <tr>
      <td align="right">地址：</td>
      <td><input name="address" type="text" id="address" size="30"></td>
      <td align="right">邮编：</td>
      <td><input name="zipcode" type="text" id="zipcode" size="30"></td>
    </tr>
    <tr>
      <td align="right">电话：</td>
      <td><input name="tel" type="text" id="tel" size="30"></td>
      <td align="right">手机：</td>
      <td><input name="mobile" type="text" id="mobile" size="30"></td>
    </tr>
    <tr>
      <td align="right">所在地区：</td>
      <td colspan="3"><select name="country" id="selCountries" onchange="region.changed(this, 1, 'selProvinces')">
          <option value="0">请选择...</option>
                    <option value="1">中国</option>
                </select>
        <select name="province" id="selProvinces" onchange="region.changed(this, 2, 'selCities')">
          <option value="0">请选择...</option>
        </select>
        <select name="city" id="selCities" onchange="region.changed(this, 3, 'selDistricts')">
          <option value="0">请选择...</option>
        </select>
        <select name="district" id="selDistricts">
          <option value="0">请选择...</option>
        </select></td>
      </tr>
    <tr>
      <td align="right">配送方式：</td>
      <td><select name="shipping_id" id="select4">
        <option value="0">请选择...</option>
                <option value="27">韵达快递</option>
                <option value="3">顺丰速运</option>
                <option value="12">申通快递</option>
                <option value="5">中通速递</option>
                <option value="25">圆通速递</option>
                <option value="7">上门取货</option>
                <option value="8">城际快递</option>
                <option value="10">申通快递</option>
                <option value="11"><font color="#ff3300">同城快递</font></option>
                <option value="15">申通快递</option>
                <option value="16">宅急送</option>
                <option value="17">申通快递</option>
                <option value="18">申通快递</option>
                <option value="19">门店自提</option>
                <option value="20">申通快递</option>
                <option value="26">顺丰速运</option>
                <option value="22">天天快递</option>
                <option value="24">中通速递</option>
                <option value="28">中通速递</option>
                    </select></td>
      <td align="right">支付方式：</td>
      <td><select name="pay_id" id="select5">
        <option value="0">请选择...</option>
                <option value="1">支付宝</option>
                <option value="3">支付宝-网银直连</option>
                <option value="4">余额支付</option>
                <option value="5">银联在线</option>
                <option value="6">货到付款</option>
                <option value="7">微信支付</option>
                    </select></td>
    </tr>
    <tr>
      <td align="right">下单时间：</td>
      <td colspan="3">
      <input type="text" name="start_time" maxlength="60" size="30" readonly="readonly" id="start_time_id" />
      <input name="start_time_btn" type="button" id="start_time_btn" onclick="return showCalendar('start_time_id', '%Y-%m-%d %H:%M', '24', false, 'start_time_btn');" value="选择" class="button"/>
      ~      
      <input type="text" name="end_time" maxlength="60" size="30" readonly="readonly" id="end_time_id" />
      <input name="end_time_btn" type="button" id="end_time_btn" onclick="return showCalendar('end_time_id', '%Y-%m-%d %H:%M', '24', false, 'end_time_btn');" value="选择" class="button"/>  
      </td>
    </tr>
    <tr>
      <td align="right">订单状态：</td>
      <td colspan="3">
        <select name="order_status" id="select9">
          <option value="-1">请选择...</option>
          <option value="0">未确认</option><option value="1">已确认</option><option value="2"><font color="red"> 取消</font></option><option value="3"><font color="red">无效</font></option><option value="4"><font color="red">退货</font></option><option value="5">已分单</option><option value="6">部分分单</option>        </select>
        付款状态：       
        <select name="pay_status" id="select11">
          <option value="-1">请选择...</option>
          <option value="0">未付款</option><option value="1">付款中</option><option value="2">已付款</option>        </select>
        发货状态：        <select name="shipping_status" id="select10">
          <option value="-1">请选择...</option>
          <option value="0">未发货</option><option value="3">配货中</option><option value="1">已发货</option><option value="2">收货确认</option><option value="4">已发货(部分商品)</option><option value="5">发货中</option>        </select></td>
    </tr>
    <tr>
      <td colspan="4"><div align="center">
        <input name="query" type="submit" class="button" id="query" value=" 搜索 " />
        <input name="reset" type="reset" class='button' value=' 重置 ' />
      </div></td>
      </tr>
  </table>
</form>
</div>
<script type="text/javascript" src="../js/transport.org.js"></script><script type="text/javascript" src="../js/region.js"></script>
<script language="JavaScript">
region.isAdmin = true;
onload = function()
{
  // 开始检查订单
  startCheckOrder();
}
</script>

<div id="footer">
共执行 4 个查询，用时 0.016001 秒，Gzip 已禁用，内存占用 5.891 MB<br />
版权所有 &copy; 2008-2030 广州网软志成信息科技有限公司，并保留所有权利。</div>
<script type="text/javascript" src="../js/utils.js"></script><!-- 新订单提示信息 -->
<div id="popMsg">
  <table cellspacing="0" cellpadding="0" width="100%" bgcolor="#cfdef4" border="0">
  <tr>
    <td style="color: #0f2c8c" width="30" height="24"></td>
    <td style="font-weight: normal; color: #1f336b; padding-top: 4px;padding-left: 4px" valign="center" width="100%"> 新订单通知</td>
    <td style="padding-top: 2px;padding-right:2px" valign="center" align="right" width="19"><span title="关闭" style="cursor: hand;cursor:pointer;color:red;font-size:12px;font-weight:bold;margin-right:4px;" onclick="Message.close()" >×</span><!-- <img title=关闭 style="cursor: hand" onclick=closediv() hspace=3 src="msgclose.jpg"> --></td>
  </tr>
  <tr>
    <td style="padding-right: 1px; padding-bottom: 1px" colspan="3" height="70">
    <div id="popMsgContent">
      <p>您有 <strong style="color:#ff0000" id="spanNewOrder">1</strong> 个新订单以及       <strong style="color:#ff0000" id="spanNewPaid">0</strong> 个新付款的订单</p>
      <p align="center" style="word-break:break-all"><a href="order.php?act=list"><span style="color:#ff0000">点击查看新订单</span></a></p>
    </div>
    </td>
  </tr>
  </table>
</div>

<!--
<embed src="images/online.wav" width="0" height="0" autostart="false" name="msgBeep" id="msgBeep" enablejavascript="true"/>
-->
<object classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000" codebase="http://active.macromedia.com/flash2/cabs/swflash.cab#version=4,0,0,0" id="msgBeep" width="1" height="1">
  <param name="movie" value="images/online.swf">
  <param name="quality" value="high">
  <embed src="images/online.swf" name="msgBeep" id="msgBeep" quality="high" width="0" height="0" type="application/x-shockwave-flash" pluginspage="http://www.macromedia.com/shockwave/download/index.cgi?p1_prod_version=shockwaveflash">
  </embed>
</object>

<script language="JavaScript">
document.onmousemove=function(e)
{
  var obj = Utils.srcElement(e);
  if (typeof(obj.onclick) == 'function' && obj.onclick.toString().indexOf('listTable.edit') != -1)
  {
    obj.title = '点击修改内容';
    obj.style.cssText = 'background: #278296;';
    obj.onmouseout = function(e)
    {
      this.style.cssText = '';
    }
  }
  else if (typeof(obj.href) != 'undefined' && obj.href.indexOf('listTable.sort') != -1)
  {
    obj.title = '点击对列表排序';
  }
}
<!--


var MyTodolist;
function showTodoList(adminid)
{
  if(!MyTodolist)
  {
    var global = $import("../js/global.js","js");
    global.onload = global.onreadystatechange= function()
    {
      if(this.readyState && this.readyState=="loading")return;
      var md5 = $import("js/md5.js","js");
      md5.onload = md5.onreadystatechange= function()
      {
        if(this.readyState && this.readyState=="loading")return;
        var todolist = $import("js/todolist.js","js");
        todolist.onload = todolist.onreadystatechange = function()
        {
          if(this.readyState && this.readyState=="loading")return;
          MyTodolist = new Todolist();
          MyTodolist.show();
        }
      }
    }
  }
  else
  {
    if(MyTodolist.visibility)
    {
      MyTodolist.hide();
    }
    else
    {
      MyTodolist.show();
    }
  }
}

if (Browser.isIE)
{
  onscroll = function()
  {
    //document.getElementById('calculator').style.top = document.body.scrollTop;
    document.getElementById('popMsg').style.top = (document.body.scrollTop + document.body.clientHeight - document.getElementById('popMsg').offsetHeight) + "px";
  }
}

if (document.getElementById("listDiv"))
{
  document.getElementById("listDiv").onmouseover = function(e)
  {
    obj = Utils.srcElement(e);

    if (obj)
    {
      if (obj.parentNode.tagName.toLowerCase() == "tr") row = obj.parentNode;
      else if (obj.parentNode.parentNode.tagName.toLowerCase() == "tr") row = obj.parentNode.parentNode;
      else return;

      for (i = 0; i < row.cells.length; i++)
      {
        if (row.cells[i].tagName != "TH") row.cells[i].style.backgroundColor = '#F4FAFB';
      }
    }

  }

  document.getElementById("listDiv").onmouseout = function(e)
  {
    obj = Utils.srcElement(e);

    if (obj)
    {
      if (obj.parentNode.tagName.toLowerCase() == "tr") row = obj.parentNode;
      else if (obj.parentNode.parentNode.tagName.toLowerCase() == "tr") row = obj.parentNode.parentNode;
      else return;

      for (i = 0; i < row.cells.length; i++)
      {
          if (row.cells[i].tagName != "TH") row.cells[i].style.backgroundColor = '';
      }
    }
  }

  document.getElementById("listDiv").onclick = function(e)
  {
    var obj = Utils.srcElement(e);

    if (obj.tagName == "INPUT" && obj.type == "checkbox")
    {
      if (!document.forms['listForm'])
      {
        return;
      }
      var nodes = document.forms['listForm'].elements;
      var checked = false;

      for (i = 0; i < nodes.length; i++)
      {
        if (nodes[i].checked)
        {
           checked = true;
           break;
         }
      }

      if(document.getElementById("btnSubmit"))
      {
        document.getElementById("btnSubmit").disabled = !checked;
      }
      for (i = 1; i <= 10; i++)
      {
        if (document.getElementById("btnSubmit" + i))
        {
          document.getElementById("btnSubmit" + i).disabled = !checked;
        }
      }
    }
  }

}

//-->
</script>
</body>
</html>

<!-- $Id: order_info.htm 17060 2010-03-25 03:44:42Z liuhui $ -->

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>管理中心 - 订单信息 </title>
<meta name="robots" content="noindex, nofollow">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link href="styles/general.css" rel="stylesheet" type="text/css" />
<link href="styles/main.css" rel="stylesheet" type="text/css" />
<link href="styles/chosen/chosen.css" rel="stylesheet" type="text/css">
<script type="text/javascript" src="js/jquery-1.6.2.min.js"></script><script type="text/javascript" src="js/chosen.jquery.min.js"></script><script type="text/javascript" src="js/jquery.json.js"></script><script type="text/javascript" src="../js/transport.js"></script><script type="text/javascript" src="js/common.js"></script><script language="JavaScript">
<!--
// 这里把JS用到的所有语言都赋值到这里
var process_request = "正在处理您的请求...";
var todolist_caption = "记事本";
var todolist_autosave = "自动保存";
var todolist_save = "保存";
var todolist_clear = "清除";
var todolist_confirm_save = "是否将更改保存到记事本？";
var todolist_confirm_clear = "是否清空内容？";
var remove_confirm = "删除订单将清除该订单的所有信息。您确定要这么做吗？";
var confirm_merge = "您确实要合并这两个订单吗？";
var input_price = "自定义价格";
var pls_search_user = "请搜索并选择会员";
var confirm_drop = "确认要删除该商品吗？";
var invalid_goods_number = "商品数量不正确";
var pls_search_goods = "请搜索并选择商品";
var pls_select_area = "请完整选择所在地区";
var pls_select_shipping = "请选择配送方式";
var pls_select_payment = "请选择支付方式";
var pls_select_pack = "请选择包装";
var pls_select_card = "请选择贺卡";
var pls_input_note = "请您填写备注！";
var pls_input_cancel = "请您填写取消原因！";
var pls_select_refund = "请选择退款方式！";
var pls_select_agency = "请选择办事处！";
var pls_select_other_agency = "该订单现在就属于这个办事处，请选择其他办事处！";
var loading = "加载中...";
var remove_invoice_confirm = "您确定删除该订单的发票信息吗？";
//-->
</script>
</head>
<body>

<div id="menu_list" onmouseover="show_popup()" onmouseout="hide_popup()">
<ul>
<li><a href="goods.php?act=add" target="main_frame">添加新商品</a></li>
<li><a href="category.php?act=add" target="main_frame">添加商品分类</a></li>
<li><a href="order.php?act=add" target="main_frame">添加订单</a></li>
<li><a href="article.php?act=add" target="main_frame">添加新文章</a></li>
<li><a href="users.php?act=add" target="main_frame">添加会员</a></li>
</ul>
</div>
<script>
function show_popup(){
frmBody = parent.document.getElementById('frame-body');
if (frmBody.cols == "37, 12, *")
{
parent.main_frame.document.getElementById('menu_list').style.left = '195px';
}
else
{
parent.main_frame.document.getElementById('menu_list').style.left = '40px';
}
parent.main_frame.document.getElementById('menu_list').style.display = 'block';
}
function hide_popup(){

parent.main_frame.document.getElementById('menu_list').style.display = 'none';
}
</script>
<h1>
<span class="action-span"><a href="order.php?act=list&uselastfilter=1">订单列表</a></span>
<span class="action-span1"><a href="index.php?act=main">管理中心</a> </span><span id="search_id" class="action-span1"> - 订单信息 </span>
<div style="clear:both"></div>
</h1>
<script type="text/javascript" src="js/topbar.js"></script><script type="text/javascript" src="../js/utils.js"></script><script type="text/javascript" src="js/listtable.js"></script><script type="text/javascript" src="js/selectzone.js"></script><script type="text/javascript" src="../js/common.js"></script><div id="topbar">
  <div align="right"><a href="" onclick="closebar(); return false"><img src="images/close.gif" border="0" /></a></div>
  <table width="100%" border="0">
    <caption><strong> 购货人信息 </strong></caption>
    <tr>
      <td> 电子邮件 </td>
      <td> <a href="mailto:38306293@qq.com">38306293@qq.com</a> </td>
    </tr>
    <tr>
      <td> 账户余额 </td>
      <td> ¥6090.57 </td>
    </tr>
    <tr>
      <td> 消费积分 </td>
      <td> 14585 </td>
    </tr>
    <tr>
      <td> 等级积分 </td>
      <td> 14485 </td>
    </tr>
    <tr>
      <td> 会员等级 </td>
      <td> 钻石会员 </td>

    </tr>
    <tr>
      <td> 红包数量 </td>
      <td> 15 </td>
    </tr>
  </table>

    <table width="100%" border="0">
    <caption><strong> 收货人 : sdf </strong></caption>
    <tr>
      <td> 电子邮件 </td>
      <td> <a href="mailto:"></a> </td>
    </tr>
    <tr>
      <td> 地址 </td>
      <td> sdf  </td>
    </tr>
    <tr>
      <td> 邮编 </td>
      <td>  </td>
    </tr>
    <tr>
      <td> 电话 </td>
      <td> - </td>
    </tr>
    <tr>
      <td> 备用电话 </td>
      <td> 13245855555 </td>
    </tr>
  </table>
  </div>

<form action="order.php?act=operate" method="post" name="theForm">
<div class="list-div" style="margin-bottom: 5px">
<table width="100%" cellpadding="3" cellspacing="1">
  <tr>
    <td colspan="4">
      <div align="center">
        <input name="prev" type="button" class="button" onClick="location.href='order.php?act=info&order_id=140';" value="前一个订单"  />
        <input name="next" type="button" class="button" onClick="location.href='order.php?act=info&order_id=142';" value="后一个订单"  />
        <input type="button" onclick="window.open('order.php?act=info&order_id=141&print=1')" class="button" value="打印订单" />
    </div></td>
  </tr>
  <tr>
    <th colspan="4">基本信息</th>
  </tr>
  <tr>
    <td width="18%"><div align="right"><strong>订单号：</strong></div></td>
    <td width="34%">2016022457490</td>
    <td width="15%"><div align="right"><strong>订单状态：</strong></div></td>
    <td><font color="red">无效</font>,未付款,未发货</td>
  </tr>
  <tr>
    <td><div align="right"><strong>购货人：</strong></div></td>
    <td>yiren [ <a href="" onclick="staticbar();return false;">显示购货人信息</a> ] [ <a href="user_msg.php?act=add&order_id=141&user_id=6">发送/查看留言</a> ]</td>
    <td><div align="right"><strong>下单时间：</strong></div></td>
    <td>2016-02-24 13:10:01</td>
  </tr>
  <tr>
    <td><div align="right"><strong>支付方式：</strong></div></td>
    <td>支付宝<a href="order.php?act=edit&order_id=141&step=payment" class="special">编辑</a>
    (备注: <span onclick="listTable.edit(this, 'edit_pay_note', 141)">N/A</span>)</td>
    <td><div align="right"><strong>付款时间：</strong></div></td>
    <td>未付款</td>
  </tr>
  <tr>
    <td><div align="right"><strong>配送方式：</strong></div></td>
    <td><span id="shipping_name">顺丰速运</span><a href="order.php?act=edit&order_id=141&step=shipping" class="special">编辑</a>&nbsp;&nbsp;<input type="button" onclick="window.open('order.php?act=info&order_id=141&shipping_print=1')" class="button" value="打印快递单"> </td>
    <td><div align="right"><strong>发货时间：</strong></div></td>
    <td>未发货</td>
  </tr>
  <tr>
    <td><div align="right"><strong>发货单号：</strong></div></td>
    <td></td>
    <td><div align="right"><strong>订单来源：</strong></div></td>
    <td>网站自营</td>
  </tr>
   <!--增值税发票_添加_START_www.wrzc.net-->
  <!--普通发票显示内容-->
  <tr>
    <th colspan='4'>发票信息        <a href='order.php?act=edit&order_id=141&step=invoice&step_detail=edit' class='special'>添加</a>
        </th>
  </tr>
    <tr>
    <td colspan='4'><div align="center" style='height:50px;line-height:50px;'><strong>没有发票信息</strong></div></td>
  </tr>
    <!--增值税发票_添加_END_www.wrzc.net-->
  <!-- 代码增加_start   By www.wygk.cn -->
  <tr>
  	<td><div align="right"><strong>所选自提点：</strong></div></td>
    <td colspan="3">
    	店名：&nbsp;&nbsp;联系人：&nbsp;&nbsp;
        联系方式：&nbsp;&nbsp;地址：    </td>
  </tr>
  <!-- 代码增加_end   By www.wygk.cn -->
  <tr>
    <th colspan="4">其他信息<a href="order.php?act=edit&order_id=141&step=other" class="special">编辑</a></th>
    </tr>
 <!--增值税发票_删除_START_www.wrzc.net-->
  <!--<tr>
    <td><div align="right"><strong>发票类型：</strong></div></td>
    <td></td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td><div align="right"><strong>发票抬头：</strong></div></td>
    <td></td>
    <td><div align="right"><strong>发票内容：</strong></div></td>
    <td></td>
  </tr>-->
  <!--增值税发票_删除_END_www.wrzc.net-->
  <tr>
    <td><div align="right"><strong>客户给商家的留言：</strong></div></td>
    <td colspan="3"></td>
  </tr>
  <tr>
    <td><div align="right"><strong>缺货处理：</strong></div></td>
    <td>等待所有商品备齐后再发</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <!--包装、贺卡、贺卡祝福语_删除_START_www.wrzc.net-->
<!--   <tr>
    <td><div align="right"><strong>包装：</strong></div></td>
    <td></td>
    <td><div align="right"><strong>贺卡：</strong></div></td>
    <td></td>
  </tr>
  <tr>
    <td><div align="right"><strong>贺卡祝福语：</strong></div></td>
    <td colspan="3"></td>
  </tr> -->
  <!--包装、贺卡、贺卡祝福语_删除_END_www.wrzc.net-->
  <tr>
    <td><div align="right"><strong>商家给客户的留言：</strong></div></td>
    <td colspan="3"> </td>
  </tr>
  <tr>
    <th colspan="4">收货人信息<a href="order.php?act=edit&order_id=141&step=consignee" class="special">编辑</a></th>
    </tr>
  <tr>
    <td><div align="right"><strong>收货人：</strong></div></td>
    <td>sdf</td>
    <td><div align="right"><strong>电子邮件：</strong></div></td>
    <td>18039485645@163.com</td>
  </tr>
  <tr>
    <td><div align="right"><strong>地址：</strong></div></td>
    <td>[中国  河北  秦皇岛  海港区] sdf dsf 考虑到进水阀链接咖啡机</td>
    <td><div align="right"><strong>邮编：</strong></div></td>
    <td>123556</td>
  </tr>
  <tr>
    <td><div align="right"><strong>电话：</strong></div></td>
    <td>-</td>
    <td><div align="right"><strong>手机：</strong></div></td>
    <td>13245855555</td>
  </tr>
  <tr>
    <!--<td><div align="right"><strong>标志性建筑：</strong></div></td>
    <td></td>-->
    <td><div align="right"><strong>最佳送货时间：</strong></div></td>
    <td colspan="3">工作日/周末/假日均可</td>
  </tr>
</table>
</div>

<div class="list-div" style="margin-bottom: 5px">
<table width="100%" cellpadding="3" cellspacing="1">
  <tr>
    <th colspan="10" scope="col">商品信息<a href="order.php?act=edit&order_id=141&step=goods" class="special">编辑</a></th>
    </tr>
  <tr>
    <td scope="col" width="30%"><div align="center"><strong>商品名称 [ 品牌 ]</strong></div></td>
    <td scope="col"><div align="center"><strong>售后</strong></div></td>
    <td scope="col"><div align="center"><strong>货号</strong></div></td>
    <td scope="col"><div align="center"><strong>货品号</strong></div></td>
    <td scope="col"><div align="center"><strong>价格</strong></div></td>
	<td scope="col"><div align="center"><strong>手机专享价格</strong></div></td><!--手机专享价格 app jx -->
    <td scope="col"><div align="center"><strong>数量</strong></div></td>
    <td scope="col"><div align="center"><strong>属性</strong></div></td>
    <td scope="col"><div align="center"><strong>库存</strong></div></td>
    <td scope="col"><div align="center"><strong>小计</strong></div></td>
  </tr>
    <tr>
    <td>
        <a href="../goods.php?id=51" target="_blank">高端2015夏装新款修身淑坊女女装蕾丝短袖复女连衣裙装 [ 曼妮芬（ManniForm） ]        </a>
        </td>
    <td>正常</td>
    <td>WRZ000051</td>
    <td>WRZ000051g_p53</td>
    <td><div align="right">¥169.15</div></td>
	<td><div align="right">没有手机专享价格</div></td><!--手机专享价格 app jx -->
    <td><div align="right">1    </div></td>
    <td>颜色:棕色 <br />
尺码:M <br />
</td>
    <td><div align="right">555</div></td>
    <td><div align="right">¥169.15</div></td>
  </tr>
    <tr>
    <td>
        <a href="../goods.php?id=233" target="_blank">噜噜噜，让肌肤喝饱“水         </a>
        </td>
    <td>正常</td>
    <td>WRZ000233</td>
    <td></td>
    <td><div align="right">¥67.15</div></td>
	<td><div align="right">没有手机专享价格</div></td><!--手机专享价格 app jx -->
    <td><div align="right">1    </div></td>
    <td></td>
    <td><div align="right"></div></td>
    <td><div align="right">¥67.15</div></td>
  </tr>
    <tr>
    <td></td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td></td>
    <td></td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td><div align="right"><strong>合计：</strong></div></td>
    <td><div align="right">¥236.30</div></td>
  </tr>
</table>
</div>

<div class="list-div" style="margin-bottom: 5px">
<table width="100%" cellpadding="3" cellspacing="1">
  <tr>
    <th>费用信息<a href="order.php?act=edit&order_id=141&step=money" class="special">编辑</a></th>
  </tr>
  <tr>
    <td><div align="right">商品总金额：<strong>¥236.30</strong>
- 折扣：<strong>¥0.00</strong>     + 发票税额：<strong>¥0.00</strong>
      + 配送费用：<strong>¥0.00</strong>
<!--       + 保价费用：<strong>¥0.00</strong> -->
      + 支付费用：<strong>¥0.00</strong>
<!--      + 包装费用：<strong>¥0.00</strong>
      + 贺卡费用：<strong>¥0.00</strong>-->
      </div></td>
  <tr>
    <td><div align="right"> = 订单总金额：<strong>¥236.30</strong></div></td>
  </tr>
  <tr>
    <td><div align="right">
      - 已付款金额：<strong>¥0.00</strong> - 使用余额： <strong>¥0.00</strong>
      <!--- 使用积分： <strong>¥0.00</strong>-->
      - 使用红包： <strong>¥0.00</strong>
    </div></td>
  <tr>
    <td><div align="right"> = 应付款金额：<strong>¥236.30</strong>
      </div></td>
  </tr>
</table>
</div>

<div class="list-div" style="margin-bottom: 5px">
<table cellpadding="3" cellspacing="1">
  <tr>
    <th colspan="6">操作信息</th>
  </tr>
  <tr>
    <td><div align="right"><strong>操作备注：</strong></div></td>
  <td colspan="5"><textarea name="action_note" cols="80" rows="3"></textarea></td>
    </tr>
  <tr>
    <td><div align="right"></div>
      <div align="right"><strong>当前可执行操作：</strong> </div></td>
	<!-- 一键发货begin 修改 by www.wrzc.net  -->  
        <td colspan="5">
        <!-- 一键发货end 修改 by www.wrzc.net -->  
    <!-- 
    op_confirm-确认</br>
    op_pay-付款</br>
    op_unpay-设为未付款</br>
    op_prepare-配货</br>
    op_split-生成发货单</br>
    op_unship-未发货</br>
    op_receive-已收货</br>
    op_cancel-取消</br>
    op_invalid-无效</br>
     -->
          <input name="confirm" type="submit" value="确认" class="button" />
                   <input name="after_service" type="submit" value="售后" class="button" />        <input name="remove" type="submit" value="移除" class="button" onClick="return window.confirm('删除订单将清除该订单的所有信息。您确定要这么做吗？');" />
                                        <input name="order_id" type="hidden" value="141"></td>
    </tr>
  <tr>
    <th>操作者：</th>
    <th>操作时间</th>
    <th>订单状态</th>
    <th>付款状态</th>
    <th>发货状态</th>
    <th>备注</th>
  </tr>
    <tr>
    <td><div align="center">buyer</div></td>
    <td><div align="center">2016-02-24 17:14:12</div></td>
    <td><div align="center"><font color="red"> 取消</font></div></td>
    <td><div align="center">未付款</div></td>
    <td><div align="center">未发货</div></td>
    <td>用户取消</td>
  </tr>
  </table>
</div>
<div class="list-div" style="margin-bottom: 5px">
<!--  -->
</div>
</form>

<script language="JavaScript">

  var oldAgencyId = 0;

  onload = function()
  {
    // 开始检查订单
    startCheckOrder();
  }

  /**
   * 把订单指派给某办事处
   * @param int agencyId
   */
  function assignTo(agencyId)
  {
    if (agencyId == 0)
    {
      alert(pls_select_agency);
      return false;
    }
    if (oldAgencyId != 0 && agencyId == oldAgencyId)
    {
      alert(pls_select_other_agency);
      return false;
    }
    return true;
  }
</script>
<script language="javascript">
get_invoice_info('','',1);

function get_invoice_info(expressid,expressno,div_id)
{
	$("#ul_i").children("li").removeClass();
	document.getElementById("div_i_"+div_id).className = 'selected';
	
	if (expressid == "同城快递")
	{
		Ajax.call(
			'order.php?act=get_tc_express', 
			'expressno =' + expressno, 
			function(data){
				document.getElementById("retData").innerHTML='快递公司：'+expressid+' &nbsp; 运单号：'+expressno+'<br>';
				document.getElementById("retData").innerHTML+=data;
			}, 
			'POST', 
			'JSON',
			false
		);
	}
	else
	{
		Ajax.call(
			'../plugins/kuaidi100/kuaidi100_post.php?com='+ expressid+'&nu=' + expressno, 
			'showtest=showtest', 
			function(data){
				document.getElementById("retData").innerHTML='快递公司：'+expressid+' &nbsp; 运单号：'+expressno+'<br>';
				document.getElementById("retData").innerHTML+=data;
			}, 
			'GET', 
			'TEXT', 
			false
		);
	}
}
</script>


<div id="footer">
共执行 22 个查询，用时 0.048003 秒，Gzip 已禁用，内存占用 5.934 MB<br />
版权所有 &copy; 2008-2030 广州网软志成信息科技有限公司，并保留所有权利。</div>
<!-- 新订单提示信息 -->
<div id="popMsg">
  <table cellspacing="0" cellpadding="0" width="100%" bgcolor="#cfdef4" border="0">
  <tr>
    <td style="color: #0f2c8c" width="30" height="24"></td>
    <td style="font-weight: normal; color: #1f336b; padding-top: 4px;padding-left: 4px" valign="center" width="100%"> 新订单通知</td>
    <td style="padding-top: 2px;padding-right:2px" valign="center" align="right" width="19"><span title="关闭" style="cursor: hand;cursor:pointer;color:red;font-size:12px;font-weight:bold;margin-right:4px;" onclick="Message.close()" >×</span><!-- <img title=关闭 style="cursor: hand" onclick=closediv() hspace=3 src="msgclose.jpg"> --></td>
  </tr>
  <tr>
    <td style="padding-right: 1px; padding-bottom: 1px" colspan="3" height="70">
    <div id="popMsgContent">
      <p>您有 <strong style="color:#ff0000" id="spanNewOrder">1</strong> 个新订单以及       <strong style="color:#ff0000" id="spanNewPaid">0</strong> 个新付款的订单</p>
      <p align="center" style="word-break:break-all"><a href="order.php?act=list"><span style="color:#ff0000">点击查看新订单</span></a></p>
    </div>
    </td>
  </tr>
  </table>
</div>

<!--
<embed src="images/online.wav" width="0" height="0" autostart="false" name="msgBeep" id="msgBeep" enablejavascript="true"/>
-->
<object classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000" codebase="http://active.macromedia.com/flash2/cabs/swflash.cab#version=4,0,0,0" id="msgBeep" width="1" height="1">
  <param name="movie" value="images/online.swf">
  <param name="quality" value="high">
  <embed src="images/online.swf" name="msgBeep" id="msgBeep" quality="high" width="0" height="0" type="application/x-shockwave-flash" pluginspage="http://www.macromedia.com/shockwave/download/index.cgi?p1_prod_version=shockwaveflash">
  </embed>
</object>

<script language="JavaScript">
document.onmousemove=function(e)
{
  var obj = Utils.srcElement(e);
  if (typeof(obj.onclick) == 'function' && obj.onclick.toString().indexOf('listTable.edit') != -1)
  {
    obj.title = '点击修改内容';
    obj.style.cssText = 'background: #278296;';
    obj.onmouseout = function(e)
    {
      this.style.cssText = '';
    }
  }
  else if (typeof(obj.href) != 'undefined' && obj.href.indexOf('listTable.sort') != -1)
  {
    obj.title = '点击对列表排序';
  }
}
<!--


var MyTodolist;
function showTodoList(adminid)
{
  if(!MyTodolist)
  {
    var global = $import("../js/global.js","js");
    global.onload = global.onreadystatechange= function()
    {
      if(this.readyState && this.readyState=="loading")return;
      var md5 = $import("js/md5.js","js");
      md5.onload = md5.onreadystatechange= function()
      {
        if(this.readyState && this.readyState=="loading")return;
        var todolist = $import("js/todolist.js","js");
        todolist.onload = todolist.onreadystatechange = function()
        {
          if(this.readyState && this.readyState=="loading")return;
          MyTodolist = new Todolist();
          MyTodolist.show();
        }
      }
    }
  }
  else
  {
    if(MyTodolist.visibility)
    {
      MyTodolist.hide();
    }
    else
    {
      MyTodolist.show();
    }
  }
}

if (Browser.isIE)
{
  onscroll = function()
  {
    //document.getElementById('calculator').style.top = document.body.scrollTop;
    document.getElementById('popMsg').style.top = (document.body.scrollTop + document.body.clientHeight - document.getElementById('popMsg').offsetHeight) + "px";
  }
}

if (document.getElementById("listDiv"))
{
  document.getElementById("listDiv").onmouseover = function(e)
  {
    obj = Utils.srcElement(e);

    if (obj)
    {
      if (obj.parentNode.tagName.toLowerCase() == "tr") row = obj.parentNode;
      else if (obj.parentNode.parentNode.tagName.toLowerCase() == "tr") row = obj.parentNode.parentNode;
      else return;

      for (i = 0; i < row.cells.length; i++)
      {
        if (row.cells[i].tagName != "TH") row.cells[i].style.backgroundColor = '#F4FAFB';
      }
    }

  }

  document.getElementById("listDiv").onmouseout = function(e)
  {
    obj = Utils.srcElement(e);

    if (obj)
    {
      if (obj.parentNode.tagName.toLowerCase() == "tr") row = obj.parentNode;
      else if (obj.parentNode.parentNode.tagName.toLowerCase() == "tr") row = obj.parentNode.parentNode;
      else return;

      for (i = 0; i < row.cells.length; i++)
      {
          if (row.cells[i].tagName != "TH") row.cells[i].style.backgroundColor = '';
      }
    }
  }

  document.getElementById("listDiv").onclick = function(e)
  {
    var obj = Utils.srcElement(e);

    if (obj.tagName == "INPUT" && obj.type == "checkbox")
    {
      if (!document.forms['listForm'])
      {
        return;
      }
      var nodes = document.forms['listForm'].elements;
      var checked = false;

      for (i = 0; i < nodes.length; i++)
      {
        if (nodes[i].checked)
        {
           checked = true;
           break;
         }
      }

      if(document.getElementById("btnSubmit"))
      {
        document.getElementById("btnSubmit").disabled = !checked;
      }
      for (i = 1; i <= 10; i++)
      {
        if (document.getElementById("btnSubmit" + i))
        {
          document.getElementById("btnSubmit" + i).disabled = !checked;
        }
      }
    }
  }

}

//-->
</script>
</body>
</html>

<!-- $Id: merge_order.htm 14216 2008-03-10 02:27:21Z testyang $ -->
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>管理中心 - 合并订单 </title>
<meta name="robots" content="noindex, nofollow">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link href="styles/general.css" rel="stylesheet" type="text/css" />
<link href="styles/main.css" rel="stylesheet" type="text/css" />
<link href="styles/chosen/chosen.css" rel="stylesheet" type="text/css">
<script type="text/javascript" src="js/jquery-1.6.2.min.js"></script><script type="text/javascript" src="js/chosen.jquery.min.js"></script><script type="text/javascript" src="js/jquery.json.js"></script><script type="text/javascript" src="../js/transport.js"></script><script type="text/javascript" src="js/common.js"></script><script language="JavaScript">
<!--
// 这里把JS用到的所有语言都赋值到这里
var process_request = "正在处理您的请求...";
var todolist_caption = "记事本";
var todolist_autosave = "自动保存";
var todolist_save = "保存";
var todolist_clear = "清除";
var todolist_confirm_save = "是否将更改保存到记事本？";
var todolist_confirm_clear = "是否清空内容？";
var remove_confirm = "删除订单将清除该订单的所有信息。您确定要这么做吗？";
var confirm_merge = "您确实要合并这两个订单吗？";
var input_price = "自定义价格";
var pls_search_user = "请搜索并选择会员";
var confirm_drop = "确认要删除该商品吗？";
var invalid_goods_number = "商品数量不正确";
var pls_search_goods = "请搜索并选择商品";
var pls_select_area = "请完整选择所在地区";
var pls_select_shipping = "请选择配送方式";
var pls_select_payment = "请选择支付方式";
var pls_select_pack = "请选择包装";
var pls_select_card = "请选择贺卡";
var pls_input_note = "请您填写备注！";
var pls_input_cancel = "请您填写取消原因！";
var pls_select_refund = "请选择退款方式！";
var pls_select_agency = "请选择办事处！";
var pls_select_other_agency = "该订单现在就属于这个办事处，请选择其他办事处！";
var loading = "加载中...";
var remove_invoice_confirm = "您确定删除该订单的发票信息吗？";
//-->
</script>
</head>
<body>

<div id="menu_list" onmouseover="show_popup()" onmouseout="hide_popup()">
<ul>
<li><a href="goods.php?act=add" target="main_frame">添加新商品</a></li>
<li><a href="category.php?act=add" target="main_frame">添加商品分类</a></li>
<li><a href="order.php?act=add" target="main_frame">添加订单</a></li>
<li><a href="article.php?act=add" target="main_frame">添加新文章</a></li>
<li><a href="users.php?act=add" target="main_frame">添加会员</a></li>
</ul>
</div>
<script>
function show_popup(){
frmBody = parent.document.getElementById('frame-body');
if (frmBody.cols == "37, 12, *")
{
parent.main_frame.document.getElementById('menu_list').style.left = '195px';
}
else
{
parent.main_frame.document.getElementById('menu_list').style.left = '40px';
}
parent.main_frame.document.getElementById('menu_list').style.display = 'block';
}
function hide_popup(){

parent.main_frame.document.getElementById('menu_list').style.display = 'none';
}
</script>
<h1>
<span class="action-span"><a href="order.php?act=list">订单列表</a></span>
<span class="action-span1"><a href="index.php?act=main">管理中心</a> </span><span id="search_id" class="action-span1"> - 合并订单 </span>
<div style="clear:both"></div>
</h1>
<script type="text/javascript" src="js/validator.js"></script><div class="main-div">
<table cellspacing="1" cellpadding="3" width="100%">
  <tr>
    <td class="label"><a href="javascript:showNotice('noticeOrderSn');" title="点击此处查看提示信息"><img src="images/notice.gif" width="16" height="16" border="0" alt="点击此处查看提示信息"></a>主订单：</td>
    <td><input name="to_order_sn" type="text" id="to_order_sn">
      <select name="to_list" id="to_list" onchange="if (this.value != '') document.getElementById('to_order_sn').value = this.value;">
      <option value="">请选择...</option>
            </select>
      <span class="notice-span" style="display:block"  id="noticeOrderSn">当两个订单不一致时，合并后的订单信息（如：支付方式、配送方式、包装、贺卡、红包等）以主订单为准。</span></td>
  </tr>
  <tr>
    <td class="label">从订单：</td>
    <td><input name="from_order_sn" type="text" id="from_order_sn">
      <select name="from_list" onchange="if (this.value != '') document.getElementById('from_order_sn').value = this.value;">
      <option value="">请选择...</option>
            </select></td>
  </tr>
  <tr>
    <td colspan="2"><div align="center">
      <input name="merge" type="button" id="merge" value="合并" class="button" onclick="if (confirm(confirm_merge)) merge()" />
    </div></td>
    </tr>
</table>
</div>

<script language="JavaScript">
    /**
     * 合并
     */
    function merge()
    {
        var fromOrderSn = document.getElementById('from_order_sn').value;
        var toOrderSn = document.getElementById('to_order_sn').value;
        Ajax.call('order.php?is_ajax=1&act=ajax_merge_order','from_order_sn=o' + fromOrderSn + '&to_order_sn=o' + toOrderSn, mergeResponse, 'POST', 'JSON');
    }

    function mergeResponse(result)
    {
      if (result.message.length > 0)
      {
        alert(result.message);
      }
      if (result.error == 0)
      {
        //成功则清除用户填写信息
        document.getElementById('from_order_sn').value = '';
        document.getElementById('to_order_sn').value = '';
        location.reload();
      }
    }

    onload = function()
    {
        // 开始检查订单
        startCheckOrder();
    }
</script>

<div id="footer">
共执行 2 个查询，用时 0.016001 秒，Gzip 已禁用，内存占用 5.881 MB<br />
版权所有 &copy; 2008-2030 广州网软志成信息科技有限公司，并保留所有权利。</div>
<script type="text/javascript" src="../js/utils.js"></script><!-- 新订单提示信息 -->
<div id="popMsg">
  <table cellspacing="0" cellpadding="0" width="100%" bgcolor="#cfdef4" border="0">
  <tr>
    <td style="color: #0f2c8c" width="30" height="24"></td>
    <td style="font-weight: normal; color: #1f336b; padding-top: 4px;padding-left: 4px" valign="center" width="100%"> 新订单通知</td>
    <td style="padding-top: 2px;padding-right:2px" valign="center" align="right" width="19"><span title="关闭" style="cursor: hand;cursor:pointer;color:red;font-size:12px;font-weight:bold;margin-right:4px;" onclick="Message.close()" >×</span><!-- <img title=关闭 style="cursor: hand" onclick=closediv() hspace=3 src="msgclose.jpg"> --></td>
  </tr>
  <tr>
    <td style="padding-right: 1px; padding-bottom: 1px" colspan="3" height="70">
    <div id="popMsgContent">
      <p>您有 <strong style="color:#ff0000" id="spanNewOrder">1</strong> 个新订单以及       <strong style="color:#ff0000" id="spanNewPaid">0</strong> 个新付款的订单</p>
      <p align="center" style="word-break:break-all"><a href="order.php?act=list"><span style="color:#ff0000">点击查看新订单</span></a></p>
    </div>
    </td>
  </tr>
  </table>
</div>

<!--
<embed src="images/online.wav" width="0" height="0" autostart="false" name="msgBeep" id="msgBeep" enablejavascript="true"/>
-->
<object classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000" codebase="http://active.macromedia.com/flash2/cabs/swflash.cab#version=4,0,0,0" id="msgBeep" width="1" height="1">
  <param name="movie" value="images/online.swf">
  <param name="quality" value="high">
  <embed src="images/online.swf" name="msgBeep" id="msgBeep" quality="high" width="0" height="0" type="application/x-shockwave-flash" pluginspage="http://www.macromedia.com/shockwave/download/index.cgi?p1_prod_version=shockwaveflash">
  </embed>
</object>

<script language="JavaScript">
document.onmousemove=function(e)
{
  var obj = Utils.srcElement(e);
  if (typeof(obj.onclick) == 'function' && obj.onclick.toString().indexOf('listTable.edit') != -1)
  {
    obj.title = '点击修改内容';
    obj.style.cssText = 'background: #278296;';
    obj.onmouseout = function(e)
    {
      this.style.cssText = '';
    }
  }
  else if (typeof(obj.href) != 'undefined' && obj.href.indexOf('listTable.sort') != -1)
  {
    obj.title = '点击对列表排序';
  }
}
<!--


var MyTodolist;
function showTodoList(adminid)
{
  if(!MyTodolist)
  {
    var global = $import("../js/global.js","js");
    global.onload = global.onreadystatechange= function()
    {
      if(this.readyState && this.readyState=="loading")return;
      var md5 = $import("js/md5.js","js");
      md5.onload = md5.onreadystatechange= function()
      {
        if(this.readyState && this.readyState=="loading")return;
        var todolist = $import("js/todolist.js","js");
        todolist.onload = todolist.onreadystatechange = function()
        {
          if(this.readyState && this.readyState=="loading")return;
          MyTodolist = new Todolist();
          MyTodolist.show();
        }
      }
    }
  }
  else
  {
    if(MyTodolist.visibility)
    {
      MyTodolist.hide();
    }
    else
    {
      MyTodolist.show();
    }
  }
}

if (Browser.isIE)
{
  onscroll = function()
  {
    //document.getElementById('calculator').style.top = document.body.scrollTop;
    document.getElementById('popMsg').style.top = (document.body.scrollTop + document.body.clientHeight - document.getElementById('popMsg').offsetHeight) + "px";
  }
}

if (document.getElementById("listDiv"))
{
  document.getElementById("listDiv").onmouseover = function(e)
  {
    obj = Utils.srcElement(e);

    if (obj)
    {
      if (obj.parentNode.tagName.toLowerCase() == "tr") row = obj.parentNode;
      else if (obj.parentNode.parentNode.tagName.toLowerCase() == "tr") row = obj.parentNode.parentNode;
      else return;

      for (i = 0; i < row.cells.length; i++)
      {
        if (row.cells[i].tagName != "TH") row.cells[i].style.backgroundColor = '#F4FAFB';
      }
    }

  }

  document.getElementById("listDiv").onmouseout = function(e)
  {
    obj = Utils.srcElement(e);

    if (obj)
    {
      if (obj.parentNode.tagName.toLowerCase() == "tr") row = obj.parentNode;
      else if (obj.parentNode.parentNode.tagName.toLowerCase() == "tr") row = obj.parentNode.parentNode;
      else return;

      for (i = 0; i < row.cells.length; i++)
      {
          if (row.cells[i].tagName != "TH") row.cells[i].style.backgroundColor = '';
      }
    }
  }

  document.getElementById("listDiv").onclick = function(e)
  {
    var obj = Utils.srcElement(e);

    if (obj.tagName == "INPUT" && obj.type == "checkbox")
    {
      if (!document.forms['listForm'])
      {
        return;
      }
      var nodes = document.forms['listForm'].elements;
      var checked = false;

      for (i = 0; i < nodes.length; i++)
      {
        if (nodes[i].checked)
        {
           checked = true;
           break;
         }
      }

      if(document.getElementById("btnSubmit"))
      {
        document.getElementById("btnSubmit").disabled = !checked;
      }
      for (i = 1; i <= 10; i++)
      {
        if (document.getElementById("btnSubmit" + i))
        {
          document.getElementById("btnSubmit" + i).disabled = !checked;
        }
      }
    }
  }

}

//-->
</script>
</body>
</html>

<!-- $Id: order_templates.htm 14216 2008-03-10 02:27:21Z testyang $ -->
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>管理中心 - 编辑订单打印模板 </title>
<meta name="robots" content="noindex, nofollow">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link href="styles/general.css" rel="stylesheet" type="text/css" />
<link href="styles/main.css" rel="stylesheet" type="text/css" />
<link href="styles/chosen/chosen.css" rel="stylesheet" type="text/css">
<script type="text/javascript" src="js/jquery-1.6.2.min.js"></script><script type="text/javascript" src="js/chosen.jquery.min.js"></script><script type="text/javascript" src="js/jquery.json.js"></script><script type="text/javascript" src="../js/transport.js"></script><script type="text/javascript" src="js/common.js"></script><script language="JavaScript">
<!--
// 这里把JS用到的所有语言都赋值到这里
var process_request = "正在处理您的请求...";
var todolist_caption = "记事本";
var todolist_autosave = "自动保存";
var todolist_save = "保存";
var todolist_clear = "清除";
var todolist_confirm_save = "是否将更改保存到记事本？";
var todolist_confirm_clear = "是否清空内容？";
var remove_confirm = "删除订单将清除该订单的所有信息。您确定要这么做吗？";
var confirm_merge = "您确实要合并这两个订单吗？";
var input_price = "自定义价格";
var pls_search_user = "请搜索并选择会员";
var confirm_drop = "确认要删除该商品吗？";
var invalid_goods_number = "商品数量不正确";
var pls_search_goods = "请搜索并选择商品";
var pls_select_area = "请完整选择所在地区";
var pls_select_shipping = "请选择配送方式";
var pls_select_payment = "请选择支付方式";
var pls_select_pack = "请选择包装";
var pls_select_card = "请选择贺卡";
var pls_input_note = "请您填写备注！";
var pls_input_cancel = "请您填写取消原因！";
var pls_select_refund = "请选择退款方式！";
var pls_select_agency = "请选择办事处！";
var pls_select_other_agency = "该订单现在就属于这个办事处，请选择其他办事处！";
var loading = "加载中...";
var remove_invoice_confirm = "您确定删除该订单的发票信息吗？";
//-->
</script>
</head>
<body>

<div id="menu_list" onmouseover="show_popup()" onmouseout="hide_popup()">
<ul>
<li><a href="goods.php?act=add" target="main_frame">添加新商品</a></li>
<li><a href="category.php?act=add" target="main_frame">添加商品分类</a></li>
<li><a href="order.php?act=add" target="main_frame">添加订单</a></li>
<li><a href="article.php?act=add" target="main_frame">添加新文章</a></li>
<li><a href="users.php?act=add" target="main_frame">添加会员</a></li>
</ul>
</div>
<script>
function show_popup(){
frmBody = parent.document.getElementById('frame-body');
if (frmBody.cols == "37, 12, *")
{
parent.main_frame.document.getElementById('menu_list').style.left = '195px';
}
else
{
parent.main_frame.document.getElementById('menu_list').style.left = '40px';
}
parent.main_frame.document.getElementById('menu_list').style.display = 'block';
}
function hide_popup(){

parent.main_frame.document.getElementById('menu_list').style.display = 'none';
}
</script>
<h1>
<span class="action-span"><a href="order.php?act=list">订单列表</a></span>
<span class="action-span1"><a href="index.php?act=main">管理中心</a> </span><span id="search_id" class="action-span1"> - 编辑订单打印模板 </span>
<div style="clear:both"></div>
</h1>
<form action="order.php" method="post">
<div class="main-div">
    <table width="100%">
     <tr><td><input type="hidden" id="FCKeditor1" name="FCKeditor1" value="&lt;p&gt;{literal} &lt;style type=&quot;text/css&quot;&gt;
body,td { font-size:13px; }
&lt;/style&gt; {/literal}&lt;/p&gt;
&lt;h1 align=&quot;center&quot;&gt;{$lang.order_info}&lt;/h1&gt;
&lt;table cellpadding=&quot;1&quot; width=&quot;100%&quot;&gt;
    &lt;tbody&gt;
        &lt;tr&gt;
            &lt;td width=&quot;8%&quot;&gt;{$lang.print_buy_name}&lt;/td&gt;
            &lt;td&gt;{if $order.user_name}{$order.user_name}{else}{$lang.anonymous}{/if}&lt;!-- 购货人姓名 --&gt;&lt;/td&gt;
            &lt;td align=&quot;right&quot;&gt;{$lang.label_order_time}&lt;/td&gt;
            &lt;td&gt;{$order.order_time}&lt;!-- 下订单时间 --&gt;&lt;/td&gt;
            &lt;td align=&quot;right&quot;&gt;{$lang.label_payment}&lt;/td&gt;
            &lt;td&gt;{$order.pay_name}&lt;!-- 支付方式 --&gt;&lt;/td&gt;
            &lt;td align=&quot;right&quot;&gt;{$lang.print_order_sn}&lt;/td&gt;
            &lt;td&gt;{$order.order_sn}&lt;!-- 订单号 --&gt;&lt;/td&gt;
        &lt;/tr&gt;
        &lt;tr&gt;
            &lt;td&gt;{$lang.label_pay_time}&lt;/td&gt;
            &lt;td&gt;{$order.pay_time}&lt;/td&gt;
            &lt;!-- 付款时间 --&gt;
            &lt;td align=&quot;right&quot;&gt;{$lang.label_shipping_time}&lt;/td&gt;
            &lt;td&gt;{$order.shipping_time}&lt;!-- 发货时间 --&gt;&lt;/td&gt;
            &lt;td align=&quot;right&quot;&gt;{$lang.label_shipping}&lt;/td&gt;
            &lt;td&gt;{$order.shipping_name}&lt;!-- 配送方式 --&gt;&lt;/td&gt;
            &lt;td align=&quot;right&quot;&gt;{$lang.label_invoice_no}&lt;/td&gt;
            &lt;td&gt;{$order.invoice_no} &lt;!-- 发货单号 --&gt;&lt;/td&gt;
        &lt;/tr&gt;
        &lt;tr&gt;
            &lt;td&gt;{$lang.label_consignee_address}&lt;/td&gt;
            &lt;td colspan=&quot;7&quot;&gt;[{$order.region}]&amp;nbsp;{$order.address}&amp;nbsp;&lt;!-- 收货人地址 --&gt;         {$lang.label_consignee}{$order.consignee}&amp;nbsp;&lt;!-- 收货人姓名 --&gt;         {if $order.zipcode}{$lang.label_zipcode}{$order.zipcode}&amp;nbsp;{/if}&lt;!-- 邮政编码 --&gt;         {if $order.tel}{$lang.label_tel}{$order.tel}&amp;nbsp; {/if}&lt;!-- 联系电话 --&gt;         {if $order.mobile}{$lang.label_mobile}{$order.mobile}{/if}&lt;!-- 手机号码 --&gt;&lt;/td&gt;
        &lt;/tr&gt;
    &lt;/tbody&gt;
&lt;/table&gt;
&lt;table border=&quot;1&quot; width=&quot;100%&quot; style=&quot;border-collapse:collapse;border-color:#000;&quot;&gt;
    &lt;tbody&gt;
        &lt;tr align=&quot;center&quot;&gt;
            &lt;td bgcolor=&quot;#cccccc&quot;&gt;{$lang.goods_name}  &lt;!-- 商品名称 --&gt;&lt;/td&gt;
            &lt;td bgcolor=&quot;#cccccc&quot;&gt;{$lang.goods_sn}    &lt;!-- 商品货号 --&gt;&lt;/td&gt;
            &lt;td bgcolor=&quot;#cccccc&quot;&gt;{$lang.goods_attr}  &lt;!-- 商品属性 --&gt;&lt;/td&gt;
            &lt;td bgcolor=&quot;#cccccc&quot;&gt;{$lang.goods_price} &lt;!-- 商品单价 --&gt;&lt;/td&gt;
            &lt;td bgcolor=&quot;#cccccc&quot;&gt;{$lang.goods_number}&lt;!-- 商品数量 --&gt;&lt;/td&gt;
            &lt;td bgcolor=&quot;#cccccc&quot;&gt;{$lang.subtotal}    &lt;!-- 价格小计 --&gt;&lt;/td&gt;
        &lt;/tr&gt;
        &lt;!-- {foreach from=$goods_list item=goods key=key} --&gt;
        &lt;tr&gt;
            &lt;td&gt;&amp;nbsp;{$goods.goods_name}&lt;!-- 商品名称 --&gt;         {if $goods.is_gift}{if $goods.goods_price gt 0}{$lang.remark_favourable}{else}{$lang.remark_gift}{/if}{/if}         {if $goods.parent_id gt 0}{$lang.remark_fittings}{/if}&lt;/td&gt;
            &lt;td&gt;&amp;nbsp;{$goods.goods_sn} &lt;!-- 商品货号 --&gt;&lt;/td&gt;
            &lt;td&gt;&lt;!-- 商品属性 --&gt;         &lt;!-- {foreach key=key from=$goods_attr[$key] item=attr} --&gt;         &lt;!-- {if $attr.name} --&gt; {$attr.name}:{$attr.value} &lt;!-- {/if} --&gt;         &lt;!-- {/foreach} --&gt;&lt;/td&gt;
            &lt;td align=&quot;right&quot;&gt;{$goods.formated_goods_price}&amp;nbsp;&lt;!-- 商品单价 --&gt;&lt;/td&gt;
            &lt;td align=&quot;right&quot;&gt;{$goods.goods_number}&amp;nbsp;&lt;!-- 商品数量 --&gt;&lt;/td&gt;
            &lt;td align=&quot;right&quot;&gt;{$goods.formated_subtotal}&amp;nbsp;&lt;!-- 商品金额小计 --&gt;&lt;/td&gt;
        &lt;/tr&gt;
        &lt;!-- {/foreach} --&gt;
        &lt;tr&gt;
            &lt;!-- 发票抬头和发票内容 --&gt;
            &lt;td colspan=&quot;4&quot;&gt;{if $order.inv_payee}         {$lang.label_inv_payee}{$order.inv_payee}&amp;nbsp;&amp;nbsp;&amp;nbsp;         {$lang.label_inv_content}{$order.inv_content}         {/if}&lt;/td&gt;
            &lt;!-- 商品总金额 --&gt;
            &lt;td align=&quot;right&quot; colspan=&quot;2&quot;&gt;{$lang.label_goods_amount}{$order.formated_goods_amount}&lt;/td&gt;
        &lt;/tr&gt;
    &lt;/tbody&gt;
&lt;/table&gt;
&lt;table border=&quot;0&quot; width=&quot;100%&quot;&gt;
    &lt;tbody&gt;
        &lt;tr align=&quot;right&quot;&gt;
            &lt;td&gt;{if $order.discount gt 0}- {$lang.label_discount}{$order.formated_discount}{/if}{if $order.pack_name and $order.pack_fee neq '0.00'}          &lt;!-- 包装名称包装费用 --&gt;         + {$lang.label_pack_fee}{$order.formated_pack_fee}         {/if}         {if $order.card_name and $order.card_fee neq '0.00'}&lt;!-- 贺卡名称以及贺卡费用 --&gt;         + {$lang.label_card_fee}{$order.formated_card_fee}         {/if}         {if $order.pay_fee neq '0.00'}&lt;!-- 支付手续费 --&gt;         + {$lang.label_pay_fee}{$order.formated_pay_fee}         {/if}         {if $order.shipping_fee neq '0.00'}&lt;!-- 配送费用 --&gt;         + {$lang.label_shipping_fee}{$order.formated_shipping_fee}         {/if}         {if $order.insure_fee neq '0.00'}&lt;!-- 保价费用 --&gt;         + {$lang.label_insure_fee}{$order.formated_insure_fee}         {/if}         &lt;!-- 订单总金额 --&gt;         = {$lang.label_order_amount}{$order.formated_total_fee}&lt;/td&gt;
        &lt;/tr&gt;
        &lt;tr align=&quot;right&quot;&gt;
            &lt;td&gt;&lt;!-- 如果已付了部分款项, 减去已付款金额 --&gt;         {if $order.money_paid neq '0.00'}- {$lang.label_money_paid}{$order.formated_money_paid}{/if}          &lt;!-- 如果使用了余额支付, 减去已使用的余额 --&gt;         {if $order.surplus neq '0.00'}- {$lang.label_surplus}{$order.formated_surplus}{/if}          &lt;!-- 如果使用了积分支付, 减去已使用的积分 --&gt;         {if $order.integral_money neq '0.00'}- {$lang.label_integral}{$order.formated_integral_money}{/if}          &lt;!-- 如果使用了红包支付, 减去已使用的红包 --&gt;         {if $order.bonus neq '0.00'}- {$lang.label_bonus}{$order.formated_bonus}{/if}          &lt;!-- 应付款金额 --&gt;         = {$lang.label_money_dues}{$order.formated_order_amount}&lt;/td&gt;
        &lt;/tr&gt;
    &lt;/tbody&gt;
&lt;/table&gt;
&lt;p&gt;{if $order.to_buyer}          {/if}     {if $order.invoice_note}          {/if}     {if $order.pay_note}          {/if}&lt;/p&gt;
&lt;table border=&quot;0&quot; width=&quot;100%&quot;&gt;
    &lt;tbody&gt;
        &lt;tr&gt;
            &lt;!-- 给购货人看的备注信息 --&gt;
            &lt;td&gt;{$lang.label_to_buyer}{$order.to_buyer}&lt;/td&gt;
        &lt;/tr&gt;
        &lt;tr&gt;
            &lt;!-- 发货备注 --&gt;
            &lt;td&gt;{$lang.label_invoice_note} {$order.invoice_note}&lt;/td&gt;
        &lt;/tr&gt;
        &lt;tr&gt;
            &lt;!-- 支付备注 --&gt;
            &lt;td&gt;{$lang.pay_note} {$order.pay_note}&lt;/td&gt;
        &lt;/tr&gt;
        &lt;tr&gt;
            &lt;!-- 网店名称, 网店地址, 网店URL以及联系电话 --&gt;
            &lt;td&gt;{$shop_name}（{$shop_url}）         {$lang.label_shop_address}{$shop_address}&amp;nbsp;&amp;nbsp;{$lang.label_service_phone}{$service_phone}&lt;/td&gt;
        &lt;/tr&gt;
        &lt;tr align=&quot;right&quot;&gt;
            &lt;!-- 订单操作员以及订单打印的日期 --&gt;
            &lt;td&gt;{$lang.label_print_time}{$print_time}&amp;nbsp;&amp;nbsp;&amp;nbsp;{$lang.action_user}{$action_user}&lt;/td&gt;
        &lt;/tr&gt;
    &lt;/tbody&gt;
&lt;/table&gt;" style="display:none" /><input type="hidden" id="FCKeditor1___Config" value="" style="display:none" /><iframe id="FCKeditor1___Frame" src="../includes/fckeditor/editor/fckeditor.html?InstanceName=FCKeditor1&amp;Toolbar=Normal" width="95%" height="500" frameborder="0" scrolling="no"></iframe></td></tr>
    </table>
    <div class="button-div ">
    <input type="hidden" name="act" value="edit_templates" />
    <input type="submit" value=" 确定 " class="button" />
  </div>
</div>
</form>
<script type="Text/Javascript" language="JavaScript">
<!--

onload = function()
{
  // 开始检查订单
  startCheckOrder();
}

//-->
</script>
<div id="footer">
共执行 1 个查询，用时 0.086005 秒，Gzip 已禁用，内存占用 5.931 MB<br />
版权所有 &copy; 2008-2030 广州网软志成信息科技有限公司，并保留所有权利。</div>
<script type="text/javascript" src="../js/utils.js"></script><!-- 新订单提示信息 -->
<div id="popMsg">
  <table cellspacing="0" cellpadding="0" width="100%" bgcolor="#cfdef4" border="0">
  <tr>
    <td style="color: #0f2c8c" width="30" height="24"></td>
    <td style="font-weight: normal; color: #1f336b; padding-top: 4px;padding-left: 4px" valign="center" width="100%"> 新订单通知</td>
    <td style="padding-top: 2px;padding-right:2px" valign="center" align="right" width="19"><span title="关闭" style="cursor: hand;cursor:pointer;color:red;font-size:12px;font-weight:bold;margin-right:4px;" onclick="Message.close()" >×</span><!-- <img title=关闭 style="cursor: hand" onclick=closediv() hspace=3 src="msgclose.jpg"> --></td>
  </tr>
  <tr>
    <td style="padding-right: 1px; padding-bottom: 1px" colspan="3" height="70">
    <div id="popMsgContent">
      <p>您有 <strong style="color:#ff0000" id="spanNewOrder">1</strong> 个新订单以及       <strong style="color:#ff0000" id="spanNewPaid">0</strong> 个新付款的订单</p>
      <p align="center" style="word-break:break-all"><a href="order.php?act=list"><span style="color:#ff0000">点击查看新订单</span></a></p>
    </div>
    </td>
  </tr>
  </table>
</div>

<!--
<embed src="images/online.wav" width="0" height="0" autostart="false" name="msgBeep" id="msgBeep" enablejavascript="true"/>
-->
<object classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000" codebase="http://active.macromedia.com/flash2/cabs/swflash.cab#version=4,0,0,0" id="msgBeep" width="1" height="1">
  <param name="movie" value="images/online.swf">
  <param name="quality" value="high">
  <embed src="images/online.swf" name="msgBeep" id="msgBeep" quality="high" width="0" height="0" type="application/x-shockwave-flash" pluginspage="http://www.macromedia.com/shockwave/download/index.cgi?p1_prod_version=shockwaveflash">
  </embed>
</object>

<script language="JavaScript">
document.onmousemove=function(e)
{
  var obj = Utils.srcElement(e);
  if (typeof(obj.onclick) == 'function' && obj.onclick.toString().indexOf('listTable.edit') != -1)
  {
    obj.title = '点击修改内容';
    obj.style.cssText = 'background: #278296;';
    obj.onmouseout = function(e)
    {
      this.style.cssText = '';
    }
  }
  else if (typeof(obj.href) != 'undefined' && obj.href.indexOf('listTable.sort') != -1)
  {
    obj.title = '点击对列表排序';
  }
}
<!--


var MyTodolist;
function showTodoList(adminid)
{
  if(!MyTodolist)
  {
    var global = $import("../js/global.js","js");
    global.onload = global.onreadystatechange= function()
    {
      if(this.readyState && this.readyState=="loading")return;
      var md5 = $import("js/md5.js","js");
      md5.onload = md5.onreadystatechange= function()
      {
        if(this.readyState && this.readyState=="loading")return;
        var todolist = $import("js/todolist.js","js");
        todolist.onload = todolist.onreadystatechange = function()
        {
          if(this.readyState && this.readyState=="loading")return;
          MyTodolist = new Todolist();
          MyTodolist.show();
        }
      }
    }
  }
  else
  {
    if(MyTodolist.visibility)
    {
      MyTodolist.hide();
    }
    else
    {
      MyTodolist.show();
    }
  }
}

if (Browser.isIE)
{
  onscroll = function()
  {
    //document.getElementById('calculator').style.top = document.body.scrollTop;
    document.getElementById('popMsg').style.top = (document.body.scrollTop + document.body.clientHeight - document.getElementById('popMsg').offsetHeight) + "px";
  }
}

if (document.getElementById("listDiv"))
{
  document.getElementById("listDiv").onmouseover = function(e)
  {
    obj = Utils.srcElement(e);

    if (obj)
    {
      if (obj.parentNode.tagName.toLowerCase() == "tr") row = obj.parentNode;
      else if (obj.parentNode.parentNode.tagName.toLowerCase() == "tr") row = obj.parentNode.parentNode;
      else return;

      for (i = 0; i < row.cells.length; i++)
      {
        if (row.cells[i].tagName != "TH") row.cells[i].style.backgroundColor = '#F4FAFB';
      }
    }

  }

  document.getElementById("listDiv").onmouseout = function(e)
  {
    obj = Utils.srcElement(e);

    if (obj)
    {
      if (obj.parentNode.tagName.toLowerCase() == "tr") row = obj.parentNode;
      else if (obj.parentNode.parentNode.tagName.toLowerCase() == "tr") row = obj.parentNode.parentNode;
      else return;

      for (i = 0; i < row.cells.length; i++)
      {
          if (row.cells[i].tagName != "TH") row.cells[i].style.backgroundColor = '';
      }
    }
  }

  document.getElementById("listDiv").onclick = function(e)
  {
    var obj = Utils.srcElement(e);

    if (obj.tagName == "INPUT" && obj.type == "checkbox")
    {
      if (!document.forms['listForm'])
      {
        return;
      }
      var nodes = document.forms['listForm'].elements;
      var checked = false;

      for (i = 0; i < nodes.length; i++)
      {
        if (nodes[i].checked)
        {
           checked = true;
           break;
         }
      }

      if(document.getElementById("btnSubmit"))
      {
        document.getElementById("btnSubmit").disabled = !checked;
      }
      for (i = 1; i <= 10; i++)
      {
        if (document.getElementById("btnSubmit" + i))
        {
          document.getElementById("btnSubmit" + i).disabled = !checked;
        }
      }
    }
  }

}

//-->
</script>
</body>
</html>

<!-- $Id -->

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>管理中心 - 添加订单 </title>
<meta name="robots" content="noindex, nofollow">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link href="styles/general.css" rel="stylesheet" type="text/css" />
<link href="styles/main.css" rel="stylesheet" type="text/css" />
<link href="styles/chosen/chosen.css" rel="stylesheet" type="text/css">
<script type="text/javascript" src="js/jquery-1.6.2.min.js"></script><script type="text/javascript" src="js/chosen.jquery.min.js"></script><script type="text/javascript" src="js/jquery.json.js"></script><script type="text/javascript" src="../js/transport.js"></script><script type="text/javascript" src="js/common.js"></script><script language="JavaScript">
<!--
// 这里把JS用到的所有语言都赋值到这里
var process_request = "正在处理您的请求...";
var todolist_caption = "记事本";
var todolist_autosave = "自动保存";
var todolist_save = "保存";
var todolist_clear = "清除";
var todolist_confirm_save = "是否将更改保存到记事本？";
var todolist_confirm_clear = "是否清空内容？";
var remove_confirm = "删除订单将清除该订单的所有信息。您确定要这么做吗？";
var confirm_merge = "您确实要合并这两个订单吗？";
var input_price = "自定义价格";
var pls_search_user = "请搜索并选择会员";
var confirm_drop = "确认要删除该商品吗？";
var invalid_goods_number = "商品数量不正确";
var pls_search_goods = "请搜索并选择商品";
var pls_select_area = "请完整选择所在地区";
var pls_select_shipping = "请选择配送方式";
var pls_select_payment = "请选择支付方式";
var pls_select_pack = "请选择包装";
var pls_select_card = "请选择贺卡";
var pls_input_note = "请您填写备注！";
var pls_input_cancel = "请您填写取消原因！";
var pls_select_refund = "请选择退款方式！";
var pls_select_agency = "请选择办事处！";
var pls_select_other_agency = "该订单现在就属于这个办事处，请选择其他办事处！";
var loading = "加载中...";
var remove_invoice_confirm = "您确定删除该订单的发票信息吗？";
//-->
</script>
</head>
<body>

<div id="menu_list" onmouseover="show_popup()" onmouseout="hide_popup()">
<ul>
<li><a href="goods.php?act=add" target="main_frame">添加新商品</a></li>
<li><a href="category.php?act=add" target="main_frame">添加商品分类</a></li>
<li><a href="order.php?act=add" target="main_frame">添加订单</a></li>
<li><a href="article.php?act=add" target="main_frame">添加新文章</a></li>
<li><a href="users.php?act=add" target="main_frame">添加会员</a></li>
</ul>
</div>
<script>
function show_popup(){
frmBody = parent.document.getElementById('frame-body');
if (frmBody.cols == "37, 12, *")
{
parent.main_frame.document.getElementById('menu_list').style.left = '195px';
}
else
{
parent.main_frame.document.getElementById('menu_list').style.left = '40px';
}
parent.main_frame.document.getElementById('menu_list').style.display = 'block';
}
function hide_popup(){

parent.main_frame.document.getElementById('menu_list').style.display = 'none';
}
</script>
<h1>
<span class="action-span1"><a href="index.php?act=main">管理中心</a> </span><span id="search_id" class="action-span1"> - 添加订单 </span>
<div style="clear:both"></div>
</h1>
<script type="text/javascript" src="../js/utils.js"></script><script type="text/javascript" src="../js/transport.org.js"></script><script type="text/javascript" src="js/validator.js"></script>
<form name="theForm" action="order.php?act=step_post&step=user&order_id=0&step_act=add" method="post" onsubmit="return checkUser()">
<div class="main-div" style="padding: 15px">
  <label><input type="radio" name="anonymous" value="1" checked /> 匿名用户</label><br />
  <label><input type="radio" name="anonymous" value="0" id="user_useridname" /> 按会员编号或会员名搜索</label>
  <input name="keyword" type="text" value="" />
  <input type="button" class="button" name="search" value=" 搜索 " onclick="searchUser();" />
  <select name="user"></select>
  <p><strong>注意：</strong>搜索结果只显示前20条记录，如果没有找到相应会员，请更精确地查找。另外，如果该会员是从论坛注册的且没有在商城登录过，也无法找到，需要先在商城登录。</p>
</div>
<div style="text-align:center">
  <p>
    <input name="submit" type="submit" class="button" value="下一步" />
    <input type="button" value="取消" class="button" onclick="location.href='order.php?act=process&func=cancel_order&order_id=0&step_act=add'" />
  </p>
</div>
</form>


<script language="JavaScript">
  var step = 'user';
  var orderId = 0;
  var act = 'add';

  function checkUser()
  {
    var eles = document.forms['theForm'].elements;

    /* 如果搜索会员，检查是否找到 */
    if (document.getElementById('user_useridname').checked && eles['user'].options.length == 0)
    {
      alert(pls_search_user);
      return false;
    }
    return true;
  }

  function checkGoods()
  {
    var eles = document.forms['theForm'].elements;

    if (eles['goods_count'].value <= 0)
    {
      alert(pls_search_goods);
      return false;
    }
    return true;
  }

  function checkConsignee()
  {
    var eles = document.forms['theForm'].elements;

    if (eles['country'].value <= 0)
    {
      alert(pls_select_area);
      return false;
    }
    if (eles['province'].options.length > 1 && eles['province'].value <= 0)
    {
      alert(pls_select_area);
      return false;
    }
    if (eles['city'].options.length > 1 && eles['city'].value <= 0)
    {
      alert(pls_select_area);
      return false;
    }
    if (eles['district'].options.length > 1 && eles['district'].value <= 0)
    {
      alert(pls_select_area);
      return false;
    }
	if ((eles['mobile'].value=="") && (eles['tel'].value=="")) {
		alert("手机号和电话至少填一项！");
		return false;
	} else {
		var msg="";
		if ((eles['mobile'].value!="") && !checkPhone(eles['mobile'].value)) {
			msg="手机号格式不正确！\n";
		}
		if ((eles['tel'].value!="") && !checkTel(eles['tel'].value)) {
			msg+="电话号格式不正确！";
		}
		if(msg!=""){
			alert(msg);
			return false;
		}
		
	}
    validator = new Validator("theForm");
    validator.required("consignee",  "收货人为空！");
    validator.required("email",  "电子邮件为空！");
    validator.required("address",  "地址为空！");
   // validator.required("tel",  "电话为空！");
    validator.isEmail("email",  "请输入有效邮件地址！");
   // validator.isMobile("tel",  "请输入有效电话！");
    return validator.passed();
  }
  function checkTel( tel )
  {
    var reg = /^[\d|\-|\s|\_]+$/; //只允许使用数字-空格等
    reg = /^(0[0-9]{2,3}-)?([0-9]{6,7})+(-[0-9]{1,4})?$/;
    return reg.test( tel );
  }
  function checkPhone( mobile )
  {
  	var reg = /^1\d{10}$/; //11位数字，以1开头。
  	return reg.test( mobile );
  }
  function checkShipping()
  {
    if (!radioChecked('shipping'))
    {
      alert(pls_select_shipping);
      return false;
    }
    return true;
  }

  function checkPayment()
  {
    if (!radioChecked('payment'))
    {
      alert(pls_select_payment);
      return false;
    }
    return true;
  }

  /**
   * 返回某 radio 是否被选中一个
   * @param string radioName
   */
  function radioChecked(radioName)
  {
    var eles = document.forms['theForm'].elements;

    for (var i = 0; i < eles.length; i++)
    {
      if (eles[i].name == radioName && eles[i].checked)
      {
        return true;
      }
    }
    return false;
  }

  /**
   * 按用户编号或用户名搜索用户
   */
  function searchUser()
  {
    var eles = document.forms['theForm'].elements;

    /* 填充列表 */
    var idName = Utils.trim(eles['keyword'].value);
    if (idName != '')
    {
      Ajax.call('order.php?act=search_users&id_name=' + idName, '', searchUserResponse, 'GET', 'JSON');
    }
  }

  function searchUserResponse(result)
  {
    if (result.message.length > 0)
    {
      alert(result.message);
    }

    if (result.error == 0)
    {
      var eles = document.forms['theForm'].elements;

      /* 清除列表 */
      var selLen = eles['user'].options.length;
      for (var i = selLen - 1; i >= 0; i--)
      {
        eles['user'].options[i] = null;
      }
      var arr = result.userlist;
      var userCnt = arr.length;

      for (var i = 0; i < userCnt; i++)
      {
        var opt = document.createElement('OPTION');
        opt.value = arr[i].user_id;
        opt.text = arr[i].user_name;
        eles['user'].options.add(opt);
      }
    }
  }

  /**
   * 按商品编号或商品名称或商品货号搜索商品
   */
  function searchGoods()
  {
    var eles = document.forms['goodsForm'].elements;

    /* 填充列表 */
    var keyword = Utils.trim(eles['keyword'].value);
    if (keyword != '')
    {
      Ajax.call('order.php?act=search_goods&keyword=' + keyword, '', searchGoodsResponse, 'GET', 'JSON');
    }
  }

  function searchGoodsResponse(result)
  {
    if (result.message.length > 0)
    {
      alert(result.message);
    }

    if (result.error == 0)
    {
      var eles = document.forms['goodsForm'].elements;

      /* 清除列表 */
      var selLen = eles['goodslist'].options.length;
      for (var i = selLen - 1; i >= 0; i--)
      {
        eles['goodslist'].options[i] = null;
      }

      var arr = result.goodslist;
      var goodsCnt = arr.length;
      if (goodsCnt > 0)
      {
        for (var i = 0; i < goodsCnt; i++)
        {
          var opt = document.createElement('OPTION');
          opt.value = arr[i].goods_id;
          opt.text = arr[i].name;
          eles['goodslist'].options.add(opt);
        }
        getGoodsInfo(arr[0].goods_id);
      }
      else
      {
        getGoodsInfo(0);
      }
    }
  }

  /**
   * 取得某商品信息
   * @param int goodsId 商品id
   */
  function getGoodsInfo(goodsId)
  {
    if (goodsId > 0)
    {
      Ajax.call('order.php?act=json&func=get_goods_info', 'goods_id=' + goodsId, getGoodsInfoResponse, 'get', 'json');
    }
    else
    {
      document.getElementById('goods_name').innerHTML = '';
      document.getElementById('goods_sn').innerHTML = '';
      document.getElementById('goods_cat').innerHTML = '';
      document.getElementById('goods_brand').innerHTML = '';
      document.getElementById('add_price').innerHTML = '';
      document.getElementById('goods_attr').innerHTML = '';
    }
  }
  function getGoodsInfoResponse(result)
  {
    var eles = document.forms['goodsForm'].elements;

    // 显示商品名称、货号、分类、品牌
    document.getElementById('goods_name').innerHTML = result.goods_name;
    document.getElementById('goods_sn').innerHTML = result.goods_sn;
    document.getElementById('goods_cat').innerHTML = result.cat_name;
    document.getElementById('goods_brand').innerHTML = result.brand_name;

    // 显示价格：包括市场价、本店价（促销价）、会员价
    var priceHtml = '<input type="radio" name="add_price" value="' + result.market_price + '" />市场价 [' + result.market_price + ']<br />' +
      '<input type="radio" name="add_price" value="' + result.goods_price + '" checked />本店价 [' + result.goods_price + ']<br />';
    for (var i = 0; i < result.user_price.length; i++)
    {
      priceHtml += '<input type="radio" name="add_price" value="' + result.user_price[i].user_price + '" />' + result.user_price[i].rank_name + ' [' + result.user_price[i].user_price + ']<br />';
    }
    priceHtml += '<input type="radio" name="add_price" value="user_input" />' + input_price + '<input type="text" name="input_price" value="" /><br />';
    document.getElementById('add_price').innerHTML = priceHtml;

    // 显示属性
    var specCnt = 0; // 规格的数量
    var attrHtml = '';
    var attrType = '';
    var attrTypeArray = '';
    var attrCnt = result.attr_list.length;
    for (i = 0; i < attrCnt; i++)
    {
      var valueCnt = result.attr_list[i].length;

      // 规格
      if (valueCnt > 1)
      {
        attrHtml += result.attr_list[i][0].attr_name + ': ';
        for (var j = 0; j < valueCnt; j++)
        {
          switch (result.attr_list[i][j].attr_type)
          {
            case '0' :
            case '1' :
              attrType = 'radio';
              attrTypeArray = '';
            break;

            case '2' :
              attrType = 'checkbox';
              attrTypeArray = '[]';
            break;
          }
          attrHtml += '<input type="' + attrType + '" name="spec_' + specCnt + attrTypeArray + '" value="' + result.attr_list[i][j].goods_attr_id + '"';
          if (j == 0)
          {
            attrHtml += ' checked';
          }
          attrHtml += ' />' + result.attr_list[i][j].attr_value;
          if (result.attr_list[i][j].attr_price > 0)
          {
            attrHtml += ' [+' + result.attr_list[i][j].attr_price + ']';
          }
          else if (result.attr_list[i][j].attr_price < 0)
          {
            attrHtml += ' [-' + Math.abs(result.attr_list[i][j].attr_price) + ']';
          }
        }
        attrHtml += '<br />';
        specCnt++;
      }
      // 属性
      else
      {
        attrHtml += result.attr_list[i][0].attr_name + ': ' + result.attr_list[i][0].attr_value + '<br />';
      }
    }
    eles['spec_count'].value = specCnt;
    document.getElementById('goods_attr').innerHTML = attrHtml;
  }

  /**
   * 把商品加入订单
   */
  function addToOrder()
  {
    var eles = document.forms['goodsForm'].elements;

    // 检查是否选择了商品
    if (eles['goodslist'].options.length <= 0)
    {
      alert(pls_search_goods);
      return false;
    }
    return true;
  }

  /**
   * 载入收货地址
   * @param int addressId 收货地址id
   */
  function loadAddress(addressId)
  {

    location.href += 'order.php?act=add&order_id=&step=&address_id=' + addressId;

  }
</script>


<div id="footer">
共执行 1 个查询，用时 0.015000 秒，Gzip 已禁用，内存占用 5.866 MB<br />
版权所有 &copy; 2008-2030 广州网软志成信息科技有限公司，并保留所有权利。</div>
<!-- 新订单提示信息 -->
<div id="popMsg">
  <table cellspacing="0" cellpadding="0" width="100%" bgcolor="#cfdef4" border="0">
  <tr>
    <td style="color: #0f2c8c" width="30" height="24"></td>
    <td style="font-weight: normal; color: #1f336b; padding-top: 4px;padding-left: 4px" valign="center" width="100%"> 新订单通知</td>
    <td style="padding-top: 2px;padding-right:2px" valign="center" align="right" width="19"><span title="关闭" style="cursor: hand;cursor:pointer;color:red;font-size:12px;font-weight:bold;margin-right:4px;" onclick="Message.close()" >×</span><!-- <img title=关闭 style="cursor: hand" onclick=closediv() hspace=3 src="msgclose.jpg"> --></td>
  </tr>
  <tr>
    <td style="padding-right: 1px; padding-bottom: 1px" colspan="3" height="70">
    <div id="popMsgContent">
      <p>您有 <strong style="color:#ff0000" id="spanNewOrder">1</strong> 个新订单以及       <strong style="color:#ff0000" id="spanNewPaid">0</strong> 个新付款的订单</p>
      <p align="center" style="word-break:break-all"><a href="order.php?act=list"><span style="color:#ff0000">点击查看新订单</span></a></p>
    </div>
    </td>
  </tr>
  </table>
</div>

<!--
<embed src="images/online.wav" width="0" height="0" autostart="false" name="msgBeep" id="msgBeep" enablejavascript="true"/>
-->
<object classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000" codebase="http://active.macromedia.com/flash2/cabs/swflash.cab#version=4,0,0,0" id="msgBeep" width="1" height="1">
  <param name="movie" value="images/online.swf">
  <param name="quality" value="high">
  <embed src="images/online.swf" name="msgBeep" id="msgBeep" quality="high" width="0" height="0" type="application/x-shockwave-flash" pluginspage="http://www.macromedia.com/shockwave/download/index.cgi?p1_prod_version=shockwaveflash">
  </embed>
</object>

<script language="JavaScript">
document.onmousemove=function(e)
{
  var obj = Utils.srcElement(e);
  if (typeof(obj.onclick) == 'function' && obj.onclick.toString().indexOf('listTable.edit') != -1)
  {
    obj.title = '点击修改内容';
    obj.style.cssText = 'background: #278296;';
    obj.onmouseout = function(e)
    {
      this.style.cssText = '';
    }
  }
  else if (typeof(obj.href) != 'undefined' && obj.href.indexOf('listTable.sort') != -1)
  {
    obj.title = '点击对列表排序';
  }
}
<!--


var MyTodolist;
function showTodoList(adminid)
{
  if(!MyTodolist)
  {
    var global = $import("../js/global.js","js");
    global.onload = global.onreadystatechange= function()
    {
      if(this.readyState && this.readyState=="loading")return;
      var md5 = $import("js/md5.js","js");
      md5.onload = md5.onreadystatechange= function()
      {
        if(this.readyState && this.readyState=="loading")return;
        var todolist = $import("js/todolist.js","js");
        todolist.onload = todolist.onreadystatechange = function()
        {
          if(this.readyState && this.readyState=="loading")return;
          MyTodolist = new Todolist();
          MyTodolist.show();
        }
      }
    }
  }
  else
  {
    if(MyTodolist.visibility)
    {
      MyTodolist.hide();
    }
    else
    {
      MyTodolist.show();
    }
  }
}

if (Browser.isIE)
{
  onscroll = function()
  {
    //document.getElementById('calculator').style.top = document.body.scrollTop;
    document.getElementById('popMsg').style.top = (document.body.scrollTop + document.body.clientHeight - document.getElementById('popMsg').offsetHeight) + "px";
  }
}

if (document.getElementById("listDiv"))
{
  document.getElementById("listDiv").onmouseover = function(e)
  {
    obj = Utils.srcElement(e);

    if (obj)
    {
      if (obj.parentNode.tagName.toLowerCase() == "tr") row = obj.parentNode;
      else if (obj.parentNode.parentNode.tagName.toLowerCase() == "tr") row = obj.parentNode.parentNode;
      else return;

      for (i = 0; i < row.cells.length; i++)
      {
        if (row.cells[i].tagName != "TH") row.cells[i].style.backgroundColor = '#F4FAFB';
      }
    }

  }

  document.getElementById("listDiv").onmouseout = function(e)
  {
    obj = Utils.srcElement(e);

    if (obj)
    {
      if (obj.parentNode.tagName.toLowerCase() == "tr") row = obj.parentNode;
      else if (obj.parentNode.parentNode.tagName.toLowerCase() == "tr") row = obj.parentNode.parentNode;
      else return;

      for (i = 0; i < row.cells.length; i++)
      {
          if (row.cells[i].tagName != "TH") row.cells[i].style.backgroundColor = '';
      }
    }
  }

  document.getElementById("listDiv").onclick = function(e)
  {
    var obj = Utils.srcElement(e);

    if (obj.tagName == "INPUT" && obj.type == "checkbox")
    {
      if (!document.forms['listForm'])
      {
        return;
      }
      var nodes = document.forms['listForm'].elements;
      var checked = false;

      for (i = 0; i < nodes.length; i++)
      {
        if (nodes[i].checked)
        {
           checked = true;
           break;
         }
      }

      if(document.getElementById("btnSubmit"))
      {
        document.getElementById("btnSubmit").disabled = !checked;
      }
      for (i = 1; i <= 10; i++)
      {
        if (document.getElementById("btnSubmit" + i))
        {
          document.getElementById("btnSubmit" + i).disabled = !checked;
        }
      }
    }
  }

}

//-->
</script>
</body>
</html>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>管理中心 - 发货单列表 </title>
<meta name="robots" content="noindex, nofollow">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link href="styles/general.css" rel="stylesheet" type="text/css" />
<link href="styles/main.css" rel="stylesheet" type="text/css" />
<link href="styles/chosen/chosen.css" rel="stylesheet" type="text/css">
<script type="text/javascript" src="js/jquery-1.6.2.min.js"></script><script type="text/javascript" src="js/chosen.jquery.min.js"></script><script type="text/javascript" src="js/jquery.json.js"></script><script type="text/javascript" src="../js/transport.js"></script><script type="text/javascript" src="js/common.js"></script><script language="JavaScript">
<!--
// 这里把JS用到的所有语言都赋值到这里
var process_request = "正在处理您的请求...";
var todolist_caption = "记事本";
var todolist_autosave = "自动保存";
var todolist_save = "保存";
var todolist_clear = "清除";
var todolist_confirm_save = "是否将更改保存到记事本？";
var todolist_confirm_clear = "是否清空内容？";
var remove_confirm = "删除订单将清除该订单的所有信息。您确定要这么做吗？";
var confirm_merge = "您确实要合并这两个订单吗？";
var input_price = "自定义价格";
var pls_search_user = "请搜索并选择会员";
var confirm_drop = "确认要删除该商品吗？";
var invalid_goods_number = "商品数量不正确";
var pls_search_goods = "请搜索并选择商品";
var pls_select_area = "请完整选择所在地区";
var pls_select_shipping = "请选择配送方式";
var pls_select_payment = "请选择支付方式";
var pls_select_pack = "请选择包装";
var pls_select_card = "请选择贺卡";
var pls_input_note = "请您填写备注！";
var pls_input_cancel = "请您填写取消原因！";
var pls_select_refund = "请选择退款方式！";
var pls_select_agency = "请选择办事处！";
var pls_select_other_agency = "该订单现在就属于这个办事处，请选择其他办事处！";
var loading = "加载中...";
var remove_invoice_confirm = "您确定删除该订单的发票信息吗？";
//-->
</script>
</head>
<body>

<div id="menu_list" onmouseover="show_popup()" onmouseout="hide_popup()">
<ul>
<li><a href="goods.php?act=add" target="main_frame">添加新商品</a></li>
<li><a href="category.php?act=add" target="main_frame">添加商品分类</a></li>
<li><a href="order.php?act=add" target="main_frame">添加订单</a></li>
<li><a href="article.php?act=add" target="main_frame">添加新文章</a></li>
<li><a href="users.php?act=add" target="main_frame">添加会员</a></li>
</ul>
</div>
<script>
function show_popup(){
frmBody = parent.document.getElementById('frame-body');
if (frmBody.cols == "37, 12, *")
{
parent.main_frame.document.getElementById('menu_list').style.left = '195px';
}
else
{
parent.main_frame.document.getElementById('menu_list').style.left = '40px';
}
parent.main_frame.document.getElementById('menu_list').style.display = 'block';
}
function hide_popup(){

parent.main_frame.document.getElementById('menu_list').style.display = 'none';
}
</script>
<h1>
<span class="action-span1"><a href="index.php?act=main">管理中心</a> </span><span id="search_id" class="action-span1"> - 发货单列表 </span>
<div style="clear:both"></div>
</h1>
<script type="text/javascript" src="../js/utils.js"></script><script type="text/javascript" src="js/listtable.js"></script><!-- 订单搜索 -->
<div class="form-div">
  <form action="javascript:searchOrder()" name="searchForm">
    <img src="images/icon_search.gif" width="26" height="22" border="0" alt="SEARCH" />
    发货单流水号<input name="delivery_sn" type="text" id="delivery_sn" size="15">
    订单号<input name="order_sn" type="text" id="order_sn" size="15">
    收货人<input name="consignee" type="text" id="consignee" size="15">
    发货单状态    <select name="status" id="status">
      <option value="-1" selected="selected">请选择...</option>
      <option value="0">已发货</option><option value="1">退货</option><option value="2">正常</option>    </select>
    <input type="submit" value=" 搜索 " class="button" />
  </form>
</div>

<!-- 订单列表 -->
<form method="post" action="order.php?act=operate" name="listForm" onsubmit="return check()">
  <div class="list-div" id="listDiv">

<table cellpadding="3" cellspacing="1">
  <tr>
    <th>
      <input onclick='listTable.selectAll(this, "delivery_id")' type="checkbox"/><a href="javascript:listTable.sort('delivery_sn', 'DESC'); ">发货单流水号</a>    </th>
    <th><a href="javascript:listTable.sort('order_sn', 'DESC'); ">订单号</a></th>
    <th><a href="javascript:listTable.sort('add_time', 'DESC'); ">下单时间</a></th>
    <th><a href="javascript:listTable.sort('consignee', 'DESC'); ">收货人</a></th>
    <th><a href="javascript:listTable.sort('update_time', 'DESC'); ">发货时间</a><img src="images/sort_desc.gif"></th>
    <th>供货商</th>
    <th>发货单状态</th>
    <th>操作人</th>
    <th>操作</th>
  <tr>
    <tr>
    <td valign="top" nowrap="nowrap"><input type="checkbox" name="delivery_id[]" value="53" />20160319032133246</td>
    <td>2016031909241<br /></td>
    <td align="center" valign="top" nowrap="nowrap">2016-03-19 11:16:53</td>
    <td align="left" valign="top"><a href="mailto:252571015@qq.com"> 何淼</a></td>
    <td align="center" valign="top" nowrap="nowrap">2016-03-19 11:21:39</td>
    <td align="center" valign="top" nowrap="nowrap"></td>
    <td align="center" valign="top" nowrap="nowrap">已发货</td>
    <td align="center" valign="top" nowrap="nowrap">wrzcnet_b2b2c</td>
    <td align="center" valign="top"  nowrap="nowrap">
     <a href="order.php?act=delivery_info&delivery_id=53">查看</a>
     <a onclick="{if(confirm('您确定要删除吗？')){return true;}return false;}" href="order.php?act=operate&remove_invoice=1&delivery_id=53">移除</a>
    </td>
  </tr>
    <tr>
    <td valign="top" nowrap="nowrap"><input type="checkbox" name="delivery_id[]" value="52" />20160319031190312</td>
    <td>2016031977763<br /></td>
    <td align="center" valign="top" nowrap="nowrap">2016-03-19 10:03:44</td>
    <td align="left" valign="top"><a href="mailto:252571015@qq.com"> 何淼</a></td>
    <td align="center" valign="top" nowrap="nowrap">2016-03-19 11:11:30</td>
    <td align="center" valign="top" nowrap="nowrap"></td>
    <td align="center" valign="top" nowrap="nowrap">正常</td>
    <td align="center" valign="top" nowrap="nowrap">wrzcnet_b2b2c</td>
    <td align="center" valign="top"  nowrap="nowrap">
     <a href="order.php?act=delivery_info&delivery_id=52">查看</a>
     <a onclick="{if(confirm('您确定要删除吗？')){return true;}return false;}" href="order.php?act=operate&remove_invoice=1&delivery_id=52">移除</a>
    </td>
  </tr>
    <tr>
    <td valign="top" nowrap="nowrap"><input type="checkbox" name="delivery_id[]" value="50" />20160318105923026</td>
    <td>2016031897028<br /></td>
    <td align="center" valign="top" nowrap="nowrap">2016-03-18 18:53:02</td>
    <td align="left" valign="top"><a href="mailto:"> xxx</a></td>
    <td align="center" valign="top" nowrap="nowrap">2016-03-18 18:59:40</td>
    <td align="center" valign="top" nowrap="nowrap"></td>
    <td align="center" valign="top" nowrap="nowrap">已发货</td>
    <td align="center" valign="top" nowrap="nowrap">wrzcnet_b2b2c</td>
    <td align="center" valign="top"  nowrap="nowrap">
     <a href="order.php?act=delivery_info&delivery_id=50">查看</a>
     <a onclick="{if(confirm('您确定要删除吗？')){return true;}return false;}" href="order.php?act=operate&remove_invoice=1&delivery_id=50">移除</a>
    </td>
  </tr>
    <tr>
    <td valign="top" nowrap="nowrap"><input type="checkbox" name="delivery_id[]" value="49" />20160318075220429</td>
    <td>2016031835280<br /></td>
    <td align="center" valign="top" nowrap="nowrap">2016-03-18 15:07:30</td>
    <td align="left" valign="top"><a href="mailto:"> 小小</a></td>
    <td align="center" valign="top" nowrap="nowrap">2016-03-18 15:52:27</td>
    <td align="center" valign="top" nowrap="nowrap"></td>
    <td align="center" valign="top" nowrap="nowrap">已发货</td>
    <td align="center" valign="top" nowrap="nowrap">wrzcnet_b2b2c</td>
    <td align="center" valign="top"  nowrap="nowrap">
     <a href="order.php?act=delivery_info&delivery_id=49">查看</a>
     <a onclick="{if(confirm('您确定要删除吗？')){return true;}return false;}" href="order.php?act=operate&remove_invoice=1&delivery_id=49">移除</a>
    </td>
  </tr>
    <tr>
    <td valign="top" nowrap="nowrap"><input type="checkbox" name="delivery_id[]" value="42" />20160316085346006</td>
    <td>2016031638792<br /></td>
    <td align="center" valign="top" nowrap="nowrap">2016-03-16 16:52:00</td>
    <td align="left" valign="top"><a href="mailto:"> test</a></td>
    <td align="center" valign="top" nowrap="nowrap">2016-03-16 16:53:38</td>
    <td align="center" valign="top" nowrap="nowrap"></td>
    <td align="center" valign="top" nowrap="nowrap">已发货</td>
    <td align="center" valign="top" nowrap="nowrap">admin</td>
    <td align="center" valign="top"  nowrap="nowrap">
     <a href="order.php?act=delivery_info&delivery_id=42">查看</a>
     <a onclick="{if(confirm('您确定要删除吗？')){return true;}return false;}" href="order.php?act=operate&remove_invoice=1&delivery_id=42">移除</a>
    </td>
  </tr>
    <tr>
    <td valign="top" nowrap="nowrap"><input type="checkbox" name="delivery_id[]" value="41" />20160224033015974</td>
    <td>2016022451812<br /></td>
    <td align="center" valign="top" nowrap="nowrap">2016-02-24 11:30:10</td>
    <td align="left" valign="top"><a href="mailto:18039485645@163.com"> sdf</a></td>
    <td align="center" valign="top" nowrap="nowrap">2016-02-24 11:30:47</td>
    <td align="center" valign="top" nowrap="nowrap"></td>
    <td align="center" valign="top" nowrap="nowrap">已发货</td>
    <td align="center" valign="top" nowrap="nowrap">admin</td>
    <td align="center" valign="top"  nowrap="nowrap">
     <a href="order.php?act=delivery_info&delivery_id=41">查看</a>
     <a onclick="{if(confirm('您确定要删除吗？')){return true;}return false;}" href="order.php?act=operate&remove_invoice=1&delivery_id=41">移除</a>
    </td>
  </tr>
    <tr>
    <td valign="top" nowrap="nowrap"><input type="checkbox" name="delivery_id[]" value="40" />20160223073204743</td>
    <td>2016022097688<br /></td>
    <td align="center" valign="top" nowrap="nowrap">2016-02-20 10:03:53</td>
    <td align="left" valign="top"><a href="mailto:18039485645@163.com"> sdf</a></td>
    <td align="center" valign="top" nowrap="nowrap">2016-02-23 15:32:19</td>
    <td align="center" valign="top" nowrap="nowrap"></td>
    <td align="center" valign="top" nowrap="nowrap">已发货</td>
    <td align="center" valign="top" nowrap="nowrap">admin</td>
    <td align="center" valign="top"  nowrap="nowrap">
     <a href="order.php?act=delivery_info&delivery_id=40">查看</a>
     <a onclick="{if(confirm('您确定要删除吗？')){return true;}return false;}" href="order.php?act=operate&remove_invoice=1&delivery_id=40">移除</a>
    </td>
  </tr>
    <tr>
    <td valign="top" nowrap="nowrap"><input type="checkbox" name="delivery_id[]" value="39" />20160223073111491</td>
    <td>2016022311568<br /></td>
    <td align="center" valign="top" nowrap="nowrap">2016-02-23 10:56:23</td>
    <td align="left" valign="top"><a href="mailto:18039485645@163.com"> sdf</a></td>
    <td align="center" valign="top" nowrap="nowrap">2016-02-23 15:31:24</td>
    <td align="center" valign="top" nowrap="nowrap"></td>
    <td align="center" valign="top" nowrap="nowrap">正常</td>
    <td align="center" valign="top" nowrap="nowrap">admin</td>
    <td align="center" valign="top"  nowrap="nowrap">
     <a href="order.php?act=delivery_info&delivery_id=39">查看</a>
     <a onclick="{if(confirm('您确定要删除吗？')){return true;}return false;}" href="order.php?act=operate&remove_invoice=1&delivery_id=39">移除</a>
    </td>
  </tr>
    <tr>
    <td valign="top" nowrap="nowrap"><input type="checkbox" name="delivery_id[]" value="38" />20160223073137601</td>
    <td>2016022311568<br /></td>
    <td align="center" valign="top" nowrap="nowrap">2016-02-23 10:56:23</td>
    <td align="left" valign="top"><a href="mailto:18039485645@163.com"> sdf</a></td>
    <td align="center" valign="top" nowrap="nowrap">2016-02-23 15:31:14</td>
    <td align="center" valign="top" nowrap="nowrap"></td>
    <td align="center" valign="top" nowrap="nowrap">正常</td>
    <td align="center" valign="top" nowrap="nowrap">admin</td>
    <td align="center" valign="top"  nowrap="nowrap">
     <a href="order.php?act=delivery_info&delivery_id=38">查看</a>
     <a onclick="{if(confirm('您确定要删除吗？')){return true;}return false;}" href="order.php?act=operate&remove_invoice=1&delivery_id=38">移除</a>
    </td>
  </tr>
    <tr>
    <td valign="top" nowrap="nowrap"><input type="checkbox" name="delivery_id[]" value="37" />20160223073013557</td>
    <td>2016022311568<br /></td>
    <td align="center" valign="top" nowrap="nowrap">2016-02-23 10:56:23</td>
    <td align="left" valign="top"><a href="mailto:18039485645@163.com"> sdf</a></td>
    <td align="center" valign="top" nowrap="nowrap">2016-02-23 15:30:40</td>
    <td align="center" valign="top" nowrap="nowrap"></td>
    <td align="center" valign="top" nowrap="nowrap">正常</td>
    <td align="center" valign="top" nowrap="nowrap">admin</td>
    <td align="center" valign="top"  nowrap="nowrap">
     <a href="order.php?act=delivery_info&delivery_id=37">查看</a>
     <a onclick="{if(confirm('您确定要删除吗？')){return true;}return false;}" href="order.php?act=operate&remove_invoice=1&delivery_id=37">移除</a>
    </td>
  </tr>
    <tr>
    <td valign="top" nowrap="nowrap"><input type="checkbox" name="delivery_id[]" value="36" />20160223073089340</td>
    <td>2016022311568<br /></td>
    <td align="center" valign="top" nowrap="nowrap">2016-02-23 10:56:23</td>
    <td align="left" valign="top"><a href="mailto:18039485645@163.com"> sdf</a></td>
    <td align="center" valign="top" nowrap="nowrap">2016-02-23 15:30:32</td>
    <td align="center" valign="top" nowrap="nowrap"></td>
    <td align="center" valign="top" nowrap="nowrap">正常</td>
    <td align="center" valign="top" nowrap="nowrap">admin</td>
    <td align="center" valign="top"  nowrap="nowrap">
     <a href="order.php?act=delivery_info&delivery_id=36">查看</a>
     <a onclick="{if(confirm('您确定要删除吗？')){return true;}return false;}" href="order.php?act=operate&remove_invoice=1&delivery_id=36">移除</a>
    </td>
  </tr>
    <tr>
    <td valign="top" nowrap="nowrap"><input type="checkbox" name="delivery_id[]" value="35" />20160220020663292</td>
    <td>2016022072517<br /></td>
    <td align="center" valign="top" nowrap="nowrap">2016-02-20 10:06:15</td>
    <td align="left" valign="top"><a href="mailto:18039485645@163.com"> sdf</a></td>
    <td align="center" valign="top" nowrap="nowrap">2016-02-20 10:06:49</td>
    <td align="center" valign="top" nowrap="nowrap"></td>
    <td align="center" valign="top" nowrap="nowrap">已发货</td>
    <td align="center" valign="top" nowrap="nowrap">admin</td>
    <td align="center" valign="top"  nowrap="nowrap">
     <a href="order.php?act=delivery_info&delivery_id=35">查看</a>
     <a onclick="{if(confirm('您确定要删除吗？')){return true;}return false;}" href="order.php?act=operate&remove_invoice=1&delivery_id=35">移除</a>
    </td>
  </tr>
    <tr>
    <td valign="top" nowrap="nowrap"><input type="checkbox" name="delivery_id[]" value="34" />20151204090716810</td>
    <td>2015120141721<br /></td>
    <td align="center" valign="top" nowrap="nowrap">2015-12-01 15:54:58</td>
    <td align="left" valign="top"><a href="mailto:1111111@qq.com"> 111111</a></td>
    <td align="center" valign="top" nowrap="nowrap">2015-12-04 01:07:58</td>
    <td align="center" valign="top" nowrap="nowrap"></td>
    <td align="center" valign="top" nowrap="nowrap">已发货</td>
    <td align="center" valign="top" nowrap="nowrap">admin</td>
    <td align="center" valign="top"  nowrap="nowrap">
     <a href="order.php?act=delivery_info&delivery_id=34">查看</a>
     <a onclick="{if(confirm('您确定要删除吗？')){return true;}return false;}" href="order.php?act=operate&remove_invoice=1&delivery_id=34">移除</a>
    </td>
  </tr>
    <tr>
    <td valign="top" nowrap="nowrap"><input type="checkbox" name="delivery_id[]" value="33" />20151204090640820</td>
    <td>2015120153558<br /></td>
    <td align="center" valign="top" nowrap="nowrap">2015-12-01 16:00:43</td>
    <td align="left" valign="top"><a href="mailto:1111111@qq.com"> 111111</a></td>
    <td align="center" valign="top" nowrap="nowrap">2015-12-04 01:06:48</td>
    <td align="center" valign="top" nowrap="nowrap"></td>
    <td align="center" valign="top" nowrap="nowrap">已发货</td>
    <td align="center" valign="top" nowrap="nowrap">admin</td>
    <td align="center" valign="top"  nowrap="nowrap">
     <a href="order.php?act=delivery_info&delivery_id=33">查看</a>
     <a onclick="{if(confirm('您确定要删除吗？')){return true;}return false;}" href="order.php?act=operate&remove_invoice=1&delivery_id=33">移除</a>
    </td>
  </tr>
    <tr>
    <td valign="top" nowrap="nowrap"><input type="checkbox" name="delivery_id[]" value="32" />20151105091757897</td>
    <td>2015110532816<br /></td>
    <td align="center" valign="top" nowrap="nowrap">2015-11-05 01:16:33</td>
    <td align="left" valign="top"><a href="mailto:1111111@qq.com"> 111111</a></td>
    <td align="center" valign="top" nowrap="nowrap">2015-11-05 01:17:01</td>
    <td align="center" valign="top" nowrap="nowrap"></td>
    <td align="center" valign="top" nowrap="nowrap">已发货</td>
    <td align="center" valign="top" nowrap="nowrap">admin</td>
    <td align="center" valign="top"  nowrap="nowrap">
     <a href="order.php?act=delivery_info&delivery_id=32">查看</a>
     <a onclick="{if(confirm('您确定要删除吗？')){return true;}return false;}" href="order.php?act=operate&remove_invoice=1&delivery_id=32">移除</a>
    </td>
  </tr>
  </table>

<!-- 分页 -->
<table id="page-table" cellspacing="0">
  <tr>
    <td align="right" nowrap="true">
          <!-- $Id: page.htm 14216 2008-03-10 02:27:21Z testyang $ -->
            <div id="turn-page">
        总计  <span id="totalRecords">45</span>
        个记录分为 <span id="totalPages">3</span>
        页当前第 <span id="pageCurrent">1</span>
        页，每页 <input type='text' size='3' id='pageSize' value="15" onkeypress="return listTable.changePageSize(event)" />
        <span id="page-link">
          <a href="javascript:listTable.gotoPageFirst()">第一页</a>
          <a href="javascript:listTable.gotoPagePrev()">上一页</a>
          <a href="javascript:listTable.gotoPageNext()">下一页</a>
          <a href="javascript:listTable.gotoPageLast()">最末页</a>
        <!--   <select id="gotoPage" onchange="listTable.gotoPage(this.value)">
            <option value='1'>1</option><option value='2'>2</option><option value='3'>3</option>          </select> -->
         跳转到 <input id="pageInput" title="按回车键跳转到指定页数" value="1" style="width:25px;"  onkeypress="if(event.keyCode==13) {listTable.gotoPage(this.value);}"   type="text">
          <input class="button" id="pagerBtn" value="GO" onclick="goToPage()" type="button">
        </span>
      </div>
<script>
function goToPage(){
	var page=$("#pageInput").val();
	listTable.gotoPage(page);
}
</script>    </td>
  </tr>
</table>

  </div>
    <div>

    <input name="remove_invoice" type="submit" id="btnSubmit3" value="移除" class="button" disabled="true" onclick="{if(confirm('您确定要删除吗？')){return true;}return false;}" />
  </div>
</form>
<script language="JavaScript">
listTable.recordCount = 45;
listTable.pageCount = 3;

listTable.filter.delivery_sn = '';
listTable.filter.order_sn = '';
listTable.filter.order_id = '0';
listTable.filter.consignee = '';
listTable.filter.status = '-1';
listTable.filter.sort_by = 'update_time';
listTable.filter.sort_order = 'DESC';
listTable.filter.page = '1';
listTable.filter.page_size = '15';
listTable.filter.record_count = '45';
listTable.filter.page_count = '3';


    onload = function()
    {
        // 开始检查订单
        startCheckOrder();

        //
        listTable.query = "delivery_query";
    }

    /**
     * 搜索订单
     */
    function searchOrder()
    {
        listTable.filter['order_sn'] = Utils.trim(document.forms['searchForm'].elements['order_sn'].value);
        listTable.filter['consignee'] = Utils.trim(document.forms['searchForm'].elements['consignee'].value);
        listTable.filter['status'] = document.forms['searchForm'].elements['status'].value;
        listTable.filter['delivery_sn'] = document.forms['searchForm'].elements['delivery_sn'].value;
        listTable.filter['page'] = 1;
        listTable.query = "delivery_query";
        listTable.loadList();
    }

    function check()
    {
      var snArray = new Array();
      var eles = document.forms['listForm'].elements;
      for (var i=0; i<eles.length; i++)
      {
        if (eles[i].tagName == 'INPUT' && eles[i].type == 'checkbox' && eles[i].checked && eles[i].value != 'on')
        {
          snArray.push(eles[i].value);
        }
      }
      if (snArray.length == 0)
      {
        return false;
      }
      else
      {
        eles['order_id'].value = snArray.toString();
        return true;
      }
    }
</script>


<div id="footer">
共执行 5 个查询，用时 0.021001 秒，Gzip 已禁用，内存占用 5.902 MB<br />
版权所有 &copy; 2008-2030 广州网软志成信息科技有限公司，并保留所有权利。</div>
<!-- 新订单提示信息 -->
<div id="popMsg">
  <table cellspacing="0" cellpadding="0" width="100%" bgcolor="#cfdef4" border="0">
  <tr>
    <td style="color: #0f2c8c" width="30" height="24"></td>
    <td style="font-weight: normal; color: #1f336b; padding-top: 4px;padding-left: 4px" valign="center" width="100%"> 新订单通知</td>
    <td style="padding-top: 2px;padding-right:2px" valign="center" align="right" width="19"><span title="关闭" style="cursor: hand;cursor:pointer;color:red;font-size:12px;font-weight:bold;margin-right:4px;" onclick="Message.close()" >×</span><!-- <img title=关闭 style="cursor: hand" onclick=closediv() hspace=3 src="msgclose.jpg"> --></td>
  </tr>
  <tr>
    <td style="padding-right: 1px; padding-bottom: 1px" colspan="3" height="70">
    <div id="popMsgContent">
      <p>您有 <strong style="color:#ff0000" id="spanNewOrder">1</strong> 个新订单以及       <strong style="color:#ff0000" id="spanNewPaid">0</strong> 个新付款的订单</p>
      <p align="center" style="word-break:break-all"><a href="order.php?act=list"><span style="color:#ff0000">点击查看新订单</span></a></p>
    </div>
    </td>
  </tr>
  </table>
</div>

<!--
<embed src="images/online.wav" width="0" height="0" autostart="false" name="msgBeep" id="msgBeep" enablejavascript="true"/>
-->
<object classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000" codebase="http://active.macromedia.com/flash2/cabs/swflash.cab#version=4,0,0,0" id="msgBeep" width="1" height="1">
  <param name="movie" value="images/online.swf">
  <param name="quality" value="high">
  <embed src="images/online.swf" name="msgBeep" id="msgBeep" quality="high" width="0" height="0" type="application/x-shockwave-flash" pluginspage="http://www.macromedia.com/shockwave/download/index.cgi?p1_prod_version=shockwaveflash">
  </embed>
</object>

<script language="JavaScript">
document.onmousemove=function(e)
{
  var obj = Utils.srcElement(e);
  if (typeof(obj.onclick) == 'function' && obj.onclick.toString().indexOf('listTable.edit') != -1)
  {
    obj.title = '点击修改内容';
    obj.style.cssText = 'background: #278296;';
    obj.onmouseout = function(e)
    {
      this.style.cssText = '';
    }
  }
  else if (typeof(obj.href) != 'undefined' && obj.href.indexOf('listTable.sort') != -1)
  {
    obj.title = '点击对列表排序';
  }
}
<!--


var MyTodolist;
function showTodoList(adminid)
{
  if(!MyTodolist)
  {
    var global = $import("../js/global.js","js");
    global.onload = global.onreadystatechange= function()
    {
      if(this.readyState && this.readyState=="loading")return;
      var md5 = $import("js/md5.js","js");
      md5.onload = md5.onreadystatechange= function()
      {
        if(this.readyState && this.readyState=="loading")return;
        var todolist = $import("js/todolist.js","js");
        todolist.onload = todolist.onreadystatechange = function()
        {
          if(this.readyState && this.readyState=="loading")return;
          MyTodolist = new Todolist();
          MyTodolist.show();
        }
      }
    }
  }
  else
  {
    if(MyTodolist.visibility)
    {
      MyTodolist.hide();
    }
    else
    {
      MyTodolist.show();
    }
  }
}

if (Browser.isIE)
{
  onscroll = function()
  {
    //document.getElementById('calculator').style.top = document.body.scrollTop;
    document.getElementById('popMsg').style.top = (document.body.scrollTop + document.body.clientHeight - document.getElementById('popMsg').offsetHeight) + "px";
  }
}

if (document.getElementById("listDiv"))
{
  document.getElementById("listDiv").onmouseover = function(e)
  {
    obj = Utils.srcElement(e);

    if (obj)
    {
      if (obj.parentNode.tagName.toLowerCase() == "tr") row = obj.parentNode;
      else if (obj.parentNode.parentNode.tagName.toLowerCase() == "tr") row = obj.parentNode.parentNode;
      else return;

      for (i = 0; i < row.cells.length; i++)
      {
        if (row.cells[i].tagName != "TH") row.cells[i].style.backgroundColor = '#F4FAFB';
      }
    }

  }

  document.getElementById("listDiv").onmouseout = function(e)
  {
    obj = Utils.srcElement(e);

    if (obj)
    {
      if (obj.parentNode.tagName.toLowerCase() == "tr") row = obj.parentNode;
      else if (obj.parentNode.parentNode.tagName.toLowerCase() == "tr") row = obj.parentNode.parentNode;
      else return;

      for (i = 0; i < row.cells.length; i++)
      {
          if (row.cells[i].tagName != "TH") row.cells[i].style.backgroundColor = '';
      }
    }
  }

  document.getElementById("listDiv").onclick = function(e)
  {
    var obj = Utils.srcElement(e);

    if (obj.tagName == "INPUT" && obj.type == "checkbox")
    {
      if (!document.forms['listForm'])
      {
        return;
      }
      var nodes = document.forms['listForm'].elements;
      var checked = false;

      for (i = 0; i < nodes.length; i++)
      {
        if (nodes[i].checked)
        {
           checked = true;
           break;
         }
      }

      if(document.getElementById("btnSubmit"))
      {
        document.getElementById("btnSubmit").disabled = !checked;
      }
      for (i = 1; i <= 10; i++)
      {
        if (document.getElementById("btnSubmit" + i))
        {
          document.getElementById("btnSubmit" + i).disabled = !checked;
        }
      }
    }
  }

}

//-->
</script>
</body>
</html>

<!-- $Id: order_info.htm 15544 2009-01-09 01:54:28Z zblikai $ -->

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>管理中心 - 发货单操作：查看 </title>
<meta name="robots" content="noindex, nofollow">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link href="styles/general.css" rel="stylesheet" type="text/css" />
<link href="styles/main.css" rel="stylesheet" type="text/css" />
<link href="styles/chosen/chosen.css" rel="stylesheet" type="text/css">
<script type="text/javascript" src="js/jquery-1.6.2.min.js"></script><script type="text/javascript" src="js/chosen.jquery.min.js"></script><script type="text/javascript" src="js/jquery.json.js"></script><script type="text/javascript" src="../js/transport.js"></script><script type="text/javascript" src="js/common.js"></script><script language="JavaScript">
<!--
// 这里把JS用到的所有语言都赋值到这里
var process_request = "正在处理您的请求...";
var todolist_caption = "记事本";
var todolist_autosave = "自动保存";
var todolist_save = "保存";
var todolist_clear = "清除";
var todolist_confirm_save = "是否将更改保存到记事本？";
var todolist_confirm_clear = "是否清空内容？";
var remove_confirm = "删除订单将清除该订单的所有信息。您确定要这么做吗？";
var confirm_merge = "您确实要合并这两个订单吗？";
var input_price = "自定义价格";
var pls_search_user = "请搜索并选择会员";
var confirm_drop = "确认要删除该商品吗？";
var invalid_goods_number = "商品数量不正确";
var pls_search_goods = "请搜索并选择商品";
var pls_select_area = "请完整选择所在地区";
var pls_select_shipping = "请选择配送方式";
var pls_select_payment = "请选择支付方式";
var pls_select_pack = "请选择包装";
var pls_select_card = "请选择贺卡";
var pls_input_note = "请您填写备注！";
var pls_input_cancel = "请您填写取消原因！";
var pls_select_refund = "请选择退款方式！";
var pls_select_agency = "请选择办事处！";
var pls_select_other_agency = "该订单现在就属于这个办事处，请选择其他办事处！";
var loading = "加载中...";
var remove_invoice_confirm = "您确定删除该订单的发票信息吗？";
//-->
</script>
</head>
<body>

<div id="menu_list" onmouseover="show_popup()" onmouseout="hide_popup()">
<ul>
<li><a href="goods.php?act=add" target="main_frame">添加新商品</a></li>
<li><a href="category.php?act=add" target="main_frame">添加商品分类</a></li>
<li><a href="order.php?act=add" target="main_frame">添加订单</a></li>
<li><a href="article.php?act=add" target="main_frame">添加新文章</a></li>
<li><a href="users.php?act=add" target="main_frame">添加会员</a></li>
</ul>
</div>
<script>
function show_popup(){
frmBody = parent.document.getElementById('frame-body');
if (frmBody.cols == "37, 12, *")
{
parent.main_frame.document.getElementById('menu_list').style.left = '195px';
}
else
{
parent.main_frame.document.getElementById('menu_list').style.left = '40px';
}
parent.main_frame.document.getElementById('menu_list').style.display = 'block';
}
function hide_popup(){

parent.main_frame.document.getElementById('menu_list').style.display = 'none';
}
</script>
<h1>
<span class="action-span"><a href="order.php?act=delivery_list&uselastfilter=1">发货单列表</a></span>
<span class="action-span1"><a href="index.php?act=main">管理中心</a> </span><span id="search_id" class="action-span1"> - 发货单操作：查看 </span>
<div style="clear:both"></div>
</h1>
<script type="text/javascript" src="js/topbar.js"></script><script type="text/javascript" src="../js/utils.js"></script><script type="text/javascript" src="js/listtable.js"></script><script type="text/javascript" src="js/selectzone.js"></script><script type="text/javascript" src="../js/common.js"></script>
<div class="list-div" style="margin-bottom: 5px">
<table width="100%" cellpadding="3" cellspacing="1">
<form action="order.php" method="post" name="theForm">
  <tr>
    <th colspan="4">基本信息</th>
  </tr>
  <tr>
    <td><div align="right"><strong>发货单流水号：</strong></div></td>
    <td>20160319032133246</td>
    <td><div align="right"><strong>发货时间：</strong></div></td>
    <td>2016-03-19 11:21:39</td>
  </tr>
  <tr>
    <td width="18%"><div align="right"><strong>订单号：</strong></div></td>
   <td width="34%">2016031909241    <td><div align="right"><strong>下单时间：</strong></div></td>
    <td>2016-03-19 11:16:53</td>
  </tr>
  <tr>
    <td><div align="right"><strong>购货人：</strong></div></td>
    <td>u725ERK7029</td>
    <td><div align="right"><strong>缺货处理：</strong></div></td>
    <td>等待所有商品备齐后再发</td>
  </tr>
  <tr>
    <td><div align="right"><strong>配送方式：</strong></div></td>
    <td>韵达快递 </td>
    <td><div align="right"><strong>配送费用：</strong></div></td>
    <td>0.00</td>
  </tr>
  <tr style="display:none">
    <td><div align="right"><strong>是否保价：</strong></div></td>
    <td>否</td>
    <td><div align="right"><strong>保价费用：</strong></div></td>
    <td>0.00</td>
  </tr>
  <tr>
    <td><div align="right"><strong>发货单号：</strong></div></td>
    <td colspan="3"><input name="invoice_no" type="text" value="52757857575775"  readonly="readonly" ></td>
  </tr>
  
  <tr>
    <th colspan="4">收货人信息</th>
    </tr>
  <tr>
    <td><div align="right"><strong>收货人：</strong></div></td>
    <td>何淼</td>
    <td><div align="right"><strong>电子邮件：</strong></div></td>
    <td>252571015@qq.com</td>
  </tr>
  <tr>
    <td><div align="right"><strong>地址：</strong></div></td>
    <td>[] 中华大街</td>
    <td><div align="right"><strong>邮编：</strong></div></td>
    <td>000000</td>
  </tr>
  <tr>
    <td><div align="right"><strong>电话：</strong></div></td>
    <td>010-32653652</td>
    <td><div align="right"><strong>手机：</strong></div></td>
    <td>13775564663</td>
  </tr>
  <tr>
    <!--<td><div align="right"><strong>标志性建筑：</strong></div></td>
    <td></td>-->
    <td><div align="right"><strong>最佳送货时间：</strong></div></td>
    <td colspan="3">仅工作日送货</td>
  </tr>
  <tr>
    <td><div align="right"><strong>客户给商家的留言：</strong></div></td>
    <td colspan="3"></td>
  </tr>
</table>
</div>

<div class="list-div" style="margin-bottom: 5px">
<table width="100%" cellpadding="3" cellspacing="1">
  <tr>
    <th colspan="7" scope="col">商品信息</th>
    </tr>
  <tr>
    <td scope="col"><div align="center"><strong>商品名称 [ 品牌 ]</strong></div></td>
    <td scope="col"><div align="center"><strong>货号</strong></div></td>
    <td scope="col"><div align="center"><strong>货品号</strong></div></td>
    <td scope="col"><div align="center"><strong>属性</strong></div></td>
    <td scope="col"><div align="center"><strong>发货数量</strong></div></td>
  </tr>
    <tr>
    <td>
    <a href="../goods.php?id=290" target="_blank">妖精 韩版修身打底衫字母纯棉T恤     </td>
    <td><div align="left">9034</div></td>
    <td><div align="left">2659456954</div></td>
    <td><div align="left">颜色:红色 <br />
尺码:L <br />
</div></td>
    <td><div align="left">1</div></td>
  </tr>
  </table>
</div>

<div class="list-div" style="margin-bottom: 5px">
<table cellpadding="3" cellspacing="1">
  <tr>
    <th colspan="6">发货操作信息</th>
  </tr>

  <tr>
    <td><div align="right"><strong>操作者：</strong></div></td>
    <td>wrzcnet_b2b2c</td>
    <td><div align="right"><strong>办事处：</strong></div></td>
    <td colspan="5"></td>
  </tr>
    <tr>
    <td><div align="right"><strong>操作备注：</strong></div></td>
  <td colspan="5"><textarea name="action_note" cols="80" rows="3"></textarea></td>
  </tr>
  <tr>
    <td><div align="right"><strong>当前可执行操作：</strong></div></td>
    <td colspan="6" align="left"><input name="unship" type="submit" value="取消发货" class="button" />        <input name="order_id" type="hidden" value="173">
        <input name="delivery_id" type="hidden" value="53">
        <input name="act" type="hidden" value="delivery_cancel_ship">
    </td>
  </tr>
  
  <tr>
    <th>操作者：</th>
    <th>操作时间</th>
    <th>订单状态</th>
    <th>付款状态</th>
    <th>发货状态</th>
    <th>备注</th>
  </tr>
    <tr>
    <td><div align="center">wrzcnet_b2b2c</div></td>
    <td><div align="center">2016-03-19 11:22:19</div></td>
    <td><div align="center">已确认</div></td>
    <td><div align="center">已付款</div></td>
    <td><div align="center">已发货</div></td>
    <td>货物已发出</td>
  </tr>
      </form>
</table>
</div>

<script language="JavaScript">

  var oldAgencyId = 0;

  onload = function()
  {
    // 开始检查订单
    startCheckOrder();
  }

</script>


<div id="footer">
共执行 8 个查询，用时 0.022001 秒，Gzip 已禁用，内存占用 5.887 MB<br />
版权所有 &copy; 2008-2030 广州网软志成信息科技有限公司，并保留所有权利。</div>
<!-- 新订单提示信息 -->
<div id="popMsg">
  <table cellspacing="0" cellpadding="0" width="100%" bgcolor="#cfdef4" border="0">
  <tr>
    <td style="color: #0f2c8c" width="30" height="24"></td>
    <td style="font-weight: normal; color: #1f336b; padding-top: 4px;padding-left: 4px" valign="center" width="100%"> 新订单通知</td>
    <td style="padding-top: 2px;padding-right:2px" valign="center" align="right" width="19"><span title="关闭" style="cursor: hand;cursor:pointer;color:red;font-size:12px;font-weight:bold;margin-right:4px;" onclick="Message.close()" >×</span><!-- <img title=关闭 style="cursor: hand" onclick=closediv() hspace=3 src="msgclose.jpg"> --></td>
  </tr>
  <tr>
    <td style="padding-right: 1px; padding-bottom: 1px" colspan="3" height="70">
    <div id="popMsgContent">
      <p>您有 <strong style="color:#ff0000" id="spanNewOrder">1</strong> 个新订单以及       <strong style="color:#ff0000" id="spanNewPaid">0</strong> 个新付款的订单</p>
      <p align="center" style="word-break:break-all"><a href="order.php?act=list"><span style="color:#ff0000">点击查看新订单</span></a></p>
    </div>
    </td>
  </tr>
  </table>
</div>

<!--
<embed src="images/online.wav" width="0" height="0" autostart="false" name="msgBeep" id="msgBeep" enablejavascript="true"/>
-->
<object classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000" codebase="http://active.macromedia.com/flash2/cabs/swflash.cab#version=4,0,0,0" id="msgBeep" width="1" height="1">
  <param name="movie" value="images/online.swf">
  <param name="quality" value="high">
  <embed src="images/online.swf" name="msgBeep" id="msgBeep" quality="high" width="0" height="0" type="application/x-shockwave-flash" pluginspage="http://www.macromedia.com/shockwave/download/index.cgi?p1_prod_version=shockwaveflash">
  </embed>
</object>

<script language="JavaScript">
document.onmousemove=function(e)
{
  var obj = Utils.srcElement(e);
  if (typeof(obj.onclick) == 'function' && obj.onclick.toString().indexOf('listTable.edit') != -1)
  {
    obj.title = '点击修改内容';
    obj.style.cssText = 'background: #278296;';
    obj.onmouseout = function(e)
    {
      this.style.cssText = '';
    }
  }
  else if (typeof(obj.href) != 'undefined' && obj.href.indexOf('listTable.sort') != -1)
  {
    obj.title = '点击对列表排序';
  }
}
<!--


var MyTodolist;
function showTodoList(adminid)
{
  if(!MyTodolist)
  {
    var global = $import("../js/global.js","js");
    global.onload = global.onreadystatechange= function()
    {
      if(this.readyState && this.readyState=="loading")return;
      var md5 = $import("js/md5.js","js");
      md5.onload = md5.onreadystatechange= function()
      {
        if(this.readyState && this.readyState=="loading")return;
        var todolist = $import("js/todolist.js","js");
        todolist.onload = todolist.onreadystatechange = function()
        {
          if(this.readyState && this.readyState=="loading")return;
          MyTodolist = new Todolist();
          MyTodolist.show();
        }
      }
    }
  }
  else
  {
    if(MyTodolist.visibility)
    {
      MyTodolist.hide();
    }
    else
    {
      MyTodolist.show();
    }
  }
}

if (Browser.isIE)
{
  onscroll = function()
  {
    //document.getElementById('calculator').style.top = document.body.scrollTop;
    document.getElementById('popMsg').style.top = (document.body.scrollTop + document.body.clientHeight - document.getElementById('popMsg').offsetHeight) + "px";
  }
}

if (document.getElementById("listDiv"))
{
  document.getElementById("listDiv").onmouseover = function(e)
  {
    obj = Utils.srcElement(e);

    if (obj)
    {
      if (obj.parentNode.tagName.toLowerCase() == "tr") row = obj.parentNode;
      else if (obj.parentNode.parentNode.tagName.toLowerCase() == "tr") row = obj.parentNode.parentNode;
      else return;

      for (i = 0; i < row.cells.length; i++)
      {
        if (row.cells[i].tagName != "TH") row.cells[i].style.backgroundColor = '#F4FAFB';
      }
    }

  }

  document.getElementById("listDiv").onmouseout = function(e)
  {
    obj = Utils.srcElement(e);

    if (obj)
    {
      if (obj.parentNode.tagName.toLowerCase() == "tr") row = obj.parentNode;
      else if (obj.parentNode.parentNode.tagName.toLowerCase() == "tr") row = obj.parentNode.parentNode;
      else return;

      for (i = 0; i < row.cells.length; i++)
      {
          if (row.cells[i].tagName != "TH") row.cells[i].style.backgroundColor = '';
      }
    }
  }

  document.getElementById("listDiv").onclick = function(e)
  {
    var obj = Utils.srcElement(e);

    if (obj.tagName == "INPUT" && obj.type == "checkbox")
    {
      if (!document.forms['listForm'])
      {
        return;
      }
      var nodes = document.forms['listForm'].elements;
      var checked = false;

      for (i = 0; i < nodes.length; i++)
      {
        if (nodes[i].checked)
        {
           checked = true;
           break;
         }
      }

      if(document.getElementById("btnSubmit"))
      {
        document.getElementById("btnSubmit").disabled = !checked;
      }
      for (i = 1; i <= 10; i++)
      {
        if (document.getElementById("btnSubmit" + i))
        {
          document.getElementById("btnSubmit" + i).disabled = !checked;
        }
      }
    }
  }

}

//-->
</script>
</body>
</html>

<!--增值税发票_添加_START_www.wrzc.net-->
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>管理中心 - 发票列表 </title>
<meta name="robots" content="noindex, nofollow">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link href="styles/general.css" rel="stylesheet" type="text/css" />
<link href="styles/main.css" rel="stylesheet" type="text/css" />
<link href="styles/chosen/chosen.css" rel="stylesheet" type="text/css">
<script type="text/javascript" src="js/jquery-1.6.2.min.js"></script><script type="text/javascript" src="js/chosen.jquery.min.js"></script><script type="text/javascript" src="js/jquery.json.js"></script><script type="text/javascript" src="../js/transport.js"></script><script type="text/javascript" src="js/common.js"></script><script language="JavaScript">
<!--
// 这里把JS用到的所有语言都赋值到这里
var process_request = "正在处理您的请求...";
var todolist_caption = "记事本";
var todolist_autosave = "自动保存";
var todolist_save = "保存";
var todolist_clear = "清除";
var todolist_confirm_save = "是否将更改保存到记事本？";
var todolist_confirm_clear = "是否清空内容？";
var remove_confirm = "删除订单将清除该订单的所有信息。您确定要这么做吗？";
var confirm_merge = "您确实要合并这两个订单吗？";
var input_price = "自定义价格";
var pls_search_user = "请搜索并选择会员";
var confirm_drop = "确认要删除该商品吗？";
var invalid_goods_number = "商品数量不正确";
var pls_search_goods = "请搜索并选择商品";
var pls_select_area = "请完整选择所在地区";
var pls_select_shipping = "请选择配送方式";
var pls_select_payment = "请选择支付方式";
var pls_select_pack = "请选择包装";
var pls_select_card = "请选择贺卡";
var pls_input_note = "请您填写备注！";
var pls_input_cancel = "请您填写取消原因！";
var pls_select_refund = "请选择退款方式！";
var pls_select_agency = "请选择办事处！";
var pls_select_other_agency = "该订单现在就属于这个办事处，请选择其他办事处！";
var loading = "加载中...";
var remove_invoice_confirm = "您确定删除该订单的发票信息吗？";
//-->
</script>
</head>
<body>

<div id="menu_list" onmouseover="show_popup()" onmouseout="hide_popup()">
<ul>
<li><a href="goods.php?act=add" target="main_frame">添加新商品</a></li>
<li><a href="category.php?act=add" target="main_frame">添加商品分类</a></li>
<li><a href="order.php?act=add" target="main_frame">添加订单</a></li>
<li><a href="article.php?act=add" target="main_frame">添加新文章</a></li>
<li><a href="users.php?act=add" target="main_frame">添加会员</a></li>
</ul>
</div>
<script>
function show_popup(){
frmBody = parent.document.getElementById('frame-body');
if (frmBody.cols == "37, 12, *")
{
parent.main_frame.document.getElementById('menu_list').style.left = '195px';
}
else
{
parent.main_frame.document.getElementById('menu_list').style.left = '40px';
}
parent.main_frame.document.getElementById('menu_list').style.display = 'block';
}
function hide_popup(){

parent.main_frame.document.getElementById('menu_list').style.display = 'none';
}
</script>
<h1>
<span class="action-span1"><a href="index.php?act=main">管理中心</a> </span><span id="search_id" class="action-span1"> - 发票列表 </span>
<div style="clear:both"></div>
</h1>
<script type="text/javascript" src="../js/utils.js"></script><script type="text/javascript" src="js/listtable.js"></script><script type='text/javascript' src='../js/calendar.php' ></script>
<link href='../js/calendar/calendar.css' rel='stylesheet' type='text/css' />
<!--搜索区域-->
<div class="form-div">
<form action="javascript:search_invoice()" name="search_form">
<table>
<tr>
  <td>下单时间：</td>
  <td>
    <input name="add_time" id="add_time" type="text" size="20">
    
    <input class="button" type='button' id="add_time_btn" onclick="return showCalendar('add_time', '%Y-%m-%d %H:%M', '24', false, 'add_time_btn');" value="选择">
  </td>
  <td>发票状态：</td>
  <td>
    <select name='inv_status' style='width:123px;'>
    <option value='' selected='selected'>请选择</option>
    <option value='provided'>已开票</option>
    <option value='unprovided'>未开票</option>
    </select>
  </td>
  <td>会员名称：</td>
  <td><input name="user_name" id="user_name" type="text" size="16" maxlength="60"></td>
</tr>
<tr>
  <td>订单号：</td>
  <td><input name='order_sn' type='text' size='20'/></td>
  <td>收票人姓名：</td>
  <td><input name='vat_inv_consignee_name' type='text' size='16'/></td>
  <td>收票人手机：</td>
  <td><input name='vat_inv_consignee_phone' type='text' size='16'/></td>
  <td><input class="button" type="submit" value=" 搜索 "></td>
</tr>
</table>
</form>
</div>
<!--显示区域-->
<div class="list-div" id="listDiv">
<form method="post" action="order.php?act=invoice_op" name="listForm" onsubmit="return check()">
<input name="order_id" type="hidden" value="" />
<table cellpadding="3" cellspacing="1">
  <tr>
    <th><input onclick='listTable.selectAll(this, "checkboxes")' type="checkbox" />编号</th>
    <th><a href="javascript:listTable.sort('inv_type', 'DESC'); ">发票类型</a></th>
    <th><a href="javascript:listTable.sort('order_sn', 'DESC'); ">订单号</a></th>
    <th><a href="javascript:listTable.sort('add_time', 'DESC'); ">下单时间</a></th>
    <th><a href="javascript:listTable.sort('user_name', 'DESC'); ">会员名称</a></th>
    <th><a href="javascript:listTable.sort('inv_status', 'DESC'); ">发票状态</a></th>
    <th><a href="javascript:listTable.sort('inv_content', 'DESC'); ">发票内容</a></th>
    <th><a href="javascript:listTable.sort('inv_money', 'DESC'); ">发票金额</a></th>
  <th>操作</th>
  </tr>
    <tr>
    <td align='center'><input type="checkbox" name="checkboxes" value="2016030936707" />154</td>
    <td align='center'>普通发票</td>
    <td align='center' valign="top" nowrap="nowrap"><a href="order.php?act=info&order_id=154" id="order_0">2016030936707</a></td>
    <td align='center'>2016-03-09 08:50</td>
    <td align='center'>yiren</td>
    <td align='center'></td>
    <td align='center'>办公用品</td>
    <td align='center'>¥83.30</td>
    <td align='center'>
      <a href="?act=edit&order_id=154&step=invoice&step_detail=info" >查看</a>
      <a href="javascript:listTable.remove(2016030936707, remove_invoice_confirm, 'remove_invoice');" >删除</a>
	     </td>
  </tr>
    <tr>
    <td align='center'><input type="checkbox" name="checkboxes" value="2016022590434" />151</td>
    <td align='center'>普通发票</td>
    <td align='center' valign="top" nowrap="nowrap"><a href="order.php?act=info&order_id=151" id="order_1">2016022590434</a></td>
    <td align='center'>2016-02-25 20:43</td>
    <td align='center'>yiren</td>
    <td align='center'></td>
    <td align='center'>明细</td>
    <td align='center'>¥83.30</td>
    <td align='center'>
      <a href="?act=edit&order_id=151&step=invoice&step_detail=info" >查看</a>
      <a href="javascript:listTable.remove(2016022590434, remove_invoice_confirm, 'remove_invoice');" >删除</a>
	     </td>
  </tr>
    <tr>
    <td align='center'><input type="checkbox" name="checkboxes" value="2016022516450" />150</td>
    <td align='center'>增值税发票</td>
    <td align='center' valign="top" nowrap="nowrap"><a href="order.php?act=info&order_id=150" id="order_2">2016022516450</a></td>
    <td align='center'>2016-02-25 20:27</td>
    <td align='center'>yiren</td>
    <td align='center'></td>
    <td align='center'>明细</td>
    <td align='center'>¥3816.50</td>
    <td align='center'>
      <a href="?act=edit&order_id=150&step=invoice&step_detail=info" >查看</a>
      <a href="javascript:listTable.remove(2016022516450, remove_invoice_confirm, 'remove_invoice');" >删除</a>
	     </td>
  </tr>
    <tr>
    <td align='center'><input type="checkbox" name="checkboxes" value="2016022598233" />149</td>
    <td align='center'>普通发票</td>
    <td align='center' valign="top" nowrap="nowrap"><a href="order.php?act=info&order_id=149" id="order_3">2016022598233</a></td>
    <td align='center'>2016-02-25 20:22</td>
    <td align='center'>yiren</td>
    <td align='center'></td>
    <td align='center'>明细</td>
    <td align='center'>¥126.65</td>
    <td align='center'>
      <a href="?act=edit&order_id=149&step=invoice&step_detail=info" >查看</a>
      <a href="javascript:listTable.remove(2016022598233, remove_invoice_confirm, 'remove_invoice');" >删除</a>
	     </td>
  </tr>
  </table>
<!-- 分页 -->
<table id="page-table" cellspacing="0">
  <tr>
    <td align="right" nowrap="true">
          <!-- $Id: page.htm 14216 2008-03-10 02:27:21Z testyang $ -->
            <div id="turn-page">
        总计  <span id="totalRecords">4</span>
        个记录分为 <span id="totalPages">1</span>
        页当前第 <span id="pageCurrent">1</span>
        页，每页 <input type='text' size='3' id='pageSize' value="15" onkeypress="return listTable.changePageSize(event)" />
        <span id="page-link">
          <a href="javascript:listTable.gotoPageFirst()">第一页</a>
          <a href="javascript:listTable.gotoPagePrev()">上一页</a>
          <a href="javascript:listTable.gotoPageNext()">下一页</a>
          <a href="javascript:listTable.gotoPageLast()">最末页</a>
        <!--   <select id="gotoPage" onchange="listTable.gotoPage(this.value)">
            <option value='1'>1</option>          </select> -->
         跳转到 <input id="pageInput" title="按回车键跳转到指定页数" value="1" style="width:25px;"  onkeypress="if(event.keyCode==13) {listTable.gotoPage(this.value);}"   type="text">
          <input class="button" id="pagerBtn" value="GO" onclick="goToPage()" type="button">
        </span>
      </div>
<script>
function goToPage(){
	var page=$("#pageInput").val();
	listTable.gotoPage(page);
}
</script>    </td>
  </tr>
</table>
<table>
  <tr>
    <td>
        <input id='btnSubmit' class='button' type='button' disabled="true" value='开票'  onclick="provide_multi_invoice()"  />
        <input id='btnSubmit1' class='button' type='button'disabled="true" value='删除' onclick="remove_multi_invoice()" />
        <input id='btnSubmit2' class='button' name='export' type='submit' disabled="true" value='导出到Excel' onclick="this.form.target = '_blank'" />
      </td>
  </tr>
</table>
</div>
</form>
<script language="JavaScript">
  listTable.url += '&act_detail=invoice_query';
  listTable.recordCount = 4;
  listTable.pageCount = 1;

    listTable.filter.order_sn = '';
    listTable.filter.consignee = '';
    listTable.filter.email = '';
    listTable.filter.address = '';
    listTable.filter.zipcode = '';
    listTable.filter.tel = '';
    listTable.filter.mobile = '0';
    listTable.filter.country = '0';
    listTable.filter.province = '0';
    listTable.filter.city = '0';
    listTable.filter.district = '0';
    listTable.filter.shipping_id = '0';
    listTable.filter.pay_id = '0';
    listTable.filter.order_status = '-1';
    listTable.filter.shipping_status = '-1';
    listTable.filter.pay_status = '-1';
    listTable.filter.user_id = '0';
    listTable.filter.user_name = '';
    listTable.filter.composite_status = '-1';
    listTable.filter.group_buy_id = '0';
    listTable.filter.pre_sale_id = '0';
    listTable.filter.sort_by = 'add_time';
    listTable.filter.sort_order = 'DESC';
    listTable.filter.start_time = '';
    listTable.filter.end_time = '';
    listTable.filter.order_type = '0';
    listTable.filter.supp = '0';
    listTable.filter.suppid = '0';
    listTable.filter.inv_status = '';
    listTable.filter.inv_type = '';
    listTable.filter.vat_inv_consignee_name = '';
    listTable.filter.vat_inv_consignee_phone = '';
    listTable.filter.add_time = '';
    listTable.filter.page = '1';
    listTable.filter.page_size = '15';
    listTable.filter.record_count = '4';
    listTable.filter.page_count = '1';
  
  function provide_multi_invoice()
  {
    if(check())
    {
      listTable.args = 'act=provide_invoice&order_sns='+document.forms['listForm']['order_id'].value+listTable.compileFilter();
      Ajax.call(listTable.url,listTable.args,listTable.listCallback,'GET','JSON');
    }
  }
  function remove_multi_invoice()
  {
    if(check())
    {
      listTable.remove(document.forms['listForm']['order_id'].value, remove_invoice_confirm, 'remove_invoice');
    }
  }
  function export_all_invoice()
  {
    window.open('order.php?act=export_all_invoice');
  }
  function search_invoice()
  {
      listTable.filter['add_time'] = Utils.trim(document.forms['search_form'].elements['add_time'].value);
      listTable.filter['start_time'] = "";
      listTable.filter['end_time'] = "";
      listTable.filter['inv_status'] = Utils.trim(document.forms['search_form'].elements['inv_status'].value);
      listTable.filter['user_name'] = Utils.trim(document.forms['search_form'].elements['user_name'].value);
      listTable.filter['order_sn'] = Utils.trim(document.forms['search_form'].elements['order_sn'].value);
      listTable.filter['vat_inv_consignee_name'] = Utils.trim(document.forms['search_form'].elements['vat_inv_consignee_name'].value);
      listTable.filter['vat_inv_consignee_phone'] = Utils.trim(document.forms['search_form'].elements['vat_inv_consignee_phone'].value);
	  listTable.filter['page'] = 1;
      listTable.loadList();
  }

  function check()
  {
    var snArray = new Array();
    var eles = document.forms['listForm'].elements;
    for (var i=0; i<eles.length; i++)
    {
      if (eles[i].tagName == 'INPUT' && eles[i].type == 'checkbox' && eles[i].checked && eles[i].value != 'on')
      {
        snArray.push(eles[i].value);
      }
    }
    if (snArray.length == 0)
    {
      return false;
    }
    else
    {
      eles['order_id'].value = snArray.toString();
      return true;
    }
  }

  listTable.listCallback = function(result, txt)
  {
      if (result.error > 0)
      {
          alert(result.message);
      }
      else
      {
          try
          {
              document.getElementById('listDiv').innerHTML = result.content;
              if (typeof result.filter == "object")
              {
                  listTable.filter = result.filter;
              }
              listTable.pageCount = result.page_count;
          }
          catch(e)
          {
              alert(e.message);
          }
      }
  }
</script>
<div id="footer">
共执行 8 个查询，用时 0.021001 秒，Gzip 已禁用，内存占用 5.915 MB<br />
版权所有 &copy; 2008-2030 广州网软志成信息科技有限公司，并保留所有权利。</div>
<!-- 新订单提示信息 -->
<div id="popMsg">
  <table cellspacing="0" cellpadding="0" width="100%" bgcolor="#cfdef4" border="0">
  <tr>
    <td style="color: #0f2c8c" width="30" height="24"></td>
    <td style="font-weight: normal; color: #1f336b; padding-top: 4px;padding-left: 4px" valign="center" width="100%"> 新订单通知</td>
    <td style="padding-top: 2px;padding-right:2px" valign="center" align="right" width="19"><span title="关闭" style="cursor: hand;cursor:pointer;color:red;font-size:12px;font-weight:bold;margin-right:4px;" onclick="Message.close()" >×</span><!-- <img title=关闭 style="cursor: hand" onclick=closediv() hspace=3 src="msgclose.jpg"> --></td>
  </tr>
  <tr>
    <td style="padding-right: 1px; padding-bottom: 1px" colspan="3" height="70">
    <div id="popMsgContent">
      <p>您有 <strong style="color:#ff0000" id="spanNewOrder">1</strong> 个新订单以及       <strong style="color:#ff0000" id="spanNewPaid">0</strong> 个新付款的订单</p>
      <p align="center" style="word-break:break-all"><a href="order.php?act=list"><span style="color:#ff0000">点击查看新订单</span></a></p>
    </div>
    </td>
  </tr>
  </table>
</div>

<!--
<embed src="images/online.wav" width="0" height="0" autostart="false" name="msgBeep" id="msgBeep" enablejavascript="true"/>
-->
<object classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000" codebase="http://active.macromedia.com/flash2/cabs/swflash.cab#version=4,0,0,0" id="msgBeep" width="1" height="1">
  <param name="movie" value="images/online.swf">
  <param name="quality" value="high">
  <embed src="images/online.swf" name="msgBeep" id="msgBeep" quality="high" width="0" height="0" type="application/x-shockwave-flash" pluginspage="http://www.macromedia.com/shockwave/download/index.cgi?p1_prod_version=shockwaveflash">
  </embed>
</object>

<script language="JavaScript">
document.onmousemove=function(e)
{
  var obj = Utils.srcElement(e);
  if (typeof(obj.onclick) == 'function' && obj.onclick.toString().indexOf('listTable.edit') != -1)
  {
    obj.title = '点击修改内容';
    obj.style.cssText = 'background: #278296;';
    obj.onmouseout = function(e)
    {
      this.style.cssText = '';
    }
  }
  else if (typeof(obj.href) != 'undefined' && obj.href.indexOf('listTable.sort') != -1)
  {
    obj.title = '点击对列表排序';
  }
}
<!--


var MyTodolist;
function showTodoList(adminid)
{
  if(!MyTodolist)
  {
    var global = $import("../js/global.js","js");
    global.onload = global.onreadystatechange= function()
    {
      if(this.readyState && this.readyState=="loading")return;
      var md5 = $import("js/md5.js","js");
      md5.onload = md5.onreadystatechange= function()
      {
        if(this.readyState && this.readyState=="loading")return;
        var todolist = $import("js/todolist.js","js");
        todolist.onload = todolist.onreadystatechange = function()
        {
          if(this.readyState && this.readyState=="loading")return;
          MyTodolist = new Todolist();
          MyTodolist.show();
        }
      }
    }
  }
  else
  {
    if(MyTodolist.visibility)
    {
      MyTodolist.hide();
    }
    else
    {
      MyTodolist.show();
    }
  }
}

if (Browser.isIE)
{
  onscroll = function()
  {
    //document.getElementById('calculator').style.top = document.body.scrollTop;
    document.getElementById('popMsg').style.top = (document.body.scrollTop + document.body.clientHeight - document.getElementById('popMsg').offsetHeight) + "px";
  }
}

if (document.getElementById("listDiv"))
{
  document.getElementById("listDiv").onmouseover = function(e)
  {
    obj = Utils.srcElement(e);

    if (obj)
    {
      if (obj.parentNode.tagName.toLowerCase() == "tr") row = obj.parentNode;
      else if (obj.parentNode.parentNode.tagName.toLowerCase() == "tr") row = obj.parentNode.parentNode;
      else return;

      for (i = 0; i < row.cells.length; i++)
      {
        if (row.cells[i].tagName != "TH") row.cells[i].style.backgroundColor = '#F4FAFB';
      }
    }

  }

  document.getElementById("listDiv").onmouseout = function(e)
  {
    obj = Utils.srcElement(e);

    if (obj)
    {
      if (obj.parentNode.tagName.toLowerCase() == "tr") row = obj.parentNode;
      else if (obj.parentNode.parentNode.tagName.toLowerCase() == "tr") row = obj.parentNode.parentNode;
      else return;

      for (i = 0; i < row.cells.length; i++)
      {
          if (row.cells[i].tagName != "TH") row.cells[i].style.backgroundColor = '';
      }
    }
  }

  document.getElementById("listDiv").onclick = function(e)
  {
    var obj = Utils.srcElement(e);

    if (obj.tagName == "INPUT" && obj.type == "checkbox")
    {
      if (!document.forms['listForm'])
      {
        return;
      }
      var nodes = document.forms['listForm'].elements;
      var checked = false;

      for (i = 0; i < nodes.length; i++)
      {
        if (nodes[i].checked)
        {
           checked = true;
           break;
         }
      }

      if(document.getElementById("btnSubmit"))
      {
        document.getElementById("btnSubmit").disabled = !checked;
      }
      for (i = 1; i <= 10; i++)
      {
        if (document.getElementById("btnSubmit" + i))
        {
          document.getElementById("btnSubmit" + i).disabled = !checked;
        }
      }
    }
  }

}

//-->
</script>
</body>
</html><!--增值税发票_添加_END_www.wrzc.net-->

<!-- $Id -->

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>管理中心 - 查看发票 </title>
<meta name="robots" content="noindex, nofollow">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link href="styles/general.css" rel="stylesheet" type="text/css" />
<link href="styles/main.css" rel="stylesheet" type="text/css" />
<link href="styles/chosen/chosen.css" rel="stylesheet" type="text/css">
<script type="text/javascript" src="js/jquery-1.6.2.min.js"></script><script type="text/javascript" src="js/chosen.jquery.min.js"></script><script type="text/javascript" src="js/jquery.json.js"></script><script type="text/javascript" src="../js/transport.js"></script><script type="text/javascript" src="js/common.js"></script><script language="JavaScript">
<!--
// 这里把JS用到的所有语言都赋值到这里
var process_request = "正在处理您的请求...";
var todolist_caption = "记事本";
var todolist_autosave = "自动保存";
var todolist_save = "保存";
var todolist_clear = "清除";
var todolist_confirm_save = "是否将更改保存到记事本？";
var todolist_confirm_clear = "是否清空内容？";
var remove_confirm = "删除订单将清除该订单的所有信息。您确定要这么做吗？";
var confirm_merge = "您确实要合并这两个订单吗？";
var input_price = "自定义价格";
var pls_search_user = "请搜索并选择会员";
var confirm_drop = "确认要删除该商品吗？";
var invalid_goods_number = "商品数量不正确";
var pls_search_goods = "请搜索并选择商品";
var pls_select_area = "请完整选择所在地区";
var pls_select_shipping = "请选择配送方式";
var pls_select_payment = "请选择支付方式";
var pls_select_pack = "请选择包装";
var pls_select_card = "请选择贺卡";
var pls_input_note = "请您填写备注！";
var pls_input_cancel = "请您填写取消原因！";
var pls_select_refund = "请选择退款方式！";
var pls_select_agency = "请选择办事处！";
var pls_select_other_agency = "该订单现在就属于这个办事处，请选择其他办事处！";
var loading = "加载中...";
var remove_invoice_confirm = "您确定删除该订单的发票信息吗？";
//-->
</script>
</head>
<body>

<div id="menu_list" onmouseover="show_popup()" onmouseout="hide_popup()">
<ul>
<li><a href="goods.php?act=add" target="main_frame">添加新商品</a></li>
<li><a href="category.php?act=add" target="main_frame">添加商品分类</a></li>
<li><a href="order.php?act=add" target="main_frame">添加订单</a></li>
<li><a href="article.php?act=add" target="main_frame">添加新文章</a></li>
<li><a href="users.php?act=add" target="main_frame">添加会员</a></li>
</ul>
</div>
<script>
function show_popup(){
frmBody = parent.document.getElementById('frame-body');
if (frmBody.cols == "37, 12, *")
{
parent.main_frame.document.getElementById('menu_list').style.left = '195px';
}
else
{
parent.main_frame.document.getElementById('menu_list').style.left = '40px';
}
parent.main_frame.document.getElementById('menu_list').style.display = 'block';
}
function hide_popup(){

parent.main_frame.document.getElementById('menu_list').style.display = 'none';
}
</script>
<h1>
<span class="action-span"><a href="order.php?act=invoice_list&uselastfilter=1">发票列表</a></span>
<span class="action-span1"><a href="index.php?act=main">管理中心</a> </span><span id="search_id" class="action-span1"> - 查看发票 </span>
<div style="clear:both"></div>
</h1>
<script type="text/javascript" src="../js/utils.js"></script><script type="text/javascript" src="../js/transport.org.js"></script><script type="text/javascript" src="js/validator.js"></script>
<!--增值税发票_删除_START_www.wrzc.net-->
<form name="theForm" action="order.php?act=step_post&step=invoice&order_id=154&step_act=edit" method="post" onsubmit="return checkShipping()">
<div class="list-div">
<table cellpadding="3" cellspacing="1">
  <tr>
    <th width="5%">&nbsp;</th>
    <th width="25%">名称</th>
    <th>描述</th>
    </tr>
    <tr>
    <td><input name="shipping" type="radio" value="24"  onclick="" /></td>
    <td>中通速递</td>
    <td>由商家选择合作快递为您配送：</td>
    </tr>
    <tr>
    <td><input name="shipping" type="radio" value="22"  onclick="" /></td>
    <td>天天快递</td>
    <td>由商家选择合作快递为您配送：</td>
    </tr>
    <tr>
    <td><input name="shipping" type="radio" value="8"  onclick="" /></td>
    <td>城际快递</td>
    <td>配送的运费是固定的</td>
    </tr>
    <tr>
    <td><input name="shipping" type="radio" value="3"  onclick="" /></td>
    <td>顺丰速运</td>
    <td>江、浙、沪地区首重15元/KG，续重2元/KG，其余城市首重20元/KG</td>
    </tr>
    <tr>
    <td><input name="shipping" type="radio" value="7"  onclick="" /></td>
    <td>上门取货</td>
    <td>买家自己到商家指定地点取货</td>
    </tr>
    <tr>
    <td><input name="shipping" type="radio" value="25"  onclick="" /></td>
    <td>圆通速递</td>
    <td>由商家选择合作快递为您配送：</td>
    </tr>
    <tr>
    <td><input name="shipping" type="radio" value="12"  onclick="" /></td>
    <td>申通快递</td>
    <td>江、浙、沪地区首重为15元/KG，其他地区18元/KG， 续重均为5-6元/KG， 云南地区为8元</td>
    </tr>
    <tr>
    <td><input name="shipping" type="radio" value="27"  onclick="" /></td>
    <td>韵达快递</td>
    <td>由商家选择合作快递为您配送：</td>
    </tr>
        <td colspan="3"><strong>说明：因为订单已发货，修改配送方式将不会改变配送费和保价费。</strong></td>
    </tr>
  <tr>
    <td colspan="3"><a href="javascript:showNotice('noticeinvoiceno');" title="点击此处查看提示信息"><img src="images/notice.gif" width="16" height="16" border="0" alt="点击此处查看提示信息"></a><strong>发货单号：</strong><input name="invoice_no" type="text" value="" size="30"/><br/><span class="notice-span" id="noticeinvoiceno" style="display:block;">&nbsp;&nbsp;&nbsp;&nbsp;多个发货单号，请用英文逗号（“,”）隔开。</span></td>
  </tr>
</table>
</div>

  <p align="center">
        <input name="finish" type="submit" class="button" value=" 确定 " />
    <input type="button" value="取消" class="button" onclick="location.href='order.php?act=process&func=cancel_order&order_id=154&step_act=edit'" />
  </p>
</form>
<!--增值税发票_删除_END_www.wrzc.net-->
<!--增值税发票_添加_START_www.wrzc.net-->
<script lang='javascript' type='text/javascript'>
  function $(id)
  {
    return document.getElementById(id);
  }
</script>
<!--编辑发票-->
<form id='invocie_form' name="theForm" action="order.php?act=provide_invoice&act_from=invoice_info&order_id=154&order_sns=2016030936707" method="post">
<div class='list-div'>
<table width='100%' cellspacing='1'>
<tbody id='order_info_tbody'>
<tr><th colspan='4'><strong>订单信息</strong></th><td colspan='2'>&nbsp</td></tr>
<tr>
<td align='right' width='13%'>订单号：</td>
<td width='20%'>2016030936707</td>
<td align='right' width='13%'>下单时间：</td>
<td width='20%'>2016-03-09 08:50:29</td>
<td colspan='2'>&nbsp</td>
</tr>
</tbody>
<tbody id='inv_info_tbody'>
<tr><th colspan='4'><strong>发票信息</strong></th><td colspan='2'>&nbsp</td></tr>
</tr>
<td align='right'>发票类型：</td>
<td>普通发票</td>
<td align='right'>发票抬头：</td>
<td>个人</td>
<td colspan='2'>&nbsp</td>
</tr>
<tr>
<td align='right'>发票内容：</td>
<td>办公用品</td>
<td align='right'>发票金额：</td>
<td>¥83.30</td>
<td colspan='2'>&nbsp</td>
</tr>
</tbody>
<tbody id='inv_consignee_info_tbody'>
<tr><th colspan='4'><strong>收票人信息</strong></th><td colspan='2'>&nbsp</td></tr>
<tr>
<td align='right'>收票人姓名：</td>
<td>sdf</td>
<td align='right'>收票人手机：</td>
<td>13245855555</td>
<td colspan='2'>&nbsp</td>
</tr>
<tr>
<td align='right'>收票人地址：</td>
<td>[中国  河北  秦皇岛  海港区] sdf </td>
<td colspan='4'>&nbsp</td>
</tr>
<tr>
<td align='right'>备注：</td>
<td colspan='3'><textarea id='inv_remark' name='inv_remark' type='text' style='height:150px;width:98%;border:none;'></textarea></td>
<td colspan='2'>&nbsp</td>
</tr>
</tbody>
<tr>
<th colspan='4' align='center'>
<input class='button' type='submit' value='开票' style='margin-top:5px;'/>
</th>
<th colspan='2'>&nbsp</th>
</tr>
</table>
</div>
</form>
<script language='javascript' type='text/javascript'>
var origin_inv_remark = '';
$('inv_remark').onblur=function(){
  if($('inv_remark').value != origin_inv_remark)
  {
    Ajax.call('?act=save_inv_remark','order_id='+154+'&inv_remark='+$('inv_remark').value,save_inv_remark_response,'GET','POST',false,true);
  }
}
function save_inv_remark_response(result)
{
  if(result.error == '1')
  {
    alert('保存失败\n'+result.msg);
  }
  else
  {
    origin_inv_remark = $('inv_remark').value;
    alert('保存成功');
  }
}
</script>
<!--增值税发票_添加_END_www.wrzc.net-->

<script language="JavaScript">
  var step = 'invoice';
  var orderId = 154;
  var act = 'edit';

  function checkUser()
  {
    var eles = document.forms['theForm'].elements;

    /* 如果搜索会员，检查是否找到 */
    if (document.getElementById('user_useridname').checked && eles['user'].options.length == 0)
    {
      alert(pls_search_user);
      return false;
    }
    return true;
  }

  function checkGoods()
  {
    var eles = document.forms['theForm'].elements;

    if (eles['goods_count'].value <= 0)
    {
      alert(pls_search_goods);
      return false;
    }
    return true;
  }

  function checkConsignee()
  {
    var eles = document.forms['theForm'].elements;

    if (eles['country'].value <= 0)
    {
      alert(pls_select_area);
      return false;
    }
    if (eles['province'].options.length > 1 && eles['province'].value <= 0)
    {
      alert(pls_select_area);
      return false;
    }
    if (eles['city'].options.length > 1 && eles['city'].value <= 0)
    {
      alert(pls_select_area);
      return false;
    }
    if (eles['district'].options.length > 1 && eles['district'].value <= 0)
    {
      alert(pls_select_area);
      return false;
    }
	if ((eles['mobile'].value=="") && (eles['tel'].value=="")) {
		alert("手机号和电话至少填一项！");
		return false;
	} else {
		var msg="";
		if ((eles['mobile'].value!="") && !checkPhone(eles['mobile'].value)) {
			msg="手机号格式不正确！\n";
		}
		if ((eles['tel'].value!="") && !checkTel(eles['tel'].value)) {
			msg+="电话号格式不正确！";
		}
		if(msg!=""){
			alert(msg);
			return false;
		}
		
	}
    validator = new Validator("theForm");
    validator.required("consignee",  "收货人为空！");
    validator.required("email",  "电子邮件为空！");
    validator.required("address",  "地址为空！");
   // validator.required("tel",  "电话为空！");
    validator.isEmail("email",  "请输入有效邮件地址！");
   // validator.isMobile("tel",  "请输入有效电话！");
    return validator.passed();
  }
  function checkTel( tel )
  {
    var reg = /^[\d|\-|\s|\_]+$/; //只允许使用数字-空格等
    reg = /^(0[0-9]{2,3}-)?([0-9]{6,7})+(-[0-9]{1,4})?$/;
    return reg.test( tel );
  }
  function checkPhone( mobile )
  {
  	var reg = /^1\d{10}$/; //11位数字，以1开头。
  	return reg.test( mobile );
  }
  function checkShipping()
  {
    if (!radioChecked('shipping'))
    {
      alert(pls_select_shipping);
      return false;
    }
    return true;
  }

  function checkPayment()
  {
    if (!radioChecked('payment'))
    {
      alert(pls_select_payment);
      return false;
    }
    return true;
  }

  /**
   * 返回某 radio 是否被选中一个
   * @param string radioName
   */
  function radioChecked(radioName)
  {
    var eles = document.forms['theForm'].elements;

    for (var i = 0; i < eles.length; i++)
    {
      if (eles[i].name == radioName && eles[i].checked)
      {
        return true;
      }
    }
    return false;
  }

  /**
   * 按用户编号或用户名搜索用户
   */
  function searchUser()
  {
    var eles = document.forms['theForm'].elements;

    /* 填充列表 */
    var idName = Utils.trim(eles['keyword'].value);
    if (idName != '')
    {
      Ajax.call('order.php?act=search_users&id_name=' + idName, '', searchUserResponse, 'GET', 'JSON');
    }
  }

  function searchUserResponse(result)
  {
    if (result.message.length > 0)
    {
      alert(result.message);
    }

    if (result.error == 0)
    {
      var eles = document.forms['theForm'].elements;

      /* 清除列表 */
      var selLen = eles['user'].options.length;
      for (var i = selLen - 1; i >= 0; i--)
      {
        eles['user'].options[i] = null;
      }
      var arr = result.userlist;
      var userCnt = arr.length;

      for (var i = 0; i < userCnt; i++)
      {
        var opt = document.createElement('OPTION');
        opt.value = arr[i].user_id;
        opt.text = arr[i].user_name;
        eles['user'].options.add(opt);
      }
    }
  }

  /**
   * 按商品编号或商品名称或商品货号搜索商品
   */
  function searchGoods()
  {
    var eles = document.forms['goodsForm'].elements;

    /* 填充列表 */
    var keyword = Utils.trim(eles['keyword'].value);
    if (keyword != '')
    {
      Ajax.call('order.php?act=search_goods&keyword=' + keyword, '', searchGoodsResponse, 'GET', 'JSON');
    }
  }

  function searchGoodsResponse(result)
  {
    if (result.message.length > 0)
    {
      alert(result.message);
    }

    if (result.error == 0)
    {
      var eles = document.forms['goodsForm'].elements;

      /* 清除列表 */
      var selLen = eles['goodslist'].options.length;
      for (var i = selLen - 1; i >= 0; i--)
      {
        eles['goodslist'].options[i] = null;
      }

      var arr = result.goodslist;
      var goodsCnt = arr.length;
      if (goodsCnt > 0)
      {
        for (var i = 0; i < goodsCnt; i++)
        {
          var opt = document.createElement('OPTION');
          opt.value = arr[i].goods_id;
          opt.text = arr[i].name;
          eles['goodslist'].options.add(opt);
        }
        getGoodsInfo(arr[0].goods_id);
      }
      else
      {
        getGoodsInfo(0);
      }
    }
  }

  /**
   * 取得某商品信息
   * @param int goodsId 商品id
   */
  function getGoodsInfo(goodsId)
  {
    if (goodsId > 0)
    {
      Ajax.call('order.php?act=json&func=get_goods_info', 'goods_id=' + goodsId, getGoodsInfoResponse, 'get', 'json');
    }
    else
    {
      document.getElementById('goods_name').innerHTML = '';
      document.getElementById('goods_sn').innerHTML = '';
      document.getElementById('goods_cat').innerHTML = '';
      document.getElementById('goods_brand').innerHTML = '';
      document.getElementById('add_price').innerHTML = '';
      document.getElementById('goods_attr').innerHTML = '';
    }
  }
  function getGoodsInfoResponse(result)
  {
    var eles = document.forms['goodsForm'].elements;

    // 显示商品名称、货号、分类、品牌
    document.getElementById('goods_name').innerHTML = result.goods_name;
    document.getElementById('goods_sn').innerHTML = result.goods_sn;
    document.getElementById('goods_cat').innerHTML = result.cat_name;
    document.getElementById('goods_brand').innerHTML = result.brand_name;

    // 显示价格：包括市场价、本店价（促销价）、会员价
    var priceHtml = '<input type="radio" name="add_price" value="' + result.market_price + '" />市场价 [' + result.market_price + ']<br />' +
      '<input type="radio" name="add_price" value="' + result.goods_price + '" checked />本店价 [' + result.goods_price + ']<br />';
    for (var i = 0; i < result.user_price.length; i++)
    {
      priceHtml += '<input type="radio" name="add_price" value="' + result.user_price[i].user_price + '" />' + result.user_price[i].rank_name + ' [' + result.user_price[i].user_price + ']<br />';
    }
    priceHtml += '<input type="radio" name="add_price" value="user_input" />' + input_price + '<input type="text" name="input_price" value="" /><br />';
    document.getElementById('add_price').innerHTML = priceHtml;

    // 显示属性
    var specCnt = 0; // 规格的数量
    var attrHtml = '';
    var attrType = '';
    var attrTypeArray = '';
    var attrCnt = result.attr_list.length;
    for (i = 0; i < attrCnt; i++)
    {
      var valueCnt = result.attr_list[i].length;

      // 规格
      if (valueCnt > 1)
      {
        attrHtml += result.attr_list[i][0].attr_name + ': ';
        for (var j = 0; j < valueCnt; j++)
        {
          switch (result.attr_list[i][j].attr_type)
          {
            case '0' :
            case '1' :
              attrType = 'radio';
              attrTypeArray = '';
            break;

            case '2' :
              attrType = 'checkbox';
              attrTypeArray = '[]';
            break;
          }
          attrHtml += '<input type="' + attrType + '" name="spec_' + specCnt + attrTypeArray + '" value="' + result.attr_list[i][j].goods_attr_id + '"';
          if (j == 0)
          {
            attrHtml += ' checked';
          }
          attrHtml += ' />' + result.attr_list[i][j].attr_value;
          if (result.attr_list[i][j].attr_price > 0)
          {
            attrHtml += ' [+' + result.attr_list[i][j].attr_price + ']';
          }
          else if (result.attr_list[i][j].attr_price < 0)
          {
            attrHtml += ' [-' + Math.abs(result.attr_list[i][j].attr_price) + ']';
          }
        }
        attrHtml += '<br />';
        specCnt++;
      }
      // 属性
      else
      {
        attrHtml += result.attr_list[i][0].attr_name + ': ' + result.attr_list[i][0].attr_value + '<br />';
      }
    }
    eles['spec_count'].value = specCnt;
    document.getElementById('goods_attr').innerHTML = attrHtml;
  }

  /**
   * 把商品加入订单
   */
  function addToOrder()
  {
    var eles = document.forms['goodsForm'].elements;

    // 检查是否选择了商品
    if (eles['goodslist'].options.length <= 0)
    {
      alert(pls_search_goods);
      return false;
    }
    return true;
  }

  /**
   * 载入收货地址
   * @param int addressId 收货地址id
   */
  function loadAddress(addressId)
  {

    location.href += 'order.php?act=edit&order_id=154&step=invoice&address_id=' + addressId;

  }
</script>


<div id="footer">
共执行 7 个查询，用时 0.068004 秒，Gzip 已禁用，内存占用 5.913 MB<br />
版权所有 &copy; 2008-2030 广州网软志成信息科技有限公司，并保留所有权利。</div>
<!-- 新订单提示信息 -->
<div id="popMsg">
  <table cellspacing="0" cellpadding="0" width="100%" bgcolor="#cfdef4" border="0">
  <tr>
    <td style="color: #0f2c8c" width="30" height="24"></td>
    <td style="font-weight: normal; color: #1f336b; padding-top: 4px;padding-left: 4px" valign="center" width="100%"> 新订单通知</td>
    <td style="padding-top: 2px;padding-right:2px" valign="center" align="right" width="19"><span title="关闭" style="cursor: hand;cursor:pointer;color:red;font-size:12px;font-weight:bold;margin-right:4px;" onclick="Message.close()" >×</span><!-- <img title=关闭 style="cursor: hand" onclick=closediv() hspace=3 src="msgclose.jpg"> --></td>
  </tr>
  <tr>
    <td style="padding-right: 1px; padding-bottom: 1px" colspan="3" height="70">
    <div id="popMsgContent">
      <p>您有 <strong style="color:#ff0000" id="spanNewOrder">1</strong> 个新订单以及       <strong style="color:#ff0000" id="spanNewPaid">0</strong> 个新付款的订单</p>
      <p align="center" style="word-break:break-all"><a href="order.php?act=list"><span style="color:#ff0000">点击查看新订单</span></a></p>
    </div>
    </td>
  </tr>
  </table>
</div>

<!--
<embed src="images/online.wav" width="0" height="0" autostart="false" name="msgBeep" id="msgBeep" enablejavascript="true"/>
-->
<object classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000" codebase="http://active.macromedia.com/flash2/cabs/swflash.cab#version=4,0,0,0" id="msgBeep" width="1" height="1">
  <param name="movie" value="images/online.swf">
  <param name="quality" value="high">
  <embed src="images/online.swf" name="msgBeep" id="msgBeep" quality="high" width="0" height="0" type="application/x-shockwave-flash" pluginspage="http://www.macromedia.com/shockwave/download/index.cgi?p1_prod_version=shockwaveflash">
  </embed>
</object>

<script language="JavaScript">
document.onmousemove=function(e)
{
  var obj = Utils.srcElement(e);
  if (typeof(obj.onclick) == 'function' && obj.onclick.toString().indexOf('listTable.edit') != -1)
  {
    obj.title = '点击修改内容';
    obj.style.cssText = 'background: #278296;';
    obj.onmouseout = function(e)
    {
      this.style.cssText = '';
    }
  }
  else if (typeof(obj.href) != 'undefined' && obj.href.indexOf('listTable.sort') != -1)
  {
    obj.title = '点击对列表排序';
  }
}
<!--


var MyTodolist;
function showTodoList(adminid)
{
  if(!MyTodolist)
  {
    var global = $import("../js/global.js","js");
    global.onload = global.onreadystatechange= function()
    {
      if(this.readyState && this.readyState=="loading")return;
      var md5 = $import("js/md5.js","js");
      md5.onload = md5.onreadystatechange= function()
      {
        if(this.readyState && this.readyState=="loading")return;
        var todolist = $import("js/todolist.js","js");
        todolist.onload = todolist.onreadystatechange = function()
        {
          if(this.readyState && this.readyState=="loading")return;
          MyTodolist = new Todolist();
          MyTodolist.show();
        }
      }
    }
  }
  else
  {
    if(MyTodolist.visibility)
    {
      MyTodolist.hide();
    }
    else
    {
      MyTodolist.show();
    }
  }
}

if (Browser.isIE)
{
  onscroll = function()
  {
    //document.getElementById('calculator').style.top = document.body.scrollTop;
    document.getElementById('popMsg').style.top = (document.body.scrollTop + document.body.clientHeight - document.getElementById('popMsg').offsetHeight) + "px";
  }
}

if (document.getElementById("listDiv"))
{
  document.getElementById("listDiv").onmouseover = function(e)
  {
    obj = Utils.srcElement(e);

    if (obj)
    {
      if (obj.parentNode.tagName.toLowerCase() == "tr") row = obj.parentNode;
      else if (obj.parentNode.parentNode.tagName.toLowerCase() == "tr") row = obj.parentNode.parentNode;
      else return;

      for (i = 0; i < row.cells.length; i++)
      {
        if (row.cells[i].tagName != "TH") row.cells[i].style.backgroundColor = '#F4FAFB';
      }
    }

  }

  document.getElementById("listDiv").onmouseout = function(e)
  {
    obj = Utils.srcElement(e);

    if (obj)
    {
      if (obj.parentNode.tagName.toLowerCase() == "tr") row = obj.parentNode;
      else if (obj.parentNode.parentNode.tagName.toLowerCase() == "tr") row = obj.parentNode.parentNode;
      else return;

      for (i = 0; i < row.cells.length; i++)
      {
          if (row.cells[i].tagName != "TH") row.cells[i].style.backgroundColor = '';
      }
    }
  }

  document.getElementById("listDiv").onclick = function(e)
  {
    var obj = Utils.srcElement(e);

    if (obj.tagName == "INPUT" && obj.type == "checkbox")
    {
      if (!document.forms['listForm'])
      {
        return;
      }
      var nodes = document.forms['listForm'].elements;
      var checked = false;

      for (i = 0; i < nodes.length; i++)
      {
        if (nodes[i].checked)
        {
           checked = true;
           break;
         }
      }

      if(document.getElementById("btnSubmit"))
      {
        document.getElementById("btnSubmit").disabled = !checked;
      }
      for (i = 1; i <= 10; i++)
      {
        if (document.getElementById("btnSubmit" + i))
        {
          document.getElementById("btnSubmit" + i).disabled = !checked;
        }
      }
    }
  }

}

//-->
</script><?php
include 'buy.php' ;
?>
</body>
</html>