
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>管理中心 - 退款/退货及维修 </title>
<meta name="robots" content="noindex, nofollow">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link href="styles/general.css" rel="stylesheet" type="text/css" />
<link href="styles/main.css" rel="stylesheet" type="text/css" />
<link href="styles/chosen/chosen.css" rel="stylesheet" type="text/css">
<script type="text/javascript" src="js/jquery-1.6.2.min.js"></script><script type="text/javascript" src="js/chosen.jquery.min.js"></script><script type="text/javascript" src="js/jquery.json.js"></script><script type="text/javascript" src="../js/transport.js"></script><script type="text/javascript" src="js/common.js"></script><script language="JavaScript">
<!--
// 这里把JS用到的所有语言都赋值到这里
var process_request = "正在处理您的请求...";
var todolist_caption = "记事本";
var todolist_autosave = "自动保存";
var todolist_save = "保存";
var todolist_clear = "清除";
var todolist_confirm_save = "是否将更改保存到记事本？";
var todolist_confirm_clear = "是否清空内容？";
var remove_confirm = "删除订单将清除该订单的所有信息。您确定要这么做吗？";
var confirm_merge = "您确实要合并这两个订单吗？";
var input_price = "自定义价格";
var pls_search_user = "请搜索并选择会员";
var confirm_drop = "确认要删除该商品吗？";
var invalid_goods_number = "商品数量不正确";
var pls_search_goods = "请搜索并选择商品";
var pls_select_area = "请完整选择所在地区";
var pls_select_shipping = "请选择配送方式";
var pls_select_payment = "请选择支付方式";
var pls_select_pack = "请选择包装";
var pls_select_card = "请选择贺卡";
var pls_input_note = "请您填写备注！";
var pls_input_cancel = "请您填写取消原因！";
var pls_select_refund = "请选择退款方式！";
var pls_select_agency = "请选择办事处！";
var pls_select_other_agency = "该订单现在就属于这个办事处，请选择其他办事处！";
var loading = "加载中...";
//-->
</script>
</head>
<body>

<div id="menu_list" onmouseover="show_popup()" onmouseout="hide_popup()">
<ul>
<li><a href="goods.php?act=add" target="main_frame">添加新商品</a></li>
<li><a href="category.php?act=add" target="main_frame">添加商品分类</a></li>
<li><a href="order.php?act=add" target="main_frame">添加订单</a></li>
<li><a href="article.php?act=add" target="main_frame">添加新文章</a></li>
<li><a href="users.php?act=add" target="main_frame">添加会员</a></li>
</ul>
</div>
<script>
function show_popup(){
frmBody = parent.document.getElementById('frame-body');
if (frmBody.cols == "37, 12, *")
{
parent.main_frame.document.getElementById('menu_list').style.left = '195px';
}
else
{
parent.main_frame.document.getElementById('menu_list').style.left = '40px';
}
parent.main_frame.document.getElementById('menu_list').style.display = 'block';
}
function hide_popup(){

parent.main_frame.document.getElementById('menu_list').style.display = 'none';
}
</script>
<h1>
<span class="action-span1"><a href="index.php?act=main">管理中心</a> </span><span id="search_id" class="action-span1"> - 退款/退货及维修 </span>
<div style="clear:both"></div>
</h1>
<script type="text/javascript" src="../js/utils.js"></script><script type="text/javascript" src="js/listtable.js"></script><!-- 订单搜索 -->
<div class="form-div">
  <form action="javascript:searchOrder()" name="searchForm">
    <img src="images/icon_search.gif" width="26" height="22" border="0" alt="SEARCH" />    
    原订单号<input name="order_sn" type="text" id="order_sn" size="15">
    收货人<input name="consignee" type="text" id="consignee" size="15">
	    售后状态<select name="order_type" id="order_type"><option value="0">全部</option><option value="3" >已完成</option><option value="2" >未完成</option><option value="4" >已取消</option></select>
    退单类型<select name="back_type" id="back_type"><option value="0">全部</option><option value="4" >退款</option><option value="1" >退货</option></select>
    <input type="submit" value=" 搜索 " class="button" />
  </form>
</div>

<!-- 订单列表 -->
<form method="post" action="back.php?act=remove_back" name="listForm" onsubmit="return check()">
  <div class="list-div" id="listDiv">

<table cellpadding="3" cellspacing="1">
  <tr>
  <th align=left><input onclick='listTable.selectAll(this, "back_id")' type="checkbox"/>序号</th>
    <th><a href="javascript:listTable.sort('order_sn', 'DESC'); ">原订单号</a></th>
		<th >退货/返修商品</th>
    <th><a href="javascript:listTable.sort('add_time', 'DESC'); ">申请时间</a></th>
	<th>应退金额</th>
	<th>实退金额</th>
    <th><a href="javascript:listTable.sort('consignee', 'DESC'); ">收货人</a></th>
    <!--<th><a href="javascript:listTable.sort('update_time', 'DESC'); ">签收时间</a><img src="images/sort_desc.gif"></th>-->
    <th>退换状态</th>
    <th>申请人</th>
    <th>操作</th>
  <tr>
    <tr>
  <td><input type="checkbox" name="back_id[]" value="1" />1</td>
    <td>2015072862934</td>
		<td >
		[ ID：55 ] &nbsp; &nbsp; 
	<a href="../goods.php?id=55" target="_blank">嘉云糖 300g玻璃罐装 水果硬糖 喜糖 德国进口</a><br />
		</td>
    <td align="center"  nowrap="nowrap">2015-07-28 12:48:26</td>
	<td>¥0.00</td>
	<td>¥0.00</td>
    <td align="right" > 喵喵 (手机：13912345678)<br>河北大街</td>
    <!--<td align="center" valign="top" nowrap="nowrap"></td>	-->
    <td align="center"  nowrap="nowrap">退款(无需退货)-已退款</td>
    <td align="center"  nowrap="nowrap">喵喵</td>
    <td align="center"   nowrap="nowrap">
     <a href="back.php?act=back_info&back_id=1">查看</a>
     <a onclick="{if(confirm('您确定要删除吗？')){return true;}return false;}" href="back.php?act=remove_back&back_id=1">移除</a>
    </td>
  </tr>
    <tr>
  <td><input type="checkbox" name="back_id[]" value="3" />3</td>
    <td>2015092380127</td>
		<td >
		[ ID：183 ] &nbsp; &nbsp; 
	<a href="../goods.php?id=183" target="_blank">幻响（i-mu）百变羊 创意指环扣 手机支架 双指环 360度旋转 防止手机滑落 金属材质 强力粘胶</a><br />
		</td>
    <td align="center"  nowrap="nowrap">2015-09-23 09:47:20</td>
	<td>¥18.90</td>
	<td>¥0.00</td>
    <td align="right" > 111111 (手机：13111111111)<br>111111</td>
    <!--<td align="center" valign="top" nowrap="nowrap"></td>	-->
    <td align="center"  nowrap="nowrap">退款(无需退货)-未退款</td>
    <td align="center"  nowrap="nowrap">111111</td>
    <td align="center"   nowrap="nowrap">
     <a href="back.php?act=back_info&back_id=3">查看</a>
     <a onclick="{if(confirm('您确定要删除吗？')){return true;}return false;}" href="back.php?act=remove_back&back_id=3">移除</a>
    </td>
  </tr>
    <tr>
  <td><input type="checkbox" name="back_id[]" value="4" />4</td>
    <td>2015092308424</td>
		<td >
		[ ID：183 ] &nbsp; &nbsp; 
	<a href="../goods.php?id=183" target="_blank">幻响（i-mu）百变羊 创意指环扣 手机支架 双指环 360度旋转 防止手机滑落 金属材质 强力粘胶</a><br />
		</td>
    <td align="center"  nowrap="nowrap">2015-09-23 09:54:26</td>
	<td>¥18.90</td>
	<td>¥0.00</td>
    <td align="right" > 111111 (手机：13111111111)<br>111111</td>
    <!--<td align="center" valign="top" nowrap="nowrap"></td>	-->
    <td align="center"  nowrap="nowrap">寄回商品超期，申请被自动取消-未退款</td>
    <td align="center"  nowrap="nowrap">111111</td>
    <td align="center"   nowrap="nowrap">
     <a href="back.php?act=back_info&back_id=4">查看</a>
     <a onclick="{if(confirm('您确定要删除吗？')){return true;}return false;}" href="back.php?act=remove_back&back_id=4">移除</a>
    </td>
  </tr>
    <tr>
  <td><input type="checkbox" name="back_id[]" value="6" />6</td>
    <td>2016022451812</td>
		<td >
		[ ID：51 ] &nbsp; &nbsp; 
	<a href="../goods.php?id=51" target="_blank">高端2015夏装新款修身淑坊女女装蕾丝短袖复女连衣裙装</a><br />
		</td>
    <td align="center"  nowrap="nowrap">2016-02-24 13:22:28</td>
	<td>¥169.15</td>
	<td>¥0.00</td>
    <td align="right" > sdf (手机：13245855555)<br>sdf dsf 考虑到进水阀链接咖啡机</td>
    <!--<td align="center" valign="top" nowrap="nowrap"></td>	-->
    <td align="center"  nowrap="nowrap">寄回商品超期，申请被自动取消-未退款</td>
    <td align="center"  nowrap="nowrap">sdf</td>
    <td align="center"   nowrap="nowrap">
     <a href="back.php?act=back_info&back_id=6">查看</a>
     <a onclick="{if(confirm('您确定要删除吗？')){return true;}return false;}" href="back.php?act=remove_back&back_id=6">移除</a>
    </td>
  </tr>
    <tr>
  <td><input type="checkbox" name="back_id[]" value="7" />7</td>
    <td>2016022097688</td>
		<td >
		[ ID：231 ] &nbsp; &nbsp; 
	<a href="../goods.php?id=231" target="_blank">可爱卡通餐盘水果盘点心盘 盘子儿童托盘餐具6件套</a><br />
		</td>
    <td align="center"  nowrap="nowrap">2016-02-24 13:23:20</td>
	<td>¥99.00</td>
	<td>¥0.00</td>
    <td align="right" > sdf (手机：13245855555)<br>sdf dsf 考虑到进水阀链接咖啡机</td>
    <!--<td align="center" valign="top" nowrap="nowrap"></td>	-->
    <td align="center"  nowrap="nowrap">寄回商品超期，申请被自动取消-未退款</td>
    <td align="center"  nowrap="nowrap">sdf</td>
    <td align="center"   nowrap="nowrap">
     <a href="back.php?act=back_info&back_id=7">查看</a>
     <a onclick="{if(confirm('您确定要删除吗？')){return true;}return false;}" href="back.php?act=remove_back&back_id=7">移除</a>
    </td>
  </tr>
    <tr>
  <td><input type="checkbox" name="back_id[]" value="8" />8</td>
    <td>2016022590434</td>
		<td >
		[ ID：200 ] &nbsp; &nbsp; 
	<a href="../goods.php?id=200" target="_blank">爱度AY800蓝牙音箱手机电脑迷你音响无线便携插卡低音炮 带蓝牙自拍 土豪金</a><br />
		</td>
    <td align="center"  nowrap="nowrap">2016-02-25 21:29:26</td>
	<td>¥0.00</td>
	<td>¥0.00</td>
    <td align="right" > 123 (手机：13245855555)<br>圣达菲</td>
    <!--<td align="center" valign="top" nowrap="nowrap"></td>	-->
    <td align="center"  nowrap="nowrap">退款(无需退货)-未退款</td>
    <td align="center"  nowrap="nowrap">123</td>
    <td align="center"   nowrap="nowrap">
     <a href="back.php?act=back_info&back_id=8">查看</a>
     <a onclick="{if(confirm('您确定要删除吗？')){return true;}return false;}" href="back.php?act=remove_back&back_id=8">移除</a>
    </td>
  </tr>
    <tr>
  <td><input type="checkbox" name="back_id[]" value="9" />9</td>
    <td>2016031897028</td>
		<td >
		[ ID：278 ] &nbsp; &nbsp; 
	<a href="../goods.php?id=278" target="_blank">西门子开关插座面板 悦动白墙壁二位双控带荧光开关</a><br />
		</td>
    <td align="center"  nowrap="nowrap">2016-03-18 19:09:15</td>
	<td>¥21.60</td>
	<td>¥0.00</td>
    <td align="right" > xxx (手机：13527894748)<br>fghfghfghfgh</td>
    <!--<td align="center" valign="top" nowrap="nowrap"></td>	-->
    <td align="center"  nowrap="nowrap">用户自行取消申请-未退款</td>
    <td align="center"  nowrap="nowrap">xxx</td>
    <td align="center"   nowrap="nowrap">
     <a href="back.php?act=back_info&back_id=9">查看</a>
     <a onclick="{if(confirm('您确定要删除吗？')){return true;}return false;}" href="back.php?act=remove_back&back_id=9">移除</a>
    </td>
  </tr>
  </table>

<!-- 分页 -->
<table id="page-table" cellspacing="0">
  <tr>
    <td align="right" nowrap="true">
          <!-- $Id: page.htm 14216 2008-03-10 02:27:21Z testyang $ -->
            <div id="turn-page">
        总计  <span id="totalRecords">7</span>
        个记录分为 <span id="totalPages">1</span>
        页当前第 <span id="pageCurrent">1</span>
        页，每页 <input type='text' size='3' id='pageSize' value="15" onkeypress="return listTable.changePageSize(event)" />
        <span id="page-link">
          <a href="javascript:listTable.gotoPageFirst()">第一页</a>
          <a href="javascript:listTable.gotoPagePrev()">上一页</a>
          <a href="javascript:listTable.gotoPageNext()">下一页</a>
          <a href="javascript:listTable.gotoPageLast()">最末页</a>
        <!--   <select id="gotoPage" onchange="listTable.gotoPage(this.value)">
            <option value='1'>1</option>          </select> -->
         跳转到 <input id="pageInput" title="按回车键跳转到指定页数" value="1" style="width:25px;"  onkeypress="if(event.keyCode==13) {listTable.gotoPage(this.value);}"   type="text">
          <input class="button" id="pagerBtn" value="GO" onclick="goToPage()" type="button">
        </span>
      </div>
<script>
function goToPage(){
	var page=$("#pageInput").val();
	listTable.gotoPage(page);
}
</script>    </td>
  </tr>
</table>

  </div>
  <div>
    <input name="remove_back" type="submit" id="btnSubmit3" value="移除" class="button" disabled="true" onclick="{if(confirm('您确定要删除吗？')){return true;}return false;}" />
  </div>
</form>
<script language="JavaScript">
listTable.recordCount = 7;
listTable.pageCount = 1;

listTable.filter.delivery_sn = '';
listTable.filter.order_sn = '';
listTable.filter.order_id = '0';
listTable.filter.order_type = '0';
listTable.filter.back_type = '0';
listTable.filter.consignee = '';
listTable.filter.sort_by = 'status_back ASC, update_time';
listTable.filter.sort_order = 'DESC';
listTable.filter.supp = '0';
listTable.filter.suppid = '0';
listTable.filter.page = '1';
listTable.filter.page_size = '15';
listTable.filter.record_count = '7';
listTable.filter.page_count = '1';


    onload = function()
    {
        // 开始检查订单
        startCheckOrder();
                
        //
        listTable.query = "back_query";
    }

    /**
     * 搜索订单
     */
    function searchOrder()
    {
        listTable.filter['order_sn'] = Utils.trim(document.forms['searchForm'].elements['order_sn'].value);
        listTable.filter['consignee'] = Utils.trim(document.forms['searchForm'].elements['consignee'].value);
        //listTable.filter['delivery_sn'] = document.forms['searchForm'].elements['delivery_sn'].value;
				listTable.filter['order_type'] = document.forms['searchForm'].elements['order_type'].value;
		listTable.filter['back_type'] = document.forms['searchForm'].elements['back_type'].value;
		
		
        listTable.filter['page'] = 1;
        listTable.query = "back_query";
        listTable.loadList();
    }

    function check()
    {
      var snArray = new Array();
      var eles = document.forms['listForm'].elements;
      for (var i=0; i<eles.length; i++)
      {
        if (eles[i].tagName == 'INPUT' && eles[i].type == 'checkbox' && eles[i].checked && eles[i].value != 'on')
        {
          snArray.push(eles[i].value);
        }
      }
      if (snArray.length == 0)
      {
        return false;
      }
      else
      {
        eles['order_id'].value = snArray.toString();
        return true;
      }
    }
</script>


<div id="footer">
共执行 11 个查询，用时 0.023002 秒，Gzip 已禁用，内存占用 4.141 MB<br />
版权所有 &copy; 2008-2030 广州网软志成信息科技有限公司，并保留所有权利。</div>
<!-- 新订单提示信息 -->
<div id="popMsg">
  <table cellspacing="0" cellpadding="0" width="100%" bgcolor="#cfdef4" border="0">
  <tr>
    <td style="color: #0f2c8c" width="30" height="24"></td>
    <td style="font-weight: normal; color: #1f336b; padding-top: 4px;padding-left: 4px" valign="center" width="100%"> 新订单通知</td>
    <td style="padding-top: 2px;padding-right:2px" valign="center" align="right" width="19"><span title="关闭" style="cursor: hand;cursor:pointer;color:red;font-size:12px;font-weight:bold;margin-right:4px;" onclick="Message.close()" >×</span><!-- <img title=关闭 style="cursor: hand" onclick=closediv() hspace=3 src="msgclose.jpg"> --></td>
  </tr>
  <tr>
    <td style="padding-right: 1px; padding-bottom: 1px" colspan="3" height="70">
    <div id="popMsgContent">
      <p>您有 <strong style="color:#ff0000" id="spanNewOrder">1</strong> 个新订单以及       <strong style="color:#ff0000" id="spanNewPaid">0</strong> 个新付款的订单</p>
      <p align="center" style="word-break:break-all"><a href="order.php?act=list"><span style="color:#ff0000">点击查看新订单</span></a></p>
    </div>
    </td>
  </tr>
  </table>
</div>

<!--
<embed src="images/online.wav" width="0" height="0" autostart="false" name="msgBeep" id="msgBeep" enablejavascript="true"/>
-->
<object classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000" codebase="http://active.macromedia.com/flash2/cabs/swflash.cab#version=4,0,0,0" id="msgBeep" width="1" height="1">
  <param name="movie" value="images/online.swf">
  <param name="quality" value="high">
  <embed src="images/online.swf" name="msgBeep" id="msgBeep" quality="high" width="0" height="0" type="application/x-shockwave-flash" pluginspage="http://www.macromedia.com/shockwave/download/index.cgi?p1_prod_version=shockwaveflash">
  </embed>
</object>

<script language="JavaScript">
document.onmousemove=function(e)
{
  var obj = Utils.srcElement(e);
  if (typeof(obj.onclick) == 'function' && obj.onclick.toString().indexOf('listTable.edit') != -1)
  {
    obj.title = '点击修改内容';
    obj.style.cssText = 'background: #278296;';
    obj.onmouseout = function(e)
    {
      this.style.cssText = '';
    }
  }
  else if (typeof(obj.href) != 'undefined' && obj.href.indexOf('listTable.sort') != -1)
  {
    obj.title = '点击对列表排序';
  }
}
<!--


var MyTodolist;
function showTodoList(adminid)
{
  if(!MyTodolist)
  {
    var global = $import("../js/global.js","js");
    global.onload = global.onreadystatechange= function()
    {
      if(this.readyState && this.readyState=="loading")return;
      var md5 = $import("js/md5.js","js");
      md5.onload = md5.onreadystatechange= function()
      {
        if(this.readyState && this.readyState=="loading")return;
        var todolist = $import("js/todolist.js","js");
        todolist.onload = todolist.onreadystatechange = function()
        {
          if(this.readyState && this.readyState=="loading")return;
          MyTodolist = new Todolist();
          MyTodolist.show();
        }
      }
    }
  }
  else
  {
    if(MyTodolist.visibility)
    {
      MyTodolist.hide();
    }
    else
    {
      MyTodolist.show();
    }
  }
}

if (Browser.isIE)
{
  onscroll = function()
  {
    //document.getElementById('calculator').style.top = document.body.scrollTop;
    document.getElementById('popMsg').style.top = (document.body.scrollTop + document.body.clientHeight - document.getElementById('popMsg').offsetHeight) + "px";
  }
}

if (document.getElementById("listDiv"))
{
  document.getElementById("listDiv").onmouseover = function(e)
  {
    obj = Utils.srcElement(e);

    if (obj)
    {
      if (obj.parentNode.tagName.toLowerCase() == "tr") row = obj.parentNode;
      else if (obj.parentNode.parentNode.tagName.toLowerCase() == "tr") row = obj.parentNode.parentNode;
      else return;

      for (i = 0; i < row.cells.length; i++)
      {
        if (row.cells[i].tagName != "TH") row.cells[i].style.backgroundColor = '#F4FAFB';
      }
    }

  }

  document.getElementById("listDiv").onmouseout = function(e)
  {
    obj = Utils.srcElement(e);

    if (obj)
    {
      if (obj.parentNode.tagName.toLowerCase() == "tr") row = obj.parentNode;
      else if (obj.parentNode.parentNode.tagName.toLowerCase() == "tr") row = obj.parentNode.parentNode;
      else return;

      for (i = 0; i < row.cells.length; i++)
      {
          if (row.cells[i].tagName != "TH") row.cells[i].style.backgroundColor = '';
      }
    }
  }

  document.getElementById("listDiv").onclick = function(e)
  {
    var obj = Utils.srcElement(e);

    if (obj.tagName == "INPUT" && obj.type == "checkbox")
    {
      if (!document.forms['listForm'])
      {
        return;
      }
      var nodes = document.forms['listForm'].elements;
      var checked = false;

      for (i = 0; i < nodes.length; i++)
      {
        if (nodes[i].checked)
        {
           checked = true;
           break;
         }
      }

      if(document.getElementById("btnSubmit"))
      {
        document.getElementById("btnSubmit").disabled = !checked;
      }
      for (i = 1; i <= 10; i++)
      {
        if (document.getElementById("btnSubmit" + i))
        {
          document.getElementById("btnSubmit" + i).disabled = !checked;
        }
      }
    }
  }

}

//-->
</script>
</body>
</html>

<!-- $Id: order_info.htm 15544 2009-01-09 01:54:28Z zblikai $ -->

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>管理中心 - 退货单操作：查看 </title>
<meta name="robots" content="noindex, nofollow">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link href="styles/general.css" rel="stylesheet" type="text/css" />
<link href="styles/main.css" rel="stylesheet" type="text/css" />
<link href="styles/chosen/chosen.css" rel="stylesheet" type="text/css">
<script type="text/javascript" src="js/jquery-1.6.2.min.js"></script><script type="text/javascript" src="js/chosen.jquery.min.js"></script><script type="text/javascript" src="js/jquery.json.js"></script><script type="text/javascript" src="../js/transport.js"></script><script type="text/javascript" src="js/common.js"></script><script language="JavaScript">
<!--
// 这里把JS用到的所有语言都赋值到这里
var process_request = "正在处理您的请求...";
var todolist_caption = "记事本";
var todolist_autosave = "自动保存";
var todolist_save = "保存";
var todolist_clear = "清除";
var todolist_confirm_save = "是否将更改保存到记事本？";
var todolist_confirm_clear = "是否清空内容？";
var remove_confirm = "删除订单将清除该订单的所有信息。您确定要这么做吗？";
var confirm_merge = "您确实要合并这两个订单吗？";
var input_price = "自定义价格";
var pls_search_user = "请搜索并选择会员";
var confirm_drop = "确认要删除该商品吗？";
var invalid_goods_number = "商品数量不正确";
var pls_search_goods = "请搜索并选择商品";
var pls_select_area = "请完整选择所在地区";
var pls_select_shipping = "请选择配送方式";
var pls_select_payment = "请选择支付方式";
var pls_select_pack = "请选择包装";
var pls_select_card = "请选择贺卡";
var pls_input_note = "请您填写备注！";
var pls_input_cancel = "请您填写取消原因！";
var pls_select_refund = "请选择退款方式！";
var pls_select_agency = "请选择办事处！";
var pls_select_other_agency = "该订单现在就属于这个办事处，请选择其他办事处！";
var loading = "加载中...";
//-->
</script>
</head>
<body>

<div id="menu_list" onmouseover="show_popup()" onmouseout="hide_popup()">
<ul>
<li><a href="goods.php?act=add" target="main_frame">添加新商品</a></li>
<li><a href="category.php?act=add" target="main_frame">添加商品分类</a></li>
<li><a href="order.php?act=add" target="main_frame">添加订单</a></li>
<li><a href="article.php?act=add" target="main_frame">添加新文章</a></li>
<li><a href="users.php?act=add" target="main_frame">添加会员</a></li>
</ul>
</div>
<script>
function show_popup(){
frmBody = parent.document.getElementById('frame-body');
if (frmBody.cols == "37, 12, *")
{
parent.main_frame.document.getElementById('menu_list').style.left = '195px';
}
else
{
parent.main_frame.document.getElementById('menu_list').style.left = '40px';
}
parent.main_frame.document.getElementById('menu_list').style.display = 'block';
}
function hide_popup(){

parent.main_frame.document.getElementById('menu_list').style.display = 'none';
}
</script>
<h1>
<span class="action-span"><a href="back.php?act=back_list&uselastfilter=1">退款/退货及维修</a></span>
<span class="action-span1"><a href="index.php?act=main">管理中心</a> </span><span id="search_id" class="action-span1"> - 退货单操作：查看 </span>
<div style="clear:both"></div>
</h1>
<script type="text/javascript" src="js/topbar.js"></script><script type="text/javascript" src="../js/utils.js"></script><script type="text/javascript" src="js/listtable.js"></script><script type="text/javascript" src="js/selectzone.js"></script><script type="text/javascript" src="../js/common.js"></script>
<div class="list-div" style="margin-bottom: 5px">
<table width="100%" cellpadding="3" cellspacing="1">
  <tr>
    <th colspan="4">原订单基本信息</th>
  </tr>

  <tr>
    <td width="18%"><div align="right"><strong>订单号：</strong></div></td>
   <td width="34%">2015072862934    <td><div align="right"><strong>下单时间：</strong></div></td>
    <td></td>
  </tr>
  <tr>
    <td width="18%"><div align="right"><strong>服务类型：</strong></div></td>
   <td width="34%">
                退款（无需退货）   </td>
    <td><div align="right"><strong>退款方式：</strong></div></td>
    <td>
    	退款至账户余额            </td>
  </tr>
  <tr>
    <td><div align="right"><strong>购货人：</strong></div></td>
    <td>wrzcyy</td>
    <td><div align="right"><strong>缺货处理：</strong></div></td>
    <td></td>
  </tr>
  <tr>
    <td><div align="right"><strong>配送方式：</strong></div></td>
    <td> </td>
    <td><div align="right"><strong>配送费用：</strong></div></td>
    <td></td>
  </tr>
  <tr>
    <td><div align="right"><strong>是否保价：</strong></div></td>
    <td>否</td>
    <td><div align="right"><strong>保价费用：</strong></div></td>
    <td>0.00</td>
  </tr>
  <tr>
    <td><div align="right"><strong>发货单号：</strong></div></td>
    <td ></td>
	<td><div align="right"><strong>发货时间：</strong></div></td>
    <td></td>
  </tr>
  </table>
</div>
<div class="list-div" style="margin-bottom: 5px">
<table width="100%" cellpadding="3" cellspacing="1">
  <tr>
    <th colspan="4">退款/退货/返修信息</th>
    </tr>
  <tr>
    <td><div align="right"><strong>申请退货/维修时间：</strong></div></td>
    <td>2015-07-28 12:48:26</td>
    <td><div align="right"><strong>申请人用户名：</strong></div></td>
    <td>wrzcyy</td>
  </tr>
  <tr>
    <td><div align="right"><strong>换回商品收件人：</strong></div></td>
    <td>喵喵</td>
    <td><div align="right"><strong>联系电话：</strong></div></td>
    <td></td>
  </tr>
  <tr>
    <td><div align="right"><strong>换回商品收货人地址：</strong></div></td>
    <td >中国 北京 北京 东城区 河北大街</td>
	 <td><div align="right"><strong>邮编：</strong></div></td>
    <td></td>
  </tr>
  <tr>
    <td><div align="right"><strong>退货/维修原因：</strong></div></td>
    <td colspan=3></td>
	</tr>
  <tr>
    <td><div align="right"><strong>用户退回商品所用快递：</strong></div></td>
    <td></td>
    <td><div align="right"><strong>运单号：</strong></div></td>
    <td></td>
  </tr>
	<tr>
    <td><div align="right"><strong>图片：</strong></div></td>
    <td colspan=3>
    <!---->
    </td>
  </tr>
</table>
</div>
<div class="list-div" style="margin-bottom: 5px">
<table width="100%" cellpadding="3" cellspacing="1">
  <tr>
    <th colspan="2">客户留言</th>
  </tr>
    <!---->
  
  <tr>
    <td width="18%"></td>
    <td>
    	<script>
		function check_replay()
		{
			if (document.getElementById("message").value == '')
			{
				alert("请输入回复内容！");
				document.getElementById("message").focus();
				return false;	
			}
		}
		</script>
    	<form action="back.php?act=replay" method="post" onsubmit="return check_replay()">
        <input name="back_id" type="hidden" value="1">
    	<div><textarea id="message" name="message" style="width:500px; height:60px;"></textarea></div>
        <div><input type="submit" value="回复" /></div>
        </form>
    </td>
  </tr>
</table>
</div>



<div class="list-div" style="margin-bottom: 5px">

<table width="100%" cellpadding="3" cellspacing="1">
  <tr>
    <th colspan="7" scope="col">原订单-商品信息</th>
    </tr>
  <tr>
    <td scope="col"><div align="center"><strong>商品名称 [ 品牌 ]</strong></div></td>
    <td scope="col"><div align="center"><strong>货号</strong></div></td>
    <td scope="col"><div align="center"><strong>货品号</strong></div></td>
    <td scope="col"><div align="center"><strong>属性</strong></div></td>
    <td colspan=3><div align="center"><strong>数量</strong></div></td>
  </tr>
   <tr>
    <th colspan="7" scope="col">退货/返修-商品信息</th>
    </tr>
  <tr>
    <td scope="col"><div align="center"><strong>商品名称 [ 品牌 ]</strong></div></td>
    <td scope="col"><div align="center"><strong>货号</strong></div></td>
    <td scope="col"><div align="center"><strong>货品号</strong></div></td>
    <td scope="col"><div align="center"><strong>属性</strong></div></td>
	<td scope="col"><div align="center"><strong>业务</strong></div></td>
	<td scope="col"><div align="center"><strong>应退金额</strong></div></td>
    <td scope="col" ><div align="center"><strong>数量</strong></div></td>
  </tr>
    <tr>
    <td>
        <a href="../goods.php?id=55" target="_blank">嘉云糖 300g玻璃罐装 水果硬糖 喜糖 德国进口         </td>
    <td><div align="left">55</div></td>
    <td><div align="left">0</div></td>
    <td><div align="left"></div></td>
	<td>
	    	    	    	    退款（无需退货）	
	</td>
	<td></td>
    <td><div align="left">1</div></td>
  </tr>
      <tr>
  <td colspan="7"><strong>此单为整单退款，订单已付金额（应退金额）：</strong></td>
  </tr>
  </table>
</div>

<div class="list-div" style="margin-bottom: 5px">
<form  action="back.php?act=operate" method="post">
<table cellpadding="3" cellspacing="1">
  <tr>
    <th colspan="6">操作信息</th>
  </tr>
  <tr>
    <td><div align="right"><strong>操作备注：</strong></div></td>
  <td colspan="5"><textarea name="action_note" cols="80" rows="3"></textarea></td>
    </tr>
  <tr>
    <td><div align="right"></div>
      <div align="right"><strong>当前可执行操作：</strong> </div></td>
    <td colspan="5">
						 
									
			 
        		<input name="after_service" type="submit" value="售后" class="button" />       
		<input name="back_id" type="hidden" value="1">
		</td>
  </tr>
  <tr>
    <th>操作者：</th>
    <th>操作时间</th>
    <th>退换货状态</th>
    <th>退款状态</th>
    <th>备注</th>
  </tr>
    <tr>
    <td><div align="center">wrzcnet_b2b2c</div></td>
    <td><div align="center">2016-03-19 11:24:26</div></td>
    <td><div align="center">
        退款（无需退货）
        </div></td>
    <td><div align="center">已退款</div></td>
    <td>[售后] 范德萨发</td>
  </tr>
    <tr>
    <td><div align="center">wrzcnet_b2b2c</div></td>
    <td><div align="center">2016-03-16 14:29:03</div></td>
    <td><div align="center">
        退款（无需退货）
        </div></td>
    <td><div align="center">已退款</div></td>
    <td>[售后] 56656</td>
  </tr>
    <tr>
    <td><div align="center">wrzcnet_b2b2c</div></td>
    <td><div align="center">2016-03-16 14:28:47</div></td>
    <td><div align="center">
        退款（无需退货）
        </div></td>
    <td><div align="center">已退款</div></td>
    <td>[售后] 45415</td>
  </tr>
    <tr>
    <td><div align="center">admin</div></td>
    <td><div align="center">2015-09-23 09:44:37</div></td>
    <td><div align="center">
        退款（无需退货）
        </div></td>
    <td><div align="center">已退款</div></td>
    <td></td>
  </tr>
    <tr>
    <td><div align="center">admin</div></td>
    <td><div align="center">2015-09-23 09:44:33</div></td>
    <td><div align="center">
        退款（无需退货）
        </div></td>
    <td><div align="center">已退款</div></td>
    <td>f</td>
  </tr>
    <tr>
    <td><div align="center">admin</div></td>
    <td><div align="center">2015-07-28 12:48:37</div></td>
    <td><div align="center">
        退款（无需退货）
        </div></td>
    <td><div align="center">未退款</div></td>
    <td></td>
  </tr>
  </table>
</form>
</div>



<script language="JavaScript">

  var oldAgencyId = 0;

  onload = function()
  {
    // 开始检查订单
    startCheckOrder();
  }

</script>


<div id="footer">
共执行 16 个查询，用时 0.029001 秒，Gzip 已禁用，内存占用 4.114 MB<br />
版权所有 &copy; 2008-2030 广州网软志成信息科技有限公司，并保留所有权利。</div>
<!-- 新订单提示信息 -->
<div id="popMsg">
  <table cellspacing="0" cellpadding="0" width="100%" bgcolor="#cfdef4" border="0">
  <tr>
    <td style="color: #0f2c8c" width="30" height="24"></td>
    <td style="font-weight: normal; color: #1f336b; padding-top: 4px;padding-left: 4px" valign="center" width="100%"> 新订单通知</td>
    <td style="padding-top: 2px;padding-right:2px" valign="center" align="right" width="19"><span title="关闭" style="cursor: hand;cursor:pointer;color:red;font-size:12px;font-weight:bold;margin-right:4px;" onclick="Message.close()" >×</span><!-- <img title=关闭 style="cursor: hand" onclick=closediv() hspace=3 src="msgclose.jpg"> --></td>
  </tr>
  <tr>
    <td style="padding-right: 1px; padding-bottom: 1px" colspan="3" height="70">
    <div id="popMsgContent">
      <p>您有 <strong style="color:#ff0000" id="spanNewOrder">1</strong> 个新订单以及       <strong style="color:#ff0000" id="spanNewPaid">0</strong> 个新付款的订单</p>
      <p align="center" style="word-break:break-all"><a href="order.php?act=list"><span style="color:#ff0000">点击查看新订单</span></a></p>
    </div>
    </td>
  </tr>
  </table>
</div>

<!--
<embed src="images/online.wav" width="0" height="0" autostart="false" name="msgBeep" id="msgBeep" enablejavascript="true"/>
-->
<object classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000" codebase="http://active.macromedia.com/flash2/cabs/swflash.cab#version=4,0,0,0" id="msgBeep" width="1" height="1">
  <param name="movie" value="images/online.swf">
  <param name="quality" value="high">
  <embed src="images/online.swf" name="msgBeep" id="msgBeep" quality="high" width="0" height="0" type="application/x-shockwave-flash" pluginspage="http://www.macromedia.com/shockwave/download/index.cgi?p1_prod_version=shockwaveflash">
  </embed>
</object>

<script language="JavaScript">
document.onmousemove=function(e)
{
  var obj = Utils.srcElement(e);
  if (typeof(obj.onclick) == 'function' && obj.onclick.toString().indexOf('listTable.edit') != -1)
  {
    obj.title = '点击修改内容';
    obj.style.cssText = 'background: #278296;';
    obj.onmouseout = function(e)
    {
      this.style.cssText = '';
    }
  }
  else if (typeof(obj.href) != 'undefined' && obj.href.indexOf('listTable.sort') != -1)
  {
    obj.title = '点击对列表排序';
  }
}
<!--


var MyTodolist;
function showTodoList(adminid)
{
  if(!MyTodolist)
  {
    var global = $import("../js/global.js","js");
    global.onload = global.onreadystatechange= function()
    {
      if(this.readyState && this.readyState=="loading")return;
      var md5 = $import("js/md5.js","js");
      md5.onload = md5.onreadystatechange= function()
      {
        if(this.readyState && this.readyState=="loading")return;
        var todolist = $import("js/todolist.js","js");
        todolist.onload = todolist.onreadystatechange = function()
        {
          if(this.readyState && this.readyState=="loading")return;
          MyTodolist = new Todolist();
          MyTodolist.show();
        }
      }
    }
  }
  else
  {
    if(MyTodolist.visibility)
    {
      MyTodolist.hide();
    }
    else
    {
      MyTodolist.show();
    }
  }
}

if (Browser.isIE)
{
  onscroll = function()
  {
    //document.getElementById('calculator').style.top = document.body.scrollTop;
    document.getElementById('popMsg').style.top = (document.body.scrollTop + document.body.clientHeight - document.getElementById('popMsg').offsetHeight) + "px";
  }
}

if (document.getElementById("listDiv"))
{
  document.getElementById("listDiv").onmouseover = function(e)
  {
    obj = Utils.srcElement(e);

    if (obj)
    {
      if (obj.parentNode.tagName.toLowerCase() == "tr") row = obj.parentNode;
      else if (obj.parentNode.parentNode.tagName.toLowerCase() == "tr") row = obj.parentNode.parentNode;
      else return;

      for (i = 0; i < row.cells.length; i++)
      {
        if (row.cells[i].tagName != "TH") row.cells[i].style.backgroundColor = '#F4FAFB';
      }
    }

  }

  document.getElementById("listDiv").onmouseout = function(e)
  {
    obj = Utils.srcElement(e);

    if (obj)
    {
      if (obj.parentNode.tagName.toLowerCase() == "tr") row = obj.parentNode;
      else if (obj.parentNode.parentNode.tagName.toLowerCase() == "tr") row = obj.parentNode.parentNode;
      else return;

      for (i = 0; i < row.cells.length; i++)
      {
          if (row.cells[i].tagName != "TH") row.cells[i].style.backgroundColor = '';
      }
    }
  }

  document.getElementById("listDiv").onclick = function(e)
  {
    var obj = Utils.srcElement(e);

    if (obj.tagName == "INPUT" && obj.type == "checkbox")
    {
      if (!document.forms['listForm'])
      {
        return;
      }
      var nodes = document.forms['listForm'].elements;
      var checked = false;

      for (i = 0; i < nodes.length; i++)
      {
        if (nodes[i].checked)
        {
           checked = true;
           break;
         }
      }

      if(document.getElementById("btnSubmit"))
      {
        document.getElementById("btnSubmit").disabled = !checked;
      }
      for (i = 1; i <= 10; i++)
      {
        if (document.getElementById("btnSubmit" + i))
        {
          document.getElementById("btnSubmit" + i).disabled = !checked;
        }
      }
    }
  }

}

//-->
</script><?php
include 'buy.php' ;
?>
</body>
</html>