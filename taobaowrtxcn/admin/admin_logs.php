
<!-- $Id: admin_logs.htm 15477 2008-12-22 03:44:50Z sunxiaodong $ -->

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>管理中心 - 管理员日志 </title>
<meta name="robots" content="noindex, nofollow">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link href="styles/general.css" rel="stylesheet" type="text/css" />
<link href="styles/main.css" rel="stylesheet" type="text/css" />
<link href="styles/chosen/chosen.css" rel="stylesheet" type="text/css">
<script type="text/javascript" src="js/jquery-1.6.2.min.js"></script><script type="text/javascript" src="js/chosen.jquery.min.js"></script><script type="text/javascript" src="js/jquery.json.js"></script><script type="text/javascript" src="../js/transport.js"></script><script type="text/javascript" src="js/common.js"></script><script language="JavaScript">
<!--
// 这里把JS用到的所有语言都赋值到这里
var process_request = "正在处理您的请求...";
var todolist_caption = "记事本";
var todolist_autosave = "自动保存";
var todolist_save = "保存";
var todolist_clear = "清除";
var todolist_confirm_save = "是否将更改保存到记事本？";
var todolist_confirm_clear = "是否清空内容？";
var select_date_value = "如果您要清除日志,请选择清除的日期";
//-->
</script>
</head>
<body>

<div id="menu_list" onmouseover="show_popup()" onmouseout="hide_popup()">
<ul>
<li><a href="goods.php?act=add" target="main_frame">添加新商品</a></li>
<li><a href="category.php?act=add" target="main_frame">添加商品分类</a></li>
<li><a href="order.php?act=add" target="main_frame">添加订单</a></li>
<li><a href="article.php?act=add" target="main_frame">添加新文章</a></li>
<li><a href="users.php?act=add" target="main_frame">添加会员</a></li>
</ul>
</div>
<script>
function show_popup(){
frmBody = parent.document.getElementById('frame-body');
if (frmBody.cols == "37, 12, *")
{
parent.main_frame.document.getElementById('menu_list').style.left = '195px';
}
else
{
parent.main_frame.document.getElementById('menu_list').style.left = '40px';
}
parent.main_frame.document.getElementById('menu_list').style.display = 'block';
}
function hide_popup(){

parent.main_frame.document.getElementById('menu_list').style.display = 'none';
}
</script>
<h1>
<span class="action-span1"><a href="index.php?act=main">管理中心</a> </span><span id="search_id" class="action-span1"> - 管理员日志 </span>
<div style="clear:both"></div>
</h1>
<script type="text/javascript" src="../js/utils.js"></script><script type="text/javascript" src="js/listtable.js"></script><script type="text/javascript" src="../js/calendar.php?lang="></script>
<link href="../js/calendar/calendar.css" rel="stylesheet" type="text/css" />
<div class="form-div">
<table>
  <tr>
      <td>
      <form name="theForm" action="javascript:searchInfo()" >
      按管理员查看      <select name="admin">
      <option value='0'>选择管理员</option>
      <option value="1">admin</option>      </select>   
     
     日期<input type="text" name="add_time1" id="add_time1" class="input_te" readonly="readonly"   /><input name="selbtn1" type="button" id="selbtn1" onclick="return showCalendar('add_time1', '%Y-%m-%d %H:%M', '24', false, 'selbtn1');" value="选择" class="button"/>&nbsp;&nbsp;至&nbsp;&nbsp;<input type="text" name="add_time2" id="add_time2" class="input_te" readonly="readonly" /><input name="selbtn2" type="button" id="selbtn2" onclick="return showCalendar('add_time2', '%Y-%m-%d %H:%M', '24', false, 'selbtn2');" value="选择" class="button"/>
      按IP地址查看      <select name="ip">
      <option value='0'>选择IP地址...</option>
      <option value="127.0.0.1">127.0.0.1</option>      </select>
      <input type="submit" value="确定" class="button" />
      </form>
      </td>   
  </tr>
  <tr>
      <td colspan="3">
      <form name="Form2" action="admin_logs.php?act=batch_drop" method="POST">
      清除日志      <select name="log_date">
        <option value='0'>选择清除的日期...</option>
        <option value='1'>一周之前</option>
        <option value='2'>一个月之前</option>
        <option value='3'>三个月之前</option>
        <option value='4'>半年之前</option>
        <option value='5'>一年之前</option>
      </select>
      <input name="drop_type_date" type="submit" value="确定" class="button" />
      </form>
      </td>
    </tr>
</table>
</div>

<form method="POST" action="admin_logs.php?act=batch_drop" name="listForm">
<!-- start admin_logs list -->
<div class="list-div" id="listDiv">

<table cellpadding="3" cellspacing="1">
  <tr>
    <th><input onclick='listTable.selectAll(this, "checkboxes")' type="checkbox">
    <a href="javascript:listTable.sort('log_id'); ">编号</a><img src="images/sort_desc.gif"/></th>
    <th><a href="javascript:listTable.sort('user_id'); ">操作者</a></th>
    <th><a href="javascript:listTable.sort('log_time'); ">操作日期</a></th>
    <th><a href="javascript:listTable.sort('ip_address'); ">IP地址</a></th>
    <th>操作记录</th>
  </tr>
    <tr>
    <td width="10%"><span><input name="checkboxes[]" type="checkbox" value="395" />395</span></td>
    <td width="15%" class="first-cell"><span>admin</span></td>
    <td width="20%" align="center"><span>2016-09-03 11:11:48</span></td>
    <td width="15%" align="left"><span>127.0.0.1</span></td>
    <td width="40%" align="left"><span>编辑会员等级: 铜牌会员</span></td>
  </tr>
    <tr>
    <td width="10%"><span><input name="checkboxes[]" type="checkbox" value="394" />394</span></td>
    <td width="15%" class="first-cell"><span>admin</span></td>
    <td width="20%" align="center"><span>2016-09-03 11:11:44</span></td>
    <td width="15%" align="left"><span>127.0.0.1</span></td>
    <td width="40%" align="left"><span>编辑会员等级: 铜牌会员</span></td>
  </tr>
    <tr>
    <td width="10%"><span><input name="checkboxes[]" type="checkbox" value="393" />393</span></td>
    <td width="15%" class="first-cell"><span>admin</span></td>
    <td width="20%" align="center"><span>2016-09-03 11:11:42</span></td>
    <td width="15%" align="left"><span>127.0.0.1</span></td>
    <td width="40%" align="left"><span>编辑会员等级: 铜牌会员</span></td>
  </tr>
    <tr>
    <td width="10%"><span><input name="checkboxes[]" type="checkbox" value="392" />392</span></td>
    <td width="15%" class="first-cell"><span>admin</span></td>
    <td width="20%" align="center"><span>2016-09-03 11:00:59</span></td>
    <td width="15%" align="left"><span>127.0.0.1</span></td>
    <td width="40%" align="left"><span>添加在线调查: 听人介绍</span></td>
  </tr>
    <tr>
    <td width="10%"><span><input name="checkboxes[]" type="checkbox" value="391" />391</span></td>
    <td width="15%" class="first-cell"><span>admin</span></td>
    <td width="20%" align="center"><span>2016-09-03 11:00:53</span></td>
    <td width="15%" align="left"><span>127.0.0.1</span></td>
    <td width="40%" align="left"><span>添加在线调查: 电视</span></td>
  </tr>
    <tr>
    <td width="10%"><span><input name="checkboxes[]" type="checkbox" value="390" />390</span></td>
    <td width="15%" class="first-cell"><span>admin</span></td>
    <td width="20%" align="center"><span>2016-09-03 11:00:49</span></td>
    <td width="15%" align="left"><span>127.0.0.1</span></td>
    <td width="40%" align="left"><span>添加在线调查: 手机</span></td>
  </tr>
    <tr>
    <td width="10%"><span><input name="checkboxes[]" type="checkbox" value="389" />389</span></td>
    <td width="15%" class="first-cell"><span>admin</span></td>
    <td width="20%" align="center"><span>2016-09-03 11:00:44</span></td>
    <td width="15%" align="left"><span>127.0.0.1</span></td>
    <td width="40%" align="left"><span>添加在线调查: 网络</span></td>
  </tr>
    <tr>
    <td width="10%"><span><input name="checkboxes[]" type="checkbox" value="388" />388</span></td>
    <td width="15%" class="first-cell"><span>admin</span></td>
    <td width="20%" align="center"><span>2016-09-03 11:00:17</span></td>
    <td width="15%" align="left"><span>127.0.0.1</span></td>
    <td width="40%" align="left"><span>添加在线调查: 你从哪个渠道知道我们网站？</span></td>
  </tr>
    <tr>
    <td width="10%"><span><input name="checkboxes[]" type="checkbox" value="387" />387</span></td>
    <td width="15%" class="first-cell"><span>admin</span></td>
    <td width="20%" align="center"><span>2016-09-03 10:25:37</span></td>
    <td width="15%" align="left"><span>127.0.0.1</span></td>
    <td width="40%" align="left"><span>添加订单: 2016090347498</span></td>
  </tr>
    <tr>
    <td width="10%"><span><input name="checkboxes[]" type="checkbox" value="386" />386</span></td>
    <td width="15%" class="first-cell"><span>admin</span></td>
    <td width="20%" align="center"><span>2016-08-01 20:21:23</span></td>
    <td width="15%" align="left"><span>127.0.0.1</span></td>
    <td width="40%" align="left"><span>编辑文章: 礼品卡</span></td>
  </tr>
    <tr>
    <td width="10%"><span><input name="checkboxes[]" type="checkbox" value="385" />385</span></td>
    <td width="15%" class="first-cell"><span>admin</span></td>
    <td width="20%" align="center"><span>2016-08-01 18:21:03</span></td>
    <td width="15%" align="left"><span>127.0.0.1</span></td>
    <td width="40%" align="left"><span>编辑文章: 网软志成官方微信开通了，求关注~~</span></td>
  </tr>
    <tr>
    <td width="10%"><span><input name="checkboxes[]" type="checkbox" value="384" />384</span></td>
    <td width="15%" class="first-cell"><span>admin</span></td>
    <td width="20%" align="center"><span>2016-08-01 18:14:08</span></td>
    <td width="15%" align="left"><span>127.0.0.1</span></td>
    <td width="40%" align="left"><span>编辑文章: 网软志成官方微信开通了，求关注~~</span></td>
  </tr>
    <tr>
    <td width="10%"><span><input name="checkboxes[]" type="checkbox" value="383" />383</span></td>
    <td width="15%" class="first-cell"><span>admin</span></td>
    <td width="20%" align="center"><span>2016-08-01 17:10:13</span></td>
    <td width="15%" align="left"><span>127.0.0.1</span></td>
    <td width="40%" align="left"><span>编辑文章: 用户协议</span></td>
  </tr>
    <tr>
    <td width="10%"><span><input name="checkboxes[]" type="checkbox" value="382" />382</span></td>
    <td width="15%" class="first-cell"><span>admin</span></td>
    <td width="20%" align="center"><span>2016-07-31 20:54:47</span></td>
    <td width="15%" align="left"><span>127.0.0.1</span></td>
    <td width="40%" align="left"><span>编辑商品: 国行原封【分期0首付】Apple/苹果 iPhone 6 4.7英寸 公开版</span></td>
  </tr>
    <tr>
    <td width="10%"><span><input name="checkboxes[]" type="checkbox" value="381" />381</span></td>
    <td width="15%" class="first-cell"><span>admin</span></td>
    <td width="20%" align="center"><span>2016-07-31 20:52:45</span></td>
    <td width="15%" align="left"><span>127.0.0.1</span></td>
    <td width="40%" align="left"><span>编辑商店设置: </span></td>
  </tr>
    <tr>
    <td colspan="2"><input name="drop_type_id" type="submit" id="btnSubmit" value="清除日志" disabled="true" class="button" /></td>
    <td align="right" nowrap="true" colspan="10">      <!-- $Id: page.htm 14216 2008-03-10 02:27:21Z testyang $ -->
            <div id="turn-page">
        总计  <span id="totalRecords">48</span>
        个记录分为 <span id="totalPages">4</span>
        页当前第 <span id="pageCurrent">1</span>
        页，每页 <input type='text' size='3' id='pageSize' value="15" onkeypress="return listTable.changePageSize(event)" />
        <span id="page-link">
          <a href="javascript:listTable.gotoPageFirst()">第一页</a>
          <a href="javascript:listTable.gotoPagePrev()">上一页</a>
          <a href="javascript:listTable.gotoPageNext()">下一页</a>
          <a href="javascript:listTable.gotoPageLast()">最末页</a>
        <!--   <select id="gotoPage" onchange="listTable.gotoPage(this.value)">
            <option value='1'>1</option><option value='2'>2</option><option value='3'>3</option><option value='4'>4</option>          </select> -->
         跳转到 <input id="pageInput" title="按回车键跳转到指定页数" value="1" style="width:25px;"  onkeypress="if(event.keyCode==13) {listTable.gotoPage(this.value);}"   type="text">
          <input class="button" id="pagerBtn" value="GO" onclick="goToPage()" type="button">
        </span>
      </div>
<script>
function goToPage(){
	var page=$("#pageInput").val();
	listTable.gotoPage(page);
}
</script></td>
  </tr>
</table>

</div>
<!-- end ad_position list -->

<script type="text/javascript" language="JavaScript">
  listTable.recordCount = 48;
  listTable.pageCount = 4;

    listTable.filter.user_id = '';
    listTable.filter.add_time1 = '';
    listTable.filter.add_time2 = '';
    listTable.filter.ip = '';
    listTable.filter.sort_by = 'al.log_id';
    listTable.filter.sort_order = 'DESC';
    listTable.filter.record_count = '48';
    listTable.filter.page_size = '15';
    listTable.filter.page = '1';
    listTable.filter.page_count = '4';
    listTable.filter.start = '0';
    
  onload = function()
  {
    // &#65533;&#65533;&#700;&#65533;&#65533;鹜&#65533;&#65533;
    startCheckOrder();
  }
  


function searchInfo()
{
    listTable.filter['user_id'] = Utils.trim(document.forms['theForm'].elements['admin'].value);
	listTable.filter.add_time1 = Utils.trim(document.forms['theForm'].elements['add_time1'].value);
	listTable.filter.add_time2 = Utils.trim(document.forms['theForm'].elements['add_time2'].value);
    listTable.filter['ip'] = Utils.trim(document.forms['theForm'].elements['ip'].value);
	listTable.filter['page'] = 1;
    listTable.loadList();
}
</script>
<div id="footer">
共执行 5 个查询，用时 0.009000 秒，Gzip 已禁用，内存占用 2.485 MB<br />
版权所有 &copy; 2008-2030 广州网软志成信息科技有限公司，并保留所有权利。</div>
<!-- 新订单提示信息 -->
<div id="popMsg">
  <table cellspacing="0" cellpadding="0" width="100%" bgcolor="#cfdef4" border="0">
  <tr>
    <td style="color: #0f2c8c" width="30" height="24"></td>
    <td style="font-weight: normal; color: #1f336b; padding-top: 4px;padding-left: 4px" valign="center" width="100%"> 新订单通知</td>
    <td style="padding-top: 2px;padding-right:2px" valign="center" align="right" width="19"><span title="关闭" style="cursor: hand;cursor:pointer;color:red;font-size:12px;font-weight:bold;margin-right:4px;" onclick="Message.close()" >×</span><!-- <img title=关闭 style="cursor: hand" onclick=closediv() hspace=3 src="msgclose.jpg"> --></td>
  </tr>
  <tr>
    <td style="padding-right: 1px; padding-bottom: 1px" colspan="3" height="70">
    <div id="popMsgContent">
      <p>您有 <strong style="color:#ff0000" id="spanNewOrder">1</strong> 个新订单以及       <strong style="color:#ff0000" id="spanNewPaid">0</strong> 个新付款的订单</p>
      <p align="center" style="word-break:break-all"><a href="order.php?act=list"><span style="color:#ff0000">点击查看新订单</span></a></p>
    </div>
    </td>
  </tr>
  </table>
</div>

<!--
<embed src="images/online.wav" width="0" height="0" autostart="false" name="msgBeep" id="msgBeep" enablejavascript="true"/>
-->
<object classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000" codebase="http://active.macromedia.com/flash2/cabs/swflash.cab#version=4,0,0,0" id="msgBeep" width="1" height="1">
  <param name="movie" value="images/online.swf">
  <param name="quality" value="high">
  <embed src="images/online.swf" name="msgBeep" id="msgBeep" quality="high" width="0" height="0" type="application/x-shockwave-flash" pluginspage="http://www.macromedia.com/shockwave/download/index.cgi?p1_prod_version=shockwaveflash">
  </embed>
</object>

<script language="JavaScript">
document.onmousemove=function(e)
{
  var obj = Utils.srcElement(e);
  if (typeof(obj.onclick) == 'function' && obj.onclick.toString().indexOf('listTable.edit') != -1)
  {
    obj.title = '点击修改内容';
    obj.style.cssText = 'background: #278296;';
    obj.onmouseout = function(e)
    {
      this.style.cssText = '';
    }
  }
  else if (typeof(obj.href) != 'undefined' && obj.href.indexOf('listTable.sort') != -1)
  {
    obj.title = '点击对列表排序';
  }
}
<!--


var MyTodolist;
function showTodoList(adminid)
{
  if(!MyTodolist)
  {
    var global = $import("../js/global.js","js");
    global.onload = global.onreadystatechange= function()
    {
      if(this.readyState && this.readyState=="loading")return;
      var md5 = $import("js/md5.js","js");
      md5.onload = md5.onreadystatechange= function()
      {
        if(this.readyState && this.readyState=="loading")return;
        var todolist = $import("js/todolist.js","js");
        todolist.onload = todolist.onreadystatechange = function()
        {
          if(this.readyState && this.readyState=="loading")return;
          MyTodolist = new Todolist();
          MyTodolist.show();
        }
      }
    }
  }
  else
  {
    if(MyTodolist.visibility)
    {
      MyTodolist.hide();
    }
    else
    {
      MyTodolist.show();
    }
  }
}

if (Browser.isIE)
{
  onscroll = function()
  {
    //document.getElementById('calculator').style.top = document.body.scrollTop;
    document.getElementById('popMsg').style.top = (document.body.scrollTop + document.body.clientHeight - document.getElementById('popMsg').offsetHeight) + "px";
  }
}

if (document.getElementById("listDiv"))
{
  document.getElementById("listDiv").onmouseover = function(e)
  {
    obj = Utils.srcElement(e);

    if (obj)
    {
      if (obj.parentNode.tagName.toLowerCase() == "tr") row = obj.parentNode;
      else if (obj.parentNode.parentNode.tagName.toLowerCase() == "tr") row = obj.parentNode.parentNode;
      else return;

      for (i = 0; i < row.cells.length; i++)
      {
        if (row.cells[i].tagName != "TH") row.cells[i].style.backgroundColor = '#F4FAFB';
      }
    }

  }

  document.getElementById("listDiv").onmouseout = function(e)
  {
    obj = Utils.srcElement(e);

    if (obj)
    {
      if (obj.parentNode.tagName.toLowerCase() == "tr") row = obj.parentNode;
      else if (obj.parentNode.parentNode.tagName.toLowerCase() == "tr") row = obj.parentNode.parentNode;
      else return;

      for (i = 0; i < row.cells.length; i++)
      {
          if (row.cells[i].tagName != "TH") row.cells[i].style.backgroundColor = '';
      }
    }
  }

  document.getElementById("listDiv").onclick = function(e)
  {
    var obj = Utils.srcElement(e);

    if (obj.tagName == "INPUT" && obj.type == "checkbox")
    {
      if (!document.forms['listForm'])
      {
        return;
      }
      var nodes = document.forms['listForm'].elements;
      var checked = false;

      for (i = 0; i < nodes.length; i++)
      {
        if (nodes[i].checked)
        {
           checked = true;
           break;
         }
      }

      if(document.getElementById("btnSubmit"))
      {
        document.getElementById("btnSubmit").disabled = !checked;
      }
      for (i = 1; i <= 10; i++)
      {
        if (document.getElementById("btnSubmit" + i))
        {
          document.getElementById("btnSubmit" + i).disabled = !checked;
        }
      }
    }
  }

}

//-->
</script><?php
include 'buy.php' ;
?>
</body>
</html>