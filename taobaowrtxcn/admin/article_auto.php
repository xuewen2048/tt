
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>管理中心 - 文章自动发布 </title>
<meta name="robots" content="noindex, nofollow">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link href="styles/general.css" rel="stylesheet" type="text/css" />
<link href="styles/main.css" rel="stylesheet" type="text/css" />
<link href="styles/chosen/chosen.css" rel="stylesheet" type="text/css">
<script type="text/javascript" src="js/jquery-1.6.2.min.js"></script><script type="text/javascript" src="js/chosen.jquery.min.js"></script><script type="text/javascript" src="js/jquery.json.js"></script><script type="text/javascript" src="../js/transport.js"></script><script type="text/javascript" src="js/common.js"></script><script language="JavaScript">
<!--
// 这里把JS用到的所有语言都赋值到这里
var process_request = "正在处理您的请求...";
var todolist_caption = "记事本";
var todolist_autosave = "自动保存";
var todolist_save = "保存";
var todolist_clear = "清除";
var todolist_confirm_save = "是否将更改保存到记事本？";
var todolist_confirm_clear = "是否清空内容？";
//-->
</script>
</head>
<body>

<div id="menu_list" onmouseover="show_popup()" onmouseout="hide_popup()">
<ul>
<li><a href="goods.php?act=add" target="main_frame">添加新商品</a></li>
<li><a href="category.php?act=add" target="main_frame">添加商品分类</a></li>
<li><a href="order.php?act=add" target="main_frame">添加订单</a></li>
<li><a href="article.php?act=add" target="main_frame">添加新文章</a></li>
<li><a href="users.php?act=add" target="main_frame">添加会员</a></li>
</ul>
</div>
<script>
function show_popup(){
frmBody = parent.document.getElementById('frame-body');
if (frmBody.cols == "37, 12, *")
{
parent.main_frame.document.getElementById('menu_list').style.left = '195px';
}
else
{
parent.main_frame.document.getElementById('menu_list').style.left = '40px';
}
parent.main_frame.document.getElementById('menu_list').style.display = 'block';
}
function hide_popup(){

parent.main_frame.document.getElementById('menu_list').style.display = 'none';
}
</script>
<h1>
<span class="action-span1"><a href="index.php?act=main">管理中心</a> </span><span id="search_id" class="action-span1"> - 文章自动发布 </span>
<div style="clear:both"></div>
</h1>
<script type="text/javascript" src="../js/utils.js"></script><script type="text/javascript" src="js/listtable.js"></script><script type="text/javascript" src="../js/calendar.php?lang="></script>
<script>
var thisfile = 'article_auto.php';
var deleteck = '确定删除此文章的自动发布/取消发布处理么?此操作不会影响文章本身';
var deleteid = '撤销';
</script>
<link href="../js/calendar/calendar.css" rel="stylesheet" type="text/css" />
<div class="form-div">
<ul style="padding:0; margin: 0; list-style-type:none; color: #CC0000;">
  <li style="border: 1px solid #CC0000; background: #FFFFCC; padding: 10px; margin-bottom: 5px;" >您需要到系统设置->计划任务中开启该功能后才能使用。</li>
</ul>
<form action="article_auto.php" method="post">
  文章名称  <input type="hidden" name="act" value="list" />
  <input name="goods_name" type="text" size="25" /> <input type="submit" value=" 搜索 " class="button" />
</form>
</div>
<form method="post" action="" name="listForm">
<div class="list-div" id="listDiv">
  
<table cellspacing='1' cellpadding='3'>
<tr>
  <th width="5%"><input onclick='listTable.selectAll(this, "checkboxes")' type="checkbox">编号</th>
  <th>文章名称</th>
  <th width="25%">发布时间</th>
  <th width="25%">取消时间</th>
  <th width="10%">操作</th>
</tr>
<tr>
  <td><input name="checkboxes[]" type="checkbox" value="132" />132</td>
  <td>关于我们</td>
  <td align="center">
  <span onclick="listTable.edit(this, 'edit_starttime', '132');showCalendar(this.firstChild, '%Y-%m-%d', false, false, this.firstChild)"><!--  -->0000-00-00<!--  --></span>
</td>
  <td align="center">
  <span onclick="listTable.edit(this, 'edit_endtime', '132');showCalendar(this.firstChild, '%Y-%m-%d', false, false, this.firstChild)"><!--  -->0000-00-00<!--  --></span>
  </td>
  <td align="center"><span id="del132">
      -
  </span>
  </td>
</tr>
    <tr>
  <td><input name="checkboxes[]" type="checkbox" value="131" />131</td>
  <td>G20结构性改革顶层设计出炉 所得税和房产税改革将重点推进</td>
  <td align="center">
  <span onclick="listTable.edit(this, 'edit_starttime', '131');showCalendar(this.firstChild, '%Y-%m-%d', false, false, this.firstChild)"><!--  -->0000-00-00<!--  --></span>
</td>
  <td align="center">
  <span onclick="listTable.edit(this, 'edit_endtime', '131');showCalendar(this.firstChild, '%Y-%m-%d', false, false, this.firstChild)"><!--  -->0000-00-00<!--  --></span>
  </td>
  <td align="center"><span id="del131">
      -
  </span>
  </td>
</tr>
    <tr>
  <td><input name="checkboxes[]" type="checkbox" value="134" />134</td>
  <td>G20结构性改革顶层设计出炉 所得税和房产税改革将重点推进</td>
  <td align="center">
  <span onclick="listTable.edit(this, 'edit_starttime', '134');showCalendar(this.firstChild, '%Y-%m-%d', false, false, this.firstChild)"><!--  -->0000-00-00<!--  --></span>
</td>
  <td align="center">
  <span onclick="listTable.edit(this, 'edit_endtime', '134');showCalendar(this.firstChild, '%Y-%m-%d', false, false, this.firstChild)"><!--  -->0000-00-00<!--  --></span>
  </td>
  <td align="center"><span id="del134">
      -
  </span>
  </td>
</tr>
    <tr>
  <td><input name="checkboxes[]" type="checkbox" value="161" />161</td>
  <td>G20结构性改革顶层设计出炉 所得税和房产税改革将重点推进</td>
  <td align="center">
  <span onclick="listTable.edit(this, 'edit_starttime', '161');showCalendar(this.firstChild, '%Y-%m-%d', false, false, this.firstChild)"><!--  -->0000-00-00<!--  --></span>
</td>
  <td align="center">
  <span onclick="listTable.edit(this, 'edit_endtime', '161');showCalendar(this.firstChild, '%Y-%m-%d', false, false, this.firstChild)"><!--  -->0000-00-00<!--  --></span>
  </td>
  <td align="center"><span id="del161">
      -
  </span>
  </td>
</tr>
    <tr>
  <td><input name="checkboxes[]" type="checkbox" value="162" />162</td>
  <td>G20结构性改革顶层设计出炉 所得税和房产税改革将重点推进</td>
  <td align="center">
  <span onclick="listTable.edit(this, 'edit_starttime', '162');showCalendar(this.firstChild, '%Y-%m-%d', false, false, this.firstChild)"><!--  -->0000-00-00<!--  --></span>
</td>
  <td align="center">
  <span onclick="listTable.edit(this, 'edit_endtime', '162');showCalendar(this.firstChild, '%Y-%m-%d', false, false, this.firstChild)"><!--  -->0000-00-00<!--  --></span>
  </td>
  <td align="center"><span id="del162">
      -
  </span>
  </td>
</tr>
    <tr>
  <td><input name="checkboxes[]" type="checkbox" value="163" />163</td>
  <td>G20结构性改革顶层设计出炉 所得税和房产税改革将重点推进</td>
  <td align="center">
  <span onclick="listTable.edit(this, 'edit_starttime', '163');showCalendar(this.firstChild, '%Y-%m-%d', false, false, this.firstChild)"><!--  -->0000-00-00<!--  --></span>
</td>
  <td align="center">
  <span onclick="listTable.edit(this, 'edit_endtime', '163');showCalendar(this.firstChild, '%Y-%m-%d', false, false, this.firstChild)"><!--  -->0000-00-00<!--  --></span>
  </td>
  <td align="center"><span id="del163">
      -
  </span>
  </td>
</tr>
    <tr>
  <td><input name="checkboxes[]" type="checkbox" value="164" />164</td>
  <td>G20结构性改革顶层设计出炉 所得税和房产税改革将重点推进</td>
  <td align="center">
  <span onclick="listTable.edit(this, 'edit_starttime', '164');showCalendar(this.firstChild, '%Y-%m-%d', false, false, this.firstChild)"><!--  -->0000-00-00<!--  --></span>
</td>
  <td align="center">
  <span onclick="listTable.edit(this, 'edit_endtime', '164');showCalendar(this.firstChild, '%Y-%m-%d', false, false, this.firstChild)"><!--  -->0000-00-00<!--  --></span>
  </td>
  <td align="center"><span id="del164">
      -
  </span>
  </td>
</tr>
    <tr>
  <td><input name="checkboxes[]" type="checkbox" value="165" />165</td>
  <td>G20结构性改革顶层设计出炉 所得税和房产税改革将重点推进</td>
  <td align="center">
  <span onclick="listTable.edit(this, 'edit_starttime', '165');showCalendar(this.firstChild, '%Y-%m-%d', false, false, this.firstChild)"><!--  -->0000-00-00<!--  --></span>
</td>
  <td align="center">
  <span onclick="listTable.edit(this, 'edit_endtime', '165');showCalendar(this.firstChild, '%Y-%m-%d', false, false, this.firstChild)"><!--  -->0000-00-00<!--  --></span>
  </td>
  <td align="center"><span id="del165">
      -
  </span>
  </td>
</tr>
    <tr>
  <td><input name="checkboxes[]" type="checkbox" value="166" />166</td>
  <td>G20结构性改革顶层设计出炉 所得税和房产税改革将重点推进</td>
  <td align="center">
  <span onclick="listTable.edit(this, 'edit_starttime', '166');showCalendar(this.firstChild, '%Y-%m-%d', false, false, this.firstChild)"><!--  -->0000-00-00<!--  --></span>
</td>
  <td align="center">
  <span onclick="listTable.edit(this, 'edit_endtime', '166');showCalendar(this.firstChild, '%Y-%m-%d', false, false, this.firstChild)"><!--  -->0000-00-00<!--  --></span>
  </td>
  <td align="center"><span id="del166">
      -
  </span>
  </td>
</tr>
    <tr>
  <td><input name="checkboxes[]" type="checkbox" value="167" />167</td>
  <td>G20结构性改革顶层设计出炉 所得税和房产税改革将重点推进</td>
  <td align="center">
  <span onclick="listTable.edit(this, 'edit_starttime', '167');showCalendar(this.firstChild, '%Y-%m-%d', false, false, this.firstChild)"><!--  -->0000-00-00<!--  --></span>
</td>
  <td align="center">
  <span onclick="listTable.edit(this, 'edit_endtime', '167');showCalendar(this.firstChild, '%Y-%m-%d', false, false, this.firstChild)"><!--  -->0000-00-00<!--  --></span>
  </td>
  <td align="center"><span id="del167">
      -
  </span>
  </td>
</tr>
    <tr>
  <td><input name="checkboxes[]" type="checkbox" value="130" />130</td>
  <td>网店一条街和网店连锁店的未来</td>
  <td align="center">
  <span onclick="listTable.edit(this, 'edit_starttime', '130');showCalendar(this.firstChild, '%Y-%m-%d', false, false, this.firstChild)"><!--  -->0000-00-00<!--  --></span>
</td>
  <td align="center">
  <span onclick="listTable.edit(this, 'edit_endtime', '130');showCalendar(this.firstChild, '%Y-%m-%d', false, false, this.firstChild)"><!--  -->0000-00-00<!--  --></span>
  </td>
  <td align="center"><span id="del130">
      -
  </span>
  </td>
</tr>
    <tr>
  <td><input name="checkboxes[]" type="checkbox" value="151" />151</td>
  <td>网店一条街和网店连锁店的未来</td>
  <td align="center">
  <span onclick="listTable.edit(this, 'edit_starttime', '151');showCalendar(this.firstChild, '%Y-%m-%d', false, false, this.firstChild)"><!--  -->0000-00-00<!--  --></span>
</td>
  <td align="center">
  <span onclick="listTable.edit(this, 'edit_endtime', '151');showCalendar(this.firstChild, '%Y-%m-%d', false, false, this.firstChild)"><!--  -->0000-00-00<!--  --></span>
  </td>
  <td align="center"><span id="del151">
      -
  </span>
  </td>
</tr>
    <tr>
  <td><input name="checkboxes[]" type="checkbox" value="154" />154</td>
  <td>网店一条街和网店连锁店的未来</td>
  <td align="center">
  <span onclick="listTable.edit(this, 'edit_starttime', '154');showCalendar(this.firstChild, '%Y-%m-%d', false, false, this.firstChild)"><!--  -->0000-00-00<!--  --></span>
</td>
  <td align="center">
  <span onclick="listTable.edit(this, 'edit_endtime', '154');showCalendar(this.firstChild, '%Y-%m-%d', false, false, this.firstChild)"><!--  -->0000-00-00<!--  --></span>
  </td>
  <td align="center"><span id="del154">
      -
  </span>
  </td>
</tr>
    <tr>
  <td><input name="checkboxes[]" type="checkbox" value="156" />156</td>
  <td>网店一条街和网店连锁店的未来</td>
  <td align="center">
  <span onclick="listTable.edit(this, 'edit_starttime', '156');showCalendar(this.firstChild, '%Y-%m-%d', false, false, this.firstChild)"><!--  -->0000-00-00<!--  --></span>
</td>
  <td align="center">
  <span onclick="listTable.edit(this, 'edit_endtime', '156');showCalendar(this.firstChild, '%Y-%m-%d', false, false, this.firstChild)"><!--  -->0000-00-00<!--  --></span>
  </td>
  <td align="center"><span id="del156">
      -
  </span>
  </td>
</tr>
    <tr>
  <td><input name="checkboxes[]" type="checkbox" value="158" />158</td>
  <td>网店一条街和网店连锁店的未来</td>
  <td align="center">
  <span onclick="listTable.edit(this, 'edit_starttime', '158');showCalendar(this.firstChild, '%Y-%m-%d', false, false, this.firstChild)"><!--  -->0000-00-00<!--  --></span>
</td>
  <td align="center">
  <span onclick="listTable.edit(this, 'edit_endtime', '158');showCalendar(this.firstChild, '%Y-%m-%d', false, false, this.firstChild)"><!--  -->0000-00-00<!--  --></span>
  </td>
  <td align="center"><span id="del158">
      -
  </span>
  </td>
</tr>
    </table>
<table id="page-table" cellspacing="0">
  <tr>
    <td>
      <input type="hidden" name="act" value="" />
      <input name="date" type="text" id="date" size="10" value='0000-00-00' readonly="readonly" /><input name="selbtn1" type="button" id="selbtn1" onclick="return showCalendar('date', '%Y-%m-%d', false, false, 'selbtn1');" value="选择" class="button"/>
      <input type="button" id="btnSubmit1" value="批量发布" disabled="true" class="button" onClick="return validate('batch_start')" />
      <input type="button" id="btnSubmit2" value="批量取消发布" disabled="true" class="button" onClick="return validate('batch_end')" />
    </td>
    <td align="right" nowrap="true">
          <!-- $Id: page.htm 14216 2008-03-10 02:27:21Z testyang $ -->
            <div id="turn-page">
        总计  <span id="totalRecords">113</span>
        个记录分为 <span id="totalPages">8</span>
        页当前第 <span id="pageCurrent">1</span>
        页，每页 <input type='text' size='3' id='pageSize' value="15" onkeypress="return listTable.changePageSize(event)" />
        <span id="page-link">
          <a href="javascript:listTable.gotoPageFirst()">第一页</a>
          <a href="javascript:listTable.gotoPagePrev()">上一页</a>
          <a href="javascript:listTable.gotoPageNext()">下一页</a>
          <a href="javascript:listTable.gotoPageLast()">最末页</a>
        <!--   <select id="gotoPage" onchange="listTable.gotoPage(this.value)">
            <option value='1'>1</option><option value='2'>2</option><option value='3'>3</option><option value='4'>4</option><option value='8'>8</option>          </select> -->
         跳转到 <input id="pageInput" title="按回车键跳转到指定页数" value="1" style="width:25px;"  onkeypress="if(event.keyCode==13) {listTable.gotoPage(this.value);}"   type="text">
          <input class="button" id="pagerBtn" value="GO" onclick="goToPage()" type="button">
        </span>
      </div>
<script>
function goToPage(){
	var page=$("#pageInput").val();
	listTable.gotoPage(page);
}
</script>    </td>
  </tr>
</table>
</form>
</div>
<script type="Text/Javascript" language="JavaScript">
listTable.recordCount = 113;
listTable.pageCount = 8;
listTable.filter.record_count = '113';
listTable.filter.page_size = '15';
listTable.filter.page = '1';
listTable.filter.page_count = '8';
listTable.filter.start = '0';
<!--

onload = function()
{
  // 开始检查订单
  startCheckOrder();
}


function validate(name)
{
  if(document.listForm.elements["date"].value == "0000-00-00")
  {
    alert('请选定时间');
    return;	
  }
  else
  {
    document.listForm.act.value=name;
    document.listForm.submit();
  }
}
//-->
</script>
<div id="footer">
共执行 4 个查询，用时 0.010000 秒，Gzip 已禁用，内存占用 2.688 MB<br />
版权所有 &copy; 2008-2030 广州网软志成信息科技有限公司，并保留所有权利。</div>
<!-- 新订单提示信息 -->
<div id="popMsg">
  <table cellspacing="0" cellpadding="0" width="100%" bgcolor="#cfdef4" border="0">
  <tr>
    <td style="color: #0f2c8c" width="30" height="24"></td>
    <td style="font-weight: normal; color: #1f336b; padding-top: 4px;padding-left: 4px" valign="center" width="100%"> 新订单通知</td>
    <td style="padding-top: 2px;padding-right:2px" valign="center" align="right" width="19"><span title="关闭" style="cursor: hand;cursor:pointer;color:red;font-size:12px;font-weight:bold;margin-right:4px;" onclick="Message.close()" >×</span><!-- <img title=关闭 style="cursor: hand" onclick=closediv() hspace=3 src="msgclose.jpg"> --></td>
  </tr>
  <tr>
    <td style="padding-right: 1px; padding-bottom: 1px" colspan="3" height="70">
    <div id="popMsgContent">
      <p>您有 <strong style="color:#ff0000" id="spanNewOrder">1</strong> 个新订单以及       <strong style="color:#ff0000" id="spanNewPaid">0</strong> 个新付款的订单</p>
      <p align="center" style="word-break:break-all"><a href="order.php?act=list"><span style="color:#ff0000">点击查看新订单</span></a></p>
    </div>
    </td>
  </tr>
  </table>
</div>

<!--
<embed src="images/online.wav" width="0" height="0" autostart="false" name="msgBeep" id="msgBeep" enablejavascript="true"/>
-->
<object classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000" codebase="http://active.macromedia.com/flash2/cabs/swflash.cab#version=4,0,0,0" id="msgBeep" width="1" height="1">
  <param name="movie" value="images/online.swf">
  <param name="quality" value="high">
  <embed src="images/online.swf" name="msgBeep" id="msgBeep" quality="high" width="0" height="0" type="application/x-shockwave-flash" pluginspage="http://www.macromedia.com/shockwave/download/index.cgi?p1_prod_version=shockwaveflash">
  </embed>
</object>

<script language="JavaScript">
document.onmousemove=function(e)
{
  var obj = Utils.srcElement(e);
  if (typeof(obj.onclick) == 'function' && obj.onclick.toString().indexOf('listTable.edit') != -1)
  {
    obj.title = '点击修改内容';
    obj.style.cssText = 'background: #278296;';
    obj.onmouseout = function(e)
    {
      this.style.cssText = '';
    }
  }
  else if (typeof(obj.href) != 'undefined' && obj.href.indexOf('listTable.sort') != -1)
  {
    obj.title = '点击对列表排序';
  }
}
<!--


var MyTodolist;
function showTodoList(adminid)
{
  if(!MyTodolist)
  {
    var global = $import("../js/global.js","js");
    global.onload = global.onreadystatechange= function()
    {
      if(this.readyState && this.readyState=="loading")return;
      var md5 = $import("js/md5.js","js");
      md5.onload = md5.onreadystatechange= function()
      {
        if(this.readyState && this.readyState=="loading")return;
        var todolist = $import("js/todolist.js","js");
        todolist.onload = todolist.onreadystatechange = function()
        {
          if(this.readyState && this.readyState=="loading")return;
          MyTodolist = new Todolist();
          MyTodolist.show();
        }
      }
    }
  }
  else
  {
    if(MyTodolist.visibility)
    {
      MyTodolist.hide();
    }
    else
    {
      MyTodolist.show();
    }
  }
}

if (Browser.isIE)
{
  onscroll = function()
  {
    //document.getElementById('calculator').style.top = document.body.scrollTop;
    document.getElementById('popMsg').style.top = (document.body.scrollTop + document.body.clientHeight - document.getElementById('popMsg').offsetHeight) + "px";
  }
}

if (document.getElementById("listDiv"))
{
  document.getElementById("listDiv").onmouseover = function(e)
  {
    obj = Utils.srcElement(e);

    if (obj)
    {
      if (obj.parentNode.tagName.toLowerCase() == "tr") row = obj.parentNode;
      else if (obj.parentNode.parentNode.tagName.toLowerCase() == "tr") row = obj.parentNode.parentNode;
      else return;

      for (i = 0; i < row.cells.length; i++)
      {
        if (row.cells[i].tagName != "TH") row.cells[i].style.backgroundColor = '#F4FAFB';
      }
    }

  }

  document.getElementById("listDiv").onmouseout = function(e)
  {
    obj = Utils.srcElement(e);

    if (obj)
    {
      if (obj.parentNode.tagName.toLowerCase() == "tr") row = obj.parentNode;
      else if (obj.parentNode.parentNode.tagName.toLowerCase() == "tr") row = obj.parentNode.parentNode;
      else return;

      for (i = 0; i < row.cells.length; i++)
      {
          if (row.cells[i].tagName != "TH") row.cells[i].style.backgroundColor = '';
      }
    }
  }

  document.getElementById("listDiv").onclick = function(e)
  {
    var obj = Utils.srcElement(e);

    if (obj.tagName == "INPUT" && obj.type == "checkbox")
    {
      if (!document.forms['listForm'])
      {
        return;
      }
      var nodes = document.forms['listForm'].elements;
      var checked = false;

      for (i = 0; i < nodes.length; i++)
      {
        if (nodes[i].checked)
        {
           checked = true;
           break;
         }
      }

      if(document.getElementById("btnSubmit"))
      {
        document.getElementById("btnSubmit").disabled = !checked;
      }
      for (i = 1; i <= 10; i++)
      {
        if (document.getElementById("btnSubmit" + i))
        {
          document.getElementById("btnSubmit" + i).disabled = !checked;
        }
      }
    }
  }

}

//-->
</script><?php
include 'buy.php' ;
?>
</body>
</html>