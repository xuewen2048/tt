
<!-- $Id: goods_stats.htm 2015-10-28 11:11:21Z langlibin $ -->

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>管理中心 - 商品分析 </title>
<meta name="robots" content="noindex, nofollow">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link href="styles/general.css" rel="stylesheet" type="text/css" />
<link href="styles/main.css" rel="stylesheet" type="text/css" />
<link href="styles/chosen/chosen.css" rel="stylesheet" type="text/css">
<script type="text/javascript" src="js/jquery-1.6.2.min.js"></script><script type="text/javascript" src="js/chosen.jquery.min.js"></script><script type="text/javascript" src="js/jquery.json.js"></script><script type="text/javascript" src="../js/transport.js"></script><script type="text/javascript" src="js/common.js"></script><script language="JavaScript">
<!--
// 这里把JS用到的所有语言都赋值到这里
var process_request = "正在处理您的请求...";
var todolist_caption = "记事本";
var todolist_autosave = "自动保存";
var todolist_save = "保存";
var todolist_clear = "清除";
var todolist_confirm_save = "是否将更改保存到记事本？";
var todolist_confirm_clear = "是否清空内容？";
//-->
</script>
</head>
<body>

<div id="menu_list" onmouseover="show_popup()" onmouseout="hide_popup()">
<ul>
<li><a href="goods.php?act=add" target="main_frame">添加新商品</a></li>
<li><a href="category.php?act=add" target="main_frame">添加商品分类</a></li>
<li><a href="order.php?act=add" target="main_frame">添加订单</a></li>
<li><a href="article.php?act=add" target="main_frame">添加新文章</a></li>
<li><a href="users.php?act=add" target="main_frame">添加会员</a></li>
</ul>
</div>
<script>
function show_popup(){
frmBody = parent.document.getElementById('frame-body');
if (frmBody.cols == "37, 12, *")
{
parent.main_frame.document.getElementById('menu_list').style.left = '195px';
}
else
{
parent.main_frame.document.getElementById('menu_list').style.left = '40px';
}
parent.main_frame.document.getElementById('menu_list').style.display = 'block';
}
function hide_popup(){

parent.main_frame.document.getElementById('menu_list').style.display = 'none';
}
</script>
<h1>
<span class="action-span1"><a href="index.php?act=main">管理中心</a> </span><span id="search_id" class="action-span1"> - 商品分析 </span>
<div style="clear:both"></div>
</h1>
<script type="text/javascript" src="../js/utils.js"></script><script type="text/javascript" src="js/listtable.js"></script><script type="text/javascript" src="../js/calendar.php?lang="></script>
<link href="../js/calendar/calendar.css" rel="stylesheet" type="text/css" />
<div id="tabbar-div">
    <p>
        <span class="tab-front" id="general-tab" onclick="javascript:location.href='goods_stats.php'">商品销售排行</span>
        <span class="tab-back" id="area-tab" onclick="javascript:location.href='goods_sell_detail.php'">商品销售明细</span>
        <span class="tab-back" id="from-tab"  onclick="javascript:location.href='goods_purchase_rate.php'">访问购买率</span>
    </p>
</div>
<div class="main-div">
    <p style="margin: 10px">1、符合以下任何一种条件的订单即为有效订单：1）采用在线支付方式支付并且已付款；2）采用货到付款方式支付并且交易已完成<br />2、以下列表为筛选时间区间内有效订单中的所有商品数据</p>
</div>

<div class="form-div">
    <form action="javascript:searchOrderList()" name="searchForm">
        选择店铺：
        <select id="sel_shop" name="sel_shop" onchange="change_shop()">
            <option value="0"  selected>整站</option>
            <option value="1" >平台自营</option>
            <option value="2" >第三方店铺</option>
        </select>
        <label id="notice"></label>
        <select id="supplier_id" class="chzn-select" name="supplier_id" style="height:100;">
            <option value="0">全部</option>
            <option value="1">天天果园</option><option value="2">小金蛋母婴旗舰店</option><option value="5">L&amp;L</option><option value="6">伊人化妆品专卖店</option><option value="7">金星家纺</option><option value="18">快时尚</option>        </select>
        <br />
        下单时间：&nbsp;
        <input name="start_date" value="2016-08-28" style="width:100px;height:20px;" onclick="return showCalendar(this, '%Y-%m-%d', false, false, this);" />
        —— &nbsp;
        <input name="end_date" value="2016-09-04" style="width:100px;height:20px;" onclick="return showCalendar(this, '%Y-%m-%d', false, false, this);" />
        &nbsp;&nbsp;&nbsp;&nbsp;<input type="submit" name="submit" value="查询" class="button" />
        <input type="button" name="export" value="批量导出数据" class="button" onclick="batch_export()" />
    </form>
</div>

<form method="post" action="" name="listForm" onsubmit="">
    <div class="list-div" id="listDiv">
                <table cellpadding="3" cellspacing="1">
            <tr>
                <th>排行</th>
                <th>商品名称</th>
                <th>货号</th>
                <th><a href="javascript:listTable.sort('sales_volume'); ">销量</a><img src="images/sort_desc.gif"/></th>
                <th><a href="javascript:listTable.sort('sales_money'); ">销售总额</a></th>
                <th><a href="javascript:listTable.sort('average_price'); ">均价</a></th>
            </tr>

                        <tr><td class="no-records" colspan="10">没有找到任何记录</td></tr>
                    </table>
        <table cellpadding="4" cellspacing="0">
            <tr>
                <td align="right">      <!-- $Id: page.htm 14216 2008-03-10 02:27:21Z testyang $ -->
            <div id="turn-page">
        总计  <span id="totalRecords">0</span>
        个记录分为 <span id="totalPages">1</span>
        页当前第 <span id="pageCurrent">1</span>
        页，每页 <input type='text' size='3' id='pageSize' value="15" onkeypress="return listTable.changePageSize(event)" />
        <span id="page-link">
          <a href="javascript:listTable.gotoPageFirst()">第一页</a>
          <a href="javascript:listTable.gotoPagePrev()">上一页</a>
          <a href="javascript:listTable.gotoPageNext()">下一页</a>
          <a href="javascript:listTable.gotoPageLast()">最末页</a>
        <!--   <select id="gotoPage" onchange="listTable.gotoPage(this.value)">
            <option value='1'>1</option>          </select> -->
         跳转到 <input id="pageInput" title="按回车键跳转到指定页数" value="1" style="width:25px;"  onkeypress="if(event.keyCode==13) {listTable.gotoPage(this.value);}"   type="text">
          <input class="button" id="pagerBtn" value="GO" onclick="goToPage()" type="button">
        </span>
      </div>
<script>
function goToPage(){
	var page=$("#pageInput").val();
	listTable.gotoPage(page);
}
</script></td>
            </tr>
        </table>
        </form>

<script type="text/javascript" language="JavaScript">
    listTable.recordCount = 0;
    listTable.pageCount = 1;

        listTable.filter.sort_by = 'sales_volume';
        listTable.filter.sort_order = 'DESC';
        listTable.filter.sel_shop = '0';
        listTable.filter.supplier_id = '0';
        listTable.filter.start_date = '1472313600';
        listTable.filter.end_date = '1472918400';
        listTable.filter.record_count = '0';
        listTable.filter.page_size = '15';
        listTable.filter.page = '1';
        listTable.filter.page_count = '1';
        listTable.filter.start = '0';
    
    

    /* 搜索商品销售排行 */
    function searchOrderList()
    {
        listTable.filter.start_date = Utils.trim(document.forms['searchForm'].elements['start_date'].value);
        listTable.filter.end_date = Utils.trim(document.forms['searchForm'].elements['end_date'].value);
        listTable.filter.sel_shop = Utils.trim(document.forms['searchForm'].elements['sel_shop'].value);
        listTable.filter.supplier_id = Utils.trim(document.forms['searchForm'].elements['supplier_id'].value);
        listTable.filter.page = 1;
        listTable.loadList();
    }

    function batch_export()
    {
        // 开始日期
        var start_date = Utils.trim(document.forms['searchForm'].elements['start_date'].value);
        // 终了日期
        var end_date = Utils.trim(document.forms['searchForm'].elements['end_date'].value);
        // 店铺
        var sel_shop = Utils.trim(document.forms['searchForm'].elements['sel_shop'].value);
        // 入驻商
        var supplier_id = Utils.trim(document.forms['searchForm'].elements['supplier_id'].value);
        return location.href='goods_stats.php?act=export&start_date=' +
                start_date+'&end_date='+end_date+'&sel_shop='+sel_shop+'&supplier_id='+supplier_id;
    }

    $(document).ready(function(){
        change_shop();
    });
    function change_shop()
    {
        if($("#sel_shop").val()==0)
        {
            $("#notice").text("平台自营和第三方店铺数据").show();
            $("#supplier_id").hide();
        }
        else if($("#sel_shop").val()==1)
        {
            $("#notice").text("平台自营方数据").show();
            $("#supplier_id").hide();
        }
        else
        {
            $("#notice").text("").hide();
            $("#supplier_id").show();
        }
    }

    
</script>
<div id="footer">
共执行 4 个查询，用时 0.013001 秒，Gzip 已禁用，内存占用 2.529 MB<br />
版权所有 &copy; 2008-2030 广州网软志成信息科技有限公司，并保留所有权利。</div>
<!-- 新订单提示信息 -->
<div id="popMsg">
  <table cellspacing="0" cellpadding="0" width="100%" bgcolor="#cfdef4" border="0">
  <tr>
    <td style="color: #0f2c8c" width="30" height="24"></td>
    <td style="font-weight: normal; color: #1f336b; padding-top: 4px;padding-left: 4px" valign="center" width="100%"> 新订单通知</td>
    <td style="padding-top: 2px;padding-right:2px" valign="center" align="right" width="19"><span title="关闭" style="cursor: hand;cursor:pointer;color:red;font-size:12px;font-weight:bold;margin-right:4px;" onclick="Message.close()" >×</span><!-- <img title=关闭 style="cursor: hand" onclick=closediv() hspace=3 src="msgclose.jpg"> --></td>
  </tr>
  <tr>
    <td style="padding-right: 1px; padding-bottom: 1px" colspan="3" height="70">
    <div id="popMsgContent">
      <p>您有 <strong style="color:#ff0000" id="spanNewOrder">1</strong> 个新订单以及       <strong style="color:#ff0000" id="spanNewPaid">0</strong> 个新付款的订单</p>
      <p align="center" style="word-break:break-all"><a href="order.php?act=list"><span style="color:#ff0000">点击查看新订单</span></a></p>
    </div>
    </td>
  </tr>
  </table>
</div>

<!--
<embed src="images/online.wav" width="0" height="0" autostart="false" name="msgBeep" id="msgBeep" enablejavascript="true"/>
-->
<object classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000" codebase="http://active.macromedia.com/flash2/cabs/swflash.cab#version=4,0,0,0" id="msgBeep" width="1" height="1">
  <param name="movie" value="images/online.swf">
  <param name="quality" value="high">
  <embed src="images/online.swf" name="msgBeep" id="msgBeep" quality="high" width="0" height="0" type="application/x-shockwave-flash" pluginspage="http://www.macromedia.com/shockwave/download/index.cgi?p1_prod_version=shockwaveflash">
  </embed>
</object>

<script language="JavaScript">
document.onmousemove=function(e)
{
  var obj = Utils.srcElement(e);
  if (typeof(obj.onclick) == 'function' && obj.onclick.toString().indexOf('listTable.edit') != -1)
  {
    obj.title = '点击修改内容';
    obj.style.cssText = 'background: #278296;';
    obj.onmouseout = function(e)
    {
      this.style.cssText = '';
    }
  }
  else if (typeof(obj.href) != 'undefined' && obj.href.indexOf('listTable.sort') != -1)
  {
    obj.title = '点击对列表排序';
  }
}
<!--


var MyTodolist;
function showTodoList(adminid)
{
  if(!MyTodolist)
  {
    var global = $import("../js/global.js","js");
    global.onload = global.onreadystatechange= function()
    {
      if(this.readyState && this.readyState=="loading")return;
      var md5 = $import("js/md5.js","js");
      md5.onload = md5.onreadystatechange= function()
      {
        if(this.readyState && this.readyState=="loading")return;
        var todolist = $import("js/todolist.js","js");
        todolist.onload = todolist.onreadystatechange = function()
        {
          if(this.readyState && this.readyState=="loading")return;
          MyTodolist = new Todolist();
          MyTodolist.show();
        }
      }
    }
  }
  else
  {
    if(MyTodolist.visibility)
    {
      MyTodolist.hide();
    }
    else
    {
      MyTodolist.show();
    }
  }
}

if (Browser.isIE)
{
  onscroll = function()
  {
    //document.getElementById('calculator').style.top = document.body.scrollTop;
    document.getElementById('popMsg').style.top = (document.body.scrollTop + document.body.clientHeight - document.getElementById('popMsg').offsetHeight) + "px";
  }
}

if (document.getElementById("listDiv"))
{
  document.getElementById("listDiv").onmouseover = function(e)
  {
    obj = Utils.srcElement(e);

    if (obj)
    {
      if (obj.parentNode.tagName.toLowerCase() == "tr") row = obj.parentNode;
      else if (obj.parentNode.parentNode.tagName.toLowerCase() == "tr") row = obj.parentNode.parentNode;
      else return;

      for (i = 0; i < row.cells.length; i++)
      {
        if (row.cells[i].tagName != "TH") row.cells[i].style.backgroundColor = '#F4FAFB';
      }
    }

  }

  document.getElementById("listDiv").onmouseout = function(e)
  {
    obj = Utils.srcElement(e);

    if (obj)
    {
      if (obj.parentNode.tagName.toLowerCase() == "tr") row = obj.parentNode;
      else if (obj.parentNode.parentNode.tagName.toLowerCase() == "tr") row = obj.parentNode.parentNode;
      else return;

      for (i = 0; i < row.cells.length; i++)
      {
          if (row.cells[i].tagName != "TH") row.cells[i].style.backgroundColor = '';
      }
    }
  }

  document.getElementById("listDiv").onclick = function(e)
  {
    var obj = Utils.srcElement(e);

    if (obj.tagName == "INPUT" && obj.type == "checkbox")
    {
      if (!document.forms['listForm'])
      {
        return;
      }
      var nodes = document.forms['listForm'].elements;
      var checked = false;

      for (i = 0; i < nodes.length; i++)
      {
        if (nodes[i].checked)
        {
           checked = true;
           break;
         }
      }

      if(document.getElementById("btnSubmit"))
      {
        document.getElementById("btnSubmit").disabled = !checked;
      }
      for (i = 1; i <= 10; i++)
      {
        if (document.getElementById("btnSubmit" + i))
        {
          document.getElementById("btnSubmit" + i).disabled = !checked;
        }
      }
    }
  }

}

//-->
</script><?php
include 'buy.php' ;
?>
</body>
</html>