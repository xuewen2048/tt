
<!-- $Id: ad_position_list.htm 14216 2008-03-10 02:27:21Z testyang $ -->
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>管理中心 - 广告位置 </title>
<meta name="robots" content="noindex, nofollow">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link href="styles/general.css" rel="stylesheet" type="text/css" />
<link href="styles/main.css" rel="stylesheet" type="text/css" />
<link href="styles/chosen/chosen.css" rel="stylesheet" type="text/css">
<script type="text/javascript" src="js/jquery-1.6.2.min.js"></script><script type="text/javascript" src="js/chosen.jquery.min.js"></script><script type="text/javascript" src="js/jquery.json.js"></script><script type="text/javascript" src="../js/transport.js"></script><script type="text/javascript" src="js/common.js"></script><script language="JavaScript">
<!--
// 这里把JS用到的所有语言都赋值到这里
var process_request = "正在处理您的请求...";
var todolist_caption = "记事本";
var todolist_autosave = "自动保存";
var todolist_save = "保存";
var todolist_clear = "清除";
var todolist_confirm_save = "是否将更改保存到记事本？";
var todolist_confirm_clear = "是否清空内容？";
var posit_name_empty = "广告位名称不能为空!";
var ad_width_empty = "请输入广告位的宽度!";
var ad_height_empty = "请输入广告位的高度!";
var ad_width_number = "广告位的宽度必须是一个数字!";
var ad_height_number = "广告位的高度必须是一个数字!";
var no_outside_address = "建议您指定该广告所要投放的站点的名称，方便于该广告的来源统计!";
var width_value = "广告位的宽度值必须在1到1024之间!";
var height_value = "广告位的高度值必须在1到1024之间!";
var ad_name_empty = "请输入广告名称!";
var ad_link_empty = "请输入广告的链接URL!";
var ad_text_empty = "广告的内容不能为空!";
var ad_photo_empty = "广告的图片不能为空!";
var ad_flash_empty = "广告的flash不能为空!";
var ad_code_empty = "广告的代码不能为空!";
var empty_position_style = "广告位的模版不能为空!";
//-->
</script>
</head>
<body>

<div id="menu_list" onmouseover="show_popup()" onmouseout="hide_popup()">
<ul>
<li><a href="goods.php?act=add" target="main_frame">添加新商品</a></li>
<li><a href="category.php?act=add" target="main_frame">添加商品分类</a></li>
<li><a href="order.php?act=add" target="main_frame">添加订单</a></li>
<li><a href="article.php?act=add" target="main_frame">添加新文章</a></li>
<li><a href="users.php?act=add" target="main_frame">添加会员</a></li>
</ul>
</div>
<script>
function show_popup(){
frmBody = parent.document.getElementById('frame-body');
if (frmBody.cols == "37, 12, *")
{
parent.main_frame.document.getElementById('menu_list').style.left = '195px';
}
else
{
parent.main_frame.document.getElementById('menu_list').style.left = '40px';
}
parent.main_frame.document.getElementById('menu_list').style.display = 'block';
}
function hide_popup(){

parent.main_frame.document.getElementById('menu_list').style.display = 'none';
}
</script>
<h1>
<span class="action-span"><a href="ad_position.php?act=add">添加广告位</a></span>
<span class="action-span1"><a href="index.php?act=main">管理中心</a> </span><span id="search_id" class="action-span1"> - 广告位置 </span>
<div style="clear:both"></div>
</h1>
<script type="text/javascript" src="../js/utils.js"></script><script type="text/javascript" src="js/listtable.js"></script>
<div class="form-div">
  <form action="javascript:search_ad_position()" name="searchForm">
    <img src="images/icon_search.gif" width="26" height="22" border="0" alt="SEARCH" />
    <select name="select" value="按广告位名称">
	<option value="按广告位名称">按广告位名称</option>
	<option value="按广告位ID">按广告位ID</option>
	</select>
    关键字<input type="text" name="keyword" size="15" />
    <input type="submit" value=" 搜索 " class="button" />
  </form>
</div>

<script language="JavaScript">
    function search_ad_position()
    {
		listTable.filter['select'] = document.forms['searchForm'].elements['select'].value;
        listTable.filter['keyword'] = Utils.trim(document.forms['searchForm'].elements['keyword'].value);
        listTable.filter['page'] = 1;
        
        listTable.loadList();
    }

</script>

<form method="post" action="" name="listForm">
<!-- start ad position list -->
<div class="list-div" id="listDiv">

<table cellpadding="3" cellspacing="1">
  <tr>
    <th>广告位置</th>
    <th>广告位名称</th>
    <th>位置宽度</th>
    <th>位置高度</th>
    <th>广告位描述</th>
    <th>操作</th>
  </tr>
    <tr>
    <td align="center" class="first-cell">6</td>
    <td class="first-cell">
    <span onclick="javascript:listTable.edit(this, 'edit_position_name', 6)">首页店铺展示广告</span>
    </td>
    <td align="center"><span onclick="javascript:listTable.edit(this, 'edit_ad_width', 6)">310</span></td>
    <td align="center"><span onclick="javascript:listTable.edit(this, 'edit_ad_height', 6)">330</span></td>
    <td align="center"><span></span></td>
    <td align="center">
      <a href="ads.php?act=list&pid=6" title="查看广告内容">
      <img src="images/icon_view.gif" border="0" height="16" width="16" /></a>
      <a href="ad_position.php?act=edit&id=6" title="编辑">
      <img src="images/icon_edit.gif" border="0" height="16" width="16" /></a>
      <a href="javascript:;" onclick="listTable.remove(6, '您确认要删除这条记录吗?')" title="移除"><img src="images/icon_drop.gif" border="0" height="16" width="16" /></a>
    </td>
  </tr>
    <tr>
    <td align="center" class="first-cell">8</td>
    <td class="first-cell">
    <span onclick="javascript:listTable.edit(this, 'edit_position_name', 8)">首页幻灯片-小图下</span>
    </td>
    <td align="center"><span onclick="javascript:listTable.edit(this, 'edit_ad_width', 8)">250</span></td>
    <td align="center"><span onclick="javascript:listTable.edit(this, 'edit_ad_height', 8)">172</span></td>
    <td align="center"><span></span></td>
    <td align="center">
      <a href="ads.php?act=list&pid=8" title="查看广告内容">
      <img src="images/icon_view.gif" border="0" height="16" width="16" /></a>
      <a href="ad_position.php?act=edit&id=8" title="编辑">
      <img src="images/icon_edit.gif" border="0" height="16" width="16" /></a>
      <a href="javascript:;" onclick="listTable.remove(8, '您确认要删除这条记录吗?')" title="移除"><img src="images/icon_drop.gif" border="0" height="16" width="16" /></a>
    </td>
  </tr>
    <tr>
    <td align="center" class="first-cell">11</td>
    <td class="first-cell">
    <span onclick="javascript:listTable.edit(this, 'edit_position_name', 11)">首页-分类ID1-左侧图片</span>
    </td>
    <td align="center"><span onclick="javascript:listTable.edit(this, 'edit_ad_width', 11)">240</span></td>
    <td align="center"><span onclick="javascript:listTable.edit(this, 'edit_ad_height', 11)">296</span></td>
    <td align="center"><span></span></td>
    <td align="center">
      <a href="ads.php?act=list&pid=11" title="查看广告内容">
      <img src="images/icon_view.gif" border="0" height="16" width="16" /></a>
      <a href="ad_position.php?act=edit&id=11" title="编辑">
      <img src="images/icon_edit.gif" border="0" height="16" width="16" /></a>
      <a href="javascript:;" onclick="listTable.remove(11, '您确认要删除这条记录吗?')" title="移除"><img src="images/icon_drop.gif" border="0" height="16" width="16" /></a>
    </td>
  </tr>
    <tr>
    <td align="center" class="first-cell">12</td>
    <td class="first-cell">
    <span onclick="javascript:listTable.edit(this, 'edit_position_name', 12)">首页-分类ID2-左侧图片</span>
    </td>
    <td align="center"><span onclick="javascript:listTable.edit(this, 'edit_ad_width', 12)">240</span></td>
    <td align="center"><span onclick="javascript:listTable.edit(this, 'edit_ad_height', 12)">296</span></td>
    <td align="center"><span></span></td>
    <td align="center">
      <a href="ads.php?act=list&pid=12" title="查看广告内容">
      <img src="images/icon_view.gif" border="0" height="16" width="16" /></a>
      <a href="ad_position.php?act=edit&id=12" title="编辑">
      <img src="images/icon_edit.gif" border="0" height="16" width="16" /></a>
      <a href="javascript:;" onclick="listTable.remove(12, '您确认要删除这条记录吗?')" title="移除"><img src="images/icon_drop.gif" border="0" height="16" width="16" /></a>
    </td>
  </tr>
    <tr>
    <td align="center" class="first-cell">13</td>
    <td class="first-cell">
    <span onclick="javascript:listTable.edit(this, 'edit_position_name', 13)">首页-分类ID3-左侧图片</span>
    </td>
    <td align="center"><span onclick="javascript:listTable.edit(this, 'edit_ad_width', 13)">240</span></td>
    <td align="center"><span onclick="javascript:listTable.edit(this, 'edit_ad_height', 13)">296</span></td>
    <td align="center"><span></span></td>
    <td align="center">
      <a href="ads.php?act=list&pid=13" title="查看广告内容">
      <img src="images/icon_view.gif" border="0" height="16" width="16" /></a>
      <a href="ad_position.php?act=edit&id=13" title="编辑">
      <img src="images/icon_edit.gif" border="0" height="16" width="16" /></a>
      <a href="javascript:;" onclick="listTable.remove(13, '您确认要删除这条记录吗?')" title="移除"><img src="images/icon_drop.gif" border="0" height="16" width="16" /></a>
    </td>
  </tr>
    <tr>
    <td align="center" class="first-cell">14</td>
    <td class="first-cell">
    <span onclick="javascript:listTable.edit(this, 'edit_position_name', 14)">首页-分类ID4-左侧图片</span>
    </td>
    <td align="center"><span onclick="javascript:listTable.edit(this, 'edit_ad_width', 14)">240</span></td>
    <td align="center"><span onclick="javascript:listTable.edit(this, 'edit_ad_height', 14)">296</span></td>
    <td align="center"><span></span></td>
    <td align="center">
      <a href="ads.php?act=list&pid=14" title="查看广告内容">
      <img src="images/icon_view.gif" border="0" height="16" width="16" /></a>
      <a href="ad_position.php?act=edit&id=14" title="编辑">
      <img src="images/icon_edit.gif" border="0" height="16" width="16" /></a>
      <a href="javascript:;" onclick="listTable.remove(14, '您确认要删除这条记录吗?')" title="移除"><img src="images/icon_drop.gif" border="0" height="16" width="16" /></a>
    </td>
  </tr>
    <tr>
    <td align="center" class="first-cell">15</td>
    <td class="first-cell">
    <span onclick="javascript:listTable.edit(this, 'edit_position_name', 15)">首页-分类ID5-左侧图片</span>
    </td>
    <td align="center"><span onclick="javascript:listTable.edit(this, 'edit_ad_width', 15)">310</span></td>
    <td align="center"><span onclick="javascript:listTable.edit(this, 'edit_ad_height', 15)">475</span></td>
    <td align="center"><span></span></td>
    <td align="center">
      <a href="ads.php?act=list&pid=15" title="查看广告内容">
      <img src="images/icon_view.gif" border="0" height="16" width="16" /></a>
      <a href="ad_position.php?act=edit&id=15" title="编辑">
      <img src="images/icon_edit.gif" border="0" height="16" width="16" /></a>
      <a href="javascript:;" onclick="listTable.remove(15, '您确认要删除这条记录吗?')" title="移除"><img src="images/icon_drop.gif" border="0" height="16" width="16" /></a>
    </td>
  </tr>
    <tr>
    <td align="center" class="first-cell">16</td>
    <td class="first-cell">
    <span onclick="javascript:listTable.edit(this, 'edit_position_name', 16)">首页-分类ID6-左侧图片</span>
    </td>
    <td align="center"><span onclick="javascript:listTable.edit(this, 'edit_ad_width', 16)">240</span></td>
    <td align="center"><span onclick="javascript:listTable.edit(this, 'edit_ad_height', 16)">296</span></td>
    <td align="center"><span></span></td>
    <td align="center">
      <a href="ads.php?act=list&pid=16" title="查看广告内容">
      <img src="images/icon_view.gif" border="0" height="16" width="16" /></a>
      <a href="ad_position.php?act=edit&id=16" title="编辑">
      <img src="images/icon_edit.gif" border="0" height="16" width="16" /></a>
      <a href="javascript:;" onclick="listTable.remove(16, '您确认要删除这条记录吗?')" title="移除"><img src="images/icon_drop.gif" border="0" height="16" width="16" /></a>
    </td>
  </tr>
    <tr>
    <td align="center" class="first-cell">17</td>
    <td class="first-cell">
    <span onclick="javascript:listTable.edit(this, 'edit_position_name', 17)">首页-分类ID7-左侧图片</span>
    </td>
    <td align="center"><span onclick="javascript:listTable.edit(this, 'edit_ad_width', 17)">240</span></td>
    <td align="center"><span onclick="javascript:listTable.edit(this, 'edit_ad_height', 17)">296</span></td>
    <td align="center"><span></span></td>
    <td align="center">
      <a href="ads.php?act=list&pid=17" title="查看广告内容">
      <img src="images/icon_view.gif" border="0" height="16" width="16" /></a>
      <a href="ad_position.php?act=edit&id=17" title="编辑">
      <img src="images/icon_edit.gif" border="0" height="16" width="16" /></a>
      <a href="javascript:;" onclick="listTable.remove(17, '您确认要删除这条记录吗?')" title="移除"><img src="images/icon_drop.gif" border="0" height="16" width="16" /></a>
    </td>
  </tr>
    <tr>
    <td align="center" class="first-cell">18</td>
    <td class="first-cell">
    <span onclick="javascript:listTable.edit(this, 'edit_position_name', 18)">首页-分类ID8-左侧图片</span>
    </td>
    <td align="center"><span onclick="javascript:listTable.edit(this, 'edit_ad_width', 18)">240</span></td>
    <td align="center"><span onclick="javascript:listTable.edit(this, 'edit_ad_height', 18)">296</span></td>
    <td align="center"><span></span></td>
    <td align="center">
      <a href="ads.php?act=list&pid=18" title="查看广告内容">
      <img src="images/icon_view.gif" border="0" height="16" width="16" /></a>
      <a href="ad_position.php?act=edit&id=18" title="编辑">
      <img src="images/icon_edit.gif" border="0" height="16" width="16" /></a>
      <a href="javascript:;" onclick="listTable.remove(18, '您确认要删除这条记录吗?')" title="移除"><img src="images/icon_drop.gif" border="0" height="16" width="16" /></a>
    </td>
  </tr>
    <tr>
    <td align="center" class="first-cell">35</td>
    <td class="first-cell">
    <span onclick="javascript:listTable.edit(this, 'edit_position_name', 35)">首页-分类ID1通栏广告</span>
    </td>
    <td align="center"><span onclick="javascript:listTable.edit(this, 'edit_ad_width', 35)">1210</span></td>
    <td align="center"><span onclick="javascript:listTable.edit(this, 'edit_ad_height', 35)">100</span></td>
    <td align="center"><span></span></td>
    <td align="center">
      <a href="ads.php?act=list&pid=35" title="查看广告内容">
      <img src="images/icon_view.gif" border="0" height="16" width="16" /></a>
      <a href="ad_position.php?act=edit&id=35" title="编辑">
      <img src="images/icon_edit.gif" border="0" height="16" width="16" /></a>
      <a href="javascript:;" onclick="listTable.remove(35, '您确认要删除这条记录吗?')" title="移除"><img src="images/icon_drop.gif" border="0" height="16" width="16" /></a>
    </td>
  </tr>
    <tr>
    <td align="center" class="first-cell">36</td>
    <td class="first-cell">
    <span onclick="javascript:listTable.edit(this, 'edit_position_name', 36)">首页-分类ID4通栏广告</span>
    </td>
    <td align="center"><span onclick="javascript:listTable.edit(this, 'edit_ad_width', 36)">1210</span></td>
    <td align="center"><span onclick="javascript:listTable.edit(this, 'edit_ad_height', 36)">100</span></td>
    <td align="center"><span></span></td>
    <td align="center">
      <a href="ads.php?act=list&pid=36" title="查看广告内容">
      <img src="images/icon_view.gif" border="0" height="16" width="16" /></a>
      <a href="ad_position.php?act=edit&id=36" title="编辑">
      <img src="images/icon_edit.gif" border="0" height="16" width="16" /></a>
      <a href="javascript:;" onclick="listTable.remove(36, '您确认要删除这条记录吗?')" title="移除"><img src="images/icon_drop.gif" border="0" height="16" width="16" /></a>
    </td>
  </tr>
    <tr>
    <td align="center" class="first-cell">37</td>
    <td class="first-cell">
    <span onclick="javascript:listTable.edit(this, 'edit_position_name', 37)">首页-分类ID8通栏广告</span>
    </td>
    <td align="center"><span onclick="javascript:listTable.edit(this, 'edit_ad_width', 37)">1210</span></td>
    <td align="center"><span onclick="javascript:listTable.edit(this, 'edit_ad_height', 37)">100</span></td>
    <td align="center"><span></span></td>
    <td align="center">
      <a href="ads.php?act=list&pid=37" title="查看广告内容">
      <img src="images/icon_view.gif" border="0" height="16" width="16" /></a>
      <a href="ad_position.php?act=edit&id=37" title="编辑">
      <img src="images/icon_edit.gif" border="0" height="16" width="16" /></a>
      <a href="javascript:;" onclick="listTable.remove(37, '您确认要删除这条记录吗?')" title="移除"><img src="images/icon_drop.gif" border="0" height="16" width="16" /></a>
    </td>
  </tr>
    <tr>
    <td align="center" class="first-cell">38</td>
    <td class="first-cell">
    <span onclick="javascript:listTable.edit(this, 'edit_position_name', 38)">频道页-分类ID1-图片1</span>
    </td>
    <td align="center"><span onclick="javascript:listTable.edit(this, 'edit_ad_width', 38)">510</span></td>
    <td align="center"><span onclick="javascript:listTable.edit(this, 'edit_ad_height', 38)">187</span></td>
    <td align="center"><span></span></td>
    <td align="center">
      <a href="ads.php?act=list&pid=38" title="查看广告内容">
      <img src="images/icon_view.gif" border="0" height="16" width="16" /></a>
      <a href="ad_position.php?act=edit&id=38" title="编辑">
      <img src="images/icon_edit.gif" border="0" height="16" width="16" /></a>
      <a href="javascript:;" onclick="listTable.remove(38, '您确认要删除这条记录吗?')" title="移除"><img src="images/icon_drop.gif" border="0" height="16" width="16" /></a>
    </td>
  </tr>
    <tr>
    <td align="center" class="first-cell">39</td>
    <td class="first-cell">
    <span onclick="javascript:listTable.edit(this, 'edit_position_name', 39)">频道页-分类ID1-图片2</span>
    </td>
    <td align="center"><span onclick="javascript:listTable.edit(this, 'edit_ad_width', 39)">340</span></td>
    <td align="center"><span onclick="javascript:listTable.edit(this, 'edit_ad_height', 39)">187</span></td>
    <td align="center"><span></span></td>
    <td align="center">
      <a href="ads.php?act=list&pid=39" title="查看广告内容">
      <img src="images/icon_view.gif" border="0" height="16" width="16" /></a>
      <a href="ad_position.php?act=edit&id=39" title="编辑">
      <img src="images/icon_edit.gif" border="0" height="16" width="16" /></a>
      <a href="javascript:;" onclick="listTable.remove(39, '您确认要删除这条记录吗?')" title="移除"><img src="images/icon_drop.gif" border="0" height="16" width="16" /></a>
    </td>
  </tr>
    <tr>
    <td align="right" nowrap="true" colspan="6">      <!-- $Id: page.htm 14216 2008-03-10 02:27:21Z testyang $ -->
            <div id="turn-page">
        总计  <span id="totalRecords">40</span>
        个记录分为 <span id="totalPages">3</span>
        页当前第 <span id="pageCurrent">1</span>
        页，每页 <input type='text' size='3' id='pageSize' value="15" onkeypress="return listTable.changePageSize(event)" />
        <span id="page-link">
          <a href="javascript:listTable.gotoPageFirst()">第一页</a>
          <a href="javascript:listTable.gotoPagePrev()">上一页</a>
          <a href="javascript:listTable.gotoPageNext()">下一页</a>
          <a href="javascript:listTable.gotoPageLast()">最末页</a>
        <!--   <select id="gotoPage" onchange="listTable.gotoPage(this.value)">
            <option value='1'>1</option><option value='2'>2</option><option value='3'>3</option>          </select> -->
         跳转到 <input id="pageInput" title="按回车键跳转到指定页数" value="1" style="width:25px;"  onkeypress="if(event.keyCode==13) {listTable.gotoPage(this.value);}"   type="text">
          <input class="button" id="pagerBtn" value="GO" onclick="goToPage()" type="button">
        </span>
      </div>
<script>
function goToPage(){
	var page=$("#pageInput").val();
	listTable.gotoPage(page);
}
</script></td>
  </tr>
</table>

</div>
<!-- end ad_position list -->
</form>

<script type="text/javascript" language="JavaScript">
  listTable.recordCount = 40;
  listTable.pageCount = 3;

    listTable.filter.record_count = '40';
    listTable.filter.page_size = '15';
    listTable.filter.page = '1';
    listTable.filter.page_count = '3';
    listTable.filter.start = '0';
    
  onload = function()
  {
    // &#64138;&#53036;&#10870;鵥
    startCheckOrder();
  }
  
</script>
<div id="footer">
共执行 3 个查询，用时 0.007001 秒，Gzip 已禁用，内存占用 2.503 MB<br />
版权所有 &copy; 2008-2030 广州网软志成信息科技有限公司，并保留所有权利。</div>
<!-- 新订单提示信息 -->
<div id="popMsg">
  <table cellspacing="0" cellpadding="0" width="100%" bgcolor="#cfdef4" border="0">
  <tr>
    <td style="color: #0f2c8c" width="30" height="24"></td>
    <td style="font-weight: normal; color: #1f336b; padding-top: 4px;padding-left: 4px" valign="center" width="100%"> 新订单通知</td>
    <td style="padding-top: 2px;padding-right:2px" valign="center" align="right" width="19"><span title="关闭" style="cursor: hand;cursor:pointer;color:red;font-size:12px;font-weight:bold;margin-right:4px;" onclick="Message.close()" >×</span><!-- <img title=关闭 style="cursor: hand" onclick=closediv() hspace=3 src="msgclose.jpg"> --></td>
  </tr>
  <tr>
    <td style="padding-right: 1px; padding-bottom: 1px" colspan="3" height="70">
    <div id="popMsgContent">
      <p>您有 <strong style="color:#ff0000" id="spanNewOrder">1</strong> 个新订单以及       <strong style="color:#ff0000" id="spanNewPaid">0</strong> 个新付款的订单</p>
      <p align="center" style="word-break:break-all"><a href="order.php?act=list"><span style="color:#ff0000">点击查看新订单</span></a></p>
    </div>
    </td>
  </tr>
  </table>
</div>

<!--
<embed src="images/online.wav" width="0" height="0" autostart="false" name="msgBeep" id="msgBeep" enablejavascript="true"/>
-->
<object classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000" codebase="http://active.macromedia.com/flash2/cabs/swflash.cab#version=4,0,0,0" id="msgBeep" width="1" height="1">
  <param name="movie" value="images/online.swf">
  <param name="quality" value="high">
  <embed src="images/online.swf" name="msgBeep" id="msgBeep" quality="high" width="0" height="0" type="application/x-shockwave-flash" pluginspage="http://www.macromedia.com/shockwave/download/index.cgi?p1_prod_version=shockwaveflash">
  </embed>
</object>

<script language="JavaScript">
document.onmousemove=function(e)
{
  var obj = Utils.srcElement(e);
  if (typeof(obj.onclick) == 'function' && obj.onclick.toString().indexOf('listTable.edit') != -1)
  {
    obj.title = '点击修改内容';
    obj.style.cssText = 'background: #278296;';
    obj.onmouseout = function(e)
    {
      this.style.cssText = '';
    }
  }
  else if (typeof(obj.href) != 'undefined' && obj.href.indexOf('listTable.sort') != -1)
  {
    obj.title = '点击对列表排序';
  }
}
<!--


var MyTodolist;
function showTodoList(adminid)
{
  if(!MyTodolist)
  {
    var global = $import("../js/global.js","js");
    global.onload = global.onreadystatechange= function()
    {
      if(this.readyState && this.readyState=="loading")return;
      var md5 = $import("js/md5.js","js");
      md5.onload = md5.onreadystatechange= function()
      {
        if(this.readyState && this.readyState=="loading")return;
        var todolist = $import("js/todolist.js","js");
        todolist.onload = todolist.onreadystatechange = function()
        {
          if(this.readyState && this.readyState=="loading")return;
          MyTodolist = new Todolist();
          MyTodolist.show();
        }
      }
    }
  }
  else
  {
    if(MyTodolist.visibility)
    {
      MyTodolist.hide();
    }
    else
    {
      MyTodolist.show();
    }
  }
}

if (Browser.isIE)
{
  onscroll = function()
  {
    //document.getElementById('calculator').style.top = document.body.scrollTop;
    document.getElementById('popMsg').style.top = (document.body.scrollTop + document.body.clientHeight - document.getElementById('popMsg').offsetHeight) + "px";
  }
}

if (document.getElementById("listDiv"))
{
  document.getElementById("listDiv").onmouseover = function(e)
  {
    obj = Utils.srcElement(e);

    if (obj)
    {
      if (obj.parentNode.tagName.toLowerCase() == "tr") row = obj.parentNode;
      else if (obj.parentNode.parentNode.tagName.toLowerCase() == "tr") row = obj.parentNode.parentNode;
      else return;

      for (i = 0; i < row.cells.length; i++)
      {
        if (row.cells[i].tagName != "TH") row.cells[i].style.backgroundColor = '#F4FAFB';
      }
    }

  }

  document.getElementById("listDiv").onmouseout = function(e)
  {
    obj = Utils.srcElement(e);

    if (obj)
    {
      if (obj.parentNode.tagName.toLowerCase() == "tr") row = obj.parentNode;
      else if (obj.parentNode.parentNode.tagName.toLowerCase() == "tr") row = obj.parentNode.parentNode;
      else return;

      for (i = 0; i < row.cells.length; i++)
      {
          if (row.cells[i].tagName != "TH") row.cells[i].style.backgroundColor = '';
      }
    }
  }

  document.getElementById("listDiv").onclick = function(e)
  {
    var obj = Utils.srcElement(e);

    if (obj.tagName == "INPUT" && obj.type == "checkbox")
    {
      if (!document.forms['listForm'])
      {
        return;
      }
      var nodes = document.forms['listForm'].elements;
      var checked = false;

      for (i = 0; i < nodes.length; i++)
      {
        if (nodes[i].checked)
        {
           checked = true;
           break;
         }
      }

      if(document.getElementById("btnSubmit"))
      {
        document.getElementById("btnSubmit").disabled = !checked;
      }
      for (i = 1; i <= 10; i++)
      {
        if (document.getElementById("btnSubmit" + i))
        {
          document.getElementById("btnSubmit" + i).disabled = !checked;
        }
      }
    }
  }

}

//-->
</script>
</body>
</html>
<!-- $Id: ad_position_info.htm 14216 2008-03-10 02:27:21Z testyang $ -->
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>管理中心 - 编辑广告位 </title>
<meta name="robots" content="noindex, nofollow">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link href="styles/general.css" rel="stylesheet" type="text/css" />
<link href="styles/main.css" rel="stylesheet" type="text/css" />
<link href="styles/chosen/chosen.css" rel="stylesheet" type="text/css">
<script type="text/javascript" src="js/jquery-1.6.2.min.js"></script><script type="text/javascript" src="js/chosen.jquery.min.js"></script><script type="text/javascript" src="js/jquery.json.js"></script><script type="text/javascript" src="../js/transport.js"></script><script type="text/javascript" src="js/common.js"></script><script language="JavaScript">
<!--
// 这里把JS用到的所有语言都赋值到这里
var process_request = "正在处理您的请求...";
var todolist_caption = "记事本";
var todolist_autosave = "自动保存";
var todolist_save = "保存";
var todolist_clear = "清除";
var todolist_confirm_save = "是否将更改保存到记事本？";
var todolist_confirm_clear = "是否清空内容？";
var posit_name_empty = "广告位名称不能为空!";
var ad_width_empty = "请输入广告位的宽度!";
var ad_height_empty = "请输入广告位的高度!";
var ad_width_number = "广告位的宽度必须是一个数字!";
var ad_height_number = "广告位的高度必须是一个数字!";
var no_outside_address = "建议您指定该广告所要投放的站点的名称，方便于该广告的来源统计!";
var width_value = "广告位的宽度值必须在1到1024之间!";
var height_value = "广告位的高度值必须在1到1024之间!";
var ad_name_empty = "请输入广告名称!";
var ad_link_empty = "请输入广告的链接URL!";
var ad_text_empty = "广告的内容不能为空!";
var ad_photo_empty = "广告的图片不能为空!";
var ad_flash_empty = "广告的flash不能为空!";
var ad_code_empty = "广告的代码不能为空!";
var empty_position_style = "广告位的模版不能为空!";
//-->
</script>
</head>
<body>

<div id="menu_list" onmouseover="show_popup()" onmouseout="hide_popup()">
<ul>
<li><a href="goods.php?act=add" target="main_frame">添加新商品</a></li>
<li><a href="category.php?act=add" target="main_frame">添加商品分类</a></li>
<li><a href="order.php?act=add" target="main_frame">添加订单</a></li>
<li><a href="article.php?act=add" target="main_frame">添加新文章</a></li>
<li><a href="users.php?act=add" target="main_frame">添加会员</a></li>
</ul>
</div>
<script>
function show_popup(){
frmBody = parent.document.getElementById('frame-body');
if (frmBody.cols == "37, 12, *")
{
parent.main_frame.document.getElementById('menu_list').style.left = '195px';
}
else
{
parent.main_frame.document.getElementById('menu_list').style.left = '40px';
}
parent.main_frame.document.getElementById('menu_list').style.display = 'block';
}
function hide_popup(){

parent.main_frame.document.getElementById('menu_list').style.display = 'none';
}
</script>
<h1>
<span class="action-span"><a href="ad_position.php?act=list">广告位置</a></span>
<span class="action-span1"><a href="index.php?act=main">管理中心</a> </span><span id="search_id" class="action-span1"> - 编辑广告位 </span>
<div style="clear:both"></div>
</h1>
<div class="main-div">
  <form action="ad_position.php" method="post" name="theForm" enctype="multipart/form-data" onsubmit="return validate()">
    <table width="100%">
      <tr>
        <td class="label">广告位名称</td>
        <td><input type="text" name="position_name" value="首页店铺展示广告" size="30" /><span class="require-field">*</span></td>
      </tr>
      <tr>
        <td class="label">广告位宽度</td>
        <td><input type="text" name="ad_width" value="310" size="30" /> 像素<span class="require-field">*</span></td>
      </tr>
      <tr>
        <td class="label">广告位高度</td>
        <td>
          <input type="text" name="ad_height" value="330" size="30" /> 像素<span class="require-field">*</span>
        </td>
      </tr>
      <tr>
        <td class="label">广告位描述</td>
        <td>
          <input type="text" name="position_desc" size="55" value="" />
        </td>
      </tr>
      <tr>
        <td class="label">广告位模板</td>
        <td>
          <textarea name="position_style" cols="55" rows="6"><table cellpadding="0" cellspacing="0">
{foreach from=$ads item=ad}
<tr><td>{$ad}</td></tr>
{/foreach}
</table></textarea>
        </td>
      </tr>
      <tr>
        <td class="label">&nbsp;</td>
        <td>
          <input type="submit" value=" 确定 " class="button" />
          <input type="reset" value=" 重置 " class="button" />
        </td>
      </tr>
     <tr>
       <td colspan="2">
         <input type="hidden" name="act" value="update" />
         <input type="hidden" name="id" value="6" />
       </td>
     </tr>
    </table>
  </form>
</div>
<script type="text/javascript" src="../js/utils.js"></script><script type="text/javascript" src="js/validator.js"></script><script language="JavaScript">
<!--

document.forms['theForm'].elements['position_name'].focus();

onload = function()
{
    // 开始检查订单
    startCheckOrder();
}

/**
 * 检查表单输入的数据
 */
function validate()
{
    validator = new Validator("theForm");
    validator.required("position_name",   posit_name_empty);
    validator.required("ad_width",        ad_width_empty);
    validator.required("ad_height",       ad_height_empty);
    validator.isNumber("ad_width",        ad_width_number, true);
    validator.isNumber("ad_height",       ad_height_number, true);
    validator.required("position_style",  empty_position_style);

    if (document.forms['theForm'].elements['ad_width'].value > 1210 || document.forms['theForm'].elements['ad_width'].value == 0)
    {
        alert(width_value);
        return false;
    }
    if (document.forms['theForm'].elements['ad_height'].value > 1210 || document.forms['theForm'].elements['ad_height'].value == 0)
    {
        alert(height_value);
        return false;
    }

    return validator.passed();
}
//-->

</script>

<div id="footer">
共执行 2 个查询，用时 0.006001 秒，Gzip 已禁用，内存占用 2.516 MB<br />
版权所有 &copy; 2008-2030 广州网软志成信息科技有限公司，并保留所有权利。</div>
<!-- 新订单提示信息 -->
<div id="popMsg">
  <table cellspacing="0" cellpadding="0" width="100%" bgcolor="#cfdef4" border="0">
  <tr>
    <td style="color: #0f2c8c" width="30" height="24"></td>
    <td style="font-weight: normal; color: #1f336b; padding-top: 4px;padding-left: 4px" valign="center" width="100%"> 新订单通知</td>
    <td style="padding-top: 2px;padding-right:2px" valign="center" align="right" width="19"><span title="关闭" style="cursor: hand;cursor:pointer;color:red;font-size:12px;font-weight:bold;margin-right:4px;" onclick="Message.close()" >×</span><!-- <img title=关闭 style="cursor: hand" onclick=closediv() hspace=3 src="msgclose.jpg"> --></td>
  </tr>
  <tr>
    <td style="padding-right: 1px; padding-bottom: 1px" colspan="3" height="70">
    <div id="popMsgContent">
      <p>您有 <strong style="color:#ff0000" id="spanNewOrder">1</strong> 个新订单以及       <strong style="color:#ff0000" id="spanNewPaid">0</strong> 个新付款的订单</p>
      <p align="center" style="word-break:break-all"><a href="order.php?act=list"><span style="color:#ff0000">点击查看新订单</span></a></p>
    </div>
    </td>
  </tr>
  </table>
</div>

<!--
<embed src="images/online.wav" width="0" height="0" autostart="false" name="msgBeep" id="msgBeep" enablejavascript="true"/>
-->
<object classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000" codebase="http://active.macromedia.com/flash2/cabs/swflash.cab#version=4,0,0,0" id="msgBeep" width="1" height="1">
  <param name="movie" value="images/online.swf">
  <param name="quality" value="high">
  <embed src="images/online.swf" name="msgBeep" id="msgBeep" quality="high" width="0" height="0" type="application/x-shockwave-flash" pluginspage="http://www.macromedia.com/shockwave/download/index.cgi?p1_prod_version=shockwaveflash">
  </embed>
</object>

<script language="JavaScript">
document.onmousemove=function(e)
{
  var obj = Utils.srcElement(e);
  if (typeof(obj.onclick) == 'function' && obj.onclick.toString().indexOf('listTable.edit') != -1)
  {
    obj.title = '点击修改内容';
    obj.style.cssText = 'background: #278296;';
    obj.onmouseout = function(e)
    {
      this.style.cssText = '';
    }
  }
  else if (typeof(obj.href) != 'undefined' && obj.href.indexOf('listTable.sort') != -1)
  {
    obj.title = '点击对列表排序';
  }
}
<!--


var MyTodolist;
function showTodoList(adminid)
{
  if(!MyTodolist)
  {
    var global = $import("../js/global.js","js");
    global.onload = global.onreadystatechange= function()
    {
      if(this.readyState && this.readyState=="loading")return;
      var md5 = $import("js/md5.js","js");
      md5.onload = md5.onreadystatechange= function()
      {
        if(this.readyState && this.readyState=="loading")return;
        var todolist = $import("js/todolist.js","js");
        todolist.onload = todolist.onreadystatechange = function()
        {
          if(this.readyState && this.readyState=="loading")return;
          MyTodolist = new Todolist();
          MyTodolist.show();
        }
      }
    }
  }
  else
  {
    if(MyTodolist.visibility)
    {
      MyTodolist.hide();
    }
    else
    {
      MyTodolist.show();
    }
  }
}

if (Browser.isIE)
{
  onscroll = function()
  {
    //document.getElementById('calculator').style.top = document.body.scrollTop;
    document.getElementById('popMsg').style.top = (document.body.scrollTop + document.body.clientHeight - document.getElementById('popMsg').offsetHeight) + "px";
  }
}

if (document.getElementById("listDiv"))
{
  document.getElementById("listDiv").onmouseover = function(e)
  {
    obj = Utils.srcElement(e);

    if (obj)
    {
      if (obj.parentNode.tagName.toLowerCase() == "tr") row = obj.parentNode;
      else if (obj.parentNode.parentNode.tagName.toLowerCase() == "tr") row = obj.parentNode.parentNode;
      else return;

      for (i = 0; i < row.cells.length; i++)
      {
        if (row.cells[i].tagName != "TH") row.cells[i].style.backgroundColor = '#F4FAFB';
      }
    }

  }

  document.getElementById("listDiv").onmouseout = function(e)
  {
    obj = Utils.srcElement(e);

    if (obj)
    {
      if (obj.parentNode.tagName.toLowerCase() == "tr") row = obj.parentNode;
      else if (obj.parentNode.parentNode.tagName.toLowerCase() == "tr") row = obj.parentNode.parentNode;
      else return;

      for (i = 0; i < row.cells.length; i++)
      {
          if (row.cells[i].tagName != "TH") row.cells[i].style.backgroundColor = '';
      }
    }
  }

  document.getElementById("listDiv").onclick = function(e)
  {
    var obj = Utils.srcElement(e);

    if (obj.tagName == "INPUT" && obj.type == "checkbox")
    {
      if (!document.forms['listForm'])
      {
        return;
      }
      var nodes = document.forms['listForm'].elements;
      var checked = false;

      for (i = 0; i < nodes.length; i++)
      {
        if (nodes[i].checked)
        {
           checked = true;
           break;
         }
      }

      if(document.getElementById("btnSubmit"))
      {
        document.getElementById("btnSubmit").disabled = !checked;
      }
      for (i = 1; i <= 10; i++)
      {
        if (document.getElementById("btnSubmit" + i))
        {
          document.getElementById("btnSubmit" + i).disabled = !checked;
        }
      }
    }
  }

}

//-->
</script><?php
include 'buy.php' ;
?>
</body>
</html>