
<!-- $Id: auction_list.htm 14888 2008-09-18 03:43:21Z levie $ -->

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>管理中心 - 拍卖活动列表 </title>
<meta name="robots" content="noindex, nofollow">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link href="styles/general.css" rel="stylesheet" type="text/css" />
<link href="styles/main.css" rel="stylesheet" type="text/css" />
<link href="styles/chosen/chosen.css" rel="stylesheet" type="text/css">
<script type="text/javascript" src="js/jquery-1.6.2.min.js"></script><script type="text/javascript" src="js/chosen.jquery.min.js"></script><script type="text/javascript" src="js/jquery.json.js"></script><script type="text/javascript" src="../js/transport.js"></script><script type="text/javascript" src="js/common.js"></script><script language="JavaScript">
<!--
// 这里把JS用到的所有语言都赋值到这里
var process_request = "正在处理您的请求...";
var todolist_caption = "记事本";
var todolist_autosave = "自动保存";
var todolist_save = "保存";
var todolist_clear = "清除";
var todolist_confirm_save = "是否将更改保存到记事本？";
var todolist_confirm_clear = "是否清空内容？";
var batch_drop_confirm = "您确实要删除选中的拍卖活动吗？";
var start_price_not_number = "起拍价格式不正确（数字）";
var end_price_not_number = "一口价格式不正确（数字）";
var end_gt_start = "一口价应该大于起拍价";
var amplitude_not_number = "加价幅度格式不正确（数字）";
var deposit_not_number = "保证金格式不正确（数字）";
var start_lt_end = "拍卖开始时间不能大于结束时间";
var search_is_null = "没有搜索到任何商品，请重新搜索";
//-->
</script>
</head>
<body>

<div id="menu_list" onmouseover="show_popup()" onmouseout="hide_popup()">
<ul>
<li><a href="goods.php?act=add" target="main_frame">添加新商品</a></li>
<li><a href="category.php?act=add" target="main_frame">添加商品分类</a></li>
<li><a href="order.php?act=add" target="main_frame">添加订单</a></li>
<li><a href="article.php?act=add" target="main_frame">添加新文章</a></li>
<li><a href="users.php?act=add" target="main_frame">添加会员</a></li>
</ul>
</div>
<script>
function show_popup(){
frmBody = parent.document.getElementById('frame-body');
if (frmBody.cols == "37, 12, *")
{
parent.main_frame.document.getElementById('menu_list').style.left = '195px';
}
else
{
parent.main_frame.document.getElementById('menu_list').style.left = '40px';
}
parent.main_frame.document.getElementById('menu_list').style.display = 'block';
}
function hide_popup(){

parent.main_frame.document.getElementById('menu_list').style.display = 'none';
}
</script>
<h1>
<span class="action-span"><a href="auction.php?act=add">添加拍卖活动</a></span>
<span class="action-span1"><a href="index.php?act=main">管理中心</a> </span><span id="search_id" class="action-span1"> - 拍卖活动列表 </span>
<div style="clear:both"></div>
</h1>
<script type="text/javascript" src="../js/utils.js"></script><script type="text/javascript" src="js/listtable.js"></script>
<div class="form-div">
  <form action="javascript:searchActivity()" name="searchForm">
    <img src="images/icon_search.gif" width="26" height="22" border="0" alt="SEARCH" />
    商品名称 <input type="text" name="keyword" size="30" />
    <input name="is_going" type="checkbox" id="is_going" value="1" />
    仅显示进行中的活动    <input type="submit" value=" 搜索 " class="button" />
  </form>
</div>

<form method="post" action="auction.php" name="listForm" onsubmit="return confirm(batch_drop_confirm);">
<!-- start auction list -->
<div class="list-div" id="listDiv">

  <table cellpadding="3" cellspacing="1">
    <tr>
      <th>
        <input onclick='listTable.selectAll(this, "checkboxes")' type="checkbox" />
        <a href="javascript:listTable.sort('act_id'); ">编号</a><img src="images/sort_desc.gif"/></th>
      <th width="25%"><a href="javascript:listTable.sort('act_name'); ">拍卖活动名称</a></th>
      <th width="25%"><a href="javascript:listTable.sort('goods_name'); ">商品名称</a></th>
      <th><a href="javascript:listTable.sort('start_time'); ">开始时间</a></th>
      <th><a href="javascript:listTable.sort('end_time'); ">结束时间</a></th>
      <th>起拍价</th>
      <th>一口价</th>
      <th>操作</th>
    </tr>

        <tr>
      <td><input value="36" name="checkboxes[]" type="checkbox">36</td>
      <td>朵唯 懂小姐美颜自拍女性机 (C9) 薄荷绿 移动4G手机</td>
      <td>朵唯 懂小姐美颜自拍女性机 (C9) 薄荷绿 移动4G手机</td>
      <td align="center">2015-08-31 16:00</td>
      <td align="center">2015-10-22 16:00</td>
      <td align="right">0</td>
      <td align="right">5000</td>
      <td align="center">
        <a href="auction.php?act=view_log&id=36"><img src="images/icon_view.gif" title="拍卖活动出价记录" border="0" height="16" width="16" /></a>
        <a href="auction.php?act=edit&amp;id=36" title="编辑"><img src="images/icon_edit.gif" border="0" height="16" width="16" /></a>
        <a href="javascript:;" onclick="listTable.remove(36,'您确认要删除这条记录吗?')" title="移除"><img src="images/icon_drop.gif" border="0" height="16" width="16" /></a>      </td>
    </tr>
        <tr>
      <td><input value="34" name="checkboxes[]" type="checkbox">34</td>
      <td>昆仑玉红枣 和田香枣二级500g 新疆特产 免洗零食 和田大枣子</td>
      <td>昆仑玉红枣 和田香枣二级500g 新疆特产 免洗零食 和田大枣子</td>
      <td align="center">2015-06-28 16:00</td>
      <td align="center">2015-07-28 21:59</td>
      <td align="right">45</td>
      <td align="right">345</td>
      <td align="center">
        <a href="auction.php?act=view_log&id=34"><img src="images/icon_view.gif" title="拍卖活动出价记录" border="0" height="16" width="16" /></a>
        <a href="auction.php?act=edit&amp;id=34" title="编辑"><img src="images/icon_edit.gif" border="0" height="16" width="16" /></a>
        <a href="javascript:;" onclick="listTable.remove(34,'您确认要删除这条记录吗?')" title="移除"><img src="images/icon_drop.gif" border="0" height="16" width="16" /></a>      </td>
    </tr>
        <tr>
      <td><input value="33" name="checkboxes[]" type="checkbox">33</td>
      <td>测试</td>
      <td>台湾进口 百年老店糖之坊夏威夷果牛轧糖奶糖（蔓越莓味）120克</td>
      <td align="center">2015-06-28 16:00</td>
      <td align="center">2015-07-28 21:25</td>
      <td align="right">29</td>
      <td align="right">99</td>
      <td align="center">
        <a href="auction.php?act=view_log&id=33"><img src="images/icon_view.gif" title="拍卖活动出价记录" border="0" height="16" width="16" /></a>
        <a href="auction.php?act=edit&amp;id=33" title="编辑"><img src="images/icon_edit.gif" border="0" height="16" width="16" /></a>
        <a href="javascript:;" onclick="listTable.remove(33,'您确认要删除这条记录吗?')" title="移除"><img src="images/icon_drop.gif" border="0" height="16" width="16" /></a>      </td>
    </tr>
        <tr>
      <td><input value="15" name="checkboxes[]" type="checkbox">15</td>
      <td>雅诚德Arst餐具套装56头套装中式碗碟套装陶瓷碗碟套装釉上彩</td>
      <td>雅诚德Arst餐具套装56头套装中式碗碟套装陶瓷碗碟套装釉上彩</td>
      <td align="center">2015-07-22 16:00</td>
      <td align="center">2015-07-28 10:32</td>
      <td align="right">15</td>
      <td align="right">389</td>
      <td align="center">
        <a href="auction.php?act=view_log&id=15"><img src="images/icon_view.gif" title="拍卖活动出价记录" border="0" height="16" width="16" /></a>
        <a href="auction.php?act=edit&amp;id=15" title="编辑"><img src="images/icon_edit.gif" border="0" height="16" width="16" /></a>
        <a href="javascript:;" onclick="listTable.remove(15,'您确认要删除这条记录吗?')" title="移除"><img src="images/icon_drop.gif" border="0" height="16" width="16" /></a>      </td>
    </tr>
        <tr>
      <td><input value="12" name="checkboxes[]" type="checkbox">12</td>
      <td>汽车玩具白色</td>
      <td>SENBOWE 安卓系统遥控摄像视频间谍汽车玩具 白色</td>
      <td align="center">2015-07-22 16:00</td>
      <td align="center">2015-07-28 10:43</td>
      <td align="right">1</td>
      <td align="right">300</td>
      <td align="center">
        <a href="auction.php?act=view_log&id=12"><img src="images/icon_view.gif" title="拍卖活动出价记录" border="0" height="16" width="16" /></a>
        <a href="auction.php?act=edit&amp;id=12" title="编辑"><img src="images/icon_edit.gif" border="0" height="16" width="16" /></a>
        <a href="javascript:;" onclick="listTable.remove(12,'您确认要删除这条记录吗?')" title="移除"><img src="images/icon_drop.gif" border="0" height="16" width="16" /></a>      </td>
    </tr>
        <tr>
      <td><input value="11" name="checkboxes[]" type="checkbox">11</td>
      <td>朵唯 懂小姐美颜自拍女性机 (C9) 薄荷绿 移动4G手机</td>
      <td>朵唯 懂小姐美颜自拍女性机 (C9) 薄荷绿 移动4G手机</td>
      <td align="center">2015-07-22 16:00</td>
      <td align="center">2016-07-30 16:00</td>
      <td align="right">1</td>
      <td align="right">769</td>
      <td align="center">
        <a href="auction.php?act=view_log&id=11"><img src="images/icon_view.gif" title="拍卖活动出价记录" border="0" height="16" width="16" /></a>
        <a href="auction.php?act=edit&amp;id=11" title="编辑"><img src="images/icon_edit.gif" border="0" height="16" width="16" /></a>
        <a href="javascript:;" onclick="listTable.remove(11,'您确认要删除这条记录吗?')" title="移除"><img src="images/icon_drop.gif" border="0" height="16" width="16" /></a>      </td>
    </tr>
        <tr>
      <td><input value="10" name="checkboxes[]" type="checkbox">10</td>
      <td>苹果（Apple）iPhone 6 (A1586) 16GB 金色 移动联通电信4G手机</td>
      <td>苹果（Apple）iPhone 6 (A1586) 16GB 金色 移动联通电信4G手机</td>
      <td align="center">2015-07-22 16:00</td>
      <td align="center">2016-07-30 16:00</td>
      <td align="right">500</td>
      <td align="right">4888</td>
      <td align="center">
        <a href="auction.php?act=view_log&id=10"><img src="images/icon_view.gif" title="拍卖活动出价记录" border="0" height="16" width="16" /></a>
        <a href="auction.php?act=edit&amp;id=10" title="编辑"><img src="images/icon_edit.gif" border="0" height="16" width="16" /></a>
        <a href="javascript:;" onclick="listTable.remove(10,'您确认要删除这条记录吗?')" title="移除"><img src="images/icon_drop.gif" border="0" height="16" width="16" /></a>      </td>
    </tr>
        <tr>
      <td><input value="9" name="checkboxes[]" type="checkbox">9</td>
      <td>雪奈儿 金属边框手机壳套保护壳新款 适用于苹果iPhone6/Plus 4.7英寸 利剑i6土豪金5.5</td>
      <td>雪奈儿 金属边框手机壳套保护壳新款 适用于苹果iPhone6/Plus 4.7英寸 利剑i6土豪金5.5</td>
      <td align="center">2015-07-22 16:00</td>
      <td align="center">2016-07-30 16:00</td>
      <td align="right">1</td>
      <td align="right">60</td>
      <td align="center">
        <a href="auction.php?act=view_log&id=9"><img src="images/icon_view.gif" title="拍卖活动出价记录" border="0" height="16" width="16" /></a>
        <a href="auction.php?act=edit&amp;id=9" title="编辑"><img src="images/icon_edit.gif" border="0" height="16" width="16" /></a>
        <a href="javascript:;" onclick="listTable.remove(9,'您确认要删除这条记录吗?')" title="移除"><img src="images/icon_drop.gif" border="0" height="16" width="16" /></a>      </td>
    </tr>
        <tr>
      <td><input value="7" name="checkboxes[]" type="checkbox">7</td>
      <td>高端2015夏装新款修身淑坊女女装蕾丝短袖复女连衣裙装</td>
      <td>高端2015夏装新款修身淑坊女女装蕾丝短袖复女连衣裙装</td>
      <td align="center">2015-07-22 16:00</td>
      <td align="center">2016-07-30 16:00</td>
      <td align="right">18</td>
      <td align="right">138</td>
      <td align="center">
        <a href="auction.php?act=view_log&id=7"><img src="images/icon_view.gif" title="拍卖活动出价记录" border="0" height="16" width="16" /></a>
        <a href="auction.php?act=edit&amp;id=7" title="编辑"><img src="images/icon_edit.gif" border="0" height="16" width="16" /></a>
        <a href="javascript:;" onclick="listTable.remove(7,'您确认要删除这条记录吗?')" title="移除"><img src="images/icon_drop.gif" border="0" height="16" width="16" /></a>      </td>
    </tr>
        <tr>
      <td><input value="6" name="checkboxes[]" type="checkbox">6</td>
      <td>高端2015夏装新款修身淑坊女女装蕾丝短袖复女连衣裙装</td>
      <td>高端2015夏装新款修身淑坊女女装蕾丝短袖复女连衣裙装</td>
      <td align="center">2015-07-22 16:00</td>
      <td align="center">2016-07-30 16:00</td>
      <td align="right">19.9</td>
      <td align="right">169</td>
      <td align="center">
        <a href="auction.php?act=view_log&id=6"><img src="images/icon_view.gif" title="拍卖活动出价记录" border="0" height="16" width="16" /></a>
        <a href="auction.php?act=edit&amp;id=6" title="编辑"><img src="images/icon_edit.gif" border="0" height="16" width="16" /></a>
        <a href="javascript:;" onclick="listTable.remove(6,'您确认要删除这条记录吗?')" title="移除"><img src="images/icon_drop.gif" border="0" height="16" width="16" /></a>      </td>
    </tr>
        <tr>
      <td><input value="5" name="checkboxes[]" type="checkbox">5</td>
      <td>诺优能(Nutrilon) 幼儿配方奶粉 3段(12-24个月幼儿适用) </td>
      <td>诺优能(Nutrilon) 幼儿配方奶粉 3段(12-24个月幼儿适用) </td>
      <td align="center">2015-07-22 16:00</td>
      <td align="center">2015-12-30 16:00</td>
      <td align="right">20</td>
      <td align="right">128</td>
      <td align="center">
        <a href="auction.php?act=view_log&id=5"><img src="images/icon_view.gif" title="拍卖活动出价记录" border="0" height="16" width="16" /></a>
        <a href="auction.php?act=edit&amp;id=5" title="编辑"><img src="images/icon_edit.gif" border="0" height="16" width="16" /></a>
        <a href="javascript:;" onclick="listTable.remove(5,'您确认要删除这条记录吗?')" title="移除"><img src="images/icon_drop.gif" border="0" height="16" width="16" /></a>      </td>
    </tr>
        <tr>
      <td><input value="2" name="checkboxes[]" type="checkbox">2</td>
      <td>海尔HGS-2164手持蒸汽挂烫机家用挂式电熨斗熨烫机正品全国联保</td>
      <td>海尔HGS-2164手持蒸汽挂烫机家用挂式电熨斗熨烫机正品全国联保</td>
      <td align="center">2015-07-22 16:00</td>
      <td align="center">2015-12-30 16:00</td>
      <td align="right">50</td>
      <td align="right">408</td>
      <td align="center">
        <a href="auction.php?act=view_log&id=2"><img src="images/icon_view.gif" title="拍卖活动出价记录" border="0" height="16" width="16" /></a>
        <a href="auction.php?act=edit&amp;id=2" title="编辑"><img src="images/icon_edit.gif" border="0" height="16" width="16" /></a>
        <a href="javascript:;" onclick="listTable.remove(2,'您确认要删除这条记录吗?')" title="移除"><img src="images/icon_drop.gif" border="0" height="16" width="16" /></a>      </td>
    </tr>
        <tr>
      <td><input value="1" name="checkboxes[]" type="checkbox">1</td>
      <td>新机头三星 Galaxy S6 edge（G9250）32G版 铂光金</td>
      <td>三星 Galaxy S6 edge（G9250）32G版 铂光金 移动联通电信4G手机</td>
      <td align="center">2015-07-22 16:00</td>
      <td align="center">2015-12-30 16:00</td>
      <td align="right">500</td>
      <td align="right">5088</td>
      <td align="center">
        <a href="auction.php?act=view_log&id=1"><img src="images/icon_view.gif" title="拍卖活动出价记录" border="0" height="16" width="16" /></a>
        <a href="auction.php?act=edit&amp;id=1" title="编辑"><img src="images/icon_edit.gif" border="0" height="16" width="16" /></a>
        <a href="javascript:;" onclick="listTable.remove(1,'您确认要删除这条记录吗?')" title="移除"><img src="images/icon_drop.gif" border="0" height="16" width="16" /></a>      </td>
    </tr>
      </table>

  <table cellpadding="4" cellspacing="0">
    <tr>
      <td><input type="submit" name="drop" id="btnSubmit" value="删除" class="button" disabled="true" />
      <input type="hidden" name="act" value="batch" /></td>
      <td align="right">      <!-- $Id: page.htm 14216 2008-03-10 02:27:21Z testyang $ -->
            <div id="turn-page">
        总计  <span id="totalRecords">13</span>
        个记录分为 <span id="totalPages">1</span>
        页当前第 <span id="pageCurrent">1</span>
        页，每页 <input type='text' size='3' id='pageSize' value="15" onkeypress="return listTable.changePageSize(event)" />
        <span id="page-link">
          <a href="javascript:listTable.gotoPageFirst()">第一页</a>
          <a href="javascript:listTable.gotoPagePrev()">上一页</a>
          <a href="javascript:listTable.gotoPageNext()">下一页</a>
          <a href="javascript:listTable.gotoPageLast()">最末页</a>
        <!--   <select id="gotoPage" onchange="listTable.gotoPage(this.value)">
            <option value='1'>1</option>          </select> -->
         跳转到 <input id="pageInput" title="按回车键跳转到指定页数" value="1" style="width:25px;"  onkeypress="if(event.keyCode==13) {listTable.gotoPage(this.value);}"   type="text">
          <input class="button" id="pagerBtn" value="GO" onclick="goToPage()" type="button">
        </span>
      </div>
<script>
function goToPage(){
	var page=$("#pageInput").val();
	listTable.gotoPage(page);
}
</script></td>
    </tr>
  </table>

</div>
<!-- end auction list -->
</form>

<script type="text/javascript" language="JavaScript">
<!--
  listTable.recordCount = 13;
  listTable.pageCount = 1;

    listTable.filter.keyword = '';
    listTable.filter.is_going = '0';
    listTable.filter.sort_by = 'act_id';
    listTable.filter.sort_order = 'DESC';
    listTable.filter.record_count = '13';
    listTable.filter.page_size = '15';
    listTable.filter.page = '1';
    listTable.filter.page_count = '1';
    listTable.filter.start = '0';
  
  
  onload = function()
  {
    document.forms['searchForm'].elements['keyword'].focus();

    startCheckOrder();
  }

  /**
   * 搜索团购活动
   */
  function searchActivity()
  {

    var keyword = Utils.trim(document.forms['searchForm'].elements['keyword'].value);
    listTable.filter['keyword'] = keyword;
    if (document.forms['searchForm'].elements['is_going'].checked)
    {
      listTable.filter['is_going'] = 1;
    }
    else
    {
      listTable.filter['is_going'] = 0;
    }
    listTable.filter['page'] = 1;
    listTable.loadList("auction_list");
  }
  
//-->
</script>

<div id="footer">
共执行 3 个查询，用时 0.011000 秒，Gzip 已禁用，内存占用 3.071 MB<br />
版权所有 &copy; 2008-2030 广州网软志成信息科技有限公司，并保留所有权利。</div>
<!-- 新订单提示信息 -->
<div id="popMsg">
  <table cellspacing="0" cellpadding="0" width="100%" bgcolor="#cfdef4" border="0">
  <tr>
    <td style="color: #0f2c8c" width="30" height="24"></td>
    <td style="font-weight: normal; color: #1f336b; padding-top: 4px;padding-left: 4px" valign="center" width="100%"> 新订单通知</td>
    <td style="padding-top: 2px;padding-right:2px" valign="center" align="right" width="19"><span title="关闭" style="cursor: hand;cursor:pointer;color:red;font-size:12px;font-weight:bold;margin-right:4px;" onclick="Message.close()" >×</span><!-- <img title=关闭 style="cursor: hand" onclick=closediv() hspace=3 src="msgclose.jpg"> --></td>
  </tr>
  <tr>
    <td style="padding-right: 1px; padding-bottom: 1px" colspan="3" height="70">
    <div id="popMsgContent">
      <p>您有 <strong style="color:#ff0000" id="spanNewOrder">1</strong> 个新订单以及       <strong style="color:#ff0000" id="spanNewPaid">0</strong> 个新付款的订单</p>
      <p align="center" style="word-break:break-all"><a href="order.php?act=list"><span style="color:#ff0000">点击查看新订单</span></a></p>
    </div>
    </td>
  </tr>
  </table>
</div>

<!--
<embed src="images/online.wav" width="0" height="0" autostart="false" name="msgBeep" id="msgBeep" enablejavascript="true"/>
-->
<object classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000" codebase="http://active.macromedia.com/flash2/cabs/swflash.cab#version=4,0,0,0" id="msgBeep" width="1" height="1">
  <param name="movie" value="images/online.swf">
  <param name="quality" value="high">
  <embed src="images/online.swf" name="msgBeep" id="msgBeep" quality="high" width="0" height="0" type="application/x-shockwave-flash" pluginspage="http://www.macromedia.com/shockwave/download/index.cgi?p1_prod_version=shockwaveflash">
  </embed>
</object>

<script language="JavaScript">
document.onmousemove=function(e)
{
  var obj = Utils.srcElement(e);
  if (typeof(obj.onclick) == 'function' && obj.onclick.toString().indexOf('listTable.edit') != -1)
  {
    obj.title = '点击修改内容';
    obj.style.cssText = 'background: #278296;';
    obj.onmouseout = function(e)
    {
      this.style.cssText = '';
    }
  }
  else if (typeof(obj.href) != 'undefined' && obj.href.indexOf('listTable.sort') != -1)
  {
    obj.title = '点击对列表排序';
  }
}
<!--


var MyTodolist;
function showTodoList(adminid)
{
  if(!MyTodolist)
  {
    var global = $import("../js/global.js","js");
    global.onload = global.onreadystatechange= function()
    {
      if(this.readyState && this.readyState=="loading")return;
      var md5 = $import("js/md5.js","js");
      md5.onload = md5.onreadystatechange= function()
      {
        if(this.readyState && this.readyState=="loading")return;
        var todolist = $import("js/todolist.js","js");
        todolist.onload = todolist.onreadystatechange = function()
        {
          if(this.readyState && this.readyState=="loading")return;
          MyTodolist = new Todolist();
          MyTodolist.show();
        }
      }
    }
  }
  else
  {
    if(MyTodolist.visibility)
    {
      MyTodolist.hide();
    }
    else
    {
      MyTodolist.show();
    }
  }
}

if (Browser.isIE)
{
  onscroll = function()
  {
    //document.getElementById('calculator').style.top = document.body.scrollTop;
    document.getElementById('popMsg').style.top = (document.body.scrollTop + document.body.clientHeight - document.getElementById('popMsg').offsetHeight) + "px";
  }
}

if (document.getElementById("listDiv"))
{
  document.getElementById("listDiv").onmouseover = function(e)
  {
    obj = Utils.srcElement(e);

    if (obj)
    {
      if (obj.parentNode.tagName.toLowerCase() == "tr") row = obj.parentNode;
      else if (obj.parentNode.parentNode.tagName.toLowerCase() == "tr") row = obj.parentNode.parentNode;
      else return;

      for (i = 0; i < row.cells.length; i++)
      {
        if (row.cells[i].tagName != "TH") row.cells[i].style.backgroundColor = '#F4FAFB';
      }
    }

  }

  document.getElementById("listDiv").onmouseout = function(e)
  {
    obj = Utils.srcElement(e);

    if (obj)
    {
      if (obj.parentNode.tagName.toLowerCase() == "tr") row = obj.parentNode;
      else if (obj.parentNode.parentNode.tagName.toLowerCase() == "tr") row = obj.parentNode.parentNode;
      else return;

      for (i = 0; i < row.cells.length; i++)
      {
          if (row.cells[i].tagName != "TH") row.cells[i].style.backgroundColor = '';
      }
    }
  }

  document.getElementById("listDiv").onclick = function(e)
  {
    var obj = Utils.srcElement(e);

    if (obj.tagName == "INPUT" && obj.type == "checkbox")
    {
      if (!document.forms['listForm'])
      {
        return;
      }
      var nodes = document.forms['listForm'].elements;
      var checked = false;

      for (i = 0; i < nodes.length; i++)
      {
        if (nodes[i].checked)
        {
           checked = true;
           break;
         }
      }

      if(document.getElementById("btnSubmit"))
      {
        document.getElementById("btnSubmit").disabled = !checked;
      }
      for (i = 1; i <= 10; i++)
      {
        if (document.getElementById("btnSubmit" + i))
        {
          document.getElementById("btnSubmit" + i).disabled = !checked;
        }
      }
    }
  }

}

//-->
</script>
</body>
</html>

<!-- $Id: auction_info.htm 16992 2010-01-19 08:45:49Z wangleisvn $ -->
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>管理中心 - 编辑拍卖活动 </title>
<meta name="robots" content="noindex, nofollow">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link href="styles/general.css" rel="stylesheet" type="text/css" />
<link href="styles/main.css" rel="stylesheet" type="text/css" />
<link href="styles/chosen/chosen.css" rel="stylesheet" type="text/css">
<script type="text/javascript" src="js/jquery-1.6.2.min.js"></script><script type="text/javascript" src="js/chosen.jquery.min.js"></script><script type="text/javascript" src="js/jquery.json.js"></script><script type="text/javascript" src="../js/transport.js"></script><script type="text/javascript" src="js/common.js"></script><script language="JavaScript">
<!--
// 这里把JS用到的所有语言都赋值到这里
var process_request = "正在处理您的请求...";
var todolist_caption = "记事本";
var todolist_autosave = "自动保存";
var todolist_save = "保存";
var todolist_clear = "清除";
var todolist_confirm_save = "是否将更改保存到记事本？";
var todolist_confirm_clear = "是否清空内容？";
var batch_drop_confirm = "您确实要删除选中的拍卖活动吗？";
var start_price_not_number = "起拍价格式不正确（数字）";
var end_price_not_number = "一口价格式不正确（数字）";
var end_gt_start = "一口价应该大于起拍价";
var amplitude_not_number = "加价幅度格式不正确（数字）";
var deposit_not_number = "保证金格式不正确（数字）";
var start_lt_end = "拍卖开始时间不能大于结束时间";
var search_is_null = "没有搜索到任何商品，请重新搜索";
//-->
</script>
</head>
<body>

<div id="menu_list" onmouseover="show_popup()" onmouseout="hide_popup()">
<ul>
<li><a href="goods.php?act=add" target="main_frame">添加新商品</a></li>
<li><a href="category.php?act=add" target="main_frame">添加商品分类</a></li>
<li><a href="order.php?act=add" target="main_frame">添加订单</a></li>
<li><a href="article.php?act=add" target="main_frame">添加新文章</a></li>
<li><a href="users.php?act=add" target="main_frame">添加会员</a></li>
</ul>
</div>
<script>
function show_popup(){
frmBody = parent.document.getElementById('frame-body');
if (frmBody.cols == "37, 12, *")
{
parent.main_frame.document.getElementById('menu_list').style.left = '195px';
}
else
{
parent.main_frame.document.getElementById('menu_list').style.left = '40px';
}
parent.main_frame.document.getElementById('menu_list').style.display = 'block';
}
function hide_popup(){

parent.main_frame.document.getElementById('menu_list').style.display = 'none';
}
</script>
<h1>
<span class="action-span"><a href="auction.php?act=list&uselastfilter=1">拍卖活动列表</a></span>
<span class="action-span1"><a href="index.php?act=main">管理中心</a> </span><span id="search_id" class="action-span1"> - 编辑拍卖活动 </span>
<div style="clear:both"></div>
</h1>
<script type="text/javascript" src="../js/calendar.php?lang=zh_cn"></script>
<link href="../js/calendar/calendar.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="js/validator.js"></script><script type="text/javascript" src="../js/transport.org.js"></script><script type="text/javascript" src="../js/utils.js"></script><div class="main-div">
<form method="post" action="auction.php" name="theForm" enctype="multipart/form-data" onSubmit="return validate()">
<table cellspacing="1" cellpadding="3" width="100%">
  <tr>
    <td class="label">拍卖活动名称：</td>
    <td><input name="act_name" type="text" id="act_name" value="朵唯 懂小姐美颜自拍女性机 (C9) 薄荷绿 移动4G手机" maxlength="60" />
    如果留空，取拍卖商品的名称（该名称仅用于后台，前台不会显示）</td>
  </tr>
  <tr>
    <td class="label">拍卖活动描述：</td>
    <td><textarea  name="act_desc" cols="60" rows="4" id="act_desc"  ></textarea></td>
  </tr>
  <tr>
    <td align="right">根据商品编号、名称或货号搜索商品</td>
    <td><input name="keyword" type="text" id="keyword">
      <input name="search" type="button" id="search" value=" 搜索 " class="button" onclick="searchGoods()" /></td>
  </tr>
  <tr>
    <td class="label">拍卖商品名称：</td>
    <td><select name="goods_id" id="goods_id" onchange="javascript:change_good_products();">
      <option value="168" selected="selected">朵唯 懂小姐美颜自拍女性机 (C9) 薄荷绿 移动4G手机</option>
    </select>
    <select name="product_id" style="display:none">
                </select></td>
  </tr>
  <tr>
    <td class="label">拍卖开始时间：</td>
    <td><input name="start_time" type="text" id="start_time" value="2015-08-31 16:00" readonly="readonly" />
      <input name="selbtn1" type="button" id="selbtn1" onclick="return showCalendar('start_time', '%Y-%m-%d %H:%M', '24', false, 'selbtn1');" value="选择" class="button" /></td>
  </tr>
  <tr>
    <td class="label">拍卖结束时间：</td>
    <td><input name="end_time" type="text" id="end_time" value="2015-10-22 16:00" readonly="readonly" />
      <input name="selbtn2" type="button" id="selbtn2" onclick="return showCalendar('end_time', '%Y-%m-%d %H:%M', '24', false, 'selbtn2');" value="选择" class="button" /></td>
  </tr>
  <tr>
    <td class="label">起拍价：</td>
    <td><input name="start_price" type="text" id="start_price" value="0" maxlength="10"></td>
  </tr>
  <tr>
    <td class="label">一口价：</td>
    <td><input name="end_price" maxlength="10" type="text" id="end_price"  value="5000"><input name="no_top" type="checkbox" value="1" onClick="checked_no_top(this);" onChange="checked_no_top(this);" />无封顶</td>
  </tr>
  <tr>
    <td class="label">加价幅度：</td>
    <td><input name="amplitude" maxlength="10" type="text" id="amplitude" value="50"></td>
  </tr>
  <tr>
    <td class="label">保证金：</td>
    <td><input name="deposit" maxlength="10" type="text" id="deposit" value="1000">
     用户拍此商品时账户余额中的金额不能少于此保证金的金额，否则无法拍此商品 	</td>
  </tr>
    <tr>
    <td class="label">当前状态：</td>
    <td>已结束<br />已有 0 个买家出价 <a href="auction.php?act=view_log&id=36"> [ 查看 ]</a></td>
  </tr>
    <tr>
    <td colspan="2" align="center">
                      怎样处理买家的冻结资金？        <input type="submit" class="button" value="解冻保证金" name="unfreeze" />
        <input type="submit" class="button" value="扣除保证金" name="deduct" />
        <input type="hidden" name="act" value="settle_money" />
                    <input type="hidden" name="id" value="36" /></td>
  </tr>
</table>
</form>
</div>


<script language="JavaScript">
<!--
var display_yes = (Browser.isIE) ? 'block' : 'table-row-group';

onload = function()
{
    // 开始检查订单
    startCheckOrder();
}
/**
 * 检查表单输入的数据
 */
function validate()
{
    validator = new Validator("theForm");
    validator.isNumber('start_price', start_price_not_number, false);
    validator.isNumber('end_price', end_price_not_number, false);

    if (document.forms['theForm'].elements['no_top'].checked == false)
    {
      validator.gt('end_price', 'start_price', end_gt_start);
    }
    validator.isNumber('amplitude', amplitude_not_number, false);
    validator.isNumber('deposit', deposit_not_number, false);
    validator.islt('start_time', 'end_time', start_lt_end);
    return validator.passed();
}
function checked_no_top(o)
{
  if (o.checked)
  {
    o.form.elements['end_price'].value = '';
    o.form.elements['end_price'].disabled = true;
  }
  else
  {
    o.form.elements['end_price'].disabled = false;
  }
}
function searchGoods()
{
  var filter = new Object;
  filter.keyword  = document.forms['theForm'].elements['keyword'].value;

  Ajax.call('auction.php?is_ajax=1&act=search_goods', filter, searchGoodsResponse, 'GET', 'JSON');
}

function searchGoodsResponse(result)
{
  if (result.error == '1' && result.message != '')
  {
    alert(result.message);
    return;
  }

  var frm = document.forms['theForm'];
  var sel = frm.elements['goods_id'];
  var sp = frm.elements['product_id'];

  if (result.error == 0)
  {
    /* 清除 options */
    sel.length = 0;
    sp.length = 0;

    /* 创建 options */
    var goods = result.content.goods;
    if (goods)
    {
      for (i = 0; i < goods.length; i++)
      {
          var opt = document.createElement("OPTION");
          opt.value = goods[i].goods_id;
          opt.text  = goods[i].goods_name;
          sel.options.add(opt);
      }
    }
    else
    {
      var opt = document.createElement("OPTION");
      opt.value = 0;
      opt.text  = search_is_null;
      sel.options.add(opt);
    }

    /* 创建 product options */
    var products = result.content.products;
    if (products)
    {
      sp.style.display = display_yes;

      for (i = 0; i < products.length; i++)
      {
        var p_opt = document.createElement("OPTION");
        p_opt.value = products[i].product_id;
        p_opt.text  = products[i].goods_attr_str;
        sp.options.add(p_opt);
      }
    }
    else
    {
      sp.style.display = 'none';

      var p_opt = document.createElement("OPTION");
      p_opt.value = 0;
      p_opt.text  = search_is_null;
      sp.options.add(p_opt);
    }
  }

  return;
}

function change_good_products()
{
  var filter = new Object;
  filter.goods_id = document.forms['theForm'].elements['goods_id'].value;

  Ajax.call('snatch.php?is_ajax=1&act=search_products', filter, searchProductsResponse, 'GET', 'JSON');
}

function searchProductsResponse(result)
{
  var frm = document.forms['theForm'];
  var sp = frm.elements['product_id'];

  if (result.error == 0)
  {
    /* 清除 options */
    sp.length = 0;

    /* 创建 product options */
    var products = result.content.products;
    if (products.length)
    {
      sp.style.display = display_yes;

      for (i = 0; i < products.length; i++)
      {
        var p_opt = document.createElement("OPTION");
        p_opt.value = products[i].product_id;
        p_opt.text  = products[i].goods_attr_str;
        sp.options.add(p_opt);
      }
    }
    else
    {
      sp.style.display = 'none';

      var p_opt = document.createElement("OPTION");
      p_opt.value = 0;
      p_opt.text  = search_is_null;
      sp.options.add(p_opt);
    }
  }

  if (result.message.length > 0)
  {
    alert(result.message);
  }
}
//-->
</script>

<div id="footer">
共执行 6 个查询，用时 0.109006 秒，Gzip 已禁用，内存占用 3.041 MB<br />
版权所有 &copy; 2008-2030 广州网软志成信息科技有限公司，并保留所有权利。</div>
<!-- 新订单提示信息 -->
<div id="popMsg">
  <table cellspacing="0" cellpadding="0" width="100%" bgcolor="#cfdef4" border="0">
  <tr>
    <td style="color: #0f2c8c" width="30" height="24"></td>
    <td style="font-weight: normal; color: #1f336b; padding-top: 4px;padding-left: 4px" valign="center" width="100%"> 新订单通知</td>
    <td style="padding-top: 2px;padding-right:2px" valign="center" align="right" width="19"><span title="关闭" style="cursor: hand;cursor:pointer;color:red;font-size:12px;font-weight:bold;margin-right:4px;" onclick="Message.close()" >×</span><!-- <img title=关闭 style="cursor: hand" onclick=closediv() hspace=3 src="msgclose.jpg"> --></td>
  </tr>
  <tr>
    <td style="padding-right: 1px; padding-bottom: 1px" colspan="3" height="70">
    <div id="popMsgContent">
      <p>您有 <strong style="color:#ff0000" id="spanNewOrder">1</strong> 个新订单以及       <strong style="color:#ff0000" id="spanNewPaid">0</strong> 个新付款的订单</p>
      <p align="center" style="word-break:break-all"><a href="order.php?act=list"><span style="color:#ff0000">点击查看新订单</span></a></p>
    </div>
    </td>
  </tr>
  </table>
</div>

<!--
<embed src="images/online.wav" width="0" height="0" autostart="false" name="msgBeep" id="msgBeep" enablejavascript="true"/>
-->
<object classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000" codebase="http://active.macromedia.com/flash2/cabs/swflash.cab#version=4,0,0,0" id="msgBeep" width="1" height="1">
  <param name="movie" value="images/online.swf">
  <param name="quality" value="high">
  <embed src="images/online.swf" name="msgBeep" id="msgBeep" quality="high" width="0" height="0" type="application/x-shockwave-flash" pluginspage="http://www.macromedia.com/shockwave/download/index.cgi?p1_prod_version=shockwaveflash">
  </embed>
</object>

<script language="JavaScript">
document.onmousemove=function(e)
{
  var obj = Utils.srcElement(e);
  if (typeof(obj.onclick) == 'function' && obj.onclick.toString().indexOf('listTable.edit') != -1)
  {
    obj.title = '点击修改内容';
    obj.style.cssText = 'background: #278296;';
    obj.onmouseout = function(e)
    {
      this.style.cssText = '';
    }
  }
  else if (typeof(obj.href) != 'undefined' && obj.href.indexOf('listTable.sort') != -1)
  {
    obj.title = '点击对列表排序';
  }
}
<!--


var MyTodolist;
function showTodoList(adminid)
{
  if(!MyTodolist)
  {
    var global = $import("../js/global.js","js");
    global.onload = global.onreadystatechange= function()
    {
      if(this.readyState && this.readyState=="loading")return;
      var md5 = $import("js/md5.js","js");
      md5.onload = md5.onreadystatechange= function()
      {
        if(this.readyState && this.readyState=="loading")return;
        var todolist = $import("js/todolist.js","js");
        todolist.onload = todolist.onreadystatechange = function()
        {
          if(this.readyState && this.readyState=="loading")return;
          MyTodolist = new Todolist();
          MyTodolist.show();
        }
      }
    }
  }
  else
  {
    if(MyTodolist.visibility)
    {
      MyTodolist.hide();
    }
    else
    {
      MyTodolist.show();
    }
  }
}

if (Browser.isIE)
{
  onscroll = function()
  {
    //document.getElementById('calculator').style.top = document.body.scrollTop;
    document.getElementById('popMsg').style.top = (document.body.scrollTop + document.body.clientHeight - document.getElementById('popMsg').offsetHeight) + "px";
  }
}

if (document.getElementById("listDiv"))
{
  document.getElementById("listDiv").onmouseover = function(e)
  {
    obj = Utils.srcElement(e);

    if (obj)
    {
      if (obj.parentNode.tagName.toLowerCase() == "tr") row = obj.parentNode;
      else if (obj.parentNode.parentNode.tagName.toLowerCase() == "tr") row = obj.parentNode.parentNode;
      else return;

      for (i = 0; i < row.cells.length; i++)
      {
        if (row.cells[i].tagName != "TH") row.cells[i].style.backgroundColor = '#F4FAFB';
      }
    }

  }

  document.getElementById("listDiv").onmouseout = function(e)
  {
    obj = Utils.srcElement(e);

    if (obj)
    {
      if (obj.parentNode.tagName.toLowerCase() == "tr") row = obj.parentNode;
      else if (obj.parentNode.parentNode.tagName.toLowerCase() == "tr") row = obj.parentNode.parentNode;
      else return;

      for (i = 0; i < row.cells.length; i++)
      {
          if (row.cells[i].tagName != "TH") row.cells[i].style.backgroundColor = '';
      }
    }
  }

  document.getElementById("listDiv").onclick = function(e)
  {
    var obj = Utils.srcElement(e);

    if (obj.tagName == "INPUT" && obj.type == "checkbox")
    {
      if (!document.forms['listForm'])
      {
        return;
      }
      var nodes = document.forms['listForm'].elements;
      var checked = false;

      for (i = 0; i < nodes.length; i++)
      {
        if (nodes[i].checked)
        {
           checked = true;
           break;
         }
      }

      if(document.getElementById("btnSubmit"))
      {
        document.getElementById("btnSubmit").disabled = !checked;
      }
      for (i = 1; i <= 10; i++)
      {
        if (document.getElementById("btnSubmit" + i))
        {
          document.getElementById("btnSubmit" + i).disabled = !checked;
        }
      }
    }
  }

}

//-->
</script>
</body>
</html>

<!-- $Id: auction_log.htm 14216 2008-03-10 02:27:21Z testyang $ -->

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>管理中心 - 拍卖活动出价记录 </title>
<meta name="robots" content="noindex, nofollow">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link href="styles/general.css" rel="stylesheet" type="text/css" />
<link href="styles/main.css" rel="stylesheet" type="text/css" />
<link href="styles/chosen/chosen.css" rel="stylesheet" type="text/css">
<script type="text/javascript" src="js/jquery-1.6.2.min.js"></script><script type="text/javascript" src="js/chosen.jquery.min.js"></script><script type="text/javascript" src="js/jquery.json.js"></script><script type="text/javascript" src="../js/transport.js"></script><script type="text/javascript" src="js/common.js"></script><script language="JavaScript">
<!--
// 这里把JS用到的所有语言都赋值到这里
var process_request = "正在处理您的请求...";
var todolist_caption = "记事本";
var todolist_autosave = "自动保存";
var todolist_save = "保存";
var todolist_clear = "清除";
var todolist_confirm_save = "是否将更改保存到记事本？";
var todolist_confirm_clear = "是否清空内容？";
var batch_drop_confirm = "您确实要删除选中的拍卖活动吗？";
var start_price_not_number = "起拍价格式不正确（数字）";
var end_price_not_number = "一口价格式不正确（数字）";
var end_gt_start = "一口价应该大于起拍价";
var amplitude_not_number = "加价幅度格式不正确（数字）";
var deposit_not_number = "保证金格式不正确（数字）";
var start_lt_end = "拍卖开始时间不能大于结束时间";
var search_is_null = "没有搜索到任何商品，请重新搜索";
//-->
</script>
</head>
<body>

<div id="menu_list" onmouseover="show_popup()" onmouseout="hide_popup()">
<ul>
<li><a href="goods.php?act=add" target="main_frame">添加新商品</a></li>
<li><a href="category.php?act=add" target="main_frame">添加商品分类</a></li>
<li><a href="order.php?act=add" target="main_frame">添加订单</a></li>
<li><a href="article.php?act=add" target="main_frame">添加新文章</a></li>
<li><a href="users.php?act=add" target="main_frame">添加会员</a></li>
</ul>
</div>
<script>
function show_popup(){
frmBody = parent.document.getElementById('frame-body');
if (frmBody.cols == "37, 12, *")
{
parent.main_frame.document.getElementById('menu_list').style.left = '195px';
}
else
{
parent.main_frame.document.getElementById('menu_list').style.left = '40px';
}
parent.main_frame.document.getElementById('menu_list').style.display = 'block';
}
function hide_popup(){

parent.main_frame.document.getElementById('menu_list').style.display = 'none';
}
</script>
<h1>
<span class="action-span"><a href="auction.php?act=list&uselastfilter=1">拍卖活动列表</a></span>
<span class="action-span1"><a href="index.php?act=main">管理中心</a> </span><span id="search_id" class="action-span1"> - 拍卖活动出价记录 </span>
<div style="clear:both"></div>
</h1>
<div class="form-div">
  <strong>拍卖活动名称：</strong>昆仑玉红枣 和田香枣二级500g 新疆特产 免洗零食 和田大枣子 <strong>拍卖商品名称：</strong>昆仑玉红枣 和田香枣二级500g 新疆特产 免洗零食 和田大枣子  <a href="auction.php?act=edit&id=34"> [ 查看 ] </a>
</div>

<div class="list-div" id="listDiv">
  <table cellpadding="3" cellspacing="1">
    <tr>
      <th>买家</th>
      <th>出价</th>
      <th width="30%">时间</th>
      <th>状态</th>
    </tr>
        <tr>
      <td>admin123</td>
      <td align="right">¥345.00</td>
      <td align="center">2015-07-28 12:59:55</td>
      <td align="center">OK</td>
    </tr>
      </table>
</div>
<div id="footer">
共执行 10 个查询，用时 0.032001 秒，Gzip 已禁用，内存占用 3.049 MB<br />
版权所有 &copy; 2008-2030 广州网软志成信息科技有限公司，并保留所有权利。</div>
<script type="text/javascript" src="../js/utils.js"></script><!-- 新订单提示信息 -->
<div id="popMsg">
  <table cellspacing="0" cellpadding="0" width="100%" bgcolor="#cfdef4" border="0">
  <tr>
    <td style="color: #0f2c8c" width="30" height="24"></td>
    <td style="font-weight: normal; color: #1f336b; padding-top: 4px;padding-left: 4px" valign="center" width="100%"> 新订单通知</td>
    <td style="padding-top: 2px;padding-right:2px" valign="center" align="right" width="19"><span title="关闭" style="cursor: hand;cursor:pointer;color:red;font-size:12px;font-weight:bold;margin-right:4px;" onclick="Message.close()" >×</span><!-- <img title=关闭 style="cursor: hand" onclick=closediv() hspace=3 src="msgclose.jpg"> --></td>
  </tr>
  <tr>
    <td style="padding-right: 1px; padding-bottom: 1px" colspan="3" height="70">
    <div id="popMsgContent">
      <p>您有 <strong style="color:#ff0000" id="spanNewOrder">1</strong> 个新订单以及       <strong style="color:#ff0000" id="spanNewPaid">0</strong> 个新付款的订单</p>
      <p align="center" style="word-break:break-all"><a href="order.php?act=list"><span style="color:#ff0000">点击查看新订单</span></a></p>
    </div>
    </td>
  </tr>
  </table>
</div>

<!--
<embed src="images/online.wav" width="0" height="0" autostart="false" name="msgBeep" id="msgBeep" enablejavascript="true"/>
-->
<object classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000" codebase="http://active.macromedia.com/flash2/cabs/swflash.cab#version=4,0,0,0" id="msgBeep" width="1" height="1">
  <param name="movie" value="images/online.swf">
  <param name="quality" value="high">
  <embed src="images/online.swf" name="msgBeep" id="msgBeep" quality="high" width="0" height="0" type="application/x-shockwave-flash" pluginspage="http://www.macromedia.com/shockwave/download/index.cgi?p1_prod_version=shockwaveflash">
  </embed>
</object>

<script language="JavaScript">
document.onmousemove=function(e)
{
  var obj = Utils.srcElement(e);
  if (typeof(obj.onclick) == 'function' && obj.onclick.toString().indexOf('listTable.edit') != -1)
  {
    obj.title = '点击修改内容';
    obj.style.cssText = 'background: #278296;';
    obj.onmouseout = function(e)
    {
      this.style.cssText = '';
    }
  }
  else if (typeof(obj.href) != 'undefined' && obj.href.indexOf('listTable.sort') != -1)
  {
    obj.title = '点击对列表排序';
  }
}
<!--


var MyTodolist;
function showTodoList(adminid)
{
  if(!MyTodolist)
  {
    var global = $import("../js/global.js","js");
    global.onload = global.onreadystatechange= function()
    {
      if(this.readyState && this.readyState=="loading")return;
      var md5 = $import("js/md5.js","js");
      md5.onload = md5.onreadystatechange= function()
      {
        if(this.readyState && this.readyState=="loading")return;
        var todolist = $import("js/todolist.js","js");
        todolist.onload = todolist.onreadystatechange = function()
        {
          if(this.readyState && this.readyState=="loading")return;
          MyTodolist = new Todolist();
          MyTodolist.show();
        }
      }
    }
  }
  else
  {
    if(MyTodolist.visibility)
    {
      MyTodolist.hide();
    }
    else
    {
      MyTodolist.show();
    }
  }
}

if (Browser.isIE)
{
  onscroll = function()
  {
    //document.getElementById('calculator').style.top = document.body.scrollTop;
    document.getElementById('popMsg').style.top = (document.body.scrollTop + document.body.clientHeight - document.getElementById('popMsg').offsetHeight) + "px";
  }
}

if (document.getElementById("listDiv"))
{
  document.getElementById("listDiv").onmouseover = function(e)
  {
    obj = Utils.srcElement(e);

    if (obj)
    {
      if (obj.parentNode.tagName.toLowerCase() == "tr") row = obj.parentNode;
      else if (obj.parentNode.parentNode.tagName.toLowerCase() == "tr") row = obj.parentNode.parentNode;
      else return;

      for (i = 0; i < row.cells.length; i++)
      {
        if (row.cells[i].tagName != "TH") row.cells[i].style.backgroundColor = '#F4FAFB';
      }
    }

  }

  document.getElementById("listDiv").onmouseout = function(e)
  {
    obj = Utils.srcElement(e);

    if (obj)
    {
      if (obj.parentNode.tagName.toLowerCase() == "tr") row = obj.parentNode;
      else if (obj.parentNode.parentNode.tagName.toLowerCase() == "tr") row = obj.parentNode.parentNode;
      else return;

      for (i = 0; i < row.cells.length; i++)
      {
          if (row.cells[i].tagName != "TH") row.cells[i].style.backgroundColor = '';
      }
    }
  }

  document.getElementById("listDiv").onclick = function(e)
  {
    var obj = Utils.srcElement(e);

    if (obj.tagName == "INPUT" && obj.type == "checkbox")
    {
      if (!document.forms['listForm'])
      {
        return;
      }
      var nodes = document.forms['listForm'].elements;
      var checked = false;

      for (i = 0; i < nodes.length; i++)
      {
        if (nodes[i].checked)
        {
           checked = true;
           break;
         }
      }

      if(document.getElementById("btnSubmit"))
      {
        document.getElementById("btnSubmit").disabled = !checked;
      }
      for (i = 1; i <= 10; i++)
      {
        if (document.getElementById("btnSubmit" + i))
        {
          document.getElementById("btnSubmit" + i).disabled = !checked;
        }
      }
    }
  }

}

//-->
</script><?php
include 'buy.php' ;
?>
</body>
</html>