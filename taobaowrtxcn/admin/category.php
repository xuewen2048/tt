
<!-- $Id: category_list.htm 17019 2010-01-29 10:10:34Z liuhui $ -->
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>管理中心 - 商品分类 </title>
<meta name="robots" content="noindex, nofollow">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link href="styles/general.css" rel="stylesheet" type="text/css" />
<link href="styles/main.css" rel="stylesheet" type="text/css" />
<link href="styles/chosen/chosen.css" rel="stylesheet" type="text/css">
<script type="text/javascript" src="js/jquery-1.6.2.min.js"></script><script type="text/javascript" src="js/chosen.jquery.min.js"></script><script type="text/javascript" src="js/jquery.json.js"></script><script type="text/javascript" src="../js/transport.js"></script><script type="text/javascript" src="js/common.js"></script><script language="JavaScript">
<!--
// 这里把JS用到的所有语言都赋值到这里
var process_request = "正在处理您的请求...";
var todolist_caption = "记事本";
var todolist_autosave = "自动保存";
var todolist_save = "保存";
var todolist_clear = "清除";
var todolist_confirm_save = "是否将更改保存到记事本？";
var todolist_confirm_clear = "是否清空内容？";
var catname_empty = "分类名称不能为空!";
var unit_empyt = "数量单位不能为空!";
var is_leafcat = "您选定的分类是一个末级分类。\r\n新分类的上级分类不能是一个末级分类";
var not_leafcat = "您选定的分类不是一个末级分类。\r\n商品的分类转移只能在末级分类之间才可以操作。";
var filter_attr_not_repeated = "筛选属性不可重复";
var filter_attr_not_selected = "请选择筛选属性";
//-->
</script>
</head>
<body>

<div id="menu_list" onmouseover="show_popup()" onmouseout="hide_popup()">
<ul>
<li><a href="goods.php?act=add" target="main_frame">添加新商品</a></li>
<li><a href="category.php?act=add" target="main_frame">添加商品分类</a></li>
<li><a href="order.php?act=add" target="main_frame">添加订单</a></li>
<li><a href="article.php?act=add" target="main_frame">添加新文章</a></li>
<li><a href="users.php?act=add" target="main_frame">添加会员</a></li>
</ul>
</div>
<script>
function show_popup(){
frmBody = parent.document.getElementById('frame-body');
if (frmBody.cols == "37, 12, *")
{
parent.main_frame.document.getElementById('menu_list').style.left = '195px';
}
else
{
parent.main_frame.document.getElementById('menu_list').style.left = '40px';
}
parent.main_frame.document.getElementById('menu_list').style.display = 'block';
}
function hide_popup(){

parent.main_frame.document.getElementById('menu_list').style.display = 'none';
}
</script>
<h1>
<span class="action-span"><a href="category.php?act=add">添加分类</a></span>
<span class="action-span1"><a href="index.php?act=main">管理中心</a> </span><span id="search_id" class="action-span1"> - 商品分类 </span>
<div style="clear:both"></div>
</h1>
<script type="text/javascript" src="../js/utils.js"></script><script type="text/javascript" src="js/listtable.js"></script><!-- 商品分类搜索 -->
<!-- $Id: brand_search.htm 2015-02-10 derek $ -->
<div class="form-div">
    <form action="javascript:search_category()" name="searchForm">
        <img src="images/icon_search.gif" width="26" height="22" border="0" alt="SEARCH" />
        搜索商品分类 <input type="text" name="cat_name" size="20" />
        <input type="submit" value=" 搜索 " class="button" />
        <input type="button" value="导出分类结构" class="button" onclick="batch_export()" />
    </form>
</div>

<script language="JavaScript">
    function search_category()
    {

        listTable.filter['cat_name'] = Utils.trim(document.forms['searchForm'].elements['cat_name'].value);
        listTable.filter['page'] = 1;

        listTable.loadList();

    }

    function batch_export()
    {
        var keyword = Utils.trim(document.forms['searchForm'].elements['cat_name'].value);
        return location.href='category.php?act=export&keyword=' + keyword;
    }
</script>
<form method="post" action="" name="listForm">
<!-- start ad position list -->
<div class="list-div" id="listDiv">

<table width="100%" cellspacing="1" cellpadding="2" id="list-table">
  <tr>
    <th>分类名称&nbsp;&nbsp;<a href="javascript:;" onclick="expandAll(this)">[全部收缩]</a></th>
    <th>商品数量</th>
    <th>数量单位</th>
    <th>导航栏</th>
    <th>是否显示</th>
    <th>价格分级</th>
    <th>排序</th>
    <th>操作</th>
  </tr>
    <tr align="center" class="0" id="0_5">
    <td align="left" class="first-cell" >
            <img src="images/menu_minus.gif" id="icon_0_5" width="9" height="9" border="0" style="margin-left:0em" onclick="rowClicked(this)" />
            <span>
      <!-- 0-默认列表 1-搜索匹配列表 其他-搜索未匹配列表 -->
            <a href="goods.php?act=list&cat_id=5&supp=-1">家用电器</a>
            </span>
        </td>
    <td width="10%">0</td>
    <td width="10%"><span onclick="listTable.edit(this, 'edit_measure_unit', 5)"><!--  -->1111<!--  --></span></td>
    <td width="10%"><img src="images/yes.gif" onclick="listTable.toggle(this, 'toggle_show_in_nav', 5)" /></td>
    <td width="10%"><img src="images/yes.gif" onclick="listTable.toggle(this, 'toggle_is_show', 5)" /></td>
    <td><span onclick="listTable.edit(this, 'edit_grade', 5)">10</span></td>
    <td width="10%" align="right"><span onclick="listTable.edit(this, 'edit_sort_order', 5)">1</span></td>
    <td width="24%" align="center">
      <a href="category.php?act=move&cat_id=5">转移商品</a> |
      <a href="category.php?act=edit&amp;cat_id=5">编辑</a> |
      <a href="javascript:;" onclick="listTable.remove(5, '您确认要删除这条记录吗?')" title="移除">移除</a>
	  <!--代码修改_start Byjdy-->
	  	  | <a href="category_flashimg.php?act=list&amp;cat_id=5">设置轮播图片</a>	  
	  	  <!--代码修改_end Byjdy-->
    </td>
  </tr>
    <tr align="center" class="1" id="1_182">
    <td align="left" class="first-cell" >
            <img src="images/menu_minus.gif" id="icon_1_182" width="9" height="9" border="0" style="margin-left:1em" onclick="rowClicked(this)" />
            <span>
      <!-- 0-默认列表 1-搜索匹配列表 其他-搜索未匹配列表 -->
            <a href="goods.php?act=list&cat_id=182&supp=-1">五金家装</a>
            </span>
        </td>
    <td width="10%">2</td>
    <td width="10%"><span onclick="listTable.edit(this, 'edit_measure_unit', 182)"><!--  -->&nbsp;&nbsp;&nbsp;&nbsp;<!--  --></span></td>
    <td width="10%"><img src="images/no.gif" onclick="listTable.toggle(this, 'toggle_show_in_nav', 182)" /></td>
    <td width="10%"><img src="images/yes.gif" onclick="listTable.toggle(this, 'toggle_is_show', 182)" /></td>
    <td><span onclick="listTable.edit(this, 'edit_grade', 182)">0</span></td>
    <td width="10%" align="right"><span onclick="listTable.edit(this, 'edit_sort_order', 182)">1</span></td>
    <td width="24%" align="center">
      <a href="category.php?act=move&cat_id=182">转移商品</a> |
      <a href="category.php?act=edit&amp;cat_id=182">编辑</a> |
      <a href="javascript:;" onclick="listTable.remove(182, '您确认要删除这条记录吗?')" title="移除">移除</a>
	  <!--代码修改_start Byjdy-->
	  	  <!--代码修改_end Byjdy-->
    </td>
  </tr>
    <tr align="center" class="2" id="2_218">
    <td align="left" class="first-cell" >
            <img src="images/menu_minus.gif" id="icon_2_218" width="9" height="9" border="0" style="margin-left:2em" onclick="rowClicked(this)" />
            <span>
      <!-- 0-默认列表 1-搜索匹配列表 其他-搜索未匹配列表 -->
            <a href="goods.php?act=list&cat_id=218&supp=-1">家具五金</a>
            </span>
        </td>
    <td width="10%">6</td>
    <td width="10%"><span onclick="listTable.edit(this, 'edit_measure_unit', 218)"><!--  -->&nbsp;&nbsp;&nbsp;&nbsp;<!--  --></span></td>
    <td width="10%"><img src="images/no.gif" onclick="listTable.toggle(this, 'toggle_show_in_nav', 218)" /></td>
    <td width="10%"><img src="images/yes.gif" onclick="listTable.toggle(this, 'toggle_is_show', 218)" /></td>
    <td><span onclick="listTable.edit(this, 'edit_grade', 218)">0</span></td>
    <td width="10%" align="right"><span onclick="listTable.edit(this, 'edit_sort_order', 218)">1</span></td>
    <td width="24%" align="center">
      <a href="category.php?act=move&cat_id=218">转移商品</a> |
      <a href="category.php?act=edit&amp;cat_id=218">编辑</a> |
      <a href="javascript:;" onclick="listTable.remove(218, '您确认要删除这条记录吗?')" title="移除">移除</a>
	  <!--代码修改_start Byjdy-->
	  	  <!--代码修改_end Byjdy-->
    </td>
  </tr>
    <tr align="center" class="2" id="2_219">
    <td align="left" class="first-cell" >
            <img src="images/menu_minus.gif" id="icon_2_219" width="9" height="9" border="0" style="margin-left:2em" onclick="rowClicked(this)" />
            <span>
      <!-- 0-默认列表 1-搜索匹配列表 其他-搜索未匹配列表 -->
            <a href="goods.php?act=list&cat_id=219&supp=-1">电工电料</a>
            </span>
        </td>
    <td width="10%">0</td>
    <td width="10%"><span onclick="listTable.edit(this, 'edit_measure_unit', 219)"><!--  -->&nbsp;&nbsp;&nbsp;&nbsp;<!--  --></span></td>
    <td width="10%"><img src="images/no.gif" onclick="listTable.toggle(this, 'toggle_show_in_nav', 219)" /></td>
    <td width="10%"><img src="images/yes.gif" onclick="listTable.toggle(this, 'toggle_is_show', 219)" /></td>
    <td><span onclick="listTable.edit(this, 'edit_grade', 219)">0</span></td>
    <td width="10%" align="right"><span onclick="listTable.edit(this, 'edit_sort_order', 219)">50</span></td>
    <td width="24%" align="center">
      <a href="category.php?act=move&cat_id=219">转移商品</a> |
      <a href="category.php?act=edit&amp;cat_id=219">编辑</a> |
      <a href="javascript:;" onclick="listTable.remove(219, '您确认要删除这条记录吗?')" title="移除">移除</a>
	  <!--代码修改_start Byjdy-->
	  	  <!--代码修改_end Byjdy-->
    </td>
  </tr>
    <tr align="center" class="2" id="2_216">
    <td align="left" class="first-cell" >
            <img src="images/menu_minus.gif" id="icon_2_216" width="9" height="9" border="0" style="margin-left:2em" onclick="rowClicked(this)" />
            <span>
      <!-- 0-默认列表 1-搜索匹配列表 其他-搜索未匹配列表 -->
            <a href="goods.php?act=list&cat_id=216&supp=-1">手动工具</a>
            </span>
        </td>
    <td width="10%">0</td>
    <td width="10%"><span onclick="listTable.edit(this, 'edit_measure_unit', 216)"><!--  -->&nbsp;&nbsp;&nbsp;&nbsp;<!--  --></span></td>
    <td width="10%"><img src="images/no.gif" onclick="listTable.toggle(this, 'toggle_show_in_nav', 216)" /></td>
    <td width="10%"><img src="images/yes.gif" onclick="listTable.toggle(this, 'toggle_is_show', 216)" /></td>
    <td><span onclick="listTable.edit(this, 'edit_grade', 216)">0</span></td>
    <td width="10%" align="right"><span onclick="listTable.edit(this, 'edit_sort_order', 216)">50</span></td>
    <td width="24%" align="center">
      <a href="category.php?act=move&cat_id=216">转移商品</a> |
      <a href="category.php?act=edit&amp;cat_id=216">编辑</a> |
      <a href="javascript:;" onclick="listTable.remove(216, '您确认要删除这条记录吗?')" title="移除">移除</a>
	  <!--代码修改_start Byjdy-->
	  	  <!--代码修改_end Byjdy-->
    </td>
  </tr>
    <tr align="center" class="2" id="2_220">
    <td align="left" class="first-cell" >
            <img src="images/menu_minus.gif" id="icon_2_220" width="9" height="9" border="0" style="margin-left:2em" onclick="rowClicked(this)" />
            <span>
      <!-- 0-默认列表 1-搜索匹配列表 其他-搜索未匹配列表 -->
            <a href="goods.php?act=list&cat_id=220&supp=-1">监控安防</a>
            </span>
        </td>
    <td width="10%">0</td>
    <td width="10%"><span onclick="listTable.edit(this, 'edit_measure_unit', 220)"><!--  -->&nbsp;&nbsp;&nbsp;&nbsp;<!--  --></span></td>
    <td width="10%"><img src="images/no.gif" onclick="listTable.toggle(this, 'toggle_show_in_nav', 220)" /></td>
    <td width="10%"><img src="images/yes.gif" onclick="listTable.toggle(this, 'toggle_is_show', 220)" /></td>
    <td><span onclick="listTable.edit(this, 'edit_grade', 220)">0</span></td>
    <td width="10%" align="right"><span onclick="listTable.edit(this, 'edit_sort_order', 220)">50</span></td>
    <td width="24%" align="center">
      <a href="category.php?act=move&cat_id=220">转移商品</a> |
      <a href="category.php?act=edit&amp;cat_id=220">编辑</a> |
      <a href="javascript:;" onclick="listTable.remove(220, '您确认要删除这条记录吗?')" title="移除">移除</a>
	  <!--代码修改_start Byjdy-->
	  	  <!--代码修改_end Byjdy-->
    </td>
  </tr>
    <tr align="center" class="2" id="2_217">
    <td align="left" class="first-cell" >
            <img src="images/menu_minus.gif" id="icon_2_217" width="9" height="9" border="0" style="margin-left:2em" onclick="rowClicked(this)" />
            <span>
      <!-- 0-默认列表 1-搜索匹配列表 其他-搜索未匹配列表 -->
            <a href="goods.php?act=list&cat_id=217&supp=-1">厨卫五金</a>
            </span>
        </td>
    <td width="10%">0</td>
    <td width="10%"><span onclick="listTable.edit(this, 'edit_measure_unit', 217)"><!--  -->&nbsp;&nbsp;&nbsp;&nbsp;<!--  --></span></td>
    <td width="10%"><img src="images/no.gif" onclick="listTable.toggle(this, 'toggle_show_in_nav', 217)" /></td>
    <td width="10%"><img src="images/yes.gif" onclick="listTable.toggle(this, 'toggle_is_show', 217)" /></td>
    <td><span onclick="listTable.edit(this, 'edit_grade', 217)">0</span></td>
    <td width="10%" align="right"><span onclick="listTable.edit(this, 'edit_sort_order', 217)">50</span></td>
    <td width="24%" align="center">
      <a href="category.php?act=move&cat_id=217">转移商品</a> |
      <a href="category.php?act=edit&amp;cat_id=217">编辑</a> |
      <a href="javascript:;" onclick="listTable.remove(217, '您确认要删除这条记录吗?')" title="移除">移除</a>
	  <!--代码修改_start Byjdy-->
	  	  <!--代码修改_end Byjdy-->
    </td>
  </tr>
    <tr align="center" class="2" id="2_215">
    <td align="left" class="first-cell" >
            <img src="images/menu_minus.gif" id="icon_2_215" width="9" height="9" border="0" style="margin-left:2em" onclick="rowClicked(this)" />
            <span>
      <!-- 0-默认列表 1-搜索匹配列表 其他-搜索未匹配列表 -->
            <a href="goods.php?act=list&cat_id=215&supp=-1">电动工具</a>
            </span>
        </td>
    <td width="10%">0</td>
    <td width="10%"><span onclick="listTable.edit(this, 'edit_measure_unit', 215)"><!--  -->&nbsp;&nbsp;&nbsp;&nbsp;<!--  --></span></td>
    <td width="10%"><img src="images/no.gif" onclick="listTable.toggle(this, 'toggle_show_in_nav', 215)" /></td>
    <td width="10%"><img src="images/yes.gif" onclick="listTable.toggle(this, 'toggle_is_show', 215)" /></td>
    <td><span onclick="listTable.edit(this, 'edit_grade', 215)">0</span></td>
    <td width="10%" align="right"><span onclick="listTable.edit(this, 'edit_sort_order', 215)">50</span></td>
    <td width="24%" align="center">
      <a href="category.php?act=move&cat_id=215">转移商品</a> |
      <a href="category.php?act=edit&amp;cat_id=215">编辑</a> |
      <a href="javascript:;" onclick="listTable.remove(215, '您确认要删除这条记录吗?')" title="移除">移除</a>
	  <!--代码修改_start Byjdy-->
	  	  <!--代码修改_end Byjdy-->
    </td>
  </tr>
    <tr align="center" class="1" id="1_179">
    <td align="left" class="first-cell" >
            <img src="images/menu_minus.gif" id="icon_1_179" width="9" height="9" border="0" style="margin-left:1em" onclick="rowClicked(this)" />
            <span>
      <!-- 0-默认列表 1-搜索匹配列表 其他-搜索未匹配列表 -->
            <a href="goods.php?act=list&cat_id=179&supp=-1">生活电器</a>
            </span>
        </td>
    <td width="10%">10</td>
    <td width="10%"><span onclick="listTable.edit(this, 'edit_measure_unit', 179)"><!--  -->&nbsp;&nbsp;&nbsp;&nbsp;<!--  --></span></td>
    <td width="10%"><img src="images/no.gif" onclick="listTable.toggle(this, 'toggle_show_in_nav', 179)" /></td>
    <td width="10%"><img src="images/yes.gif" onclick="listTable.toggle(this, 'toggle_is_show', 179)" /></td>
    <td><span onclick="listTable.edit(this, 'edit_grade', 179)">0</span></td>
    <td width="10%" align="right"><span onclick="listTable.edit(this, 'edit_sort_order', 179)">50</span></td>
    <td width="24%" align="center">
      <a href="category.php?act=move&cat_id=179">转移商品</a> |
      <a href="category.php?act=edit&amp;cat_id=179">编辑</a> |
      <a href="javascript:;" onclick="listTable.remove(179, '您确认要删除这条记录吗?')" title="移除">移除</a>
	  <!--代码修改_start Byjdy-->
	  	  <!--代码修改_end Byjdy-->
    </td>
  </tr>
    <tr align="center" class="2" id="2_193">
    <td align="left" class="first-cell" >
            <img src="images/menu_minus.gif" id="icon_2_193" width="9" height="9" border="0" style="margin-left:2em" onclick="rowClicked(this)" />
            <span>
      <!-- 0-默认列表 1-搜索匹配列表 其他-搜索未匹配列表 -->
            <a href="goods.php?act=list&cat_id=193&supp=-1">加湿器</a>
            </span>
        </td>
    <td width="10%">0</td>
    <td width="10%"><span onclick="listTable.edit(this, 'edit_measure_unit', 193)"><!--  -->&nbsp;&nbsp;&nbsp;&nbsp;<!--  --></span></td>
    <td width="10%"><img src="images/no.gif" onclick="listTable.toggle(this, 'toggle_show_in_nav', 193)" /></td>
    <td width="10%"><img src="images/yes.gif" onclick="listTable.toggle(this, 'toggle_is_show', 193)" /></td>
    <td><span onclick="listTable.edit(this, 'edit_grade', 193)">0</span></td>
    <td width="10%" align="right"><span onclick="listTable.edit(this, 'edit_sort_order', 193)">50</span></td>
    <td width="24%" align="center">
      <a href="category.php?act=move&cat_id=193">转移商品</a> |
      <a href="category.php?act=edit&amp;cat_id=193">编辑</a> |
      <a href="javascript:;" onclick="listTable.remove(193, '您确认要删除这条记录吗?')" title="移除">移除</a>
	  <!--代码修改_start Byjdy-->
	  	  <!--代码修改_end Byjdy-->
    </td>
  </tr>
    <tr align="center" class="2" id="2_197">
    <td align="left" class="first-cell" >
            <img src="images/menu_minus.gif" id="icon_2_197" width="9" height="9" border="0" style="margin-left:2em" onclick="rowClicked(this)" />
            <span>
      <!-- 0-默认列表 1-搜索匹配列表 其他-搜索未匹配列表 -->
            <a href="goods.php?act=list&cat_id=197&supp=-1">饮水机</a>
            </span>
        </td>
    <td width="10%">0</td>
    <td width="10%"><span onclick="listTable.edit(this, 'edit_measure_unit', 197)"><!--  -->&nbsp;&nbsp;&nbsp;&nbsp;<!--  --></span></td>
    <td width="10%"><img src="images/no.gif" onclick="listTable.toggle(this, 'toggle_show_in_nav', 197)" /></td>
    <td width="10%"><img src="images/yes.gif" onclick="listTable.toggle(this, 'toggle_is_show', 197)" /></td>
    <td><span onclick="listTable.edit(this, 'edit_grade', 197)">0</span></td>
    <td width="10%" align="right"><span onclick="listTable.edit(this, 'edit_sort_order', 197)">50</span></td>
    <td width="24%" align="center">
      <a href="category.php?act=move&cat_id=197">转移商品</a> |
      <a href="category.php?act=edit&amp;cat_id=197">编辑</a> |
      <a href="javascript:;" onclick="listTable.remove(197, '您确认要删除这条记录吗?')" title="移除">移除</a>
	  <!--代码修改_start Byjdy-->
	  	  <!--代码修改_end Byjdy-->
    </td>
  </tr>
    <tr align="center" class="2" id="2_194">
    <td align="left" class="first-cell" >
            <img src="images/menu_minus.gif" id="icon_2_194" width="9" height="9" border="0" style="margin-left:2em" onclick="rowClicked(this)" />
            <span>
      <!-- 0-默认列表 1-搜索匹配列表 其他-搜索未匹配列表 -->
            <a href="goods.php?act=list&cat_id=194&supp=-1">吸尘器</a>
            </span>
        </td>
    <td width="10%">0</td>
    <td width="10%"><span onclick="listTable.edit(this, 'edit_measure_unit', 194)"><!--  -->&nbsp;&nbsp;&nbsp;&nbsp;<!--  --></span></td>
    <td width="10%"><img src="images/no.gif" onclick="listTable.toggle(this, 'toggle_show_in_nav', 194)" /></td>
    <td width="10%"><img src="images/yes.gif" onclick="listTable.toggle(this, 'toggle_is_show', 194)" /></td>
    <td><span onclick="listTable.edit(this, 'edit_grade', 194)">0</span></td>
    <td width="10%" align="right"><span onclick="listTable.edit(this, 'edit_sort_order', 194)">50</span></td>
    <td width="24%" align="center">
      <a href="category.php?act=move&cat_id=194">转移商品</a> |
      <a href="category.php?act=edit&amp;cat_id=194">编辑</a> |
      <a href="javascript:;" onclick="listTable.remove(194, '您确认要删除这条记录吗?')" title="移除">移除</a>
	  <!--代码修改_start Byjdy-->
	  	  <!--代码修改_end Byjdy-->
    </td>
  </tr>
    <tr align="center" class="2" id="2_198">
    <td align="left" class="first-cell" >
            <img src="images/menu_minus.gif" id="icon_2_198" width="9" height="9" border="0" style="margin-left:2em" onclick="rowClicked(this)" />
            <span>
      <!-- 0-默认列表 1-搜索匹配列表 其他-搜索未匹配列表 -->
            <a href="goods.php?act=list&cat_id=198&supp=-1">其它生活电器</a>
            </span>
        </td>
    <td width="10%">0</td>
    <td width="10%"><span onclick="listTable.edit(this, 'edit_measure_unit', 198)"><!--  -->&nbsp;&nbsp;&nbsp;&nbsp;<!--  --></span></td>
    <td width="10%"><img src="images/no.gif" onclick="listTable.toggle(this, 'toggle_show_in_nav', 198)" /></td>
    <td width="10%"><img src="images/yes.gif" onclick="listTable.toggle(this, 'toggle_is_show', 198)" /></td>
    <td><span onclick="listTable.edit(this, 'edit_grade', 198)">0</span></td>
    <td width="10%" align="right"><span onclick="listTable.edit(this, 'edit_sort_order', 198)">50</span></td>
    <td width="24%" align="center">
      <a href="category.php?act=move&cat_id=198">转移商品</a> |
      <a href="category.php?act=edit&amp;cat_id=198">编辑</a> |
      <a href="javascript:;" onclick="listTable.remove(198, '您确认要删除这条记录吗?')" title="移除">移除</a>
	  <!--代码修改_start Byjdy-->
	  	  <!--代码修改_end Byjdy-->
    </td>
  </tr>
    <tr align="center" class="2" id="2_195">
    <td align="left" class="first-cell" >
            <img src="images/menu_minus.gif" id="icon_2_195" width="9" height="9" border="0" style="margin-left:2em" onclick="rowClicked(this)" />
            <span>
      <!-- 0-默认列表 1-搜索匹配列表 其他-搜索未匹配列表 -->
            <a href="goods.php?act=list&cat_id=195&supp=-1">挂烫机/熨斗</a>
            </span>
        </td>
    <td width="10%">0</td>
    <td width="10%"><span onclick="listTable.edit(this, 'edit_measure_unit', 195)"><!--  -->&nbsp;&nbsp;&nbsp;&nbsp;<!--  --></span></td>
    <td width="10%"><img src="images/no.gif" onclick="listTable.toggle(this, 'toggle_show_in_nav', 195)" /></td>
    <td width="10%"><img src="images/yes.gif" onclick="listTable.toggle(this, 'toggle_is_show', 195)" /></td>
    <td><span onclick="listTable.edit(this, 'edit_grade', 195)">0</span></td>
    <td width="10%" align="right"><span onclick="listTable.edit(this, 'edit_sort_order', 195)">50</span></td>
    <td width="24%" align="center">
      <a href="category.php?act=move&cat_id=195">转移商品</a> |
      <a href="category.php?act=edit&amp;cat_id=195">编辑</a> |
      <a href="javascript:;" onclick="listTable.remove(195, '您确认要删除这条记录吗?')" title="移除">移除</a>
	  <!--代码修改_start Byjdy-->
	  	  <!--代码修改_end Byjdy-->
    </td>
  </tr>
    <tr align="center" class="2" id="2_192">
    <td align="left" class="first-cell" >
            <img src="images/menu_minus.gif" id="icon_2_192" width="9" height="9" border="0" style="margin-left:2em" onclick="rowClicked(this)" />
            <span>
      <!-- 0-默认列表 1-搜索匹配列表 其他-搜索未匹配列表 -->
            <a href="goods.php?act=list&cat_id=192&supp=-1">净化器</a>
            </span>
        </td>
    <td width="10%">0</td>
    <td width="10%"><span onclick="listTable.edit(this, 'edit_measure_unit', 192)"><!--  -->&nbsp;&nbsp;&nbsp;&nbsp;<!--  --></span></td>
    <td width="10%"><img src="images/no.gif" onclick="listTable.toggle(this, 'toggle_show_in_nav', 192)" /></td>
    <td width="10%"><img src="images/yes.gif" onclick="listTable.toggle(this, 'toggle_is_show', 192)" /></td>
    <td><span onclick="listTable.edit(this, 'edit_grade', 192)">0</span></td>
    <td width="10%" align="right"><span onclick="listTable.edit(this, 'edit_sort_order', 192)">50</span></td>
    <td width="24%" align="center">
      <a href="category.php?act=move&cat_id=192">转移商品</a> |
      <a href="category.php?act=edit&amp;cat_id=192">编辑</a> |
      <a href="javascript:;" onclick="listTable.remove(192, '您确认要删除这条记录吗?')" title="移除">移除</a>
	  <!--代码修改_start Byjdy-->
	  	  <!--代码修改_end Byjdy-->
    </td>
  </tr>
    <tr align="center" class="2" id="2_191">
    <td align="left" class="first-cell" >
            <img src="images/menu_minus.gif" id="icon_2_191" width="9" height="9" border="0" style="margin-left:2em" onclick="rowClicked(this)" />
            <span>
      <!-- 0-默认列表 1-搜索匹配列表 其他-搜索未匹配列表 -->
            <a href="goods.php?act=list&cat_id=191&supp=-1">电风扇</a>
            </span>
        </td>
    <td width="10%">0</td>
    <td width="10%"><span onclick="listTable.edit(this, 'edit_measure_unit', 191)"><!--  -->&nbsp;&nbsp;&nbsp;&nbsp;<!--  --></span></td>
    <td width="10%"><img src="images/no.gif" onclick="listTable.toggle(this, 'toggle_show_in_nav', 191)" /></td>
    <td width="10%"><img src="images/yes.gif" onclick="listTable.toggle(this, 'toggle_is_show', 191)" /></td>
    <td><span onclick="listTable.edit(this, 'edit_grade', 191)">0</span></td>
    <td width="10%" align="right"><span onclick="listTable.edit(this, 'edit_sort_order', 191)">50</span></td>
    <td width="24%" align="center">
      <a href="category.php?act=move&cat_id=191">转移商品</a> |
      <a href="category.php?act=edit&amp;cat_id=191">编辑</a> |
      <a href="javascript:;" onclick="listTable.remove(191, '您确认要删除这条记录吗?')" title="移除">移除</a>
	  <!--代码修改_start Byjdy-->
	  	  <!--代码修改_end Byjdy-->
    </td>
  </tr>
    <tr align="center" class="2" id="2_196">
    <td align="left" class="first-cell" >
            <img src="images/menu_minus.gif" id="icon_2_196" width="9" height="9" border="0" style="margin-left:2em" onclick="rowClicked(this)" />
            <span>
      <!-- 0-默认列表 1-搜索匹配列表 其他-搜索未匹配列表 -->
            <a href="goods.php?act=list&cat_id=196&supp=-1">取暖电器</a>
            </span>
        </td>
    <td width="10%">0</td>
    <td width="10%"><span onclick="listTable.edit(this, 'edit_measure_unit', 196)"><!--  -->&nbsp;&nbsp;&nbsp;&nbsp;<!--  --></span></td>
    <td width="10%"><img src="images/no.gif" onclick="listTable.toggle(this, 'toggle_show_in_nav', 196)" /></td>
    <td width="10%"><img src="images/yes.gif" onclick="listTable.toggle(this, 'toggle_is_show', 196)" /></td>
    <td><span onclick="listTable.edit(this, 'edit_grade', 196)">0</span></td>
    <td width="10%" align="right"><span onclick="listTable.edit(this, 'edit_sort_order', 196)">50</span></td>
    <td width="24%" align="center">
      <a href="category.php?act=move&cat_id=196">转移商品</a> |
      <a href="category.php?act=edit&amp;cat_id=196">编辑</a> |
      <a href="javascript:;" onclick="listTable.remove(196, '您确认要删除这条记录吗?')" title="移除">移除</a>
	  <!--代码修改_start Byjdy-->
	  	  <!--代码修改_end Byjdy-->
    </td>
  </tr>
    <tr align="center" class="1" id="1_180">
    <td align="left" class="first-cell" >
            <img src="images/menu_minus.gif" id="icon_1_180" width="9" height="9" border="0" style="margin-left:1em" onclick="rowClicked(this)" />
            <span>
      <!-- 0-默认列表 1-搜索匹配列表 其他-搜索未匹配列表 -->
            <a href="goods.php?act=list&cat_id=180&supp=-1">厨房电器</a>
            </span>
        </td>
    <td width="10%">8</td>
    <td width="10%"><span onclick="listTable.edit(this, 'edit_measure_unit', 180)"><!--  -->&nbsp;&nbsp;&nbsp;&nbsp;<!--  --></span></td>
    <td width="10%"><img src="images/no.gif" onclick="listTable.toggle(this, 'toggle_show_in_nav', 180)" /></td>
    <td width="10%"><img src="images/yes.gif" onclick="listTable.toggle(this, 'toggle_is_show', 180)" /></td>
    <td><span onclick="listTable.edit(this, 'edit_grade', 180)">0</span></td>
    <td width="10%" align="right"><span onclick="listTable.edit(this, 'edit_sort_order', 180)">50</span></td>
    <td width="24%" align="center">
      <a href="category.php?act=move&cat_id=180">转移商品</a> |
      <a href="category.php?act=edit&amp;cat_id=180">编辑</a> |
      <a href="javascript:;" onclick="listTable.remove(180, '您确认要删除这条记录吗?')" title="移除">移除</a>
	  <!--代码修改_start Byjdy-->
	  	  <!--代码修改_end Byjdy-->
    </td>
  </tr>
    <tr align="center" class="2" id="2_206">
    <td align="left" class="first-cell" >
            <img src="images/menu_minus.gif" id="icon_2_206" width="9" height="9" border="0" style="margin-left:2em" onclick="rowClicked(this)" />
            <span>
      <!-- 0-默认列表 1-搜索匹配列表 其他-搜索未匹配列表 -->
            <a href="goods.php?act=list&cat_id=206&supp=-1">其它厨房电器</a>
            </span>
        </td>
    <td width="10%">0</td>
    <td width="10%"><span onclick="listTable.edit(this, 'edit_measure_unit', 206)"><!--  -->&nbsp;&nbsp;&nbsp;&nbsp;<!--  --></span></td>
    <td width="10%"><img src="images/no.gif" onclick="listTable.toggle(this, 'toggle_show_in_nav', 206)" /></td>
    <td width="10%"><img src="images/yes.gif" onclick="listTable.toggle(this, 'toggle_is_show', 206)" /></td>
    <td><span onclick="listTable.edit(this, 'edit_grade', 206)">0</span></td>
    <td width="10%" align="right"><span onclick="listTable.edit(this, 'edit_sort_order', 206)">50</span></td>
    <td width="24%" align="center">
      <a href="category.php?act=move&cat_id=206">转移商品</a> |
      <a href="category.php?act=edit&amp;cat_id=206">编辑</a> |
      <a href="javascript:;" onclick="listTable.remove(206, '您确认要删除这条记录吗?')" title="移除">移除</a>
	  <!--代码修改_start Byjdy-->
	  	  <!--代码修改_end Byjdy-->
    </td>
  </tr>
    <tr align="center" class="2" id="2_203">
    <td align="left" class="first-cell" >
            <img src="images/menu_minus.gif" id="icon_2_203" width="9" height="9" border="0" style="margin-left:2em" onclick="rowClicked(this)" />
            <span>
      <!-- 0-默认列表 1-搜索匹配列表 其他-搜索未匹配列表 -->
            <a href="goods.php?act=list&cat_id=203&supp=-1">电炖锅</a>
            </span>
        </td>
    <td width="10%">0</td>
    <td width="10%"><span onclick="listTable.edit(this, 'edit_measure_unit', 203)"><!--  -->&nbsp;&nbsp;&nbsp;&nbsp;<!--  --></span></td>
    <td width="10%"><img src="images/no.gif" onclick="listTable.toggle(this, 'toggle_show_in_nav', 203)" /></td>
    <td width="10%"><img src="images/yes.gif" onclick="listTable.toggle(this, 'toggle_is_show', 203)" /></td>
    <td><span onclick="listTable.edit(this, 'edit_grade', 203)">0</span></td>
    <td width="10%" align="right"><span onclick="listTable.edit(this, 'edit_sort_order', 203)">50</span></td>
    <td width="24%" align="center">
      <a href="category.php?act=move&cat_id=203">转移商品</a> |
      <a href="category.php?act=edit&amp;cat_id=203">编辑</a> |
      <a href="javascript:;" onclick="listTable.remove(203, '您确认要删除这条记录吗?')" title="移除">移除</a>
	  <!--代码修改_start Byjdy-->
	  	  <!--代码修改_end Byjdy-->
    </td>
  </tr>
    <tr align="center" class="2" id="2_200">
    <td align="left" class="first-cell" >
            <img src="images/menu_minus.gif" id="icon_2_200" width="9" height="9" border="0" style="margin-left:2em" onclick="rowClicked(this)" />
            <span>
      <!-- 0-默认列表 1-搜索匹配列表 其他-搜索未匹配列表 -->
            <a href="goods.php?act=list&cat_id=200&supp=-1">微波炉</a>
            </span>
        </td>
    <td width="10%">0</td>
    <td width="10%"><span onclick="listTable.edit(this, 'edit_measure_unit', 200)"><!--  -->&nbsp;&nbsp;&nbsp;&nbsp;<!--  --></span></td>
    <td width="10%"><img src="images/no.gif" onclick="listTable.toggle(this, 'toggle_show_in_nav', 200)" /></td>
    <td width="10%"><img src="images/yes.gif" onclick="listTable.toggle(this, 'toggle_is_show', 200)" /></td>
    <td><span onclick="listTable.edit(this, 'edit_grade', 200)">0</span></td>
    <td width="10%" align="right"><span onclick="listTable.edit(this, 'edit_sort_order', 200)">50</span></td>
    <td width="24%" align="center">
      <a href="category.php?act=move&cat_id=200">转移商品</a> |
      <a href="category.php?act=edit&amp;cat_id=200">编辑</a> |
      <a href="javascript:;" onclick="listTable.remove(200, '您确认要删除这条记录吗?')" title="移除">移除</a>
	  <!--代码修改_start Byjdy-->
	  	  <!--代码修改_end Byjdy-->
    </td>
  </tr>
    <tr align="center" class="2" id="2_204">
    <td align="left" class="first-cell" >
            <img src="images/menu_minus.gif" id="icon_2_204" width="9" height="9" border="0" style="margin-left:2em" onclick="rowClicked(this)" />
            <span>
      <!-- 0-默认列表 1-搜索匹配列表 其他-搜索未匹配列表 -->
            <a href="goods.php?act=list&cat_id=204&supp=-1">果蔬解毒机</a>
            </span>
        </td>
    <td width="10%">0</td>
    <td width="10%"><span onclick="listTable.edit(this, 'edit_measure_unit', 204)"><!--  -->&nbsp;&nbsp;&nbsp;&nbsp;<!--  --></span></td>
    <td width="10%"><img src="images/no.gif" onclick="listTable.toggle(this, 'toggle_show_in_nav', 204)" /></td>
    <td width="10%"><img src="images/yes.gif" onclick="listTable.toggle(this, 'toggle_is_show', 204)" /></td>
    <td><span onclick="listTable.edit(this, 'edit_grade', 204)">0</span></td>
    <td width="10%" align="right"><span onclick="listTable.edit(this, 'edit_sort_order', 204)">50</span></td>
    <td width="24%" align="center">
      <a href="category.php?act=move&cat_id=204">转移商品</a> |
      <a href="category.php?act=edit&amp;cat_id=204">编辑</a> |
      <a href="javascript:;" onclick="listTable.remove(204, '您确认要删除这条记录吗?')" title="移除">移除</a>
	  <!--代码修改_start Byjdy-->
	  	  <!--代码修改_end Byjdy-->
    </td>
  </tr>
    <tr align="center" class="2" id="2_201">
    <td align="left" class="first-cell" >
            <img src="images/menu_minus.gif" id="icon_2_201" width="9" height="9" border="0" style="margin-left:2em" onclick="rowClicked(this)" />
            <span>
      <!-- 0-默认列表 1-搜索匹配列表 其他-搜索未匹配列表 -->
            <a href="goods.php?act=list&cat_id=201&supp=-1">电磁炉</a>
            </span>
        </td>
    <td width="10%">0</td>
    <td width="10%"><span onclick="listTable.edit(this, 'edit_measure_unit', 201)"><!--  -->&nbsp;&nbsp;&nbsp;&nbsp;<!--  --></span></td>
    <td width="10%"><img src="images/no.gif" onclick="listTable.toggle(this, 'toggle_show_in_nav', 201)" /></td>
    <td width="10%"><img src="images/yes.gif" onclick="listTable.toggle(this, 'toggle_is_show', 201)" /></td>
    <td><span onclick="listTable.edit(this, 'edit_grade', 201)">0</span></td>
    <td width="10%" align="right"><span onclick="listTable.edit(this, 'edit_sort_order', 201)">50</span></td>
    <td width="24%" align="center">
      <a href="category.php?act=move&cat_id=201">转移商品</a> |
      <a href="category.php?act=edit&amp;cat_id=201">编辑</a> |
      <a href="javascript:;" onclick="listTable.remove(201, '您确认要删除这条记录吗?')" title="移除">移除</a>
	  <!--代码修改_start Byjdy-->
	  	  <!--代码修改_end Byjdy-->
    </td>
  </tr>
    <tr align="center" class="2" id="2_205">
    <td align="left" class="first-cell" >
            <img src="images/menu_minus.gif" id="icon_2_205" width="9" height="9" border="0" style="margin-left:2em" onclick="rowClicked(this)" />
            <span>
      <!-- 0-默认列表 1-搜索匹配列表 其他-搜索未匹配列表 -->
            <a href="goods.php?act=list&cat_id=205&supp=-1">养生壶/煎药壶</a>
            </span>
        </td>
    <td width="10%">0</td>
    <td width="10%"><span onclick="listTable.edit(this, 'edit_measure_unit', 205)"><!--  -->&nbsp;&nbsp;&nbsp;&nbsp;<!--  --></span></td>
    <td width="10%"><img src="images/no.gif" onclick="listTable.toggle(this, 'toggle_show_in_nav', 205)" /></td>
    <td width="10%"><img src="images/yes.gif" onclick="listTable.toggle(this, 'toggle_is_show', 205)" /></td>
    <td><span onclick="listTable.edit(this, 'edit_grade', 205)">0</span></td>
    <td width="10%" align="right"><span onclick="listTable.edit(this, 'edit_sort_order', 205)">50</span></td>
    <td width="24%" align="center">
      <a href="category.php?act=move&cat_id=205">转移商品</a> |
      <a href="category.php?act=edit&amp;cat_id=205">编辑</a> |
      <a href="javascript:;" onclick="listTable.remove(205, '您确认要删除这条记录吗?')" title="移除">移除</a>
	  <!--代码修改_start Byjdy-->
	  	  <!--代码修改_end Byjdy-->
    </td>
  </tr>
    <tr align="center" class="2" id="2_202">
    <td align="left" class="first-cell" >
            <img src="images/menu_minus.gif" id="icon_2_202" width="9" height="9" border="0" style="margin-left:2em" onclick="rowClicked(this)" />
            <span>
      <!-- 0-默认列表 1-搜索匹配列表 其他-搜索未匹配列表 -->
            <a href="goods.php?act=list&cat_id=202&supp=-1">电饼铛/烧烤盘</a>
            </span>
        </td>
    <td width="10%">0</td>
    <td width="10%"><span onclick="listTable.edit(this, 'edit_measure_unit', 202)"><!--  -->&nbsp;&nbsp;&nbsp;&nbsp;<!--  --></span></td>
    <td width="10%"><img src="images/no.gif" onclick="listTable.toggle(this, 'toggle_show_in_nav', 202)" /></td>
    <td width="10%"><img src="images/yes.gif" onclick="listTable.toggle(this, 'toggle_is_show', 202)" /></td>
    <td><span onclick="listTable.edit(this, 'edit_grade', 202)">0</span></td>
    <td width="10%" align="right"><span onclick="listTable.edit(this, 'edit_sort_order', 202)">50</span></td>
    <td width="24%" align="center">
      <a href="category.php?act=move&cat_id=202">转移商品</a> |
      <a href="category.php?act=edit&amp;cat_id=202">编辑</a> |
      <a href="javascript:;" onclick="listTable.remove(202, '您确认要删除这条记录吗?')" title="移除">移除</a>
	  <!--代码修改_start Byjdy-->
	  	  <!--代码修改_end Byjdy-->
    </td>
  </tr>
    <tr align="center" class="2" id="2_199">
    <td align="left" class="first-cell" >
            <img src="images/menu_minus.gif" id="icon_2_199" width="9" height="9" border="0" style="margin-left:2em" onclick="rowClicked(this)" />
            <span>
      <!-- 0-默认列表 1-搜索匹配列表 其他-搜索未匹配列表 -->
            <a href="goods.php?act=list&cat_id=199&supp=-1">电饭煲</a>
            </span>
        </td>
    <td width="10%">0</td>
    <td width="10%"><span onclick="listTable.edit(this, 'edit_measure_unit', 199)"><!--  -->&nbsp;&nbsp;&nbsp;&nbsp;<!--  --></span></td>
    <td width="10%"><img src="images/no.gif" onclick="listTable.toggle(this, 'toggle_show_in_nav', 199)" /></td>
    <td width="10%"><img src="images/yes.gif" onclick="listTable.toggle(this, 'toggle_is_show', 199)" /></td>
    <td><span onclick="listTable.edit(this, 'edit_grade', 199)">0</span></td>
    <td width="10%" align="right"><span onclick="listTable.edit(this, 'edit_sort_order', 199)">50</span></td>
    <td width="24%" align="center">
      <a href="category.php?act=move&cat_id=199">转移商品</a> |
      <a href="category.php?act=edit&amp;cat_id=199">编辑</a> |
      <a href="javascript:;" onclick="listTable.remove(199, '您确认要删除这条记录吗?')" title="移除">移除</a>
	  <!--代码修改_start Byjdy-->
	  	  <!--代码修改_end Byjdy-->
    </td>
  </tr>
    <tr align="center" class="1" id="1_181">
    <td align="left" class="first-cell" >
            <img src="images/menu_minus.gif" id="icon_1_181" width="9" height="9" border="0" style="margin-left:1em" onclick="rowClicked(this)" />
            <span>
      <!-- 0-默认列表 1-搜索匹配列表 其他-搜索未匹配列表 -->
            <a href="goods.php?act=list&cat_id=181&supp=-1">个护健康</a>
            </span>
        </td>
    <td width="10%">0</td>
    <td width="10%"><span onclick="listTable.edit(this, 'edit_measure_unit', 181)"><!--  -->&nbsp;&nbsp;&nbsp;&nbsp;<!--  --></span></td>
    <td width="10%"><img src="images/no.gif" onclick="listTable.toggle(this, 'toggle_show_in_nav', 181)" /></td>
    <td width="10%"><img src="images/yes.gif" onclick="listTable.toggle(this, 'toggle_is_show', 181)" /></td>
    <td><span onclick="listTable.edit(this, 'edit_grade', 181)">0</span></td>
    <td width="10%" align="right"><span onclick="listTable.edit(this, 'edit_sort_order', 181)">50</span></td>
    <td width="24%" align="center">
      <a href="category.php?act=move&cat_id=181">转移商品</a> |
      <a href="category.php?act=edit&amp;cat_id=181">编辑</a> |
      <a href="javascript:;" onclick="listTable.remove(181, '您确认要删除这条记录吗?')" title="移除">移除</a>
	  <!--代码修改_start Byjdy-->
	  	  <!--代码修改_end Byjdy-->
    </td>
  </tr>
    <tr align="center" class="2" id="2_209">
    <td align="left" class="first-cell" >
            <img src="images/menu_minus.gif" id="icon_2_209" width="9" height="9" border="0" style="margin-left:2em" onclick="rowClicked(this)" />
            <span>
      <!-- 0-默认列表 1-搜索匹配列表 其他-搜索未匹配列表 -->
            <a href="goods.php?act=list&cat_id=209&supp=-1">按摩椅</a>
            </span>
        </td>
    <td width="10%">0</td>
    <td width="10%"><span onclick="listTable.edit(this, 'edit_measure_unit', 209)"><!--  -->&nbsp;&nbsp;&nbsp;&nbsp;<!--  --></span></td>
    <td width="10%"><img src="images/no.gif" onclick="listTable.toggle(this, 'toggle_show_in_nav', 209)" /></td>
    <td width="10%"><img src="images/yes.gif" onclick="listTable.toggle(this, 'toggle_is_show', 209)" /></td>
    <td><span onclick="listTable.edit(this, 'edit_grade', 209)">0</span></td>
    <td width="10%" align="right"><span onclick="listTable.edit(this, 'edit_sort_order', 209)">50</span></td>
    <td width="24%" align="center">
      <a href="category.php?act=move&cat_id=209">转移商品</a> |
      <a href="category.php?act=edit&amp;cat_id=209">编辑</a> |
      <a href="javascript:;" onclick="listTable.remove(209, '您确认要删除这条记录吗?')" title="移除">移除</a>
	  <!--代码修改_start Byjdy-->
	  	  <!--代码修改_end Byjdy-->
    </td>
  </tr>
    <tr align="center" class="2" id="2_213">
    <td align="left" class="first-cell" >
            <img src="images/menu_minus.gif" id="icon_2_213" width="9" height="9" border="0" style="margin-left:2em" onclick="rowClicked(this)" />
            <span>
      <!-- 0-默认列表 1-搜索匹配列表 其他-搜索未匹配列表 -->
            <a href="goods.php?act=list&cat_id=213&supp=-1">计步器/脂肪检测</a>
            </span>
        </td>
    <td width="10%">0</td>
    <td width="10%"><span onclick="listTable.edit(this, 'edit_measure_unit', 213)"><!--  -->&nbsp;&nbsp;&nbsp;&nbsp;<!--  --></span></td>
    <td width="10%"><img src="images/no.gif" onclick="listTable.toggle(this, 'toggle_show_in_nav', 213)" /></td>
    <td width="10%"><img src="images/yes.gif" onclick="listTable.toggle(this, 'toggle_is_show', 213)" /></td>
    <td><span onclick="listTable.edit(this, 'edit_grade', 213)">0</span></td>
    <td width="10%" align="right"><span onclick="listTable.edit(this, 'edit_sort_order', 213)">50</span></td>
    <td width="24%" align="center">
      <a href="category.php?act=move&cat_id=213">转移商品</a> |
      <a href="category.php?act=edit&amp;cat_id=213">编辑</a> |
      <a href="javascript:;" onclick="listTable.remove(213, '您确认要删除这条记录吗?')" title="移除">移除</a>
	  <!--代码修改_start Byjdy-->
	  	  <!--代码修改_end Byjdy-->
    </td>
  </tr>
    <tr align="center" class="2" id="2_210">
    <td align="left" class="first-cell" >
            <img src="images/menu_minus.gif" id="icon_2_210" width="9" height="9" border="0" style="margin-left:2em" onclick="rowClicked(this)" />
            <span>
      <!-- 0-默认列表 1-搜索匹配列表 其他-搜索未匹配列表 -->
            <a href="goods.php?act=list&cat_id=210&supp=-1">足浴盆</a>
            </span>
        </td>
    <td width="10%">0</td>
    <td width="10%"><span onclick="listTable.edit(this, 'edit_measure_unit', 210)"><!--  -->&nbsp;&nbsp;&nbsp;&nbsp;<!--  --></span></td>
    <td width="10%"><img src="images/no.gif" onclick="listTable.toggle(this, 'toggle_show_in_nav', 210)" /></td>
    <td width="10%"><img src="images/yes.gif" onclick="listTable.toggle(this, 'toggle_is_show', 210)" /></td>
    <td><span onclick="listTable.edit(this, 'edit_grade', 210)">0</span></td>
    <td width="10%" align="right"><span onclick="listTable.edit(this, 'edit_sort_order', 210)">50</span></td>
    <td width="24%" align="center">
      <a href="category.php?act=move&cat_id=210">转移商品</a> |
      <a href="category.php?act=edit&amp;cat_id=210">编辑</a> |
      <a href="javascript:;" onclick="listTable.remove(210, '您确认要删除这条记录吗?')" title="移除">移除</a>
	  <!--代码修改_start Byjdy-->
	  	  <!--代码修改_end Byjdy-->
    </td>
  </tr>
    <tr align="center" class="2" id="2_207">
    <td align="left" class="first-cell" >
            <img src="images/menu_minus.gif" id="icon_2_207" width="9" height="9" border="0" style="margin-left:2em" onclick="rowClicked(this)" />
            <span>
      <!-- 0-默认列表 1-搜索匹配列表 其他-搜索未匹配列表 -->
            <a href="goods.php?act=list&cat_id=207&supp=-1">剃须刀</a>
            </span>
        </td>
    <td width="10%">0</td>
    <td width="10%"><span onclick="listTable.edit(this, 'edit_measure_unit', 207)"><!--  -->&nbsp;&nbsp;&nbsp;&nbsp;<!--  --></span></td>
    <td width="10%"><img src="images/no.gif" onclick="listTable.toggle(this, 'toggle_show_in_nav', 207)" /></td>
    <td width="10%"><img src="images/yes.gif" onclick="listTable.toggle(this, 'toggle_is_show', 207)" /></td>
    <td><span onclick="listTable.edit(this, 'edit_grade', 207)">0</span></td>
    <td width="10%" align="right"><span onclick="listTable.edit(this, 'edit_sort_order', 207)">50</span></td>
    <td width="24%" align="center">
      <a href="category.php?act=move&cat_id=207">转移商品</a> |
      <a href="category.php?act=edit&amp;cat_id=207">编辑</a> |
      <a href="javascript:;" onclick="listTable.remove(207, '您确认要删除这条记录吗?')" title="移除">移除</a>
	  <!--代码修改_start Byjdy-->
	  	  <!--代码修改_end Byjdy-->
    </td>
  </tr>
    <tr align="center" class="2" id="2_214">
    <td align="left" class="first-cell" >
            <img src="images/menu_minus.gif" id="icon_2_214" width="9" height="9" border="0" style="margin-left:2em" onclick="rowClicked(this)" />
            <span>
      <!-- 0-默认列表 1-搜索匹配列表 其他-搜索未匹配列表 -->
            <a href="goods.php?act=list&cat_id=214&supp=-1">其它健康电器</a>
            </span>
        </td>
    <td width="10%">0</td>
    <td width="10%"><span onclick="listTable.edit(this, 'edit_measure_unit', 214)"><!--  -->&nbsp;&nbsp;&nbsp;&nbsp;<!--  --></span></td>
    <td width="10%"><img src="images/no.gif" onclick="listTable.toggle(this, 'toggle_show_in_nav', 214)" /></td>
    <td width="10%"><img src="images/yes.gif" onclick="listTable.toggle(this, 'toggle_is_show', 214)" /></td>
    <td><span onclick="listTable.edit(this, 'edit_grade', 214)">0</span></td>
    <td width="10%" align="right"><span onclick="listTable.edit(this, 'edit_sort_order', 214)">50</span></td>
    <td width="24%" align="center">
      <a href="category.php?act=move&cat_id=214">转移商品</a> |
      <a href="category.php?act=edit&amp;cat_id=214">编辑</a> |
      <a href="javascript:;" onclick="listTable.remove(214, '您确认要删除这条记录吗?')" title="移除">移除</a>
	  <!--代码修改_start Byjdy-->
	  	  <!--代码修改_end Byjdy-->
    </td>
  </tr>
    <tr align="center" class="2" id="2_211">
    <td align="left" class="first-cell" >
            <img src="images/menu_minus.gif" id="icon_2_211" width="9" height="9" border="0" style="margin-left:2em" onclick="rowClicked(this)" />
            <span>
      <!-- 0-默认列表 1-搜索匹配列表 其他-搜索未匹配列表 -->
            <a href="goods.php?act=list&cat_id=211&supp=-1">健康秤/厨房秤</a>
            </span>
        </td>
    <td width="10%">0</td>
    <td width="10%"><span onclick="listTable.edit(this, 'edit_measure_unit', 211)"><!--  -->&nbsp;&nbsp;&nbsp;&nbsp;<!--  --></span></td>
    <td width="10%"><img src="images/no.gif" onclick="listTable.toggle(this, 'toggle_show_in_nav', 211)" /></td>
    <td width="10%"><img src="images/yes.gif" onclick="listTable.toggle(this, 'toggle_is_show', 211)" /></td>
    <td><span onclick="listTable.edit(this, 'edit_grade', 211)">0</span></td>
    <td width="10%" align="right"><span onclick="listTable.edit(this, 'edit_sort_order', 211)">50</span></td>
    <td width="24%" align="center">
      <a href="category.php?act=move&cat_id=211">转移商品</a> |
      <a href="category.php?act=edit&amp;cat_id=211">编辑</a> |
      <a href="javascript:;" onclick="listTable.remove(211, '您确认要删除这条记录吗?')" title="移除">移除</a>
	  <!--代码修改_start Byjdy-->
	  	  <!--代码修改_end Byjdy-->
    </td>
  </tr>
    <tr align="center" class="2" id="2_208">
    <td align="left" class="first-cell" >
            <img src="images/menu_minus.gif" id="icon_2_208" width="9" height="9" border="0" style="margin-left:2em" onclick="rowClicked(this)" />
            <span>
      <!-- 0-默认列表 1-搜索匹配列表 其他-搜索未匹配列表 -->
            <a href="goods.php?act=list&cat_id=208&supp=-1">电吹风</a>
            </span>
        </td>
    <td width="10%">0</td>
    <td width="10%"><span onclick="listTable.edit(this, 'edit_measure_unit', 208)"><!--  -->&nbsp;&nbsp;&nbsp;&nbsp;<!--  --></span></td>
    <td width="10%"><img src="images/no.gif" onclick="listTable.toggle(this, 'toggle_show_in_nav', 208)" /></td>
    <td width="10%"><img src="images/yes.gif" onclick="listTable.toggle(this, 'toggle_is_show', 208)" /></td>
    <td><span onclick="listTable.edit(this, 'edit_grade', 208)">0</span></td>
    <td width="10%" align="right"><span onclick="listTable.edit(this, 'edit_sort_order', 208)">50</span></td>
    <td width="24%" align="center">
      <a href="category.php?act=move&cat_id=208">转移商品</a> |
      <a href="category.php?act=edit&amp;cat_id=208">编辑</a> |
      <a href="javascript:;" onclick="listTable.remove(208, '您确认要删除这条记录吗?')" title="移除">移除</a>
	  <!--代码修改_start Byjdy-->
	  	  <!--代码修改_end Byjdy-->
    </td>
  </tr>
    <tr align="center" class="2" id="2_212">
    <td align="left" class="first-cell" >
            <img src="images/menu_minus.gif" id="icon_2_212" width="9" height="9" border="0" style="margin-left:2em" onclick="rowClicked(this)" />
            <span>
      <!-- 0-默认列表 1-搜索匹配列表 其他-搜索未匹配列表 -->
            <a href="goods.php?act=list&cat_id=212&supp=-1">血糖仪</a>
            </span>
        </td>
    <td width="10%">0</td>
    <td width="10%"><span onclick="listTable.edit(this, 'edit_measure_unit', 212)"><!--  -->&nbsp;&nbsp;&nbsp;&nbsp;<!--  --></span></td>
    <td width="10%"><img src="images/no.gif" onclick="listTable.toggle(this, 'toggle_show_in_nav', 212)" /></td>
    <td width="10%"><img src="images/yes.gif" onclick="listTable.toggle(this, 'toggle_is_show', 212)" /></td>
    <td><span onclick="listTable.edit(this, 'edit_grade', 212)">0</span></td>
    <td width="10%" align="right"><span onclick="listTable.edit(this, 'edit_sort_order', 212)">50</span></td>
    <td width="24%" align="center">
      <a href="category.php?act=move&cat_id=212">转移商品</a> |
      <a href="category.php?act=edit&amp;cat_id=212">编辑</a> |
      <a href="javascript:;" onclick="listTable.remove(212, '您确认要删除这条记录吗?')" title="移除">移除</a>
	  <!--代码修改_start Byjdy-->
	  	  <!--代码修改_end Byjdy-->
    </td>
  </tr>
    <tr align="center" class="1" id="1_178">
    <td align="left" class="first-cell" >
            <img src="images/menu_minus.gif" id="icon_1_178" width="9" height="9" border="0" style="margin-left:1em" onclick="rowClicked(this)" />
            <span>
      <!-- 0-默认列表 1-搜索匹配列表 其他-搜索未匹配列表 -->
            <a href="goods.php?act=list&cat_id=178&supp=-1">大家电</a>
            </span>
        </td>
    <td width="10%">8</td>
    <td width="10%"><span onclick="listTable.edit(this, 'edit_measure_unit', 178)"><!--  -->&nbsp;&nbsp;&nbsp;&nbsp;<!--  --></span></td>
    <td width="10%"><img src="images/no.gif" onclick="listTable.toggle(this, 'toggle_show_in_nav', 178)" /></td>
    <td width="10%"><img src="images/yes.gif" onclick="listTable.toggle(this, 'toggle_is_show', 178)" /></td>
    <td><span onclick="listTable.edit(this, 'edit_grade', 178)">0</span></td>
    <td width="10%" align="right"><span onclick="listTable.edit(this, 'edit_sort_order', 178)">50</span></td>
    <td width="24%" align="center">
      <a href="category.php?act=move&cat_id=178">转移商品</a> |
      <a href="category.php?act=edit&amp;cat_id=178">编辑</a> |
      <a href="javascript:;" onclick="listTable.remove(178, '您确认要删除这条记录吗?')" title="移除">移除</a>
	  <!--代码修改_start Byjdy-->
	  	  <!--代码修改_end Byjdy-->
    </td>
  </tr>
    <tr align="center" class="2" id="2_185">
    <td align="left" class="first-cell" >
            <img src="images/menu_minus.gif" id="icon_2_185" width="9" height="9" border="0" style="margin-left:2em" onclick="rowClicked(this)" />
            <span>
      <!-- 0-默认列表 1-搜索匹配列表 其他-搜索未匹配列表 -->
            <a href="goods.php?act=list&cat_id=185&supp=-1">洗衣机</a>
            </span>
        </td>
    <td width="10%">0</td>
    <td width="10%"><span onclick="listTable.edit(this, 'edit_measure_unit', 185)"><!--  -->&nbsp;&nbsp;&nbsp;&nbsp;<!--  --></span></td>
    <td width="10%"><img src="images/no.gif" onclick="listTable.toggle(this, 'toggle_show_in_nav', 185)" /></td>
    <td width="10%"><img src="images/yes.gif" onclick="listTable.toggle(this, 'toggle_is_show', 185)" /></td>
    <td><span onclick="listTable.edit(this, 'edit_grade', 185)">0</span></td>
    <td width="10%" align="right"><span onclick="listTable.edit(this, 'edit_sort_order', 185)">50</span></td>
    <td width="24%" align="center">
      <a href="category.php?act=move&cat_id=185">转移商品</a> |
      <a href="category.php?act=edit&amp;cat_id=185">编辑</a> |
      <a href="javascript:;" onclick="listTable.remove(185, '您确认要删除这条记录吗?')" title="移除">移除</a>
	  <!--代码修改_start Byjdy-->
	  	  <!--代码修改_end Byjdy-->
    </td>
  </tr>
    <tr align="center" class="2" id="2_189">
    <td align="left" class="first-cell" >
            <img src="images/menu_minus.gif" id="icon_2_189" width="9" height="9" border="0" style="margin-left:2em" onclick="rowClicked(this)" />
            <span>
      <!-- 0-默认列表 1-搜索匹配列表 其他-搜索未匹配列表 -->
            <a href="goods.php?act=list&cat_id=189&supp=-1">消毒柜/洗碗机</a>
            </span>
        </td>
    <td width="10%">0</td>
    <td width="10%"><span onclick="listTable.edit(this, 'edit_measure_unit', 189)"><!--  -->&nbsp;&nbsp;&nbsp;&nbsp;<!--  --></span></td>
    <td width="10%"><img src="images/no.gif" onclick="listTable.toggle(this, 'toggle_show_in_nav', 189)" /></td>
    <td width="10%"><img src="images/yes.gif" onclick="listTable.toggle(this, 'toggle_is_show', 189)" /></td>
    <td><span onclick="listTable.edit(this, 'edit_grade', 189)">0</span></td>
    <td width="10%" align="right"><span onclick="listTable.edit(this, 'edit_sort_order', 189)">50</span></td>
    <td width="24%" align="center">
      <a href="category.php?act=move&cat_id=189">转移商品</a> |
      <a href="category.php?act=edit&amp;cat_id=189">编辑</a> |
      <a href="javascript:;" onclick="listTable.remove(189, '您确认要删除这条记录吗?')" title="移除">移除</a>
	  <!--代码修改_start Byjdy-->
	  	  <!--代码修改_end Byjdy-->
    </td>
  </tr>
    <tr align="center" class="2" id="2_186">
    <td align="left" class="first-cell" >
            <img src="images/menu_minus.gif" id="icon_2_186" width="9" height="9" border="0" style="margin-left:2em" onclick="rowClicked(this)" />
            <span>
      <!-- 0-默认列表 1-搜索匹配列表 其他-搜索未匹配列表 -->
            <a href="goods.php?act=list&cat_id=186&supp=-1">家庭影院</a>
            </span>
        </td>
    <td width="10%">0</td>
    <td width="10%"><span onclick="listTable.edit(this, 'edit_measure_unit', 186)"><!--  -->&nbsp;&nbsp;&nbsp;&nbsp;<!--  --></span></td>
    <td width="10%"><img src="images/no.gif" onclick="listTable.toggle(this, 'toggle_show_in_nav', 186)" /></td>
    <td width="10%"><img src="images/yes.gif" onclick="listTable.toggle(this, 'toggle_is_show', 186)" /></td>
    <td><span onclick="listTable.edit(this, 'edit_grade', 186)">0</span></td>
    <td width="10%" align="right"><span onclick="listTable.edit(this, 'edit_sort_order', 186)">50</span></td>
    <td width="24%" align="center">
      <a href="category.php?act=move&cat_id=186">转移商品</a> |
      <a href="category.php?act=edit&amp;cat_id=186">编辑</a> |
      <a href="javascript:;" onclick="listTable.remove(186, '您确认要删除这条记录吗?')" title="移除">移除</a>
	  <!--代码修改_start Byjdy-->
	  	  <!--代码修改_end Byjdy-->
    </td>
  </tr>
    <tr align="center" class="2" id="2_183">
    <td align="left" class="first-cell" >
            <img src="images/menu_minus.gif" id="icon_2_183" width="9" height="9" border="0" style="margin-left:2em" onclick="rowClicked(this)" />
            <span>
      <!-- 0-默认列表 1-搜索匹配列表 其他-搜索未匹配列表 -->
            <a href="goods.php?act=list&cat_id=183&supp=-1">平板电视</a>
            </span>
        </td>
    <td width="10%">0</td>
    <td width="10%"><span onclick="listTable.edit(this, 'edit_measure_unit', 183)"><!--  -->&nbsp;&nbsp;&nbsp;&nbsp;<!--  --></span></td>
    <td width="10%"><img src="images/no.gif" onclick="listTable.toggle(this, 'toggle_show_in_nav', 183)" /></td>
    <td width="10%"><img src="images/yes.gif" onclick="listTable.toggle(this, 'toggle_is_show', 183)" /></td>
    <td><span onclick="listTable.edit(this, 'edit_grade', 183)">0</span></td>
    <td width="10%" align="right"><span onclick="listTable.edit(this, 'edit_sort_order', 183)">50</span></td>
    <td width="24%" align="center">
      <a href="category.php?act=move&cat_id=183">转移商品</a> |
      <a href="category.php?act=edit&amp;cat_id=183">编辑</a> |
      <a href="javascript:;" onclick="listTable.remove(183, '您确认要删除这条记录吗?')" title="移除">移除</a>
	  <!--代码修改_start Byjdy-->
	  	  <!--代码修改_end Byjdy-->
    </td>
  </tr>
    <tr align="center" class="2" id="2_190">
    <td align="left" class="first-cell" >
            <img src="images/menu_minus.gif" id="icon_2_190" width="9" height="9" border="0" style="margin-left:2em" onclick="rowClicked(this)" />
            <span>
      <!-- 0-默认列表 1-搜索匹配列表 其他-搜索未匹配列表 -->
            <a href="goods.php?act=list&cat_id=190&supp=-1">冷柜/冰吧</a>
            </span>
        </td>
    <td width="10%">0</td>
    <td width="10%"><span onclick="listTable.edit(this, 'edit_measure_unit', 190)"><!--  -->&nbsp;&nbsp;&nbsp;&nbsp;<!--  --></span></td>
    <td width="10%"><img src="images/no.gif" onclick="listTable.toggle(this, 'toggle_show_in_nav', 190)" /></td>
    <td width="10%"><img src="images/yes.gif" onclick="listTable.toggle(this, 'toggle_is_show', 190)" /></td>
    <td><span onclick="listTable.edit(this, 'edit_grade', 190)">0</span></td>
    <td width="10%" align="right"><span onclick="listTable.edit(this, 'edit_sort_order', 190)">50</span></td>
    <td width="24%" align="center">
      <a href="category.php?act=move&cat_id=190">转移商品</a> |
      <a href="category.php?act=edit&amp;cat_id=190">编辑</a> |
      <a href="javascript:;" onclick="listTable.remove(190, '您确认要删除这条记录吗?')" title="移除">移除</a>
	  <!--代码修改_start Byjdy-->
	  	  <!--代码修改_end Byjdy-->
    </td>
  </tr>
    <tr align="center" class="2" id="2_187">
    <td align="left" class="first-cell" >
            <img src="images/menu_minus.gif" id="icon_2_187" width="9" height="9" border="0" style="margin-left:2em" onclick="rowClicked(this)" />
            <span>
      <!-- 0-默认列表 1-搜索匹配列表 其他-搜索未匹配列表 -->
            <a href="goods.php?act=list&cat_id=187&supp=-1">烟机/灶具</a>
            </span>
        </td>
    <td width="10%">0</td>
    <td width="10%"><span onclick="listTable.edit(this, 'edit_measure_unit', 187)"><!--  -->&nbsp;&nbsp;&nbsp;&nbsp;<!--  --></span></td>
    <td width="10%"><img src="images/no.gif" onclick="listTable.toggle(this, 'toggle_show_in_nav', 187)" /></td>
    <td width="10%"><img src="images/yes.gif" onclick="listTable.toggle(this, 'toggle_is_show', 187)" /></td>
    <td><span onclick="listTable.edit(this, 'edit_grade', 187)">0</span></td>
    <td width="10%" align="right"><span onclick="listTable.edit(this, 'edit_sort_order', 187)">50</span></td>
    <td width="24%" align="center">
      <a href="category.php?act=move&cat_id=187">转移商品</a> |
      <a href="category.php?act=edit&amp;cat_id=187">编辑</a> |
      <a href="javascript:;" onclick="listTable.remove(187, '您确认要删除这条记录吗?')" title="移除">移除</a>
	  <!--代码修改_start Byjdy-->
	  	  <!--代码修改_end Byjdy-->
    </td>
  </tr>
    <tr align="center" class="2" id="2_184">
    <td align="left" class="first-cell" >
            <img src="images/menu_minus.gif" id="icon_2_184" width="9" height="9" border="0" style="margin-left:2em" onclick="rowClicked(this)" />
            <span>
      <!-- 0-默认列表 1-搜索匹配列表 其他-搜索未匹配列表 -->
            <a href="goods.php?act=list&cat_id=184&supp=-1">空调冰箱</a>
            </span>
        </td>
    <td width="10%">0</td>
    <td width="10%"><span onclick="listTable.edit(this, 'edit_measure_unit', 184)"><!--  -->&nbsp;&nbsp;&nbsp;&nbsp;<!--  --></span></td>
    <td width="10%"><img src="images/no.gif" onclick="listTable.toggle(this, 'toggle_show_in_nav', 184)" /></td>
    <td width="10%"><img src="images/yes.gif" onclick="listTable.toggle(this, 'toggle_is_show', 184)" /></td>
    <td><span onclick="listTable.edit(this, 'edit_grade', 184)">0</span></td>
    <td width="10%" align="right"><span onclick="listTable.edit(this, 'edit_sort_order', 184)">50</span></td>
    <td width="24%" align="center">
      <a href="category.php?act=move&cat_id=184">转移商品</a> |
      <a href="category.php?act=edit&amp;cat_id=184">编辑</a> |
      <a href="javascript:;" onclick="listTable.remove(184, '您确认要删除这条记录吗?')" title="移除">移除</a>
	  <!--代码修改_start Byjdy-->
	  	  <!--代码修改_end Byjdy-->
    </td>
  </tr>
    <tr align="center" class="2" id="2_188">
    <td align="left" class="first-cell" >
            <img src="images/menu_minus.gif" id="icon_2_188" width="9" height="9" border="0" style="margin-left:2em" onclick="rowClicked(this)" />
            <span>
      <!-- 0-默认列表 1-搜索匹配列表 其他-搜索未匹配列表 -->
            <a href="goods.php?act=list&cat_id=188&supp=-1">热水器</a>
            </span>
        </td>
    <td width="10%">0</td>
    <td width="10%"><span onclick="listTable.edit(this, 'edit_measure_unit', 188)"><!--  -->&nbsp;&nbsp;&nbsp;&nbsp;<!--  --></span></td>
    <td width="10%"><img src="images/no.gif" onclick="listTable.toggle(this, 'toggle_show_in_nav', 188)" /></td>
    <td width="10%"><img src="images/yes.gif" onclick="listTable.toggle(this, 'toggle_is_show', 188)" /></td>
    <td><span onclick="listTable.edit(this, 'edit_grade', 188)">0</span></td>
    <td width="10%" align="right"><span onclick="listTable.edit(this, 'edit_sort_order', 188)">50</span></td>
    <td width="24%" align="center">
      <a href="category.php?act=move&cat_id=188">转移商品</a> |
      <a href="category.php?act=edit&amp;cat_id=188">编辑</a> |
      <a href="javascript:;" onclick="listTable.remove(188, '您确认要删除这条记录吗?')" title="移除">移除</a>
	  <!--代码修改_start Byjdy-->
	  	  <!--代码修改_end Byjdy-->
    </td>
  </tr>
    <tr align="center" class="0" id="0_4">
    <td align="left" class="first-cell" >
            <img src="images/menu_minus.gif" id="icon_0_4" width="9" height="9" border="0" style="margin-left:0em" onclick="rowClicked(this)" />
            <span>
      <!-- 0-默认列表 1-搜索匹配列表 其他-搜索未匹配列表 -->
            <a href="goods.php?act=list&cat_id=4&supp=-1">手机、数码、通信</a>
            </span>
        </td>
    <td width="10%">0</td>
    <td width="10%"><span onclick="listTable.edit(this, 'edit_measure_unit', 4)"><!--  -->&nbsp;&nbsp;&nbsp;&nbsp;<!--  --></span></td>
    <td width="10%"><img src="images/no.gif" onclick="listTable.toggle(this, 'toggle_show_in_nav', 4)" /></td>
    <td width="10%"><img src="images/yes.gif" onclick="listTable.toggle(this, 'toggle_is_show', 4)" /></td>
    <td><span onclick="listTable.edit(this, 'edit_grade', 4)">8</span></td>
    <td width="10%" align="right"><span onclick="listTable.edit(this, 'edit_sort_order', 4)">2</span></td>
    <td width="24%" align="center">
      <a href="category.php?act=move&cat_id=4">转移商品</a> |
      <a href="category.php?act=edit&amp;cat_id=4">编辑</a> |
      <a href="javascript:;" onclick="listTable.remove(4, '您确认要删除这条记录吗?')" title="移除">移除</a>
	  <!--代码修改_start Byjdy-->
	  	  | <a href="category_flashimg.php?act=list&amp;cat_id=4">设置轮播图片</a>	  
	  	  <!--代码修改_end Byjdy-->
    </td>
  </tr>
    <tr align="center" class="1" id="1_147">
    <td align="left" class="first-cell" >
            <img src="images/menu_minus.gif" id="icon_1_147" width="9" height="9" border="0" style="margin-left:1em" onclick="rowClicked(this)" />
            <span>
      <!-- 0-默认列表 1-搜索匹配列表 其他-搜索未匹配列表 -->
            <a href="goods.php?act=list&cat_id=147&supp=-1">智能设备</a>
            </span>
        </td>
    <td width="10%">0</td>
    <td width="10%"><span onclick="listTable.edit(this, 'edit_measure_unit', 147)"><!--  -->&nbsp;&nbsp;&nbsp;&nbsp;<!--  --></span></td>
    <td width="10%"><img src="images/no.gif" onclick="listTable.toggle(this, 'toggle_show_in_nav', 147)" /></td>
    <td width="10%"><img src="images/yes.gif" onclick="listTable.toggle(this, 'toggle_is_show', 147)" /></td>
    <td><span onclick="listTable.edit(this, 'edit_grade', 147)">0</span></td>
    <td width="10%" align="right"><span onclick="listTable.edit(this, 'edit_sort_order', 147)">50</span></td>
    <td width="24%" align="center">
      <a href="category.php?act=move&cat_id=147">转移商品</a> |
      <a href="category.php?act=edit&amp;cat_id=147">编辑</a> |
      <a href="javascript:;" onclick="listTable.remove(147, '您确认要删除这条记录吗?')" title="移除">移除</a>
	  <!--代码修改_start Byjdy-->
	  	  <!--代码修改_end Byjdy-->
    </td>
  </tr>
    <tr align="center" class="2" id="2_169">
    <td align="left" class="first-cell" >
            <img src="images/menu_minus.gif" id="icon_2_169" width="9" height="9" border="0" style="margin-left:2em" onclick="rowClicked(this)" />
            <span>
      <!-- 0-默认列表 1-搜索匹配列表 其他-搜索未匹配列表 -->
            <a href="goods.php?act=list&cat_id=169&supp=-1">体感车</a>
            </span>
        </td>
    <td width="10%">0</td>
    <td width="10%"><span onclick="listTable.edit(this, 'edit_measure_unit', 169)"><!--  -->&nbsp;&nbsp;&nbsp;&nbsp;<!--  --></span></td>
    <td width="10%"><img src="images/no.gif" onclick="listTable.toggle(this, 'toggle_show_in_nav', 169)" /></td>
    <td width="10%"><img src="images/yes.gif" onclick="listTable.toggle(this, 'toggle_is_show', 169)" /></td>
    <td><span onclick="listTable.edit(this, 'edit_grade', 169)">0</span></td>
    <td width="10%" align="right"><span onclick="listTable.edit(this, 'edit_sort_order', 169)">50</span></td>
    <td width="24%" align="center">
      <a href="category.php?act=move&cat_id=169">转移商品</a> |
      <a href="category.php?act=edit&amp;cat_id=169">编辑</a> |
      <a href="javascript:;" onclick="listTable.remove(169, '您确认要删除这条记录吗?')" title="移除">移除</a>
	  <!--代码修改_start Byjdy-->
	  	  <!--代码修改_end Byjdy-->
    </td>
  </tr>
    <tr align="center" class="2" id="2_166">
    <td align="left" class="first-cell" >
            <img src="images/menu_minus.gif" id="icon_2_166" width="9" height="9" border="0" style="margin-left:2em" onclick="rowClicked(this)" />
            <span>
      <!-- 0-默认列表 1-搜索匹配列表 其他-搜索未匹配列表 -->
            <a href="goods.php?act=list&cat_id=166&supp=-1">智能眼镜</a>
            </span>
        </td>
    <td width="10%">0</td>
    <td width="10%"><span onclick="listTable.edit(this, 'edit_measure_unit', 166)"><!--  -->&nbsp;&nbsp;&nbsp;&nbsp;<!--  --></span></td>
    <td width="10%"><img src="images/no.gif" onclick="listTable.toggle(this, 'toggle_show_in_nav', 166)" /></td>
    <td width="10%"><img src="images/yes.gif" onclick="listTable.toggle(this, 'toggle_is_show', 166)" /></td>
    <td><span onclick="listTable.edit(this, 'edit_grade', 166)">0</span></td>
    <td width="10%" align="right"><span onclick="listTable.edit(this, 'edit_sort_order', 166)">50</span></td>
    <td width="24%" align="center">
      <a href="category.php?act=move&cat_id=166">转移商品</a> |
      <a href="category.php?act=edit&amp;cat_id=166">编辑</a> |
      <a href="javascript:;" onclick="listTable.remove(166, '您确认要删除这条记录吗?')" title="移除">移除</a>
	  <!--代码修改_start Byjdy-->
	  	  <!--代码修改_end Byjdy-->
    </td>
  </tr>
    <tr align="center" class="2" id="2_167">
    <td align="left" class="first-cell" >
            <img src="images/menu_minus.gif" id="icon_2_167" width="9" height="9" border="0" style="margin-left:2em" onclick="rowClicked(this)" />
            <span>
      <!-- 0-默认列表 1-搜索匹配列表 其他-搜索未匹配列表 -->
            <a href="goods.php?act=list&cat_id=167&supp=-1">运动跟踪器</a>
            </span>
        </td>
    <td width="10%">0</td>
    <td width="10%"><span onclick="listTable.edit(this, 'edit_measure_unit', 167)"><!--  -->&nbsp;&nbsp;&nbsp;&nbsp;<!--  --></span></td>
    <td width="10%"><img src="images/no.gif" onclick="listTable.toggle(this, 'toggle_show_in_nav', 167)" /></td>
    <td width="10%"><img src="images/yes.gif" onclick="listTable.toggle(this, 'toggle_is_show', 167)" /></td>
    <td><span onclick="listTable.edit(this, 'edit_grade', 167)">0</span></td>
    <td width="10%" align="right"><span onclick="listTable.edit(this, 'edit_sort_order', 167)">50</span></td>
    <td width="24%" align="center">
      <a href="category.php?act=move&cat_id=167">转移商品</a> |
      <a href="category.php?act=edit&amp;cat_id=167">编辑</a> |
      <a href="javascript:;" onclick="listTable.remove(167, '您确认要删除这条记录吗?')" title="移除">移除</a>
	  <!--代码修改_start Byjdy-->
	  	  <!--代码修改_end Byjdy-->
    </td>
  </tr>
    <tr align="center" class="2" id="2_164">
    <td align="left" class="first-cell" >
            <img src="images/menu_minus.gif" id="icon_2_164" width="9" height="9" border="0" style="margin-left:2em" onclick="rowClicked(this)" />
            <span>
      <!-- 0-默认列表 1-搜索匹配列表 其他-搜索未匹配列表 -->
            <a href="goods.php?act=list&cat_id=164&supp=-1">智能手环</a>
            </span>
        </td>
    <td width="10%">0</td>
    <td width="10%"><span onclick="listTable.edit(this, 'edit_measure_unit', 164)"><!--  -->&nbsp;&nbsp;&nbsp;&nbsp;<!--  --></span></td>
    <td width="10%"><img src="images/no.gif" onclick="listTable.toggle(this, 'toggle_show_in_nav', 164)" /></td>
    <td width="10%"><img src="images/yes.gif" onclick="listTable.toggle(this, 'toggle_is_show', 164)" /></td>
    <td><span onclick="listTable.edit(this, 'edit_grade', 164)">0</span></td>
    <td width="10%" align="right"><span onclick="listTable.edit(this, 'edit_sort_order', 164)">50</span></td>
    <td width="24%" align="center">
      <a href="category.php?act=move&cat_id=164">转移商品</a> |
      <a href="category.php?act=edit&amp;cat_id=164">编辑</a> |
      <a href="javascript:;" onclick="listTable.remove(164, '您确认要删除这条记录吗?')" title="移除">移除</a>
	  <!--代码修改_start Byjdy-->
	  	  <!--代码修改_end Byjdy-->
    </td>
  </tr>
    <tr align="center" class="2" id="2_168">
    <td align="left" class="first-cell" >
            <img src="images/menu_minus.gif" id="icon_2_168" width="9" height="9" border="0" style="margin-left:2em" onclick="rowClicked(this)" />
            <span>
      <!-- 0-默认列表 1-搜索匹配列表 其他-搜索未匹配列表 -->
            <a href="goods.php?act=list&cat_id=168&supp=-1">智能家居</a>
            </span>
        </td>
    <td width="10%">0</td>
    <td width="10%"><span onclick="listTable.edit(this, 'edit_measure_unit', 168)"><!--  -->&nbsp;&nbsp;&nbsp;&nbsp;<!--  --></span></td>
    <td width="10%"><img src="images/no.gif" onclick="listTable.toggle(this, 'toggle_show_in_nav', 168)" /></td>
    <td width="10%"><img src="images/yes.gif" onclick="listTable.toggle(this, 'toggle_is_show', 168)" /></td>
    <td><span onclick="listTable.edit(this, 'edit_grade', 168)">0</span></td>
    <td width="10%" align="right"><span onclick="listTable.edit(this, 'edit_sort_order', 168)">50</span></td>
    <td width="24%" align="center">
      <a href="category.php?act=move&cat_id=168">转移商品</a> |
      <a href="category.php?act=edit&amp;cat_id=168">编辑</a> |
      <a href="javascript:;" onclick="listTable.remove(168, '您确认要删除这条记录吗?')" title="移除">移除</a>
	  <!--代码修改_start Byjdy-->
	  	  <!--代码修改_end Byjdy-->
    </td>
  </tr>
    <tr align="center" class="2" id="2_165">
    <td align="left" class="first-cell" >
            <img src="images/menu_minus.gif" id="icon_2_165" width="9" height="9" border="0" style="margin-left:2em" onclick="rowClicked(this)" />
            <span>
      <!-- 0-默认列表 1-搜索匹配列表 其他-搜索未匹配列表 -->
            <a href="goods.php?act=list&cat_id=165&supp=-1">智能手表</a>
            </span>
        </td>
    <td width="10%">0</td>
    <td width="10%"><span onclick="listTable.edit(this, 'edit_measure_unit', 165)"><!--  -->&nbsp;&nbsp;&nbsp;&nbsp;<!--  --></span></td>
    <td width="10%"><img src="images/no.gif" onclick="listTable.toggle(this, 'toggle_show_in_nav', 165)" /></td>
    <td width="10%"><img src="images/yes.gif" onclick="listTable.toggle(this, 'toggle_is_show', 165)" /></td>
    <td><span onclick="listTable.edit(this, 'edit_grade', 165)">0</span></td>
    <td width="10%" align="right"><span onclick="listTable.edit(this, 'edit_sort_order', 165)">50</span></td>
    <td width="24%" align="center">
      <a href="category.php?act=move&cat_id=165">转移商品</a> |
      <a href="category.php?act=edit&amp;cat_id=165">编辑</a> |
      <a href="javascript:;" onclick="listTable.remove(165, '您确认要删除这条记录吗?')" title="移除">移除</a>
	  <!--代码修改_start Byjdy-->
	  	  <!--代码修改_end Byjdy-->
    </td>
  </tr>
    <tr align="center" class="1" id="1_144">
    <td align="left" class="first-cell" >
            <img src="images/menu_minus.gif" id="icon_1_144" width="9" height="9" border="0" style="margin-left:1em" onclick="rowClicked(this)" />
            <span>
      <!-- 0-默认列表 1-搜索匹配列表 其他-搜索未匹配列表 -->
            <a href="goods.php?act=list&cat_id=144&supp=-1">热卖手机</a>
            </span>
        </td>
    <td width="10%">4</td>
    <td width="10%"><span onclick="listTable.edit(this, 'edit_measure_unit', 144)"><!--  -->&nbsp;&nbsp;&nbsp;&nbsp;<!--  --></span></td>
    <td width="10%"><img src="images/no.gif" onclick="listTable.toggle(this, 'toggle_show_in_nav', 144)" /></td>
    <td width="10%"><img src="images/yes.gif" onclick="listTable.toggle(this, 'toggle_is_show', 144)" /></td>
    <td><span onclick="listTable.edit(this, 'edit_grade', 144)">0</span></td>
    <td width="10%" align="right"><span onclick="listTable.edit(this, 'edit_sort_order', 144)">50</span></td>
    <td width="24%" align="center">
      <a href="category.php?act=move&cat_id=144">转移商品</a> |
      <a href="category.php?act=edit&amp;cat_id=144">编辑</a> |
      <a href="javascript:;" onclick="listTable.remove(144, '您确认要删除这条记录吗?')" title="移除">移除</a>
	  <!--代码修改_start Byjdy-->
	  	  <!--代码修改_end Byjdy-->
    </td>
  </tr>
    <tr align="center" class="2" id="2_153">
    <td align="left" class="first-cell" >
            <img src="images/menu_minus.gif" id="icon_2_153" width="9" height="9" border="0" style="margin-left:2em" onclick="rowClicked(this)" />
            <span>
      <!-- 0-默认列表 1-搜索匹配列表 其他-搜索未匹配列表 -->
            <a href="goods.php?act=list&cat_id=153&supp=-1">联通4G</a>
            </span>
        </td>
    <td width="10%">0</td>
    <td width="10%"><span onclick="listTable.edit(this, 'edit_measure_unit', 153)"><!--  -->&nbsp;&nbsp;&nbsp;&nbsp;<!--  --></span></td>
    <td width="10%"><img src="images/no.gif" onclick="listTable.toggle(this, 'toggle_show_in_nav', 153)" /></td>
    <td width="10%"><img src="images/yes.gif" onclick="listTable.toggle(this, 'toggle_is_show', 153)" /></td>
    <td><span onclick="listTable.edit(this, 'edit_grade', 153)">0</span></td>
    <td width="10%" align="right"><span onclick="listTable.edit(this, 'edit_sort_order', 153)">50</span></td>
    <td width="24%" align="center">
      <a href="category.php?act=move&cat_id=153">转移商品</a> |
      <a href="category.php?act=edit&amp;cat_id=153">编辑</a> |
      <a href="javascript:;" onclick="listTable.remove(153, '您确认要删除这条记录吗?')" title="移除">移除</a>
	  <!--代码修改_start Byjdy-->
	  	  <!--代码修改_end Byjdy-->
    </td>
  </tr>
    <tr align="center" class="2" id="2_150">
    <td align="left" class="first-cell" >
            <img src="images/menu_minus.gif" id="icon_2_150" width="9" height="9" border="0" style="margin-left:2em" onclick="rowClicked(this)" />
            <span>
      <!-- 0-默认列表 1-搜索匹配列表 其他-搜索未匹配列表 -->
            <a href="goods.php?act=list&cat_id=150&supp=-1">小米特供</a>
            </span>
        </td>
    <td width="10%">1</td>
    <td width="10%"><span onclick="listTable.edit(this, 'edit_measure_unit', 150)"><!--  -->&nbsp;&nbsp;&nbsp;&nbsp;<!--  --></span></td>
    <td width="10%"><img src="images/no.gif" onclick="listTable.toggle(this, 'toggle_show_in_nav', 150)" /></td>
    <td width="10%"><img src="images/yes.gif" onclick="listTable.toggle(this, 'toggle_is_show', 150)" /></td>
    <td><span onclick="listTable.edit(this, 'edit_grade', 150)">0</span></td>
    <td width="10%" align="right"><span onclick="listTable.edit(this, 'edit_sort_order', 150)">50</span></td>
    <td width="24%" align="center">
      <a href="category.php?act=move&cat_id=150">转移商品</a> |
      <a href="category.php?act=edit&amp;cat_id=150">编辑</a> |
      <a href="javascript:;" onclick="listTable.remove(150, '您确认要删除这条记录吗?')" title="移除">移除</a>
	  <!--代码修改_start Byjdy-->
	  	  <!--代码修改_end Byjdy-->
    </td>
  </tr>
    <tr align="center" class="2" id="2_154">
    <td align="left" class="first-cell" >
            <img src="images/menu_minus.gif" id="icon_2_154" width="9" height="9" border="0" style="margin-left:2em" onclick="rowClicked(this)" />
            <span>
      <!-- 0-默认列表 1-搜索匹配列表 其他-搜索未匹配列表 -->
            <a href="goods.php?act=list&cat_id=154&supp=-1">电信4G</a>
            </span>
        </td>
    <td width="10%">0</td>
    <td width="10%"><span onclick="listTable.edit(this, 'edit_measure_unit', 154)"><!--  -->&nbsp;&nbsp;&nbsp;&nbsp;<!--  --></span></td>
    <td width="10%"><img src="images/no.gif" onclick="listTable.toggle(this, 'toggle_show_in_nav', 154)" /></td>
    <td width="10%"><img src="images/yes.gif" onclick="listTable.toggle(this, 'toggle_is_show', 154)" /></td>
    <td><span onclick="listTable.edit(this, 'edit_grade', 154)">0</span></td>
    <td width="10%" align="right"><span onclick="listTable.edit(this, 'edit_sort_order', 154)">50</span></td>
    <td width="24%" align="center">
      <a href="category.php?act=move&cat_id=154">转移商品</a> |
      <a href="category.php?act=edit&amp;cat_id=154">编辑</a> |
      <a href="javascript:;" onclick="listTable.remove(154, '您确认要删除这条记录吗?')" title="移除">移除</a>
	  <!--代码修改_start Byjdy-->
	  	  <!--代码修改_end Byjdy-->
    </td>
  </tr>
    <tr align="center" class="2" id="2_151">
    <td align="left" class="first-cell" >
            <img src="images/menu_minus.gif" id="icon_2_151" width="9" height="9" border="0" style="margin-left:2em" onclick="rowClicked(this)" />
            <span>
      <!-- 0-默认列表 1-搜索匹配列表 其他-搜索未匹配列表 -->
            <a href="goods.php?act=list&cat_id=151&supp=-1">魅族手机</a>
            </span>
        </td>
    <td width="10%">0</td>
    <td width="10%"><span onclick="listTable.edit(this, 'edit_measure_unit', 151)"><!--  -->&nbsp;&nbsp;&nbsp;&nbsp;<!--  --></span></td>
    <td width="10%"><img src="images/no.gif" onclick="listTable.toggle(this, 'toggle_show_in_nav', 151)" /></td>
    <td width="10%"><img src="images/yes.gif" onclick="listTable.toggle(this, 'toggle_is_show', 151)" /></td>
    <td><span onclick="listTable.edit(this, 'edit_grade', 151)">0</span></td>
    <td width="10%" align="right"><span onclick="listTable.edit(this, 'edit_sort_order', 151)">50</span></td>
    <td width="24%" align="center">
      <a href="category.php?act=move&cat_id=151">转移商品</a> |
      <a href="category.php?act=edit&amp;cat_id=151">编辑</a> |
      <a href="javascript:;" onclick="listTable.remove(151, '您确认要删除这条记录吗?')" title="移除">移除</a>
	  <!--代码修改_start Byjdy-->
	  	  <!--代码修改_end Byjdy-->
    </td>
  </tr>
    <tr align="center" class="2" id="2_148">
    <td align="left" class="first-cell" >
            <img src="images/menu_minus.gif" id="icon_2_148" width="9" height="9" border="0" style="margin-left:2em" onclick="rowClicked(this)" />
            <span>
      <!-- 0-默认列表 1-搜索匹配列表 其他-搜索未匹配列表 -->
            <a href="goods.php?act=list&cat_id=148&supp=-1">三星盖乐世</a>
            </span>
        </td>
    <td width="10%">1</td>
    <td width="10%"><span onclick="listTable.edit(this, 'edit_measure_unit', 148)"><!--  -->&nbsp;&nbsp;&nbsp;&nbsp;<!--  --></span></td>
    <td width="10%"><img src="images/no.gif" onclick="listTable.toggle(this, 'toggle_show_in_nav', 148)" /></td>
    <td width="10%"><img src="images/yes.gif" onclick="listTable.toggle(this, 'toggle_is_show', 148)" /></td>
    <td><span onclick="listTable.edit(this, 'edit_grade', 148)">0</span></td>
    <td width="10%" align="right"><span onclick="listTable.edit(this, 'edit_sort_order', 148)">50</span></td>
    <td width="24%" align="center">
      <a href="category.php?act=move&cat_id=148">转移商品</a> |
      <a href="category.php?act=edit&amp;cat_id=148">编辑</a> |
      <a href="javascript:;" onclick="listTable.remove(148, '您确认要删除这条记录吗?')" title="移除">移除</a>
	  <!--代码修改_start Byjdy-->
	  	  <!--代码修改_end Byjdy-->
    </td>
  </tr>
    <tr align="center" class="2" id="2_155">
    <td align="left" class="first-cell" >
            <img src="images/menu_minus.gif" id="icon_2_155" width="9" height="9" border="0" style="margin-left:2em" onclick="rowClicked(this)" />
            <span>
      <!-- 0-默认列表 1-搜索匹配列表 其他-搜索未匹配列表 -->
            <a href="goods.php?act=list&cat_id=155&supp=-1">移动4G</a>
            </span>
        </td>
    <td width="10%">0</td>
    <td width="10%"><span onclick="listTable.edit(this, 'edit_measure_unit', 155)"><!--  -->&nbsp;&nbsp;&nbsp;&nbsp;<!--  --></span></td>
    <td width="10%"><img src="images/no.gif" onclick="listTable.toggle(this, 'toggle_show_in_nav', 155)" /></td>
    <td width="10%"><img src="images/yes.gif" onclick="listTable.toggle(this, 'toggle_is_show', 155)" /></td>
    <td><span onclick="listTable.edit(this, 'edit_grade', 155)">0</span></td>
    <td width="10%" align="right"><span onclick="listTable.edit(this, 'edit_sort_order', 155)">50</span></td>
    <td width="24%" align="center">
      <a href="category.php?act=move&cat_id=155">转移商品</a> |
      <a href="category.php?act=edit&amp;cat_id=155">编辑</a> |
      <a href="javascript:;" onclick="listTable.remove(155, '您确认要删除这条记录吗?')" title="移除">移除</a>
	  <!--代码修改_start Byjdy-->
	  	  <!--代码修改_end Byjdy-->
    </td>
  </tr>
    <tr align="center" class="2" id="2_152">
    <td align="left" class="first-cell" >
            <img src="images/menu_minus.gif" id="icon_2_152" width="9" height="9" border="0" style="margin-left:2em" onclick="rowClicked(this)" />
            <span>
      <!-- 0-默认列表 1-搜索匹配列表 其他-搜索未匹配列表 -->
            <a href="goods.php?act=list&cat_id=152&supp=-1">华为荣耀</a>
            </span>
        </td>
    <td width="10%">1</td>
    <td width="10%"><span onclick="listTable.edit(this, 'edit_measure_unit', 152)"><!--  -->&nbsp;&nbsp;&nbsp;&nbsp;<!--  --></span></td>
    <td width="10%"><img src="images/no.gif" onclick="listTable.toggle(this, 'toggle_show_in_nav', 152)" /></td>
    <td width="10%"><img src="images/yes.gif" onclick="listTable.toggle(this, 'toggle_is_show', 152)" /></td>
    <td><span onclick="listTable.edit(this, 'edit_grade', 152)">0</span></td>
    <td width="10%" align="right"><span onclick="listTable.edit(this, 'edit_sort_order', 152)">50</span></td>
    <td width="24%" align="center">
      <a href="category.php?act=move&cat_id=152">转移商品</a> |
      <a href="category.php?act=edit&amp;cat_id=152">编辑</a> |
      <a href="javascript:;" onclick="listTable.remove(152, '您确认要删除这条记录吗?')" title="移除">移除</a>
	  <!--代码修改_start Byjdy-->
	  	  <!--代码修改_end Byjdy-->
    </td>
  </tr>
    <tr align="center" class="2" id="2_149">
    <td align="left" class="first-cell" >
            <img src="images/menu_minus.gif" id="icon_2_149" width="9" height="9" border="0" style="margin-left:2em" onclick="rowClicked(this)" />
            <span>
      <!-- 0-默认列表 1-搜索匹配列表 其他-搜索未匹配列表 -->
            <a href="goods.php?act=list&cat_id=149&supp=-1">iPhone</a>
            </span>
        </td>
    <td width="10%">2</td>
    <td width="10%"><span onclick="listTable.edit(this, 'edit_measure_unit', 149)"><!--  -->&nbsp;&nbsp;&nbsp;&nbsp;<!--  --></span></td>
    <td width="10%"><img src="images/no.gif" onclick="listTable.toggle(this, 'toggle_show_in_nav', 149)" /></td>
    <td width="10%"><img src="images/yes.gif" onclick="listTable.toggle(this, 'toggle_is_show', 149)" /></td>
    <td><span onclick="listTable.edit(this, 'edit_grade', 149)">0</span></td>
    <td width="10%" align="right"><span onclick="listTable.edit(this, 'edit_sort_order', 149)">50</span></td>
    <td width="24%" align="center">
      <a href="category.php?act=move&cat_id=149">转移商品</a> |
      <a href="category.php?act=edit&amp;cat_id=149">编辑</a> |
      <a href="javascript:;" onclick="listTable.remove(149, '您确认要删除这条记录吗?')" title="移除">移除</a>
	  <!--代码修改_start Byjdy-->
	  	  <!--代码修改_end Byjdy-->
    </td>
  </tr>
    <tr align="center" class="1" id="1_145">
    <td align="left" class="first-cell" >
            <img src="images/menu_minus.gif" id="icon_1_145" width="9" height="9" border="0" style="margin-left:1em" onclick="rowClicked(this)" />
            <span>
      <!-- 0-默认列表 1-搜索匹配列表 其他-搜索未匹配列表 -->
            <a href="goods.php?act=list&cat_id=145&supp=-1">手机配件</a>
            </span>
        </td>
    <td width="10%">8</td>
    <td width="10%"><span onclick="listTable.edit(this, 'edit_measure_unit', 145)"><!--  -->&nbsp;&nbsp;&nbsp;&nbsp;<!--  --></span></td>
    <td width="10%"><img src="images/no.gif" onclick="listTable.toggle(this, 'toggle_show_in_nav', 145)" /></td>
    <td width="10%"><img src="images/yes.gif" onclick="listTable.toggle(this, 'toggle_is_show', 145)" /></td>
    <td><span onclick="listTable.edit(this, 'edit_grade', 145)">0</span></td>
    <td width="10%" align="right"><span onclick="listTable.edit(this, 'edit_sort_order', 145)">50</span></td>
    <td width="24%" align="center">
      <a href="category.php?act=move&cat_id=145">转移商品</a> |
      <a href="category.php?act=edit&amp;cat_id=145">编辑</a> |
      <a href="javascript:;" onclick="listTable.remove(145, '您确认要删除这条记录吗?')" title="移除">移除</a>
	  <!--代码修改_start Byjdy-->
	  	  <!--代码修改_end Byjdy-->
    </td>
  </tr>
    <tr align="center" class="2" id="2_163">
    <td align="left" class="first-cell" >
            <img src="images/menu_minus.gif" id="icon_2_163" width="9" height="9" border="0" style="margin-left:2em" onclick="rowClicked(this)" />
            <span>
      <!-- 0-默认列表 1-搜索匹配列表 其他-搜索未匹配列表 -->
            <a href="goods.php?act=list&cat_id=163&supp=-1">保护套</a>
            </span>
        </td>
    <td width="10%">0</td>
    <td width="10%"><span onclick="listTable.edit(this, 'edit_measure_unit', 163)"><!--  -->&nbsp;&nbsp;&nbsp;&nbsp;<!--  --></span></td>
    <td width="10%"><img src="images/no.gif" onclick="listTable.toggle(this, 'toggle_show_in_nav', 163)" /></td>
    <td width="10%"><img src="images/yes.gif" onclick="listTable.toggle(this, 'toggle_is_show', 163)" /></td>
    <td><span onclick="listTable.edit(this, 'edit_grade', 163)">0</span></td>
    <td width="10%" align="right"><span onclick="listTable.edit(this, 'edit_sort_order', 163)">50</span></td>
    <td width="24%" align="center">
      <a href="category.php?act=move&cat_id=163">转移商品</a> |
      <a href="category.php?act=edit&amp;cat_id=163">编辑</a> |
      <a href="javascript:;" onclick="listTable.remove(163, '您确认要删除这条记录吗?')" title="移除">移除</a>
	  <!--代码修改_start Byjdy-->
	  	  <!--代码修改_end Byjdy-->
    </td>
  </tr>
    <tr align="center" class="2" id="2_160">
    <td align="left" class="first-cell" >
            <img src="images/menu_minus.gif" id="icon_2_160" width="9" height="9" border="0" style="margin-left:2em" onclick="rowClicked(this)" />
            <span>
      <!-- 0-默认列表 1-搜索匹配列表 其他-搜索未匹配列表 -->
            <a href="goods.php?act=list&cat_id=160&supp=-1">创意配件</a>
            </span>
        </td>
    <td width="10%">0</td>
    <td width="10%"><span onclick="listTable.edit(this, 'edit_measure_unit', 160)"><!--  -->&nbsp;&nbsp;&nbsp;&nbsp;<!--  --></span></td>
    <td width="10%"><img src="images/no.gif" onclick="listTable.toggle(this, 'toggle_show_in_nav', 160)" /></td>
    <td width="10%"><img src="images/yes.gif" onclick="listTable.toggle(this, 'toggle_is_show', 160)" /></td>
    <td><span onclick="listTable.edit(this, 'edit_grade', 160)">0</span></td>
    <td width="10%" align="right"><span onclick="listTable.edit(this, 'edit_sort_order', 160)">50</span></td>
    <td width="24%" align="center">
      <a href="category.php?act=move&cat_id=160">转移商品</a> |
      <a href="category.php?act=edit&amp;cat_id=160">编辑</a> |
      <a href="javascript:;" onclick="listTable.remove(160, '您确认要删除这条记录吗?')" title="移除">移除</a>
	  <!--代码修改_start Byjdy-->
	  	  <!--代码修改_end Byjdy-->
    </td>
  </tr>
    <tr align="center" class="2" id="2_157">
    <td align="left" class="first-cell" >
            <img src="images/menu_minus.gif" id="icon_2_157" width="9" height="9" border="0" style="margin-left:2em" onclick="rowClicked(this)" />
            <span>
      <!-- 0-默认列表 1-搜索匹配列表 其他-搜索未匹配列表 -->
            <a href="goods.php?act=list&cat_id=157&supp=-1">移动电源</a>
            </span>
        </td>
    <td width="10%">0</td>
    <td width="10%"><span onclick="listTable.edit(this, 'edit_measure_unit', 157)"><!--  -->&nbsp;&nbsp;&nbsp;&nbsp;<!--  --></span></td>
    <td width="10%"><img src="images/no.gif" onclick="listTable.toggle(this, 'toggle_show_in_nav', 157)" /></td>
    <td width="10%"><img src="images/yes.gif" onclick="listTable.toggle(this, 'toggle_is_show', 157)" /></td>
    <td><span onclick="listTable.edit(this, 'edit_grade', 157)">0</span></td>
    <td width="10%" align="right"><span onclick="listTable.edit(this, 'edit_sort_order', 157)">50</span></td>
    <td width="24%" align="center">
      <a href="category.php?act=move&cat_id=157">转移商品</a> |
      <a href="category.php?act=edit&amp;cat_id=157">编辑</a> |
      <a href="javascript:;" onclick="listTable.remove(157, '您确认要删除这条记录吗?')" title="移除">移除</a>
	  <!--代码修改_start Byjdy-->
	  	  <!--代码修改_end Byjdy-->
    </td>
  </tr>
    <tr align="center" class="2" id="2_161">
    <td align="left" class="first-cell" >
            <img src="images/menu_minus.gif" id="icon_2_161" width="9" height="9" border="0" style="margin-left:2em" onclick="rowClicked(this)" />
            <span>
      <!-- 0-默认列表 1-搜索匹配列表 其他-搜索未匹配列表 -->
            <a href="goods.php?act=list&cat_id=161&supp=-1">手机饰品</a>
            </span>
        </td>
    <td width="10%">0</td>
    <td width="10%"><span onclick="listTable.edit(this, 'edit_measure_unit', 161)"><!--  -->&nbsp;&nbsp;&nbsp;&nbsp;<!--  --></span></td>
    <td width="10%"><img src="images/no.gif" onclick="listTable.toggle(this, 'toggle_show_in_nav', 161)" /></td>
    <td width="10%"><img src="images/yes.gif" onclick="listTable.toggle(this, 'toggle_is_show', 161)" /></td>
    <td><span onclick="listTable.edit(this, 'edit_grade', 161)">0</span></td>
    <td width="10%" align="right"><span onclick="listTable.edit(this, 'edit_sort_order', 161)">50</span></td>
    <td width="24%" align="center">
      <a href="category.php?act=move&cat_id=161">转移商品</a> |
      <a href="category.php?act=edit&amp;cat_id=161">编辑</a> |
      <a href="javascript:;" onclick="listTable.remove(161, '您确认要删除这条记录吗?')" title="移除">移除</a>
	  <!--代码修改_start Byjdy-->
	  	  <!--代码修改_end Byjdy-->
    </td>
  </tr>
    <tr align="center" class="2" id="2_158">
    <td align="left" class="first-cell" >
            <img src="images/menu_minus.gif" id="icon_2_158" width="9" height="9" border="0" style="margin-left:2em" onclick="rowClicked(this)" />
            <span>
      <!-- 0-默认列表 1-搜索匹配列表 其他-搜索未匹配列表 -->
            <a href="goods.php?act=list&cat_id=158&supp=-1">蓝牙耳机</a>
            </span>
        </td>
    <td width="10%">0</td>
    <td width="10%"><span onclick="listTable.edit(this, 'edit_measure_unit', 158)"><!--  -->&nbsp;&nbsp;&nbsp;&nbsp;<!--  --></span></td>
    <td width="10%"><img src="images/no.gif" onclick="listTable.toggle(this, 'toggle_show_in_nav', 158)" /></td>
    <td width="10%"><img src="images/yes.gif" onclick="listTable.toggle(this, 'toggle_is_show', 158)" /></td>
    <td><span onclick="listTable.edit(this, 'edit_grade', 158)">0</span></td>
    <td width="10%" align="right"><span onclick="listTable.edit(this, 'edit_sort_order', 158)">50</span></td>
    <td width="24%" align="center">
      <a href="category.php?act=move&cat_id=158">转移商品</a> |
      <a href="category.php?act=edit&amp;cat_id=158">编辑</a> |
      <a href="javascript:;" onclick="listTable.remove(158, '您确认要删除这条记录吗?')" title="移除">移除</a>
	  <!--代码修改_start Byjdy-->
	  	  <!--代码修改_end Byjdy-->
    </td>
  </tr>
    <tr align="center" class="2" id="2_162">
    <td align="left" class="first-cell" >
            <img src="images/menu_minus.gif" id="icon_2_162" width="9" height="9" border="0" style="margin-left:2em" onclick="rowClicked(this)" />
            <span>
      <!-- 0-默认列表 1-搜索匹配列表 其他-搜索未匹配列表 -->
            <a href="goods.php?act=list&cat_id=162&supp=-1">手机耳机</a>
            </span>
        </td>
    <td width="10%">0</td>
    <td width="10%"><span onclick="listTable.edit(this, 'edit_measure_unit', 162)"><!--  -->&nbsp;&nbsp;&nbsp;&nbsp;<!--  --></span></td>
    <td width="10%"><img src="images/no.gif" onclick="listTable.toggle(this, 'toggle_show_in_nav', 162)" /></td>
    <td width="10%"><img src="images/yes.gif" onclick="listTable.toggle(this, 'toggle_is_show', 162)" /></td>
    <td><span onclick="listTable.edit(this, 'edit_grade', 162)">0</span></td>
    <td width="10%" align="right"><span onclick="listTable.edit(this, 'edit_sort_order', 162)">50</span></td>
    <td width="24%" align="center">
      <a href="category.php?act=move&cat_id=162">转移商品</a> |
      <a href="category.php?act=edit&amp;cat_id=162">编辑</a> |
      <a href="javascript:;" onclick="listTable.remove(162, '您确认要删除这条记录吗?')" title="移除">移除</a>
	  <!--代码修改_start Byjdy-->
	  	  <!--代码修改_end Byjdy-->
    </td>
  </tr>
    <tr align="center" class="2" id="2_159">
    <td align="left" class="first-cell" >
            <img src="images/menu_minus.gif" id="icon_2_159" width="9" height="9" border="0" style="margin-left:2em" onclick="rowClicked(this)" />
            <span>
      <!-- 0-默认列表 1-搜索匹配列表 其他-搜索未匹配列表 -->
            <a href="goods.php?act=list&cat_id=159&supp=-1">充电器</a>
            </span>
        </td>
    <td width="10%">0</td>
    <td width="10%"><span onclick="listTable.edit(this, 'edit_measure_unit', 159)"><!--  -->&nbsp;&nbsp;&nbsp;&nbsp;<!--  --></span></td>
    <td width="10%"><img src="images/no.gif" onclick="listTable.toggle(this, 'toggle_show_in_nav', 159)" /></td>
    <td width="10%"><img src="images/yes.gif" onclick="listTable.toggle(this, 'toggle_is_show', 159)" /></td>
    <td><span onclick="listTable.edit(this, 'edit_grade', 159)">0</span></td>
    <td width="10%" align="right"><span onclick="listTable.edit(this, 'edit_sort_order', 159)">50</span></td>
    <td width="24%" align="center">
      <a href="category.php?act=move&cat_id=159">转移商品</a> |
      <a href="category.php?act=edit&amp;cat_id=159">编辑</a> |
      <a href="javascript:;" onclick="listTable.remove(159, '您确认要删除这条记录吗?')" title="移除">移除</a>
	  <!--代码修改_start Byjdy-->
	  	  <!--代码修改_end Byjdy-->
    </td>
  </tr>
    <tr align="center" class="2" id="2_156">
    <td align="left" class="first-cell" >
            <img src="images/menu_minus.gif" id="icon_2_156" width="9" height="9" border="0" style="margin-left:2em" onclick="rowClicked(this)" />
            <span>
      <!-- 0-默认列表 1-搜索匹配列表 其他-搜索未匹配列表 -->
            <a href="goods.php?act=list&cat_id=156&supp=-1">电池</a>
            </span>
        </td>
    <td width="10%">0</td>
    <td width="10%"><span onclick="listTable.edit(this, 'edit_measure_unit', 156)"><!--  -->&nbsp;&nbsp;&nbsp;&nbsp;<!--  --></span></td>
    <td width="10%"><img src="images/no.gif" onclick="listTable.toggle(this, 'toggle_show_in_nav', 156)" /></td>
    <td width="10%"><img src="images/yes.gif" onclick="listTable.toggle(this, 'toggle_is_show', 156)" /></td>
    <td><span onclick="listTable.edit(this, 'edit_grade', 156)">0</span></td>
    <td width="10%" align="right"><span onclick="listTable.edit(this, 'edit_sort_order', 156)">50</span></td>
    <td width="24%" align="center">
      <a href="category.php?act=move&cat_id=156">转移商品</a> |
      <a href="category.php?act=edit&amp;cat_id=156">编辑</a> |
      <a href="javascript:;" onclick="listTable.remove(156, '您确认要删除这条记录吗?')" title="移除">移除</a>
	  <!--代码修改_start Byjdy-->
	  	  <!--代码修改_end Byjdy-->
    </td>
  </tr>
    <tr align="center" class="1" id="1_146">
    <td align="left" class="first-cell" >
            <img src="images/menu_minus.gif" id="icon_1_146" width="9" height="9" border="0" style="margin-left:1em" onclick="rowClicked(this)" />
            <span>
      <!-- 0-默认列表 1-搜索匹配列表 其他-搜索未匹配列表 -->
            <a href="goods.php?act=list&cat_id=146&supp=-1">数码影音</a>
            </span>
        </td>
    <td width="10%">8</td>
    <td width="10%"><span onclick="listTable.edit(this, 'edit_measure_unit', 146)"><!--  -->&nbsp;&nbsp;&nbsp;&nbsp;<!--  --></span></td>
    <td width="10%"><img src="images/no.gif" onclick="listTable.toggle(this, 'toggle_show_in_nav', 146)" /></td>
    <td width="10%"><img src="images/yes.gif" onclick="listTable.toggle(this, 'toggle_is_show', 146)" /></td>
    <td><span onclick="listTable.edit(this, 'edit_grade', 146)">0</span></td>
    <td width="10%" align="right"><span onclick="listTable.edit(this, 'edit_sort_order', 146)">50</span></td>
    <td width="24%" align="center">
      <a href="category.php?act=move&cat_id=146">转移商品</a> |
      <a href="category.php?act=edit&amp;cat_id=146">编辑</a> |
      <a href="javascript:;" onclick="listTable.remove(146, '您确认要删除这条记录吗?')" title="移除">移除</a>
	  <!--代码修改_start Byjdy-->
	  	  <!--代码修改_end Byjdy-->
    </td>
  </tr>
    <tr align="center" class="2" id="2_176">
    <td align="left" class="first-cell" >
            <img src="images/menu_minus.gif" id="icon_2_176" width="9" height="9" border="0" style="margin-left:2em" onclick="rowClicked(this)" />
            <span>
      <!-- 0-默认列表 1-搜索匹配列表 其他-搜索未匹配列表 -->
            <a href="goods.php?act=list&cat_id=176&supp=-1">数码相框</a>
            </span>
        </td>
    <td width="10%">0</td>
    <td width="10%"><span onclick="listTable.edit(this, 'edit_measure_unit', 176)"><!--  -->&nbsp;&nbsp;&nbsp;&nbsp;<!--  --></span></td>
    <td width="10%"><img src="images/no.gif" onclick="listTable.toggle(this, 'toggle_show_in_nav', 176)" /></td>
    <td width="10%"><img src="images/yes.gif" onclick="listTable.toggle(this, 'toggle_is_show', 176)" /></td>
    <td><span onclick="listTable.edit(this, 'edit_grade', 176)">0</span></td>
    <td width="10%" align="right"><span onclick="listTable.edit(this, 'edit_sort_order', 176)">50</span></td>
    <td width="24%" align="center">
      <a href="category.php?act=move&cat_id=176">转移商品</a> |
      <a href="category.php?act=edit&amp;cat_id=176">编辑</a> |
      <a href="javascript:;" onclick="listTable.remove(176, '您确认要删除这条记录吗?')" title="移除">移除</a>
	  <!--代码修改_start Byjdy-->
	  	  <!--代码修改_end Byjdy-->
    </td>
  </tr>
    <tr align="center" class="2" id="2_173">
    <td align="left" class="first-cell" >
            <img src="images/menu_minus.gif" id="icon_2_173" width="9" height="9" border="0" style="margin-left:2em" onclick="rowClicked(this)" />
            <span>
      <!-- 0-默认列表 1-搜索匹配列表 其他-搜索未匹配列表 -->
            <a href="goods.php?act=list&cat_id=173&supp=-1">运动相机</a>
            </span>
        </td>
    <td width="10%">0</td>
    <td width="10%"><span onclick="listTable.edit(this, 'edit_measure_unit', 173)"><!--  -->&nbsp;&nbsp;&nbsp;&nbsp;<!--  --></span></td>
    <td width="10%"><img src="images/no.gif" onclick="listTable.toggle(this, 'toggle_show_in_nav', 173)" /></td>
    <td width="10%"><img src="images/yes.gif" onclick="listTable.toggle(this, 'toggle_is_show', 173)" /></td>
    <td><span onclick="listTable.edit(this, 'edit_grade', 173)">0</span></td>
    <td width="10%" align="right"><span onclick="listTable.edit(this, 'edit_sort_order', 173)">50</span></td>
    <td width="24%" align="center">
      <a href="category.php?act=move&cat_id=173">转移商品</a> |
      <a href="category.php?act=edit&amp;cat_id=173">编辑</a> |
      <a href="javascript:;" onclick="listTable.remove(173, '您确认要删除这条记录吗?')" title="移除">移除</a>
	  <!--代码修改_start Byjdy-->
	  	  <!--代码修改_end Byjdy-->
    </td>
  </tr>
    <tr align="center" class="2" id="2_170">
    <td align="left" class="first-cell" >
            <img src="images/menu_minus.gif" id="icon_2_170" width="9" height="9" border="0" style="margin-left:2em" onclick="rowClicked(this)" />
            <span>
      <!-- 0-默认列表 1-搜索匹配列表 其他-搜索未匹配列表 -->
            <a href="goods.php?act=list&cat_id=170&supp=-1">数码相机</a>
            </span>
        </td>
    <td width="10%">0</td>
    <td width="10%"><span onclick="listTable.edit(this, 'edit_measure_unit', 170)"><!--  -->&nbsp;&nbsp;&nbsp;&nbsp;<!--  --></span></td>
    <td width="10%"><img src="images/no.gif" onclick="listTable.toggle(this, 'toggle_show_in_nav', 170)" /></td>
    <td width="10%"><img src="images/yes.gif" onclick="listTable.toggle(this, 'toggle_is_show', 170)" /></td>
    <td><span onclick="listTable.edit(this, 'edit_grade', 170)">0</span></td>
    <td width="10%" align="right"><span onclick="listTable.edit(this, 'edit_sort_order', 170)">50</span></td>
    <td width="24%" align="center">
      <a href="category.php?act=move&cat_id=170">转移商品</a> |
      <a href="category.php?act=edit&amp;cat_id=170">编辑</a> |
      <a href="javascript:;" onclick="listTable.remove(170, '您确认要删除这条记录吗?')" title="移除">移除</a>
	  <!--代码修改_start Byjdy-->
	  	  <!--代码修改_end Byjdy-->
    </td>
  </tr>
    <tr align="center" class="2" id="2_177">
    <td align="left" class="first-cell" >
            <img src="images/menu_minus.gif" id="icon_2_177" width="9" height="9" border="0" style="margin-left:2em" onclick="rowClicked(this)" />
            <span>
      <!-- 0-默认列表 1-搜索匹配列表 其他-搜索未匹配列表 -->
            <a href="goods.php?act=list&cat_id=177&supp=-1">影棚器材</a>
            </span>
        </td>
    <td width="10%">0</td>
    <td width="10%"><span onclick="listTable.edit(this, 'edit_measure_unit', 177)"><!--  -->&nbsp;&nbsp;&nbsp;&nbsp;<!--  --></span></td>
    <td width="10%"><img src="images/no.gif" onclick="listTable.toggle(this, 'toggle_show_in_nav', 177)" /></td>
    <td width="10%"><img src="images/yes.gif" onclick="listTable.toggle(this, 'toggle_is_show', 177)" /></td>
    <td><span onclick="listTable.edit(this, 'edit_grade', 177)">0</span></td>
    <td width="10%" align="right"><span onclick="listTable.edit(this, 'edit_sort_order', 177)">50</span></td>
    <td width="24%" align="center">
      <a href="category.php?act=move&cat_id=177">转移商品</a> |
      <a href="category.php?act=edit&amp;cat_id=177">编辑</a> |
      <a href="javascript:;" onclick="listTable.remove(177, '您确认要删除这条记录吗?')" title="移除">移除</a>
	  <!--代码修改_start Byjdy-->
	  	  <!--代码修改_end Byjdy-->
    </td>
  </tr>
    <tr align="center" class="2" id="2_174">
    <td align="left" class="first-cell" >
            <img src="images/menu_minus.gif" id="icon_2_174" width="9" height="9" border="0" style="margin-left:2em" onclick="rowClicked(this)" />
            <span>
      <!-- 0-默认列表 1-搜索匹配列表 其他-搜索未匹配列表 -->
            <a href="goods.php?act=list&cat_id=174&supp=-1">摄像机</a>
            </span>
        </td>
    <td width="10%">0</td>
    <td width="10%"><span onclick="listTable.edit(this, 'edit_measure_unit', 174)"><!--  -->&nbsp;&nbsp;&nbsp;&nbsp;<!--  --></span></td>
    <td width="10%"><img src="images/no.gif" onclick="listTable.toggle(this, 'toggle_show_in_nav', 174)" /></td>
    <td width="10%"><img src="images/yes.gif" onclick="listTable.toggle(this, 'toggle_is_show', 174)" /></td>
    <td><span onclick="listTable.edit(this, 'edit_grade', 174)">0</span></td>
    <td width="10%" align="right"><span onclick="listTable.edit(this, 'edit_sort_order', 174)">50</span></td>
    <td width="24%" align="center">
      <a href="category.php?act=move&cat_id=174">转移商品</a> |
      <a href="category.php?act=edit&amp;cat_id=174">编辑</a> |
      <a href="javascript:;" onclick="listTable.remove(174, '您确认要删除这条记录吗?')" title="移除">移除</a>
	  <!--代码修改_start Byjdy-->
	  	  <!--代码修改_end Byjdy-->
    </td>
  </tr>
    <tr align="center" class="2" id="2_171">
    <td align="left" class="first-cell" >
            <img src="images/menu_minus.gif" id="icon_2_171" width="9" height="9" border="0" style="margin-left:2em" onclick="rowClicked(this)" />
            <span>
      <!-- 0-默认列表 1-搜索匹配列表 其他-搜索未匹配列表 -->
            <a href="goods.php?act=list&cat_id=171&supp=-1">单反相机</a>
            </span>
        </td>
    <td width="10%">0</td>
    <td width="10%"><span onclick="listTable.edit(this, 'edit_measure_unit', 171)"><!--  -->&nbsp;&nbsp;&nbsp;&nbsp;<!--  --></span></td>
    <td width="10%"><img src="images/no.gif" onclick="listTable.toggle(this, 'toggle_show_in_nav', 171)" /></td>
    <td width="10%"><img src="images/yes.gif" onclick="listTable.toggle(this, 'toggle_is_show', 171)" /></td>
    <td><span onclick="listTable.edit(this, 'edit_grade', 171)">0</span></td>
    <td width="10%" align="right"><span onclick="listTable.edit(this, 'edit_sort_order', 171)">50</span></td>
    <td width="24%" align="center">
      <a href="category.php?act=move&cat_id=171">转移商品</a> |
      <a href="category.php?act=edit&amp;cat_id=171">编辑</a> |
      <a href="javascript:;" onclick="listTable.remove(171, '您确认要删除这条记录吗?')" title="移除">移除</a>
	  <!--代码修改_start Byjdy-->
	  	  <!--代码修改_end Byjdy-->
    </td>
  </tr>
    <tr align="center" class="2" id="2_175">
    <td align="left" class="first-cell" >
            <img src="images/menu_minus.gif" id="icon_2_175" width="9" height="9" border="0" style="margin-left:2em" onclick="rowClicked(this)" />
            <span>
      <!-- 0-默认列表 1-搜索匹配列表 其他-搜索未匹配列表 -->
            <a href="goods.php?act=list&cat_id=175&supp=-1">户外器材</a>
            </span>
        </td>
    <td width="10%">0</td>
    <td width="10%"><span onclick="listTable.edit(this, 'edit_measure_unit', 175)"><!--  -->&nbsp;&nbsp;&nbsp;&nbsp;<!--  --></span></td>
    <td width="10%"><img src="images/no.gif" onclick="listTable.toggle(this, 'toggle_show_in_nav', 175)" /></td>
    <td width="10%"><img src="images/yes.gif" onclick="listTable.toggle(this, 'toggle_is_show', 175)" /></td>
    <td><span onclick="listTable.edit(this, 'edit_grade', 175)">0</span></td>
    <td width="10%" align="right"><span onclick="listTable.edit(this, 'edit_sort_order', 175)">50</span></td>
    <td width="24%" align="center">
      <a href="category.php?act=move&cat_id=175">转移商品</a> |
      <a href="category.php?act=edit&amp;cat_id=175">编辑</a> |
      <a href="javascript:;" onclick="listTable.remove(175, '您确认要删除这条记录吗?')" title="移除">移除</a>
	  <!--代码修改_start Byjdy-->
	  	  <!--代码修改_end Byjdy-->
    </td>
  </tr>
    <tr align="center" class="2" id="2_172">
    <td align="left" class="first-cell" >
            <img src="images/menu_minus.gif" id="icon_2_172" width="9" height="9" border="0" style="margin-left:2em" onclick="rowClicked(this)" />
            <span>
      <!-- 0-默认列表 1-搜索匹配列表 其他-搜索未匹配列表 -->
            <a href="goods.php?act=list&cat_id=172&supp=-1">拍立得</a>
            </span>
        </td>
    <td width="10%">0</td>
    <td width="10%"><span onclick="listTable.edit(this, 'edit_measure_unit', 172)"><!--  -->&nbsp;&nbsp;&nbsp;&nbsp;<!--  --></span></td>
    <td width="10%"><img src="images/no.gif" onclick="listTable.toggle(this, 'toggle_show_in_nav', 172)" /></td>
    <td width="10%"><img src="images/yes.gif" onclick="listTable.toggle(this, 'toggle_is_show', 172)" /></td>
    <td><span onclick="listTable.edit(this, 'edit_grade', 172)">0</span></td>
    <td width="10%" align="right"><span onclick="listTable.edit(this, 'edit_sort_order', 172)">50</span></td>
    <td width="24%" align="center">
      <a href="category.php?act=move&cat_id=172">转移商品</a> |
      <a href="category.php?act=edit&amp;cat_id=172">编辑</a> |
      <a href="javascript:;" onclick="listTable.remove(172, '您确认要删除这条记录吗?')" title="移除">移除</a>
	  <!--代码修改_start Byjdy-->
	  	  <!--代码修改_end Byjdy-->
    </td>
  </tr>
    <tr align="center" class="0" id="0_358">
    <td align="left" class="first-cell" >
            <img src="images/menu_minus.gif" id="icon_0_358" width="9" height="9" border="0" style="margin-left:0em" onclick="rowClicked(this)" />
            <span>
      <!-- 0-默认列表 1-搜索匹配列表 其他-搜索未匹配列表 -->
            <a href="goods.php?act=list&cat_id=358&supp=-1">电脑、办公</a>
            </span>
        </td>
    <td width="10%">0</td>
    <td width="10%"><span onclick="listTable.edit(this, 'edit_measure_unit', 358)"><!--  -->&nbsp;&nbsp;&nbsp;&nbsp;<!--  --></span></td>
    <td width="10%"><img src="images/no.gif" onclick="listTable.toggle(this, 'toggle_show_in_nav', 358)" /></td>
    <td width="10%"><img src="images/yes.gif" onclick="listTable.toggle(this, 'toggle_is_show', 358)" /></td>
    <td><span onclick="listTable.edit(this, 'edit_grade', 358)">0</span></td>
    <td width="10%" align="right"><span onclick="listTable.edit(this, 'edit_sort_order', 358)">3</span></td>
    <td width="24%" align="center">
      <a href="category.php?act=move&cat_id=358">转移商品</a> |
      <a href="category.php?act=edit&amp;cat_id=358">编辑</a> |
      <a href="javascript:;" onclick="listTable.remove(358, '您确认要删除这条记录吗?')" title="移除">移除</a>
	  <!--代码修改_start Byjdy-->
	  	  | <a href="category_flashimg.php?act=list&amp;cat_id=358">设置轮播图片</a>	  
	  	  <!--代码修改_end Byjdy-->
    </td>
  </tr>
    <tr align="center" class="0" id="0_6">
    <td align="left" class="first-cell" >
            <img src="images/menu_minus.gif" id="icon_0_6" width="9" height="9" border="0" style="margin-left:0em" onclick="rowClicked(this)" />
            <span>
      <!-- 0-默认列表 1-搜索匹配列表 其他-搜索未匹配列表 -->
            <a href="goods.php?act=list&cat_id=6&supp=-1">家居、家具、家装、厨具</a>
            </span>

        </td>
    <td width="10%">0</td>
    <td width="10%"><span onclick="listTable.edit(this, 'edit_measure_unit', 6)"><!--  -->&nbsp;&nbsp;&nbsp;&nbsp;<!--  --></span></td>
    <td width="10%"><img src="images/no.gif" onclick="listTable.toggle(this, 'toggle_show_in_nav', 6)" /></td>
    <td width="10%"><img src="images/yes.gif" onclick="listTable.toggle(this, 'toggle_is_show', 6)" /></td>
    <td><span onclick="listTable.edit(this, 'edit_grade', 6)">5</span></td>
    <td width="10%" align="right"><span onclick="listTable.edit(this, 'edit_sort_order', 6)">4</span></td>
    <td width="24%" align="center">
      <a href="category.php?act=move&cat_id=6">转移商品</a> |
      <a href="category.php?act=edit&amp;cat_id=6">编辑</a> |
      <a href="javascript:;" onclick="listTable.remove(6, '您确认要删除这条记录吗?')" title="移除">移除</a>
	  <!--代码修改_start Byjdy-->
	  	  | <a href="category_flashimg.php?act=list&amp;cat_id=6">设置轮播图片</a>	  
	  	  <!--代码修改_end Byjdy-->
    </td>
  </tr>
    <tr align="center" class="1" id="1_310">
    <td align="left" class="first-cell" >
            <img src="images/menu_minus.gif" id="icon_1_310" width="9" height="9" border="0" style="margin-left:1em" onclick="rowClicked(this)" />
            <span>
      <!-- 0-默认列表 1-搜索匹配列表 其他-搜索未匹配列表 -->
            <a href="goods.php?act=list&cat_id=310&supp=-1">家装软饰</a>
            </span>
        </td>
    <td width="10%">0</td>
    <td width="10%"><span onclick="listTable.edit(this, 'edit_measure_unit', 310)"><!--  -->&nbsp;&nbsp;&nbsp;&nbsp;<!--  --></span></td>
    <td width="10%"><img src="images/no.gif" onclick="listTable.toggle(this, 'toggle_show_in_nav', 310)" /></td>
    <td width="10%"><img src="images/yes.gif" onclick="listTable.toggle(this, 'toggle_is_show', 310)" /></td>
    <td><span onclick="listTable.edit(this, 'edit_grade', 310)">0</span></td>
    <td width="10%" align="right"><span onclick="listTable.edit(this, 'edit_sort_order', 310)">50</span></td>
    <td width="24%" align="center">
      <a href="category.php?act=move&cat_id=310">转移商品</a> |
      <a href="category.php?act=edit&amp;cat_id=310">编辑</a> |
      <a href="javascript:;" onclick="listTable.remove(310, '您确认要删除这条记录吗?')" title="移除">移除</a>
	  <!--代码修改_start Byjdy-->
	  	  <!--代码修改_end Byjdy-->
    </td>
  </tr>
    <tr align="center" class="2" id="2_348">
    <td align="left" class="first-cell" >
            <img src="images/menu_minus.gif" id="icon_2_348" width="9" height="9" border="0" style="margin-left:2em" onclick="rowClicked(this)" />
            <span>
      <!-- 0-默认列表 1-搜索匹配列表 其他-搜索未匹配列表 -->
            <a href="goods.php?act=list&cat_id=348&supp=-1">墙贴/装饰贴</a>
            </span>
        </td>
    <td width="10%">0</td>
    <td width="10%"><span onclick="listTable.edit(this, 'edit_measure_unit', 348)"><!--  -->&nbsp;&nbsp;&nbsp;&nbsp;<!--  --></span></td>
    <td width="10%"><img src="images/no.gif" onclick="listTable.toggle(this, 'toggle_show_in_nav', 348)" /></td>
    <td width="10%"><img src="images/yes.gif" onclick="listTable.toggle(this, 'toggle_is_show', 348)" /></td>
    <td><span onclick="listTable.edit(this, 'edit_grade', 348)">0</span></td>
    <td width="10%" align="right"><span onclick="listTable.edit(this, 'edit_sort_order', 348)">50</span></td>
    <td width="24%" align="center">
      <a href="category.php?act=move&cat_id=348">转移商品</a> |
      <a href="category.php?act=edit&amp;cat_id=348">编辑</a> |
      <a href="javascript:;" onclick="listTable.remove(348, '您确认要删除这条记录吗?')" title="移除">移除</a>
	  <!--代码修改_start Byjdy-->
	  	  <!--代码修改_end Byjdy-->
    </td>
  </tr>
    <tr align="center" class="2" id="2_345">
    <td align="left" class="first-cell" >
            <img src="images/menu_minus.gif" id="icon_2_345" width="9" height="9" border="0" style="margin-left:2em" onclick="rowClicked(this)" />
            <span>
      <!-- 0-默认列表 1-搜索匹配列表 其他-搜索未匹配列表 -->
            <a href="goods.php?act=list&cat_id=345&supp=-1">帘艺隔断</a>
            </span>
        </td>
    <td width="10%">0</td>
    <td width="10%"><span onclick="listTable.edit(this, 'edit_measure_unit', 345)"><!--  -->&nbsp;&nbsp;&nbsp;&nbsp;<!--  --></span></td>
    <td width="10%"><img src="images/no.gif" onclick="listTable.toggle(this, 'toggle_show_in_nav', 345)" /></td>
    <td width="10%"><img src="images/yes.gif" onclick="listTable.toggle(this, 'toggle_is_show', 345)" /></td>
    <td><span onclick="listTable.edit(this, 'edit_grade', 345)">0</span></td>
    <td width="10%" align="right"><span onclick="listTable.edit(this, 'edit_sort_order', 345)">50</span></td>
    <td width="24%" align="center">
      <a href="category.php?act=move&cat_id=345">转移商品</a> |
      <a href="category.php?act=edit&amp;cat_id=345">编辑</a> |
      <a href="javascript:;" onclick="listTable.remove(345, '您确认要删除这条记录吗?')" title="移除">移除</a>
	  <!--代码修改_start Byjdy-->
	  	  <!--代码修改_end Byjdy-->
    </td>
  </tr>
    <tr align="center" class="2" id="2_342">
    <td align="left" class="first-cell" >
            <img src="images/menu_minus.gif" id="icon_2_342" width="9" height="9" border="0" style="margin-left:2em" onclick="rowClicked(this)" />
            <span>
      <!-- 0-默认列表 1-搜索匹配列表 其他-搜索未匹配列表 -->
            <a href="goods.php?act=list&cat_id=342&supp=-1">桌布/罩件</a>
            </span>
        </td>
    <td width="10%">0</td>
    <td width="10%"><span onclick="listTable.edit(this, 'edit_measure_unit', 342)"><!--  -->&nbsp;&nbsp;&nbsp;&nbsp;<!--  --></span></td>
    <td width="10%"><img src="images/no.gif" onclick="listTable.toggle(this, 'toggle_show_in_nav', 342)" /></td>
    <td width="10%"><img src="images/yes.gif" onclick="listTable.toggle(this, 'toggle_is_show', 342)" /></td>
    <td><span onclick="listTable.edit(this, 'edit_grade', 342)">0</span></td>
    <td width="10%" align="right"><span onclick="listTable.edit(this, 'edit_sort_order', 342)">50</span></td>
    <td width="24%" align="center">
      <a href="category.php?act=move&cat_id=342">转移商品</a> |
      <a href="category.php?act=edit&amp;cat_id=342">编辑</a> |
      <a href="javascript:;" onclick="listTable.remove(342, '您确认要删除这条记录吗?')" title="移除">移除</a>
	  <!--代码修改_start Byjdy-->
	  	  <!--代码修改_end Byjdy-->
    </td>
  </tr>
    <tr align="center" class="2" id="2_349">
    <td align="left" class="first-cell" >
            <img src="images/menu_minus.gif" id="icon_2_349" width="9" height="9" border="0" style="margin-left:2em" onclick="rowClicked(this)" />
            <span>
      <!-- 0-默认列表 1-搜索匹配列表 其他-搜索未匹配列表 -->
            <a href="goods.php?act=list&cat_id=349&supp=-1">摆件花瓶</a>
            </span>
        </td>
    <td width="10%">0</td>
    <td width="10%"><span onclick="listTable.edit(this, 'edit_measure_unit', 349)"><!--  -->&nbsp;&nbsp;&nbsp;&nbsp;<!--  --></span></td>
    <td width="10%"><img src="images/no.gif" onclick="listTable.toggle(this, 'toggle_show_in_nav', 349)" /></td>
    <td width="10%"><img src="images/yes.gif" onclick="listTable.toggle(this, 'toggle_is_show', 349)" /></td>
    <td><span onclick="listTable.edit(this, 'edit_grade', 349)">0</span></td>
    <td width="10%" align="right"><span onclick="listTable.edit(this, 'edit_sort_order', 349)">50</span></td>
    <td width="24%" align="center">
      <a href="category.php?act=move&cat_id=349">转移商品</a> |
      <a href="category.php?act=edit&amp;cat_id=349">编辑</a> |
      <a href="javascript:;" onclick="listTable.remove(349, '您确认要删除这条记录吗?')" title="移除">移除</a>
	  <!--代码修改_start Byjdy-->
	  	  <!--代码修改_end Byjdy-->
    </td>
  </tr>
    <tr align="center" class="2" id="2_346">
    <td align="left" class="first-cell" >
            <img src="images/menu_minus.gif" id="icon_2_346" width="9" height="9" border="0" style="margin-left:2em" onclick="rowClicked(this)" />
            <span>
      <!-- 0-默认列表 1-搜索匹配列表 其他-搜索未匹配列表 -->
            <a href="goods.php?act=list&cat_id=346&supp=-1">相框/照片墙</a>
            </span>
        </td>
    <td width="10%">0</td>
    <td width="10%"><span onclick="listTable.edit(this, 'edit_measure_unit', 346)"><!--  -->&nbsp;&nbsp;&nbsp;&nbsp;<!--  --></span></td>
    <td width="10%"><img src="images/no.gif" onclick="listTable.toggle(this, 'toggle_show_in_nav', 346)" /></td>
    <td width="10%"><img src="images/yes.gif" onclick="listTable.toggle(this, 'toggle_is_show', 346)" /></td>
    <td><span onclick="listTable.edit(this, 'edit_grade', 346)">0</span></td>
    <td width="10%" align="right"><span onclick="listTable.edit(this, 'edit_sort_order', 346)">50</span></td>
    <td width="24%" align="center">
      <a href="category.php?act=move&cat_id=346">转移商品</a> |
      <a href="category.php?act=edit&amp;cat_id=346">编辑</a> |
      <a href="javascript:;" onclick="listTable.remove(346, '您确认要删除这条记录吗?')" title="移除">移除</a>
	  <!--代码修改_start Byjdy-->
	  	  <!--代码修改_end Byjdy-->
    </td>
  </tr>
    <tr align="center" class="2" id="2_343">
    <td align="left" class="first-cell" >
            <img src="images/menu_minus.gif" id="icon_2_343" width="9" height="9" border="0" style="margin-left:2em" onclick="rowClicked(this)" />
            <span>
      <!-- 0-默认列表 1-搜索匹配列表 其他-搜索未匹配列表 -->
            <a href="goods.php?act=list&cat_id=343&supp=-1">地毯地垫</a>
            </span>
        </td>
    <td width="10%">0</td>
    <td width="10%"><span onclick="listTable.edit(this, 'edit_measure_unit', 343)"><!--  -->&nbsp;&nbsp;&nbsp;&nbsp;<!--  --></span></td>
    <td width="10%"><img src="images/no.gif" onclick="listTable.toggle(this, 'toggle_show_in_nav', 343)" /></td>
    <td width="10%"><img src="images/yes.gif" onclick="listTable.toggle(this, 'toggle_is_show', 343)" /></td>
    <td><span onclick="listTable.edit(this, 'edit_grade', 343)">0</span></td>
    <td width="10%" align="right"><span onclick="listTable.edit(this, 'edit_sort_order', 343)">50</span></td>
    <td width="24%" align="center">
      <a href="category.php?act=move&cat_id=343">转移商品</a> |
      <a href="category.php?act=edit&amp;cat_id=343">编辑</a> |
      <a href="javascript:;" onclick="listTable.remove(343, '您确认要删除这条记录吗?')" title="移除">移除</a>
	  <!--代码修改_start Byjdy-->
	  	  <!--代码修改_end Byjdy-->
    </td>
  </tr>
    <tr align="center" class="2" id="2_347">
    <td align="left" class="first-cell" >
            <img src="images/menu_minus.gif" id="icon_2_347" width="9" height="9" border="0" style="margin-left:2em" onclick="rowClicked(this)" />
            <span>
      <!-- 0-默认列表 1-搜索匹配列表 其他-搜索未匹配列表 -->
            <a href="goods.php?act=list&cat_id=347&supp=-1">装饰字画</a>
            </span>
        </td>
    <td width="10%">0</td>
    <td width="10%"><span onclick="listTable.edit(this, 'edit_measure_unit', 347)"><!--  -->&nbsp;&nbsp;&nbsp;&nbsp;<!--  --></span></td>
    <td width="10%"><img src="images/no.gif" onclick="listTable.toggle(this, 'toggle_show_in_nav', 347)" /></td>
    <td width="10%"><img src="images/yes.gif" onclick="listTable.toggle(this, 'toggle_is_show', 347)" /></td>
    <td><span onclick="listTable.edit(this, 'edit_grade', 347)">0</span></td>
    <td width="10%" align="right"><span onclick="listTable.edit(this, 'edit_sort_order', 347)">50</span></td>
    <td width="24%" align="center">
      <a href="category.php?act=move&cat_id=347">转移商品</a> |
      <a href="category.php?act=edit&amp;cat_id=347">编辑</a> |
      <a href="javascript:;" onclick="listTable.remove(347, '您确认要删除这条记录吗?')" title="移除">移除</a>
	  <!--代码修改_start Byjdy-->
	  	  <!--代码修改_end Byjdy-->
    </td>
  </tr>
    <tr align="center" class="2" id="2_344">
    <td align="left" class="first-cell" >
            <img src="images/menu_minus.gif" id="icon_2_344" width="9" height="9" border="0" style="margin-left:2em" onclick="rowClicked(this)" />
            <span>
      <!-- 0-默认列表 1-搜索匹配列表 其他-搜索未匹配列表 -->
            <a href="goods.php?act=list&cat_id=344&supp=-1">沙发垫套/椅垫</a>
            </span>
        </td>
    <td width="10%">0</td>
    <td width="10%"><span onclick="listTable.edit(this, 'edit_measure_unit', 344)"><!--  -->&nbsp;&nbsp;&nbsp;&nbsp;<!--  --></span></td>
    <td width="10%"><img src="images/no.gif" onclick="listTable.toggle(this, 'toggle_show_in_nav', 344)" /></td>
    <td width="10%"><img src="images/yes.gif" onclick="listTable.toggle(this, 'toggle_is_show', 344)" /></td>
    <td><span onclick="listTable.edit(this, 'edit_grade', 344)">0</span></td>
    <td width="10%" align="right"><span onclick="listTable.edit(this, 'edit_sort_order', 344)">50</span></td>
    <td width="24%" align="center">
      <a href="category.php?act=move&cat_id=344">转移商品</a> |
      <a href="category.php?act=edit&amp;cat_id=344">编辑</a> |
      <a href="javascript:;" onclick="listTable.remove(344, '您确认要删除这条记录吗?')" title="移除">移除</a>
	  <!--代码修改_start Byjdy-->
	  	  <!--代码修改_end Byjdy-->
    </td>
  </tr>
    <tr align="center" class="1" id="1_307">
    <td align="left" class="first-cell" >
            <img src="images/menu_minus.gif" id="icon_1_307" width="9" height="9" border="0" style="margin-left:1em" onclick="rowClicked(this)" />
            <span>
      <!-- 0-默认列表 1-搜索匹配列表 其他-搜索未匹配列表 -->
            <a href="goods.php?act=list&cat_id=307&supp=-1">家具</a>
            </span>
        </td>
    <td width="10%">8</td>
    <td width="10%"><span onclick="listTable.edit(this, 'edit_measure_unit', 307)"><!--  -->&nbsp;&nbsp;&nbsp;&nbsp;<!--  --></span></td>
    <td width="10%"><img src="images/no.gif" onclick="listTable.toggle(this, 'toggle_show_in_nav', 307)" /></td>
    <td width="10%"><img src="images/yes.gif" onclick="listTable.toggle(this, 'toggle_is_show', 307)" /></td>
    <td><span onclick="listTable.edit(this, 'edit_grade', 307)">0</span></td>
    <td width="10%" align="right"><span onclick="listTable.edit(this, 'edit_sort_order', 307)">50</span></td>
    <td width="24%" align="center">
      <a href="category.php?act=move&cat_id=307">转移商品</a> |
      <a href="category.php?act=edit&amp;cat_id=307">编辑</a> |
      <a href="javascript:;" onclick="listTable.remove(307, '您确认要删除这条记录吗?')" title="移除">移除</a>
	  <!--代码修改_start Byjdy-->
	  	  <!--代码修改_end Byjdy-->
    </td>
  </tr>
    <tr align="center" class="2" id="2_322">
    <td align="left" class="first-cell" >
            <img src="images/menu_minus.gif" id="icon_2_322" width="9" height="9" border="0" style="margin-left:2em" onclick="rowClicked(this)" />
            <span>
      <!-- 0-默认列表 1-搜索匹配列表 其他-搜索未匹配列表 -->
            <a href="goods.php?act=list&cat_id=322&supp=-1">餐厅家具</a>
            </span>
        </td>
    <td width="10%">0</td>
    <td width="10%"><span onclick="listTable.edit(this, 'edit_measure_unit', 322)"><!--  -->&nbsp;&nbsp;&nbsp;&nbsp;<!--  --></span></td>
    <td width="10%"><img src="images/no.gif" onclick="listTable.toggle(this, 'toggle_show_in_nav', 322)" /></td>
    <td width="10%"><img src="images/yes.gif" onclick="listTable.toggle(this, 'toggle_is_show', 322)" /></td>
    <td><span onclick="listTable.edit(this, 'edit_grade', 322)">0</span></td>
    <td width="10%" align="right"><span onclick="listTable.edit(this, 'edit_sort_order', 322)">50</span></td>
    <td width="24%" align="center">
      <a href="category.php?act=move&cat_id=322">转移商品</a> |
      <a href="category.php?act=edit&amp;cat_id=322">编辑</a> |
      <a href="javascript:;" onclick="listTable.remove(322, '您确认要删除这条记录吗?')" title="移除">移除</a>
	  <!--代码修改_start Byjdy-->
	  	  <!--代码修改_end Byjdy-->
    </td>
  </tr>
    <tr align="center" class="2" id="2_326">
    <td align="left" class="first-cell" >
            <img src="images/menu_minus.gif" id="icon_2_326" width="9" height="9" border="0" style="margin-left:2em" onclick="rowClicked(this)" />
            <span>
      <!-- 0-默认列表 1-搜索匹配列表 其他-搜索未匹配列表 -->
            <a href="goods.php?act=list&cat_id=326&supp=-1">沙发</a>
            </span>
        </td>
    <td width="10%">0</td>
    <td width="10%"><span onclick="listTable.edit(this, 'edit_measure_unit', 326)"><!--  -->&nbsp;&nbsp;&nbsp;&nbsp;<!--  --></span></td>
    <td width="10%"><img src="images/no.gif" onclick="listTable.toggle(this, 'toggle_show_in_nav', 326)" /></td>
    <td width="10%"><img src="images/yes.gif" onclick="listTable.toggle(this, 'toggle_is_show', 326)" /></td>
    <td><span onclick="listTable.edit(this, 'edit_grade', 326)">0</span></td>
    <td width="10%" align="right"><span onclick="listTable.edit(this, 'edit_sort_order', 326)">50</span></td>
    <td width="24%" align="center">
      <a href="category.php?act=move&cat_id=326">转移商品</a> |
      <a href="category.php?act=edit&amp;cat_id=326">编辑</a> |
      <a href="javascript:;" onclick="listTable.remove(326, '您确认要删除这条记录吗?')" title="移除">移除</a>
	  <!--代码修改_start Byjdy-->
	  	  <!--代码修改_end Byjdy-->
    </td>
  </tr>
    <tr align="center" class="2" id="2_323">
    <td align="left" class="first-cell" >
            <img src="images/menu_minus.gif" id="icon_2_323" width="9" height="9" border="0" style="margin-left:2em" onclick="rowClicked(this)" />
            <span>
      <!-- 0-默认列表 1-搜索匹配列表 其他-搜索未匹配列表 -->
            <a href="goods.php?act=list&cat_id=323&supp=-1">书房家具</a>
            </span>
        </td>
    <td width="10%">0</td>
    <td width="10%"><span onclick="listTable.edit(this, 'edit_measure_unit', 323)"><!--  -->&nbsp;&nbsp;&nbsp;&nbsp;<!--  --></span></td>
    <td width="10%"><img src="images/no.gif" onclick="listTable.toggle(this, 'toggle_show_in_nav', 323)" /></td>
    <td width="10%"><img src="images/yes.gif" onclick="listTable.toggle(this, 'toggle_is_show', 323)" /></td>
    <td><span onclick="listTable.edit(this, 'edit_grade', 323)">0</span></td>
    <td width="10%" align="right"><span onclick="listTable.edit(this, 'edit_sort_order', 323)">50</span></td>
    <td width="24%" align="center">
      <a href="category.php?act=move&cat_id=323">转移商品</a> |
      <a href="category.php?act=edit&amp;cat_id=323">编辑</a> |
      <a href="javascript:;" onclick="listTable.remove(323, '您确认要删除这条记录吗?')" title="移除">移除</a>
	  <!--代码修改_start Byjdy-->
	  	  <!--代码修改_end Byjdy-->
    </td>
  </tr>
    <tr align="center" class="2" id="2_320">
    <td align="left" class="first-cell" >
            <img src="images/menu_minus.gif" id="icon_2_320" width="9" height="9" border="0" style="margin-left:2em" onclick="rowClicked(this)" />
            <span>
      <!-- 0-默认列表 1-搜索匹配列表 其他-搜索未匹配列表 -->
            <a href="goods.php?act=list&cat_id=320&supp=-1">卧室家具</a>
            </span>
        </td>
    <td width="10%">0</td>
    <td width="10%"><span onclick="listTable.edit(this, 'edit_measure_unit', 320)"><!--  -->&nbsp;&nbsp;&nbsp;&nbsp;<!--  --></span></td>
    <td width="10%"><img src="images/no.gif" onclick="listTable.toggle(this, 'toggle_show_in_nav', 320)" /></td>
    <td width="10%"><img src="images/yes.gif" onclick="listTable.toggle(this, 'toggle_is_show', 320)" /></td>
    <td><span onclick="listTable.edit(this, 'edit_grade', 320)">0</span></td>
    <td width="10%" align="right"><span onclick="listTable.edit(this, 'edit_sort_order', 320)">50</span></td>
    <td width="24%" align="center">
      <a href="category.php?act=move&cat_id=320">转移商品</a> |
      <a href="category.php?act=edit&amp;cat_id=320">编辑</a> |
      <a href="javascript:;" onclick="listTable.remove(320, '您确认要删除这条记录吗?')" title="移除">移除</a>
	  <!--代码修改_start Byjdy-->
	  	  <!--代码修改_end Byjdy-->
    </td>
  </tr>
    <tr align="center" class="2" id="2_327">
    <td align="left" class="first-cell" >
            <img src="images/menu_minus.gif" id="icon_2_327" width="9" height="9" border="0" style="margin-left:2em" onclick="rowClicked(this)" />
            <span>
      <!-- 0-默认列表 1-搜索匹配列表 其他-搜索未匹配列表 -->
            <a href="goods.php?act=list&cat_id=327&supp=-1">鞋架/衣帽架</a>
            </span>
        </td>
    <td width="10%">0</td>
    <td width="10%"><span onclick="listTable.edit(this, 'edit_measure_unit', 327)"><!--  -->&nbsp;&nbsp;&nbsp;&nbsp;<!--  --></span></td>
    <td width="10%"><img src="images/no.gif" onclick="listTable.toggle(this, 'toggle_show_in_nav', 327)" /></td>
    <td width="10%"><img src="images/yes.gif" onclick="listTable.toggle(this, 'toggle_is_show', 327)" /></td>
    <td><span onclick="listTable.edit(this, 'edit_grade', 327)">0</span></td>
    <td width="10%" align="right"><span onclick="listTable.edit(this, 'edit_sort_order', 327)">50</span></td>
    <td width="24%" align="center">
      <a href="category.php?act=move&cat_id=327">转移商品</a> |
      <a href="category.php?act=edit&amp;cat_id=327">编辑</a> |
      <a href="javascript:;" onclick="listTable.remove(327, '您确认要删除这条记录吗?')" title="移除">移除</a>
	  <!--代码修改_start Byjdy-->
	  	  <!--代码修改_end Byjdy-->
    </td>
  </tr>
    <tr align="center" class="2" id="2_324">
    <td align="left" class="first-cell" >
            <img src="images/menu_minus.gif" id="icon_2_324" width="9" height="9" border="0" style="margin-left:2em" onclick="rowClicked(this)" />
            <span>
      <!-- 0-默认列表 1-搜索匹配列表 其他-搜索未匹配列表 -->
            <a href="goods.php?act=list&cat_id=324&supp=-1">储物家具</a>
            </span>
        </td>
    <td width="10%">0</td>
    <td width="10%"><span onclick="listTable.edit(this, 'edit_measure_unit', 324)"><!--  -->&nbsp;&nbsp;&nbsp;&nbsp;<!--  --></span></td>
    <td width="10%"><img src="images/no.gif" onclick="listTable.toggle(this, 'toggle_show_in_nav', 324)" /></td>
    <td width="10%"><img src="images/yes.gif" onclick="listTable.toggle(this, 'toggle_is_show', 324)" /></td>
    <td><span onclick="listTable.edit(this, 'edit_grade', 324)">0</span></td>
    <td width="10%" align="right"><span onclick="listTable.edit(this, 'edit_sort_order', 324)">50</span></td>
    <td width="24%" align="center">
      <a href="category.php?act=move&cat_id=324">转移商品</a> |
      <a href="category.php?act=edit&amp;cat_id=324">编辑</a> |
      <a href="javascript:;" onclick="listTable.remove(324, '您确认要删除这条记录吗?')" title="移除">移除</a>
	  <!--代码修改_start Byjdy-->
	  	  <!--代码修改_end Byjdy-->
    </td>
  </tr>
    <tr align="center" class="2" id="2_321">
    <td align="left" class="first-cell" >
            <img src="images/menu_minus.gif" id="icon_2_321" width="9" height="9" border="0" style="margin-left:2em" onclick="rowClicked(this)" />
            <span>
      <!-- 0-默认列表 1-搜索匹配列表 其他-搜索未匹配列表 -->
            <a href="goods.php?act=list&cat_id=321&supp=-1">客厅家具</a>
            </span>
        </td>
    <td width="10%">0</td>
    <td width="10%"><span onclick="listTable.edit(this, 'edit_measure_unit', 321)"><!--  -->&nbsp;&nbsp;&nbsp;&nbsp;<!--  --></span></td>
    <td width="10%"><img src="images/no.gif" onclick="listTable.toggle(this, 'toggle_show_in_nav', 321)" /></td>
    <td width="10%"><img src="images/yes.gif" onclick="listTable.toggle(this, 'toggle_is_show', 321)" /></td>
    <td><span onclick="listTable.edit(this, 'edit_grade', 321)">0</span></td>
    <td width="10%" align="right"><span onclick="listTable.edit(this, 'edit_sort_order', 321)">50</span></td>
    <td width="24%" align="center">
      <a href="category.php?act=move&cat_id=321">转移商品</a> |
      <a href="category.php?act=edit&amp;cat_id=321">编辑</a> |
      <a href="javascript:;" onclick="listTable.remove(321, '您确认要删除这条记录吗?')" title="移除">移除</a>
	  <!--代码修改_start Byjdy-->
	  	  <!--代码修改_end Byjdy-->
    </td>
  </tr>
    <tr align="center" class="2" id="2_325">
    <td align="left" class="first-cell" >
            <img src="images/menu_minus.gif" id="icon_2_325" width="9" height="9" border="0" style="margin-left:2em" onclick="rowClicked(this)" />
            <span>
      <!-- 0-默认列表 1-搜索匹配列表 其他-搜索未匹配列表 -->
            <a href="goods.php?act=list&cat_id=325&supp=-1">阳台/户外</a>
            </span>
        </td>
    <td width="10%">0</td>
    <td width="10%"><span onclick="listTable.edit(this, 'edit_measure_unit', 325)"><!--  -->&nbsp;&nbsp;&nbsp;&nbsp;<!--  --></span></td>
    <td width="10%"><img src="images/no.gif" onclick="listTable.toggle(this, 'toggle_show_in_nav', 325)" /></td>
    <td width="10%"><img src="images/yes.gif" onclick="listTable.toggle(this, 'toggle_is_show', 325)" /></td>
    <td><span onclick="listTable.edit(this, 'edit_grade', 325)">0</span></td>
    <td width="10%" align="right"><span onclick="listTable.edit(this, 'edit_sort_order', 325)">50</span></td>
    <td width="24%" align="center">
      <a href="category.php?act=move&cat_id=325">转移商品</a> |
      <a href="category.php?act=edit&amp;cat_id=325">编辑</a> |
      <a href="javascript:;" onclick="listTable.remove(325, '您确认要删除这条记录吗?')" title="移除">移除</a>
	  <!--代码修改_start Byjdy-->
	  	  <!--代码修改_end Byjdy-->
    </td>
  </tr>
    <tr align="center" class="1" id="1_311">
    <td align="left" class="first-cell" >
            <img src="images/menu_minus.gif" id="icon_1_311" width="9" height="9" border="0" style="margin-left:1em" onclick="rowClicked(this)" />
            <span>
      <!-- 0-默认列表 1-搜索匹配列表 其他-搜索未匹配列表 -->
            <a href="goods.php?act=list&cat_id=311&supp=-1">生活日用</a>
            </span>
        </td>
    <td width="10%">0</td>
    <td width="10%"><span onclick="listTable.edit(this, 'edit_measure_unit', 311)"><!--  -->&nbsp;&nbsp;&nbsp;&nbsp;<!--  --></span></td>
    <td width="10%"><img src="images/no.gif" onclick="listTable.toggle(this, 'toggle_show_in_nav', 311)" /></td>
    <td width="10%"><img src="images/yes.gif" onclick="listTable.toggle(this, 'toggle_is_show', 311)" /></td>
    <td><span onclick="listTable.edit(this, 'edit_grade', 311)">0</span></td>
    <td width="10%" align="right"><span onclick="listTable.edit(this, 'edit_sort_order', 311)">50</span></td>
    <td width="24%" align="center">
      <a href="category.php?act=move&cat_id=311">转移商品</a> |
      <a href="category.php?act=edit&amp;cat_id=311">编辑</a> |
      <a href="javascript:;" onclick="listTable.remove(311, '您确认要删除这条记录吗?')" title="移除">移除</a>
	  <!--代码修改_start Byjdy-->
	  	  <!--代码修改_end Byjdy-->
    </td>
  </tr>
    <tr align="center" class="2" id="2_354">
    <td align="left" class="first-cell" >
            <img src="images/menu_minus.gif" id="icon_2_354" width="9" height="9" border="0" style="margin-left:2em" onclick="rowClicked(this)" />
            <span>
      <!-- 0-默认列表 1-搜索匹配列表 其他-搜索未匹配列表 -->
            <a href="goods.php?act=list&cat_id=354&supp=-1">洗晒/熨烫</a>
            </span>
        </td>
    <td width="10%">0</td>
    <td width="10%"><span onclick="listTable.edit(this, 'edit_measure_unit', 354)"><!--  -->&nbsp;&nbsp;&nbsp;&nbsp;<!--  --></span></td>
    <td width="10%"><img src="images/no.gif" onclick="listTable.toggle(this, 'toggle_show_in_nav', 354)" /></td>
    <td width="10%"><img src="images/yes.gif" onclick="listTable.toggle(this, 'toggle_is_show', 354)" /></td>
    <td><span onclick="listTable.edit(this, 'edit_grade', 354)">0</span></td>
    <td width="10%" align="right"><span onclick="listTable.edit(this, 'edit_sort_order', 354)">50</span></td>
    <td width="24%" align="center">
      <a href="category.php?act=move&cat_id=354">转移商品</a> |
      <a href="category.php?act=edit&amp;cat_id=354">编辑</a> |
      <a href="javascript:;" onclick="listTable.remove(354, '您确认要删除这条记录吗?')" title="移除">移除</a>
	  <!--代码修改_start Byjdy-->
	  	  <!--代码修改_end Byjdy-->
    </td>
  </tr>
    <tr align="center" class="2" id="2_351">
    <td align="left" class="first-cell" >
            <img src="images/menu_minus.gif" id="icon_2_351" width="9" height="9" border="0" style="margin-left:2em" onclick="rowClicked(this)" />
            <span>
      <!-- 0-默认列表 1-搜索匹配列表 其他-搜索未匹配列表 -->
            <a href="goods.php?act=list&cat_id=351&supp=-1">雨伞雨具</a>
            </span>
        </td>
    <td width="10%">0</td>
    <td width="10%"><span onclick="listTable.edit(this, 'edit_measure_unit', 351)"><!--  -->&nbsp;&nbsp;&nbsp;&nbsp;<!--  --></span></td>
    <td width="10%"><img src="images/no.gif" onclick="listTable.toggle(this, 'toggle_show_in_nav', 351)" /></td>
    <td width="10%"><img src="images/yes.gif" onclick="listTable.toggle(this, 'toggle_is_show', 351)" /></td>
    <td><span onclick="listTable.edit(this, 'edit_grade', 351)">0</span></td>
    <td width="10%" align="right"><span onclick="listTable.edit(this, 'edit_sort_order', 351)">50</span></td>
    <td width="24%" align="center">
      <a href="category.php?act=move&cat_id=351">转移商品</a> |
      <a href="category.php?act=edit&amp;cat_id=351">编辑</a> |
      <a href="javascript:;" onclick="listTable.remove(351, '您确认要删除这条记录吗?')" title="移除">移除</a>
	  <!--代码修改_start Byjdy-->
	  	  <!--代码修改_end Byjdy-->
    </td>
  </tr>
    <tr align="center" class="2" id="2_355">
    <td align="left" class="first-cell" >
            <img src="images/menu_minus.gif" id="icon_2_355" width="9" height="9" border="0" style="margin-left:2em" onclick="rowClicked(this)" />
            <span>
      <!-- 0-默认列表 1-搜索匹配列表 其他-搜索未匹配列表 -->
            <a href="goods.php?act=list&cat_id=355&supp=-1">净化除味</a>
            </span>
        </td>
    <td width="10%">0</td>
    <td width="10%"><span onclick="listTable.edit(this, 'edit_measure_unit', 355)"><!--  -->&nbsp;&nbsp;&nbsp;&nbsp;<!--  --></span></td>
    <td width="10%"><img src="images/no.gif" onclick="listTable.toggle(this, 'toggle_show_in_nav', 355)" /></td>
    <td width="10%"><img src="images/yes.gif" onclick="listTable.toggle(this, 'toggle_is_show', 355)" /></td>
    <td><span onclick="listTable.edit(this, 'edit_grade', 355)">0</span></td>
    <td width="10%" align="right"><span onclick="listTable.edit(this, 'edit_sort_order', 355)">50</span></td>
    <td width="24%" align="center">
      <a href="category.php?act=move&cat_id=355">转移商品</a> |
      <a href="category.php?act=edit&amp;cat_id=355">编辑</a> |
      <a href="javascript:;" onclick="listTable.remove(355, '您确认要删除这条记录吗?')" title="移除">移除</a>
	  <!--代码修改_start Byjdy-->
	  	  <!--代码修改_end Byjdy-->
    </td>
  </tr>
    <tr align="center" class="2" id="2_352">
    <td align="left" class="first-cell" >
            <img src="images/menu_minus.gif" id="icon_2_352" width="9" height="9" border="0" style="margin-left:2em" onclick="rowClicked(this)" />
            <span>
      <!-- 0-默认列表 1-搜索匹配列表 其他-搜索未匹配列表 -->
            <a href="goods.php?act=list&cat_id=352&supp=-1">浴室用品</a>
            </span>
        </td>
    <td width="10%">0</td>
    <td width="10%"><span onclick="listTable.edit(this, 'edit_measure_unit', 352)"><!--  -->&nbsp;&nbsp;&nbsp;&nbsp;<!--  --></span></td>
    <td width="10%"><img src="images/no.gif" onclick="listTable.toggle(this, 'toggle_show_in_nav', 352)" /></td>
    <td width="10%"><img src="images/yes.gif" onclick="listTable.toggle(this, 'toggle_is_show', 352)" /></td>
    <td><span onclick="listTable.edit(this, 'edit_grade', 352)">0</span></td>
    <td width="10%" align="right"><span onclick="listTable.edit(this, 'edit_sort_order', 352)">50</span></td>
    <td width="24%" align="center">
      <a href="category.php?act=move&cat_id=352">转移商品</a> |
      <a href="category.php?act=edit&amp;cat_id=352">编辑</a> |
      <a href="javascript:;" onclick="listTable.remove(352, '您确认要删除这条记录吗?')" title="移除">移除</a>
	  <!--代码修改_start Byjdy-->
	  	  <!--代码修改_end Byjdy-->
    </td>
  </tr>
    <tr align="center" class="2" id="2_353">
    <td align="left" class="first-cell" >
            <img src="images/menu_minus.gif" id="icon_2_353" width="9" height="9" border="0" style="margin-left:2em" onclick="rowClicked(this)" />
            <span>
      <!-- 0-默认列表 1-搜索匹配列表 其他-搜索未匹配列表 -->
            <a href="goods.php?act=list&cat_id=353&supp=-1">缝纫/针织用品</a>
            </span>
        </td>
    <td width="10%">0</td>
    <td width="10%"><span onclick="listTable.edit(this, 'edit_measure_unit', 353)"><!--  -->&nbsp;&nbsp;&nbsp;&nbsp;<!--  --></span></td>
    <td width="10%"><img src="images/no.gif" onclick="listTable.toggle(this, 'toggle_show_in_nav', 353)" /></td>
    <td width="10%"><img src="images/yes.gif" onclick="listTable.toggle(this, 'toggle_is_show', 353)" /></td>
    <td><span onclick="listTable.edit(this, 'edit_grade', 353)">0</span></td>
    <td width="10%" align="right"><span onclick="listTable.edit(this, 'edit_sort_order', 353)">50</span></td>
    <td width="24%" align="center">
      <a href="category.php?act=move&cat_id=353">转移商品</a> |
      <a href="category.php?act=edit&amp;cat_id=353">编辑</a> |
      <a href="javascript:;" onclick="listTable.remove(353, '您确认要删除这条记录吗?')" title="移除">移除</a>
	  <!--代码修改_start Byjdy-->
	  	  <!--代码修改_end Byjdy-->
    </td>
  </tr>
    <tr align="center" class="2" id="2_350">
    <td align="left" class="first-cell" >
            <img src="images/menu_minus.gif" id="icon_2_350" width="9" height="9" border="0" style="margin-left:2em" onclick="rowClicked(this)" />
            <span>
      <!-- 0-默认列表 1-搜索匹配列表 其他-搜索未匹配列表 -->
            <a href="goods.php?act=list&cat_id=350&supp=-1">收纳用品</a>
            </span>
        </td>
    <td width="10%">0</td>
    <td width="10%"><span onclick="listTable.edit(this, 'edit_measure_unit', 350)"><!--  -->&nbsp;&nbsp;&nbsp;&nbsp;<!--  --></span></td>
    <td width="10%"><img src="images/no.gif" onclick="listTable.toggle(this, 'toggle_show_in_nav', 350)" /></td>
    <td width="10%"><img src="images/yes.gif" onclick="listTable.toggle(this, 'toggle_is_show', 350)" /></td>
    <td><span onclick="listTable.edit(this, 'edit_grade', 350)">0</span></td>
    <td width="10%" align="right"><span onclick="listTable.edit(this, 'edit_sort_order', 350)">50</span></td>
    <td width="24%" align="center">
      <a href="category.php?act=move&cat_id=350">转移商品</a> |
      <a href="category.php?act=edit&amp;cat_id=350">编辑</a> |
      <a href="javascript:;" onclick="listTable.remove(350, '您确认要删除这条记录吗?')" title="移除">移除</a>
	  <!--代码修改_start Byjdy-->
	  	  <!--代码修改_end Byjdy-->
    </td>
  </tr>
    <tr align="center" class="1" id="1_306">
    <td align="left" class="first-cell" >
            <img src="images/menu_minus.gif" id="icon_1_306" width="9" height="9" border="0" style="margin-left:1em" onclick="rowClicked(this)" />
            <span>
      <!-- 0-默认列表 1-搜索匹配列表 其他-搜索未匹配列表 -->
            <a href="goods.php?act=list&cat_id=306&supp=-1">家纺</a>
            </span>
        </td>
    <td width="10%">8</td>
    <td width="10%"><span onclick="listTable.edit(this, 'edit_measure_unit', 306)"><!--  -->&nbsp;&nbsp;&nbsp;&nbsp;<!--  --></span></td>
    <td width="10%"><img src="images/no.gif" onclick="listTable.toggle(this, 'toggle_show_in_nav', 306)" /></td>
    <td width="10%"><img src="images/yes.gif" onclick="listTable.toggle(this, 'toggle_is_show', 306)" /></td>
    <td><span onclick="listTable.edit(this, 'edit_grade', 306)">0</span></td>
    <td width="10%" align="right"><span onclick="listTable.edit(this, 'edit_sort_order', 306)">50</span></td>
    <td width="24%" align="center">
      <a href="category.php?act=move&cat_id=306">转移商品</a> |
      <a href="category.php?act=edit&amp;cat_id=306">编辑</a> |
      <a href="javascript:;" onclick="listTable.remove(306, '您确认要删除这条记录吗?')" title="移除">移除</a>
	  <!--代码修改_start Byjdy-->
	  	  <!--代码修改_end Byjdy-->
    </td>
  </tr>
    <tr align="center" class="2" id="2_319">
    <td align="left" class="first-cell" >
            <img src="images/menu_minus.gif" id="icon_2_319" width="9" height="9" border="0" style="margin-left:2em" onclick="rowClicked(this)" />
            <span>
      <!-- 0-默认列表 1-搜索匹配列表 其他-搜索未匹配列表 -->
            <a href="goods.php?act=list&cat_id=319&supp=-1">窗帘/窗纱</a>
            </span>
        </td>
    <td width="10%">0</td>
    <td width="10%"><span onclick="listTable.edit(this, 'edit_measure_unit', 319)"><!--  -->&nbsp;&nbsp;&nbsp;&nbsp;<!--  --></span></td>
    <td width="10%"><img src="images/no.gif" onclick="listTable.toggle(this, 'toggle_show_in_nav', 319)" /></td>
    <td width="10%"><img src="images/yes.gif" onclick="listTable.toggle(this, 'toggle_is_show', 319)" /></td>
    <td><span onclick="listTable.edit(this, 'edit_grade', 319)">0</span></td>
    <td width="10%" align="right"><span onclick="listTable.edit(this, 'edit_sort_order', 319)">50</span></td>
    <td width="24%" align="center">
      <a href="category.php?act=move&cat_id=319">转移商品</a> |
      <a href="category.php?act=edit&amp;cat_id=319">编辑</a> |
      <a href="javascript:;" onclick="listTable.remove(319, '您确认要删除这条记录吗?')" title="移除">移除</a>
	  <!--代码修改_start Byjdy-->
	  	  <!--代码修改_end Byjdy-->
    </td>
  </tr>
    <tr align="center" class="2" id="2_316">
    <td align="left" class="first-cell" >
            <img src="images/menu_minus.gif" id="icon_2_316" width="9" height="9" border="0" style="margin-left:2em" onclick="rowClicked(this)" />
            <span>
      <!-- 0-默认列表 1-搜索匹配列表 其他-搜索未匹配列表 -->
            <a href="goods.php?act=list&cat_id=316&supp=-1">床单被罩</a>
            </span>
        </td>
    <td width="10%">0</td>
    <td width="10%"><span onclick="listTable.edit(this, 'edit_measure_unit', 316)"><!--  -->&nbsp;&nbsp;&nbsp;&nbsp;<!--  --></span></td>
    <td width="10%"><img src="images/no.gif" onclick="listTable.toggle(this, 'toggle_show_in_nav', 316)" /></td>
    <td width="10%"><img src="images/yes.gif" onclick="listTable.toggle(this, 'toggle_is_show', 316)" /></td>
    <td><span onclick="listTable.edit(this, 'edit_grade', 316)">0</span></td>
    <td width="10%" align="right"><span onclick="listTable.edit(this, 'edit_sort_order', 316)">50</span></td>
    <td width="24%" align="center">
      <a href="category.php?act=move&cat_id=316">转移商品</a> |
      <a href="category.php?act=edit&amp;cat_id=316">编辑</a> |
      <a href="javascript:;" onclick="listTable.remove(316, '您确认要删除这条记录吗?')" title="移除">移除</a>
	  <!--代码修改_start Byjdy-->
	  	  <!--代码修改_end Byjdy-->
    </td>
  </tr>
    <tr align="center" class="2" id="2_313">
    <td align="left" class="first-cell" >
            <img src="images/menu_minus.gif" id="icon_2_313" width="9" height="9" border="0" style="margin-left:2em" onclick="rowClicked(this)" />
            <span>
      <!-- 0-默认列表 1-搜索匹配列表 其他-搜索未匹配列表 -->
            <a href="goods.php?act=list&cat_id=313&supp=-1">被子</a>
            </span>
        </td>
    <td width="10%">0</td>
    <td width="10%"><span onclick="listTable.edit(this, 'edit_measure_unit', 313)"><!--  -->&nbsp;&nbsp;&nbsp;&nbsp;<!--  --></span></td>
    <td width="10%"><img src="images/no.gif" onclick="listTable.toggle(this, 'toggle_show_in_nav', 313)" /></td>
    <td width="10%"><img src="images/yes.gif" onclick="listTable.toggle(this, 'toggle_is_show', 313)" /></td>
    <td><span onclick="listTable.edit(this, 'edit_grade', 313)">0</span></td>
    <td width="10%" align="right"><span onclick="listTable.edit(this, 'edit_sort_order', 313)">50</span></td>
    <td width="24%" align="center">
      <a href="category.php?act=move&cat_id=313">转移商品</a> |
      <a href="category.php?act=edit&amp;cat_id=313">编辑</a> |
      <a href="javascript:;" onclick="listTable.remove(313, '您确认要删除这条记录吗?')" title="移除">移除</a>
	  <!--代码修改_start Byjdy-->
	  	  <!--代码修改_end Byjdy-->
    </td>
  </tr>
    <tr align="center" class="2" id="2_317">
    <td align="left" class="first-cell" >
            <img src="images/menu_minus.gif" id="icon_2_317" width="9" height="9" border="0" style="margin-left:2em" onclick="rowClicked(this)" />
            <span>
      <!-- 0-默认列表 1-搜索匹配列表 其他-搜索未匹配列表 -->
            <a href="goods.php?act=list&cat_id=317&supp=-1">毛巾浴巾</a>
            </span>
        </td>
    <td width="10%">0</td>
    <td width="10%"><span onclick="listTable.edit(this, 'edit_measure_unit', 317)"><!--  -->&nbsp;&nbsp;&nbsp;&nbsp;<!--  --></span></td>
    <td width="10%"><img src="images/no.gif" onclick="listTable.toggle(this, 'toggle_show_in_nav', 317)" /></td>
    <td width="10%"><img src="images/yes.gif" onclick="listTable.toggle(this, 'toggle_is_show', 317)" /></td>
    <td><span onclick="listTable.edit(this, 'edit_grade', 317)">0</span></td>
    <td width="10%" align="right"><span onclick="listTable.edit(this, 'edit_sort_order', 317)">50</span></td>
    <td width="24%" align="center">
      <a href="category.php?act=move&cat_id=317">转移商品</a> |
      <a href="category.php?act=edit&amp;cat_id=317">编辑</a> |
      <a href="javascript:;" onclick="listTable.remove(317, '您确认要删除这条记录吗?')" title="移除">移除</a>
	  <!--代码修改_start Byjdy-->
	  	  <!--代码修改_end Byjdy-->
    </td>
  </tr>
    <tr align="center" class="2" id="2_314">
    <td align="left" class="first-cell" >
            <img src="images/menu_minus.gif" id="icon_2_314" width="9" height="9" border="0" style="margin-left:2em" onclick="rowClicked(this)" />
            <span>
      <!-- 0-默认列表 1-搜索匹配列表 其他-搜索未匹配列表 -->
            <a href="goods.php?act=list&cat_id=314&supp=-1">蚊帐</a>
            </span>
        </td>
    <td width="10%">0</td>
    <td width="10%"><span onclick="listTable.edit(this, 'edit_measure_unit', 314)"><!--  -->&nbsp;&nbsp;&nbsp;&nbsp;<!--  --></span></td>
    <td width="10%"><img src="images/no.gif" onclick="listTable.toggle(this, 'toggle_show_in_nav', 314)" /></td>
    <td width="10%"><img src="images/yes.gif" onclick="listTable.toggle(this, 'toggle_is_show', 314)" /></td>
    <td><span onclick="listTable.edit(this, 'edit_grade', 314)">0</span></td>
    <td width="10%" align="right"><span onclick="listTable.edit(this, 'edit_sort_order', 314)">50</span></td>
    <td width="24%" align="center">
      <a href="category.php?act=move&cat_id=314">转移商品</a> |
      <a href="category.php?act=edit&amp;cat_id=314">编辑</a> |
      <a href="javascript:;" onclick="listTable.remove(314, '您确认要删除这条记录吗?')" title="移除">移除</a>
	  <!--代码修改_start Byjdy-->
	  	  <!--代码修改_end Byjdy-->
    </td>
  </tr>
    <tr align="center" class="2" id="2_318">
    <td align="left" class="first-cell" >
            <img src="images/menu_minus.gif" id="icon_2_318" width="9" height="9" border="0" style="margin-left:2em" onclick="rowClicked(this)" />
            <span>
      <!-- 0-默认列表 1-搜索匹配列表 其他-搜索未匹配列表 -->
            <a href="goods.php?act=list&cat_id=318&supp=-1">床垫/床褥</a>
            </span>
        </td>
    <td width="10%">0</td>
    <td width="10%"><span onclick="listTable.edit(this, 'edit_measure_unit', 318)"><!--  -->&nbsp;&nbsp;&nbsp;&nbsp;<!--  --></span></td>
    <td width="10%"><img src="images/no.gif" onclick="listTable.toggle(this, 'toggle_show_in_nav', 318)" /></td>
    <td width="10%"><img src="images/yes.gif" onclick="listTable.toggle(this, 'toggle_is_show', 318)" /></td>
    <td><span onclick="listTable.edit(this, 'edit_grade', 318)">0</span></td>
    <td width="10%" align="right"><span onclick="listTable.edit(this, 'edit_sort_order', 318)">50</span></td>
    <td width="24%" align="center">
      <a href="category.php?act=move&cat_id=318">转移商品</a> |
      <a href="category.php?act=edit&amp;cat_id=318">编辑</a> |
      <a href="javascript:;" onclick="listTable.remove(318, '您确认要删除这条记录吗?')" title="移除">移除</a>
	  <!--代码修改_start Byjdy-->
	  	  <!--代码修改_end Byjdy-->
    </td>
  </tr>
    <tr align="center" class="2" id="2_315">
    <td align="left" class="first-cell" >
            <img src="images/menu_minus.gif" id="icon_2_315" width="9" height="9" border="0" style="margin-left:2em" onclick="rowClicked(this)" />
            <span>
      <!-- 0-默认列表 1-搜索匹配列表 其他-搜索未匹配列表 -->
            <a href="goods.php?act=list&cat_id=315&supp=-1">凉席</a>
            </span>
        </td>
    <td width="10%">0</td>
    <td width="10%"><span onclick="listTable.edit(this, 'edit_measure_unit', 315)"><!--  -->&nbsp;&nbsp;&nbsp;&nbsp;<!--  --></span></td>
    <td width="10%"><img src="images/no.gif" onclick="listTable.toggle(this, 'toggle_show_in_nav', 315)" /></td>
    <td width="10%"><img src="images/yes.gif" onclick="listTable.toggle(this, 'toggle_is_show', 315)" /></td>
    <td><span onclick="listTable.edit(this, 'edit_grade', 315)">0</span></td>
    <td width="10%" align="right"><span onclick="listTable.edit(this, 'edit_sort_order', 315)">50</span></td>
    <td width="24%" align="center">
      <a href="category.php?act=move&cat_id=315">转移商品</a> |
      <a href="category.php?act=edit&amp;cat_id=315">编辑</a> |
      <a href="javascript:;" onclick="listTable.remove(315, '您确认要删除这条记录吗?')" title="移除">移除</a>
	  <!--代码修改_start Byjdy-->
	  	  <!--代码修改_end Byjdy-->
    </td>
  </tr>
    <tr align="center" class="2" id="2_312">
    <td align="left" class="first-cell" >
            <img src="images/menu_minus.gif" id="icon_2_312" width="9" height="9" border="0" style="margin-left:2em" onclick="rowClicked(this)" />
            <span>
      <!-- 0-默认列表 1-搜索匹配列表 其他-搜索未匹配列表 -->
            <a href="goods.php?act=list&cat_id=312&supp=-1">床品套件</a>
            </span>
        </td>
    <td width="10%">0</td>

    <td width="10%"><span onclick="listTable.edit(this, 'edit_measure_unit', 312)"><!--  -->&nbsp;&nbsp;&nbsp;&nbsp;<!--  --></span></td>
    <td width="10%"><img src="images/no.gif" onclick="listTable.toggle(this, 'toggle_show_in_nav', 312)" /></td>
    <td width="10%"><img src="images/yes.gif" onclick="listTable.toggle(this, 'toggle_is_show', 312)" /></td>
    <td><span onclick="listTable.edit(this, 'edit_grade', 312)">0</span></td>
    <td width="10%" align="right"><span onclick="listTable.edit(this, 'edit_sort_order', 312)">50</span></td>
    <td width="24%" align="center">
      <a href="category.php?act=move&cat_id=312">转移商品</a> |
      <a href="category.php?act=edit&amp;cat_id=312">编辑</a> |
      <a href="javascript:;" onclick="listTable.remove(312, '您确认要删除这条记录吗?')" title="移除">移除</a>
	  <!--代码修改_start Byjdy-->
	  	  <!--代码修改_end Byjdy-->
    </td>
  </tr>
    <tr align="center" class="1" id="1_308">
    <td align="left" class="first-cell" >
            <img src="images/menu_minus.gif" id="icon_1_308" width="9" height="9" border="0" style="margin-left:1em" onclick="rowClicked(this)" />
            <span>
      <!-- 0-默认列表 1-搜索匹配列表 其他-搜索未匹配列表 -->
            <a href="goods.php?act=list&cat_id=308&supp=-1">厨具</a>
            </span>
        </td>
    <td width="10%">8</td>
    <td width="10%"><span onclick="listTable.edit(this, 'edit_measure_unit', 308)"><!--  -->&nbsp;&nbsp;&nbsp;&nbsp;<!--  --></span></td>
    <td width="10%"><img src="images/no.gif" onclick="listTable.toggle(this, 'toggle_show_in_nav', 308)" /></td>
    <td width="10%"><img src="images/yes.gif" onclick="listTable.toggle(this, 'toggle_is_show', 308)" /></td>
    <td><span onclick="listTable.edit(this, 'edit_grade', 308)">0</span></td>
    <td width="10%" align="right"><span onclick="listTable.edit(this, 'edit_sort_order', 308)">50</span></td>
    <td width="24%" align="center">
      <a href="category.php?act=move&cat_id=308">转移商品</a> |
      <a href="category.php?act=edit&amp;cat_id=308">编辑</a> |
      <a href="javascript:;" onclick="listTable.remove(308, '您确认要删除这条记录吗?')" title="移除">移除</a>
	  <!--代码修改_start Byjdy-->
	  	  <!--代码修改_end Byjdy-->
    </td>
  </tr>
    <tr align="center" class="2" id="2_332">
    <td align="left" class="first-cell" >
            <img src="images/menu_minus.gif" id="icon_2_332" width="9" height="9" border="0" style="margin-left:2em" onclick="rowClicked(this)" />
            <span>
      <!-- 0-默认列表 1-搜索匹配列表 其他-搜索未匹配列表 -->
            <a href="goods.php?act=list&cat_id=332&supp=-1">餐具</a>
            </span>
        </td>
    <td width="10%">0</td>
    <td width="10%"><span onclick="listTable.edit(this, 'edit_measure_unit', 332)"><!--  -->&nbsp;&nbsp;&nbsp;&nbsp;<!--  --></span></td>
    <td width="10%"><img src="images/no.gif" onclick="listTable.toggle(this, 'toggle_show_in_nav', 332)" /></td>
    <td width="10%"><img src="images/yes.gif" onclick="listTable.toggle(this, 'toggle_is_show', 332)" /></td>
    <td><span onclick="listTable.edit(this, 'edit_grade', 332)">0</span></td>
    <td width="10%" align="right"><span onclick="listTable.edit(this, 'edit_sort_order', 332)">50</span></td>
    <td width="24%" align="center">
      <a href="category.php?act=move&cat_id=332">转移商品</a> |
      <a href="category.php?act=edit&amp;cat_id=332">编辑</a> |
      <a href="javascript:;" onclick="listTable.remove(332, '您确认要删除这条记录吗?')" title="移除">移除</a>
	  <!--代码修改_start Byjdy-->
	  	  <!--代码修改_end Byjdy-->
    </td>
  </tr>
    <tr align="center" class="2" id="2_329">
    <td align="left" class="first-cell" >
            <img src="images/menu_minus.gif" id="icon_2_329" width="9" height="9" border="0" style="margin-left:2em" onclick="rowClicked(this)" />
            <span>
      <!-- 0-默认列表 1-搜索匹配列表 其他-搜索未匹配列表 -->
            <a href="goods.php?act=list&cat_id=329&supp=-1">刀剪菜板</a>
            </span>
        </td>
    <td width="10%">0</td>
    <td width="10%"><span onclick="listTable.edit(this, 'edit_measure_unit', 329)"><!--  -->&nbsp;&nbsp;&nbsp;&nbsp;<!--  --></span></td>
    <td width="10%"><img src="images/no.gif" onclick="listTable.toggle(this, 'toggle_show_in_nav', 329)" /></td>
    <td width="10%"><img src="images/yes.gif" onclick="listTable.toggle(this, 'toggle_is_show', 329)" /></td>
    <td><span onclick="listTable.edit(this, 'edit_grade', 329)">0</span></td>
    <td width="10%" align="right"><span onclick="listTable.edit(this, 'edit_sort_order', 329)">50</span></td>
    <td width="24%" align="center">
      <a href="category.php?act=move&cat_id=329">转移商品</a> |
      <a href="category.php?act=edit&amp;cat_id=329">编辑</a> |
      <a href="javascript:;" onclick="listTable.remove(329, '您确认要删除这条记录吗?')" title="移除">移除</a>
	  <!--代码修改_start Byjdy-->
	  	  <!--代码修改_end Byjdy-->
    </td>
  </tr>
    <tr align="center" class="2" id="2_333">
    <td align="left" class="first-cell" >
            <img src="images/menu_minus.gif" id="icon_2_333" width="9" height="9" border="0" style="margin-left:2em" onclick="rowClicked(this)" />
            <span>
      <!-- 0-默认列表 1-搜索匹配列表 其他-搜索未匹配列表 -->
            <a href="goods.php?act=list&cat_id=333&supp=-1">茶具/咖啡具</a>
            </span>
        </td>
    <td width="10%">0</td>
    <td width="10%"><span onclick="listTable.edit(this, 'edit_measure_unit', 333)"><!--  -->&nbsp;&nbsp;&nbsp;&nbsp;<!--  --></span></td>
    <td width="10%"><img src="images/no.gif" onclick="listTable.toggle(this, 'toggle_show_in_nav', 333)" /></td>
    <td width="10%"><img src="images/yes.gif" onclick="listTable.toggle(this, 'toggle_is_show', 333)" /></td>
    <td><span onclick="listTable.edit(this, 'edit_grade', 333)">0</span></td>
    <td width="10%" align="right"><span onclick="listTable.edit(this, 'edit_sort_order', 333)">50</span></td>
    <td width="24%" align="center">
      <a href="category.php?act=move&cat_id=333">转移商品</a> |
      <a href="category.php?act=edit&amp;cat_id=333">编辑</a> |
      <a href="javascript:;" onclick="listTable.remove(333, '您确认要删除这条记录吗?')" title="移除">移除</a>
	  <!--代码修改_start Byjdy-->
	  	  <!--代码修改_end Byjdy-->
    </td>
  </tr>
    <tr align="center" class="2" id="2_330">
    <td align="left" class="first-cell" >
            <img src="images/menu_minus.gif" id="icon_2_330" width="9" height="9" border="0" style="margin-left:2em" onclick="rowClicked(this)" />
            <span>
      <!-- 0-默认列表 1-搜索匹配列表 其他-搜索未匹配列表 -->
            <a href="goods.php?act=list&cat_id=330&supp=-1">厨房配件</a>
            </span>
        </td>
    <td width="10%">0</td>
    <td width="10%"><span onclick="listTable.edit(this, 'edit_measure_unit', 330)"><!--  -->&nbsp;&nbsp;&nbsp;&nbsp;<!--  --></span></td>
    <td width="10%"><img src="images/no.gif" onclick="listTable.toggle(this, 'toggle_show_in_nav', 330)" /></td>
    <td width="10%"><img src="images/yes.gif" onclick="listTable.toggle(this, 'toggle_is_show', 330)" /></td>
    <td><span onclick="listTable.edit(this, 'edit_grade', 330)">0</span></td>
    <td width="10%" align="right"><span onclick="listTable.edit(this, 'edit_sort_order', 330)">50</span></td>
    <td width="24%" align="center">
      <a href="category.php?act=move&cat_id=330">转移商品</a> |
      <a href="category.php?act=edit&amp;cat_id=330">编辑</a> |
      <a href="javascript:;" onclick="listTable.remove(330, '您确认要删除这条记录吗?')" title="移除">移除</a>
	  <!--代码修改_start Byjdy-->
	  	  <!--代码修改_end Byjdy-->
    </td>
  </tr>
    <tr align="center" class="2" id="2_331">
    <td align="left" class="first-cell" >
            <img src="images/menu_minus.gif" id="icon_2_331" width="9" height="9" border="0" style="margin-left:2em" onclick="rowClicked(this)" />
            <span>
      <!-- 0-默认列表 1-搜索匹配列表 其他-搜索未匹配列表 -->
            <a href="goods.php?act=list&cat_id=331&supp=-1">水具酒具</a>
            </span>
        </td>
    <td width="10%">0</td>
    <td width="10%"><span onclick="listTable.edit(this, 'edit_measure_unit', 331)"><!--  -->&nbsp;&nbsp;&nbsp;&nbsp;<!--  --></span></td>
    <td width="10%"><img src="images/no.gif" onclick="listTable.toggle(this, 'toggle_show_in_nav', 331)" /></td>
    <td width="10%"><img src="images/yes.gif" onclick="listTable.toggle(this, 'toggle_is_show', 331)" /></td>
    <td><span onclick="listTable.edit(this, 'edit_grade', 331)">0</span></td>
    <td width="10%" align="right"><span onclick="listTable.edit(this, 'edit_sort_order', 331)">50</span></td>
    <td width="24%" align="center">
      <a href="category.php?act=move&cat_id=331">转移商品</a> |
      <a href="category.php?act=edit&amp;cat_id=331">编辑</a> |
      <a href="javascript:;" onclick="listTable.remove(331, '您确认要删除这条记录吗?')" title="移除">移除</a>
	  <!--代码修改_start Byjdy-->
	  	  <!--代码修改_end Byjdy-->
    </td>
  </tr>
    <tr align="center" class="2" id="2_328">
    <td align="left" class="first-cell" >
            <img src="images/menu_minus.gif" id="icon_2_328" width="9" height="9" border="0" style="margin-left:2em" onclick="rowClicked(this)" />
            <span>
      <!-- 0-默认列表 1-搜索匹配列表 其他-搜索未匹配列表 -->
            <a href="goods.php?act=list&cat_id=328&supp=-1">烹饪锅具</a>
            </span>
        </td>
    <td width="10%">0</td>
    <td width="10%"><span onclick="listTable.edit(this, 'edit_measure_unit', 328)"><!--  -->&nbsp;&nbsp;&nbsp;&nbsp;<!--  --></span></td>
    <td width="10%"><img src="images/no.gif" onclick="listTable.toggle(this, 'toggle_show_in_nav', 328)" /></td>
    <td width="10%"><img src="images/yes.gif" onclick="listTable.toggle(this, 'toggle_is_show', 328)" /></td>
    <td><span onclick="listTable.edit(this, 'edit_grade', 328)">0</span></td>
    <td width="10%" align="right"><span onclick="listTable.edit(this, 'edit_sort_order', 328)">50</span></td>
    <td width="24%" align="center">
      <a href="category.php?act=move&cat_id=328">转移商品</a> |
      <a href="category.php?act=edit&amp;cat_id=328">编辑</a> |
      <a href="javascript:;" onclick="listTable.remove(328, '您确认要删除这条记录吗?')" title="移除">移除</a>
	  <!--代码修改_start Byjdy-->
	  	  <!--代码修改_end Byjdy-->
    </td>
  </tr>
    <tr align="center" class="1" id="1_309">
    <td align="left" class="first-cell" >
            <img src="images/menu_minus.gif" id="icon_1_309" width="9" height="9" border="0" style="margin-left:1em" onclick="rowClicked(this)" />
            <span>
      <!-- 0-默认列表 1-搜索匹配列表 其他-搜索未匹配列表 -->
            <a href="goods.php?act=list&cat_id=309&supp=-1">灯具</a>
            </span>
        </td>
    <td width="10%">0</td>
    <td width="10%"><span onclick="listTable.edit(this, 'edit_measure_unit', 309)"><!--  -->&nbsp;&nbsp;&nbsp;&nbsp;<!--  --></span></td>
    <td width="10%"><img src="images/no.gif" onclick="listTable.toggle(this, 'toggle_show_in_nav', 309)" /></td>
    <td width="10%"><img src="images/yes.gif" onclick="listTable.toggle(this, 'toggle_is_show', 309)" /></td>
    <td><span onclick="listTable.edit(this, 'edit_grade', 309)">0</span></td>
    <td width="10%" align="right"><span onclick="listTable.edit(this, 'edit_sort_order', 309)">50</span></td>
    <td width="24%" align="center">
      <a href="category.php?act=move&cat_id=309">转移商品</a> |
      <a href="category.php?act=edit&amp;cat_id=309">编辑</a> |
      <a href="javascript:;" onclick="listTable.remove(309, '您确认要删除这条记录吗?')" title="移除">移除</a>
	  <!--代码修改_start Byjdy-->
	  	  <!--代码修改_end Byjdy-->
    </td>
  </tr>
    <tr align="center" class="2" id="2_338">
    <td align="left" class="first-cell" >
            <img src="images/menu_minus.gif" id="icon_2_338" width="9" height="9" border="0" style="margin-left:2em" onclick="rowClicked(this)" />
            <span>
      <!-- 0-默认列表 1-搜索匹配列表 其他-搜索未匹配列表 -->
            <a href="goods.php?act=list&cat_id=338&supp=-1">落地灯</a>
            </span>
        </td>
    <td width="10%">0</td>
    <td width="10%"><span onclick="listTable.edit(this, 'edit_measure_unit', 338)"><!--  -->&nbsp;&nbsp;&nbsp;&nbsp;<!--  --></span></td>
    <td width="10%"><img src="images/no.gif" onclick="listTable.toggle(this, 'toggle_show_in_nav', 338)" /></td>
    <td width="10%"><img src="images/yes.gif" onclick="listTable.toggle(this, 'toggle_is_show', 338)" /></td>
    <td><span onclick="listTable.edit(this, 'edit_grade', 338)">0</span></td>
    <td width="10%" align="right"><span onclick="listTable.edit(this, 'edit_sort_order', 338)">50</span></td>
    <td width="24%" align="center">
      <a href="category.php?act=move&cat_id=338">转移商品</a> |
      <a href="category.php?act=edit&amp;cat_id=338">编辑</a> |
      <a href="javascript:;" onclick="listTable.remove(338, '您确认要删除这条记录吗?')" title="移除">移除</a>
	  <!--代码修改_start Byjdy-->
	  	  <!--代码修改_end Byjdy-->
    </td>
  </tr>
    <tr align="center" class="2" id="2_335">
    <td align="left" class="first-cell" >
            <img src="images/menu_minus.gif" id="icon_2_335" width="9" height="9" border="0" style="margin-left:2em" onclick="rowClicked(this)" />
            <span>
      <!-- 0-默认列表 1-搜索匹配列表 其他-搜索未匹配列表 -->
            <a href="goods.php?act=list&cat_id=335&supp=-1">吸顶灯</a>
            </span>
        </td>
    <td width="10%">0</td>
    <td width="10%"><span onclick="listTable.edit(this, 'edit_measure_unit', 335)"><!--  -->&nbsp;&nbsp;&nbsp;&nbsp;<!--  --></span></td>
    <td width="10%"><img src="images/no.gif" onclick="listTable.toggle(this, 'toggle_show_in_nav', 335)" /></td>
    <td width="10%"><img src="images/yes.gif" onclick="listTable.toggle(this, 'toggle_is_show', 335)" /></td>
    <td><span onclick="listTable.edit(this, 'edit_grade', 335)">0</span></td>
    <td width="10%" align="right"><span onclick="listTable.edit(this, 'edit_sort_order', 335)">50</span></td>
    <td width="24%" align="center">
      <a href="category.php?act=move&cat_id=335">转移商品</a> |
      <a href="category.php?act=edit&amp;cat_id=335">编辑</a> |
      <a href="javascript:;" onclick="listTable.remove(335, '您确认要删除这条记录吗?')" title="移除">移除</a>
	  <!--代码修改_start Byjdy-->
	  	  <!--代码修改_end Byjdy-->
    </td>
  </tr>
    <tr align="center" class="2" id="2_339">
    <td align="left" class="first-cell" >
            <img src="images/menu_minus.gif" id="icon_2_339" width="9" height="9" border="0" style="margin-left:2em" onclick="rowClicked(this)" />
            <span>
      <!-- 0-默认列表 1-搜索匹配列表 其他-搜索未匹配列表 -->
            <a href="goods.php?act=list&cat_id=339&supp=-1">应急灯/手电</a>
            </span>
        </td>
    <td width="10%">0</td>
    <td width="10%"><span onclick="listTable.edit(this, 'edit_measure_unit', 339)"><!--  -->&nbsp;&nbsp;&nbsp;&nbsp;<!--  --></span></td>
    <td width="10%"><img src="images/no.gif" onclick="listTable.toggle(this, 'toggle_show_in_nav', 339)" /></td>
    <td width="10%"><img src="images/yes.gif" onclick="listTable.toggle(this, 'toggle_is_show', 339)" /></td>
    <td><span onclick="listTable.edit(this, 'edit_grade', 339)">0</span></td>
    <td width="10%" align="right"><span onclick="listTable.edit(this, 'edit_sort_order', 339)">50</span></td>
    <td width="24%" align="center">
      <a href="category.php?act=move&cat_id=339">转移商品</a> |
      <a href="category.php?act=edit&amp;cat_id=339">编辑</a> |
      <a href="javascript:;" onclick="listTable.remove(339, '您确认要删除这条记录吗?')" title="移除">移除</a>
	  <!--代码修改_start Byjdy-->
	  	  <!--代码修改_end Byjdy-->
    </td>
  </tr>
    <tr align="center" class="2" id="2_336">
    <td align="left" class="first-cell" >
            <img src="images/menu_minus.gif" id="icon_2_336" width="9" height="9" border="0" style="margin-left:2em" onclick="rowClicked(this)" />
            <span>
      <!-- 0-默认列表 1-搜索匹配列表 其他-搜索未匹配列表 -->
            <a href="goods.php?act=list&cat_id=336&supp=-1">筒灯射灯</a>
            </span>
        </td>
    <td width="10%">0</td>
    <td width="10%"><span onclick="listTable.edit(this, 'edit_measure_unit', 336)"><!--  -->&nbsp;&nbsp;&nbsp;&nbsp;<!--  --></span></td>
    <td width="10%"><img src="images/no.gif" onclick="listTable.toggle(this, 'toggle_show_in_nav', 336)" /></td>
    <td width="10%"><img src="images/yes.gif" onclick="listTable.toggle(this, 'toggle_is_show', 336)" /></td>
    <td><span onclick="listTable.edit(this, 'edit_grade', 336)">0</span></td>
    <td width="10%" align="right"><span onclick="listTable.edit(this, 'edit_sort_order', 336)">50</span></td>
    <td width="24%" align="center">
      <a href="category.php?act=move&cat_id=336">转移商品</a> |
      <a href="category.php?act=edit&amp;cat_id=336">编辑</a> |
      <a href="javascript:;" onclick="listTable.remove(336, '您确认要删除这条记录吗?')" title="移除">移除</a>
	  <!--代码修改_start Byjdy-->
	  	  <!--代码修改_end Byjdy-->
    </td>
  </tr>
    <tr align="center" class="2" id="2_340">
    <td align="left" class="first-cell" >
            <img src="images/menu_minus.gif" id="icon_2_340" width="9" height="9" border="0" style="margin-left:2em" onclick="rowClicked(this)" />
            <span>
      <!-- 0-默认列表 1-搜索匹配列表 其他-搜索未匹配列表 -->
            <a href="goods.php?act=list&cat_id=340&supp=-1">装饰灯</a>
            </span>
        </td>
    <td width="10%">0</td>
    <td width="10%"><span onclick="listTable.edit(this, 'edit_measure_unit', 340)"><!--  -->&nbsp;&nbsp;&nbsp;&nbsp;<!--  --></span></td>
    <td width="10%"><img src="images/no.gif" onclick="listTable.toggle(this, 'toggle_show_in_nav', 340)" /></td>
    <td width="10%"><img src="images/yes.gif" onclick="listTable.toggle(this, 'toggle_is_show', 340)" /></td>
    <td><span onclick="listTable.edit(this, 'edit_grade', 340)">0</span></td>
    <td width="10%" align="right"><span onclick="listTable.edit(this, 'edit_sort_order', 340)">50</span></td>
    <td width="24%" align="center">
      <a href="category.php?act=move&cat_id=340">转移商品</a> |
      <a href="category.php?act=edit&amp;cat_id=340">编辑</a> |
      <a href="javascript:;" onclick="listTable.remove(340, '您确认要删除这条记录吗?')" title="移除">移除</a>
	  <!--代码修改_start Byjdy-->
	  	  <!--代码修改_end Byjdy-->
    </td>
  </tr>
    <tr align="center" class="2" id="2_337">
    <td align="left" class="first-cell" >
            <img src="images/menu_minus.gif" id="icon_2_337" width="9" height="9" border="0" style="margin-left:2em" onclick="rowClicked(this)" />
            <span>
      <!-- 0-默认列表 1-搜索匹配列表 其他-搜索未匹配列表 -->
            <a href="goods.php?act=list&cat_id=337&supp=-1">LED灯</a>
            </span>
        </td>
    <td width="10%">0</td>
    <td width="10%"><span onclick="listTable.edit(this, 'edit_measure_unit', 337)"><!--  -->&nbsp;&nbsp;&nbsp;&nbsp;<!--  --></span></td>
    <td width="10%"><img src="images/no.gif" onclick="listTable.toggle(this, 'toggle_show_in_nav', 337)" /></td>
    <td width="10%"><img src="images/yes.gif" onclick="listTable.toggle(this, 'toggle_is_show', 337)" /></td>
    <td><span onclick="listTable.edit(this, 'edit_grade', 337)">0</span></td>
    <td width="10%" align="right"><span onclick="listTable.edit(this, 'edit_sort_order', 337)">50</span></td>
    <td width="24%" align="center">
      <a href="category.php?act=move&cat_id=337">转移商品</a> |
      <a href="category.php?act=edit&amp;cat_id=337">编辑</a> |
      <a href="javascript:;" onclick="listTable.remove(337, '您确认要删除这条记录吗?')" title="移除">移除</a>
	  <!--代码修改_start Byjdy-->
	  	  <!--代码修改_end Byjdy-->
    </td>
  </tr>
    <tr align="center" class="2" id="2_334">
    <td align="left" class="first-cell" >
            <img src="images/menu_minus.gif" id="icon_2_334" width="9" height="9" border="0" style="margin-left:2em" onclick="rowClicked(this)" />
            <span>
      <!-- 0-默认列表 1-搜索匹配列表 其他-搜索未匹配列表 -->
            <a href="goods.php?act=list&cat_id=334&supp=-1">台灯</a>
            </span>
        </td>
    <td width="10%">0</td>
    <td width="10%"><span onclick="listTable.edit(this, 'edit_measure_unit', 334)"><!--  -->&nbsp;&nbsp;&nbsp;&nbsp;<!--  --></span></td>
    <td width="10%"><img src="images/no.gif" onclick="listTable.toggle(this, 'toggle_show_in_nav', 334)" /></td>
    <td width="10%"><img src="images/yes.gif" onclick="listTable.toggle(this, 'toggle_is_show', 334)" /></td>
    <td><span onclick="listTable.edit(this, 'edit_grade', 334)">0</span></td>
    <td width="10%" align="right"><span onclick="listTable.edit(this, 'edit_sort_order', 334)">50</span></td>
    <td width="24%" align="center">
      <a href="category.php?act=move&cat_id=334">转移商品</a> |
      <a href="category.php?act=edit&amp;cat_id=334">编辑</a> |
      <a href="javascript:;" onclick="listTable.remove(334, '您确认要删除这条记录吗?')" title="移除">移除</a>
	  <!--代码修改_start Byjdy-->
	  	  <!--代码修改_end Byjdy-->
    </td>
  </tr>
    <tr align="center" class="2" id="2_341">
    <td align="left" class="first-cell" >
            <img src="images/menu_minus.gif" id="icon_2_341" width="9" height="9" border="0" style="margin-left:2em" onclick="rowClicked(this)" />
            <span>
      <!-- 0-默认列表 1-搜索匹配列表 其他-搜索未匹配列表 -->
            <a href="goods.php?act=list&cat_id=341&supp=-1">吊灯</a>
            </span>
        </td>
    <td width="10%">0</td>
    <td width="10%"><span onclick="listTable.edit(this, 'edit_measure_unit', 341)"><!--  -->&nbsp;&nbsp;&nbsp;&nbsp;<!--  --></span></td>
    <td width="10%"><img src="images/no.gif" onclick="listTable.toggle(this, 'toggle_show_in_nav', 341)" /></td>
    <td width="10%"><img src="images/yes.gif" onclick="listTable.toggle(this, 'toggle_is_show', 341)" /></td>
    <td><span onclick="listTable.edit(this, 'edit_grade', 341)">0</span></td>
    <td width="10%" align="right"><span onclick="listTable.edit(this, 'edit_sort_order', 341)">50</span></td>
    <td width="24%" align="center">
      <a href="category.php?act=move&cat_id=341">转移商品</a> |
      <a href="category.php?act=edit&amp;cat_id=341">编辑</a> |
      <a href="javascript:;" onclick="listTable.remove(341, '您确认要删除这条记录吗?')" title="移除">移除</a>
	  <!--代码修改_start Byjdy-->
	  	  <!--代码修改_end Byjdy-->
    </td>
  </tr>
    <tr align="center" class="0" id="0_2">
    <td align="left" class="first-cell" >
            <img src="images/menu_minus.gif" id="icon_0_2" width="9" height="9" border="0" style="margin-left:0em" onclick="rowClicked(this)" />
            <span>
      <!-- 0-默认列表 1-搜索匹配列表 其他-搜索未匹配列表 -->
            <a href="goods.php?act=list&cat_id=2&supp=-1">男装、女装、内衣、珠宝</a>
            </span>
        </td>
    <td width="10%">0</td>
    <td width="10%"><span onclick="listTable.edit(this, 'edit_measure_unit', 2)"><!--  -->&nbsp;&nbsp;&nbsp;&nbsp;<!--  --></span></td>
    <td width="10%"><img src="images/no.gif" onclick="listTable.toggle(this, 'toggle_show_in_nav', 2)" /></td>
    <td width="10%"><img src="images/yes.gif" onclick="listTable.toggle(this, 'toggle_is_show', 2)" /></td>
    <td><span onclick="listTable.edit(this, 'edit_grade', 2)">5</span></td>
    <td width="10%" align="right"><span onclick="listTable.edit(this, 'edit_sort_order', 2)">5</span></td>
    <td width="24%" align="center">
      <a href="category.php?act=move&cat_id=2">转移商品</a> |
      <a href="category.php?act=edit&amp;cat_id=2">编辑</a> |
      <a href="javascript:;" onclick="listTable.remove(2, '您确认要删除这条记录吗?')" title="移除">移除</a>
	  <!--代码修改_start Byjdy-->
	  	  | <a href="category_flashimg.php?act=list&amp;cat_id=2">设置轮播图片</a>	  
	  	  <!--代码修改_end Byjdy-->
    </td>
  </tr>
    <tr align="center" class="1" id="1_57">
    <td align="left" class="first-cell" >
            <img src="images/menu_minus.gif" id="icon_1_57" width="9" height="9" border="0" style="margin-left:1em" onclick="rowClicked(this)" />
            <span>
      <!-- 0-默认列表 1-搜索匹配列表 其他-搜索未匹配列表 -->
            <a href="goods.php?act=list&cat_id=57&supp=-1">男装馆</a>
            </span>
        </td>
    <td width="10%">0</td>
    <td width="10%"><span onclick="listTable.edit(this, 'edit_measure_unit', 57)"><!--  -->&nbsp;&nbsp;&nbsp;&nbsp;<!--  --></span></td>
    <td width="10%"><img src="images/no.gif" onclick="listTable.toggle(this, 'toggle_show_in_nav', 57)" /></td>
    <td width="10%"><img src="images/yes.gif" onclick="listTable.toggle(this, 'toggle_is_show', 57)" /></td>
    <td><span onclick="listTable.edit(this, 'edit_grade', 57)">0</span></td>
    <td width="10%" align="right"><span onclick="listTable.edit(this, 'edit_sort_order', 57)">50</span></td>
    <td width="24%" align="center">
      <a href="category.php?act=move&cat_id=57">转移商品</a> |
      <a href="category.php?act=edit&amp;cat_id=57">编辑</a> |
      <a href="javascript:;" onclick="listTable.remove(57, '您确认要删除这条记录吗?')" title="移除">移除</a>
	  <!--代码修改_start Byjdy-->
	  	  <!--代码修改_end Byjdy-->
    </td>
  </tr>
    <tr align="center" class="2" id="2_83">
    <td align="left" class="first-cell" >
            <img src="images/menu_minus.gif" id="icon_2_83" width="9" height="9" border="0" style="margin-left:2em" onclick="rowClicked(this)" />
            <span>
      <!-- 0-默认列表 1-搜索匹配列表 其他-搜索未匹配列表 -->
            <a href="goods.php?act=list&cat_id=83&supp=-1">休闲短裤</a>
            </span>
        </td>
    <td width="10%">0</td>
    <td width="10%"><span onclick="listTable.edit(this, 'edit_measure_unit', 83)"><!--  -->&nbsp;&nbsp;&nbsp;&nbsp;<!--  --></span></td>
    <td width="10%"><img src="images/no.gif" onclick="listTable.toggle(this, 'toggle_show_in_nav', 83)" /></td>
    <td width="10%"><img src="images/yes.gif" onclick="listTable.toggle(this, 'toggle_is_show', 83)" /></td>
    <td><span onclick="listTable.edit(this, 'edit_grade', 83)">0</span></td>
    <td width="10%" align="right"><span onclick="listTable.edit(this, 'edit_sort_order', 83)">50</span></td>
    <td width="24%" align="center">
      <a href="category.php?act=move&cat_id=83">转移商品</a> |
      <a href="category.php?act=edit&amp;cat_id=83">编辑</a> |
      <a href="javascript:;" onclick="listTable.remove(83, '您确认要删除这条记录吗?')" title="移除">移除</a>
	  <!--代码修改_start Byjdy-->
	  	  <!--代码修改_end Byjdy-->
    </td>
  </tr>
    <tr align="center" class="2" id="2_80">
    <td align="left" class="first-cell" >
            <img src="images/menu_minus.gif" id="icon_2_80" width="9" height="9" border="0" style="margin-left:2em" onclick="rowClicked(this)" />
            <span>
      <!-- 0-默认列表 1-搜索匹配列表 其他-搜索未匹配列表 -->
            <a href="goods.php?act=list&cat_id=80&supp=-1">风衣</a>
            </span>
        </td>
    <td width="10%">0</td>
    <td width="10%"><span onclick="listTable.edit(this, 'edit_measure_unit', 80)"><!--  -->&nbsp;&nbsp;&nbsp;&nbsp;<!--  --></span></td>
    <td width="10%"><img src="images/no.gif" onclick="listTable.toggle(this, 'toggle_show_in_nav', 80)" /></td>
    <td width="10%"><img src="images/yes.gif" onclick="listTable.toggle(this, 'toggle_is_show', 80)" /></td>
    <td><span onclick="listTable.edit(this, 'edit_grade', 80)">0</span></td>
    <td width="10%" align="right"><span onclick="listTable.edit(this, 'edit_sort_order', 80)">50</span></td>
    <td width="24%" align="center">
      <a href="category.php?act=move&cat_id=80">转移商品</a> |
      <a href="category.php?act=edit&amp;cat_id=80">编辑</a> |
      <a href="javascript:;" onclick="listTable.remove(80, '您确认要删除这条记录吗?')" title="移除">移除</a>
	  <!--代码修改_start Byjdy-->
	  	  <!--代码修改_end Byjdy-->
    </td>
  </tr>
    <tr align="center" class="2" id="2_77">
    <td align="left" class="first-cell" >
            <img src="images/menu_minus.gif" id="icon_2_77" width="9" height="9" border="0" style="margin-left:2em" onclick="rowClicked(this)" />
            <span>
      <!-- 0-默认列表 1-搜索匹配列表 其他-搜索未匹配列表 -->
            <a href="goods.php?act=list&cat_id=77&supp=-1">休闲裤</a>
            </span>
        </td>
    <td width="10%">0</td>
    <td width="10%"><span onclick="listTable.edit(this, 'edit_measure_unit', 77)"><!--  -->&nbsp;&nbsp;&nbsp;&nbsp;<!--  --></span></td>
    <td width="10%"><img src="images/no.gif" onclick="listTable.toggle(this, 'toggle_show_in_nav', 77)" /></td>
    <td width="10%"><img src="images/yes.gif" onclick="listTable.toggle(this, 'toggle_is_show', 77)" /></td>
    <td><span onclick="listTable.edit(this, 'edit_grade', 77)">0</span></td>
    <td width="10%" align="right"><span onclick="listTable.edit(this, 'edit_sort_order', 77)">50</span></td>
    <td width="24%" align="center">
      <a href="category.php?act=move&cat_id=77">转移商品</a> |
      <a href="category.php?act=edit&amp;cat_id=77">编辑</a> |
      <a href="javascript:;" onclick="listTable.remove(77, '您确认要删除这条记录吗?')" title="移除">移除</a>
	  <!--代码修改_start Byjdy-->
	  	  <!--代码修改_end Byjdy-->
    </td>
  </tr>
    <tr align="center" class="2" id="2_84">
    <td align="left" class="first-cell" >
            <img src="images/menu_minus.gif" id="icon_2_84" width="9" height="9" border="0" style="margin-left:2em" onclick="rowClicked(this)" />
            <span>
      <!-- 0-默认列表 1-搜索匹配列表 其他-搜索未匹配列表 -->
            <a href="goods.php?act=list&cat_id=84&supp=-1">POLO衫</a>
            </span>
        </td>
    <td width="10%">8</td>
    <td width="10%"><span onclick="listTable.edit(this, 'edit_measure_unit', 84)"><!--  -->&nbsp;&nbsp;&nbsp;&nbsp;<!--  --></span></td>
    <td width="10%"><img src="images/no.gif" onclick="listTable.toggle(this, 'toggle_show_in_nav', 84)" /></td>
    <td width="10%"><img src="images/yes.gif" onclick="listTable.toggle(this, 'toggle_is_show', 84)" /></td>
    <td><span onclick="listTable.edit(this, 'edit_grade', 84)">0</span></td>
    <td width="10%" align="right"><span onclick="listTable.edit(this, 'edit_sort_order', 84)">50</span></td>
    <td width="24%" align="center">
      <a href="category.php?act=move&cat_id=84">转移商品</a> |
      <a href="category.php?act=edit&amp;cat_id=84">编辑</a> |
      <a href="javascript:;" onclick="listTable.remove(84, '您确认要删除这条记录吗?')" title="移除">移除</a>
	  <!--代码修改_start Byjdy-->
	  	  <!--代码修改_end Byjdy-->
    </td>
  </tr>
    <tr align="center" class="2" id="2_81">
    <td align="left" class="first-cell" >
            <img src="images/menu_minus.gif" id="icon_2_81" width="9" height="9" border="0" style="margin-left:2em" onclick="rowClicked(this)" />
            <span>
      <!-- 0-默认列表 1-搜索匹配列表 其他-搜索未匹配列表 -->
            <a href="goods.php?act=list&cat_id=81&supp=-1">针织衫</a>
            </span>
        </td>
    <td width="10%">0</td>
    <td width="10%"><span onclick="listTable.edit(this, 'edit_measure_unit', 81)"><!--  -->&nbsp;&nbsp;&nbsp;&nbsp;<!--  --></span></td>
    <td width="10%"><img src="images/no.gif" onclick="listTable.toggle(this, 'toggle_show_in_nav', 81)" /></td>
    <td width="10%"><img src="images/yes.gif" onclick="listTable.toggle(this, 'toggle_is_show', 81)" /></td>
    <td><span onclick="listTable.edit(this, 'edit_grade', 81)">0</span></td>
    <td width="10%" align="right"><span onclick="listTable.edit(this, 'edit_sort_order', 81)">50</span></td>
    <td width="24%" align="center">
      <a href="category.php?act=move&cat_id=81">转移商品</a> |
      <a href="category.php?act=edit&amp;cat_id=81">编辑</a> |
      <a href="javascript:;" onclick="listTable.remove(81, '您确认要删除这条记录吗?')" title="移除">移除</a>
	  <!--代码修改_start Byjdy-->
	  	  <!--代码修改_end Byjdy-->
    </td>
  </tr>
    <tr align="center" class="2" id="2_78">
    <td align="left" class="first-cell" >
            <img src="images/menu_minus.gif" id="icon_2_78" width="9" height="9" border="0" style="margin-left:2em" onclick="rowClicked(this)" />
            <span>
      <!-- 0-默认列表 1-搜索匹配列表 其他-搜索未匹配列表 -->
            <a href="goods.php?act=list&cat_id=78&supp=-1">牛仔裤</a>
            </span>
        </td>
    <td width="10%">0</td>
    <td width="10%"><span onclick="listTable.edit(this, 'edit_measure_unit', 78)"><!--  -->&nbsp;&nbsp;&nbsp;&nbsp;<!--  --></span></td>
    <td width="10%"><img src="images/no.gif" onclick="listTable.toggle(this, 'toggle_show_in_nav', 78)" /></td>
    <td width="10%"><img src="images/yes.gif" onclick="listTable.toggle(this, 'toggle_is_show', 78)" /></td>
    <td><span onclick="listTable.edit(this, 'edit_grade', 78)">0</span></td>
    <td width="10%" align="right"><span onclick="listTable.edit(this, 'edit_sort_order', 78)">50</span></td>
    <td width="24%" align="center">
      <a href="category.php?act=move&cat_id=78">转移商品</a> |
      <a href="category.php?act=edit&amp;cat_id=78">编辑</a> |
      <a href="javascript:;" onclick="listTable.remove(78, '您确认要删除这条记录吗?')" title="移除">移除</a>
	  <!--代码修改_start Byjdy-->
	  	  <!--代码修改_end Byjdy-->
    </td>
  </tr>
    <tr align="center" class="2" id="2_82">
    <td align="left" class="first-cell" >
            <img src="images/menu_minus.gif" id="icon_2_82" width="9" height="9" border="0" style="margin-left:2em" onclick="rowClicked(this)" />
            <span>
      <!-- 0-默认列表 1-搜索匹配列表 其他-搜索未匹配列表 -->
            <a href="goods.php?act=list&cat_id=82&supp=-1">长袖衬衫</a>
            </span>
        </td>
    <td width="10%">0</td>
    <td width="10%"><span onclick="listTable.edit(this, 'edit_measure_unit', 82)"><!--  -->&nbsp;&nbsp;&nbsp;&nbsp;<!--  --></span></td>
    <td width="10%"><img src="images/no.gif" onclick="listTable.toggle(this, 'toggle_show_in_nav', 82)" /></td>
    <td width="10%"><img src="images/yes.gif" onclick="listTable.toggle(this, 'toggle_is_show', 82)" /></td>
    <td><span onclick="listTable.edit(this, 'edit_grade', 82)">0</span></td>
    <td width="10%" align="right"><span onclick="listTable.edit(this, 'edit_sort_order', 82)">50</span></td>
    <td width="24%" align="center">
      <a href="category.php?act=move&cat_id=82">转移商品</a> |
      <a href="category.php?act=edit&amp;cat_id=82">编辑</a> |
      <a href="javascript:;" onclick="listTable.remove(82, '您确认要删除这条记录吗?')" title="移除">移除</a>
	  <!--代码修改_start Byjdy-->
	  	  <!--代码修改_end Byjdy-->
    </td>
  </tr>
    <tr align="center" class="2" id="2_79">
    <td align="left" class="first-cell" >
            <img src="images/menu_minus.gif" id="icon_2_79" width="9" height="9" border="0" style="margin-left:2em" onclick="rowClicked(this)" />
            <span>
      <!-- 0-默认列表 1-搜索匹配列表 其他-搜索未匹配列表 -->
            <a href="goods.php?act=list&cat_id=79&supp=-1">夹克</a>
            </span>
        </td>
    <td width="10%">0</td>
    <td width="10%"><span onclick="listTable.edit(this, 'edit_measure_unit', 79)"><!--  -->&nbsp;&nbsp;&nbsp;&nbsp;<!--  --></span></td>
    <td width="10%"><img src="images/no.gif" onclick="listTable.toggle(this, 'toggle_show_in_nav', 79)" /></td>
    <td width="10%"><img src="images/yes.gif" onclick="listTable.toggle(this, 'toggle_is_show', 79)" /></td>
    <td><span onclick="listTable.edit(this, 'edit_grade', 79)">0</span></td>
    <td width="10%" align="right"><span onclick="listTable.edit(this, 'edit_sort_order', 79)">50</span></td>
    <td width="24%" align="center">
      <a href="category.php?act=move&cat_id=79">转移商品</a> |
      <a href="category.php?act=edit&amp;cat_id=79">编辑</a> |
      <a href="javascript:;" onclick="listTable.remove(79, '您确认要删除这条记录吗?')" title="移除">移除</a>
	  <!--代码修改_start Byjdy-->
	  	  <!--代码修改_end Byjdy-->
    </td>
  </tr>
    <tr align="center" class="1" id="1_55">
    <td align="left" class="first-cell" >
            <img src="images/menu_minus.gif" id="icon_1_55" width="9" height="9" border="0" style="margin-left:1em" onclick="rowClicked(this)" />
            <span>
      <!-- 0-默认列表 1-搜索匹配列表 其他-搜索未匹配列表 -->
            <a href="goods.php?act=list&cat_id=55&supp=-1">女装馆</a>
            </span>
        </td>
    <td width="10%">0</td>
    <td width="10%"><span onclick="listTable.edit(this, 'edit_measure_unit', 55)"><!--  -->&nbsp;&nbsp;&nbsp;&nbsp;<!--  --></span></td>
    <td width="10%"><img src="images/no.gif" onclick="listTable.toggle(this, 'toggle_show_in_nav', 55)" /></td>
    <td width="10%"><img src="images/yes.gif" onclick="listTable.toggle(this, 'toggle_is_show', 55)" /></td>
    <td><span onclick="listTable.edit(this, 'edit_grade', 55)">0</span></td>
    <td width="10%" align="right"><span onclick="listTable.edit(this, 'edit_sort_order', 55)">50</span></td>
    <td width="24%" align="center">
      <a href="category.php?act=move&cat_id=55">转移商品</a> |
      <a href="category.php?act=edit&amp;cat_id=55">编辑</a> |
      <a href="javascript:;" onclick="listTable.remove(55, '您确认要删除这条记录吗?')" title="移除">移除</a>
	  <!--代码修改_start Byjdy-->
	  	  <!--代码修改_end Byjdy-->
    </td>
  </tr>
    <tr align="center" class="2" id="2_67">
    <td align="left" class="first-cell" >
            <img src="images/menu_minus.gif" id="icon_2_67" width="9" height="9" border="0" style="margin-left:2em" onclick="rowClicked(this)" />
            <span>
      <!-- 0-默认列表 1-搜索匹配列表 其他-搜索未匹配列表 -->
            <a href="goods.php?act=list&cat_id=67&supp=-1">短外套</a>
            </span>
        </td>
    <td width="10%">0</td>
    <td width="10%"><span onclick="listTable.edit(this, 'edit_measure_unit', 67)"><!--  -->&nbsp;&nbsp;&nbsp;&nbsp;<!--  --></span></td>
    <td width="10%"><img src="images/no.gif" onclick="listTable.toggle(this, 'toggle_show_in_nav', 67)" /></td>
    <td width="10%"><img src="images/yes.gif" onclick="listTable.toggle(this, 'toggle_is_show', 67)" /></td>
    <td><span onclick="listTable.edit(this, 'edit_grade', 67)">0</span></td>
    <td width="10%" align="right"><span onclick="listTable.edit(this, 'edit_sort_order', 67)">50</span></td>
    <td width="24%" align="center">
      <a href="category.php?act=move&cat_id=67">转移商品</a> |
      <a href="category.php?act=edit&amp;cat_id=67">编辑</a> |
      <a href="javascript:;" onclick="listTable.remove(67, '您确认要删除这条记录吗?')" title="移除">移除</a>
	  <!--代码修改_start Byjdy-->
	  	  <!--代码修改_end Byjdy-->
    </td>
  </tr>
    <tr align="center" class="2" id="2_64">
    <td align="left" class="first-cell" >
            <img src="images/menu_minus.gif" id="icon_2_64" width="9" height="9" border="0" style="margin-left:2em" onclick="rowClicked(this)" />
            <span>
      <!-- 0-默认列表 1-搜索匹配列表 其他-搜索未匹配列表 -->
            <a href="goods.php?act=list&cat_id=64&supp=-1">时尚套装</a>
            </span>
        </td>
    <td width="10%">0</td>
    <td width="10%"><span onclick="listTable.edit(this, 'edit_measure_unit', 64)"><!--  -->&nbsp;&nbsp;&nbsp;&nbsp;<!--  --></span></td>
    <td width="10%"><img src="images/no.gif" onclick="listTable.toggle(this, 'toggle_show_in_nav', 64)" /></td>
    <td width="10%"><img src="images/yes.gif" onclick="listTable.toggle(this, 'toggle_is_show', 64)" /></td>
    <td><span onclick="listTable.edit(this, 'edit_grade', 64)">0</span></td>
    <td width="10%" align="right"><span onclick="listTable.edit(this, 'edit_sort_order', 64)">50</span></td>
    <td width="24%" align="center">
      <a href="category.php?act=move&cat_id=64">转移商品</a> |
      <a href="category.php?act=edit&amp;cat_id=64">编辑</a> |
      <a href="javascript:;" onclick="listTable.remove(64, '您确认要删除这条记录吗?')" title="移除">移除</a>
	  <!--代码修改_start Byjdy-->
	  	  <!--代码修改_end Byjdy-->
    </td>
  </tr>
    <tr align="center" class="2" id="2_61">
    <td align="left" class="first-cell" >
            <img src="images/menu_minus.gif" id="icon_2_61" width="9" height="9" border="0" style="margin-left:2em" onclick="rowClicked(this)" />
            <span>
      <!-- 0-默认列表 1-搜索匹配列表 其他-搜索未匹配列表 -->
            <a href="goods.php?act=list&cat_id=61&supp=-1">连衣裙</a>
            </span>
        </td>
    <td width="10%">9</td>
    <td width="10%"><span onclick="listTable.edit(this, 'edit_measure_unit', 61)"><!--  -->&nbsp;&nbsp;&nbsp;&nbsp;<!--  --></span></td>
    <td width="10%"><img src="images/no.gif" onclick="listTable.toggle(this, 'toggle_show_in_nav', 61)" /></td>
    <td width="10%"><img src="images/yes.gif" onclick="listTable.toggle(this, 'toggle_is_show', 61)" /></td>
    <td><span onclick="listTable.edit(this, 'edit_grade', 61)">0</span></td>
    <td width="10%" align="right"><span onclick="listTable.edit(this, 'edit_sort_order', 61)">50</span></td>
    <td width="24%" align="center">
      <a href="category.php?act=move&cat_id=61">转移商品</a> |
      <a href="category.php?act=edit&amp;cat_id=61">编辑</a> |
      <a href="javascript:;" onclick="listTable.remove(61, '您确认要删除这条记录吗?')" title="移除">移除</a>
	  <!--代码修改_start Byjdy-->
	  	  <!--代码修改_end Byjdy-->
    </td>
  </tr>
    <tr align="center" class="2" id="2_68">
    <td align="left" class="first-cell" >
            <img src="images/menu_minus.gif" id="icon_2_68" width="9" height="9" border="0" style="margin-left:2em" onclick="rowClicked(this)" />
            <span>
      <!-- 0-默认列表 1-搜索匹配列表 其他-搜索未匹配列表 -->
            <a href="goods.php?act=list&cat_id=68&supp=-1">防晒衫</a>
            </span>
        </td>
    <td width="10%">0</td>
    <td width="10%"><span onclick="listTable.edit(this, 'edit_measure_unit', 68)"><!--  -->&nbsp;&nbsp;&nbsp;&nbsp;<!--  --></span></td>
    <td width="10%"><img src="images/no.gif" onclick="listTable.toggle(this, 'toggle_show_in_nav', 68)" /></td>
    <td width="10%"><img src="images/yes.gif" onclick="listTable.toggle(this, 'toggle_is_show', 68)" /></td>
    <td><span onclick="listTable.edit(this, 'edit_grade', 68)">0</span></td>
    <td width="10%" align="right"><span onclick="listTable.edit(this, 'edit_sort_order', 68)">50</span></td>
    <td width="24%" align="center">
      <a href="category.php?act=move&cat_id=68">转移商品</a> |
      <a href="category.php?act=edit&amp;cat_id=68">编辑</a> |
      <a href="javascript:;" onclick="listTable.remove(68, '您确认要删除这条记录吗?')" title="移除">移除</a>
	  <!--代码修改_start Byjdy-->
	  	  <!--代码修改_end Byjdy-->
    </td>
  </tr>
    <tr align="center" class="2" id="2_65">
    <td align="left" class="first-cell" >
            <img src="images/menu_minus.gif" id="icon_2_65" width="9" height="9" border="0" style="margin-left:2em" onclick="rowClicked(this)" />
            <span>
      <!-- 0-默认列表 1-搜索匹配列表 其他-搜索未匹配列表 -->
            <a href="goods.php?act=list&cat_id=65&supp=-1">复古旗袍</a>
            </span>
        </td>
    <td width="10%">0</td>
    <td width="10%"><span onclick="listTable.edit(this, 'edit_measure_unit', 65)"><!--  -->&nbsp;&nbsp;&nbsp;&nbsp;<!--  --></span></td>
    <td width="10%"><img src="images/no.gif" onclick="listTable.toggle(this, 'toggle_show_in_nav', 65)" /></td>
    <td width="10%"><img src="images/yes.gif" onclick="listTable.toggle(this, 'toggle_is_show', 65)" /></td>
    <td><span onclick="listTable.edit(this, 'edit_grade', 65)">0</span></td>
    <td width="10%" align="right"><span onclick="listTable.edit(this, 'edit_sort_order', 65)">50</span></td>
    <td width="24%" align="center">
      <a href="category.php?act=move&cat_id=65">转移商品</a> |
      <a href="category.php?act=edit&amp;cat_id=65">编辑</a> |
      <a href="javascript:;" onclick="listTable.remove(65, '您确认要删除这条记录吗?')" title="移除">移除</a>
	  <!--代码修改_start Byjdy-->
	  	  <!--代码修改_end Byjdy-->
    </td>
  </tr>
    <tr align="center" class="2" id="2_62">
    <td align="left" class="first-cell" >
            <img src="images/menu_minus.gif" id="icon_2_62" width="9" height="9" border="0" style="margin-left:2em" onclick="rowClicked(this)" />
            <span>
      <!-- 0-默认列表 1-搜索匹配列表 其他-搜索未匹配列表 -->
            <a href="goods.php?act=list&cat_id=62&supp=-1">连体裤</a>
            </span>
        </td>
    <td width="10%">0</td>
    <td width="10%"><span onclick="listTable.edit(this, 'edit_measure_unit', 62)"><!--  -->&nbsp;&nbsp;&nbsp;&nbsp;<!--  --></span></td>
    <td width="10%"><img src="images/no.gif" onclick="listTable.toggle(this, 'toggle_show_in_nav', 62)" /></td>
    <td width="10%"><img src="images/yes.gif" onclick="listTable.toggle(this, 'toggle_is_show', 62)" /></td>
    <td><span onclick="listTable.edit(this, 'edit_grade', 62)">0</span></td>
    <td width="10%" align="right"><span onclick="listTable.edit(this, 'edit_sort_order', 62)">50</span></td>
    <td width="24%" align="center">
      <a href="category.php?act=move&cat_id=62">转移商品</a> |
      <a href="category.php?act=edit&amp;cat_id=62">编辑</a> |
      <a href="javascript:;" onclick="listTable.remove(62, '您确认要删除这条记录吗?')" title="移除">移除</a>
	  <!--代码修改_start Byjdy-->
	  	  <!--代码修改_end Byjdy-->
    </td>
  </tr>
    <tr align="center" class="2" id="2_66">
    <td align="left" class="first-cell" >
            <img src="images/menu_minus.gif" id="icon_2_66" width="9" height="9" border="0" style="margin-left:2em" onclick="rowClicked(this)" />
            <span>
      <!-- 0-默认列表 1-搜索匹配列表 其他-搜索未匹配列表 -->
            <a href="goods.php?act=list&cat_id=66&supp=-1">牛仔裤</a>
            </span>
        </td>
    <td width="10%">0</td>
    <td width="10%"><span onclick="listTable.edit(this, 'edit_measure_unit', 66)"><!--  -->&nbsp;&nbsp;&nbsp;&nbsp;<!--  --></span></td>
    <td width="10%"><img src="images/no.gif" onclick="listTable.toggle(this, 'toggle_show_in_nav', 66)" /></td>
    <td width="10%"><img src="images/yes.gif" onclick="listTable.toggle(this, 'toggle_is_show', 66)" /></td>
    <td><span onclick="listTable.edit(this, 'edit_grade', 66)">0</span></td>
    <td width="10%" align="right"><span onclick="listTable.edit(this, 'edit_sort_order', 66)">50</span></td>
    <td width="24%" align="center">
      <a href="category.php?act=move&cat_id=66">转移商品</a> |
      <a href="category.php?act=edit&amp;cat_id=66">编辑</a> |
      <a href="javascript:;" onclick="listTable.remove(66, '您确认要删除这条记录吗?')" title="移除">移除</a>
	  <!--代码修改_start Byjdy-->
	  	  <!--代码修改_end Byjdy-->
    </td>
  </tr>
    <tr align="center" class="2" id="2_63">
    <td align="left" class="first-cell" >
            <img src="images/menu_minus.gif" id="icon_2_63" width="9" height="9" border="0" style="margin-left:2em" onclick="rowClicked(this)" />
            <span>
      <!-- 0-默认列表 1-搜索匹配列表 其他-搜索未匹配列表 -->
            <a href="goods.php?act=list&cat_id=63&supp=-1">棉麻T恤</a>
            </span>
        </td>
    <td width="10%">0</td>
    <td width="10%"><span onclick="listTable.edit(this, 'edit_measure_unit', 63)"><!--  -->&nbsp;&nbsp;&nbsp;&nbsp;<!--  --></span></td>
    <td width="10%"><img src="images/no.gif" onclick="listTable.toggle(this, 'toggle_show_in_nav', 63)" /></td>
    <td width="10%"><img src="images/yes.gif" onclick="listTable.toggle(this, 'toggle_is_show', 63)" /></td>
    <td><span onclick="listTable.edit(this, 'edit_grade', 63)">0</span></td>
    <td width="10%" align="right"><span onclick="listTable.edit(this, 'edit_sort_order', 63)">50</span></td>
    <td width="24%" align="center">
      <a href="category.php?act=move&cat_id=63">转移商品</a> |
      <a href="category.php?act=edit&amp;cat_id=63">编辑</a> |
      <a href="javascript:;" onclick="listTable.remove(63, '您确认要删除这条记录吗?')" title="移除">移除</a>
	  <!--代码修改_start Byjdy-->
	  	  <!--代码修改_end Byjdy-->
    </td>
  </tr>
    <tr align="center" class="1" id="1_58">
    <td align="left" class="first-cell" >
            <img src="images/menu_minus.gif" id="icon_1_58" width="9" height="9" border="0" style="margin-left:1em" onclick="rowClicked(this)" />
            <span>
      <!-- 0-默认列表 1-搜索匹配列表 其他-搜索未匹配列表 -->
            <a href="goods.php?act=list&cat_id=58&supp=-1">户外鞋服</a>
            </span>
        </td>
    <td width="10%">0</td>
    <td width="10%"><span onclick="listTable.edit(this, 'edit_measure_unit', 58)"><!--  -->&nbsp;&nbsp;&nbsp;&nbsp;<!--  --></span></td>
    <td width="10%"><img src="images/no.gif" onclick="listTable.toggle(this, 'toggle_show_in_nav', 58)" /></td>
    <td width="10%"><img src="images/yes.gif" onclick="listTable.toggle(this, 'toggle_is_show', 58)" /></td>
    <td><span onclick="listTable.edit(this, 'edit_grade', 58)">0</span></td>
    <td width="10%" align="right"><span onclick="listTable.edit(this, 'edit_sort_order', 58)">50</span></td>
    <td width="24%" align="center">
      <a href="category.php?act=move&cat_id=58">转移商品</a> |
      <a href="category.php?act=edit&amp;cat_id=58">编辑</a> |
      <a href="javascript:;" onclick="listTable.remove(58, '您确认要删除这条记录吗?')" title="移除">移除</a>
	  <!--代码修改_start Byjdy-->
	  	  <!--代码修改_end Byjdy-->
    </td>
  </tr>
    <tr align="center" class="2" id="2_89">
    <td align="left" class="first-cell" >
            <img src="images/menu_minus.gif" id="icon_2_89" width="9" height="9" border="0" style="margin-left:2em" onclick="rowClicked(this)" />
            <span>
      <!-- 0-默认列表 1-搜索匹配列表 其他-搜索未匹配列表 -->
            <a href="goods.php?act=list&cat_id=89&supp=-1">迷彩裤</a>
            </span>
        </td>
    <td width="10%">0</td>
    <td width="10%"><span onclick="listTable.edit(this, 'edit_measure_unit', 89)"><!--  -->&nbsp;&nbsp;&nbsp;&nbsp;<!--  --></span></td>
    <td width="10%"><img src="images/no.gif" onclick="listTable.toggle(this, 'toggle_show_in_nav', 89)" /></td>
    <td width="10%"><img src="images/yes.gif" onclick="listTable.toggle(this, 'toggle_is_show', 89)" /></td>
    <td><span onclick="listTable.edit(this, 'edit_grade', 89)">0</span></td>
    <td width="10%" align="right"><span onclick="listTable.edit(this, 'edit_sort_order', 89)">50</span></td>
    <td width="24%" align="center">
      <a href="category.php?act=move&cat_id=89">转移商品</a> |
      <a href="category.php?act=edit&amp;cat_id=89">编辑</a> |
      <a href="javascript:;" onclick="listTable.remove(89, '您确认要删除这条记录吗?')" title="移除">移除</a>
	  <!--代码修改_start Byjdy-->
	  	  <!--代码修改_end Byjdy-->
    </td>
  </tr>
    <tr align="center" class="2" id="2_86">
    <td align="left" class="first-cell" >
            <img src="images/menu_minus.gif" id="icon_2_86" width="9" height="9" border="0" style="margin-left:2em" onclick="rowClicked(this)" />
            <span>
      <!-- 0-默认列表 1-搜索匹配列表 其他-搜索未匹配列表 -->
            <a href="goods.php?act=list&cat_id=86&supp=-1">篮球鞋</a>
            </span>
        </td>
    <td width="10%">0</td>
    <td width="10%"><span onclick="listTable.edit(this, 'edit_measure_unit', 86)"><!--  -->&nbsp;&nbsp;&nbsp;&nbsp;<!--  --></span></td>
    <td width="10%"><img src="images/no.gif" onclick="listTable.toggle(this, 'toggle_show_in_nav', 86)" /></td>
    <td width="10%"><img src="images/yes.gif" onclick="listTable.toggle(this, 'toggle_is_show', 86)" /></td>
    <td><span onclick="listTable.edit(this, 'edit_grade', 86)">0</span></td>
    <td width="10%" align="right"><span onclick="listTable.edit(this, 'edit_sort_order', 86)">50</span></td>
    <td width="24%" align="center">
      <a href="category.php?act=move&cat_id=86">转移商品</a> |
      <a href="category.php?act=edit&amp;cat_id=86">编辑</a> |
      <a href="javascript:;" onclick="listTable.remove(86, '您确认要删除这条记录吗?')" title="移除">移除</a>
	  <!--代码修改_start Byjdy-->
	  	  <!--代码修改_end Byjdy-->
    </td>
  </tr>
    <tr align="center" class="2" id="2_90">
    <td align="left" class="first-cell" >
            <img src="images/menu_minus.gif" id="icon_2_90" width="9" height="9" border="0" style="margin-left:2em" onclick="rowClicked(this)" />
            <span>
      <!-- 0-默认列表 1-搜索匹配列表 其他-搜索未匹配列表 -->
            <a href="goods.php?act=list&cat_id=90&supp=-1">沙滩鞋</a>
            </span>
        </td>
    <td width="10%">0</td>
    <td width="10%"><span onclick="listTable.edit(this, 'edit_measure_unit', 90)"><!--  -->&nbsp;&nbsp;&nbsp;&nbsp;<!--  --></span></td>
    <td width="10%"><img src="images/no.gif" onclick="listTable.toggle(this, 'toggle_show_in_nav', 90)" /></td>
    <td width="10%"><img src="images/yes.gif" onclick="listTable.toggle(this, 'toggle_is_show', 90)" /></td>
    <td><span onclick="listTable.edit(this, 'edit_grade', 90)">0</span></td>
    <td width="10%" align="right"><span onclick="listTable.edit(this, 'edit_sort_order', 90)">50</span></td>
    <td width="24%" align="center">
      <a href="category.php?act=move&cat_id=90">转移商品</a> |
      <a href="category.php?act=edit&amp;cat_id=90">编辑</a> |
      <a href="javascript:;" onclick="listTable.remove(90, '您确认要删除这条记录吗?')" title="移除">移除</a>
	  <!--代码修改_start Byjdy-->
	  	  <!--代码修改_end Byjdy-->
    </td>
  </tr>
    <tr align="center" class="2" id="2_87">
    <td align="left" class="first-cell" >
            <img src="images/menu_minus.gif" id="icon_2_87" width="9" height="9" border="0" style="margin-left:2em" onclick="rowClicked(this)" />
            <span>
      <!-- 0-默认列表 1-搜索匹配列表 其他-搜索未匹配列表 -->
            <a href="goods.php?act=list&cat_id=87&supp=-1">帆布鞋</a>
            </span>
        </td>
    <td width="10%">0</td>
    <td width="10%"><span onclick="listTable.edit(this, 'edit_measure_unit', 87)"><!--  -->&nbsp;&nbsp;&nbsp;&nbsp;<!--  --></span></td>
    <td width="10%"><img src="images/no.gif" onclick="listTable.toggle(this, 'toggle_show_in_nav', 87)" /></td>
    <td width="10%"><img src="images/yes.gif" onclick="listTable.toggle(this, 'toggle_is_show', 87)" /></td>
    <td><span onclick="listTable.edit(this, 'edit_grade', 87)">0</span></td>
    <td width="10%" align="right"><span onclick="listTable.edit(this, 'edit_sort_order', 87)">50</span></td>
    <td width="24%" align="center">
      <a href="category.php?act=move&cat_id=87">转移商品</a> |
      <a href="category.php?act=edit&amp;cat_id=87">编辑</a> |
      <a href="javascript:;" onclick="listTable.remove(87, '您确认要删除这条记录吗?')" title="移除">移除</a>
	  <!--代码修改_start Byjdy-->
	  	  <!--代码修改_end Byjdy-->
    </td>
  </tr>
    <tr align="center" class="2" id="2_91">
    <td align="left" class="first-cell" >
            <img src="images/menu_minus.gif" id="icon_2_91" width="9" height="9" border="0" style="margin-left:2em" onclick="rowClicked(this)" />
            <span>
      <!-- 0-默认列表 1-搜索匹配列表 其他-搜索未匹配列表 -->
            <a href="goods.php?act=list&cat_id=91&supp=-1">钓鱼服</a>
            </span>
        </td>
    <td width="10%">0</td>
    <td width="10%"><span onclick="listTable.edit(this, 'edit_measure_unit', 91)"><!--  -->&nbsp;&nbsp;&nbsp;&nbsp;<!--  --></span></td>
    <td width="10%"><img src="images/no.gif" onclick="listTable.toggle(this, 'toggle_show_in_nav', 91)" /></td>
    <td width="10%"><img src="images/yes.gif" onclick="listTable.toggle(this, 'toggle_is_show', 91)" /></td>
    <td><span onclick="listTable.edit(this, 'edit_grade', 91)">0</span></td>
    <td width="10%" align="right"><span onclick="listTable.edit(this, 'edit_sort_order', 91)">50</span></td>
    <td width="24%" align="center">
      <a href="category.php?act=move&cat_id=91">转移商品</a> |
      <a href="category.php?act=edit&amp;cat_id=91">编辑</a> |
      <a href="javascript:;" onclick="listTable.remove(91, '您确认要删除这条记录吗?')" title="移除">移除</a>
	  <!--代码修改_start Byjdy-->
	  	  <!--代码修改_end Byjdy-->
    </td>
  </tr>
    <tr align="center" class="2" id="2_88">
    <td align="left" class="first-cell" >
            <img src="images/menu_minus.gif" id="icon_2_88" width="9" height="9" border="0" style="margin-left:2em" onclick="rowClicked(this)" />
            <span>
      <!-- 0-默认列表 1-搜索匹配列表 其他-搜索未匹配列表 -->
            <a href="goods.php?act=list&cat_id=88&supp=-1">羽毛球鞋</a>
            </span>
        </td>
    <td width="10%">0</td>
    <td width="10%"><span onclick="listTable.edit(this, 'edit_measure_unit', 88)"><!--  -->&nbsp;&nbsp;&nbsp;&nbsp;<!--  --></span></td>
    <td width="10%"><img src="images/no.gif" onclick="listTable.toggle(this, 'toggle_show_in_nav', 88)" /></td>
    <td width="10%"><img src="images/yes.gif" onclick="listTable.toggle(this, 'toggle_is_show', 88)" /></td>
    <td><span onclick="listTable.edit(this, 'edit_grade', 88)">0</span></td>
    <td width="10%" align="right"><span onclick="listTable.edit(this, 'edit_sort_order', 88)">50</span></td>
    <td width="24%" align="center">
      <a href="category.php?act=move&cat_id=88">转移商品</a> |
      <a href="category.php?act=edit&amp;cat_id=88">编辑</a> |
      <a href="javascript:;" onclick="listTable.remove(88, '您确认要删除这条记录吗?')" title="移除">移除</a>
	  <!--代码修改_start Byjdy-->
	  	  <!--代码修改_end Byjdy-->
    </td>
  </tr>
    <tr align="center" class="2" id="2_85">
    <td align="left" class="first-cell" >
            <img src="images/menu_minus.gif" id="icon_2_85" width="9" height="9" border="0" style="margin-left:2em" onclick="rowClicked(this)" />
            <span>
      <!-- 0-默认列表 1-搜索匹配列表 其他-搜索未匹配列表 -->
            <a href="goods.php?act=list&cat_id=85&supp=-1">跑步鞋</a>
            </span>
        </td>
    <td width="10%">0</td>
    <td width="10%"><span onclick="listTable.edit(this, 'edit_measure_unit', 85)"><!--  -->&nbsp;&nbsp;&nbsp;&nbsp;<!--  --></span></td>
    <td width="10%"><img src="images/no.gif" onclick="listTable.toggle(this, 'toggle_show_in_nav', 85)" /></td>
    <td width="10%"><img src="images/yes.gif" onclick="listTable.toggle(this, 'toggle_is_show', 85)" /></td>
    <td><span onclick="listTable.edit(this, 'edit_grade', 85)">0</span></td>
    <td width="10%" align="right"><span onclick="listTable.edit(this, 'edit_sort_order', 85)">50</span></td>
    <td width="24%" align="center">
      <a href="category.php?act=move&cat_id=85">转移商品</a> |
      <a href="category.php?act=edit&amp;cat_id=85">编辑</a> |
      <a href="javascript:;" onclick="listTable.remove(85, '您确认要删除这条记录吗?')" title="移除">移除</a>
	  <!--代码修改_start Byjdy-->
	  	  <!--代码修改_end Byjdy-->
    </td>
  </tr>
    <tr align="center" class="2" id="2_92">
    <td align="left" class="first-cell" >
            <img src="images/menu_minus.gif" id="icon_2_92" width="9" height="9" border="0" style="margin-left:2em" onclick="rowClicked(this)" />
            <span>
      <!-- 0-默认列表 1-搜索匹配列表 其他-搜索未匹配列表 -->
            <a href="goods.php?act=list&cat_id=92&supp=-1">登山鞋</a>
            </span>
        </td>
    <td width="10%">0</td>
    <td width="10%"><span onclick="listTable.edit(this, 'edit_measure_unit', 92)"><!--  -->&nbsp;&nbsp;&nbsp;&nbsp;<!--  --></span></td>
    <td width="10%"><img src="images/no.gif" onclick="listTable.toggle(this, 'toggle_show_in_nav', 92)" /></td>
    <td width="10%"><img src="images/yes.gif" onclick="listTable.toggle(this, 'toggle_is_show', 92)" /></td>
    <td><span onclick="listTable.edit(this, 'edit_grade', 92)">0</span></td>
    <td width="10%" align="right"><span onclick="listTable.edit(this, 'edit_sort_order', 92)">50</span></td>
    <td width="24%" align="center">
      <a href="category.php?act=move&cat_id=92">转移商品</a> |
      <a href="category.php?act=edit&amp;cat_id=92">编辑</a> |
      <a href="javascript:;" onclick="listTable.remove(92, '您确认要删除这条记录吗?')" title="移除">移除</a>
	  <!--代码修改_start Byjdy-->
	  	  <!--代码修改_end Byjdy-->
    </td>
  </tr>
    <tr align="center" class="1" id="1_59">
    <td align="left" class="first-cell" >
            <img src="images/menu_minus.gif" id="icon_1_59" width="9" height="9" border="0" style="margin-left:1em" onclick="rowClicked(this)" />
            <span>
      <!-- 0-默认列表 1-搜索匹配列表 其他-搜索未匹配列表 -->
            <a href="goods.php?act=list&cat_id=59&supp=-1">女鞋馆</a>
            </span>
        </td>
    <td width="10%">0</td>
    <td width="10%"><span onclick="listTable.edit(this, 'edit_measure_unit', 59)"><!--  -->&nbsp;&nbsp;&nbsp;&nbsp;<!--  --></span></td>
    <td width="10%"><img src="images/no.gif" onclick="listTable.toggle(this, 'toggle_show_in_nav', 59)" /></td>
    <td width="10%"><img src="images/yes.gif" onclick="listTable.toggle(this, 'toggle_is_show', 59)" /></td>
    <td><span onclick="listTable.edit(this, 'edit_grade', 59)">0</span></td>
    <td width="10%" align="right"><span onclick="listTable.edit(this, 'edit_sort_order', 59)">50</span></td>
    <td width="24%" align="center">
      <a href="category.php?act=move&cat_id=59">转移商品</a> |
      <a href="category.php?act=edit&amp;cat_id=59">编辑</a> |
      <a href="javascript:;" onclick="listTable.remove(59, '您确认要删除这条记录吗?')" title="移除">移除</a>
	  <!--代码修改_start Byjdy-->
	  	  <!--代码修改_end Byjdy-->
    </td>
  </tr>
    <tr align="center" class="2" id="2_99">
    <td align="left" class="first-cell" >
            <img src="images/menu_minus.gif" id="icon_2_99" width="9" height="9" border="0" style="margin-left:2em" onclick="rowClicked(this)" />
            <span>
      <!-- 0-默认列表 1-搜索匹配列表 其他-搜索未匹配列表 -->
            <a href="goods.php?act=list&cat_id=99&supp=-1">乐福鞋</a>
            </span>
        </td>
    <td width="10%">0</td>
    <td width="10%"><span onclick="listTable.edit(this, 'edit_measure_unit', 99)"><!--  -->&nbsp;&nbsp;&nbsp;&nbsp;<!--  --></span></td>
    <td width="10%"><img src="images/no.gif" onclick="listTable.toggle(this, 'toggle_show_in_nav', 99)" /></td>
    <td width="10%"><img src="images/yes.gif" onclick="listTable.toggle(this, 'toggle_is_show', 99)" /></td>
    <td><span onclick="listTable.edit(this, 'edit_grade', 99)">0</span></td>
    <td width="10%" align="right"><span onclick="listTable.edit(this, 'edit_sort_order', 99)">50</span></td>
    <td width="24%" align="center">
      <a href="category.php?act=move&cat_id=99">转移商品</a> |
      <a href="category.php?act=edit&amp;cat_id=99">编辑</a> |
      <a href="javascript:;" onclick="listTable.remove(99, '您确认要删除这条记录吗?')" title="移除">移除</a>
	  <!--代码修改_start Byjdy-->
	  	  <!--代码修改_end Byjdy-->
    </td>
  </tr>
    <tr align="center" class="2" id="2_96">
    <td align="left" class="first-cell" >
            <img src="images/menu_minus.gif" id="icon_2_96" width="9" height="9" border="0" style="margin-left:2em" onclick="rowClicked(this)" />
            <span>
      <!-- 0-默认列表 1-搜索匹配列表 其他-搜索未匹配列表 -->
            <a href="goods.php?act=list&cat_id=96&supp=-1">坡跟单鞋</a>
            </span>
        </td>
    <td width="10%">0</td>
    <td width="10%"><span onclick="listTable.edit(this, 'edit_measure_unit', 96)"><!--  -->&nbsp;&nbsp;&nbsp;&nbsp;<!--  --></span></td>
    <td width="10%"><img src="images/no.gif" onclick="listTable.toggle(this, 'toggle_show_in_nav', 96)" /></td>
    <td width="10%"><img src="images/yes.gif" onclick="listTable.toggle(this, 'toggle_is_show', 96)" /></td>
    <td><span onclick="listTable.edit(this, 'edit_grade', 96)">0</span></td>
    <td width="10%" align="right"><span onclick="listTable.edit(this, 'edit_sort_order', 96)">50</span></td>
    <td width="24%" align="center">
      <a href="category.php?act=move&cat_id=96">转移商品</a> |
      <a href="category.php?act=edit&amp;cat_id=96">编辑</a> |
      <a href="javascript:;" onclick="listTable.remove(96, '您确认要删除这条记录吗?')" title="移除">移除</a>
	  <!--代码修改_start Byjdy-->
	  	  <!--代码修改_end Byjdy-->
    </td>
  </tr>
    <tr align="center" class="2" id="2_93">
    <td align="left" class="first-cell" >
            <img src="images/menu_minus.gif" id="icon_2_93" width="9" height="9" border="0" style="margin-left:2em" onclick="rowClicked(this)" />
            <span>
      <!-- 0-默认列表 1-搜索匹配列表 其他-搜索未匹配列表 -->
            <a href="goods.php?act=list&cat_id=93&supp=-1">高跟凉拖</a>
            </span>
        </td>
    <td width="10%">0</td>
    <td width="10%"><span onclick="listTable.edit(this, 'edit_measure_unit', 93)"><!--  -->&nbsp;&nbsp;&nbsp;&nbsp;<!--  --></span></td>
    <td width="10%"><img src="images/no.gif" onclick="listTable.toggle(this, 'toggle_show_in_nav', 93)" /></td>
    <td width="10%"><img src="images/yes.gif" onclick="listTable.toggle(this, 'toggle_is_show', 93)" /></td>
    <td><span onclick="listTable.edit(this, 'edit_grade', 93)">0</span></td>
    <td width="10%" align="right"><span onclick="listTable.edit(this, 'edit_sort_order', 93)">50</span></td>
    <td width="24%" align="center">
      <a href="category.php?act=move&cat_id=93">转移商品</a> |
      <a href="category.php?act=edit&amp;cat_id=93">编辑</a> |
      <a href="javascript:;" onclick="listTable.remove(93, '您确认要删除这条记录吗?')" title="移除">移除</a>
	  <!--代码修改_start Byjdy-->
	  	  <!--代码修改_end Byjdy-->
    </td>
  </tr>
    <tr align="center" class="2" id="2_100">
    <td align="left" class="first-cell" >
            <img src="images/menu_minus.gif" id="icon_2_100" width="9" height="9" border="0" style="margin-left:2em" onclick="rowClicked(this)" />
            <span>
      <!-- 0-默认列表 1-搜索匹配列表 其他-搜索未匹配列表 -->
            <a href="goods.php?act=list&cat_id=100&supp=-1">松糕鞋</a>
            </span>
        </td>
    <td width="10%">0</td>
    <td width="10%"><span onclick="listTable.edit(this, 'edit_measure_unit', 100)"><!--  -->&nbsp;&nbsp;&nbsp;&nbsp;<!--  --></span></td>
    <td width="10%"><img src="images/no.gif" onclick="listTable.toggle(this, 'toggle_show_in_nav', 100)" /></td>
    <td width="10%"><img src="images/yes.gif" onclick="listTable.toggle(this, 'toggle_is_show', 100)" /></td>
    <td><span onclick="listTable.edit(this, 'edit_grade', 100)">0</span></td>
    <td width="10%" align="right"><span onclick="listTable.edit(this, 'edit_sort_order', 100)">50</span></td>
    <td width="24%" align="center">
      <a href="category.php?act=move&cat_id=100">转移商品</a> |
      <a href="category.php?act=edit&amp;cat_id=100">编辑</a> |
      <a href="javascript:;" onclick="listTable.remove(100, '您确认要删除这条记录吗?')" title="移除">移除</a>
	  <!--代码修改_start Byjdy-->
	  	  <!--代码修改_end Byjdy-->
    </td>
  </tr>
    <tr align="center" class="2" id="2_97">
    <td align="left" class="first-cell" >
            <img src="images/menu_minus.gif" id="icon_2_97" width="9" height="9" border="0" style="margin-left:2em" onclick="rowClicked(this)" />
            <span>
      <!-- 0-默认列表 1-搜索匹配列表 其他-搜索未匹配列表 -->
            <a href="goods.php?act=list&cat_id=97&supp=-1">浅口单鞋</a>
            </span>
        </td>
    <td width="10%">0</td>
    <td width="10%"><span onclick="listTable.edit(this, 'edit_measure_unit', 97)"><!--  -->&nbsp;&nbsp;&nbsp;&nbsp;<!--  --></span></td>
    <td width="10%"><img src="images/no.gif" onclick="listTable.toggle(this, 'toggle_show_in_nav', 97)" /></td>
    <td width="10%"><img src="images/yes.gif" onclick="listTable.toggle(this, 'toggle_is_show', 97)" /></td>
    <td><span onclick="listTable.edit(this, 'edit_grade', 97)">0</span></td>
    <td width="10%" align="right"><span onclick="listTable.edit(this, 'edit_sort_order', 97)">50</span></td>
    <td width="24%" align="center">
      <a href="category.php?act=move&cat_id=97">转移商品</a> |
      <a href="category.php?act=edit&amp;cat_id=97">编辑</a> |
      <a href="javascript:;" onclick="listTable.remove(97, '您确认要删除这条记录吗?')" title="移除">移除</a>
	  <!--代码修改_start Byjdy-->
	  	  <!--代码修改_end Byjdy-->
    </td>
  </tr>
    <tr align="center" class="2" id="2_94">
    <td align="left" class="first-cell" >
            <img src="images/menu_minus.gif" id="icon_2_94" width="9" height="9" border="0" style="margin-left:2em" onclick="rowClicked(this)" />
            <span>
      <!-- 0-默认列表 1-搜索匹配列表 其他-搜索未匹配列表 -->
            <a href="goods.php?act=list&cat_id=94&supp=-1">平底鞋</a>
            </span>
        </td>
    <td width="10%">0</td>
    <td width="10%"><span onclick="listTable.edit(this, 'edit_measure_unit', 94)"><!--  -->&nbsp;&nbsp;&nbsp;&nbsp;<!--  --></span></td>
    <td width="10%"><img src="images/no.gif" onclick="listTable.toggle(this, 'toggle_show_in_nav', 94)" /></td>
    <td width="10%"><img src="images/yes.gif" onclick="listTable.toggle(this, 'toggle_is_show', 94)" /></td>
    <td><span onclick="listTable.edit(this, 'edit_grade', 94)">0</span></td>
    <td width="10%" align="right"><span onclick="listTable.edit(this, 'edit_sort_order', 94)">50</span></td>
    <td width="24%" align="center">
      <a href="category.php?act=move&cat_id=94">转移商品</a> |
      <a href="category.php?act=edit&amp;cat_id=94">编辑</a> |
      <a href="javascript:;" onclick="listTable.remove(94, '您确认要删除这条记录吗?')" title="移除">移除</a>
	  <!--代码修改_start Byjdy-->
	  	  <!--代码修改_end Byjdy-->
    </td>
  </tr>
    <tr align="center" class="2" id="2_98">
    <td align="left" class="first-cell" >
            <img src="images/menu_minus.gif" id="icon_2_98" width="9" height="9" border="0" style="margin-left:2em" onclick="rowClicked(this)" />
            <span>
      <!-- 0-默认列表 1-搜索匹配列表 其他-搜索未匹配列表 -->
            <a href="goods.php?act=list&cat_id=98&supp=-1">帆布鞋</a>
            </span>
        </td>
    <td width="10%">0</td>
    <td width="10%"><span onclick="listTable.edit(this, 'edit_measure_unit', 98)"><!--  -->&nbsp;&nbsp;&nbsp;&nbsp;<!--  --></span></td>
    <td width="10%"><img src="images/no.gif" onclick="listTable.toggle(this, 'toggle_show_in_nav', 98)" /></td>
    <td width="10%"><img src="images/yes.gif" onclick="listTable.toggle(this, 'toggle_is_show', 98)" /></td>
    <td><span onclick="listTable.edit(this, 'edit_grade', 98)">0</span></td>
    <td width="10%" align="right"><span onclick="listTable.edit(this, 'edit_sort_order', 98)">50</span></td>
    <td width="24%" align="center">
      <a href="category.php?act=move&cat_id=98">转移商品</a> |
      <a href="category.php?act=edit&amp;cat_id=98">编辑</a> |
      <a href="javascript:;" onclick="listTable.remove(98, '您确认要删除这条记录吗?')" title="移除">移除</a>
	  <!--代码修改_start Byjdy-->
	  	  <!--代码修改_end Byjdy-->
    </td>
  </tr>
    <tr align="center" class="2" id="2_95">
    <td align="left" class="first-cell" >
            <img src="images/menu_minus.gif" id="icon_2_95" width="9" height="9" border="0" style="margin-left:2em" onclick="rowClicked(this)" />
            <span>
      <!-- 0-默认列表 1-搜索匹配列表 其他-搜索未匹配列表 -->
            <a href="goods.php?act=list&cat_id=95&supp=-1">高跟鞋</a>
            </span>
        </td>
    <td width="10%">0</td>
    <td width="10%"><span onclick="listTable.edit(this, 'edit_measure_unit', 95)"><!--  -->&nbsp;&nbsp;&nbsp;&nbsp;<!--  --></span></td>
    <td width="10%"><img src="images/no.gif" onclick="listTable.toggle(this, 'toggle_show_in_nav', 95)" /></td>
    <td width="10%"><img src="images/yes.gif" onclick="listTable.toggle(this, 'toggle_is_show', 95)" /></td>
    <td><span onclick="listTable.edit(this, 'edit_grade', 95)">0</span></td>
    <td width="10%" align="right"><span onclick="listTable.edit(this, 'edit_sort_order', 95)">50</span></td>
    <td width="24%" align="center">
      <a href="category.php?act=move&cat_id=95">转移商品</a> |
      <a href="category.php?act=edit&amp;cat_id=95">编辑</a> |
      <a href="javascript:;" onclick="listTable.remove(95, '您确认要删除这条记录吗?')" title="移除">移除</a>
	  <!--代码修改_start Byjdy-->
	  	  <!--代码修改_end Byjdy-->
    </td>
  </tr>
    <tr align="center" class="1" id="1_56">
    <td align="left" class="first-cell" >
            <img src="images/menu_minus.gif" id="icon_1_56" width="9" height="9" border="0" style="margin-left:1em" onclick="rowClicked(this)" />
            <span>
      <!-- 0-默认列表 1-搜索匹配列表 其他-搜索未匹配列表 -->
            <a href="goods.php?act=list&cat_id=56&supp=-1">内衣馆</a>
            </span>
        </td>
    <td width="10%">0</td>
    <td width="10%"><span onclick="listTable.edit(this, 'edit_measure_unit', 56)"><!--  -->&nbsp;&nbsp;&nbsp;&nbsp;<!--  --></span></td>
    <td width="10%"><img src="images/no.gif" onclick="listTable.toggle(this, 'toggle_show_in_nav', 56)" /></td>
    <td width="10%"><img src="images/yes.gif" onclick="listTable.toggle(this, 'toggle_is_show', 56)" /></td>
    <td><span onclick="listTable.edit(this, 'edit_grade', 56)">0</span></td>
    <td width="10%" align="right"><span onclick="listTable.edit(this, 'edit_sort_order', 56)">50</span></td>
    <td width="24%" align="center">
      <a href="category.php?act=move&cat_id=56">转移商品</a> |
      <a href="category.php?act=edit&amp;cat_id=56">编辑</a> |
      <a href="javascript:;" onclick="listTable.remove(56, '您确认要删除这条记录吗?')" title="移除">移除</a>
	  <!--代码修改_start Byjdy-->
	  	  <!--代码修改_end Byjdy-->
    </td>
  </tr>
    <tr align="center" class="2" id="2_73">
    <td align="left" class="first-cell" >
            <img src="images/menu_minus.gif" id="icon_2_73" width="9" height="9" border="0" style="margin-left:2em" onclick="rowClicked(this)" />
            <span>
      <!-- 0-默认列表 1-搜索匹配列表 其他-搜索未匹配列表 -->
            <a href="goods.php?act=list&cat_id=73&supp=-1">男士内裤</a>
            </span>
        </td>
    <td width="10%">0</td>
    <td width="10%"><span onclick="listTable.edit(this, 'edit_measure_unit', 73)"><!--  -->&nbsp;&nbsp;&nbsp;&nbsp;<!--  --></span></td>
    <td width="10%"><img src="images/no.gif" onclick="listTable.toggle(this, 'toggle_show_in_nav', 73)" /></td>
    <td width="10%"><img src="images/yes.gif" onclick="listTable.toggle(this, 'toggle_is_show', 73)" /></td>
    <td><span onclick="listTable.edit(this, 'edit_grade', 73)">0</span></td>
    <td width="10%" align="right"><span onclick="listTable.edit(this, 'edit_sort_order', 73)">50</span></td>
    <td width="24%" align="center">
      <a href="category.php?act=move&cat_id=73">转移商品</a> |
      <a href="category.php?act=edit&amp;cat_id=73">编辑</a> |
      <a href="javascript:;" onclick="listTable.remove(73, '您确认要删除这条记录吗?')" title="移除">移除</a>
	  <!--代码修改_start Byjdy-->
	  	  <!--代码修改_end Byjdy-->
    </td>
  </tr>
    <tr align="center" class="2" id="2_70">
    <td align="left" class="first-cell" >
            <img src="images/menu_minus.gif" id="icon_2_70" width="9" height="9" border="0" style="margin-left:2em" onclick="rowClicked(this)" />
            <span>
      <!-- 0-默认列表 1-搜索匹配列表 其他-搜索未匹配列表 -->
            <a href="goods.php?act=list&cat_id=70&supp=-1">薄款文胸</a>
            </span>
        </td>
    <td width="10%">0</td>
    <td width="10%"><span onclick="listTable.edit(this, 'edit_measure_unit', 70)"><!--  -->&nbsp;&nbsp;&nbsp;&nbsp;<!--  --></span></td>
    <td width="10%"><img src="images/no.gif" onclick="listTable.toggle(this, 'toggle_show_in_nav', 70)" /></td>
    <td width="10%"><img src="images/yes.gif" onclick="listTable.toggle(this, 'toggle_is_show', 70)" /></td>
    <td><span onclick="listTable.edit(this, 'edit_grade', 70)">0</span></td>
    <td width="10%" align="right"><span onclick="listTable.edit(this, 'edit_sort_order', 70)">50</span></td>
    <td width="24%" align="center">
      <a href="category.php?act=move&cat_id=70">转移商品</a> |
      <a href="category.php?act=edit&amp;cat_id=70">编辑</a> |
      <a href="javascript:;" onclick="listTable.remove(70, '您确认要删除这条记录吗?')" title="移除">移除</a>
	  <!--代码修改_start Byjdy-->
	  	  <!--代码修改_end Byjdy-->
    </td>
  </tr>
    <tr align="center" class="2" id="2_74">
    <td align="left" class="first-cell" >
            <img src="images/menu_minus.gif" id="icon_2_74" width="9" height="9" border="0" style="margin-left:2em" onclick="rowClicked(this)" />
            <span>
      <!-- 0-默认列表 1-搜索匹配列表 其他-搜索未匹配列表 -->
            <a href="goods.php?act=list&cat_id=74&supp=-1">夏季睡衣</a>
            </span>
        </td>
    <td width="10%">8</td>
    <td width="10%"><span onclick="listTable.edit(this, 'edit_measure_unit', 74)"><!--  -->&nbsp;&nbsp;&nbsp;&nbsp;<!--  --></span></td>
    <td width="10%"><img src="images/no.gif" onclick="listTable.toggle(this, 'toggle_show_in_nav', 74)" /></td>
    <td width="10%"><img src="images/yes.gif" onclick="listTable.toggle(this, 'toggle_is_show', 74)" /></td>
    <td><span onclick="listTable.edit(this, 'edit_grade', 74)">0</span></td>
    <td width="10%" align="right"><span onclick="listTable.edit(this, 'edit_sort_order', 74)">50</span></td>
    <td width="24%" align="center">
      <a href="category.php?act=move&cat_id=74">转移商品</a> |
      <a href="category.php?act=edit&amp;cat_id=74">编辑</a> |
      <a href="javascript:;" onclick="listTable.remove(74, '您确认要删除这条记录吗?')" title="移除">移除</a>
	  <!--代码修改_start Byjdy-->
	  	  <!--代码修改_end Byjdy-->
    </td>
  </tr>
    <tr align="center" class="2" id="2_71">
    <td align="left" class="first-cell" >
            <img src="images/menu_minus.gif" id="icon_2_71" width="9" height="9" border="0" style="margin-left:2em" onclick="rowClicked(this)" />
            <span>
      <!-- 0-默认列表 1-搜索匹配列表 其他-搜索未匹配列表 -->
            <a href="goods.php?act=list&cat_id=71&supp=-1">无钢圈文胸</a>
            </span>
        </td>
    <td width="10%">0</td>
    <td width="10%"><span onclick="listTable.edit(this, 'edit_measure_unit', 71)"><!--  -->&nbsp;&nbsp;&nbsp;&nbsp;<!--  --></span></td>
    <td width="10%"><img src="images/no.gif" onclick="listTable.toggle(this, 'toggle_show_in_nav', 71)" /></td>
    <td width="10%"><img src="images/yes.gif" onclick="listTable.toggle(this, 'toggle_is_show', 71)" /></td>
    <td><span onclick="listTable.edit(this, 'edit_grade', 71)">0</span></td>
    <td width="10%" align="right"><span onclick="listTable.edit(this, 'edit_sort_order', 71)">50</span></td>
    <td width="24%" align="center">
      <a href="category.php?act=move&cat_id=71">转移商品</a> |
      <a href="category.php?act=edit&amp;cat_id=71">编辑</a> |
      <a href="javascript:;" onclick="listTable.remove(71, '您确认要删除这条记录吗?')" title="移除">移除</a>
	  <!--代码修改_start Byjdy-->
	  	  <!--代码修改_end Byjdy-->
    </td>
  </tr>
    <tr align="center" class="2" id="2_75">
    <td align="left" class="first-cell" >
            <img src="images/menu_minus.gif" id="icon_2_75" width="9" height="9" border="0" style="margin-left:2em" onclick="rowClicked(this)" />
            <span>
      <!-- 0-默认列表 1-搜索匹配列表 其他-搜索未匹配列表 -->
            <a href="goods.php?act=list&cat_id=75&supp=-1">性感睡衣</a>
            </span>
        </td>
    <td width="10%">0</td>
    <td width="10%"><span onclick="listTable.edit(this, 'edit_measure_unit', 75)"><!--  -->&nbsp;&nbsp;&nbsp;&nbsp;<!--  --></span></td>
    <td width="10%"><img src="images/no.gif" onclick="listTable.toggle(this, 'toggle_show_in_nav', 75)" /></td>
    <td width="10%"><img src="images/yes.gif" onclick="listTable.toggle(this, 'toggle_is_show', 75)" /></td>
    <td><span onclick="listTable.edit(this, 'edit_grade', 75)">0</span></td>
    <td width="10%" align="right"><span onclick="listTable.edit(this, 'edit_sort_order', 75)">50</span></td>
    <td width="24%" align="center">
      <a href="category.php?act=move&cat_id=75">转移商品</a> |
      <a href="category.php?act=edit&amp;cat_id=75">编辑</a> |
      <a href="javascript:;" onclick="listTable.remove(75, '您确认要删除这条记录吗?')" title="移除">移除</a>
	  <!--代码修改_start Byjdy-->
	  	  <!--代码修改_end Byjdy-->
    </td>
  </tr>
    <tr align="center" class="2" id="2_72">
    <td align="left" class="first-cell" >
            <img src="images/menu_minus.gif" id="icon_2_72" width="9" height="9" border="0" style="margin-left:2em" onclick="rowClicked(this)" />
            <span>
      <!-- 0-默认列表 1-搜索匹配列表 其他-搜索未匹配列表 -->
            <a href="goods.php?act=list&cat_id=72&supp=-1">女士内裤</a>
            </span>
        </td>
    <td width="10%">0</td>
    <td width="10%"><span onclick="listTable.edit(this, 'edit_measure_unit', 72)"><!--  -->&nbsp;&nbsp;&nbsp;&nbsp;<!--  --></span></td>
    <td width="10%"><img src="images/no.gif" onclick="listTable.toggle(this, 'toggle_show_in_nav', 72)" /></td>
    <td width="10%"><img src="images/yes.gif" onclick="listTable.toggle(this, 'toggle_is_show', 72)" /></td>
    <td><span onclick="listTable.edit(this, 'edit_grade', 72)">0</span></td>
    <td width="10%" align="right"><span onclick="listTable.edit(this, 'edit_sort_order', 72)">50</span></td>
    <td width="24%" align="center">
      <a href="category.php?act=move&cat_id=72">转移商品</a> |
      <a href="category.php?act=edit&amp;cat_id=72">编辑</a> |
      <a href="javascript:;" onclick="listTable.remove(72, '您确认要删除这条记录吗?')" title="移除">移除</a>
	  <!--代码修改_start Byjdy-->
	  	  <!--代码修改_end Byjdy-->
    </td>
  </tr>
    <tr align="center" class="2" id="2_69">
    <td align="left" class="first-cell" >
            <img src="images/menu_minus.gif" id="icon_2_69" width="9" height="9" border="0" style="margin-left:2em" onclick="rowClicked(this)" />
            <span>
      <!-- 0-默认列表 1-搜索匹配列表 其他-搜索未匹配列表 -->
            <a href="goods.php?act=list&cat_id=69&supp=-1">聚拢文胸</a>
            </span>
        </td>
    <td width="10%">0</td>
    <td width="10%"><span onclick="listTable.edit(this, 'edit_measure_unit', 69)"><!--  -->&nbsp;&nbsp;&nbsp;&nbsp;<!--  --></span></td>
    <td width="10%"><img src="images/no.gif" onclick="listTable.toggle(this, 'toggle_show_in_nav', 69)" /></td>
    <td width="10%"><img src="images/yes.gif" onclick="listTable.toggle(this, 'toggle_is_show', 69)" /></td>
    <td><span onclick="listTable.edit(this, 'edit_grade', 69)">0</span></td>
    <td width="10%" align="right"><span onclick="listTable.edit(this, 'edit_sort_order', 69)">50</span></td>
    <td width="24%" align="center">
      <a href="category.php?act=move&cat_id=69">转移商品</a> |
      <a href="category.php?act=edit&amp;cat_id=69">编辑</a> |
      <a href="javascript:;" onclick="listTable.remove(69, '您确认要删除这条记录吗?')" title="移除">移除</a>
	  <!--代码修改_start Byjdy-->
	  	  <!--代码修改_end Byjdy-->
    </td>
  </tr>
    <tr align="center" class="2" id="2_76">
    <td align="left" class="first-cell" >
            <img src="images/menu_minus.gif" id="icon_2_76" width="9" height="9" border="0" style="margin-left:2em" onclick="rowClicked(this)" />
            <span>
      <!-- 0-默认列表 1-搜索匹配列表 其他-搜索未匹配列表 -->
            <a href="goods.php?act=list&cat_id=76&supp=-1">瘦腿袜</a>
            </span>
        </td>
    <td width="10%">0</td>
    <td width="10%"><span onclick="listTable.edit(this, 'edit_measure_unit', 76)"><!--  -->&nbsp;&nbsp;&nbsp;&nbsp;<!--  --></span></td>
    <td width="10%"><img src="images/no.gif" onclick="listTable.toggle(this, 'toggle_show_in_nav', 76)" /></td>
    <td width="10%"><img src="images/yes.gif" onclick="listTable.toggle(this, 'toggle_is_show', 76)" /></td>
    <td><span onclick="listTable.edit(this, 'edit_grade', 76)">0</span></td>
    <td width="10%" align="right"><span onclick="listTable.edit(this, 'edit_sort_order', 76)">50</span></td>
    <td width="24%" align="center">
      <a href="category.php?act=move&cat_id=76">转移商品</a> |
      <a href="category.php?act=edit&amp;cat_id=76">编辑</a> |
      <a href="javascript:;" onclick="listTable.remove(76, '您确认要删除这条记录吗?')" title="移除">移除</a>
	  <!--代码修改_start Byjdy-->
	  	  <!--代码修改_end Byjdy-->
    </td>
  </tr>
    <tr align="center" class="1" id="1_60">
    <td align="left" class="first-cell" >
            <img src="images/menu_minus.gif" id="icon_1_60" width="9" height="9" border="0" style="margin-left:1em" onclick="rowClicked(this)" />
            <span>
      <!-- 0-默认列表 1-搜索匹配列表 其他-搜索未匹配列表 -->
            <a href="goods.php?act=list&cat_id=60&supp=-1">箱包馆</a>
            </span>
        </td>
    <td width="10%">0</td>
    <td width="10%"><span onclick="listTable.edit(this, 'edit_measure_unit', 60)"><!--  -->&nbsp;&nbsp;&nbsp;&nbsp;<!--  --></span></td>
    <td width="10%"><img src="images/no.gif" onclick="listTable.toggle(this, 'toggle_show_in_nav', 60)" /></td>
    <td width="10%"><img src="images/yes.gif" onclick="listTable.toggle(this, 'toggle_is_show', 60)" /></td>
    <td><span onclick="listTable.edit(this, 'edit_grade', 60)">0</span></td>
    <td width="10%" align="right"><span onclick="listTable.edit(this, 'edit_sort_order', 60)">50</span></td>
    <td width="24%" align="center">
      <a href="category.php?act=move&cat_id=60">转移商品</a> |
      <a href="category.php?act=edit&amp;cat_id=60">编辑</a> |
      <a href="javascript:;" onclick="listTable.remove(60, '您确认要删除这条记录吗?')" title="移除">移除</a>
	  <!--代码修改_start Byjdy-->
	  	  <!--代码修改_end Byjdy-->
    </td>
  </tr>
    <tr align="center" class="2" id="2_105">
    <td align="left" class="first-cell" >
            <img src="images/menu_minus.gif" id="icon_2_105" width="9" height="9" border="0" style="margin-left:2em" onclick="rowClicked(this)" />
            <span>
      <!-- 0-默认列表 1-搜索匹配列表 其他-搜索未匹配列表 -->
            <a href="goods.php?act=list&cat_id=105&supp=-1">男士钱包</a>
            </span>
        </td>
    <td width="10%">0</td>
    <td width="10%"><span onclick="listTable.edit(this, 'edit_measure_unit', 105)"><!--  -->&nbsp;&nbsp;&nbsp;&nbsp;<!--  --></span></td>
    <td width="10%"><img src="images/no.gif" onclick="listTable.toggle(this, 'toggle_show_in_nav', 105)" /></td>
    <td width="10%"><img src="images/yes.gif" onclick="listTable.toggle(this, 'toggle_is_show', 105)" /></td>
    <td><span onclick="listTable.edit(this, 'edit_grade', 105)">0</span></td>
    <td width="10%" align="right"><span onclick="listTable.edit(this, 'edit_sort_order', 105)">50</span></td>
    <td width="24%" align="center">
      <a href="category.php?act=move&cat_id=105">转移商品</a> |
      <a href="category.php?act=edit&amp;cat_id=105">编辑</a> |
      <a href="javascript:;" onclick="listTable.remove(105, '您确认要删除这条记录吗?')" title="移除">移除</a>
	  <!--代码修改_start Byjdy-->
	  	  <!--代码修改_end Byjdy-->
    </td>
  </tr>
    <tr align="center" class="2" id="2_102">
    <td align="left" class="first-cell" >
            <img src="images/menu_minus.gif" id="icon_2_102" width="9" height="9" border="0" style="margin-left:2em" onclick="rowClicked(this)" />
            <span>
      <!-- 0-默认列表 1-搜索匹配列表 其他-搜索未匹配列表 -->
            <a href="goods.php?act=list&cat_id=102&supp=-1">手提女包</a>
            </span>
        </td>
    <td width="10%">0</td>
    <td width="10%"><span onclick="listTable.edit(this, 'edit_measure_unit', 102)"><!--  -->&nbsp;&nbsp;&nbsp;&nbsp;<!--  --></span></td>
    <td width="10%"><img src="images/no.gif" onclick="listTable.toggle(this, 'toggle_show_in_nav', 102)" /></td>
    <td width="10%"><img src="images/yes.gif" onclick="listTable.toggle(this, 'toggle_is_show', 102)" /></td>
    <td><span onclick="listTable.edit(this, 'edit_grade', 102)">0</span></td>
    <td width="10%" align="right"><span onclick="listTable.edit(this, 'edit_sort_order', 102)">50</span></td>
    <td width="24%" align="center">
      <a href="category.php?act=move&cat_id=102">转移商品</a> |
      <a href="category.php?act=edit&amp;cat_id=102">编辑</a> |
      <a href="javascript:;" onclick="listTable.remove(102, '您确认要删除这条记录吗?')" title="移除">移除</a>
	  <!--代码修改_start Byjdy-->
	  	  <!--代码修改_end Byjdy-->
    </td>
  </tr>
    <tr align="center" class="2" id="2_106">
    <td align="left" class="first-cell" >
            <img src="images/menu_minus.gif" id="icon_2_106" width="9" height="9" border="0" style="margin-left:2em" onclick="rowClicked(this)" />
            <span>
      <!-- 0-默认列表 1-搜索匹配列表 其他-搜索未匹配列表 -->
            <a href="goods.php?act=list&cat_id=106&supp=-1">旅行箱</a>
            </span>
        </td>
    <td width="10%">0</td>
    <td width="10%"><span onclick="listTable.edit(this, 'edit_measure_unit', 106)"><!--  -->&nbsp;&nbsp;&nbsp;&nbsp;<!--  --></span></td>
    <td width="10%"><img src="images/no.gif" onclick="listTable.toggle(this, 'toggle_show_in_nav', 106)" /></td>
    <td width="10%"><img src="images/yes.gif" onclick="listTable.toggle(this, 'toggle_is_show', 106)" /></td>
    <td><span onclick="listTable.edit(this, 'edit_grade', 106)">0</span></td>
    <td width="10%" align="right"><span onclick="listTable.edit(this, 'edit_sort_order', 106)">50</span></td>
    <td width="24%" align="center">
      <a href="category.php?act=move&cat_id=106">转移商品</a> |
      <a href="category.php?act=edit&amp;cat_id=106">编辑</a> |
      <a href="javascript:;" onclick="listTable.remove(106, '您确认要删除这条记录吗?')" title="移除">移除</a>
	  <!--代码修改_start Byjdy-->
	  	  <!--代码修改_end Byjdy-->
    </td>
  </tr>
    <tr align="center" class="2" id="2_103">
    <td align="left" class="first-cell" >
            <img src="images/menu_minus.gif" id="icon_2_103" width="9" height="9" border="0" style="margin-left:2em" onclick="rowClicked(this)" />
            <span>
      <!-- 0-默认列表 1-搜索匹配列表 其他-搜索未匹配列表 -->
            <a href="goods.php?act=list&cat_id=103&supp=-1">女士钱包</a>
            </span>
        </td>
    <td width="10%">0</td>
    <td width="10%"><span onclick="listTable.edit(this, 'edit_measure_unit', 103)"><!--  -->&nbsp;&nbsp;&nbsp;&nbsp;<!--  --></span></td>
    <td width="10%"><img src="images/no.gif" onclick="listTable.toggle(this, 'toggle_show_in_nav', 103)" /></td>
    <td width="10%"><img src="images/yes.gif" onclick="listTable.toggle(this, 'toggle_is_show', 103)" /></td>
    <td><span onclick="listTable.edit(this, 'edit_grade', 103)">0</span></td>
    <td width="10%" align="right"><span onclick="listTable.edit(this, 'edit_sort_order', 103)">50</span></td>
    <td width="24%" align="center">
      <a href="category.php?act=move&cat_id=103">转移商品</a> |
      <a href="category.php?act=edit&amp;cat_id=103">编辑</a> |
      <a href="javascript:;" onclick="listTable.remove(103, '您确认要删除这条记录吗?')" title="移除">移除</a>
	  <!--代码修改_start Byjdy-->
	  	  <!--代码修改_end Byjdy-->
    </td>
  </tr>
    <tr align="center" class="2" id="2_107">
    <td align="left" class="first-cell" >
            <img src="images/menu_minus.gif" id="icon_2_107" width="9" height="9" border="0" style="margin-left:2em" onclick="rowClicked(this)" />
            <span>
      <!-- 0-默认列表 1-搜索匹配列表 其他-搜索未匹配列表 -->
            <a href="goods.php?act=list&cat_id=107&supp=-1">拉杆箱</a>
            </span>
        </td>
    <td width="10%">0</td>
    <td width="10%"><span onclick="listTable.edit(this, 'edit_measure_unit', 107)"><!--  -->&nbsp;&nbsp;&nbsp;&nbsp;<!--  --></span></td>
    <td width="10%"><img src="images/no.gif" onclick="listTable.toggle(this, 'toggle_show_in_nav', 107)" /></td>
    <td width="10%"><img src="images/yes.gif" onclick="listTable.toggle(this, 'toggle_is_show', 107)" /></td>
    <td><span onclick="listTable.edit(this, 'edit_grade', 107)">0</span></td>
    <td width="10%" align="right"><span onclick="listTable.edit(this, 'edit_sort_order', 107)">50</span></td>
    <td width="24%" align="center">
      <a href="category.php?act=move&cat_id=107">转移商品</a> |
      <a href="category.php?act=edit&amp;cat_id=107">编辑</a> |
      <a href="javascript:;" onclick="listTable.remove(107, '您确认要删除这条记录吗?')" title="移除">移除</a>
	  <!--代码修改_start Byjdy-->
	  	  <!--代码修改_end Byjdy-->
    </td>
  </tr>
    <tr align="center" class="2" id="2_104">
    <td align="left" class="first-cell" >
            <img src="images/menu_minus.gif" id="icon_2_104" width="9" height="9" border="0" style="margin-left:2em" onclick="rowClicked(this)" />
            <span>
      <!-- 0-默认列表 1-搜索匹配列表 其他-搜索未匹配列表 -->
            <a href="goods.php?act=list&cat_id=104&supp=-1">男士双肩</a>
            </span>
        </td>
    <td width="10%">0</td>
    <td width="10%"><span onclick="listTable.edit(this, 'edit_measure_unit', 104)"><!--  -->&nbsp;&nbsp;&nbsp;&nbsp;<!--  --></span></td>
    <td width="10%"><img src="images/no.gif" onclick="listTable.toggle(this, 'toggle_show_in_nav', 104)" /></td>
    <td width="10%"><img src="images/yes.gif" onclick="listTable.toggle(this, 'toggle_is_show', 104)" /></td>
    <td><span onclick="listTable.edit(this, 'edit_grade', 104)">0</span></td>
    <td width="10%" align="right"><span onclick="listTable.edit(this, 'edit_sort_order', 104)">50</span></td>
    <td width="24%" align="center">
      <a href="category.php?act=move&cat_id=104">转移商品</a> |
      <a href="category.php?act=edit&amp;cat_id=104">编辑</a> |
      <a href="javascript:;" onclick="listTable.remove(104, '您确认要删除这条记录吗?')" title="移除">移除</a>
	  <!--代码修改_start Byjdy-->
	  	  <!--代码修改_end Byjdy-->
    </td>
  </tr>
    <tr align="center" class="2" id="2_101">
    <td align="left" class="first-cell" >
            <img src="images/menu_minus.gif" id="icon_2_101" width="9" height="9" border="0" style="margin-left:2em" onclick="rowClicked(this)" />
            <span>
      <!-- 0-默认列表 1-搜索匹配列表 其他-搜索未匹配列表 -->
            <a href="goods.php?act=list&cat_id=101&supp=-1">单肩女包</a>
            </span>
        </td>
    <td width="10%">0</td>
    <td width="10%"><span onclick="listTable.edit(this, 'edit_measure_unit', 101)"><!--  -->&nbsp;&nbsp;&nbsp;&nbsp;<!--  --></span></td>
    <td width="10%"><img src="images/no.gif" onclick="listTable.toggle(this, 'toggle_show_in_nav', 101)" /></td>
    <td width="10%"><img src="images/yes.gif" onclick="listTable.toggle(this, 'toggle_is_show', 101)" /></td>
    <td><span onclick="listTable.edit(this, 'edit_grade', 101)">0</span></td>
    <td width="10%" align="right"><span onclick="listTable.edit(this, 'edit_sort_order', 101)">50</span></td>
    <td width="24%" align="center">
      <a href="category.php?act=move&cat_id=101">转移商品</a> |
      <a href="category.php?act=edit&amp;cat_id=101">编辑</a> |
      <a href="javascript:;" onclick="listTable.remove(101, '您确认要删除这条记录吗?')" title="移除">移除</a>
	  <!--代码修改_start Byjdy-->
	  	  <!--代码修改_end Byjdy-->
    </td>
  </tr>
    <tr align="center" class="2" id="2_108">
    <td align="left" class="first-cell" >
            <img src="images/menu_minus.gif" id="icon_2_108" width="9" height="9" border="0" style="margin-left:2em" onclick="rowClicked(this)" />
            <span>
      <!-- 0-默认列表 1-搜索匹配列表 其他-搜索未匹配列表 -->
            <a href="goods.php?act=list&cat_id=108&supp=-1">拉杆包</a>
            </span>
        </td>
    <td width="10%">0</td>
    <td width="10%"><span onclick="listTable.edit(this, 'edit_measure_unit', 108)"><!--  -->&nbsp;&nbsp;&nbsp;&nbsp;<!--  --></span></td>
    <td width="10%"><img src="images/no.gif" onclick="listTable.toggle(this, 'toggle_show_in_nav', 108)" /></td>
    <td width="10%"><img src="images/yes.gif" onclick="listTable.toggle(this, 'toggle_is_show', 108)" /></td>
    <td><span onclick="listTable.edit(this, 'edit_grade', 108)">0</span></td>
    <td width="10%" align="right"><span onclick="listTable.edit(this, 'edit_sort_order', 108)">50</span></td>
    <td width="24%" align="center">
      <a href="category.php?act=move&cat_id=108">转移商品</a> |
      <a href="category.php?act=edit&amp;cat_id=108">编辑</a> |
      <a href="javascript:;" onclick="listTable.remove(108, '您确认要删除这条记录吗?')" title="移除">移除</a>
	  <!--代码修改_start Byjdy-->
	  	  <!--代码修改_end Byjdy-->
    </td>
  </tr>
    <tr align="center" class="0" id="0_3">
    <td align="left" class="first-cell" >
            <img src="images/menu_minus.gif" id="icon_0_3" width="9" height="9" border="0" style="margin-left:0em" onclick="rowClicked(this)" />
            <span>
      <!-- 0-默认列表 1-搜索匹配列表 其他-搜索未匹配列表 -->
            <a href="goods.php?act=list&cat_id=3&supp=-1">个护化妆、清洁用品</a>
            </span>
        </td>
    <td width="10%">0</td>
    <td width="10%"><span onclick="listTable.edit(this, 'edit_measure_unit', 3)"><!--  -->&nbsp;&nbsp;&nbsp;&nbsp;<!--  --></span></td>
    <td width="10%"><img src="images/no.gif" onclick="listTable.toggle(this, 'toggle_show_in_nav', 3)" /></td>
    <td width="10%"><img src="images/yes.gif" onclick="listTable.toggle(this, 'toggle_is_show', 3)" /></td>
    <td><span onclick="listTable.edit(this, 'edit_grade', 3)">5</span></td>
    <td width="10%" align="right"><span onclick="listTable.edit(this, 'edit_sort_order', 3)">6</span></td>
    <td width="24%" align="center">
      <a href="category.php?act=move&cat_id=3">转移商品</a> |
      <a href="category.php?act=edit&amp;cat_id=3">编辑</a> |
      <a href="javascript:;" onclick="listTable.remove(3, '您确认要删除这条记录吗?')" title="移除">移除</a>
	  <!--代码修改_start Byjdy-->
	  	  | <a href="category_flashimg.php?act=list&amp;cat_id=3">设置轮播图片</a>	  
	  	  <!--代码修改_end Byjdy-->
    </td>
  </tr>
    <tr align="center" class="1" id="1_113">
    <td align="left" class="first-cell" >
            <img src="images/menu_minus.gif" id="icon_1_113" width="9" height="9" border="0" style="margin-left:1em" onclick="rowClicked(this)" />
            <span>
      <!-- 0-默认列表 1-搜索匹配列表 其他-搜索未匹配列表 -->
            <a href="goods.php?act=list&cat_id=113&supp=-1">香水彩妆</a>
            </span>
        </td>
    <td width="10%">0</td>
    <td width="10%"><span onclick="listTable.edit(this, 'edit_measure_unit', 113)"><!--  -->&nbsp;&nbsp;&nbsp;&nbsp;<!--  --></span></td>
    <td width="10%"><img src="images/no.gif" onclick="listTable.toggle(this, 'toggle_show_in_nav', 113)" /></td>
    <td width="10%"><img src="images/yes.gif" onclick="listTable.toggle(this, 'toggle_is_show', 113)" /></td>
    <td><span onclick="listTable.edit(this, 'edit_grade', 113)">0</span></td>
    <td width="10%" align="right"><span onclick="listTable.edit(this, 'edit_sort_order', 113)">10</span></td>
    <td width="24%" align="center">
      <a href="category.php?act=move&cat_id=113">转移商品</a> |
      <a href="category.php?act=edit&amp;cat_id=113">编辑</a> |
      <a href="javascript:;" onclick="listTable.remove(113, '您确认要删除这条记录吗?')" title="移除">移除</a>
	  <!--代码修改_start Byjdy-->
	  	  <!--代码修改_end Byjdy-->
    </td>
  </tr>
    <tr align="center" class="2" id="2_137">
    <td align="left" class="first-cell" >
            <img src="images/menu_minus.gif" id="icon_2_137" width="9" height="9" border="0" style="margin-left:2em" onclick="rowClicked(this)" />
            <span>
      <!-- 0-默认列表 1-搜索匹配列表 其他-搜索未匹配列表 -->
            <a href="goods.php?act=list&cat_id=137&supp=-1">底妆</a>
            </span>
        </td>
    <td width="10%">0</td>
    <td width="10%"><span onclick="listTable.edit(this, 'edit_measure_unit', 137)"><!--  -->&nbsp;&nbsp;&nbsp;&nbsp;<!--  --></span></td>
    <td width="10%"><img src="images/no.gif" onclick="listTable.toggle(this, 'toggle_show_in_nav', 137)" /></td>
    <td width="10%"><img src="images/yes.gif" onclick="listTable.toggle(this, 'toggle_is_show', 137)" /></td>
    <td><span onclick="listTable.edit(this, 'edit_grade', 137)">0</span></td>
    <td width="10%" align="right"><span onclick="listTable.edit(this, 'edit_sort_order', 137)">50</span></td>
    <td width="24%" align="center">
      <a href="category.php?act=move&cat_id=137">转移商品</a> |
      <a href="category.php?act=edit&amp;cat_id=137">编辑</a> |
      <a href="javascript:;" onclick="listTable.remove(137, '您确认要删除这条记录吗?')" title="移除">移除</a>
	  <!--代码修改_start Byjdy-->
	  	  <!--代码修改_end Byjdy-->
    </td>
  </tr>
    <tr align="center" class="2" id="2_141">
    <td align="left" class="first-cell" >
            <img src="images/menu_minus.gif" id="icon_2_141" width="9" height="9" border="0" style="margin-left:2em" onclick="rowClicked(this)" />
            <span>
      <!-- 0-默认列表 1-搜索匹配列表 其他-搜索未匹配列表 -->
            <a href="goods.php?act=list&cat_id=141&supp=-1">美甲</a>
            </span>
        </td>
    <td width="10%">0</td>
    <td width="10%"><span onclick="listTable.edit(this, 'edit_measure_unit', 141)"><!--  -->&nbsp;&nbsp;&nbsp;&nbsp;<!--  --></span></td>
    <td width="10%"><img src="images/no.gif" onclick="listTable.toggle(this, 'toggle_show_in_nav', 141)" /></td>
    <td width="10%"><img src="images/yes.gif" onclick="listTable.toggle(this, 'toggle_is_show', 141)" /></td>
    <td><span onclick="listTable.edit(this, 'edit_grade', 141)">0</span></td>
    <td width="10%" align="right"><span onclick="listTable.edit(this, 'edit_sort_order', 141)">50</span></td>
    <td width="24%" align="center">
      <a href="category.php?act=move&cat_id=141">转移商品</a> |
      <a href="category.php?act=edit&amp;cat_id=141">编辑</a> |
      <a href="javascript:;" onclick="listTable.remove(141, '您确认要删除这条记录吗?')" title="移除">移除</a>
	  <!--代码修改_start Byjdy-->
	  	  <!--代码修改_end Byjdy-->
    </td>
  </tr>
    <tr align="center" class="2" id="2_138">
    <td align="left" class="first-cell" >
            <img src="images/menu_minus.gif" id="icon_2_138" width="9" height="9" border="0" style="margin-left:2em" onclick="rowClicked(this)" />
            <span>
      <!-- 0-默认列表 1-搜索匹配列表 其他-搜索未匹配列表 -->
            <a href="goods.php?act=list&cat_id=138&supp=-1">腮红</a>
            </span>
        </td>
    <td width="10%">0</td>
    <td width="10%"><span onclick="listTable.edit(this, 'edit_measure_unit', 138)"><!--  -->&nbsp;&nbsp;&nbsp;&nbsp;<!--  --></span></td>
    <td width="10%"><img src="images/no.gif" onclick="listTable.toggle(this, 'toggle_show_in_nav', 138)" /></td>
    <td width="10%"><img src="images/yes.gif" onclick="listTable.toggle(this, 'toggle_is_show', 138)" /></td>
    <td><span onclick="listTable.edit(this, 'edit_grade', 138)">0</span></td>
    <td width="10%" align="right"><span onclick="listTable.edit(this, 'edit_sort_order', 138)">50</span></td>
    <td width="24%" align="center">
      <a href="category.php?act=move&cat_id=138">转移商品</a> |
      <a href="category.php?act=edit&amp;cat_id=138">编辑</a> |
      <a href="javascript:;" onclick="listTable.remove(138, '您确认要删除这条记录吗?')" title="移除">移除</a>
	  <!--代码修改_start Byjdy-->
	  	  <!--代码修改_end Byjdy-->
    </td>
  </tr>
    <tr align="center" class="2" id="2_142">
    <td align="left" class="first-cell" >
            <img src="images/menu_minus.gif" id="icon_2_142" width="9" height="9" border="0" style="margin-left:2em" onclick="rowClicked(this)" />
            <span>
      <!-- 0-默认列表 1-搜索匹配列表 其他-搜索未匹配列表 -->
            <a href="goods.php?act=list&cat_id=142&supp=-1">美容工具</a>
            </span>
        </td>
    <td width="10%">0</td>
    <td width="10%"><span onclick="listTable.edit(this, 'edit_measure_unit', 142)"><!--  -->&nbsp;&nbsp;&nbsp;&nbsp;<!--  --></span></td>
    <td width="10%"><img src="images/no.gif" onclick="listTable.toggle(this, 'toggle_show_in_nav', 142)" /></td>
    <td width="10%"><img src="images/yes.gif" onclick="listTable.toggle(this, 'toggle_is_show', 142)" /></td>
    <td><span onclick="listTable.edit(this, 'edit_grade', 142)">0</span></td>
    <td width="10%" align="right"><span onclick="listTable.edit(this, 'edit_sort_order', 142)">50</span></td>
    <td width="24%" align="center">
      <a href="category.php?act=move&cat_id=142">转移商品</a> |
      <a href="category.php?act=edit&amp;cat_id=142">编辑</a> |
      <a href="javascript:;" onclick="listTable.remove(142, '您确认要删除这条记录吗?')" title="移除">移除</a>
	  <!--代码修改_start Byjdy-->
	  	  <!--代码修改_end Byjdy-->
    </td>
  </tr>
    <tr align="center" class="2" id="2_139">
    <td align="left" class="first-cell" >
            <img src="images/menu_minus.gif" id="icon_2_139" width="9" height="9" border="0" style="margin-left:2em" onclick="rowClicked(this)" />
            <span>
      <!-- 0-默认列表 1-搜索匹配列表 其他-搜索未匹配列表 -->
            <a href="goods.php?act=list&cat_id=139&supp=-1">眼部</a>
            </span>
        </td>
    <td width="10%">0</td>
    <td width="10%"><span onclick="listTable.edit(this, 'edit_measure_unit', 139)"><!--  -->&nbsp;&nbsp;&nbsp;&nbsp;<!--  --></span></td>
    <td width="10%"><img src="images/no.gif" onclick="listTable.toggle(this, 'toggle_show_in_nav', 139)" /></td>
    <td width="10%"><img src="images/yes.gif" onclick="listTable.toggle(this, 'toggle_is_show', 139)" /></td>
    <td><span onclick="listTable.edit(this, 'edit_grade', 139)">0</span></td>
    <td width="10%" align="right"><span onclick="listTable.edit(this, 'edit_sort_order', 139)">50</span></td>
    <td width="24%" align="center">
      <a href="category.php?act=move&cat_id=139">转移商品</a> |
      <a href="category.php?act=edit&amp;cat_id=139">编辑</a> |
      <a href="javascript:;" onclick="listTable.remove(139, '您确认要删除这条记录吗?')" title="移除">移除</a>
	  <!--代码修改_start Byjdy-->
	  	  <!--代码修改_end Byjdy-->
    </td>
  </tr>
    <tr align="center" class="2" id="2_136">
    <td align="left" class="first-cell" >
            <img src="images/menu_minus.gif" id="icon_2_136" width="9" height="9" border="0" style="margin-left:2em" onclick="rowClicked(this)" />
            <span>
      <!-- 0-默认列表 1-搜索匹配列表 其他-搜索未匹配列表 -->
            <a href="goods.php?act=list&cat_id=136&supp=-1">香水</a>
            </span>
        </td>
    <td width="10%">5</td>
    <td width="10%"><span onclick="listTable.edit(this, 'edit_measure_unit', 136)"><!--  -->&nbsp;&nbsp;&nbsp;&nbsp;<!--  --></span></td>
    <td width="10%"><img src="images/no.gif" onclick="listTable.toggle(this, 'toggle_show_in_nav', 136)" /></td>
    <td width="10%"><img src="images/yes.gif" onclick="listTable.toggle(this, 'toggle_is_show', 136)" /></td>
    <td><span onclick="listTable.edit(this, 'edit_grade', 136)">0</span></td>
    <td width="10%" align="right"><span onclick="listTable.edit(this, 'edit_sort_order', 136)">50</span></td>
    <td width="24%" align="center">
      <a href="category.php?act=move&cat_id=136">转移商品</a> |
      <a href="category.php?act=edit&amp;cat_id=136">编辑</a> |
      <a href="javascript:;" onclick="listTable.remove(136, '您确认要删除这条记录吗?')" title="移除">移除</a>
	  <!--代码修改_start Byjdy-->
	  	  <!--代码修改_end Byjdy-->
    </td>
  </tr>
    <tr align="center" class="2" id="2_143">
    <td align="left" class="first-cell" >
            <img src="images/menu_minus.gif" id="icon_2_143" width="9" height="9" border="0" style="margin-left:2em" onclick="rowClicked(this)" />
            <span>
      <!-- 0-默认列表 1-搜索匹配列表 其他-搜索未匹配列表 -->
            <a href="goods.php?act=list&cat_id=143&supp=-1">套装</a>
            </span>
        </td>
    <td width="10%">2</td>
    <td width="10%"><span onclick="listTable.edit(this, 'edit_measure_unit', 143)"><!--  -->&nbsp;&nbsp;&nbsp;&nbsp;<!--  --></span></td>
    <td width="10%"><img src="images/no.gif" onclick="listTable.toggle(this, 'toggle_show_in_nav', 143)" /></td>
    <td width="10%"><img src="images/yes.gif" onclick="listTable.toggle(this, 'toggle_is_show', 143)" /></td>
    <td><span onclick="listTable.edit(this, 'edit_grade', 143)">0</span></td>
    <td width="10%" align="right"><span onclick="listTable.edit(this, 'edit_sort_order', 143)">50</span></td>
    <td width="24%" align="center">
      <a href="category.php?act=move&cat_id=143">转移商品</a> |
      <a href="category.php?act=edit&amp;cat_id=143">编辑</a> |
      <a href="javascript:;" onclick="listTable.remove(143, '您确认要删除这条记录吗?')" title="移除">移除</a>
	  <!--代码修改_start Byjdy-->
	  	  <!--代码修改_end Byjdy-->
    </td>
  </tr>
    <tr align="center" class="2" id="2_140">
    <td align="left" class="first-cell" >
            <img src="images/menu_minus.gif" id="icon_2_140" width="9" height="9" border="0" style="margin-left:2em" onclick="rowClicked(this)" />
            <span>
      <!-- 0-默认列表 1-搜索匹配列表 其他-搜索未匹配列表 -->
            <a href="goods.php?act=list&cat_id=140&supp=-1">唇部</a>
            </span>
        </td>
    <td width="10%">1</td>
    <td width="10%"><span onclick="listTable.edit(this, 'edit_measure_unit', 140)"><!--  -->&nbsp;&nbsp;&nbsp;&nbsp;<!--  --></span></td>
    <td width="10%"><img src="images/no.gif" onclick="listTable.toggle(this, 'toggle_show_in_nav', 140)" /></td>
    <td width="10%"><img src="images/yes.gif" onclick="listTable.toggle(this, 'toggle_is_show', 140)" /></td>
    <td><span onclick="listTable.edit(this, 'edit_grade', 140)">0</span></td>
    <td width="10%" align="right"><span onclick="listTable.edit(this, 'edit_sort_order', 140)">50</span></td>
    <td width="24%" align="center">
      <a href="category.php?act=move&cat_id=140">转移商品</a> |
      <a href="category.php?act=edit&amp;cat_id=140">编辑</a> |
      <a href="javascript:;" onclick="listTable.remove(140, '您确认要删除这条记录吗?')" title="移除">移除</a>
	  <!--代码修改_start Byjdy-->
	  	  <!--代码修改_end Byjdy-->
    </td>
  </tr>
    <tr align="center" class="1" id="1_112">
    <td align="left" class="first-cell" >
            <img src="images/menu_minus.gif" id="icon_1_112" width="9" height="9" border="0" style="margin-left:1em" onclick="rowClicked(this)" />
            <span>
      <!-- 0-默认列表 1-搜索匹配列表 其他-搜索未匹配列表 -->
            <a href="goods.php?act=list&cat_id=112&supp=-1">口腔护理</a>
            </span>
        </td>
    <td width="10%">0</td>
    <td width="10%"><span onclick="listTable.edit(this, 'edit_measure_unit', 112)"><!--  -->&nbsp;&nbsp;&nbsp;&nbsp;<!--  --></span></td>
    <td width="10%"><img src="images/no.gif" onclick="listTable.toggle(this, 'toggle_show_in_nav', 112)" /></td>
    <td width="10%"><img src="images/yes.gif" onclick="listTable.toggle(this, 'toggle_is_show', 112)" /></td>
    <td><span onclick="listTable.edit(this, 'edit_grade', 112)">0</span></td>
    <td width="10%" align="right"><span onclick="listTable.edit(this, 'edit_sort_order', 112)">50</span></td>
    <td width="24%" align="center">
      <a href="category.php?act=move&cat_id=112">转移商品</a> |
      <a href="category.php?act=edit&amp;cat_id=112">编辑</a> |
      <a href="javascript:;" onclick="listTable.remove(112, '您确认要删除这条记录吗?')" title="移除">移除</a>
	  <!--代码修改_start Byjdy-->
	  	  <!--代码修改_end Byjdy-->
    </td>
  </tr>
    <tr align="center" class="2" id="2_134">
    <td align="left" class="first-cell" >
            <img src="images/menu_minus.gif" id="icon_2_134" width="9" height="9" border="0" style="margin-left:2em" onclick="rowClicked(this)" />
            <span>
      <!-- 0-默认列表 1-搜索匹配列表 其他-搜索未匹配列表 -->
            <a href="goods.php?act=list&cat_id=134&supp=-1">漱口水</a>
            </span>
        </td>
    <td width="10%">0</td>
    <td width="10%"><span onclick="listTable.edit(this, 'edit_measure_unit', 134)"><!--  -->&nbsp;&nbsp;&nbsp;&nbsp;<!--  --></span></td>
    <td width="10%"><img src="images/no.gif" onclick="listTable.toggle(this, 'toggle_show_in_nav', 134)" /></td>
    <td width="10%"><img src="images/yes.gif" onclick="listTable.toggle(this, 'toggle_is_show', 134)" /></td>
    <td><span onclick="listTable.edit(this, 'edit_grade', 134)">0</span></td>
    <td width="10%" align="right"><span onclick="listTable.edit(this, 'edit_sort_order', 134)">50</span></td>
    <td width="24%" align="center">
      <a href="category.php?act=move&cat_id=134">转移商品</a> |
      <a href="category.php?act=edit&amp;cat_id=134">编辑</a> |
      <a href="javascript:;" onclick="listTable.remove(134, '您确认要删除这条记录吗?')" title="移除">移除</a>
	  <!--代码修改_start Byjdy-->
	  	  <!--代码修改_end Byjdy-->
    </td>
  </tr>
    <tr align="center" class="2" id="2_135">
    <td align="left" class="first-cell" >
            <img src="images/menu_minus.gif" id="icon_2_135" width="9" height="9" border="0" style="margin-left:2em" onclick="rowClicked(this)" />
            <span>
      <!-- 0-默认列表 1-搜索匹配列表 其他-搜索未匹配列表 -->
            <a href="goods.php?act=list&cat_id=135&supp=-1">套装</a>
            </span>
        </td>
    <td width="10%">0</td>
    <td width="10%"><span onclick="listTable.edit(this, 'edit_measure_unit', 135)"><!--  -->&nbsp;&nbsp;&nbsp;&nbsp;<!--  --></span></td>
    <td width="10%"><img src="images/no.gif" onclick="listTable.toggle(this, 'toggle_show_in_nav', 135)" /></td>
    <td width="10%"><img src="images/yes.gif" onclick="listTable.toggle(this, 'toggle_is_show', 135)" /></td>
    <td><span onclick="listTable.edit(this, 'edit_grade', 135)">0</span></td>
    <td width="10%" align="right"><span onclick="listTable.edit(this, 'edit_sort_order', 135)">50</span></td>
    <td width="24%" align="center">
      <a href="category.php?act=move&cat_id=135">转移商品</a> |
      <a href="category.php?act=edit&amp;cat_id=135">编辑</a> |
      <a href="javascript:;" onclick="listTable.remove(135, '您确认要删除这条记录吗?')" title="移除">移除</a>
	  <!--代码修改_start Byjdy-->
	  	  <!--代码修改_end Byjdy-->
    </td>
  </tr>
    <tr align="center" class="2" id="2_132">
    <td align="left" class="first-cell" >
            <img src="images/menu_minus.gif" id="icon_2_132" width="9" height="9" border="0" style="margin-left:2em" onclick="rowClicked(this)" />
            <span>
      <!-- 0-默认列表 1-搜索匹配列表 其他-搜索未匹配列表 -->
            <a href="goods.php?act=list&cat_id=132&supp=-1">牙膏/牙粉</a>
            </span>
        </td>
    <td width="10%">0</td>
    <td width="10%"><span onclick="listTable.edit(this, 'edit_measure_unit', 132)"><!--  -->&nbsp;&nbsp;&nbsp;&nbsp;<!--  --></span></td>
    <td width="10%"><img src="images/no.gif" onclick="listTable.toggle(this, 'toggle_show_in_nav', 132)" /></td>
    <td width="10%"><img src="images/yes.gif" onclick="listTable.toggle(this, 'toggle_is_show', 132)" /></td>
    <td><span onclick="listTable.edit(this, 'edit_grade', 132)">0</span></td>
    <td width="10%" align="right"><span onclick="listTable.edit(this, 'edit_sort_order', 132)">50</span></td>
    <td width="24%" align="center">
      <a href="category.php?act=move&cat_id=132">转移商品</a> |
      <a href="category.php?act=edit&amp;cat_id=132">编辑</a> |
      <a href="javascript:;" onclick="listTable.remove(132, '您确认要删除这条记录吗?')" title="移除">移除</a>
	  <!--代码修改_start Byjdy-->
	  	  <!--代码修改_end Byjdy-->
    </td>
  </tr>
    <tr align="center" class="2" id="2_133">
    <td align="left" class="first-cell" >
            <img src="images/menu_minus.gif" id="icon_2_133" width="9" height="9" border="0" style="margin-left:2em" onclick="rowClicked(this)" />
            <span>
      <!-- 0-默认列表 1-搜索匹配列表 其他-搜索未匹配列表 -->
            <a href="goods.php?act=list&cat_id=133&supp=-1">牙刷/牙线</a>
            </span>
        </td>
    <td width="10%">0</td>
    <td width="10%"><span onclick="listTable.edit(this, 'edit_measure_unit', 133)"><!--  -->&nbsp;&nbsp;&nbsp;&nbsp;<!--  --></span></td>
    <td width="10%"><img src="images/no.gif" onclick="listTable.toggle(this, 'toggle_show_in_nav', 133)" /></td>
    <td width="10%"><img src="images/yes.gif" onclick="listTable.toggle(this, 'toggle_is_show', 133)" /></td>
    <td><span onclick="listTable.edit(this, 'edit_grade', 133)">0</span></td>
    <td width="10%" align="right"><span onclick="listTable.edit(this, 'edit_sort_order', 133)">50</span></td>
    <td width="24%" align="center">
      <a href="category.php?act=move&cat_id=133">转移商品</a> |
      <a href="category.php?act=edit&amp;cat_id=133">编辑</a> |
      <a href="javascript:;" onclick="listTable.remove(133, '您确认要删除这条记录吗?')" title="移除">移除</a>
	  <!--代码修改_start Byjdy-->
	  	  <!--代码修改_end Byjdy-->
    </td>
  </tr>
    <tr align="center" class="1" id="1_109">
    <td align="left" class="first-cell" >
            <img src="images/menu_minus.gif" id="icon_1_109" width="9" height="9" border="0" style="margin-left:1em" onclick="rowClicked(this)" />
            <span>
      <!-- 0-默认列表 1-搜索匹配列表 其他-搜索未匹配列表 -->
            <a href="goods.php?act=list&cat_id=109&supp=-1">面部护肤</a>
            </span>
        </td>
    <td width="10%">1</td>
    <td width="10%"><span onclick="listTable.edit(this, 'edit_measure_unit', 109)"><!--  -->&nbsp;&nbsp;&nbsp;&nbsp;<!--  --></span></td>
    <td width="10%"><img src="images/no.gif" onclick="listTable.toggle(this, 'toggle_show_in_nav', 109)" /></td>
    <td width="10%"><img src="images/yes.gif" onclick="listTable.toggle(this, 'toggle_is_show', 109)" /></td>
    <td><span onclick="listTable.edit(this, 'edit_grade', 109)">0</span></td>
    <td width="10%" align="right"><span onclick="listTable.edit(this, 'edit_sort_order', 109)">50</span></td>
    <td width="24%" align="center">
      <a href="category.php?act=move&cat_id=109">转移商品</a> |
      <a href="category.php?act=edit&amp;cat_id=109">编辑</a> |
      <a href="javascript:;" onclick="listTable.remove(109, '您确认要删除这条记录吗?')" title="移除">移除</a>
	  <!--代码修改_start Byjdy-->
	  	  <!--代码修改_end Byjdy-->
    </td>
  </tr>
    <tr align="center" class="2" id="2_118">
    <td align="left" class="first-cell" >
            <img src="images/menu_minus.gif" id="icon_2_118" width="9" height="9" border="0" style="margin-left:2em" onclick="rowClicked(this)" />
            <span>
      <!-- 0-默认列表 1-搜索匹配列表 其他-搜索未匹配列表 -->
            <a href="goods.php?act=list&cat_id=118&supp=-1">套装</a>
            </span>
        </td>
    <td width="10%">4</td>
    <td width="10%"><span onclick="listTable.edit(this, 'edit_measure_unit', 118)"><!--  -->&nbsp;&nbsp;&nbsp;&nbsp;<!--  --></span></td>
    <td width="10%"><img src="images/no.gif" onclick="listTable.toggle(this, 'toggle_show_in_nav', 118)" /></td>
    <td width="10%"><img src="images/yes.gif" onclick="listTable.toggle(this, 'toggle_is_show', 118)" /></td>
    <td><span onclick="listTable.edit(this, 'edit_grade', 118)">0</span></td>
    <td width="10%" align="right"><span onclick="listTable.edit(this, 'edit_sort_order', 118)">50</span></td>
    <td width="24%" align="center">
      <a href="category.php?act=move&cat_id=118">转移商品</a> |
      <a href="category.php?act=edit&amp;cat_id=118">编辑</a> |
      <a href="javascript:;" onclick="listTable.remove(118, '您确认要删除这条记录吗?')" title="移除">移除</a>
	  <!--代码修改_start Byjdy-->
	  	  <!--代码修改_end Byjdy-->
    </td>
  </tr>
    <tr align="center" class="2" id="2_115">
    <td align="left" class="first-cell" >
            <img src="images/menu_minus.gif" id="icon_2_115" width="9" height="9" border="0" style="margin-left:2em" onclick="rowClicked(this)" />
            <span>
      <!-- 0-默认列表 1-搜索匹配列表 其他-搜索未匹配列表 -->
            <a href="goods.php?act=list&cat_id=115&supp=-1">护肤</a>
            </span>
        </td>
    <td width="10%">6</td>
    <td width="10%"><span onclick="listTable.edit(this, 'edit_measure_unit', 115)"><!--  -->&nbsp;&nbsp;&nbsp;&nbsp;<!--  --></span></td>
    <td width="10%"><img src="images/no.gif" onclick="listTable.toggle(this, 'toggle_show_in_nav', 115)" /></td>
    <td width="10%"><img src="images/yes.gif" onclick="listTable.toggle(this, 'toggle_is_show', 115)" /></td>
    <td><span onclick="listTable.edit(this, 'edit_grade', 115)">0</span></td>
    <td width="10%" align="right"><span onclick="listTable.edit(this, 'edit_sort_order', 115)">50</span></td>
    <td width="24%" align="center">
      <a href="category.php?act=move&cat_id=115">转移商品</a> |
      <a href="category.php?act=edit&amp;cat_id=115">编辑</a> |
      <a href="javascript:;" onclick="listTable.remove(115, '您确认要删除这条记录吗?')" title="移除">移除</a>
	  <!--代码修改_start Byjdy-->
	  	  <!--代码修改_end Byjdy-->
    </td>
  </tr>
    <tr align="center" class="2" id="2_116">
    <td align="left" class="first-cell" >
            <img src="images/menu_minus.gif" id="icon_2_116" width="9" height="9" border="0" style="margin-left:2em" onclick="rowClicked(this)" />
            <span>
      <!-- 0-默认列表 1-搜索匹配列表 其他-搜索未匹配列表 -->
            <a href="goods.php?act=list&cat_id=116&supp=-1">面膜</a>
            </span>
        </td>
    <td width="10%">2</td>
    <td width="10%"><span onclick="listTable.edit(this, 'edit_measure_unit', 116)"><!--  -->&nbsp;&nbsp;&nbsp;&nbsp;<!--  --></span></td>
    <td width="10%"><img src="images/no.gif" onclick="listTable.toggle(this, 'toggle_show_in_nav', 116)" /></td>
    <td width="10%"><img src="images/yes.gif" onclick="listTable.toggle(this, 'toggle_is_show', 116)" /></td>
    <td><span onclick="listTable.edit(this, 'edit_grade', 116)">0</span></td>
    <td width="10%" align="right"><span onclick="listTable.edit(this, 'edit_sort_order', 116)">50</span></td>
    <td width="24%" align="center">
      <a href="category.php?act=move&cat_id=116">转移商品</a> |
      <a href="category.php?act=edit&amp;cat_id=116">编辑</a> |
      <a href="javascript:;" onclick="listTable.remove(116, '您确认要删除这条记录吗?')" title="移除">移除</a>
	  <!--代码修改_start Byjdy-->
	  	  <!--代码修改_end Byjdy-->
    </td>
  </tr>
    <tr align="center" class="2" id="2_117">
    <td align="left" class="first-cell" >
            <img src="images/menu_minus.gif" id="icon_2_117" width="9" height="9" border="0" style="margin-left:2em" onclick="rowClicked(this)" />
            <span>
      <!-- 0-默认列表 1-搜索匹配列表 其他-搜索未匹配列表 -->
            <a href="goods.php?act=list&cat_id=117&supp=-1">剃须</a>
            </span>
        </td>
    <td width="10%">0</td>
    <td width="10%"><span onclick="listTable.edit(this, 'edit_measure_unit', 117)"><!--  -->&nbsp;&nbsp;&nbsp;&nbsp;<!--  --></span></td>
    <td width="10%"><img src="images/no.gif" onclick="listTable.toggle(this, 'toggle_show_in_nav', 117)" /></td>
    <td width="10%"><img src="images/yes.gif" onclick="listTable.toggle(this, 'toggle_is_show', 117)" /></td>
    <td><span onclick="listTable.edit(this, 'edit_grade', 117)">0</span></td>
    <td width="10%" align="right"><span onclick="listTable.edit(this, 'edit_sort_order', 117)">50</span></td>
    <td width="24%" align="center">
      <a href="category.php?act=move&cat_id=117">转移商品</a> |
      <a href="category.php?act=edit&amp;cat_id=117">编辑</a> |
      <a href="javascript:;" onclick="listTable.remove(117, '您确认要删除这条记录吗?')" title="移除">移除</a>
	  <!--代码修改_start Byjdy-->
	  	  <!--代码修改_end Byjdy-->
    </td>
  </tr>
    <tr align="center" class="2" id="2_114">
    <td align="left" class="first-cell" >
            <img src="images/menu_minus.gif" id="icon_2_114" width="9" height="9" border="0" style="margin-left:2em" onclick="rowClicked(this)" />
            <span>
      <!-- 0-默认列表 1-搜索匹配列表 其他-搜索未匹配列表 -->
            <a href="goods.php?act=list&cat_id=114&supp=-1">清洁</a>
            </span>
        </td>
    <td width="10%">1</td>
    <td width="10%"><span onclick="listTable.edit(this, 'edit_measure_unit', 114)"><!--  -->&nbsp;&nbsp;&nbsp;&nbsp;<!--  --></span></td>
    <td width="10%"><img src="images/no.gif" onclick="listTable.toggle(this, 'toggle_show_in_nav', 114)" /></td>
    <td width="10%"><img src="images/yes.gif" onclick="listTable.toggle(this, 'toggle_is_show', 114)" /></td>
    <td><span onclick="listTable.edit(this, 'edit_grade', 114)">0</span></td>
    <td width="10%" align="right"><span onclick="listTable.edit(this, 'edit_sort_order', 114)">50</span></td>
    <td width="24%" align="center">
      <a href="category.php?act=move&cat_id=114">转移商品</a> |
      <a href="category.php?act=edit&amp;cat_id=114">编辑</a> |
      <a href="javascript:;" onclick="listTable.remove(114, '您确认要删除这条记录吗?')" title="移除">移除</a>
	  <!--代码修改_start Byjdy-->
	  	  <!--代码修改_end Byjdy-->
    </td>
  </tr>
    <tr align="center" class="1" id="1_110">
    <td align="left" class="first-cell" >
            <img src="images/menu_minus.gif" id="icon_1_110" width="9" height="9" border="0" style="margin-left:1em" onclick="rowClicked(this)" />
            <span>
      <!-- 0-默认列表 1-搜索匹配列表 其他-搜索未匹配列表 -->
            <a href="goods.php?act=list&cat_id=110&supp=-1">洗发护发</a>
            </span>
        </td>
    <td width="10%">0</td>
    <td width="10%"><span onclick="listTable.edit(this, 'edit_measure_unit', 110)"><!--  -->&nbsp;&nbsp;&nbsp;&nbsp;<!--  --></span></td>
    <td width="10%"><img src="images/no.gif" onclick="listTable.toggle(this, 'toggle_show_in_nav', 110)" /></td>
    <td width="10%"><img src="images/yes.gif" onclick="listTable.toggle(this, 'toggle_is_show', 110)" /></td>
    <td><span onclick="listTable.edit(this, 'edit_grade', 110)">0</span></td>
    <td width="10%" align="right"><span onclick="listTable.edit(this, 'edit_sort_order', 110)">50</span></td>
    <td width="24%" align="center">
      <a href="category.php?act=move&cat_id=110">转移商品</a> |
      <a href="category.php?act=edit&amp;cat_id=110">编辑</a> |
      <a href="javascript:;" onclick="listTable.remove(110, '您确认要删除这条记录吗?')" title="移除">移除</a>
	  <!--代码修改_start Byjdy-->
	  	  <!--代码修改_end Byjdy-->
    </td>
  </tr>
    <tr align="center" class="2" id="2_121">
    <td align="left" class="first-cell" >
            <img src="images/menu_minus.gif" id="icon_2_121" width="9" height="9" border="0" style="margin-left:2em" onclick="rowClicked(this)" />
            <span>
      <!-- 0-默认列表 1-搜索匹配列表 其他-搜索未匹配列表 -->
            <a href="goods.php?act=list&cat_id=121&supp=-1">染发</a>
            </span>
        </td>
    <td width="10%">0</td>
    <td width="10%"><span onclick="listTable.edit(this, 'edit_measure_unit', 121)"><!--  -->&nbsp;&nbsp;&nbsp;&nbsp;<!--  --></span></td>
    <td width="10%"><img src="images/no.gif" onclick="listTable.toggle(this, 'toggle_show_in_nav', 121)" /></td>
    <td width="10%"><img src="images/yes.gif" onclick="listTable.toggle(this, 'toggle_is_show', 121)" /></td>
    <td><span onclick="listTable.edit(this, 'edit_grade', 121)">0</span></td>
    <td width="10%" align="right"><span onclick="listTable.edit(this, 'edit_sort_order', 121)">50</span></td>
    <td width="24%" align="center">
      <a href="category.php?act=move&cat_id=121">转移商品</a> |
      <a href="category.php?act=edit&amp;cat_id=121">编辑</a> |
      <a href="javascript:;" onclick="listTable.remove(121, '您确认要删除这条记录吗?')" title="移除">移除</a>
	  <!--代码修改_start Byjdy-->
	  	  <!--代码修改_end Byjdy-->
    </td>
  </tr>
    <tr align="center" class="2" id="2_122">
    <td align="left" class="first-cell" >
            <img src="images/menu_minus.gif" id="icon_2_122" width="9" height="9" border="0" style="margin-left:2em" onclick="rowClicked(this)" />
            <span>
      <!-- 0-默认列表 1-搜索匹配列表 其他-搜索未匹配列表 -->
            <a href="goods.php?act=list&cat_id=122&supp=-1">造型</a>
            </span>
        </td>
    <td width="10%">0</td>
    <td width="10%"><span onclick="listTable.edit(this, 'edit_measure_unit', 122)"><!--  -->&nbsp;&nbsp;&nbsp;&nbsp;<!--  --></span></td>
    <td width="10%"><img src="images/no.gif" onclick="listTable.toggle(this, 'toggle_show_in_nav', 122)" /></td>
    <td width="10%"><img src="images/yes.gif" onclick="listTable.toggle(this, 'toggle_is_show', 122)" /></td>
    <td><span onclick="listTable.edit(this, 'edit_grade', 122)">0</span></td>
    <td width="10%" align="right"><span onclick="listTable.edit(this, 'edit_sort_order', 122)">50</span></td>
    <td width="24%" align="center">
      <a href="category.php?act=move&cat_id=122">转移商品</a> |
      <a href="category.php?act=edit&amp;cat_id=122">编辑</a> |
      <a href="javascript:;" onclick="listTable.remove(122, '您确认要删除这条记录吗?')" title="移除">移除</a>
	  <!--代码修改_start Byjdy-->
	  	  <!--代码修改_end Byjdy-->
    </td>
  </tr>
    <tr align="center" class="2" id="2_119">
    <td align="left" class="first-cell" >
            <img src="images/menu_minus.gif" id="icon_2_119" width="9" height="9" border="0" style="margin-left:2em" onclick="rowClicked(this)" />
            <span>
      <!-- 0-默认列表 1-搜索匹配列表 其他-搜索未匹配列表 -->
            <a href="goods.php?act=list&cat_id=119&supp=-1">洗发</a>
            </span>
        </td>
    <td width="10%">4</td>
    <td width="10%"><span onclick="listTable.edit(this, 'edit_measure_unit', 119)"><!--  -->&nbsp;&nbsp;&nbsp;&nbsp;<!--  --></span></td>
    <td width="10%"><img src="images/no.gif" onclick="listTable.toggle(this, 'toggle_show_in_nav', 119)" /></td>
    <td width="10%"><img src="images/yes.gif" onclick="listTable.toggle(this, 'toggle_is_show', 119)" /></td>
    <td><span onclick="listTable.edit(this, 'edit_grade', 119)">0</span></td>
    <td width="10%" align="right"><span onclick="listTable.edit(this, 'edit_sort_order', 119)">50</span></td>
    <td width="24%" align="center">
      <a href="category.php?act=move&cat_id=119">转移商品</a> |
      <a href="category.php?act=edit&amp;cat_id=119">编辑</a> |
      <a href="javascript:;" onclick="listTable.remove(119, '您确认要删除这条记录吗?')" title="移除">移除</a>
	  <!--代码修改_start Byjdy-->
	  	  <!--代码修改_end Byjdy-->
    </td>
  </tr>
    <tr align="center" class="2" id="2_123">
    <td align="left" class="first-cell" >
            <img src="images/menu_minus.gif" id="icon_2_123" width="9" height="9" border="0" style="margin-left:2em" onclick="rowClicked(this)" />
            <span>
      <!-- 0-默认列表 1-搜索匹配列表 其他-搜索未匹配列表 -->
            <a href="goods.php?act=list&cat_id=123&supp=-1">假发</a>
            </span>
        </td>
    <td width="10%">0</td>
    <td width="10%"><span onclick="listTable.edit(this, 'edit_measure_unit', 123)"><!--  -->&nbsp;&nbsp;&nbsp;&nbsp;<!--  --></span></td>
    <td width="10%"><img src="images/no.gif" onclick="listTable.toggle(this, 'toggle_show_in_nav', 123)" /></td>
    <td width="10%"><img src="images/yes.gif" onclick="listTable.toggle(this, 'toggle_is_show', 123)" /></td>
    <td><span onclick="listTable.edit(this, 'edit_grade', 123)">0</span></td>
    <td width="10%" align="right"><span onclick="listTable.edit(this, 'edit_sort_order', 123)">50</span></td>
    <td width="24%" align="center">
      <a href="category.php?act=move&cat_id=123">转移商品</a> |
      <a href="category.php?act=edit&amp;cat_id=123">编辑</a> |
      <a href="javascript:;" onclick="listTable.remove(123, '您确认要删除这条记录吗?')" title="移除">移除</a>
	  <!--代码修改_start Byjdy-->
	  	  <!--代码修改_end Byjdy-->
    </td>
  </tr>
    <tr align="center" class="2" id="2_120">
    <td align="left" class="first-cell" >
            <img src="images/menu_minus.gif" id="icon_2_120" width="9" height="9" border="0" style="margin-left:2em" onclick="rowClicked(this)" />
            <span>
      <!-- 0-默认列表 1-搜索匹配列表 其他-搜索未匹配列表 -->
            <a href="goods.php?act=list&cat_id=120&supp=-1">护发</a>
            </span>
        </td>
    <td width="10%">1</td>
    <td width="10%"><span onclick="listTable.edit(this, 'edit_measure_unit', 120)"><!--  -->&nbsp;&nbsp;&nbsp;&nbsp;<!--  --></span></td>
    <td width="10%"><img src="images/no.gif" onclick="listTable.toggle(this, 'toggle_show_in_nav', 120)" /></td>
    <td width="10%"><img src="images/yes.gif" onclick="listTable.toggle(this, 'toggle_is_show', 120)" /></td>
    <td><span onclick="listTable.edit(this, 'edit_grade', 120)">0</span></td>
    <td width="10%" align="right"><span onclick="listTable.edit(this, 'edit_sort_order', 120)">50</span></td>
    <td width="24%" align="center">
      <a href="category.php?act=move&cat_id=120">转移商品</a> |
      <a href="category.php?act=edit&amp;cat_id=120">编辑</a> |
      <a href="javascript:;" onclick="listTable.remove(120, '您确认要删除这条记录吗?')" title="移除">移除</a>
	  <!--代码修改_start Byjdy-->
	  	  <!--代码修改_end Byjdy-->
    </td>
  </tr>
    <tr align="center" class="2" id="2_124">
    <td align="left" class="first-cell" >
            <img src="images/menu_minus.gif" id="icon_2_124" width="9" height="9" border="0" style="margin-left:2em" onclick="rowClicked(this)" />
            <span>
      <!-- 0-默认列表 1-搜索匹配列表 其他-搜索未匹配列表 -->
            <a href="goods.php?act=list&cat_id=124&supp=-1">套装</a>
            </span>
        </td>
    <td width="10%">3</td>
    <td width="10%"><span onclick="listTable.edit(this, 'edit_measure_unit', 124)"><!--  -->&nbsp;&nbsp;&nbsp;&nbsp;<!--  --></span></td>
    <td width="10%"><img src="images/no.gif" onclick="listTable.toggle(this, 'toggle_show_in_nav', 124)" /></td>
    <td width="10%"><img src="images/yes.gif" onclick="listTable.toggle(this, 'toggle_is_show', 124)" /></td>
    <td><span onclick="listTable.edit(this, 'edit_grade', 124)">0</span></td>
    <td width="10%" align="right"><span onclick="listTable.edit(this, 'edit_sort_order', 124)">50</span></td>
    <td width="24%" align="center">
      <a href="category.php?act=move&cat_id=124">转移商品</a> |
      <a href="category.php?act=edit&amp;cat_id=124">编辑</a> |
      <a href="javascript:;" onclick="listTable.remove(124, '您确认要删除这条记录吗?')" title="移除">移除</a>
	  <!--代码修改_start Byjdy-->
	  	  <!--代码修改_end Byjdy-->
    </td>
  </tr>
    <tr align="center" class="1" id="1_111">
    <td align="left" class="first-cell" >
            <img src="images/menu_minus.gif" id="icon_1_111" width="9" height="9" border="0" style="margin-left:1em" onclick="rowClicked(this)" />
            <span>
      <!-- 0-默认列表 1-搜索匹配列表 其他-搜索未匹配列表 -->
            <a href="goods.php?act=list&cat_id=111&supp=-1">身体护肤</a>
            </span>
        </td>
    <td width="10%">0</td>
    <td width="10%"><span onclick="listTable.edit(this, 'edit_measure_unit', 111)"><!--  -->&nbsp;&nbsp;&nbsp;&nbsp;<!--  --></span></td>
    <td width="10%"><img src="images/no.gif" onclick="listTable.toggle(this, 'toggle_show_in_nav', 111)" /></td>
    <td width="10%"><img src="images/yes.gif" onclick="listTable.toggle(this, 'toggle_is_show', 111)" /></td>
    <td><span onclick="listTable.edit(this, 'edit_grade', 111)">0</span></td>
    <td width="10%" align="right"><span onclick="listTable.edit(this, 'edit_sort_order', 111)">50</span></td>
    <td width="24%" align="center">
      <a href="category.php?act=move&cat_id=111">转移商品</a> |
      <a href="category.php?act=edit&amp;cat_id=111">编辑</a> |
      <a href="javascript:;" onclick="listTable.remove(111, '您确认要删除这条记录吗?')" title="移除">移除</a>
	  <!--代码修改_start Byjdy-->
	  	  <!--代码修改_end Byjdy-->
    </td>
  </tr>
    <tr align="center" class="2" id="2_131">
    <td align="left" class="first-cell" >
            <img src="images/menu_minus.gif" id="icon_2_131" width="9" height="9" border="0" style="margin-left:2em" onclick="rowClicked(this)" />
            <span>
      <!-- 0-默认列表 1-搜索匹配列表 其他-搜索未匹配列表 -->
            <a href="goods.php?act=list&cat_id=131&supp=-1">套装</a>
            </span>
        </td>
    <td width="10%">0</td>
    <td width="10%"><span onclick="listTable.edit(this, 'edit_measure_unit', 131)"><!--  -->&nbsp;&nbsp;&nbsp;&nbsp;<!--  --></span></td>
    <td width="10%"><img src="images/no.gif" onclick="listTable.toggle(this, 'toggle_show_in_nav', 131)" /></td>
    <td width="10%"><img src="images/yes.gif" onclick="listTable.toggle(this, 'toggle_is_show', 131)" /></td>
    <td><span onclick="listTable.edit(this, 'edit_grade', 131)">0</span></td>
    <td width="10%" align="right"><span onclick="listTable.edit(this, 'edit_sort_order', 131)">50</span></td>
    <td width="24%" align="center">
      <a href="category.php?act=move&cat_id=131">转移商品</a> |
      <a href="category.php?act=edit&amp;cat_id=131">编辑</a> |
      <a href="javascript:;" onclick="listTable.remove(131, '您确认要删除这条记录吗?')" title="移除">移除</a>
	  <!--代码修改_start Byjdy-->
	  	  <!--代码修改_end Byjdy-->
    </td>
  </tr>
    <tr align="center" class="2" id="2_128">
    <td align="left" class="first-cell" >
            <img src="images/menu_minus.gif" id="icon_2_128" width="9" height="9" border="0" style="margin-left:2em" onclick="rowClicked(this)" />
            <span>
      <!-- 0-默认列表 1-搜索匹配列表 其他-搜索未匹配列表 -->
            <a href="goods.php?act=list&cat_id=128&supp=-1">手足</a>
            </span>
        </td>
    <td width="10%">0</td>
    <td width="10%"><span onclick="listTable.edit(this, 'edit_measure_unit', 128)"><!--  -->&nbsp;&nbsp;&nbsp;&nbsp;<!--  --></span></td>
    <td width="10%"><img src="images/no.gif" onclick="listTable.toggle(this, 'toggle_show_in_nav', 128)" /></td>
    <td width="10%"><img src="images/yes.gif" onclick="listTable.toggle(this, 'toggle_is_show', 128)" /></td>
    <td><span onclick="listTable.edit(this, 'edit_grade', 128)">0</span></td>
    <td width="10%" align="right"><span onclick="listTable.edit(this, 'edit_sort_order', 128)">50</span></td>
    <td width="24%" align="center">
      <a href="category.php?act=move&cat_id=128">转移商品</a> |
      <a href="category.php?act=edit&amp;cat_id=128">编辑</a> |
      <a href="javascript:;" onclick="listTable.remove(128, '您确认要删除这条记录吗?')" title="移除">移除</a>
	  <!--代码修改_start Byjdy-->
	  	  <!--代码修改_end Byjdy-->
    </td>
  </tr>
    <tr align="center" class="2" id="2_125">
    <td align="left" class="first-cell" >
            <img src="images/menu_minus.gif" id="icon_2_125" width="9" height="9" border="0" style="margin-left:2em" onclick="rowClicked(this)" />
            <span>
      <!-- 0-默认列表 1-搜索匹配列表 其他-搜索未匹配列表 -->
            <a href="goods.php?act=list&cat_id=125&supp=-1">沐浴</a>
            </span>
        </td>
    <td width="10%">0</td>
    <td width="10%"><span onclick="listTable.edit(this, 'edit_measure_unit', 125)"><!--  -->&nbsp;&nbsp;&nbsp;&nbsp;<!--  --></span></td>
    <td width="10%"><img src="images/no.gif" onclick="listTable.toggle(this, 'toggle_show_in_nav', 125)" /></td>
    <td width="10%"><img src="images/yes.gif" onclick="listTable.toggle(this, 'toggle_is_show', 125)" /></td>
    <td><span onclick="listTable.edit(this, 'edit_grade', 125)">0</span></td>
    <td width="10%" align="right"><span onclick="listTable.edit(this, 'edit_sort_order', 125)">50</span></td>
    <td width="24%" align="center">
      <a href="category.php?act=move&cat_id=125">转移商品</a> |
      <a href="category.php?act=edit&amp;cat_id=125">编辑</a> |
      <a href="javascript:;" onclick="listTable.remove(125, '您确认要删除这条记录吗?')" title="移除">移除</a>
	  <!--代码修改_start Byjdy-->
	  	  <!--代码修改_end Byjdy-->
    </td>
  </tr>
    <tr align="center" class="2" id="2_129">
    <td align="left" class="first-cell" >
            <img src="images/menu_minus.gif" id="icon_2_129" width="9" height="9" border="0" style="margin-left:2em" onclick="rowClicked(this)" />
            <span>
      <!-- 0-默认列表 1-搜索匹配列表 其他-搜索未匹配列表 -->
            <a href="goods.php?act=list&cat_id=129&supp=-1">纤体塑形</a>
            </span>
        </td>
    <td width="10%">0</td>
    <td width="10%"><span onclick="listTable.edit(this, 'edit_measure_unit', 129)"><!--  -->&nbsp;&nbsp;&nbsp;&nbsp;<!--  --></span></td>
    <td width="10%"><img src="images/no.gif" onclick="listTable.toggle(this, 'toggle_show_in_nav', 129)" /></td>
    <td width="10%"><img src="images/yes.gif" onclick="listTable.toggle(this, 'toggle_is_show', 129)" /></td>
    <td><span onclick="listTable.edit(this, 'edit_grade', 129)">0</span></td>
    <td width="10%" align="right"><span onclick="listTable.edit(this, 'edit_sort_order', 129)">50</span></td>
    <td width="24%" align="center">
      <a href="category.php?act=move&cat_id=129">转移商品</a> |
      <a href="category.php?act=edit&amp;cat_id=129">编辑</a> |
      <a href="javascript:;" onclick="listTable.remove(129, '您确认要删除这条记录吗?')" title="移除">移除</a>
	  <!--代码修改_start Byjdy-->
	  	  <!--代码修改_end Byjdy-->
    </td>
  </tr>
    <tr align="center" class="2" id="2_126">
    <td align="left" class="first-cell" >
            <img src="images/menu_minus.gif" id="icon_2_126" width="9" height="9" border="0" style="margin-left:2em" onclick="rowClicked(this)" />
            <span>
      <!-- 0-默认列表 1-搜索匹配列表 其他-搜索未匹配列表 -->
            <a href="goods.php?act=list&cat_id=126&supp=-1">润肤</a>
            </span>
        </td>
    <td width="10%">0</td>
    <td width="10%"><span onclick="listTable.edit(this, 'edit_measure_unit', 126)"><!--  -->&nbsp;&nbsp;&nbsp;&nbsp;<!--  --></span></td>
    <td width="10%"><img src="images/no.gif" onclick="listTable.toggle(this, 'toggle_show_in_nav', 126)" /></td>
    <td width="10%"><img src="images/yes.gif" onclick="listTable.toggle(this, 'toggle_is_show', 126)" /></td>
    <td><span onclick="listTable.edit(this, 'edit_grade', 126)">0</span></td>
    <td width="10%" align="right"><span onclick="listTable.edit(this, 'edit_sort_order', 126)">50</span></td>
    <td width="24%" align="center">
      <a href="category.php?act=move&cat_id=126">转移商品</a> |
      <a href="category.php?act=edit&amp;cat_id=126">编辑</a> |
      <a href="javascript:;" onclick="listTable.remove(126, '您确认要删除这条记录吗?')" title="移除">移除</a>
	  <!--代码修改_start Byjdy-->
	  	  <!--代码修改_end Byjdy-->
    </td>
  </tr>
    <tr align="center" class="2" id="2_130">
    <td align="left" class="first-cell" >
            <img src="images/menu_minus.gif" id="icon_2_130" width="9" height="9" border="0" style="margin-left:2em" onclick="rowClicked(this)" />
            <span>
      <!-- 0-默认列表 1-搜索匹配列表 其他-搜索未匹配列表 -->
            <a href="goods.php?act=list&cat_id=130&supp=-1"> 美胸</a>
            </span>
        </td>
    <td width="10%">0</td>
    <td width="10%"><span onclick="listTable.edit(this, 'edit_measure_unit', 130)"><!--  -->&nbsp;&nbsp;&nbsp;&nbsp;<!--  --></span></td>
    <td width="10%"><img src="images/no.gif" onclick="listTable.toggle(this, 'toggle_show_in_nav', 130)" /></td>
    <td width="10%"><img src="images/yes.gif" onclick="listTable.toggle(this, 'toggle_is_show', 130)" /></td>
    <td><span onclick="listTable.edit(this, 'edit_grade', 130)">0</span></td>
    <td width="10%" align="right"><span onclick="listTable.edit(this, 'edit_sort_order', 130)">50</span></td>
    <td width="24%" align="center">
      <a href="category.php?act=move&cat_id=130">转移商品</a> |
      <a href="category.php?act=edit&amp;cat_id=130">编辑</a> |
      <a href="javascript:;" onclick="listTable.remove(130, '您确认要删除这条记录吗?')" title="移除">移除</a>
	  <!--代码修改_start Byjdy-->
	  	  <!--代码修改_end Byjdy-->
    </td>
  </tr>
    <tr align="center" class="2" id="2_127">
    <td align="left" class="first-cell" >
            <img src="images/menu_minus.gif" id="icon_2_127" width="9" height="9" border="0" style="margin-left:2em" onclick="rowClicked(this)" />
            <span>
      <!-- 0-默认列表 1-搜索匹配列表 其他-搜索未匹配列表 -->
            <a href="goods.php?act=list&cat_id=127&supp=-1">颈部</a>
            </span>
        </td>
    <td width="10%">0</td>
    <td width="10%"><span onclick="listTable.edit(this, 'edit_measure_unit', 127)"><!--  -->&nbsp;&nbsp;&nbsp;&nbsp;<!--  --></span></td>
    <td width="10%"><img src="images/no.gif" onclick="listTable.toggle(this, 'toggle_show_in_nav', 127)" /></td>
    <td width="10%"><img src="images/yes.gif" onclick="listTable.toggle(this, 'toggle_is_show', 127)" /></td>
    <td><span onclick="listTable.edit(this, 'edit_grade', 127)">0</span></td>
    <td width="10%" align="right"><span onclick="listTable.edit(this, 'edit_sort_order', 127)">50</span></td>
    <td width="24%" align="center">
      <a href="category.php?act=move&cat_id=127">转移商品</a> |
      <a href="category.php?act=edit&amp;cat_id=127">编辑</a> |
      <a href="javascript:;" onclick="listTable.remove(127, '您确认要删除这条记录吗?')" title="移除">移除</a>
	  <!--代码修改_start Byjdy-->
	  	  <!--代码修改_end Byjdy-->
    </td>
  </tr>
    <tr align="center" class="0" id="0_7">
    <td align="left" class="first-cell" >
            <img src="images/menu_minus.gif" id="icon_0_7" width="9" height="9" border="0" style="margin-left:0em" onclick="rowClicked(this)" />
            <span>
      <!-- 0-默认列表 1-搜索匹配列表 其他-搜索未匹配列表 -->
            <a href="goods.php?act=list&cat_id=7&supp=-1">酒类饮料</a>
            </span>
        </td>
    <td width="10%">4</td>
    <td width="10%"><span onclick="listTable.edit(this, 'edit_measure_unit', 7)"><!--  -->&nbsp;&nbsp;&nbsp;&nbsp;<!--  --></span></td>
    <td width="10%"><img src="images/no.gif" onclick="listTable.toggle(this, 'toggle_show_in_nav', 7)" /></td>
    <td width="10%"><img src="images/yes.gif" onclick="listTable.toggle(this, 'toggle_is_show', 7)" /></td>
    <td><span onclick="listTable.edit(this, 'edit_grade', 7)">5</span></td>
    <td width="10%" align="right"><span onclick="listTable.edit(this, 'edit_sort_order', 7)">7</span></td>
    <td width="24%" align="center">
      <a href="category.php?act=move&cat_id=7">转移商品</a> |
      <a href="category.php?act=edit&amp;cat_id=7">编辑</a> |
      <a href="javascript:;" onclick="listTable.remove(7, '您确认要删除这条记录吗?')" title="移除">移除</a>
	  <!--代码修改_start Byjdy-->
	  	  | <a href="category_flashimg.php?act=list&amp;cat_id=7">设置轮播图片</a>	  
	  	  <!--代码修改_end Byjdy-->
    </td>
  </tr>
    <tr align="center" class="1" id="1_273">
    <td align="left" class="first-cell" >
            <img src="images/menu_minus.gif" id="icon_1_273" width="9" height="9" border="0" style="margin-left:1em" onclick="rowClicked(this)" />
            <span>
      <!-- 0-默认列表 1-搜索匹配列表 其他-搜索未匹配列表 -->
            <a href="goods.php?act=list&cat_id=273&supp=-1">饮料饮品</a>
            </span>
        </td>
    <td width="10%">6</td>
    <td width="10%"><span onclick="listTable.edit(this, 'edit_measure_unit', 273)"><!--  -->&nbsp;&nbsp;&nbsp;&nbsp;<!--  --></span></td>
    <td width="10%"><img src="images/no.gif" onclick="listTable.toggle(this, 'toggle_show_in_nav', 273)" /></td>
    <td width="10%"><img src="images/yes.gif" onclick="listTable.toggle(this, 'toggle_is_show', 273)" /></td>
    <td><span onclick="listTable.edit(this, 'edit_grade', 273)">0</span></td>
    <td width="10%" align="right"><span onclick="listTable.edit(this, 'edit_sort_order', 273)">50</span></td>
    <td width="24%" align="center">
      <a href="category.php?act=move&cat_id=273">转移商品</a> |
      <a href="category.php?act=edit&amp;cat_id=273">编辑</a> |
      <a href="javascript:;" onclick="listTable.remove(273, '您确认要删除这条记录吗?')" title="移除">移除</a>
	  <!--代码修改_start Byjdy-->
	  	  <!--代码修改_end Byjdy-->
    </td>
  </tr>
    <tr align="center" class="2" id="2_289">
    <td align="left" class="first-cell" >
            <img src="images/menu_minus.gif" id="icon_2_289" width="9" height="9" border="0" style="margin-left:2em" onclick="rowClicked(this)" />
            <span>
      <!-- 0-默认列表 1-搜索匹配列表 其他-搜索未匹配列表 -->
            <a href="goods.php?act=list&cat_id=289&supp=-1">果蔬汁</a>
            </span>
        </td>
    <td width="10%">0</td>
    <td width="10%"><span onclick="listTable.edit(this, 'edit_measure_unit', 289)"><!--  -->&nbsp;&nbsp;&nbsp;&nbsp;<!--  --></span></td>
    <td width="10%"><img src="images/no.gif" onclick="listTable.toggle(this, 'toggle_show_in_nav', 289)" /></td>
    <td width="10%"><img src="images/yes.gif" onclick="listTable.toggle(this, 'toggle_is_show', 289)" /></td>
    <td><span onclick="listTable.edit(this, 'edit_grade', 289)">0</span></td>
    <td width="10%" align="right"><span onclick="listTable.edit(this, 'edit_sort_order', 289)">50</span></td>
    <td width="24%" align="center">
      <a href="category.php?act=move&cat_id=289">转移商品</a> |
      <a href="category.php?act=edit&amp;cat_id=289">编辑</a> |
      <a href="javascript:;" onclick="listTable.remove(289, '您确认要删除这条记录吗?')" title="移除">移除</a>
	  <!--代码修改_start Byjdy-->
	  	  <!--代码修改_end Byjdy-->
    </td>
  </tr>
    <tr align="center" class="2" id="2_296">
    <td align="left" class="first-cell" >
            <img src="images/menu_minus.gif" id="icon_2_296" width="9" height="9" border="0" style="margin-left:2em" onclick="rowClicked(this)" />
            <span>
      <!-- 0-默认列表 1-搜索匹配列表 其他-搜索未匹配列表 -->
            <a href="goods.php?act=list&cat_id=296&supp=-1">植物蛋白饮料</a>
            </span>
        </td>
    <td width="10%">0</td>
    <td width="10%"><span onclick="listTable.edit(this, 'edit_measure_unit', 296)"><!--  -->&nbsp;&nbsp;&nbsp;&nbsp;<!--  --></span></td>
    <td width="10%"><img src="images/no.gif" onclick="listTable.toggle(this, 'toggle_show_in_nav', 296)" /></td>
    <td width="10%"><img src="images/yes.gif" onclick="listTable.toggle(this, 'toggle_is_show', 296)" /></td>
    <td><span onclick="listTable.edit(this, 'edit_grade', 296)">0</span></td>
    <td width="10%" align="right"><span onclick="listTable.edit(this, 'edit_sort_order', 296)">50</span></td>
    <td width="24%" align="center">
      <a href="category.php?act=move&cat_id=296">转移商品</a> |
      <a href="category.php?act=edit&amp;cat_id=296">编辑</a> |
      <a href="javascript:;" onclick="listTable.remove(296, '您确认要删除这条记录吗?')" title="移除">移除</a>
	  <!--代码修改_start Byjdy-->
	  	  <!--代码修改_end Byjdy-->
    </td>
  </tr>
    <tr align="center" class="2" id="2_293">
    <td align="left" class="first-cell" >
            <img src="images/menu_minus.gif" id="icon_2_293" width="9" height="9" border="0" style="margin-left:2em" onclick="rowClicked(this)" />
            <span>
      <!-- 0-默认列表 1-搜索匹配列表 其他-搜索未匹配列表 -->
            <a href="goods.php?act=list&cat_id=293&supp=-1">纯牛奶</a>
            </span>
        </td>
    <td width="10%">0</td>
    <td width="10%"><span onclick="listTable.edit(this, 'edit_measure_unit', 293)"><!--  -->&nbsp;&nbsp;&nbsp;&nbsp;<!--  --></span></td>
    <td width="10%"><img src="images/no.gif" onclick="listTable.toggle(this, 'toggle_show_in_nav', 293)" /></td>
    <td width="10%"><img src="images/yes.gif" onclick="listTable.toggle(this, 'toggle_is_show', 293)" /></td>
    <td><span onclick="listTable.edit(this, 'edit_grade', 293)">0</span></td>
    <td width="10%" align="right"><span onclick="listTable.edit(this, 'edit_sort_order', 293)">50</span></td>
    <td width="24%" align="center">
      <a href="category.php?act=move&cat_id=293">转移商品</a> |
      <a href="category.php?act=edit&amp;cat_id=293">编辑</a> |
      <a href="javascript:;" onclick="listTable.remove(293, '您确认要删除这条记录吗?')" title="移除">移除</a>
	  <!--代码修改_start Byjdy-->
	  	  <!--代码修改_end Byjdy-->
    </td>
  </tr>
    <tr align="center" class="2" id="2_290">
    <td align="left" class="first-cell" >
            <img src="images/menu_minus.gif" id="icon_2_290" width="9" height="9" border="0" style="margin-left:2em" onclick="rowClicked(this)" />
            <span>
      <!-- 0-默认列表 1-搜索匹配列表 其他-搜索未匹配列表 -->
            <a href="goods.php?act=list&cat_id=290&supp=-1">茶饮料</a>
            </span>
        </td>
    <td width="10%">0</td>
    <td width="10%"><span onclick="listTable.edit(this, 'edit_measure_unit', 290)"><!--  -->&nbsp;&nbsp;&nbsp;&nbsp;<!--  --></span></td>
    <td width="10%"><img src="images/no.gif" onclick="listTable.toggle(this, 'toggle_show_in_nav', 290)" /></td>
    <td width="10%"><img src="images/yes.gif" onclick="listTable.toggle(this, 'toggle_is_show', 290)" /></td>
    <td><span onclick="listTable.edit(this, 'edit_grade', 290)">0</span></td>
    <td width="10%" align="right"><span onclick="listTable.edit(this, 'edit_sort_order', 290)">50</span></td>
    <td width="24%" align="center">
      <a href="category.php?act=move&cat_id=290">转移商品</a> |
      <a href="category.php?act=edit&amp;cat_id=290">编辑</a> |
      <a href="javascript:;" onclick="listTable.remove(290, '您确认要删除这条记录吗?')" title="移除">移除</a>
	  <!--代码修改_start Byjdy-->
	  	  <!--代码修改_end Byjdy-->
    </td>
  </tr>
    <tr align="center" class="2" id="2_294">
    <td align="left" class="first-cell" >
            <img src="images/menu_minus.gif" id="icon_2_294" width="9" height="9" border="0" style="margin-left:2em" onclick="rowClicked(this)" />
            <span>
      <!-- 0-默认列表 1-搜索匹配列表 其他-搜索未匹配列表 -->
            <a href="goods.php?act=list&cat_id=294&supp=-1">酸奶</a>
            </span>
        </td>
    <td width="10%">0</td>
    <td width="10%"><span onclick="listTable.edit(this, 'edit_measure_unit', 294)"><!--  -->&nbsp;&nbsp;&nbsp;&nbsp;<!--  --></span></td>
    <td width="10%"><img src="images/no.gif" onclick="listTable.toggle(this, 'toggle_show_in_nav', 294)" /></td>
    <td width="10%"><img src="images/yes.gif" onclick="listTable.toggle(this, 'toggle_is_show', 294)" /></td>
    <td><span onclick="listTable.edit(this, 'edit_grade', 294)">0</span></td>
    <td width="10%" align="right"><span onclick="listTable.edit(this, 'edit_sort_order', 294)">50</span></td>
    <td width="24%" align="center">
      <a href="category.php?act=move&cat_id=294">转移商品</a> |
      <a href="category.php?act=edit&amp;cat_id=294">编辑</a> |
      <a href="javascript:;" onclick="listTable.remove(294, '您确认要删除这条记录吗?')" title="移除">移除</a>
	  <!--代码修改_start Byjdy-->
	  	  <!--代码修改_end Byjdy-->
    </td>
  </tr>
    <tr align="center" class="2" id="2_291">
    <td align="left" class="first-cell" >
            <img src="images/menu_minus.gif" id="icon_2_291" width="9" height="9" border="0" style="margin-left:2em" onclick="rowClicked(this)" />
            <span>
      <!-- 0-默认列表 1-搜索匹配列表 其他-搜索未匹配列表 -->
            <a href="goods.php?act=list&cat_id=291&supp=-1">碳酸饮料</a>
            </span>
        </td>
    <td width="10%">1</td>
    <td width="10%"><span onclick="listTable.edit(this, 'edit_measure_unit', 291)"><!--  -->&nbsp;&nbsp;&nbsp;&nbsp;<!--  --></span></td>
    <td width="10%"><img src="images/no.gif" onclick="listTable.toggle(this, 'toggle_show_in_nav', 291)" /></td>
    <td width="10%"><img src="images/yes.gif" onclick="listTable.toggle(this, 'toggle_is_show', 291)" /></td>
    <td><span onclick="listTable.edit(this, 'edit_grade', 291)">0</span></td>
    <td width="10%" align="right"><span onclick="listTable.edit(this, 'edit_sort_order', 291)">50</span></td>
    <td width="24%" align="center">
      <a href="category.php?act=move&cat_id=291">转移商品</a> |
      <a href="category.php?act=edit&amp;cat_id=291">编辑</a> |
      <a href="javascript:;" onclick="listTable.remove(291, '您确认要删除这条记录吗?')" title="移除">移除</a>
	  <!--代码修改_start Byjdy-->
	  	  <!--代码修改_end Byjdy-->
    </td>
  </tr>
    <tr align="center" class="2" id="2_295">
    <td align="left" class="first-cell" >
            <img src="images/menu_minus.gif" id="icon_2_295" width="9" height="9" border="0" style="margin-left:2em" onclick="rowClicked(this)" />
            <span>
      <!-- 0-默认列表 1-搜索匹配列表 其他-搜索未匹配列表 -->
            <a href="goods.php?act=list&cat_id=295&supp=-1">风味奶</a>
            </span>
        </td>
    <td width="10%">0</td>
    <td width="10%"><span onclick="listTable.edit(this, 'edit_measure_unit', 295)"><!--  -->&nbsp;&nbsp;&nbsp;&nbsp;<!--  --></span></td>
    <td width="10%"><img src="images/no.gif" onclick="listTable.toggle(this, 'toggle_show_in_nav', 295)" /></td>
    <td width="10%"><img src="images/yes.gif" onclick="listTable.toggle(this, 'toggle_is_show', 295)" /></td>
    <td><span onclick="listTable.edit(this, 'edit_grade', 295)">0</span></td>
    <td width="10%" align="right"><span onclick="listTable.edit(this, 'edit_sort_order', 295)">50</span></td>
    <td width="24%" align="center">
      <a href="category.php?act=move&cat_id=295">转移商品</a> |
      <a href="category.php?act=edit&amp;cat_id=295">编辑</a> |
      <a href="javascript:;" onclick="listTable.remove(295, '您确认要删除这条记录吗?')" title="移除">移除</a>
	  <!--代码修改_start Byjdy-->
	  	  <!--代码修改_end Byjdy-->
    </td>
  </tr>
    <tr align="center" class="2" id="2_292">
    <td align="left" class="first-cell" >
            <img src="images/menu_minus.gif" id="icon_2_292" width="9" height="9" border="0" style="margin-left:2em" onclick="rowClicked(this)" />
            <span>
      <!-- 0-默认列表 1-搜索匹配列表 其他-搜索未匹配列表 -->
            <a href="goods.php?act=list&cat_id=292&supp=-1">功能饮料</a>
            </span>
        </td>
    <td width="10%">1</td>
    <td width="10%"><span onclick="listTable.edit(this, 'edit_measure_unit', 292)"><!--  -->&nbsp;&nbsp;&nbsp;&nbsp;<!--  --></span></td>
    <td width="10%"><img src="images/no.gif" onclick="listTable.toggle(this, 'toggle_show_in_nav', 292)" /></td>
    <td width="10%"><img src="images/yes.gif" onclick="listTable.toggle(this, 'toggle_is_show', 292)" /></td>
    <td><span onclick="listTable.edit(this, 'edit_grade', 292)">0</span></td>
    <td width="10%" align="right"><span onclick="listTable.edit(this, 'edit_sort_order', 292)">50</span></td>
    <td width="24%" align="center">
      <a href="category.php?act=move&cat_id=292">转移商品</a> |
      <a href="category.php?act=edit&amp;cat_id=292">编辑</a> |
      <a href="javascript:;" onclick="listTable.remove(292, '您确认要删除这条记录吗?')" title="移除">移除</a>
	  <!--代码修改_start Byjdy-->
	  	  <!--代码修改_end Byjdy-->
    </td>
  </tr>
    <tr align="center" class="1" id="1_274">
    <td align="left" class="first-cell" >
            <img src="images/menu_minus.gif" id="icon_1_274" width="9" height="9" border="0" style="margin-left:1em" onclick="rowClicked(this)" />
            <span>
      <!-- 0-默认列表 1-搜索匹配列表 其他-搜索未匹配列表 -->
            <a href="goods.php?act=list&cat_id=274&supp=-1">茗茶</a>
            </span>
        </td>
    <td width="10%">0</td>
    <td width="10%"><span onclick="listTable.edit(this, 'edit_measure_unit', 274)"><!--  -->&nbsp;&nbsp;&nbsp;&nbsp;<!--  --></span></td>
    <td width="10%"><img src="images/no.gif" onclick="listTable.toggle(this, 'toggle_show_in_nav', 274)" /></td>
    <td width="10%"><img src="images/yes.gif" onclick="listTable.toggle(this, 'toggle_is_show', 274)" /></td>
    <td><span onclick="listTable.edit(this, 'edit_grade', 274)">0</span></td>
    <td width="10%" align="right"><span onclick="listTable.edit(this, 'edit_sort_order', 274)">50</span></td>
    <td width="24%" align="center">
      <a href="category.php?act=move&cat_id=274">转移商品</a> |
      <a href="category.php?act=edit&amp;cat_id=274">编辑</a> |
      <a href="javascript:;" onclick="listTable.remove(274, '您确认要删除这条记录吗?')" title="移除">移除</a>
	  <!--代码修改_start Byjdy-->
	  	  <!--代码修改_end Byjdy-->
    </td>
  </tr>
    <tr align="center" class="2" id="2_305">
    <td align="left" class="first-cell" >
            <img src="images/menu_minus.gif" id="icon_2_305" width="9" height="9" border="0" style="margin-left:2em" onclick="rowClicked(this)" />
            <span>
      <!-- 0-默认列表 1-搜索匹配列表 其他-搜索未匹配列表 -->
            <a href="goods.php?act=list&cat_id=305&supp=-1">其他茶</a>
            </span>
        </td>
    <td width="10%">0</td>
    <td width="10%"><span onclick="listTable.edit(this, 'edit_measure_unit', 305)"><!--  -->&nbsp;&nbsp;&nbsp;&nbsp;<!--  --></span></td>
    <td width="10%"><img src="images/no.gif" onclick="listTable.toggle(this, 'toggle_show_in_nav', 305)" /></td>
    <td width="10%"><img src="images/yes.gif" onclick="listTable.toggle(this, 'toggle_is_show', 305)" /></td>
    <td><span onclick="listTable.edit(this, 'edit_grade', 305)">0</span></td>
    <td width="10%" align="right"><span onclick="listTable.edit(this, 'edit_sort_order', 305)">50</span></td>
    <td width="24%" align="center">
      <a href="category.php?act=move&cat_id=305">转移商品</a> |
      <a href="category.php?act=edit&amp;cat_id=305">编辑</a> |
      <a href="javascript:;" onclick="listTable.remove(305, '您确认要删除这条记录吗?')" title="移除">移除</a>
	  <!--代码修改_start Byjdy-->
	  	  <!--代码修改_end Byjdy-->
    </td>
  </tr>
    <tr align="center" class="2" id="2_302">
    <td align="left" class="first-cell" >
            <img src="images/menu_minus.gif" id="icon_2_302" width="9" height="9" border="0" style="margin-left:2em" onclick="rowClicked(this)" />
            <span>
      <!-- 0-默认列表 1-搜索匹配列表 其他-搜索未匹配列表 -->
            <a href="goods.php?act=list&cat_id=302&supp=-1">红茶</a>
            </span>
        </td>
    <td width="10%">0</td>
    <td width="10%"><span onclick="listTable.edit(this, 'edit_measure_unit', 302)"><!--  -->&nbsp;&nbsp;&nbsp;&nbsp;<!--  --></span></td>
    <td width="10%"><img src="images/no.gif" onclick="listTable.toggle(this, 'toggle_show_in_nav', 302)" /></td>
    <td width="10%"><img src="images/yes.gif" onclick="listTable.toggle(this, 'toggle_is_show', 302)" /></td>
    <td><span onclick="listTable.edit(this, 'edit_grade', 302)">0</span></td>
    <td width="10%" align="right"><span onclick="listTable.edit(this, 'edit_sort_order', 302)">50</span></td>
    <td width="24%" align="center">
      <a href="category.php?act=move&cat_id=302">转移商品</a> |
      <a href="category.php?act=edit&amp;cat_id=302">编辑</a> |
      <a href="javascript:;" onclick="listTable.remove(302, '您确认要删除这条记录吗?')" title="移除">移除</a>
	  <!--代码修改_start Byjdy-->
	  	  <!--代码修改_end Byjdy-->
    </td>
  </tr>
    <tr align="center" class="2" id="2_299">
    <td align="left" class="first-cell" >
            <img src="images/menu_minus.gif" id="icon_2_299" width="9" height="9" border="0" style="margin-left:2em" onclick="rowClicked(this)" />
            <span>
      <!-- 0-默认列表 1-搜索匹配列表 其他-搜索未匹配列表 -->
            <a href="goods.php?act=list&cat_id=299&supp=-1">普洱</a>
            </span>
        </td>
    <td width="10%">0</td>
    <td width="10%"><span onclick="listTable.edit(this, 'edit_measure_unit', 299)"><!--  -->&nbsp;&nbsp;&nbsp;&nbsp;<!--  --></span></td>
    <td width="10%"><img src="images/no.gif" onclick="listTable.toggle(this, 'toggle_show_in_nav', 299)" /></td>
    <td width="10%"><img src="images/yes.gif" onclick="listTable.toggle(this, 'toggle_is_show', 299)" /></td>
    <td><span onclick="listTable.edit(this, 'edit_grade', 299)">0</span></td>
    <td width="10%" align="right"><span onclick="listTable.edit(this, 'edit_sort_order', 299)">50</span></td>
    <td width="24%" align="center">
      <a href="category.php?act=move&cat_id=299">转移商品</a> |
      <a href="category.php?act=edit&amp;cat_id=299">编辑</a> |
      <a href="javascript:;" onclick="listTable.remove(299, '您确认要删除这条记录吗?')" title="移除">移除</a>
	  <!--代码修改_start Byjdy-->
	  	  <!--代码修改_end Byjdy-->
    </td>
  </tr>
    <tr align="center" class="2" id="2_303">
    <td align="left" class="first-cell" >
            <img src="images/menu_minus.gif" id="icon_2_303" width="9" height="9" border="0" style="margin-left:2em" onclick="rowClicked(this)" />
            <span>
      <!-- 0-默认列表 1-搜索匹配列表 其他-搜索未匹配列表 -->
            <a href="goods.php?act=list&cat_id=303&supp=-1">花果茶</a>
            </span>
        </td>
    <td width="10%">0</td>
    <td width="10%"><span onclick="listTable.edit(this, 'edit_measure_unit', 303)"><!--  -->&nbsp;&nbsp;&nbsp;&nbsp;<!--  --></span></td>
    <td width="10%"><img src="images/no.gif" onclick="listTable.toggle(this, 'toggle_show_in_nav', 303)" /></td>
    <td width="10%"><img src="images/yes.gif" onclick="listTable.toggle(this, 'toggle_is_show', 303)" /></td>
    <td><span onclick="listTable.edit(this, 'edit_grade', 303)">0</span></td>
    <td width="10%" align="right"><span onclick="listTable.edit(this, 'edit_sort_order', 303)">50</span></td>
    <td width="24%" align="center">
      <a href="category.php?act=move&cat_id=303">转移商品</a> |
      <a href="category.php?act=edit&amp;cat_id=303">编辑</a> |
      <a href="javascript:;" onclick="listTable.remove(303, '您确认要删除这条记录吗?')" title="移除">移除</a>
	  <!--代码修改_start Byjdy-->
	  	  <!--代码修改_end Byjdy-->
    </td>
  </tr>
    <tr align="center" class="2" id="2_300">
    <td align="left" class="first-cell" >
            <img src="images/menu_minus.gif" id="icon_2_300" width="9" height="9" border="0" style="margin-left:2em" onclick="rowClicked(this)" />
            <span>
      <!-- 0-默认列表 1-搜索匹配列表 其他-搜索未匹配列表 -->
            <a href="goods.php?act=list&cat_id=300&supp=-1">龙井</a>
            </span>
        </td>
    <td width="10%">0</td>
    <td width="10%"><span onclick="listTable.edit(this, 'edit_measure_unit', 300)"><!--  -->&nbsp;&nbsp;&nbsp;&nbsp;<!--  --></span></td>
    <td width="10%"><img src="images/no.gif" onclick="listTable.toggle(this, 'toggle_show_in_nav', 300)" /></td>
    <td width="10%"><img src="images/yes.gif" onclick="listTable.toggle(this, 'toggle_is_show', 300)" /></td>
    <td><span onclick="listTable.edit(this, 'edit_grade', 300)">0</span></td>
    <td width="10%" align="right"><span onclick="listTable.edit(this, 'edit_sort_order', 300)">50</span></td>
    <td width="24%" align="center">
      <a href="category.php?act=move&cat_id=300">转移商品</a> |
      <a href="category.php?act=edit&amp;cat_id=300">编辑</a> |
      <a href="javascript:;" onclick="listTable.remove(300, '您确认要删除这条记录吗?')" title="移除">移除</a>
	  <!--代码修改_start Byjdy-->
	  	  <!--代码修改_end Byjdy-->
    </td>
  </tr>
    <tr align="center" class="2" id="2_304">
    <td align="left" class="first-cell" >
            <img src="images/menu_minus.gif" id="icon_2_304" width="9" height="9" border="0" style="margin-left:2em" onclick="rowClicked(this)" />
            <span>
      <!-- 0-默认列表 1-搜索匹配列表 其他-搜索未匹配列表 -->
            <a href="goods.php?act=list&cat_id=304&supp=-1">养生茶</a>
            </span>
        </td>
    <td width="10%">0</td>
    <td width="10%"><span onclick="listTable.edit(this, 'edit_measure_unit', 304)"><!--  -->&nbsp;&nbsp;&nbsp;&nbsp;<!--  --></span></td>
    <td width="10%"><img src="images/no.gif" onclick="listTable.toggle(this, 'toggle_show_in_nav', 304)" /></td>
    <td width="10%"><img src="images/yes.gif" onclick="listTable.toggle(this, 'toggle_is_show', 304)" /></td>
    <td><span onclick="listTable.edit(this, 'edit_grade', 304)">0</span></td>
    <td width="10%" align="right"><span onclick="listTable.edit(this, 'edit_sort_order', 304)">50</span></td>
    <td width="24%" align="center">
      <a href="category.php?act=move&cat_id=304">转移商品</a> |
      <a href="category.php?act=edit&amp;cat_id=304">编辑</a> |
      <a href="javascript:;" onclick="listTable.remove(304, '您确认要删除这条记录吗?')" title="移除">移除</a>
	  <!--代码修改_start Byjdy-->
	  	  <!--代码修改_end Byjdy-->
    </td>
  </tr>
    <tr align="center" class="2" id="2_301">
    <td align="left" class="first-cell" >
            <img src="images/menu_minus.gif" id="icon_2_301" width="9" height="9" border="0" style="margin-left:2em" onclick="rowClicked(this)" />
            <span>
      <!-- 0-默认列表 1-搜索匹配列表 其他-搜索未匹配列表 -->
            <a href="goods.php?act=list&cat_id=301&supp=-1">绿茶</a>
            </span>
        </td>
    <td width="10%">0</td>
    <td width="10%"><span onclick="listTable.edit(this, 'edit_measure_unit', 301)"><!--  -->&nbsp;&nbsp;&nbsp;&nbsp;<!--  --></span></td>
    <td width="10%"><img src="images/no.gif" onclick="listTable.toggle(this, 'toggle_show_in_nav', 301)" /></td>
    <td width="10%"><img src="images/yes.gif" onclick="listTable.toggle(this, 'toggle_is_show', 301)" /></td>
    <td><span onclick="listTable.edit(this, 'edit_grade', 301)">0</span></td>
    <td width="10%" align="right"><span onclick="listTable.edit(this, 'edit_sort_order', 301)">50</span></td>
    <td width="24%" align="center">
      <a href="category.php?act=move&cat_id=301">转移商品</a> |
      <a href="category.php?act=edit&amp;cat_id=301">编辑</a> |
      <a href="javascript:;" onclick="listTable.remove(301, '您确认要删除这条记录吗?')" title="移除">移除</a>
	  <!--代码修改_start Byjdy-->
	  	  <!--代码修改_end Byjdy-->
    </td>
  </tr>
    <tr align="center" class="2" id="2_298">
    <td align="left" class="first-cell" >
            <img src="images/menu_minus.gif" id="icon_2_298" width="9" height="9" border="0" style="margin-left:2em" onclick="rowClicked(this)" />
            <span>
      <!-- 0-默认列表 1-搜索匹配列表 其他-搜索未匹配列表 -->
            <a href="goods.php?act=list&cat_id=298&supp=-1">铁观音</a>
            </span>
        </td>
    <td width="10%">0</td>
    <td width="10%"><span onclick="listTable.edit(this, 'edit_measure_unit', 298)"><!--  -->&nbsp;&nbsp;&nbsp;&nbsp;<!--  --></span></td>
    <td width="10%"><img src="images/no.gif" onclick="listTable.toggle(this, 'toggle_show_in_nav', 298)" /></td>
    <td width="10%"><img src="images/yes.gif" onclick="listTable.toggle(this, 'toggle_is_show', 298)" /></td>
    <td><span onclick="listTable.edit(this, 'edit_grade', 298)">0</span></td>
    <td width="10%" align="right"><span onclick="listTable.edit(this, 'edit_sort_order', 298)">50</span></td>
    <td width="24%" align="center">
      <a href="category.php?act=move&cat_id=298">转移商品</a> |
      <a href="category.php?act=edit&amp;cat_id=298">编辑</a> |
      <a href="javascript:;" onclick="listTable.remove(298, '您确认要删除这条记录吗?')" title="移除">移除</a>
	  <!--代码修改_start Byjdy-->
	  	  <!--代码修改_end Byjdy-->
    </td>
  </tr>
    <tr align="center" class="1" id="1_271">
    <td align="left" class="first-cell" >
            <img src="images/menu_minus.gif" id="icon_1_271" width="9" height="9" border="0" style="margin-left:1em" onclick="rowClicked(this)" />
            <span>
      <!-- 0-默认列表 1-搜索匹配列表 其他-搜索未匹配列表 -->
            <a href="goods.php?act=list&cat_id=271&supp=-1">酒水</a>
            </span>
        </td>
    <td width="10%">0</td>
    <td width="10%"><span onclick="listTable.edit(this, 'edit_measure_unit', 271)"><!--  -->&nbsp;&nbsp;&nbsp;&nbsp;<!--  --></span></td>
    <td width="10%"><img src="images/no.gif" onclick="listTable.toggle(this, 'toggle_show_in_nav', 271)" /></td>
    <td width="10%"><img src="images/yes.gif" onclick="listTable.toggle(this, 'toggle_is_show', 271)" /></td>
    <td><span onclick="listTable.edit(this, 'edit_grade', 271)">0</span></td>
    <td width="10%" align="right"><span onclick="listTable.edit(this, 'edit_sort_order', 271)">50</span></td>
    <td width="24%" align="center">
      <a href="category.php?act=move&cat_id=271">转移商品</a> |
      <a href="category.php?act=edit&amp;cat_id=271">编辑</a> |
      <a href="javascript:;" onclick="listTable.remove(271, '您确认要删除这条记录吗?')" title="移除">移除</a>
	  <!--代码修改_start Byjdy-->
	  	  <!--代码修改_end Byjdy-->
    </td>
  </tr>
    <tr align="center" class="2" id="2_280">
    <td align="left" class="first-cell" >
            <img src="images/menu_minus.gif" id="icon_2_280" width="9" height="9" border="0" style="margin-left:2em" onclick="rowClicked(this)" />
            <span>
      <!-- 0-默认列表 1-搜索匹配列表 其他-搜索未匹配列表 -->
            <a href="goods.php?act=list&cat_id=280&supp=-1">养生酒</a>
            </span>
        </td>
    <td width="10%">0</td>
    <td width="10%"><span onclick="listTable.edit(this, 'edit_measure_unit', 280)"><!--  -->&nbsp;&nbsp;&nbsp;&nbsp;<!--  --></span></td>
    <td width="10%"><img src="images/no.gif" onclick="listTable.toggle(this, 'toggle_show_in_nav', 280)" /></td>
    <td width="10%"><img src="images/yes.gif" onclick="listTable.toggle(this, 'toggle_is_show', 280)" /></td>
    <td><span onclick="listTable.edit(this, 'edit_grade', 280)">0</span></td>
    <td width="10%" align="right"><span onclick="listTable.edit(this, 'edit_sort_order', 280)">50</span></td>
    <td width="24%" align="center">
      <a href="category.php?act=move&cat_id=280">转移商品</a> |
      <a href="category.php?act=edit&amp;cat_id=280">编辑</a> |
      <a href="javascript:;" onclick="listTable.remove(280, '您确认要删除这条记录吗?')" title="移除">移除</a>
	  <!--代码修改_start Byjdy-->
	  	  <!--代码修改_end Byjdy-->
    </td>
  </tr>
    <tr align="center" class="2" id="2_277">
    <td align="left" class="first-cell" >
            <img src="images/menu_minus.gif" id="icon_2_277" width="9" height="9" border="0" style="margin-left:2em" onclick="rowClicked(this)" />
            <span>
      <!-- 0-默认列表 1-搜索匹配列表 其他-搜索未匹配列表 -->
            <a href="goods.php?act=list&cat_id=277&supp=-1">啤酒</a>
            </span>
        </td>
    <td width="10%">0</td>
    <td width="10%"><span onclick="listTable.edit(this, 'edit_measure_unit', 277)"><!--  -->&nbsp;&nbsp;&nbsp;&nbsp;<!--  --></span></td>
    <td width="10%"><img src="images/no.gif" onclick="listTable.toggle(this, 'toggle_show_in_nav', 277)" /></td>
    <td width="10%"><img src="images/yes.gif" onclick="listTable.toggle(this, 'toggle_is_show', 277)" /></td>
    <td><span onclick="listTable.edit(this, 'edit_grade', 277)">0</span></td>
    <td width="10%" align="right"><span onclick="listTable.edit(this, 'edit_sort_order', 277)">50</span></td>
    <td width="24%" align="center">
      <a href="category.php?act=move&cat_id=277">转移商品</a> |
      <a href="category.php?act=edit&amp;cat_id=277">编辑</a> |
      <a href="javascript:;" onclick="listTable.remove(277, '您确认要删除这条记录吗?')" title="移除">移除</a>
	  <!--代码修改_start Byjdy-->
	  	  <!--代码修改_end Byjdy-->
    </td>
  </tr>
    <tr align="center" class="2" id="2_281">
    <td align="left" class="first-cell" >
            <img src="images/menu_minus.gif" id="icon_2_281" width="9" height="9" border="0" style="margin-left:2em" onclick="rowClicked(this)" />
            <span>
      <!-- 0-默认列表 1-搜索匹配列表 其他-搜索未匹配列表 -->
            <a href="goods.php?act=list&cat_id=281&supp=-1">预调酒</a>
            </span>
        </td>
    <td width="10%">2</td>
    <td width="10%"><span onclick="listTable.edit(this, 'edit_measure_unit', 281)"><!--  -->&nbsp;&nbsp;&nbsp;&nbsp;<!--  --></span></td>
    <td width="10%"><img src="images/no.gif" onclick="listTable.toggle(this, 'toggle_show_in_nav', 281)" /></td>
    <td width="10%"><img src="images/yes.gif" onclick="listTable.toggle(this, 'toggle_is_show', 281)" /></td>
    <td><span onclick="listTable.edit(this, 'edit_grade', 281)">0</span></td>
    <td width="10%" align="right"><span onclick="listTable.edit(this, 'edit_sort_order', 281)">50</span></td>
    <td width="24%" align="center">
      <a href="category.php?act=move&cat_id=281">转移商品</a> |
      <a href="category.php?act=edit&amp;cat_id=281">编辑</a> |
      <a href="javascript:;" onclick="listTable.remove(281, '您确认要删除这条记录吗?')" title="移除">移除</a>
	  <!--代码修改_start Byjdy-->
	  	  <!--代码修改_end Byjdy-->
    </td>
  </tr>
    <tr align="center" class="2" id="2_278">
    <td align="left" class="first-cell" >
            <img src="images/menu_minus.gif" id="icon_2_278" width="9" height="9" border="0" style="margin-left:2em" onclick="rowClicked(this)" />
            <span>
      <!-- 0-默认列表 1-搜索匹配列表 其他-搜索未匹配列表 -->
            <a href="goods.php?act=list&cat_id=278&supp=-1">葡萄酒/果酒</a>
            </span>
        </td>
    <td width="10%">1</td>
    <td width="10%"><span onclick="listTable.edit(this, 'edit_measure_unit', 278)"><!--  -->&nbsp;&nbsp;&nbsp;&nbsp;<!--  --></span></td>
    <td width="10%"><img src="images/no.gif" onclick="listTable.toggle(this, 'toggle_show_in_nav', 278)" /></td>
    <td width="10%"><img src="images/yes.gif" onclick="listTable.toggle(this, 'toggle_is_show', 278)" /></td>
    <td><span onclick="listTable.edit(this, 'edit_grade', 278)">0</span></td>
    <td width="10%" align="right"><span onclick="listTable.edit(this, 'edit_sort_order', 278)">50</span></td>
    <td width="24%" align="center">
      <a href="category.php?act=move&cat_id=278">转移商品</a> |
      <a href="category.php?act=edit&amp;cat_id=278">编辑</a> |
      <a href="javascript:;" onclick="listTable.remove(278, '您确认要删除这条记录吗?')" title="移除">移除</a>
	  <!--代码修改_start Byjdy-->
	  	  <!--代码修改_end Byjdy-->
    </td>
  </tr>
    <tr align="center" class="2" id="2_279">
    <td align="left" class="first-cell" >
            <img src="images/menu_minus.gif" id="icon_2_279" width="9" height="9" border="0" style="margin-left:2em" onclick="rowClicked(this)" />
            <span>
      <!-- 0-默认列表 1-搜索匹配列表 其他-搜索未匹配列表 -->
            <a href="goods.php?act=list&cat_id=279&supp=-1">黄酒/米酒</a>
            </span>
        </td>
    <td width="10%">0</td>
    <td width="10%"><span onclick="listTable.edit(this, 'edit_measure_unit', 279)"><!--  -->&nbsp;&nbsp;&nbsp;&nbsp;<!--  --></span></td>
    <td width="10%"><img src="images/no.gif" onclick="listTable.toggle(this, 'toggle_show_in_nav', 279)" /></td>
    <td width="10%"><img src="images/yes.gif" onclick="listTable.toggle(this, 'toggle_is_show', 279)" /></td>
    <td><span onclick="listTable.edit(this, 'edit_grade', 279)">0</span></td>
    <td width="10%" align="right"><span onclick="listTable.edit(this, 'edit_sort_order', 279)">50</span></td>
    <td width="24%" align="center">
      <a href="category.php?act=move&cat_id=279">转移商品</a> |
      <a href="category.php?act=edit&amp;cat_id=279">编辑</a> |
      <a href="javascript:;" onclick="listTable.remove(279, '您确认要删除这条记录吗?')" title="移除">移除</a>
	  <!--代码修改_start Byjdy-->
	  	  <!--代码修改_end Byjdy-->
    </td>
  </tr>
    <tr align="center" class="2" id="2_276">
    <td align="left" class="first-cell" >
            <img src="images/menu_minus.gif" id="icon_2_276" width="9" height="9" border="0" style="margin-left:2em" onclick="rowClicked(this)" />
            <span>
      <!-- 0-默认列表 1-搜索匹配列表 其他-搜索未匹配列表 -->
            <a href="goods.php?act=list&cat_id=276&supp=-1">白酒</a>
            </span>
        </td>
    <td width="10%">5</td>
    <td width="10%"><span onclick="listTable.edit(this, 'edit_measure_unit', 276)"><!--  -->&nbsp;&nbsp;&nbsp;&nbsp;<!--  --></span></td>
    <td width="10%"><img src="images/no.gif" onclick="listTable.toggle(this, 'toggle_show_in_nav', 276)" /></td>
    <td width="10%"><img src="images/yes.gif" onclick="listTable.toggle(this, 'toggle_is_show', 276)" /></td>
    <td><span onclick="listTable.edit(this, 'edit_grade', 276)">0</span></td>
    <td width="10%" align="right"><span onclick="listTable.edit(this, 'edit_sort_order', 276)">50</span></td>
    <td width="24%" align="center">
      <a href="category.php?act=move&cat_id=276">转移商品</a> |
      <a href="category.php?act=edit&amp;cat_id=276">编辑</a> |
      <a href="javascript:;" onclick="listTable.remove(276, '您确认要删除这条记录吗?')" title="移除">移除</a>
	  <!--代码修改_start Byjdy-->
	  	  <!--代码修改_end Byjdy-->
    </td>
  </tr>
    <tr align="center" class="1" id="1_272">
    <td align="left" class="first-cell" >
            <img src="images/menu_minus.gif" id="icon_1_272" width="9" height="9" border="0" style="margin-left:1em" onclick="rowClicked(this)" />
            <span>
      <!-- 0-默认列表 1-搜索匹配列表 其他-搜索未匹配列表 -->
            <a href="goods.php?act=list&cat_id=272&supp=-1">冲调饮品</a>
            </span>
        </td>
    <td width="10%">2</td>
    <td width="10%"><span onclick="listTable.edit(this, 'edit_measure_unit', 272)"><!--  -->&nbsp;&nbsp;&nbsp;&nbsp;<!--  --></span></td>
    <td width="10%"><img src="images/no.gif" onclick="listTable.toggle(this, 'toggle_show_in_nav', 272)" /></td>
    <td width="10%"><img src="images/yes.gif" onclick="listTable.toggle(this, 'toggle_is_show', 272)" /></td>
    <td><span onclick="listTable.edit(this, 'edit_grade', 272)">0</span></td>
    <td width="10%" align="right"><span onclick="listTable.edit(this, 'edit_sort_order', 272)">50</span></td>
    <td width="24%" align="center">
      <a href="category.php?act=move&cat_id=272">转移商品</a> |
      <a href="category.php?act=edit&amp;cat_id=272">编辑</a> |
      <a href="javascript:;" onclick="listTable.remove(272, '您确认要删除这条记录吗?')" title="移除">移除</a>
	  <!--代码修改_start Byjdy-->
	  	  <!--代码修改_end Byjdy-->
    </td>
  </tr>
    <tr align="center" class="2" id="2_286">
    <td align="left" class="first-cell" >
            <img src="images/menu_minus.gif" id="icon_2_286" width="9" height="9" border="0" style="margin-left:2em" onclick="rowClicked(this)" />
            <span>
      <!-- 0-默认列表 1-搜索匹配列表 其他-搜索未匹配列表 -->
            <a href="goods.php?act=list&cat_id=286&supp=-1">奶茶</a>
            </span>
        </td>
    <td width="10%">0</td>
    <td width="10%"><span onclick="listTable.edit(this, 'edit_measure_unit', 286)"><!--  -->&nbsp;&nbsp;&nbsp;&nbsp;<!--  --></span></td>
    <td width="10%"><img src="images/no.gif" onclick="listTable.toggle(this, 'toggle_show_in_nav', 286)" /></td>
    <td width="10%"><img src="images/yes.gif" onclick="listTable.toggle(this, 'toggle_is_show', 286)" /></td>
    <td><span onclick="listTable.edit(this, 'edit_grade', 286)">0</span></td>
    <td width="10%" align="right"><span onclick="listTable.edit(this, 'edit_sort_order', 286)">50</span></td>
    <td width="24%" align="center">
      <a href="category.php?act=move&cat_id=286">转移商品</a> |
      <a href="category.php?act=edit&amp;cat_id=286">编辑</a> |
      <a href="javascript:;" onclick="listTable.remove(286, '您确认要删除这条记录吗?')" title="移除">移除</a>
	  <!--代码修改_start Byjdy-->
	  	  <!--代码修改_end Byjdy-->
    </td>
  </tr>
    <tr align="center" class="2" id="2_283">
    <td align="left" class="first-cell" >
            <img src="images/menu_minus.gif" id="icon_2_283" width="9" height="9" border="0" style="margin-left:2em" onclick="rowClicked(this)" />
            <span>
      <!-- 0-默认列表 1-搜索匹配列表 其他-搜索未匹配列表 -->
            <a href="goods.php?act=list&cat_id=283&supp=-1">成人奶粉</a>
            </span>
        </td>
    <td width="10%">4</td>
    <td width="10%"><span onclick="listTable.edit(this, 'edit_measure_unit', 283)"><!--  -->&nbsp;&nbsp;&nbsp;&nbsp;<!--  --></span></td>
    <td width="10%"><img src="images/no.gif" onclick="listTable.toggle(this, 'toggle_show_in_nav', 283)" /></td>
    <td width="10%"><img src="images/yes.gif" onclick="listTable.toggle(this, 'toggle_is_show', 283)" /></td>
    <td><span onclick="listTable.edit(this, 'edit_grade', 283)">0</span></td>
    <td width="10%" align="right"><span onclick="listTable.edit(this, 'edit_sort_order', 283)">50</span></td>
    <td width="24%" align="center">
      <a href="category.php?act=move&cat_id=283">转移商品</a> |
      <a href="category.php?act=edit&amp;cat_id=283">编辑</a> |
      <a href="javascript:;" onclick="listTable.remove(283, '您确认要删除这条记录吗?')" title="移除">移除</a>
	  <!--代码修改_start Byjdy-->
	  	  <!--代码修改_end Byjdy-->
    </td>
  </tr>
    <tr align="center" class="2" id="2_287">
    <td align="left" class="first-cell" >
            <img src="images/menu_minus.gif" id="icon_2_287" width="9" height="9" border="0" style="margin-left:2em" onclick="rowClicked(this)" />
            <span>
      <!-- 0-默认列表 1-搜索匹配列表 其他-搜索未匹配列表 -->
            <a href="goods.php?act=list&cat_id=287&supp=-1">麦片谷物</a>
            </span>
        </td>
    <td width="10%">0</td>
    <td width="10%"><span onclick="listTable.edit(this, 'edit_measure_unit', 287)"><!--  -->&nbsp;&nbsp;&nbsp;&nbsp;<!--  --></span></td>
    <td width="10%"><img src="images/no.gif" onclick="listTable.toggle(this, 'toggle_show_in_nav', 287)" /></td>
    <td width="10%"><img src="images/yes.gif" onclick="listTable.toggle(this, 'toggle_is_show', 287)" /></td>
    <td><span onclick="listTable.edit(this, 'edit_grade', 287)">0</span></td>
    <td width="10%" align="right"><span onclick="listTable.edit(this, 'edit_sort_order', 287)">50</span></td>
    <td width="24%" align="center">
      <a href="category.php?act=move&cat_id=287">转移商品</a> |
      <a href="category.php?act=edit&amp;cat_id=287">编辑</a> |
      <a href="javascript:;" onclick="listTable.remove(287, '您确认要删除这条记录吗?')" title="移除">移除</a>
	  <!--代码修改_start Byjdy-->
	  	  <!--代码修改_end Byjdy-->
    </td>
  </tr>
    <tr align="center" class="2" id="2_284">
    <td align="left" class="first-cell" >
            <img src="images/menu_minus.gif" id="icon_2_284" width="9" height="9" border="0" style="margin-left:2em" onclick="rowClicked(this)" />
            <span>
      <!-- 0-默认列表 1-搜索匹配列表 其他-搜索未匹配列表 -->
            <a href="goods.php?act=list&cat_id=284&supp=-1">豆浆/豆奶粉</a>
            </span>
        </td>
    <td width="10%">1</td>
    <td width="10%"><span onclick="listTable.edit(this, 'edit_measure_unit', 284)"><!--  -->&nbsp;&nbsp;&nbsp;&nbsp;<!--  --></span></td>
    <td width="10%"><img src="images/no.gif" onclick="listTable.toggle(this, 'toggle_show_in_nav', 284)" /></td>
    <td width="10%"><img src="images/yes.gif" onclick="listTable.toggle(this, 'toggle_is_show', 284)" /></td>
    <td><span onclick="listTable.edit(this, 'edit_grade', 284)">0</span></td>
    <td width="10%" align="right"><span onclick="listTable.edit(this, 'edit_sort_order', 284)">50</span></td>
    <td width="24%" align="center">
      <a href="category.php?act=move&cat_id=284">转移商品</a> |
      <a href="category.php?act=edit&amp;cat_id=284">编辑</a> |
      <a href="javascript:;" onclick="listTable.remove(284, '您确认要删除这条记录吗?')" title="移除">移除</a>
	  <!--代码修改_start Byjdy-->
	  	  <!--代码修改_end Byjdy-->
    </td>
  </tr>
    <tr align="center" class="2" id="2_297">
    <td align="left" class="first-cell" >
            <img src="images/menu_minus.gif" id="icon_2_297" width="9" height="9" border="0" style="margin-left:2em" onclick="rowClicked(this)" />
            <span>
      <!-- 0-默认列表 1-搜索匹配列表 其他-搜索未匹配列表 -->
            <a href="goods.php?act=list&cat_id=297&supp=-1">果味冲调</a>
            </span>
        </td>
    <td width="10%">0</td>
    <td width="10%"><span onclick="listTable.edit(this, 'edit_measure_unit', 297)"><!--  -->&nbsp;&nbsp;&nbsp;&nbsp;<!--  --></span></td>
    <td width="10%"><img src="images/no.gif" onclick="listTable.toggle(this, 'toggle_show_in_nav', 297)" /></td>
    <td width="10%"><img src="images/yes.gif" onclick="listTable.toggle(this, 'toggle_is_show', 297)" /></td>
    <td><span onclick="listTable.edit(this, 'edit_grade', 297)">0</span></td>
    <td width="10%" align="right"><span onclick="listTable.edit(this, 'edit_sort_order', 297)">50</span></td>
    <td width="24%" align="center">
      <a href="category.php?act=move&cat_id=297">转移商品</a> |
      <a href="category.php?act=edit&amp;cat_id=297">编辑</a> |
      <a href="javascript:;" onclick="listTable.remove(297, '您确认要删除这条记录吗?')" title="移除">移除</a>
	  <!--代码修改_start Byjdy-->
	  	  <!--代码修改_end Byjdy-->
    </td>
  </tr>
    <tr align="center" class="2" id="2_288">
    <td align="left" class="first-cell" >
            <img src="images/menu_minus.gif" id="icon_2_288" width="9" height="9" border="0" style="margin-left:2em" onclick="rowClicked(this)" />
            <span>
      <!-- 0-默认列表 1-搜索匹配列表 其他-搜索未匹配列表 -->
            <a href="goods.php?act=list&cat_id=288&supp=-1">咖啡</a>
            </span>
        </td>
    <td width="10%">0</td>
    <td width="10%"><span onclick="listTable.edit(this, 'edit_measure_unit', 288)"><!--  -->&nbsp;&nbsp;&nbsp;&nbsp;<!--  --></span></td>
    <td width="10%"><img src="images/no.gif" onclick="listTable.toggle(this, 'toggle_show_in_nav', 288)" /></td>
    <td width="10%"><img src="images/yes.gif" onclick="listTable.toggle(this, 'toggle_is_show', 288)" /></td>
    <td><span onclick="listTable.edit(this, 'edit_grade', 288)">0</span></td>
    <td width="10%" align="right"><span onclick="listTable.edit(this, 'edit_sort_order', 288)">50</span></td>
    <td width="24%" align="center">
      <a href="category.php?act=move&cat_id=288">转移商品</a> |
      <a href="category.php?act=edit&amp;cat_id=288">编辑</a> |
      <a href="javascript:;" onclick="listTable.remove(288, '您确认要删除这条记录吗?')" title="移除">移除</a>
	  <!--代码修改_start Byjdy-->
	  	  <!--代码修改_end Byjdy-->
    </td>
  </tr>
    <tr align="center" class="2" id="2_285">
    <td align="left" class="first-cell" >
            <img src="images/menu_minus.gif" id="icon_2_285" width="9" height="9" border="0" style="margin-left:2em" onclick="rowClicked(this)" />
            <span>
      <!-- 0-默认列表 1-搜索匹配列表 其他-搜索未匹配列表 -->
            <a href="goods.php?act=list&cat_id=285&supp=-1">茶叶</a>
            </span>
        </td>
    <td width="10%">0</td>
    <td width="10%"><span onclick="listTable.edit(this, 'edit_measure_unit', 285)"><!--  -->&nbsp;&nbsp;&nbsp;&nbsp;<!--  --></span></td>
    <td width="10%"><img src="images/no.gif" onclick="listTable.toggle(this, 'toggle_show_in_nav', 285)" /></td>
    <td width="10%"><img src="images/yes.gif" onclick="listTable.toggle(this, 'toggle_is_show', 285)" /></td>
    <td><span onclick="listTable.edit(this, 'edit_grade', 285)">0</span></td>
    <td width="10%" align="right"><span onclick="listTable.edit(this, 'edit_sort_order', 285)">50</span></td>
    <td width="24%" align="center">
      <a href="category.php?act=move&cat_id=285">转移商品</a> |
      <a href="category.php?act=edit&amp;cat_id=285">编辑</a> |
      <a href="javascript:;" onclick="listTable.remove(285, '您确认要删除这条记录吗?')" title="移除">移除</a>
	  <!--代码修改_start Byjdy-->
	  	  <!--代码修改_end Byjdy-->
    </td>
  </tr>
    <tr align="center" class="2" id="2_282">
    <td align="left" class="first-cell" >
            <img src="images/menu_minus.gif" id="icon_2_282" width="9" height="9" border="0" style="margin-left:2em" onclick="rowClicked(this)" />
            <span>
      <!-- 0-默认列表 1-搜索匹配列表 其他-搜索未匹配列表 -->
            <a href="goods.php?act=list&cat_id=282&supp=-1">蜂蜜</a>
            </span>
        </td>
    <td width="10%">1</td>
    <td width="10%"><span onclick="listTable.edit(this, 'edit_measure_unit', 282)"><!--  -->&nbsp;&nbsp;&nbsp;&nbsp;<!--  --></span></td>
    <td width="10%"><img src="images/no.gif" onclick="listTable.toggle(this, 'toggle_show_in_nav', 282)" /></td>
    <td width="10%"><img src="images/yes.gif" onclick="listTable.toggle(this, 'toggle_is_show', 282)" /></td>
    <td><span onclick="listTable.edit(this, 'edit_grade', 282)">0</span></td>
    <td width="10%" align="right"><span onclick="listTable.edit(this, 'edit_sort_order', 282)">50</span></td>
    <td width="24%" align="center">
      <a href="category.php?act=move&cat_id=282">转移商品</a> |
      <a href="category.php?act=edit&amp;cat_id=282">编辑</a> |
      <a href="javascript:;" onclick="listTable.remove(282, '您确认要删除这条记录吗?')" title="移除">移除</a>
	  <!--代码修改_start Byjdy-->
	  	  <!--代码修改_end Byjdy-->
    </td>
  </tr>
    <tr align="center" class="0" id="0_359">
    <td align="left" class="first-cell" >
            <img src="images/menu_minus.gif" id="icon_0_359" width="9" height="9" border="0" style="margin-left:0em" onclick="rowClicked(this)" />
            <span>
      <!-- 0-默认列表 1-搜索匹配列表 其他-搜索未匹配列表 -->
            <a href="goods.php?act=list&cat_id=359&supp=-1">鞋靴、箱包、钟表、奢侈品</a>
            </span>
        </td>
    <td width="10%">0</td>
    <td width="10%"><span onclick="listTable.edit(this, 'edit_measure_unit', 359)"><!--  -->&nbsp;&nbsp;&nbsp;&nbsp;<!--  --></span></td>
    <td width="10%"><img src="images/no.gif" onclick="listTable.toggle(this, 'toggle_show_in_nav', 359)" /></td>
    <td width="10%"><img src="images/yes.gif" onclick="listTable.toggle(this, 'toggle_is_show', 359)" /></td>
    <td><span onclick="listTable.edit(this, 'edit_grade', 359)">0</span></td>
    <td width="10%" align="right"><span onclick="listTable.edit(this, 'edit_sort_order', 359)">7</span></td>
    <td width="24%" align="center">
      <a href="category.php?act=move&cat_id=359">转移商品</a> |
      <a href="category.php?act=edit&amp;cat_id=359">编辑</a> |
      <a href="javascript:;" onclick="listTable.remove(359, '您确认要删除这条记录吗?')" title="移除">移除</a>
	  <!--代码修改_start Byjdy-->
	  	  | <a href="category_flashimg.php?act=list&amp;cat_id=359">设置轮播图片</a>	  
	  	  <!--代码修改_end Byjdy-->
    </td>
  </tr>
    <tr align="center" class="0" id="0_360">
    <td align="left" class="first-cell" >
            <img src="images/menu_minus.gif" id="icon_0_360" width="9" height="9" border="0" style="margin-left:0em" onclick="rowClicked(this)" />
            <span>
      <!-- 0-默认列表 1-搜索匹配列表 其他-搜索未匹配列表 -->
            <a href="goods.php?act=list&cat_id=360&supp=-1">运动户外</a>
            </span>
        </td>
    <td width="10%">0</td>
    <td width="10%"><span onclick="listTable.edit(this, 'edit_measure_unit', 360)"><!--  -->&nbsp;&nbsp;&nbsp;&nbsp;<!--  --></span></td>
    <td width="10%"><img src="images/no.gif" onclick="listTable.toggle(this, 'toggle_show_in_nav', 360)" /></td>
    <td width="10%"><img src="images/yes.gif" onclick="listTable.toggle(this, 'toggle_is_show', 360)" /></td>
    <td><span onclick="listTable.edit(this, 'edit_grade', 360)">0</span></td>
    <td width="10%" align="right"><span onclick="listTable.edit(this, 'edit_sort_order', 360)">8</span></td>
    <td width="24%" align="center">
      <a href="category.php?act=move&cat_id=360">转移商品</a> |
      <a href="category.php?act=edit&amp;cat_id=360">编辑</a> |
      <a href="javascript:;" onclick="listTable.remove(360, '您确认要删除这条记录吗?')" title="移除">移除</a>
	  <!--代码修改_start Byjdy-->
	  	  | <a href="category_flashimg.php?act=list&amp;cat_id=360">设置轮播图片</a>	  
	  	  <!--代码修改_end Byjdy-->
    </td>
  </tr>
    <tr align="center" class="0" id="0_361">
    <td align="left" class="first-cell" >
            <img src="images/menu_minus.gif" id="icon_0_361" width="9" height="9" border="0" style="margin-left:0em" onclick="rowClicked(this)" />
            <span>
      <!-- 0-默认列表 1-搜索匹配列表 其他-搜索未匹配列表 -->
            <a href="goods.php?act=list&cat_id=361&supp=-1">汽车、汽车用品</a>
            </span>
        </td>
    <td width="10%">0</td>
    <td width="10%"><span onclick="listTable.edit(this, 'edit_measure_unit', 361)"><!--  -->&nbsp;&nbsp;&nbsp;&nbsp;<!--  --></span></td>
    <td width="10%"><img src="images/no.gif" onclick="listTable.toggle(this, 'toggle_show_in_nav', 361)" /></td>
    <td width="10%"><img src="images/yes.gif" onclick="listTable.toggle(this, 'toggle_is_show', 361)" /></td>
    <td><span onclick="listTable.edit(this, 'edit_grade', 361)">0</span></td>
    <td width="10%" align="right"><span onclick="listTable.edit(this, 'edit_sort_order', 361)">9</span></td>
    <td width="24%" align="center">
      <a href="category.php?act=move&cat_id=361">转移商品</a> |
      <a href="category.php?act=edit&amp;cat_id=361">编辑</a> |
      <a href="javascript:;" onclick="listTable.remove(361, '您确认要删除这条记录吗?')" title="移除">移除</a>
	  <!--代码修改_start Byjdy-->
	  	  | <a href="category_flashimg.php?act=list&amp;cat_id=361">设置轮播图片</a>	  
	  	  <!--代码修改_end Byjdy-->
    </td>
  </tr>
    <tr align="center" class="0" id="0_8">
    <td align="left" class="first-cell" >
            <img src="images/menu_minus.gif" id="icon_0_8" width="9" height="9" border="0" style="margin-left:0em" onclick="rowClicked(this)" />
            <span>
      <!-- 0-默认列表 1-搜索匹配列表 其他-搜索未匹配列表 -->
            <a href="goods.php?act=list&cat_id=8&supp=-1">母婴、玩具乐器</a>
            </span>
        </td>
    <td width="10%">8</td>
    <td width="10%"><span onclick="listTable.edit(this, 'edit_measure_unit', 8)"><!--  -->&nbsp;&nbsp;&nbsp;&nbsp;<!--  --></span></td>
    <td width="10%"><img src="images/no.gif" onclick="listTable.toggle(this, 'toggle_show_in_nav', 8)" /></td>
    <td width="10%"><img src="images/yes.gif" onclick="listTable.toggle(this, 'toggle_is_show', 8)" /></td>
    <td><span onclick="listTable.edit(this, 'edit_grade', 8)">5</span></td>
    <td width="10%" align="right"><span onclick="listTable.edit(this, 'edit_sort_order', 8)">10</span></td>
    <td width="24%" align="center">
      <a href="category.php?act=move&cat_id=8">转移商品</a> |
      <a href="category.php?act=edit&amp;cat_id=8">编辑</a> |
      <a href="javascript:;" onclick="listTable.remove(8, '您确认要删除这条记录吗?')" title="移除">移除</a>
	  <!--代码修改_start Byjdy-->
	  	  | <a href="category_flashimg.php?act=list&amp;cat_id=8">设置轮播图片</a>	  
	  	  <!--代码修改_end Byjdy-->
    </td>
  </tr>
    <tr align="center" class="1" id="1_225">
    <td align="left" class="first-cell" >
            <img src="images/menu_minus.gif" id="icon_1_225" width="9" height="9" border="0" style="margin-left:1em" onclick="rowClicked(this)" />
            <span>
      <!-- 0-默认列表 1-搜索匹配列表 其他-搜索未匹配列表 -->
            <a href="goods.php?act=list&cat_id=225&supp=-1">车床/床品</a>
            </span>
        </td>
    <td width="10%">0</td>
    <td width="10%"><span onclick="listTable.edit(this, 'edit_measure_unit', 225)"><!--  -->&nbsp;&nbsp;&nbsp;&nbsp;<!--  --></span></td>
    <td width="10%"><img src="images/no.gif" onclick="listTable.toggle(this, 'toggle_show_in_nav', 225)" /></td>
    <td width="10%"><img src="images/yes.gif" onclick="listTable.toggle(this, 'toggle_is_show', 225)" /></td>
    <td><span onclick="listTable.edit(this, 'edit_grade', 225)">0</span></td>
    <td width="10%" align="right"><span onclick="listTable.edit(this, 'edit_sort_order', 225)">50</span></td>
    <td width="24%" align="center">
      <a href="category.php?act=move&cat_id=225">转移商品</a> |
      <a href="category.php?act=edit&amp;cat_id=225">编辑</a> |
      <a href="javascript:;" onclick="listTable.remove(225, '您确认要删除这条记录吗?')" title="移除">移除</a>
	  <!--代码修改_start Byjdy-->
	  	  <!--代码修改_end Byjdy-->
    </td>
  </tr>
    <tr align="center" class="2" id="2_257">
    <td align="left" class="first-cell" >
            <img src="images/menu_minus.gif" id="icon_2_257" width="9" height="9" border="0" style="margin-left:2em" onclick="rowClicked(this)" />
            <span>
      <!-- 0-默认列表 1-搜索匹配列表 其他-搜索未匹配列表 -->
            <a href="goods.php?act=list&cat_id=257&supp=-1">婴儿床</a>
            </span>
        </td>
    <td width="10%">0</td>
    <td width="10%"><span onclick="listTable.edit(this, 'edit_measure_unit', 257)"><!--  -->&nbsp;&nbsp;&nbsp;&nbsp;<!--  --></span></td>
    <td width="10%"><img src="images/no.gif" onclick="listTable.toggle(this, 'toggle_show_in_nav', 257)" /></td>
    <td width="10%"><img src="images/yes.gif" onclick="listTable.toggle(this, 'toggle_is_show', 257)" /></td>
    <td><span onclick="listTable.edit(this, 'edit_grade', 257)">0</span></td>
    <td width="10%" align="right"><span onclick="listTable.edit(this, 'edit_sort_order', 257)">50</span></td>
    <td width="24%" align="center">
      <a href="category.php?act=move&cat_id=257">转移商品</a> |
      <a href="category.php?act=edit&amp;cat_id=257">编辑</a> |
      <a href="javascript:;" onclick="listTable.remove(257, '您确认要删除这条记录吗?')" title="移除">移除</a>
	  <!--代码修改_start Byjdy-->
	  	  <!--代码修改_end Byjdy-->
    </td>
  </tr>
    <tr align="center" class="2" id="2_261">
    <td align="left" class="first-cell" >
            <img src="images/menu_minus.gif" id="icon_2_261" width="9" height="9" border="0" style="margin-left:2em" onclick="rowClicked(this)" />
            <span>
      <!-- 0-默认列表 1-搜索匹配列表 其他-搜索未匹配列表 -->
            <a href="goods.php?act=list&cat_id=261&supp=-1">睡袋/抱被</a>
            </span>
        </td>
    <td width="10%">0</td>
    <td width="10%"><span onclick="listTable.edit(this, 'edit_measure_unit', 261)"><!--  -->&nbsp;&nbsp;&nbsp;&nbsp;<!--  --></span></td>
    <td width="10%"><img src="images/no.gif" onclick="listTable.toggle(this, 'toggle_show_in_nav', 261)" /></td>
    <td width="10%"><img src="images/yes.gif" onclick="listTable.toggle(this, 'toggle_is_show', 261)" /></td>
    <td><span onclick="listTable.edit(this, 'edit_grade', 261)">0</span></td>
    <td width="10%" align="right"><span onclick="listTable.edit(this, 'edit_sort_order', 261)">50</span></td>
    <td width="24%" align="center">
      <a href="category.php?act=move&cat_id=261">转移商品</a> |
      <a href="category.php?act=edit&amp;cat_id=261">编辑</a> |
      <a href="javascript:;" onclick="listTable.remove(261, '您确认要删除这条记录吗?')" title="移除">移除</a>
	  <!--代码修改_start Byjdy-->
	  	  <!--代码修改_end Byjdy-->
    </td>
  </tr>
    <tr align="center" class="2" id="2_258">
    <td align="left" class="first-cell" >
            <img src="images/menu_minus.gif" id="icon_2_258" width="9" height="9" border="0" style="margin-left:2em" onclick="rowClicked(this)" />
            <span>
      <!-- 0-默认列表 1-搜索匹配列表 其他-搜索未匹配列表 -->
            <a href="goods.php?act=list&cat_id=258&supp=-1">餐椅</a>
            </span>
        </td>
    <td width="10%">0</td>
    <td width="10%"><span onclick="listTable.edit(this, 'edit_measure_unit', 258)"><!--  -->&nbsp;&nbsp;&nbsp;&nbsp;<!--  --></span></td>
    <td width="10%"><img src="images/no.gif" onclick="listTable.toggle(this, 'toggle_show_in_nav', 258)" /></td>
    <td width="10%"><img src="images/yes.gif" onclick="listTable.toggle(this, 'toggle_is_show', 258)" /></td>
    <td><span onclick="listTable.edit(this, 'edit_grade', 258)">0</span></td>
    <td width="10%" align="right"><span onclick="listTable.edit(this, 'edit_sort_order', 258)">50</span></td>
    <td width="24%" align="center">
      <a href="category.php?act=move&cat_id=258">转移商品</a> |
      <a href="category.php?act=edit&amp;cat_id=258">编辑</a> |
      <a href="javascript:;" onclick="listTable.remove(258, '您确认要删除这条记录吗?')" title="移除">移除</a>
	  <!--代码修改_start Byjdy-->
	  	  <!--代码修改_end Byjdy-->
    </td>
  </tr>
    <tr align="center" class="2" id="2_255">
    <td align="left" class="first-cell" >
            <img src="images/menu_minus.gif" id="icon_2_255" width="9" height="9" border="0" style="margin-left:2em" onclick="rowClicked(this)" />
            <span>
      <!-- 0-默认列表 1-搜索匹配列表 其他-搜索未匹配列表 -->
            <a href="goods.php?act=list&cat_id=255&supp=-1">安全座椅</a>
            </span>
        </td>
    <td width="10%">0</td>
    <td width="10%"><span onclick="listTable.edit(this, 'edit_measure_unit', 255)"><!--  -->&nbsp;&nbsp;&nbsp;&nbsp;<!--  --></span></td>
    <td width="10%"><img src="images/no.gif" onclick="listTable.toggle(this, 'toggle_show_in_nav', 255)" /></td>
    <td width="10%"><img src="images/yes.gif" onclick="listTable.toggle(this, 'toggle_is_show', 255)" /></td>
    <td><span onclick="listTable.edit(this, 'edit_grade', 255)">0</span></td>
    <td width="10%" align="right"><span onclick="listTable.edit(this, 'edit_sort_order', 255)">50</span></td>
    <td width="24%" align="center">
      <a href="category.php?act=move&cat_id=255">转移商品</a> |
      <a href="category.php?act=edit&amp;cat_id=255">编辑</a> |
      <a href="javascript:;" onclick="listTable.remove(255, '您确认要删除这条记录吗?')" title="移除">移除</a>
	  <!--代码修改_start Byjdy-->
	  	  <!--代码修改_end Byjdy-->
    </td>
  </tr>
    <tr align="center" class="2" id="2_262">
    <td align="left" class="first-cell" >
            <img src="images/menu_minus.gif" id="icon_2_262" width="9" height="9" border="0" style="margin-left:2em" onclick="rowClicked(this)" />
            <span>
      <!-- 0-默认列表 1-搜索匹配列表 其他-搜索未匹配列表 -->
            <a href="goods.php?act=list&cat_id=262&supp=-1">凉席/蚊帐</a>
            </span>
        </td>
    <td width="10%">0</td>
    <td width="10%"><span onclick="listTable.edit(this, 'edit_measure_unit', 262)"><!--  -->&nbsp;&nbsp;&nbsp;&nbsp;<!--  --></span></td>
    <td width="10%"><img src="images/no.gif" onclick="listTable.toggle(this, 'toggle_show_in_nav', 262)" /></td>
    <td width="10%"><img src="images/yes.gif" onclick="listTable.toggle(this, 'toggle_is_show', 262)" /></td>
    <td><span onclick="listTable.edit(this, 'edit_grade', 262)">0</span></td>
    <td width="10%" align="right"><span onclick="listTable.edit(this, 'edit_sort_order', 262)">50</span></td>
    <td width="24%" align="center">
      <a href="category.php?act=move&cat_id=262">转移商品</a> |
      <a href="category.php?act=edit&amp;cat_id=262">编辑</a> |
      <a href="javascript:;" onclick="listTable.remove(262, '您确认要删除这条记录吗?')" title="移除">移除</a>
	  <!--代码修改_start Byjdy-->
	  	  <!--代码修改_end Byjdy-->
    </td>
  </tr>
    <tr align="center" class="2" id="2_259">
    <td align="left" class="first-cell" >
            <img src="images/menu_minus.gif" id="icon_2_259" width="9" height="9" border="0" style="margin-left:2em" onclick="rowClicked(this)" />
            <span>
      <!-- 0-默认列表 1-搜索匹配列表 其他-搜索未匹配列表 -->
            <a href="goods.php?act=list&cat_id=259&supp=-1">三轮车</a>
            </span>
        </td>
    <td width="10%">0</td>
    <td width="10%"><span onclick="listTable.edit(this, 'edit_measure_unit', 259)"><!--  -->&nbsp;&nbsp;&nbsp;&nbsp;<!--  --></span></td>
    <td width="10%"><img src="images/no.gif" onclick="listTable.toggle(this, 'toggle_show_in_nav', 259)" /></td>
    <td width="10%"><img src="images/yes.gif" onclick="listTable.toggle(this, 'toggle_is_show', 259)" /></td>
    <td><span onclick="listTable.edit(this, 'edit_grade', 259)">0</span></td>
    <td width="10%" align="right"><span onclick="listTable.edit(this, 'edit_sort_order', 259)">50</span></td>
    <td width="24%" align="center">
      <a href="category.php?act=move&cat_id=259">转移商品</a> |
      <a href="category.php?act=edit&amp;cat_id=259">编辑</a> |
      <a href="javascript:;" onclick="listTable.remove(259, '您确认要删除这条记录吗?')" title="移除">移除</a>
	  <!--代码修改_start Byjdy-->
	  	  <!--代码修改_end Byjdy-->
    </td>
  </tr>
    <tr align="center" class="2" id="2_256">
    <td align="left" class="first-cell" >
            <img src="images/menu_minus.gif" id="icon_2_256" width="9" height="9" border="0" style="margin-left:2em" onclick="rowClicked(this)" />
            <span>
      <!-- 0-默认列表 1-搜索匹配列表 其他-搜索未匹配列表 -->
            <a href="goods.php?act=list&cat_id=256&supp=-1">手推车</a>
            </span>
        </td>
    <td width="10%">0</td>
    <td width="10%"><span onclick="listTable.edit(this, 'edit_measure_unit', 256)"><!--  -->&nbsp;&nbsp;&nbsp;&nbsp;<!--  --></span></td>
    <td width="10%"><img src="images/no.gif" onclick="listTable.toggle(this, 'toggle_show_in_nav', 256)" /></td>
    <td width="10%"><img src="images/yes.gif" onclick="listTable.toggle(this, 'toggle_is_show', 256)" /></td>
    <td><span onclick="listTable.edit(this, 'edit_grade', 256)">0</span></td>
    <td width="10%" align="right"><span onclick="listTable.edit(this, 'edit_sort_order', 256)">50</span></td>
    <td width="24%" align="center">
      <a href="category.php?act=move&cat_id=256">转移商品</a> |
      <a href="category.php?act=edit&amp;cat_id=256">编辑</a> |
      <a href="javascript:;" onclick="listTable.remove(256, '您确认要删除这条记录吗?')" title="移除">移除</a>
	  <!--代码修改_start Byjdy-->
	  	  <!--代码修改_end Byjdy-->
    </td>
  </tr>
    <tr align="center" class="2" id="2_260">
    <td align="left" class="first-cell" >
            <img src="images/menu_minus.gif" id="icon_2_260" width="9" height="9" border="0" style="margin-left:2em" onclick="rowClicked(this)" />
            <span>
      <!-- 0-默认列表 1-搜索匹配列表 其他-搜索未匹配列表 -->
            <a href="goods.php?act=list&cat_id=260&supp=-1">儿童家具</a>
            </span>
        </td>
    <td width="10%">0</td>
    <td width="10%"><span onclick="listTable.edit(this, 'edit_measure_unit', 260)"><!--  -->&nbsp;&nbsp;&nbsp;&nbsp;<!--  --></span></td>
    <td width="10%"><img src="images/no.gif" onclick="listTable.toggle(this, 'toggle_show_in_nav', 260)" /></td>
    <td width="10%"><img src="images/yes.gif" onclick="listTable.toggle(this, 'toggle_is_show', 260)" /></td>
    <td><span onclick="listTable.edit(this, 'edit_grade', 260)">0</span></td>
    <td width="10%" align="right"><span onclick="listTable.edit(this, 'edit_sort_order', 260)">50</span></td>
    <td width="24%" align="center">
      <a href="category.php?act=move&cat_id=260">转移商品</a> |
      <a href="category.php?act=edit&amp;cat_id=260">编辑</a> |
      <a href="javascript:;" onclick="listTable.remove(260, '您确认要删除这条记录吗?')" title="移除">移除</a>
	  <!--代码修改_start Byjdy-->
	  	  <!--代码修改_end Byjdy-->
    </td>
  </tr>
    <tr align="center" class="1" id="1_222">
    <td align="left" class="first-cell" >
            <img src="images/menu_minus.gif" id="icon_1_222" width="9" height="9" border="0" style="margin-left:1em" onclick="rowClicked(this)" />
            <span>
      <!-- 0-默认列表 1-搜索匹配列表 其他-搜索未匹配列表 -->
            <a href="goods.php?act=list&cat_id=222&supp=-1">营养/辅食</a>
            </span>
        </td>
    <td width="10%">8</td>
    <td width="10%"><span onclick="listTable.edit(this, 'edit_measure_unit', 222)"><!--  -->&nbsp;&nbsp;&nbsp;&nbsp;<!--  --></span></td>
    <td width="10%"><img src="images/no.gif" onclick="listTable.toggle(this, 'toggle_show_in_nav', 222)" /></td>
    <td width="10%"><img src="images/yes.gif" onclick="listTable.toggle(this, 'toggle_is_show', 222)" /></td>
    <td><span onclick="listTable.edit(this, 'edit_grade', 222)">0</span></td>
    <td width="10%" align="right"><span onclick="listTable.edit(this, 'edit_sort_order', 222)">50</span></td>
    <td width="24%" align="center">
      <a href="category.php?act=move&cat_id=222">转移商品</a> |
      <a href="category.php?act=edit&amp;cat_id=222">编辑</a> |
      <a href="javascript:;" onclick="listTable.remove(222, '您确认要删除这条记录吗?')" title="移除">移除</a>
	  <!--代码修改_start Byjdy-->
	  	  <!--代码修改_end Byjdy-->
    </td>
  </tr>
    <tr align="center" class="2" id="2_238">
    <td align="left" class="first-cell" >
            <img src="images/menu_minus.gif" id="icon_2_238" width="9" height="9" border="0" style="margin-left:2em" onclick="rowClicked(this)" />
            <span>
      <!-- 0-默认列表 1-搜索匹配列表 其他-搜索未匹配列表 -->
            <a href="goods.php?act=list&cat_id=238&supp=-1">清火开胃</a>
            </span>
        </td>
    <td width="10%">0</td>
    <td width="10%"><span onclick="listTable.edit(this, 'edit_measure_unit', 238)"><!--  -->&nbsp;&nbsp;&nbsp;&nbsp;<!--  --></span></td>
    <td width="10%"><img src="images/no.gif" onclick="listTable.toggle(this, 'toggle_show_in_nav', 238)" /></td>
    <td width="10%"><img src="images/yes.gif" onclick="listTable.toggle(this, 'toggle_is_show', 238)" /></td>
    <td><span onclick="listTable.edit(this, 'edit_grade', 238)">0</span></td>
    <td width="10%" align="right"><span onclick="listTable.edit(this, 'edit_sort_order', 238)">50</span></td>
    <td width="24%" align="center">
      <a href="category.php?act=move&cat_id=238">转移商品</a> |
      <a href="category.php?act=edit&amp;cat_id=238">编辑</a> |
      <a href="javascript:;" onclick="listTable.remove(238, '您确认要删除这条记录吗?')" title="移除">移除</a>
	  <!--代码修改_start Byjdy-->
	  	  <!--代码修改_end Byjdy-->
    </td>
  </tr>
    <tr align="center" class="2" id="2_235">
    <td align="left" class="first-cell" >
            <img src="images/menu_minus.gif" id="icon_2_235" width="9" height="9" border="0" style="margin-left:2em" onclick="rowClicked(this)" />
            <span>
      <!-- 0-默认列表 1-搜索匹配列表 其他-搜索未匹配列表 -->
            <a href="goods.php?act=list&cat_id=235&supp=-1">面食类</a>
            </span>
        </td>
    <td width="10%">0</td>
    <td width="10%"><span onclick="listTable.edit(this, 'edit_measure_unit', 235)"><!--  -->&nbsp;&nbsp;&nbsp;&nbsp;<!--  --></span></td>
    <td width="10%"><img src="images/no.gif" onclick="listTable.toggle(this, 'toggle_show_in_nav', 235)" /></td>
    <td width="10%"><img src="images/yes.gif" onclick="listTable.toggle(this, 'toggle_is_show', 235)" /></td>
    <td><span onclick="listTable.edit(this, 'edit_grade', 235)">0</span></td>
    <td width="10%" align="right"><span onclick="listTable.edit(this, 'edit_sort_order', 235)">50</span></td>
    <td width="24%" align="center">
      <a href="category.php?act=move&cat_id=235">转移商品</a> |
      <a href="category.php?act=edit&amp;cat_id=235">编辑</a> |
      <a href="javascript:;" onclick="listTable.remove(235, '您确认要删除这条记录吗?')" title="移除">移除</a>
	  <!--代码修改_start Byjdy-->
	  	  <!--代码修改_end Byjdy-->
    </td>
  </tr>
    <tr align="center" class="2" id="2_239">
    <td align="left" class="first-cell" >
            <img src="images/menu_minus.gif" id="icon_2_239" width="9" height="9" border="0" style="margin-left:2em" onclick="rowClicked(this)" />
            <span>
      <!-- 0-默认列表 1-搜索匹配列表 其他-搜索未匹配列表 -->
            <a href="goods.php?act=list&cat_id=239&supp=-1">钙铁锌</a>
            </span>
        </td>
    <td width="10%">0</td>
    <td width="10%"><span onclick="listTable.edit(this, 'edit_measure_unit', 239)"><!--  -->&nbsp;&nbsp;&nbsp;&nbsp;<!--  --></span></td>
    <td width="10%"><img src="images/no.gif" onclick="listTable.toggle(this, 'toggle_show_in_nav', 239)" /></td>
    <td width="10%"><img src="images/yes.gif" onclick="listTable.toggle(this, 'toggle_is_show', 239)" /></td>
    <td><span onclick="listTable.edit(this, 'edit_grade', 239)">0</span></td>
    <td width="10%" align="right"><span onclick="listTable.edit(this, 'edit_sort_order', 239)">50</span></td>
    <td width="24%" align="center">
      <a href="category.php?act=move&cat_id=239">转移商品</a> |
      <a href="category.php?act=edit&amp;cat_id=239">编辑</a> |
      <a href="javascript:;" onclick="listTable.remove(239, '您确认要删除这条记录吗?')" title="移除">移除</a>
	  <!--代码修改_start Byjdy-->
	  	  <!--代码修改_end Byjdy-->
    </td>
  </tr>
    <tr align="center" class="2" id="2_236">
    <td align="left" class="first-cell" >
            <img src="images/menu_minus.gif" id="icon_2_236" width="9" height="9" border="0" style="margin-left:2em" onclick="rowClicked(this)" />
            <span>
      <!-- 0-默认列表 1-搜索匹配列表 其他-搜索未匹配列表 -->
            <a href="goods.php?act=list&cat_id=236&supp=-1">宝宝零食</a>
            </span>
        </td>
    <td width="10%">0</td>
    <td width="10%"><span onclick="listTable.edit(this, 'edit_measure_unit', 236)"><!--  -->&nbsp;&nbsp;&nbsp;&nbsp;<!--  --></span></td>
    <td width="10%"><img src="images/no.gif" onclick="listTable.toggle(this, 'toggle_show_in_nav', 236)" /></td>
    <td width="10%"><img src="images/yes.gif" onclick="listTable.toggle(this, 'toggle_is_show', 236)" /></td>
    <td><span onclick="listTable.edit(this, 'edit_grade', 236)">0</span></td>
    <td width="10%" align="right"><span onclick="listTable.edit(this, 'edit_sort_order', 236)">50</span></td>
    <td width="24%" align="center">
      <a href="category.php?act=move&cat_id=236">转移商品</a> |
      <a href="category.php?act=edit&amp;cat_id=236">编辑</a> |
      <a href="javascript:;" onclick="listTable.remove(236, '您确认要删除这条记录吗?')" title="移除">移除</a>
	  <!--代码修改_start Byjdy-->
	  	  <!--代码修改_end Byjdy-->
    </td>
  </tr>
    <tr align="center" class="2" id="2_233">
    <td align="left" class="first-cell" >
            <img src="images/menu_minus.gif" id="icon_2_233" width="9" height="9" border="0" style="margin-left:2em" onclick="rowClicked(this)" />
            <span>
      <!-- 0-默认列表 1-搜索匹配列表 其他-搜索未匹配列表 -->
            <a href="goods.php?act=list&cat_id=233&supp=-1">米粉</a>
            </span>
        </td>
    <td width="10%">0</td>
    <td width="10%"><span onclick="listTable.edit(this, 'edit_measure_unit', 233)"><!--  -->&nbsp;&nbsp;&nbsp;&nbsp;<!--  --></span></td>
    <td width="10%"><img src="images/no.gif" onclick="listTable.toggle(this, 'toggle_show_in_nav', 233)" /></td>
    <td width="10%"><img src="images/yes.gif" onclick="listTable.toggle(this, 'toggle_is_show', 233)" /></td>
    <td><span onclick="listTable.edit(this, 'edit_grade', 233)">0</span></td>
    <td width="10%" align="right"><span onclick="listTable.edit(this, 'edit_sort_order', 233)">50</span></td>
    <td width="24%" align="center">
      <a href="category.php?act=move&cat_id=233">转移商品</a> |
      <a href="category.php?act=edit&amp;cat_id=233">编辑</a> |
      <a href="javascript:;" onclick="listTable.remove(233, '您确认要删除这条记录吗?')" title="移除">移除</a>
	  <!--代码修改_start Byjdy-->
	  	  <!--代码修改_end Byjdy-->
    </td>
  </tr>
    <tr align="center" class="2" id="2_240">
    <td align="left" class="first-cell" >
            <img src="images/menu_minus.gif" id="icon_2_240" width="9" height="9" border="0" style="margin-left:2em" onclick="rowClicked(this)" />
            <span>
      <!-- 0-默认列表 1-搜索匹配列表 其他-搜索未匹配列表 -->
            <a href="goods.php?act=list&cat_id=240&supp=-1">益生菌</a>
            </span>
        </td>
    <td width="10%">0</td>
    <td width="10%"><span onclick="listTable.edit(this, 'edit_measure_unit', 240)"><!--  -->&nbsp;&nbsp;&nbsp;&nbsp;<!--  --></span></td>
    <td width="10%"><img src="images/no.gif" onclick="listTable.toggle(this, 'toggle_show_in_nav', 240)" /></td>
    <td width="10%"><img src="images/yes.gif" onclick="listTable.toggle(this, 'toggle_is_show', 240)" /></td>
    <td><span onclick="listTable.edit(this, 'edit_grade', 240)">0</span></td>
    <td width="10%" align="right"><span onclick="listTable.edit(this, 'edit_sort_order', 240)">50</span></td>
    <td width="24%" align="center">
      <a href="category.php?act=move&cat_id=240">转移商品</a> |
      <a href="category.php?act=edit&amp;cat_id=240">编辑</a> |
      <a href="javascript:;" onclick="listTable.remove(240, '您确认要删除这条记录吗?')" title="移除">移除</a>
	  <!--代码修改_start Byjdy-->
	  	  <!--代码修改_end Byjdy-->
    </td>
  </tr>
    <tr align="center" class="2" id="2_237">
    <td align="left" class="first-cell" >
            <img src="images/menu_minus.gif" id="icon_2_237" width="9" height="9" border="0" style="margin-left:2em" onclick="rowClicked(this)" />
            <span>
      <!-- 0-默认列表 1-搜索匹配列表 其他-搜索未匹配列表 -->
            <a href="goods.php?act=list&cat_id=237&supp=-1">DHA</a>
            </span>
        </td>
    <td width="10%">0</td>
    <td width="10%"><span onclick="listTable.edit(this, 'edit_measure_unit', 237)"><!--  -->&nbsp;&nbsp;&nbsp;&nbsp;<!--  --></span></td>
    <td width="10%"><img src="images/no.gif" onclick="listTable.toggle(this, 'toggle_show_in_nav', 237)" /></td>
    <td width="10%"><img src="images/yes.gif" onclick="listTable.toggle(this, 'toggle_is_show', 237)" /></td>
    <td><span onclick="listTable.edit(this, 'edit_grade', 237)">0</span></td>
    <td width="10%" align="right"><span onclick="listTable.edit(this, 'edit_sort_order', 237)">50</span></td>
    <td width="24%" align="center">
      <a href="category.php?act=move&cat_id=237">转移商品</a> |
      <a href="category.php?act=edit&amp;cat_id=237">编辑</a> |
      <a href="javascript:;" onclick="listTable.remove(237, '您确认要删除这条记录吗?')" title="移除">移除</a>
	  <!--代码修改_start Byjdy-->
	  	  <!--代码修改_end Byjdy-->
    </td>
  </tr>
    <tr align="center" class="2" id="2_234">
    <td align="left" class="first-cell" >
            <img src="images/menu_minus.gif" id="icon_2_234" width="9" height="9" border="0" style="margin-left:2em" onclick="rowClicked(this)" />
            <span>
      <!-- 0-默认列表 1-搜索匹配列表 其他-搜索未匹配列表 -->
            <a href="goods.php?act=list&cat_id=234&supp=-1">果汁/泥</a>
            </span>
        </td>
    <td width="10%">0</td>
    <td width="10%"><span onclick="listTable.edit(this, 'edit_measure_unit', 234)"><!--  -->&nbsp;&nbsp;&nbsp;&nbsp;<!--  --></span></td>
    <td width="10%"><img src="images/no.gif" onclick="listTable.toggle(this, 'toggle_show_in_nav', 234)" /></td>
    <td width="10%"><img src="images/yes.gif" onclick="listTable.toggle(this, 'toggle_is_show', 234)" /></td>
    <td><span onclick="listTable.edit(this, 'edit_grade', 234)">0</span></td>
    <td width="10%" align="right"><span onclick="listTable.edit(this, 'edit_sort_order', 234)">50</span></td>
    <td width="24%" align="center">
      <a href="category.php?act=move&cat_id=234">转移商品</a> |
      <a href="category.php?act=edit&amp;cat_id=234">编辑</a> |
      <a href="javascript:;" onclick="listTable.remove(234, '您确认要删除这条记录吗?')" title="移除">移除</a>
	  <!--代码修改_start Byjdy-->
	  	  <!--代码修改_end Byjdy-->
    </td>
  </tr>
    <tr align="center" class="1" id="1_226">
    <td align="left" class="first-cell" >
            <img src="images/menu_minus.gif" id="icon_1_226" width="9" height="9" border="0" style="margin-left:1em" onclick="rowClicked(this)" />
            <span>
      <!-- 0-默认列表 1-搜索匹配列表 其他-搜索未匹配列表 -->
            <a href="goods.php?act=list&cat_id=226&supp=-1">孕妈专区</a>
            </span>
        </td>
    <td width="10%">0</td>
    <td width="10%"><span onclick="listTable.edit(this, 'edit_measure_unit', 226)"><!--  -->&nbsp;&nbsp;&nbsp;&nbsp;<!--  --></span></td>
    <td width="10%"><img src="images/no.gif" onclick="listTable.toggle(this, 'toggle_show_in_nav', 226)" /></td>
    <td width="10%"><img src="images/yes.gif" onclick="listTable.toggle(this, 'toggle_is_show', 226)" /></td>
    <td><span onclick="listTable.edit(this, 'edit_grade', 226)">0</span></td>
    <td width="10%" align="right"><span onclick="listTable.edit(this, 'edit_sort_order', 226)">50</span></td>
    <td width="24%" align="center">
      <a href="category.php?act=move&cat_id=226">转移商品</a> |
      <a href="category.php?act=edit&amp;cat_id=226">编辑</a> |
      <a href="javascript:;" onclick="listTable.remove(226, '您确认要删除这条记录吗?')" title="移除">移除</a>
	  <!--代码修改_start Byjdy-->
	  	  <!--代码修改_end Byjdy-->
    </td>
  </tr>
    <tr align="center" class="2" id="2_270">
    <td align="left" class="first-cell" >
            <img src="images/menu_minus.gif" id="icon_2_270" width="9" height="9" border="0" style="margin-left:2em" onclick="rowClicked(this)" />
            <span>
      <!-- 0-默认列表 1-搜索匹配列表 其他-搜索未匹配列表 -->
            <a href="goods.php?act=list&cat_id=270&supp=-1">孕妇内裤</a>
            </span>
        </td>
    <td width="10%">0</td>
    <td width="10%"><span onclick="listTable.edit(this, 'edit_measure_unit', 270)"><!--  -->&nbsp;&nbsp;&nbsp;&nbsp;<!--  --></span></td>
    <td width="10%"><img src="images/no.gif" onclick="listTable.toggle(this, 'toggle_show_in_nav', 270)" /></td>
    <td width="10%"><img src="images/yes.gif" onclick="listTable.toggle(this, 'toggle_is_show', 270)" /></td>
    <td><span onclick="listTable.edit(this, 'edit_grade', 270)">0</span></td>
    <td width="10%" align="right"><span onclick="listTable.edit(this, 'edit_sort_order', 270)">50</span></td>
    <td width="24%" align="center">
      <a href="category.php?act=move&cat_id=270">转移商品</a> |
      <a href="category.php?act=edit&amp;cat_id=270">编辑</a> |
      <a href="javascript:;" onclick="listTable.remove(270, '您确认要删除这条记录吗?')" title="移除">移除</a>
	  <!--代码修改_start Byjdy-->
	  	  <!--代码修改_end Byjdy-->
    </td>
  </tr>
    <tr align="center" class="2" id="2_267">
    <td align="left" class="first-cell" >
            <img src="images/menu_minus.gif" id="icon_2_267" width="9" height="9" border="0" style="margin-left:2em" onclick="rowClicked(this)" />
            <span>
      <!-- 0-默认列表 1-搜索匹配列表 其他-搜索未匹配列表 -->
            <a href="goods.php?act=list&cat_id=267&supp=-1">妈咪包</a>
            </span>
        </td>
    <td width="10%">0</td>
    <td width="10%"><span onclick="listTable.edit(this, 'edit_measure_unit', 267)"><!--  -->&nbsp;&nbsp;&nbsp;&nbsp;<!--  --></span></td>
    <td width="10%"><img src="images/no.gif" onclick="listTable.toggle(this, 'toggle_show_in_nav', 267)" /></td>
    <td width="10%"><img src="images/yes.gif" onclick="listTable.toggle(this, 'toggle_is_show', 267)" /></td>
    <td><span onclick="listTable.edit(this, 'edit_grade', 267)">0</span></td>
    <td width="10%" align="right"><span onclick="listTable.edit(this, 'edit_sort_order', 267)">50</span></td>
    <td width="24%" align="center">
      <a href="category.php?act=move&cat_id=267">转移商品</a> |
      <a href="category.php?act=edit&amp;cat_id=267">编辑</a> |
      <a href="javascript:;" onclick="listTable.remove(267, '您确认要删除这条记录吗?')" title="移除">移除</a>
	  <!--代码修改_start Byjdy-->
	  	  <!--代码修改_end Byjdy-->
    </td>
  </tr>
    <tr align="center" class="2" id="2_264">
    <td align="left" class="first-cell" >
            <img src="images/menu_minus.gif" id="icon_2_264" width="9" height="9" border="0" style="margin-left:2em" onclick="rowClicked(this)" />
            <span>
      <!-- 0-默认列表 1-搜索匹配列表 其他-搜索未匹配列表 -->
            <a href="goods.php?act=list&cat_id=264&supp=-1">打底裤</a>
            </span>
        </td>
    <td width="10%">0</td>
    <td width="10%"><span onclick="listTable.edit(this, 'edit_measure_unit', 264)"><!--  -->&nbsp;&nbsp;&nbsp;&nbsp;<!--  --></span></td>
    <td width="10%"><img src="images/no.gif" onclick="listTable.toggle(this, 'toggle_show_in_nav', 264)" /></td>
    <td width="10%"><img src="images/yes.gif" onclick="listTable.toggle(this, 'toggle_is_show', 264)" /></td>
    <td><span onclick="listTable.edit(this, 'edit_grade', 264)">0</span></td>
    <td width="10%" align="right"><span onclick="listTable.edit(this, 'edit_sort_order', 264)">50</span></td>
    <td width="24%" align="center">
      <a href="category.php?act=move&cat_id=264">转移商品</a> |
      <a href="category.php?act=edit&amp;cat_id=264">编辑</a> |
      <a href="javascript:;" onclick="listTable.remove(264, '您确认要删除这条记录吗?')" title="移除">移除</a>
	  <!--代码修改_start Byjdy-->
	  	  <!--代码修改_end Byjdy-->
    </td>
  </tr>
    <tr align="center" class="2" id="2_268">
    <td align="left" class="first-cell" >
            <img src="images/menu_minus.gif" id="icon_2_268" width="9" height="9" border="0" style="margin-left:2em" onclick="rowClicked(this)" />
            <span>
      <!-- 0-默认列表 1-搜索匹配列表 其他-搜索未匹配列表 -->
            <a href="goods.php?act=list&cat_id=268&supp=-1">收腹带</a>
            </span>
        </td>
    <td width="10%">0</td>
    <td width="10%"><span onclick="listTable.edit(this, 'edit_measure_unit', 268)"><!--  -->&nbsp;&nbsp;&nbsp;&nbsp;<!--  --></span></td>
    <td width="10%"><img src="images/no.gif" onclick="listTable.toggle(this, 'toggle_show_in_nav', 268)" /></td>
    <td width="10%"><img src="images/yes.gif" onclick="listTable.toggle(this, 'toggle_is_show', 268)" /></td>
    <td><span onclick="listTable.edit(this, 'edit_grade', 268)">0</span></td>
    <td width="10%" align="right"><span onclick="listTable.edit(this, 'edit_sort_order', 268)">50</span></td>
    <td width="24%" align="center">
      <a href="category.php?act=move&cat_id=268">转移商品</a> |
      <a href="category.php?act=edit&amp;cat_id=268">编辑</a> |
      <a href="javascript:;" onclick="listTable.remove(268, '您确认要删除这条记录吗?')" title="移除">移除</a>
	  <!--代码修改_start Byjdy-->
	  	  <!--代码修改_end Byjdy-->
    </td>
  </tr>
    <tr align="center" class="2" id="2_265">
    <td align="left" class="first-cell" >
            <img src="images/menu_minus.gif" id="icon_2_265" width="9" height="9" border="0" style="margin-left:2em" onclick="rowClicked(this)" />
            <span>
      <!-- 0-默认列表 1-搜索匹配列表 其他-搜索未匹配列表 -->
            <a href="goods.php?act=list&cat_id=265&supp=-1">防辐射服</a>
            </span>
        </td>
    <td width="10%">0</td>
    <td width="10%"><span onclick="listTable.edit(this, 'edit_measure_unit', 265)"><!--  -->&nbsp;&nbsp;&nbsp;&nbsp;<!--  --></span></td>
    <td width="10%"><img src="images/no.gif" onclick="listTable.toggle(this, 'toggle_show_in_nav', 265)" /></td>
    <td width="10%"><img src="images/yes.gif" onclick="listTable.toggle(this, 'toggle_is_show', 265)" /></td>
    <td><span onclick="listTable.edit(this, 'edit_grade', 265)">0</span></td>
    <td width="10%" align="right"><span onclick="listTable.edit(this, 'edit_sort_order', 265)">50</span></td>
    <td width="24%" align="center">
      <a href="category.php?act=move&cat_id=265">转移商品</a> |
      <a href="category.php?act=edit&amp;cat_id=265">编辑</a> |
      <a href="javascript:;" onclick="listTable.remove(265, '您确认要删除这条记录吗?')" title="移除">移除</a>
	  <!--代码修改_start Byjdy-->
	  	  <!--代码修改_end Byjdy-->
    </td>
  </tr>
    <tr align="center" class="2" id="2_269">
    <td align="left" class="first-cell" >
            <img src="images/menu_minus.gif" id="icon_2_269" width="9" height="9" border="0" style="margin-left:2em" onclick="rowClicked(this)" />
            <span>
      <!-- 0-默认列表 1-搜索匹配列表 其他-搜索未匹配列表 -->
            <a href="goods.php?act=list&cat_id=269&supp=-1">哺乳文胸</a>
            </span>
        </td>
    <td width="10%">0</td>
    <td width="10%"><span onclick="listTable.edit(this, 'edit_measure_unit', 269)"><!--  -->&nbsp;&nbsp;&nbsp;&nbsp;<!--  --></span></td>
    <td width="10%"><img src="images/no.gif" onclick="listTable.toggle(this, 'toggle_show_in_nav', 269)" /></td>
    <td width="10%"><img src="images/yes.gif" onclick="listTable.toggle(this, 'toggle_is_show', 269)" /></td>
    <td><span onclick="listTable.edit(this, 'edit_grade', 269)">0</span></td>
    <td width="10%" align="right"><span onclick="listTable.edit(this, 'edit_sort_order', 269)">50</span></td>
    <td width="24%" align="center">
      <a href="category.php?act=move&cat_id=269">转移商品</a> |
      <a href="category.php?act=edit&amp;cat_id=269">编辑</a> |
      <a href="javascript:;" onclick="listTable.remove(269, '您确认要删除这条记录吗?')" title="移除">移除</a>
	  <!--代码修改_start Byjdy-->
	  	  <!--代码修改_end Byjdy-->
    </td>
  </tr>
    <tr align="center" class="2" id="2_266">
    <td align="left" class="first-cell" >
            <img src="images/menu_minus.gif" id="icon_2_266" width="9" height="9" border="0" style="margin-left:2em" onclick="rowClicked(this)" />
            <span>
      <!-- 0-默认列表 1-搜索匹配列表 其他-搜索未匹配列表 -->
            <a href="goods.php?act=list&cat_id=266&supp=-1">腰凳</a>
            </span>
        </td>
    <td width="10%">0</td>
    <td width="10%"><span onclick="listTable.edit(this, 'edit_measure_unit', 266)"><!--  -->&nbsp;&nbsp;&nbsp;&nbsp;<!--  --></span></td>
    <td width="10%"><img src="images/no.gif" onclick="listTable.toggle(this, 'toggle_show_in_nav', 266)" /></td>
    <td width="10%"><img src="images/yes.gif" onclick="listTable.toggle(this, 'toggle_is_show', 266)" /></td>
    <td><span onclick="listTable.edit(this, 'edit_grade', 266)">0</span></td>
    <td width="10%" align="right"><span onclick="listTable.edit(this, 'edit_sort_order', 266)">50</span></td>
    <td width="24%" align="center">
      <a href="category.php?act=move&cat_id=266">转移商品</a> |
      <a href="category.php?act=edit&amp;cat_id=266">编辑</a> |
      <a href="javascript:;" onclick="listTable.remove(266, '您确认要删除这条记录吗?')" title="移除">移除</a>
	  <!--代码修改_start Byjdy-->
	  	  <!--代码修改_end Byjdy-->
    </td>
  </tr>
    <tr align="center" class="2" id="2_263">
    <td align="left" class="first-cell" >
            <img src="images/menu_minus.gif" id="icon_2_263" width="9" height="9" border="0" style="margin-left:2em" onclick="rowClicked(this)" />
            <span>
      <!-- 0-默认列表 1-搜索匹配列表 其他-搜索未匹配列表 -->
            <a href="goods.php?act=list&cat_id=263&supp=-1">孕妇裙</a>
            </span>
        </td>
    <td width="10%">0</td>
    <td width="10%"><span onclick="listTable.edit(this, 'edit_measure_unit', 263)"><!--  -->&nbsp;&nbsp;&nbsp;&nbsp;<!--  --></span></td>
    <td width="10%"><img src="images/no.gif" onclick="listTable.toggle(this, 'toggle_show_in_nav', 263)" /></td>
    <td width="10%"><img src="images/yes.gif" onclick="listTable.toggle(this, 'toggle_is_show', 263)" /></td>
    <td><span onclick="listTable.edit(this, 'edit_grade', 263)">0</span></td>
    <td width="10%" align="right"><span onclick="listTable.edit(this, 'edit_sort_order', 263)">50</span></td>
    <td width="24%" align="center">
      <a href="category.php?act=move&cat_id=263">转移商品</a> |
      <a href="category.php?act=edit&amp;cat_id=263">编辑</a> |
      <a href="javascript:;" onclick="listTable.remove(263, '您确认要删除这条记录吗?')" title="移除">移除</a>
	  <!--代码修改_start Byjdy-->
	  	  <!--代码修改_end Byjdy-->
    </td>
  </tr>
    <tr align="center" class="1" id="1_223">
    <td align="left" class="first-cell" >
            <img src="images/menu_minus.gif" id="icon_1_223" width="9" height="9" border="0" style="margin-left:1em" onclick="rowClicked(this)" />
            <span>
      <!-- 0-默认列表 1-搜索匹配列表 其他-搜索未匹配列表 -->
            <a href="goods.php?act=list&cat_id=223&supp=-1">孕婴洗护</a>
            </span>
        </td>
    <td width="10%">9</td>
    <td width="10%"><span onclick="listTable.edit(this, 'edit_measure_unit', 223)"><!--  -->&nbsp;&nbsp;&nbsp;&nbsp;<!--  --></span></td>
    <td width="10%"><img src="images/no.gif" onclick="listTable.toggle(this, 'toggle_show_in_nav', 223)" /></td>
    <td width="10%"><img src="images/yes.gif" onclick="listTable.toggle(this, 'toggle_is_show', 223)" /></td>
    <td><span onclick="listTable.edit(this, 'edit_grade', 223)">0</span></td>
    <td width="10%" align="right"><span onclick="listTable.edit(this, 'edit_sort_order', 223)">50</span></td>
    <td width="24%" align="center">
      <a href="category.php?act=move&cat_id=223">转移商品</a> |
      <a href="category.php?act=edit&amp;cat_id=223">编辑</a> |
      <a href="javascript:;" onclick="listTable.remove(223, '您确认要删除这条记录吗?')" title="移除">移除</a>
	  <!--代码修改_start Byjdy-->
	  	  <!--代码修改_end Byjdy-->
    </td>
  </tr>
    <tr align="center" class="2" id="2_241">
    <td align="left" class="first-cell" >
            <img src="images/menu_minus.gif" id="icon_2_241" width="9" height="9" border="0" style="margin-left:2em" onclick="rowClicked(this)" />
            <span>
      <!-- 0-默认列表 1-搜索匹配列表 其他-搜索未匹配列表 -->
            <a href="goods.php?act=list&cat_id=241&supp=-1">洗衣液/皂</a>
            </span>
        </td>
    <td width="10%">0</td>
    <td width="10%"><span onclick="listTable.edit(this, 'edit_measure_unit', 241)"><!--  -->&nbsp;&nbsp;&nbsp;&nbsp;<!--  --></span></td>
    <td width="10%"><img src="images/no.gif" onclick="listTable.toggle(this, 'toggle_show_in_nav', 241)" /></td>
    <td width="10%"><img src="images/yes.gif" onclick="listTable.toggle(this, 'toggle_is_show', 241)" /></td>
    <td><span onclick="listTable.edit(this, 'edit_grade', 241)">0</span></td>
    <td width="10%" align="right"><span onclick="listTable.edit(this, 'edit_sort_order', 241)">50</span></td>
    <td width="24%" align="center">
      <a href="category.php?act=move&cat_id=241">转移商品</a> |
      <a href="category.php?act=edit&amp;cat_id=241">编辑</a> |
      <a href="javascript:;" onclick="listTable.remove(241, '您确认要删除这条记录吗?')" title="移除">移除</a>
	  <!--代码修改_start Byjdy-->
	  	  <!--代码修改_end Byjdy-->
    </td>
  </tr>
    <tr align="center" class="2" id="2_245">
    <td align="left" class="first-cell" >
            <img src="images/menu_minus.gif" id="icon_2_245" width="9" height="9" border="0" style="margin-left:2em" onclick="rowClicked(this)" />
            <span>
      <!-- 0-默认列表 1-搜索匹配列表 其他-搜索未匹配列表 -->
            <a href="goods.php?act=list&cat_id=245&supp=-1">爽身粉</a>
            </span>
        </td>
    <td width="10%">0</td>
    <td width="10%"><span onclick="listTable.edit(this, 'edit_measure_unit', 245)"><!--  -->&nbsp;&nbsp;&nbsp;&nbsp;<!--  --></span></td>
    <td width="10%"><img src="images/no.gif" onclick="listTable.toggle(this, 'toggle_show_in_nav', 245)" /></td>
    <td width="10%"><img src="images/yes.gif" onclick="listTable.toggle(this, 'toggle_is_show', 245)" /></td>
    <td><span onclick="listTable.edit(this, 'edit_grade', 245)">0</span></td>
    <td width="10%" align="right"><span onclick="listTable.edit(this, 'edit_sort_order', 245)">50</span></td>
    <td width="24%" align="center">
      <a href="category.php?act=move&cat_id=245">转移商品</a> |
      <a href="category.php?act=edit&amp;cat_id=245">编辑</a> |
      <a href="javascript:;" onclick="listTable.remove(245, '您确认要删除这条记录吗?')" title="移除">移除</a>
	  <!--代码修改_start Byjdy-->
	  	  <!--代码修改_end Byjdy-->
    </td>
  </tr>
    <tr align="center" class="2" id="2_242">
    <td align="left" class="first-cell" >
            <img src="images/menu_minus.gif" id="icon_2_242" width="9" height="9" border="0" style="margin-left:2em" onclick="rowClicked(this)" />
            <span>
      <!-- 0-默认列表 1-搜索匹配列表 其他-搜索未匹配列表 -->
            <a href="goods.php?act=list&cat_id=242&supp=-1">宝宝沐浴</a>
            </span>
        </td>
    <td width="10%">0</td>
    <td width="10%"><span onclick="listTable.edit(this, 'edit_measure_unit', 242)"><!--  -->&nbsp;&nbsp;&nbsp;&nbsp;<!--  --></span></td>
    <td width="10%"><img src="images/no.gif" onclick="listTable.toggle(this, 'toggle_show_in_nav', 242)" /></td>
    <td width="10%"><img src="images/yes.gif" onclick="listTable.toggle(this, 'toggle_is_show', 242)" /></td>
    <td><span onclick="listTable.edit(this, 'edit_grade', 242)">0</span></td>
    <td width="10%" align="right"><span onclick="listTable.edit(this, 'edit_sort_order', 242)">50</span></td>
    <td width="24%" align="center">
      <a href="category.php?act=move&cat_id=242">转移商品</a> |
      <a href="category.php?act=edit&amp;cat_id=242">编辑</a> |
      <a href="javascript:;" onclick="listTable.remove(242, '您确认要删除这条记录吗?')" title="移除">移除</a>
	  <!--代码修改_start Byjdy-->
	  	  <!--代码修改_end Byjdy-->
    </td>
  </tr>
    <tr align="center" class="2" id="2_246">
    <td align="left" class="first-cell" >
            <img src="images/menu_minus.gif" id="icon_2_246" width="9" height="9" border="0" style="margin-left:2em" onclick="rowClicked(this)" />
            <span>
      <!-- 0-默认列表 1-搜索匹配列表 其他-搜索未匹配列表 -->
            <a href="goods.php?act=list&cat_id=246&supp=-1">奶瓶清洗</a>
            </span>
        </td>
    <td width="10%">0</td>
    <td width="10%"><span onclick="listTable.edit(this, 'edit_measure_unit', 246)"><!--  -->&nbsp;&nbsp;&nbsp;&nbsp;<!--  --></span></td>
    <td width="10%"><img src="images/no.gif" onclick="listTable.toggle(this, 'toggle_show_in_nav', 246)" /></td>
    <td width="10%"><img src="images/yes.gif" onclick="listTable.toggle(this, 'toggle_is_show', 246)" /></td>
    <td><span onclick="listTable.edit(this, 'edit_grade', 246)">0</span></td>
    <td width="10%" align="right"><span onclick="listTable.edit(this, 'edit_sort_order', 246)">50</span></td>
    <td width="24%" align="center">
      <a href="category.php?act=move&cat_id=246">转移商品</a> |
      <a href="category.php?act=edit&amp;cat_id=246">编辑</a> |
      <a href="javascript:;" onclick="listTable.remove(246, '您确认要删除这条记录吗?')" title="移除">移除</a>
	  <!--代码修改_start Byjdy-->
	  	  <!--代码修改_end Byjdy-->
    </td>
  </tr>
    <tr align="center" class="2" id="2_243">
    <td align="left" class="first-cell" >
            <img src="images/menu_minus.gif" id="icon_2_243" width="9" height="9" border="0" style="margin-left:2em" onclick="rowClicked(this)" />
            <span>
      <!-- 0-默认列表 1-搜索匹配列表 其他-搜索未匹配列表 -->
            <a href="goods.php?act=list&cat_id=243&supp=-1">儿童防晒</a>
            </span>
        </td>
    <td width="10%">0</td>
    <td width="10%"><span onclick="listTable.edit(this, 'edit_measure_unit', 243)"><!--  -->&nbsp;&nbsp;&nbsp;&nbsp;<!--  --></span></td>
    <td width="10%"><img src="images/no.gif" onclick="listTable.toggle(this, 'toggle_show_in_nav', 243)" /></td>
    <td width="10%"><img src="images/yes.gif" onclick="listTable.toggle(this, 'toggle_is_show', 243)" /></td>
    <td><span onclick="listTable.edit(this, 'edit_grade', 243)">0</span></td>
    <td width="10%" align="right"><span onclick="listTable.edit(this, 'edit_sort_order', 243)">50</span></td>
    <td width="24%" align="center">
      <a href="category.php?act=move&cat_id=243">转移商品</a> |
      <a href="category.php?act=edit&amp;cat_id=243">编辑</a> |
      <a href="javascript:;" onclick="listTable.remove(243, '您确认要删除这条记录吗?')" title="移除">移除</a>
	  <!--代码修改_start Byjdy-->
	  	  <!--代码修改_end Byjdy-->
    </td>
  </tr>
    <tr align="center" class="2" id="2_247">
    <td align="left" class="first-cell" >
            <img src="images/menu_minus.gif" id="icon_2_247" width="9" height="9" border="0" style="margin-left:2em" onclick="rowClicked(this)" />
            <span>
      <!-- 0-默认列表 1-搜索匹配列表 其他-搜索未匹配列表 -->
            <a href="goods.php?act=list&cat_id=247&supp=-1">孕妇护肤</a>
            </span>
        </td>
    <td width="10%">0</td>
    <td width="10%"><span onclick="listTable.edit(this, 'edit_measure_unit', 247)"><!--  -->&nbsp;&nbsp;&nbsp;&nbsp;<!--  --></span></td>
    <td width="10%"><img src="images/no.gif" onclick="listTable.toggle(this, 'toggle_show_in_nav', 247)" /></td>
    <td width="10%"><img src="images/yes.gif" onclick="listTable.toggle(this, 'toggle_is_show', 247)" /></td>
    <td><span onclick="listTable.edit(this, 'edit_grade', 247)">0</span></td>
    <td width="10%" align="right"><span onclick="listTable.edit(this, 'edit_sort_order', 247)">50</span></td>
    <td width="24%" align="center">
      <a href="category.php?act=move&cat_id=247">转移商品</a> |
      <a href="category.php?act=edit&amp;cat_id=247">编辑</a> |
      <a href="javascript:;" onclick="listTable.remove(247, '您确认要删除这条记录吗?')" title="移除">移除</a>
	  <!--代码修改_start Byjdy-->
	  	  <!--代码修改_end Byjdy-->
    </td>
  </tr>
    <tr align="center" class="2" id="2_244">
    <td align="left" class="first-cell" >
            <img src="images/menu_minus.gif" id="icon_2_244" width="9" height="9" border="0" style="margin-left:2em" onclick="rowClicked(this)" />
            <span>
      <!-- 0-默认列表 1-搜索匹配列表 其他-搜索未匹配列表 -->
            <a href="goods.php?act=list&cat_id=244&supp=-1">防蚊/驱蚊</a>
            </span>
        </td>
    <td width="10%">0</td>
    <td width="10%"><span onclick="listTable.edit(this, 'edit_measure_unit', 244)"><!--  -->&nbsp;&nbsp;&nbsp;&nbsp;<!--  --></span></td>
    <td width="10%"><img src="images/no.gif" onclick="listTable.toggle(this, 'toggle_show_in_nav', 244)" /></td>
    <td width="10%"><img src="images/yes.gif" onclick="listTable.toggle(this, 'toggle_is_show', 244)" /></td>
    <td><span onclick="listTable.edit(this, 'edit_grade', 244)">0</span></td>
    <td width="10%" align="right"><span onclick="listTable.edit(this, 'edit_sort_order', 244)">50</span></td>
    <td width="24%" align="center">
      <a href="category.php?act=move&cat_id=244">转移商品</a> |
      <a href="category.php?act=edit&amp;cat_id=244">编辑</a> |
      <a href="javascript:;" onclick="listTable.remove(244, '您确认要删除这条记录吗?')" title="移除">移除</a>
	  <!--代码修改_start Byjdy-->
	  	  <!--代码修改_end Byjdy-->
    </td>
  </tr>
    <tr align="center" class="1" id="1_224">
    <td align="left" class="first-cell" >
            <img src="images/menu_minus.gif" id="icon_1_224" width="9" height="9" border="0" style="margin-left:1em" onclick="rowClicked(this)" />
            <span>
      <!-- 0-默认列表 1-搜索匹配列表 其他-搜索未匹配列表 -->
            <a href="goods.php?act=list&cat_id=224&supp=-1">喂养用品</a>
            </span>
        </td>
    <td width="10%">0</td>
    <td width="10%"><span onclick="listTable.edit(this, 'edit_measure_unit', 224)"><!--  -->&nbsp;&nbsp;&nbsp;&nbsp;<!--  --></span></td>
    <td width="10%"><img src="images/no.gif" onclick="listTable.toggle(this, 'toggle_show_in_nav', 224)" /></td>
    <td width="10%"><img src="images/yes.gif" onclick="listTable.toggle(this, 'toggle_is_show', 224)" /></td>
    <td><span onclick="listTable.edit(this, 'edit_grade', 224)">0</span></td>
    <td width="10%" align="right"><span onclick="listTable.edit(this, 'edit_sort_order', 224)">50</span></td>
    <td width="24%" align="center">
      <a href="category.php?act=move&cat_id=224">转移商品</a> |
      <a href="category.php?act=edit&amp;cat_id=224">编辑</a> |
      <a href="javascript:;" onclick="listTable.remove(224, '您确认要删除这条记录吗?')" title="移除">移除</a>
	  <!--代码修改_start Byjdy-->
	  	  <!--代码修改_end Byjdy-->
    </td>
  </tr>
    <tr align="center" class="2" id="2_254">
    <td align="left" class="first-cell" >
            <img src="images/menu_minus.gif" id="icon_2_254" width="9" height="9" border="0" style="margin-left:2em" onclick="rowClicked(this)" />
            <span>
      <!-- 0-默认列表 1-搜索匹配列表 其他-搜索未匹配列表 -->
            <a href="goods.php?act=list&cat_id=254&supp=-1">防溢乳垫</a>
            </span>
        </td>
    <td width="10%">0</td>
    <td width="10%"><span onclick="listTable.edit(this, 'edit_measure_unit', 254)"><!--  -->&nbsp;&nbsp;&nbsp;&nbsp;<!--  --></span></td>
    <td width="10%"><img src="images/no.gif" onclick="listTable.toggle(this, 'toggle_show_in_nav', 254)" /></td>
    <td width="10%"><img src="images/yes.gif" onclick="listTable.toggle(this, 'toggle_is_show', 254)" /></td>
    <td><span onclick="listTable.edit(this, 'edit_grade', 254)">0</span></td>
    <td width="10%" align="right"><span onclick="listTable.edit(this, 'edit_sort_order', 254)">50</span></td>
    <td width="24%" align="center">
      <a href="category.php?act=move&cat_id=254">转移商品</a> |
      <a href="category.php?act=edit&amp;cat_id=254">编辑</a> |
      <a href="javascript:;" onclick="listTable.remove(254, '您确认要删除这条记录吗?')" title="移除">移除</a>
	  <!--代码修改_start Byjdy-->
	  	  <!--代码修改_end Byjdy-->
    </td>
  </tr>
    <tr align="center" class="2" id="2_251">
    <td align="left" class="first-cell" >
            <img src="images/menu_minus.gif" id="icon_2_251" width="9" height="9" border="0" style="margin-left:2em" onclick="rowClicked(this)" />
            <span>
      <!-- 0-默认列表 1-搜索匹配列表 其他-搜索未匹配列表 -->
            <a href="goods.php?act=list&cat_id=251&supp=-1">水壶/水杯</a>
            </span>
        </td>
    <td width="10%">0</td>
    <td width="10%"><span onclick="listTable.edit(this, 'edit_measure_unit', 251)"><!--  -->&nbsp;&nbsp;&nbsp;&nbsp;<!--  --></span></td>
    <td width="10%"><img src="images/no.gif" onclick="listTable.toggle(this, 'toggle_show_in_nav', 251)" /></td>
    <td width="10%"><img src="images/yes.gif" onclick="listTable.toggle(this, 'toggle_is_show', 251)" /></td>
    <td><span onclick="listTable.edit(this, 'edit_grade', 251)">0</span></td>
    <td width="10%" align="right"><span onclick="listTable.edit(this, 'edit_sort_order', 251)">50</span></td>
    <td width="24%" align="center">
      <a href="category.php?act=move&cat_id=251">转移商品</a> |
      <a href="category.php?act=edit&amp;cat_id=251">编辑</a> |
      <a href="javascript:;" onclick="listTable.remove(251, '您确认要删除这条记录吗?')" title="移除">移除</a>
	  <!--代码修改_start Byjdy-->
	  	  <!--代码修改_end Byjdy-->
    </td>
  </tr>
    <tr align="center" class="2" id="2_248">
    <td align="left" class="first-cell" >
            <img src="images/menu_minus.gif" id="icon_2_248" width="9" height="9" border="0" style="margin-left:2em" onclick="rowClicked(this)" />
            <span>
      <!-- 0-默认列表 1-搜索匹配列表 其他-搜索未匹配列表 -->
            <a href="goods.php?act=list&cat_id=248&supp=-1">奶嘴奶瓶</a>
            </span>
        </td>
    <td width="10%">0</td>
    <td width="10%"><span onclick="listTable.edit(this, 'edit_measure_unit', 248)"><!--  -->&nbsp;&nbsp;&nbsp;&nbsp;<!--  --></span></td>
    <td width="10%"><img src="images/no.gif" onclick="listTable.toggle(this, 'toggle_show_in_nav', 248)" /></td>
    <td width="10%"><img src="images/yes.gif" onclick="listTable.toggle(this, 'toggle_is_show', 248)" /></td>
    <td><span onclick="listTable.edit(this, 'edit_grade', 248)">0</span></td>
    <td width="10%" align="right"><span onclick="listTable.edit(this, 'edit_sort_order', 248)">50</span></td>
    <td width="24%" align="center">
      <a href="category.php?act=move&cat_id=248">转移商品</a> |
      <a href="category.php?act=edit&amp;cat_id=248">编辑</a> |
      <a href="javascript:;" onclick="listTable.remove(248, '您确认要删除这条记录吗?')" title="移除">移除</a>
	  <!--代码修改_start Byjdy-->
	  	  <!--代码修改_end Byjdy-->
    </td>
  </tr>
    <tr align="center" class="2" id="2_252">
    <td align="left" class="first-cell" >
            <img src="images/menu_minus.gif" id="icon_2_252" width="9" height="9" border="0" style="margin-left:2em" onclick="rowClicked(this)" />
            <span>
      <!-- 0-默认列表 1-搜索匹配列表 其他-搜索未匹配列表 -->
            <a href="goods.php?act=list&cat_id=252&supp=-1">吸奶器</a>
            </span>
        </td>
    <td width="10%">0</td>
    <td width="10%"><span onclick="listTable.edit(this, 'edit_measure_unit', 252)"><!--  -->&nbsp;&nbsp;&nbsp;&nbsp;<!--  --></span></td>
    <td width="10%"><img src="images/no.gif" onclick="listTable.toggle(this, 'toggle_show_in_nav', 252)" /></td>
    <td width="10%"><img src="images/yes.gif" onclick="listTable.toggle(this, 'toggle_is_show', 252)" /></td>
    <td><span onclick="listTable.edit(this, 'edit_grade', 252)">0</span></td>
    <td width="10%" align="right"><span onclick="listTable.edit(this, 'edit_sort_order', 252)">50</span></td>
    <td width="24%" align="center">
      <a href="category.php?act=move&cat_id=252">转移商品</a> |
      <a href="category.php?act=edit&amp;cat_id=252">编辑</a> |
      <a href="javascript:;" onclick="listTable.remove(252, '您确认要删除这条记录吗?')" title="移除">移除</a>
	  <!--代码修改_start Byjdy-->
	  	  <!--代码修改_end Byjdy-->
    </td>
  </tr>
    <tr align="center" class="2" id="2_249">
    <td align="left" class="first-cell" >
            <img src="images/menu_minus.gif" id="icon_2_249" width="9" height="9" border="0" style="margin-left:2em" onclick="rowClicked(this)" />
            <span>
      <!-- 0-默认列表 1-搜索匹配列表 其他-搜索未匹配列表 -->
            <a href="goods.php?act=list&cat_id=249&supp=-1">驱蚊用品</a>
            </span>
        </td>
    <td width="10%">0</td>
    <td width="10%"><span onclick="listTable.edit(this, 'edit_measure_unit', 249)"><!--  -->&nbsp;&nbsp;&nbsp;&nbsp;<!--  --></span></td>
    <td width="10%"><img src="images/no.gif" onclick="listTable.toggle(this, 'toggle_show_in_nav', 249)" /></td>
    <td width="10%"><img src="images/yes.gif" onclick="listTable.toggle(this, 'toggle_is_show', 249)" /></td>
    <td><span onclick="listTable.edit(this, 'edit_grade', 249)">0</span></td>
    <td width="10%" align="right"><span onclick="listTable.edit(this, 'edit_sort_order', 249)">50</span></td>
    <td width="24%" align="center">
      <a href="category.php?act=move&cat_id=249">转移商品</a> |
      <a href="category.php?act=edit&amp;cat_id=249">编辑</a> |
      <a href="javascript:;" onclick="listTable.remove(249, '您确认要删除这条记录吗?')" title="移除">移除</a>
	  <!--代码修改_start Byjdy-->
	  	  <!--代码修改_end Byjdy-->
    </td>
  </tr>
    <tr align="center" class="2" id="2_253">
    <td align="left" class="first-cell" >
            <img src="images/menu_minus.gif" id="icon_2_253" width="9" height="9" border="0" style="margin-left:2em" onclick="rowClicked(this)" />
            <span>
      <!-- 0-默认列表 1-搜索匹配列表 其他-搜索未匹配列表 -->
            <a href="goods.php?act=list&cat_id=253&supp=-1">理发器</a>
            </span>
        </td>
    <td width="10%">0</td>
    <td width="10%"><span onclick="listTable.edit(this, 'edit_measure_unit', 253)"><!--  -->&nbsp;&nbsp;&nbsp;&nbsp;<!--  --></span></td>
    <td width="10%"><img src="images/no.gif" onclick="listTable.toggle(this, 'toggle_show_in_nav', 253)" /></td>
    <td width="10%"><img src="images/yes.gif" onclick="listTable.toggle(this, 'toggle_is_show', 253)" /></td>
    <td><span onclick="listTable.edit(this, 'edit_grade', 253)">0</span></td>
    <td width="10%" align="right"><span onclick="listTable.edit(this, 'edit_sort_order', 253)">50</span></td>
    <td width="24%" align="center">
      <a href="category.php?act=move&cat_id=253">转移商品</a> |
      <a href="category.php?act=edit&amp;cat_id=253">编辑</a> |
      <a href="javascript:;" onclick="listTable.remove(253, '您确认要删除这条记录吗?')" title="移除">移除</a>
	  <!--代码修改_start Byjdy-->
	  	  <!--代码修改_end Byjdy-->
    </td>
  </tr>
    <tr align="center" class="2" id="2_250">
    <td align="left" class="first-cell" >
            <img src="images/menu_minus.gif" id="icon_2_250" width="9" height="9" border="0" style="margin-left:2em" onclick="rowClicked(this)" />
            <span>
      <!-- 0-默认列表 1-搜索匹配列表 其他-搜索未匹配列表 -->
            <a href="goods.php?act=list&cat_id=250&supp=-1">浴室用品</a>
            </span>
        </td>
    <td width="10%">0</td>
    <td width="10%"><span onclick="listTable.edit(this, 'edit_measure_unit', 250)"><!--  -->&nbsp;&nbsp;&nbsp;&nbsp;<!--  --></span></td>
    <td width="10%"><img src="images/no.gif" onclick="listTable.toggle(this, 'toggle_show_in_nav', 250)" /></td>
    <td width="10%"><img src="images/yes.gif" onclick="listTable.toggle(this, 'toggle_is_show', 250)" /></td>
    <td><span onclick="listTable.edit(this, 'edit_grade', 250)">0</span></td>
    <td width="10%" align="right"><span onclick="listTable.edit(this, 'edit_sort_order', 250)">50</span></td>
    <td width="24%" align="center">
      <a href="category.php?act=move&cat_id=250">转移商品</a> |
      <a href="category.php?act=edit&amp;cat_id=250">编辑</a> |
      <a href="javascript:;" onclick="listTable.remove(250, '您确认要删除这条记录吗?')" title="移除">移除</a>
	  <!--代码修改_start Byjdy-->
	  	  <!--代码修改_end Byjdy-->
    </td>
  </tr>
    <tr align="center" class="1" id="1_221">
    <td align="left" class="first-cell" >
            <img src="images/menu_minus.gif" id="icon_1_221" width="9" height="9" border="0" style="margin-left:1em" onclick="rowClicked(this)" />
            <span>
      <!-- 0-默认列表 1-搜索匹配列表 其他-搜索未匹配列表 -->
            <a href="goods.php?act=list&cat_id=221&supp=-1">孕婴奶粉</a>
            </span>
        </td>
    <td width="10%">8</td>
    <td width="10%"><span onclick="listTable.edit(this, 'edit_measure_unit', 221)"><!--  -->&nbsp;&nbsp;&nbsp;&nbsp;<!--  --></span></td>
    <td width="10%"><img src="images/no.gif" onclick="listTable.toggle(this, 'toggle_show_in_nav', 221)" /></td>
    <td width="10%"><img src="images/yes.gif" onclick="listTable.toggle(this, 'toggle_is_show', 221)" /></td>
    <td><span onclick="listTable.edit(this, 'edit_grade', 221)">0</span></td>
    <td width="10%" align="right"><span onclick="listTable.edit(this, 'edit_sort_order', 221)">50</span></td>
    <td width="24%" align="center">
      <a href="category.php?act=move&cat_id=221">转移商品</a> |
      <a href="category.php?act=edit&amp;cat_id=221">编辑</a> |
      <a href="javascript:;" onclick="listTable.remove(221, '您确认要删除这条记录吗?')" title="移除">移除</a>
	  <!--代码修改_start Byjdy-->
	  	  <!--代码修改_end Byjdy-->
    </td>
  </tr>
    <tr align="center" class="2" id="2_232">
    <td align="left" class="first-cell" >
            <img src="images/menu_minus.gif" id="icon_2_232" width="9" height="9" border="0" style="margin-left:2em" onclick="rowClicked(this)" />
            <span>
      <!-- 0-默认列表 1-搜索匹配列表 其他-搜索未匹配列表 -->
            <a href="goods.php?act=list&cat_id=232&supp=-1">3段</a>
            </span>
        </td>
    <td width="10%">0</td>
    <td width="10%"><span onclick="listTable.edit(this, 'edit_measure_unit', 232)"><!--  -->&nbsp;&nbsp;&nbsp;&nbsp;<!--  --></span></td>
    <td width="10%"><img src="images/no.gif" onclick="listTable.toggle(this, 'toggle_show_in_nav', 232)" /></td>
    <td width="10%"><img src="images/yes.gif" onclick="listTable.toggle(this, 'toggle_is_show', 232)" /></td>
    <td><span onclick="listTable.edit(this, 'edit_grade', 232)">0</span></td>
    <td width="10%" align="right"><span onclick="listTable.edit(this, 'edit_sort_order', 232)">50</span></td>
    <td width="24%" align="center">
      <a href="category.php?act=move&cat_id=232">转移商品</a> |
      <a href="category.php?act=edit&amp;cat_id=232">编辑</a> |
      <a href="javascript:;" onclick="listTable.remove(232, '您确认要删除这条记录吗?')" title="移除">移除</a>
	  <!--代码修改_start Byjdy-->
	  	  <!--代码修改_end Byjdy-->
    </td>
  </tr>
    <tr align="center" class="2" id="2_229">
    <td align="left" class="first-cell" >
            <img src="images/menu_minus.gif" id="icon_2_229" width="9" height="9" border="0" style="margin-left:2em" onclick="rowClicked(this)" />
            <span>
      <!-- 0-默认列表 1-搜索匹配列表 其他-搜索未匹配列表 -->
            <a href="goods.php?act=list&cat_id=229&supp=-1">pre段</a>
            </span>
        </td>
    <td width="10%">0</td>
    <td width="10%"><span onclick="listTable.edit(this, 'edit_measure_unit', 229)"><!--  -->&nbsp;&nbsp;&nbsp;&nbsp;<!--  --></span></td>
    <td width="10%"><img src="images/no.gif" onclick="listTable.toggle(this, 'toggle_show_in_nav', 229)" /></td>
    <td width="10%"><img src="images/yes.gif" onclick="listTable.toggle(this, 'toggle_is_show', 229)" /></td>
    <td><span onclick="listTable.edit(this, 'edit_grade', 229)">0</span></td>
    <td width="10%" align="right"><span onclick="listTable.edit(this, 'edit_sort_order', 229)">50</span></td>
    <td width="24%" align="center">
      <a href="category.php?act=move&cat_id=229">转移商品</a> |
      <a href="category.php?act=edit&amp;cat_id=229">编辑</a> |
      <a href="javascript:;" onclick="listTable.remove(229, '您确认要删除这条记录吗?')" title="移除">移除</a>
	  <!--代码修改_start Byjdy-->
	  	  <!--代码修改_end Byjdy-->
    </td>
  </tr>
    <tr align="center" class="2" id="2_230">
    <td align="left" class="first-cell" >
            <img src="images/menu_minus.gif" id="icon_2_230" width="9" height="9" border="0" style="margin-left:2em" onclick="rowClicked(this)" />
            <span>
      <!-- 0-默认列表 1-搜索匹配列表 其他-搜索未匹配列表 -->
            <a href="goods.php?act=list&cat_id=230&supp=-1">1段</a>
            </span>
        </td>
    <td width="10%">0</td>
    <td width="10%"><span onclick="listTable.edit(this, 'edit_measure_unit', 230)"><!--  -->&nbsp;&nbsp;&nbsp;&nbsp;<!--  --></span></td>
    <td width="10%"><img src="images/no.gif" onclick="listTable.toggle(this, 'toggle_show_in_nav', 230)" /></td>
    <td width="10%"><img src="images/yes.gif" onclick="listTable.toggle(this, 'toggle_is_show', 230)" /></td>
    <td><span onclick="listTable.edit(this, 'edit_grade', 230)">0</span></td>
    <td width="10%" align="right"><span onclick="listTable.edit(this, 'edit_sort_order', 230)">50</span></td>
    <td width="24%" align="center">
      <a href="category.php?act=move&cat_id=230">转移商品</a> |
      <a href="category.php?act=edit&amp;cat_id=230">编辑</a> |
      <a href="javascript:;" onclick="listTable.remove(230, '您确认要删除这条记录吗?')" title="移除">移除</a>
	  <!--代码修改_start Byjdy-->
	  	  <!--代码修改_end Byjdy-->
    </td>
  </tr>
    <tr align="center" class="2" id="2_227">
    <td align="left" class="first-cell" >
            <img src="images/menu_minus.gif" id="icon_2_227" width="9" height="9" border="0" style="margin-left:2em" onclick="rowClicked(this)" />
            <span>
      <!-- 0-默认列表 1-搜索匹配列表 其他-搜索未匹配列表 -->
            <a href="goods.php?act=list&cat_id=227&supp=-1">特配奶粉</a>
            </span>
        </td>
    <td width="10%">0</td>
    <td width="10%"><span onclick="listTable.edit(this, 'edit_measure_unit', 227)"><!--  -->&nbsp;&nbsp;&nbsp;&nbsp;<!--  --></span></td>
    <td width="10%"><img src="images/no.gif" onclick="listTable.toggle(this, 'toggle_show_in_nav', 227)" /></td>
    <td width="10%"><img src="images/yes.gif" onclick="listTable.toggle(this, 'toggle_is_show', 227)" /></td>
    <td><span onclick="listTable.edit(this, 'edit_grade', 227)">0</span></td>
    <td width="10%" align="right"><span onclick="listTable.edit(this, 'edit_sort_order', 227)">50</span></td>
    <td width="24%" align="center">
      <a href="category.php?act=move&cat_id=227">转移商品</a> |
      <a href="category.php?act=edit&amp;cat_id=227">编辑</a> |
      <a href="javascript:;" onclick="listTable.remove(227, '您确认要删除这条记录吗?')" title="移除">移除</a>
	  <!--代码修改_start Byjdy-->
	  	  <!--代码修改_end Byjdy-->
    </td>
  </tr>
    <tr align="center" class="2" id="2_231">
    <td align="left" class="first-cell" >
            <img src="images/menu_minus.gif" id="icon_2_231" width="9" height="9" border="0" style="margin-left:2em" onclick="rowClicked(this)" />
            <span>
      <!-- 0-默认列表 1-搜索匹配列表 其他-搜索未匹配列表 -->
            <a href="goods.php?act=list&cat_id=231&supp=-1">2段</a>
            </span>
        </td>
    <td width="10%">0</td>
    <td width="10%"><span onclick="listTable.edit(this, 'edit_measure_unit', 231)"><!--  -->&nbsp;&nbsp;&nbsp;&nbsp;<!--  --></span></td>
    <td width="10%"><img src="images/no.gif" onclick="listTable.toggle(this, 'toggle_show_in_nav', 231)" /></td>
    <td width="10%"><img src="images/yes.gif" onclick="listTable.toggle(this, 'toggle_is_show', 231)" /></td>
    <td><span onclick="listTable.edit(this, 'edit_grade', 231)">0</span></td>
    <td width="10%" align="right"><span onclick="listTable.edit(this, 'edit_sort_order', 231)">50</span></td>
    <td width="24%" align="center">
      <a href="category.php?act=move&cat_id=231">转移商品</a> |
      <a href="category.php?act=edit&amp;cat_id=231">编辑</a> |
      <a href="javascript:;" onclick="listTable.remove(231, '您确认要删除这条记录吗?')" title="移除">移除</a>
	  <!--代码修改_start Byjdy-->
	  	  <!--代码修改_end Byjdy-->
    </td>
  </tr>
    <tr align="center" class="2" id="2_228">
    <td align="left" class="first-cell" >
            <img src="images/menu_minus.gif" id="icon_2_228" width="9" height="9" border="0" style="margin-left:2em" onclick="rowClicked(this)" />
            <span>
      <!-- 0-默认列表 1-搜索匹配列表 其他-搜索未匹配列表 -->
            <a href="goods.php?act=list&cat_id=228&supp=-1">孕妈奶粉</a>
            </span>
        </td>
    <td width="10%">0</td>
    <td width="10%"><span onclick="listTable.edit(this, 'edit_measure_unit', 228)"><!--  -->&nbsp;&nbsp;&nbsp;&nbsp;<!--  --></span></td>
    <td width="10%"><img src="images/no.gif" onclick="listTable.toggle(this, 'toggle_show_in_nav', 228)" /></td>
    <td width="10%"><img src="images/yes.gif" onclick="listTable.toggle(this, 'toggle_is_show', 228)" /></td>
    <td><span onclick="listTable.edit(this, 'edit_grade', 228)">0</span></td>
    <td width="10%" align="right"><span onclick="listTable.edit(this, 'edit_sort_order', 228)">50</span></td>
    <td width="24%" align="center">
      <a href="category.php?act=move&cat_id=228">转移商品</a> |
      <a href="category.php?act=edit&amp;cat_id=228">编辑</a> |
      <a href="javascript:;" onclick="listTable.remove(228, '您确认要删除这条记录吗?')" title="移除">移除</a>
	  <!--代码修改_start Byjdy-->
	  	  <!--代码修改_end Byjdy-->
    </td>
  </tr>
    <tr align="center" class="0" id="0_1">
    <td align="left" class="first-cell" >
            <img src="images/menu_minus.gif" id="icon_0_1" width="9" height="9" border="0" style="margin-left:0em" onclick="rowClicked(this)" />
            <span>
      <!-- 0-默认列表 1-搜索匹配列表 其他-搜索未匹配列表 -->
            <a href="goods.php?act=list&cat_id=1&supp=-1">食品、酒类、生鲜、特产</a>
            </span>
        </td>
    <td width="10%">4</td>
    <td width="10%"><span onclick="listTable.edit(this, 'edit_measure_unit', 1)"><!--  -->&nbsp;&nbsp;&nbsp;&nbsp;<!--  --></span></td>
    <td width="10%"><img src="images/yes.gif" onclick="listTable.toggle(this, 'toggle_show_in_nav', 1)" /></td>
    <td width="10%"><img src="images/yes.gif" onclick="listTable.toggle(this, 'toggle_is_show', 1)" /></td>
    <td><span onclick="listTable.edit(this, 'edit_grade', 1)">5</span></td>
    <td width="10%" align="right"><span onclick="listTable.edit(this, 'edit_sort_order', 1)">11</span></td>
    <td width="24%" align="center">
      <a href="category.php?act=move&cat_id=1">转移商品</a> |
      <a href="category.php?act=edit&amp;cat_id=1">编辑</a> |
      <a href="javascript:;" onclick="listTable.remove(1, '您确认要删除这条记录吗?')" title="移除">移除</a>
	  <!--代码修改_start Byjdy-->
	  	  | <a href="category_flashimg.php?act=list&amp;cat_id=1">设置轮播图片</a>	  
	  	  <!--代码修改_end Byjdy-->
    </td>
  </tr>
    <tr align="center" class="1" id="1_14">
    <td align="left" class="first-cell" >
            <img src="images/menu_minus.gif" id="icon_1_14" width="9" height="9" border="0" style="margin-left:1em" onclick="rowClicked(this)" />
            <span>
      <!-- 0-默认列表 1-搜索匹配列表 其他-搜索未匹配列表 -->
            <a href="goods.php?act=list&cat_id=14&supp=-1">进口水果</a>
            </span>
        </td>
    <td width="10%">17</td>
    <td width="10%"><span onclick="listTable.edit(this, 'edit_measure_unit', 14)"><!--  -->&nbsp;&nbsp;&nbsp;&nbsp;<!--  --></span></td>
    <td width="10%"><img src="images/no.gif" onclick="listTable.toggle(this, 'toggle_show_in_nav', 14)" /></td>
    <td width="10%"><img src="images/yes.gif" onclick="listTable.toggle(this, 'toggle_is_show', 14)" /></td>
    <td><span onclick="listTable.edit(this, 'edit_grade', 14)">0</span></td>
    <td width="10%" align="right"><span onclick="listTable.edit(this, 'edit_sort_order', 14)">10</span></td>
    <td width="24%" align="center">
      <a href="category.php?act=move&cat_id=14">转移商品</a> |
      <a href="category.php?act=edit&amp;cat_id=14">编辑</a> |
      <a href="javascript:;" onclick="listTable.remove(14, '您确认要删除这条记录吗?')" title="移除">移除</a>
	  <!--代码修改_start Byjdy-->
	  	  <!--代码修改_end Byjdy-->
    </td>
  </tr>
    <tr align="center" class="2" id="2_20">
    <td align="left" class="first-cell" >
            <img src="images/menu_minus.gif" id="icon_2_20" width="9" height="9" border="0" style="margin-left:2em" onclick="rowClicked(this)" />
            <span>
      <!-- 0-默认列表 1-搜索匹配列表 其他-搜索未匹配列表 -->
            <a href="goods.php?act=list&cat_id=20&supp=-1">奇异果猕猴桃</a>
            </span>
        </td>
    <td width="10%">0</td>
    <td width="10%"><span onclick="listTable.edit(this, 'edit_measure_unit', 20)"><!--  -->&nbsp;&nbsp;&nbsp;&nbsp;<!--  --></span></td>
    <td width="10%"><img src="images/no.gif" onclick="listTable.toggle(this, 'toggle_show_in_nav', 20)" /></td>
    <td width="10%"><img src="images/yes.gif" onclick="listTable.toggle(this, 'toggle_is_show', 20)" /></td>
    <td><span onclick="listTable.edit(this, 'edit_grade', 20)">0</span></td>
    <td width="10%" align="right"><span onclick="listTable.edit(this, 'edit_sort_order', 20)">50</span></td>
    <td width="24%" align="center">
      <a href="category.php?act=move&cat_id=20">转移商品</a> |
      <a href="category.php?act=edit&amp;cat_id=20">编辑</a> |
      <a href="javascript:;" onclick="listTable.remove(20, '您确认要删除这条记录吗?')" title="移除">移除</a>
	  <!--代码修改_start Byjdy-->
	  	  <!--代码修改_end Byjdy-->
    </td>
  </tr>
    <tr align="center" class="2" id="2_17">
    <td align="left" class="first-cell" >
            <img src="images/menu_minus.gif" id="icon_2_17" width="9" height="9" border="0" style="margin-left:2em" onclick="rowClicked(this)" />
            <span>
      <!-- 0-默认列表 1-搜索匹配列表 其他-搜索未匹配列表 -->
            <a href="goods.php?act=list&cat_id=17&supp=-1">芒果桃李</a>
            </span>
        </td>
    <td width="10%">0</td>
    <td width="10%"><span onclick="listTable.edit(this, 'edit_measure_unit', 17)"><!--  -->&nbsp;&nbsp;&nbsp;&nbsp;<!--  --></span></td>
    <td width="10%"><img src="images/no.gif" onclick="listTable.toggle(this, 'toggle_show_in_nav', 17)" /></td>
    <td width="10%"><img src="images/yes.gif" onclick="listTable.toggle(this, 'toggle_is_show', 17)" /></td>
    <td><span onclick="listTable.edit(this, 'edit_grade', 17)">0</span></td>
    <td width="10%" align="right"><span onclick="listTable.edit(this, 'edit_sort_order', 17)">50</span></td>
    <td width="24%" align="center">
      <a href="category.php?act=move&cat_id=17">转移商品</a> |
      <a href="category.php?act=edit&amp;cat_id=17">编辑</a> |
      <a href="javascript:;" onclick="listTable.remove(17, '您确认要删除这条记录吗?')" title="移除">移除</a>
	  <!--代码修改_start Byjdy-->
	  	  <!--代码修改_end Byjdy-->
    </td>
  </tr>
    <tr align="center" class="2" id="2_21">
    <td align="left" class="first-cell" >
            <img src="images/menu_minus.gif" id="icon_2_21" width="9" height="9" border="0" style="margin-left:2em" onclick="rowClicked(this)" />
            <span>
      <!-- 0-默认列表 1-搜索匹配列表 其他-搜索未匹配列表 -->
            <a href="goods.php?act=list&cat_id=21&supp=-1">凤梨蓝莓</a>
            </span>
        </td>
    <td width="10%">0</td>
    <td width="10%"><span onclick="listTable.edit(this, 'edit_measure_unit', 21)"><!--  -->&nbsp;&nbsp;&nbsp;&nbsp;<!--  --></span></td>
    <td width="10%"><img src="images/no.gif" onclick="listTable.toggle(this, 'toggle_show_in_nav', 21)" /></td>
    <td width="10%"><img src="images/yes.gif" onclick="listTable.toggle(this, 'toggle_is_show', 21)" /></td>
    <td><span onclick="listTable.edit(this, 'edit_grade', 21)">0</span></td>
    <td width="10%" align="right"><span onclick="listTable.edit(this, 'edit_sort_order', 21)">50</span></td>
    <td width="24%" align="center">
      <a href="category.php?act=move&cat_id=21">转移商品</a> |
      <a href="category.php?act=edit&amp;cat_id=21">编辑</a> |
      <a href="javascript:;" onclick="listTable.remove(21, '您确认要删除这条记录吗?')" title="移除">移除</a>
	  <!--代码修改_start Byjdy-->
	  	  <!--代码修改_end Byjdy-->
    </td>
  </tr>
    <tr align="center" class="2" id="2_18">
    <td align="left" class="first-cell" >
            <img src="images/menu_minus.gif" id="icon_2_18" width="9" height="9" border="0" style="margin-left:2em" onclick="rowClicked(this)" />
            <span>
      <!-- 0-默认列表 1-搜索匹配列表 其他-搜索未匹配列表 -->
            <a href="goods.php?act=list&cat_id=18&supp=-1">龙眼荔枝</a>
            </span>
        </td>
    <td width="10%">0</td>
    <td width="10%"><span onclick="listTable.edit(this, 'edit_measure_unit', 18)"><!--  -->&nbsp;&nbsp;&nbsp;&nbsp;<!--  --></span></td>
    <td width="10%"><img src="images/no.gif" onclick="listTable.toggle(this, 'toggle_show_in_nav', 18)" /></td>
    <td width="10%"><img src="images/yes.gif" onclick="listTable.toggle(this, 'toggle_is_show', 18)" /></td>
    <td><span onclick="listTable.edit(this, 'edit_grade', 18)">0</span></td>
    <td width="10%" align="right"><span onclick="listTable.edit(this, 'edit_sort_order', 18)">50</span></td>
    <td width="24%" align="center">
      <a href="category.php?act=move&cat_id=18">转移商品</a> |
      <a href="category.php?act=edit&amp;cat_id=18">编辑</a> |
      <a href="javascript:;" onclick="listTable.remove(18, '您确认要删除这条记录吗?')" title="移除">移除</a>
	  <!--代码修改_start Byjdy-->
	  	  <!--代码修改_end Byjdy-->
    </td>
  </tr>
    <tr align="center" class="2" id="2_15">
    <td align="left" class="first-cell" >
            <img src="images/menu_minus.gif" id="icon_2_15" width="9" height="9" border="0" style="margin-left:2em" onclick="rowClicked(this)" />
            <span>
      <!-- 0-默认列表 1-搜索匹配列表 其他-搜索未匹配列表 -->
            <a href="goods.php?act=list&cat_id=15&supp=-1">释迦芭乐</a>
            </span>
        </td>
    <td width="10%">0</td>
    <td width="10%"><span onclick="listTable.edit(this, 'edit_measure_unit', 15)"><!--  -->&nbsp;&nbsp;&nbsp;&nbsp;<!--  --></span></td>
    <td width="10%"><img src="images/no.gif" onclick="listTable.toggle(this, 'toggle_show_in_nav', 15)" /></td>
    <td width="10%"><img src="images/yes.gif" onclick="listTable.toggle(this, 'toggle_is_show', 15)" /></td>
    <td><span onclick="listTable.edit(this, 'edit_grade', 15)">0</span></td>
    <td width="10%" align="right"><span onclick="listTable.edit(this, 'edit_sort_order', 15)">50</span></td>
    <td width="24%" align="center">
      <a href="category.php?act=move&cat_id=15">转移商品</a> |
      <a href="category.php?act=edit&amp;cat_id=15">编辑</a> |
      <a href="javascript:;" onclick="listTable.remove(15, '您确认要删除这条记录吗?')" title="移除">移除</a>
	  <!--代码修改_start Byjdy-->
	  	  <!--代码修改_end Byjdy-->
    </td>
  </tr>
    <tr align="center" class="2" id="2_22">
    <td align="left" class="first-cell" >
            <img src="images/menu_minus.gif" id="icon_2_22" width="9" height="9" border="0" style="margin-left:2em" onclick="rowClicked(this)" />
            <span>
      <!-- 0-默认列表 1-搜索匹配列表 其他-搜索未匹配列表 -->
            <a href="goods.php?act=list&cat_id=22&supp=-1">榴莲山竹</a>
            </span>
        </td>
    <td width="10%">0</td>
    <td width="10%"><span onclick="listTable.edit(this, 'edit_measure_unit', 22)"><!--  -->&nbsp;&nbsp;&nbsp;&nbsp;<!--  --></span></td>
    <td width="10%"><img src="images/no.gif" onclick="listTable.toggle(this, 'toggle_show_in_nav', 22)" /></td>
    <td width="10%"><img src="images/yes.gif" onclick="listTable.toggle(this, 'toggle_is_show', 22)" /></td>
    <td><span onclick="listTable.edit(this, 'edit_grade', 22)">0</span></td>
    <td width="10%" align="right"><span onclick="listTable.edit(this, 'edit_sort_order', 22)">50</span></td>
    <td width="24%" align="center">
      <a href="category.php?act=move&cat_id=22">转移商品</a> |
      <a href="category.php?act=edit&amp;cat_id=22">编辑</a> |
      <a href="javascript:;" onclick="listTable.remove(22, '您确认要删除这条记录吗?')" title="移除">移除</a>
	  <!--代码修改_start Byjdy-->
	  	  <!--代码修改_end Byjdy-->
    </td>
  </tr>
    <tr align="center" class="2" id="2_19">
    <td align="left" class="first-cell" >
            <img src="images/menu_minus.gif" id="icon_2_19" width="9" height="9" border="0" style="margin-left:2em" onclick="rowClicked(this)" />
            <span>
      <!-- 0-默认列表 1-搜索匹配列表 其他-搜索未匹配列表 -->
            <a href="goods.php?act=list&cat_id=19&supp=-1">提子葡萄</a>
            </span>
        </td>
    <td width="10%">0</td>
    <td width="10%"><span onclick="listTable.edit(this, 'edit_measure_unit', 19)"><!--  -->&nbsp;&nbsp;&nbsp;&nbsp;<!--  --></span></td>
    <td width="10%"><img src="images/no.gif" onclick="listTable.toggle(this, 'toggle_show_in_nav', 19)" /></td>
    <td width="10%"><img src="images/yes.gif" onclick="listTable.toggle(this, 'toggle_is_show', 19)" /></td>
    <td><span onclick="listTable.edit(this, 'edit_grade', 19)">0</span></td>
    <td width="10%" align="right"><span onclick="listTable.edit(this, 'edit_sort_order', 19)">50</span></td>
    <td width="24%" align="center">
      <a href="category.php?act=move&cat_id=19">转移商品</a> |
      <a href="category.php?act=edit&amp;cat_id=19">编辑</a> |
      <a href="javascript:;" onclick="listTable.remove(19, '您确认要删除这条记录吗?')" title="移除">移除</a>
	  <!--代码修改_start Byjdy-->
	  	  <!--代码修改_end Byjdy-->
    </td>
  </tr>
    <tr align="center" class="2" id="2_16">
    <td align="left" class="first-cell" >
            <img src="images/menu_minus.gif" id="icon_2_16" width="9" height="9" border="0" style="margin-left:2em" onclick="rowClicked(this)" />
            <span>
      <!-- 0-默认列表 1-搜索匹配列表 其他-搜索未匹配列表 -->
            <a href="goods.php?act=list&cat_id=16&supp=-1">樱桃车厘子</a>
            </span>
        </td>
    <td width="10%">0</td>
    <td width="10%"><span onclick="listTable.edit(this, 'edit_measure_unit', 16)"><!--  -->&nbsp;&nbsp;&nbsp;&nbsp;<!--  --></span></td>
    <td width="10%"><img src="images/no.gif" onclick="listTable.toggle(this, 'toggle_show_in_nav', 16)" /></td>
    <td width="10%"><img src="images/yes.gif" onclick="listTable.toggle(this, 'toggle_is_show', 16)" /></td>
    <td><span onclick="listTable.edit(this, 'edit_grade', 16)">0</span></td>
    <td width="10%" align="right"><span onclick="listTable.edit(this, 'edit_sort_order', 16)">50</span></td>
    <td width="24%" align="center">
      <a href="category.php?act=move&cat_id=16">转移商品</a> |
      <a href="category.php?act=edit&amp;cat_id=16">编辑</a> |
      <a href="javascript:;" onclick="listTable.remove(16, '您确认要删除这条记录吗?')" title="移除">移除</a>
	  <!--代码修改_start Byjdy-->
	  	  <!--代码修改_end Byjdy-->
    </td>
  </tr>
    <tr align="center" class="1" id="1_13">
    <td align="left" class="first-cell" >
            <img src="images/menu_minus.gif" id="icon_1_13" width="9" height="9" border="0" style="margin-left:1em" onclick="rowClicked(this)" />
            <span>
      <!-- 0-默认列表 1-搜索匹配列表 其他-搜索未匹配列表 -->
            <a href="goods.php?act=list&cat_id=13&supp=-1">糖果巧克力</a>
            </span>
        </td>
    <td width="10%">8</td>
    <td width="10%"><span onclick="listTable.edit(this, 'edit_measure_unit', 13)"><!--  -->&nbsp;&nbsp;&nbsp;&nbsp;<!--  --></span></td>
    <td width="10%"><img src="images/no.gif" onclick="listTable.toggle(this, 'toggle_show_in_nav', 13)" /></td>
    <td width="10%"><img src="images/yes.gif" onclick="listTable.toggle(this, 'toggle_is_show', 13)" /></td>
    <td><span onclick="listTable.edit(this, 'edit_grade', 13)">0</span></td>
    <td width="10%" align="right"><span onclick="listTable.edit(this, 'edit_sort_order', 13)">11</span></td>
    <td width="24%" align="center">
      <a href="category.php?act=move&cat_id=13">转移商品</a> |
      <a href="category.php?act=edit&amp;cat_id=13">编辑</a> |
      <a href="javascript:;" onclick="listTable.remove(13, '您确认要删除这条记录吗?')" title="移除">移除</a>
	  <!--代码修改_start Byjdy-->
	  	  <!--代码修改_end Byjdy-->
    </td>
  </tr>
    <tr align="center" class="2" id="2_24">
    <td align="left" class="first-cell" >
            <img src="images/menu_minus.gif" id="icon_2_24" width="9" height="9" border="0" style="margin-left:2em" onclick="rowClicked(this)" />
            <span>
      <!-- 0-默认列表 1-搜索匹配列表 其他-搜索未匹配列表 -->
            <a href="goods.php?act=list&cat_id=24&supp=-1">巧克力</a>
            </span>
        </td>
    <td width="10%">0</td>
    <td width="10%"><span onclick="listTable.edit(this, 'edit_measure_unit', 24)"><!--  -->&nbsp;&nbsp;&nbsp;&nbsp;<!--  --></span></td>
    <td width="10%"><img src="images/no.gif" onclick="listTable.toggle(this, 'toggle_show_in_nav', 24)" /></td>
    <td width="10%"><img src="images/yes.gif" onclick="listTable.toggle(this, 'toggle_is_show', 24)" /></td>
    <td><span onclick="listTable.edit(this, 'edit_grade', 24)">0</span></td>
    <td width="10%" align="right"><span onclick="listTable.edit(this, 'edit_sort_order', 24)">50</span></td>
    <td width="24%" align="center">
      <a href="category.php?act=move&cat_id=24">转移商品</a> |
      <a href="category.php?act=edit&amp;cat_id=24">编辑</a> |
      <a href="javascript:;" onclick="listTable.remove(24, '您确认要删除这条记录吗?')" title="移除">移除</a>
	  <!--代码修改_start Byjdy-->
	  	  <!--代码修改_end Byjdy-->
    </td>
  </tr>
    <tr align="center" class="2" id="2_28">
    <td align="left" class="first-cell" >
            <img src="images/menu_minus.gif" id="icon_2_28" width="9" height="9" border="0" style="margin-left:2em" onclick="rowClicked(this)" />
            <span>
      <!-- 0-默认列表 1-搜索匹配列表 其他-搜索未匹配列表 -->
            <a href="goods.php?act=list&cat_id=28&supp=-1">奶糖</a>
            </span>
        </td>
    <td width="10%">0</td>
    <td width="10%"><span onclick="listTable.edit(this, 'edit_measure_unit', 28)"><!--  -->&nbsp;&nbsp;&nbsp;&nbsp;<!--  --></span></td>
    <td width="10%"><img src="images/no.gif" onclick="listTable.toggle(this, 'toggle_show_in_nav', 28)" /></td>
    <td width="10%"><img src="images/yes.gif" onclick="listTable.toggle(this, 'toggle_is_show', 28)" /></td>
    <td><span onclick="listTable.edit(this, 'edit_grade', 28)">0</span></td>
    <td width="10%" align="right"><span onclick="listTable.edit(this, 'edit_sort_order', 28)">50</span></td>
    <td width="24%" align="center">
      <a href="category.php?act=move&cat_id=28">转移商品</a> |
      <a href="category.php?act=edit&amp;cat_id=28">编辑</a> |
      <a href="javascript:;" onclick="listTable.remove(28, '您确认要删除这条记录吗?')" title="移除">移除</a>
	  <!--代码修改_start Byjdy-->
	  	  <!--代码修改_end Byjdy-->
    </td>
  </tr>
    <tr align="center" class="2" id="2_25">
    <td align="left" class="first-cell" >
            <img src="images/menu_minus.gif" id="icon_2_25" width="9" height="9" border="0" style="margin-left:2em" onclick="rowClicked(this)" />
            <span>
      <!-- 0-默认列表 1-搜索匹配列表 其他-搜索未匹配列表 -->
            <a href="goods.php?act=list&cat_id=25&supp=-1">口香糖</a>
            </span>
        </td>
    <td width="10%">0</td>
    <td width="10%"><span onclick="listTable.edit(this, 'edit_measure_unit', 25)"><!--  -->&nbsp;&nbsp;&nbsp;&nbsp;<!--  --></span></td>
    <td width="10%"><img src="images/no.gif" onclick="listTable.toggle(this, 'toggle_show_in_nav', 25)" /></td>
    <td width="10%"><img src="images/yes.gif" onclick="listTable.toggle(this, 'toggle_is_show', 25)" /></td>
    <td><span onclick="listTable.edit(this, 'edit_grade', 25)">0</span></td>
    <td width="10%" align="right"><span onclick="listTable.edit(this, 'edit_sort_order', 25)">50</span></td>
    <td width="24%" align="center">
      <a href="category.php?act=move&cat_id=25">转移商品</a> |
      <a href="category.php?act=edit&amp;cat_id=25">编辑</a> |
      <a href="javascript:;" onclick="listTable.remove(25, '您确认要删除这条记录吗?')" title="移除">移除</a>
	  <!--代码修改_start Byjdy-->
	  	  <!--代码修改_end Byjdy-->
    </td>
  </tr>
    <tr align="center" class="2" id="2_29">
    <td align="left" class="first-cell" >
            <img src="images/menu_minus.gif" id="icon_2_29" width="9" height="9" border="0" style="margin-left:2em" onclick="rowClicked(this)" />
            <span>
      <!-- 0-默认列表 1-搜索匹配列表 其他-搜索未匹配列表 -->
            <a href="goods.php?act=list&cat_id=29&supp=-1">QQ糖</a>
            </span>
        </td>
    <td width="10%">0</td>
    <td width="10%"><span onclick="listTable.edit(this, 'edit_measure_unit', 29)"><!--  -->&nbsp;&nbsp;&nbsp;&nbsp;<!--  --></span></td>
    <td width="10%"><img src="images/no.gif" onclick="listTable.toggle(this, 'toggle_show_in_nav', 29)" /></td>
    <td width="10%"><img src="images/yes.gif" onclick="listTable.toggle(this, 'toggle_is_show', 29)" /></td>
    <td><span onclick="listTable.edit(this, 'edit_grade', 29)">0</span></td>
    <td width="10%" align="right"><span onclick="listTable.edit(this, 'edit_sort_order', 29)">50</span></td>
    <td width="24%" align="center">
      <a href="category.php?act=move&cat_id=29">转移商品</a> |
      <a href="category.php?act=edit&amp;cat_id=29">编辑</a> |
      <a href="javascript:;" onclick="listTable.remove(29, '您确认要删除这条记录吗?')" title="移除">移除</a>
	  <!--代码修改_start Byjdy-->
	  	  <!--代码修改_end Byjdy-->
    </td>
  </tr>
    <tr align="center" class="2" id="2_26">
    <td align="left" class="first-cell" >
            <img src="images/menu_minus.gif" id="icon_2_26" width="9" height="9" border="0" style="margin-left:2em" onclick="rowClicked(this)" />
            <span>
      <!-- 0-默认列表 1-搜索匹配列表 其他-搜索未匹配列表 -->
            <a href="goods.php?act=list&cat_id=26&supp=-1">棒棒糖</a>
            </span>
        </td>
    <td width="10%">0</td>
    <td width="10%"><span onclick="listTable.edit(this, 'edit_measure_unit', 26)"><!--  -->&nbsp;&nbsp;&nbsp;&nbsp;<!--  --></span></td>
    <td width="10%"><img src="images/no.gif" onclick="listTable.toggle(this, 'toggle_show_in_nav', 26)" /></td>
    <td width="10%"><img src="images/yes.gif" onclick="listTable.toggle(this, 'toggle_is_show', 26)" /></td>
    <td><span onclick="listTable.edit(this, 'edit_grade', 26)">0</span></td>
    <td width="10%" align="right"><span onclick="listTable.edit(this, 'edit_sort_order', 26)">50</span></td>
    <td width="24%" align="center">
      <a href="category.php?act=move&cat_id=26">转移商品</a> |
      <a href="category.php?act=edit&amp;cat_id=26">编辑</a> |
      <a href="javascript:;" onclick="listTable.remove(26, '您确认要删除这条记录吗?')" title="移除">移除</a>
	  <!--代码修改_start Byjdy-->
	  	  <!--代码修改_end Byjdy-->
    </td>
  </tr>
    <tr align="center" class="2" id="2_30">
    <td align="left" class="first-cell" >
            <img src="images/menu_minus.gif" id="icon_2_30" width="9" height="9" border="0" style="margin-left:2em" onclick="rowClicked(this)" />
            <span>
      <!-- 0-默认列表 1-搜索匹配列表 其他-搜索未匹配列表 -->
            <a href="goods.php?act=list&cat_id=30&supp=-1">果冻</a>
            </span>
        </td>
    <td width="10%">0</td>
    <td width="10%"><span onclick="listTable.edit(this, 'edit_measure_unit', 30)"><!--  -->&nbsp;&nbsp;&nbsp;&nbsp;<!--  --></span></td>
    <td width="10%"><img src="images/no.gif" onclick="listTable.toggle(this, 'toggle_show_in_nav', 30)" /></td>
    <td width="10%"><img src="images/yes.gif" onclick="listTable.toggle(this, 'toggle_is_show', 30)" /></td>
    <td><span onclick="listTable.edit(this, 'edit_grade', 30)">0</span></td>
    <td width="10%" align="right"><span onclick="listTable.edit(this, 'edit_sort_order', 30)">50</span></td>
    <td width="24%" align="center">
      <a href="category.php?act=move&cat_id=30">转移商品</a> |
      <a href="category.php?act=edit&amp;cat_id=30">编辑</a> |
      <a href="javascript:;" onclick="listTable.remove(30, '您确认要删除这条记录吗?')" title="移除">移除</a>
	  <!--代码修改_start Byjdy-->
	  	  <!--代码修改_end Byjdy-->
    </td>
  </tr>
    <tr align="center" class="2" id="2_27">
    <td align="left" class="first-cell" >
            <img src="images/menu_minus.gif" id="icon_2_27" width="9" height="9" border="0" style="margin-left:2em" onclick="rowClicked(this)" />
            <span>
      <!-- 0-默认列表 1-搜索匹配列表 其他-搜索未匹配列表 -->
            <a href="goods.php?act=list&cat_id=27&supp=-1">软糖</a>
            </span>
        </td>
    <td width="10%">0</td>
    <td width="10%"><span onclick="listTable.edit(this, 'edit_measure_unit', 27)"><!--  -->&nbsp;&nbsp;&nbsp;&nbsp;<!--  --></span></td>
    <td width="10%"><img src="images/no.gif" onclick="listTable.toggle(this, 'toggle_show_in_nav', 27)" /></td>
    <td width="10%"><img src="images/yes.gif" onclick="listTable.toggle(this, 'toggle_is_show', 27)" /></td>
    <td><span onclick="listTable.edit(this, 'edit_grade', 27)">0</span></td>
    <td width="10%" align="right"><span onclick="listTable.edit(this, 'edit_sort_order', 27)">50</span></td>
    <td width="24%" align="center">
      <a href="category.php?act=move&cat_id=27">转移商品</a> |
      <a href="category.php?act=edit&amp;cat_id=27">编辑</a> |
      <a href="javascript:;" onclick="listTable.remove(27, '您确认要删除这条记录吗?')" title="移除">移除</a>
	  <!--代码修改_start Byjdy-->
	  	  <!--代码修改_end Byjdy-->
    </td>
  </tr>
    <tr align="center" class="1" id="1_9">
    <td align="left" class="first-cell" >
            <img src="images/menu_minus.gif" id="icon_1_9" width="9" height="9" border="0" style="margin-left:1em" onclick="rowClicked(this)" />
            <span>
      <!-- 0-默认列表 1-搜索匹配列表 其他-搜索未匹配列表 -->
            <a href="goods.php?act=list&cat_id=9&supp=-1">牛奶乳品</a>
            </span>
        </td>
    <td width="10%">8</td>
    <td width="10%"><span onclick="listTable.edit(this, 'edit_measure_unit', 9)"><!--  -->&nbsp;&nbsp;&nbsp;&nbsp;<!--  --></span></td>
    <td width="10%"><img src="images/no.gif" onclick="listTable.toggle(this, 'toggle_show_in_nav', 9)" /></td>
    <td width="10%"><img src="images/yes.gif" onclick="listTable.toggle(this, 'toggle_is_show', 9)" /></td>
    <td><span onclick="listTable.edit(this, 'edit_grade', 9)">0</span></td>
    <td width="10%" align="right"><span onclick="listTable.edit(this, 'edit_sort_order', 9)">12</span></td>
    <td width="24%" align="center">
      <a href="category.php?act=move&cat_id=9">转移商品</a> |
      <a href="category.php?act=edit&amp;cat_id=9">编辑</a> |
      <a href="javascript:;" onclick="listTable.remove(9, '您确认要删除这条记录吗?')" title="移除">移除</a>
	  <!--代码修改_start Byjdy-->
	  	  <!--代码修改_end Byjdy-->
    </td>
  </tr>
    <tr align="center" class="2" id="2_37">
    <td align="left" class="first-cell" >
            <img src="images/menu_minus.gif" id="icon_2_37" width="9" height="9" border="0" style="margin-left:2em" onclick="rowClicked(this)" />
            <span>
      <!-- 0-默认列表 1-搜索匹配列表 其他-搜索未匹配列表 -->
            <a href="goods.php?act=list&cat_id=37&supp=-1">全脂奶</a>
            </span>
        </td>
    <td width="10%">0</td>
    <td width="10%"><span onclick="listTable.edit(this, 'edit_measure_unit', 37)"><!--  -->&nbsp;&nbsp;&nbsp;&nbsp;<!--  --></span></td>
    <td width="10%"><img src="images/no.gif" onclick="listTable.toggle(this, 'toggle_show_in_nav', 37)" /></td>
    <td width="10%"><img src="images/yes.gif" onclick="listTable.toggle(this, 'toggle_is_show', 37)" /></td>
    <td><span onclick="listTable.edit(this, 'edit_grade', 37)">0</span></td>
    <td width="10%" align="right"><span onclick="listTable.edit(this, 'edit_sort_order', 37)">50</span></td>
    <td width="24%" align="center">
      <a href="category.php?act=move&cat_id=37">转移商品</a> |
      <a href="category.php?act=edit&amp;cat_id=37">编辑</a> |
      <a href="javascript:;" onclick="listTable.remove(37, '您确认要删除这条记录吗?')" title="移除">移除</a>
	  <!--代码修改_start Byjdy-->
	  	  <!--代码修改_end Byjdy-->
    </td>
  </tr>
    <tr align="center" class="2" id="2_34">
    <td align="left" class="first-cell" >
            <img src="images/menu_minus.gif" id="icon_2_34" width="9" height="9" border="0" style="margin-left:2em" onclick="rowClicked(this)" />
            <span>
      <!-- 0-默认列表 1-搜索匹配列表 其他-搜索未匹配列表 -->
            <a href="goods.php?act=list&cat_id=34&supp=-1">酸奶</a>
            </span>
        </td>
    <td width="10%">0</td>
    <td width="10%"><span onclick="listTable.edit(this, 'edit_measure_unit', 34)"><!--  -->&nbsp;&nbsp;&nbsp;&nbsp;<!--  --></span></td>
    <td width="10%"><img src="images/no.gif" onclick="listTable.toggle(this, 'toggle_show_in_nav', 34)" /></td>
    <td width="10%"><img src="images/yes.gif" onclick="listTable.toggle(this, 'toggle_is_show', 34)" /></td>
    <td><span onclick="listTable.edit(this, 'edit_grade', 34)">0</span></td>
    <td width="10%" align="right"><span onclick="listTable.edit(this, 'edit_sort_order', 34)">50</span></td>
    <td width="24%" align="center">
      <a href="category.php?act=move&cat_id=34">转移商品</a> |
      <a href="category.php?act=edit&amp;cat_id=34">编辑</a> |
      <a href="javascript:;" onclick="listTable.remove(34, '您确认要删除这条记录吗?')" title="移除">移除</a>
	  <!--代码修改_start Byjdy-->
	  	  <!--代码修改_end Byjdy-->
    </td>
  </tr>
    <tr align="center" class="2" id="2_31">
    <td align="left" class="first-cell" >
            <img src="images/menu_minus.gif" id="icon_2_31" width="9" height="9" border="0" style="margin-left:2em" onclick="rowClicked(this)" />
            <span>
      <!-- 0-默认列表 1-搜索匹配列表 其他-搜索未匹配列表 -->
            <a href="goods.php?act=list&cat_id=31&supp=-1">常温奶</a>
            </span>
        </td>
    <td width="10%">0</td>
    <td width="10%"><span onclick="listTable.edit(this, 'edit_measure_unit', 31)"><!--  -->&nbsp;&nbsp;&nbsp;&nbsp;<!--  --></span></td>
    <td width="10%"><img src="images/no.gif" onclick="listTable.toggle(this, 'toggle_show_in_nav', 31)" /></td>
    <td width="10%"><img src="images/yes.gif" onclick="listTable.toggle(this, 'toggle_is_show', 31)" /></td>
    <td><span onclick="listTable.edit(this, 'edit_grade', 31)">0</span></td>
    <td width="10%" align="right"><span onclick="listTable.edit(this, 'edit_sort_order', 31)">50</span></td>
    <td width="24%" align="center">
      <a href="category.php?act=move&cat_id=31">转移商品</a> |
      <a href="category.php?act=edit&amp;cat_id=31">编辑</a> |
      <a href="javascript:;" onclick="listTable.remove(31, '您确认要删除这条记录吗?')" title="移除">移除</a>
	  <!--代码修改_start Byjdy-->
	  	  <!--代码修改_end Byjdy-->
    </td>
  </tr>
    <tr align="center" class="2" id="2_38">
    <td align="left" class="first-cell" >
            <img src="images/menu_minus.gif" id="icon_2_38" width="9" height="9" border="0" style="margin-left:2em" onclick="rowClicked(this)" />
            <span>
      <!-- 0-默认列表 1-搜索匹配列表 其他-搜索未匹配列表 -->
            <a href="goods.php?act=list&cat_id=38&supp=-1">成人奶粉</a>
            </span>
        </td>
    <td width="10%">0</td>
    <td width="10%"><span onclick="listTable.edit(this, 'edit_measure_unit', 38)"><!--  -->&nbsp;&nbsp;&nbsp;&nbsp;<!--  --></span></td>
    <td width="10%"><img src="images/no.gif" onclick="listTable.toggle(this, 'toggle_show_in_nav', 38)" /></td>
    <td width="10%"><img src="images/yes.gif" onclick="listTable.toggle(this, 'toggle_is_show', 38)" /></td>
    <td><span onclick="listTable.edit(this, 'edit_grade', 38)">0</span></td>
    <td width="10%" align="right"><span onclick="listTable.edit(this, 'edit_sort_order', 38)">50</span></td>
    <td width="24%" align="center">
      <a href="category.php?act=move&cat_id=38">转移商品</a> |
      <a href="category.php?act=edit&amp;cat_id=38">编辑</a> |
      <a href="javascript:;" onclick="listTable.remove(38, '您确认要删除这条记录吗?')" title="移除">移除</a>
	  <!--代码修改_start Byjdy-->
	  	  <!--代码修改_end Byjdy-->
    </td>
  </tr>
    <tr align="center" class="2" id="2_35">
    <td align="left" class="first-cell" >
            <img src="images/menu_minus.gif" id="icon_2_35" width="9" height="9" border="0" style="margin-left:2em" onclick="rowClicked(this)" />
            <span>
      <!-- 0-默认列表 1-搜索匹配列表 其他-搜索未匹配列表 -->
            <a href="goods.php?act=list&cat_id=35&supp=-1">豆奶</a>
            </span>
        </td>
    <td width="10%">0</td>
    <td width="10%"><span onclick="listTable.edit(this, 'edit_measure_unit', 35)"><!--  -->&nbsp;&nbsp;&nbsp;&nbsp;<!--  --></span></td>
    <td width="10%"><img src="images/no.gif" onclick="listTable.toggle(this, 'toggle_show_in_nav', 35)" /></td>
    <td width="10%"><img src="images/yes.gif" onclick="listTable.toggle(this, 'toggle_is_show', 35)" /></td>
    <td><span onclick="listTable.edit(this, 'edit_grade', 35)">0</span></td>
    <td width="10%" align="right"><span onclick="listTable.edit(this, 'edit_sort_order', 35)">50</span></td>
    <td width="24%" align="center">
      <a href="category.php?act=move&cat_id=35">转移商品</a> |
      <a href="category.php?act=edit&amp;cat_id=35">编辑</a> |
      <a href="javascript:;" onclick="listTable.remove(35, '您确认要删除这条记录吗?')" title="移除">移除</a>
	  <!--代码修改_start Byjdy-->
	  	  <!--代码修改_end Byjdy-->
    </td>
  </tr>
    <tr align="center" class="2" id="2_32">
    <td align="left" class="first-cell" >
            <img src="images/menu_minus.gif" id="icon_2_32" width="9" height="9" border="0" style="margin-left:2em" onclick="rowClicked(this)" />
            <span>
      <!-- 0-默认列表 1-搜索匹配列表 其他-搜索未匹配列表 -->
            <a href="goods.php?act=list&cat_id=32&supp=-1">乳饮料</a>
            </span>
        </td>
    <td width="10%">0</td>
    <td width="10%"><span onclick="listTable.edit(this, 'edit_measure_unit', 32)"><!--  -->&nbsp;&nbsp;&nbsp;&nbsp;<!--  --></span></td>
    <td width="10%"><img src="images/no.gif" onclick="listTable.toggle(this, 'toggle_show_in_nav', 32)" /></td>
    <td width="10%"><img src="images/yes.gif" onclick="listTable.toggle(this, 'toggle_is_show', 32)" /></td>
    <td><span onclick="listTable.edit(this, 'edit_grade', 32)">0</span></td>
    <td width="10%" align="right"><span onclick="listTable.edit(this, 'edit_sort_order', 32)">50</span></td>
    <td width="24%" align="center">
      <a href="category.php?act=move&cat_id=32">转移商品</a> |
      <a href="category.php?act=edit&amp;cat_id=32">编辑</a> |
      <a href="javascript:;" onclick="listTable.remove(32, '您确认要删除这条记录吗?')" title="移除">移除</a>
	  <!--代码修改_start Byjdy-->
	  	  <!--代码修改_end Byjdy-->
    </td>
  </tr>
    <tr align="center" class="2" id="2_36">
    <td align="left" class="first-cell" >
            <img src="images/menu_minus.gif" id="icon_2_36" width="9" height="9" border="0" style="margin-left:2em" onclick="rowClicked(this)" />
            <span>
      <!-- 0-默认列表 1-搜索匹配列表 其他-搜索未匹配列表 -->
            <a href="goods.php?act=list&cat_id=36&supp=-1">低脂奶</a>
            </span>
        </td>
    <td width="10%">0</td>
    <td width="10%"><span onclick="listTable.edit(this, 'edit_measure_unit', 36)"><!--  -->&nbsp;&nbsp;&nbsp;&nbsp;<!--  --></span></td>
    <td width="10%"><img src="images/no.gif" onclick="listTable.toggle(this, 'toggle_show_in_nav', 36)" /></td>
    <td width="10%"><img src="images/yes.gif" onclick="listTable.toggle(this, 'toggle_is_show', 36)" /></td>
    <td><span onclick="listTable.edit(this, 'edit_grade', 36)">0</span></td>
    <td width="10%" align="right"><span onclick="listTable.edit(this, 'edit_sort_order', 36)">50</span></td>
    <td width="24%" align="center">
      <a href="category.php?act=move&cat_id=36">转移商品</a> |
      <a href="category.php?act=edit&amp;cat_id=36">编辑</a> |
      <a href="javascript:;" onclick="listTable.remove(36, '您确认要删除这条记录吗?')" title="移除">移除</a>
	  <!--代码修改_start Byjdy-->
	  	  <!--代码修改_end Byjdy-->
    </td>
  </tr>
    <tr align="center" class="2" id="2_33">
    <td align="left" class="first-cell" >
            <img src="images/menu_minus.gif" id="icon_2_33" width="9" height="9" border="0" style="margin-left:2em" onclick="rowClicked(this)" />
            <span>
      <!-- 0-默认列表 1-搜索匹配列表 其他-搜索未匹配列表 -->
            <a href="goods.php?act=list&cat_id=33&supp=-1">儿童奶</a>
            </span>
        </td>
    <td width="10%">0</td>
    <td width="10%"><span onclick="listTable.edit(this, 'edit_measure_unit', 33)"><!--  -->&nbsp;&nbsp;&nbsp;&nbsp;<!--  --></span></td>
    <td width="10%"><img src="images/no.gif" onclick="listTable.toggle(this, 'toggle_show_in_nav', 33)" /></td>
    <td width="10%"><img src="images/yes.gif" onclick="listTable.toggle(this, 'toggle_is_show', 33)" /></td>
    <td><span onclick="listTable.edit(this, 'edit_grade', 33)">0</span></td>
    <td width="10%" align="right"><span onclick="listTable.edit(this, 'edit_sort_order', 33)">50</span></td>
    <td width="24%" align="center">
      <a href="category.php?act=move&cat_id=33">转移商品</a> |
      <a href="category.php?act=edit&amp;cat_id=33">编辑</a> |
      <a href="javascript:;" onclick="listTable.remove(33, '您确认要删除这条记录吗?')" title="移除">移除</a>
	  <!--代码修改_start Byjdy-->
	  	  <!--代码修改_end Byjdy-->
    </td>
  </tr>
    <tr align="center" class="1" id="1_10">
    <td align="left" class="first-cell" >
            <img src="images/menu_minus.gif" id="icon_1_10" width="9" height="9" border="0" style="margin-left:1em" onclick="rowClicked(this)" />
            <span>
      <!-- 0-默认列表 1-搜索匹配列表 其他-搜索未匹配列表 -->
            <a href="goods.php?act=list&cat_id=10&supp=-1">坚果炒货</a>
            </span>
        </td>
    <td width="10%">0</td>
    <td width="10%"><span onclick="listTable.edit(this, 'edit_measure_unit', 10)"><!--  -->&nbsp;&nbsp;&nbsp;&nbsp;<!--  --></span></td>
    <td width="10%"><img src="images/no.gif" onclick="listTable.toggle(this, 'toggle_show_in_nav', 10)" /></td>
    <td width="10%"><img src="images/yes.gif" onclick="listTable.toggle(this, 'toggle_is_show', 10)" /></td>
    <td><span onclick="listTable.edit(this, 'edit_grade', 10)">0</span></td>
    <td width="10%" align="right"><span onclick="listTable.edit(this, 'edit_sort_order', 10)">13</span></td>
    <td width="24%" align="center">
      <a href="category.php?act=move&cat_id=10">转移商品</a> |
      <a href="category.php?act=edit&amp;cat_id=10">编辑</a> |
      <a href="javascript:;" onclick="listTable.remove(10, '您确认要删除这条记录吗?')" title="移除">移除</a>
	  <!--代码修改_start Byjdy-->
	  	  <!--代码修改_end Byjdy-->
    </td>
  </tr>
    <tr align="center" class="2" id="2_40">
    <td align="left" class="first-cell" >
            <img src="images/menu_minus.gif" id="icon_2_40" width="9" height="9" border="0" style="margin-left:2em" onclick="rowClicked(this)" />
            <span>
      <!-- 0-默认列表 1-搜索匹配列表 其他-搜索未匹配列表 -->
            <a href="goods.php?act=list&cat_id=40&supp=-1">夏威夷果</a>
            </span>
        </td>
    <td width="10%">0</td>
    <td width="10%"><span onclick="listTable.edit(this, 'edit_measure_unit', 40)"><!--  -->&nbsp;&nbsp;&nbsp;&nbsp;<!--  --></span></td>
    <td width="10%"><img src="images/no.gif" onclick="listTable.toggle(this, 'toggle_show_in_nav', 40)" /></td>
    <td width="10%"><img src="images/yes.gif" onclick="listTable.toggle(this, 'toggle_is_show', 40)" /></td>
    <td><span onclick="listTable.edit(this, 'edit_grade', 40)">0</span></td>
    <td width="10%" align="right"><span onclick="listTable.edit(this, 'edit_sort_order', 40)">50</span></td>
    <td width="24%" align="center">
      <a href="category.php?act=move&cat_id=40">转移商品</a> |
      <a href="category.php?act=edit&amp;cat_id=40">编辑</a> |
      <a href="javascript:;" onclick="listTable.remove(40, '您确认要删除这条记录吗?')" title="移除">移除</a>
	  <!--代码修改_start Byjdy-->
	  	  <!--代码修改_end Byjdy-->
    </td>
  </tr>
    <tr align="center" class="2" id="2_44">
    <td align="left" class="first-cell" >
            <img src="images/menu_minus.gif" id="icon_2_44" width="9" height="9" border="0" style="margin-left:2em" onclick="rowClicked(this)" />
            <span>
      <!-- 0-默认列表 1-搜索匹配列表 其他-搜索未匹配列表 -->
            <a href="goods.php?act=list&cat_id=44&supp=-1">瓜子</a>
            </span>
        </td>
    <td width="10%">0</td>
    <td width="10%"><span onclick="listTable.edit(this, 'edit_measure_unit', 44)"><!--  -->&nbsp;&nbsp;&nbsp;&nbsp;<!--  --></span></td>
    <td width="10%"><img src="images/no.gif" onclick="listTable.toggle(this, 'toggle_show_in_nav', 44)" /></td>
    <td width="10%"><img src="images/yes.gif" onclick="listTable.toggle(this, 'toggle_is_show', 44)" /></td>
    <td><span onclick="listTable.edit(this, 'edit_grade', 44)">0</span></td>
    <td width="10%" align="right"><span onclick="listTable.edit(this, 'edit_sort_order', 44)">50</span></td>
    <td width="24%" align="center">
      <a href="category.php?act=move&cat_id=44">转移商品</a> |
      <a href="category.php?act=edit&amp;cat_id=44">编辑</a> |
      <a href="javascript:;" onclick="listTable.remove(44, '您确认要删除这条记录吗?')" title="移除">移除</a>
	  <!--代码修改_start Byjdy-->
	  	  <!--代码修改_end Byjdy-->
    </td>
  </tr>
    <tr align="center" class="2" id="2_41">
    <td align="left" class="first-cell" >
            <img src="images/menu_minus.gif" id="icon_2_41" width="9" height="9" border="0" style="margin-left:2em" onclick="rowClicked(this)" />
            <span>
      <!-- 0-默认列表 1-搜索匹配列表 其他-搜索未匹配列表 -->
            <a href="goods.php?act=list&cat_id=41&supp=-1">碧根果</a>
            </span>
        </td>
    <td width="10%">0</td>
    <td width="10%"><span onclick="listTable.edit(this, 'edit_measure_unit', 41)"><!--  -->&nbsp;&nbsp;&nbsp;&nbsp;<!--  --></span></td>
    <td width="10%"><img src="images/no.gif" onclick="listTable.toggle(this, 'toggle_show_in_nav', 41)" /></td>
    <td width="10%"><img src="images/yes.gif" onclick="listTable.toggle(this, 'toggle_is_show', 41)" /></td>
    <td><span onclick="listTable.edit(this, 'edit_grade', 41)">0</span></td>
    <td width="10%" align="right"><span onclick="listTable.edit(this, 'edit_sort_order', 41)">50</span></td>
    <td width="24%" align="center">
      <a href="category.php?act=move&cat_id=41">转移商品</a> |
      <a href="category.php?act=edit&amp;cat_id=41">编辑</a> |
      <a href="javascript:;" onclick="listTable.remove(41, '您确认要删除这条记录吗?')" title="移除">移除</a>
	  <!--代码修改_start Byjdy-->
	  	  <!--代码修改_end Byjdy-->
    </td>
  </tr>
    <tr align="center" class="2" id="2_45">
    <td align="left" class="first-cell" >
            <img src="images/menu_minus.gif" id="icon_2_45" width="9" height="9" border="0" style="margin-left:2em" onclick="rowClicked(this)" />
            <span>
      <!-- 0-默认列表 1-搜索匹配列表 其他-搜索未匹配列表 -->
            <a href="goods.php?act=list&cat_id=45&supp=-1">花生</a>
            </span>
        </td>
    <td width="10%">0</td>
    <td width="10%"><span onclick="listTable.edit(this, 'edit_measure_unit', 45)"><!--  -->&nbsp;&nbsp;&nbsp;&nbsp;<!--  --></span></td>
    <td width="10%"><img src="images/no.gif" onclick="listTable.toggle(this, 'toggle_show_in_nav', 45)" /></td>
    <td width="10%"><img src="images/yes.gif" onclick="listTable.toggle(this, 'toggle_is_show', 45)" /></td>
    <td><span onclick="listTable.edit(this, 'edit_grade', 45)">0</span></td>
    <td width="10%" align="right"><span onclick="listTable.edit(this, 'edit_sort_order', 45)">50</span></td>
    <td width="24%" align="center">
      <a href="category.php?act=move&cat_id=45">转移商品</a> |
      <a href="category.php?act=edit&amp;cat_id=45">编辑</a> |
      <a href="javascript:;" onclick="listTable.remove(45, '您确认要删除这条记录吗?')" title="移除">移除</a>
	  <!--代码修改_start Byjdy-->
	  	  <!--代码修改_end Byjdy-->
    </td>
  </tr>
    <tr align="center" class="2" id="2_42">
    <td align="left" class="first-cell" >
            <img src="images/menu_minus.gif" id="icon_2_42" width="9" height="9" border="0" style="margin-left:2em" onclick="rowClicked(this)" />
            <span>
      <!-- 0-默认列表 1-搜索匹配列表 其他-搜索未匹配列表 -->
            <a href="goods.php?act=list&cat_id=42&supp=-1">开心果</a>
            </span>
        </td>
    <td width="10%">0</td>
    <td width="10%"><span onclick="listTable.edit(this, 'edit_measure_unit', 42)"><!--  -->&nbsp;&nbsp;&nbsp;&nbsp;<!--  --></span></td>
    <td width="10%"><img src="images/no.gif" onclick="listTable.toggle(this, 'toggle_show_in_nav', 42)" /></td>
    <td width="10%"><img src="images/yes.gif" onclick="listTable.toggle(this, 'toggle_is_show', 42)" /></td>
    <td><span onclick="listTable.edit(this, 'edit_grade', 42)">0</span></td>
    <td width="10%" align="right"><span onclick="listTable.edit(this, 'edit_sort_order', 42)">50</span></td>
    <td width="24%" align="center">
      <a href="category.php?act=move&cat_id=42">转移商品</a> |
      <a href="category.php?act=edit&amp;cat_id=42">编辑</a> |
      <a href="javascript:;" onclick="listTable.remove(42, '您确认要删除这条记录吗?')" title="移除">移除</a>
	  <!--代码修改_start Byjdy-->
	  	  <!--代码修改_end Byjdy-->
    </td>
  </tr>
    <tr align="center" class="2" id="2_39">
    <td align="left" class="first-cell" >
            <img src="images/menu_minus.gif" id="icon_2_39" width="9" height="9" border="0" style="margin-left:2em" onclick="rowClicked(this)" />
            <span>
      <!-- 0-默认列表 1-搜索匹配列表 其他-搜索未匹配列表 -->
            <a href="goods.php?act=list&cat_id=39&supp=-1">核桃</a>
            </span>
        </td>
    <td width="10%">0</td>
    <td width="10%"><span onclick="listTable.edit(this, 'edit_measure_unit', 39)"><!--  -->&nbsp;&nbsp;&nbsp;&nbsp;<!--  --></span></td>
    <td width="10%"><img src="images/no.gif" onclick="listTable.toggle(this, 'toggle_show_in_nav', 39)" /></td>
    <td width="10%"><img src="images/yes.gif" onclick="listTable.toggle(this, 'toggle_is_show', 39)" /></td>
    <td><span onclick="listTable.edit(this, 'edit_grade', 39)">0</span></td>
    <td width="10%" align="right"><span onclick="listTable.edit(this, 'edit_sort_order', 39)">50</span></td>
    <td width="24%" align="center">
      <a href="category.php?act=move&cat_id=39">转移商品</a> |
      <a href="category.php?act=edit&amp;cat_id=39">编辑</a> |
      <a href="javascript:;" onclick="listTable.remove(39, '您确认要删除这条记录吗?')" title="移除">移除</a>
	  <!--代码修改_start Byjdy-->
	  	  <!--代码修改_end Byjdy-->
    </td>
  </tr>
    <tr align="center" class="2" id="2_46">
    <td align="left" class="first-cell" >
            <img src="images/menu_minus.gif" id="icon_2_46" width="9" height="9" border="0" style="margin-left:2em" onclick="rowClicked(this)" />
            <span>
      <!-- 0-默认列表 1-搜索匹配列表 其他-搜索未匹配列表 -->
            <a href="goods.php?act=list&cat_id=46&supp=-1">杏仁</a>
            </span>
        </td>
    <td width="10%">0</td>
    <td width="10%"><span onclick="listTable.edit(this, 'edit_measure_unit', 46)"><!--  -->&nbsp;&nbsp;&nbsp;&nbsp;<!--  --></span></td>
    <td width="10%"><img src="images/no.gif" onclick="listTable.toggle(this, 'toggle_show_in_nav', 46)" /></td>
    <td width="10%"><img src="images/yes.gif" onclick="listTable.toggle(this, 'toggle_is_show', 46)" /></td>
    <td><span onclick="listTable.edit(this, 'edit_grade', 46)">0</span></td>
    <td width="10%" align="right"><span onclick="listTable.edit(this, 'edit_sort_order', 46)">50</span></td>
    <td width="24%" align="center">
      <a href="category.php?act=move&cat_id=46">转移商品</a> |
      <a href="category.php?act=edit&amp;cat_id=46">编辑</a> |
      <a href="javascript:;" onclick="listTable.remove(46, '您确认要删除这条记录吗?')" title="移除">移除</a>
	  <!--代码修改_start Byjdy-->
	  	  <!--代码修改_end Byjdy-->
    </td>
  </tr>
    <tr align="center" class="2" id="2_43">
    <td align="left" class="first-cell" >
            <img src="images/menu_minus.gif" id="icon_2_43" width="9" height="9" border="0" style="margin-left:2em" onclick="rowClicked(this)" />
            <span>
      <!-- 0-默认列表 1-搜索匹配列表 其他-搜索未匹配列表 -->
            <a href="goods.php?act=list&cat_id=43&supp=-1">腰果</a>
            </span>
        </td>
    <td width="10%">0</td>
    <td width="10%"><span onclick="listTable.edit(this, 'edit_measure_unit', 43)"><!--  -->&nbsp;&nbsp;&nbsp;&nbsp;<!--  --></span></td>
    <td width="10%"><img src="images/no.gif" onclick="listTable.toggle(this, 'toggle_show_in_nav', 43)" /></td>
    <td width="10%"><img src="images/yes.gif" onclick="listTable.toggle(this, 'toggle_is_show', 43)" /></td>
    <td><span onclick="listTable.edit(this, 'edit_grade', 43)">0</span></td>
    <td width="10%" align="right"><span onclick="listTable.edit(this, 'edit_sort_order', 43)">50</span></td>
    <td width="24%" align="center">
      <a href="category.php?act=move&cat_id=43">转移商品</a> |
      <a href="category.php?act=edit&amp;cat_id=43">编辑</a> |
      <a href="javascript:;" onclick="listTable.remove(43, '您确认要删除这条记录吗?')" title="移除">移除</a>
	  <!--代码修改_start Byjdy-->
	  	  <!--代码修改_end Byjdy-->
    </td>
  </tr>
    <tr align="center" class="1" id="1_12">
    <td align="left" class="first-cell" >
            <img src="images/menu_minus.gif" id="icon_1_12" width="9" height="9" border="0" style="margin-left:1em" onclick="rowClicked(this)" />
            <span>
      <!-- 0-默认列表 1-搜索匹配列表 其他-搜索未匹配列表 -->
            <a href="goods.php?act=list&cat_id=12&supp=-1">蜜饯果干</a>
            </span>
        </td>
    <td width="10%">0</td>
    <td width="10%"><span onclick="listTable.edit(this, 'edit_measure_unit', 12)"><!--  -->&nbsp;&nbsp;&nbsp;&nbsp;<!--  --></span></td>
    <td width="10%"><img src="images/no.gif" onclick="listTable.toggle(this, 'toggle_show_in_nav', 12)" /></td>
    <td width="10%"><img src="images/yes.gif" onclick="listTable.toggle(this, 'toggle_is_show', 12)" /></td>
    <td><span onclick="listTable.edit(this, 'edit_grade', 12)">0</span></td>
    <td width="10%" align="right"><span onclick="listTable.edit(this, 'edit_sort_order', 12)">14</span></td>
    <td width="24%" align="center">
      <a href="category.php?act=move&cat_id=12">转移商品</a> |
      <a href="category.php?act=edit&amp;cat_id=12">编辑</a> |
      <a href="javascript:;" onclick="listTable.remove(12, '您确认要删除这条记录吗?')" title="移除">移除</a>
	  <!--代码修改_start Byjdy-->
	  	  <!--代码修改_end Byjdy-->
    </td>
  </tr>
    <tr align="center" class="2" id="2_53">
    <td align="left" class="first-cell" >
            <img src="images/menu_minus.gif" id="icon_2_53" width="9" height="9" border="0" style="margin-left:2em" onclick="rowClicked(this)" />
            <span>
      <!-- 0-默认列表 1-搜索匹配列表 其他-搜索未匹配列表 -->
            <a href="goods.php?act=list&cat_id=53&supp=-1">橄榄</a>
            </span>
        </td>
    <td width="10%">0</td>
    <td width="10%"><span onclick="listTable.edit(this, 'edit_measure_unit', 53)"><!--  -->&nbsp;&nbsp;&nbsp;&nbsp;<!--  --></span></td>
    <td width="10%"><img src="images/no.gif" onclick="listTable.toggle(this, 'toggle_show_in_nav', 53)" /></td>
    <td width="10%"><img src="images/yes.gif" onclick="listTable.toggle(this, 'toggle_is_show', 53)" /></td>
    <td><span onclick="listTable.edit(this, 'edit_grade', 53)">0</span></td>
    <td width="10%" align="right"><span onclick="listTable.edit(this, 'edit_sort_order', 53)">50</span></td>
    <td width="24%" align="center">
      <a href="category.php?act=move&cat_id=53">转移商品</a> |
      <a href="category.php?act=edit&amp;cat_id=53">编辑</a> |
      <a href="javascript:;" onclick="listTable.remove(53, '您确认要删除这条记录吗?')" title="移除">移除</a>
	  <!--代码修改_start Byjdy-->
	  	  <!--代码修改_end Byjdy-->
    </td>
  </tr>
    <tr align="center" class="2" id="2_50">
    <td align="left" class="first-cell" >
            <img src="images/menu_minus.gif" id="icon_2_50" width="9" height="9" border="0" style="margin-left:2em" onclick="rowClicked(this)" />
            <span>
      <!-- 0-默认列表 1-搜索匹配列表 其他-搜索未匹配列表 -->
            <a href="goods.php?act=list&cat_id=50&supp=-1">芒果干</a>
            </span>
        </td>
    <td width="10%">0</td>
    <td width="10%"><span onclick="listTable.edit(this, 'edit_measure_unit', 50)"><!--  -->&nbsp;&nbsp;&nbsp;&nbsp;<!--  --></span></td>
    <td width="10%"><img src="images/no.gif" onclick="listTable.toggle(this, 'toggle_show_in_nav', 50)" /></td>
    <td width="10%"><img src="images/yes.gif" onclick="listTable.toggle(this, 'toggle_is_show', 50)" /></td>
    <td><span onclick="listTable.edit(this, 'edit_grade', 50)">0</span></td>
    <td width="10%" align="right"><span onclick="listTable.edit(this, 'edit_sort_order', 50)">50</span></td>
    <td width="24%" align="center">
      <a href="category.php?act=move&cat_id=50">转移商品</a> |
      <a href="category.php?act=edit&amp;cat_id=50">编辑</a> |
      <a href="javascript:;" onclick="listTable.remove(50, '您确认要删除这条记录吗?')" title="移除">移除</a>
	  <!--代码修改_start Byjdy-->
	  	  <!--代码修改_end Byjdy-->
    </td>
  </tr>
    <tr align="center" class="2" id="2_47">
    <td align="left" class="first-cell" >
            <img src="images/menu_minus.gif" id="icon_2_47" width="9" height="9" border="0" style="margin-left:2em" onclick="rowClicked(this)" />
            <span>
      <!-- 0-默认列表 1-搜索匹配列表 其他-搜索未匹配列表 -->
            <a href="goods.php?act=list&cat_id=47&supp=-1">红枣</a>
            </span>
        </td>
    <td width="10%">0</td>
    <td width="10%"><span onclick="listTable.edit(this, 'edit_measure_unit', 47)"><!--  -->&nbsp;&nbsp;&nbsp;&nbsp;<!--  --></span></td>
    <td width="10%"><img src="images/no.gif" onclick="listTable.toggle(this, 'toggle_show_in_nav', 47)" /></td>
    <td width="10%"><img src="images/yes.gif" onclick="listTable.toggle(this, 'toggle_is_show', 47)" /></td>
    <td><span onclick="listTable.edit(this, 'edit_grade', 47)">0</span></td>
    <td width="10%" align="right"><span onclick="listTable.edit(this, 'edit_sort_order', 47)">50</span></td>
    <td width="24%" align="center">
      <a href="category.php?act=move&cat_id=47">转移商品</a> |
      <a href="category.php?act=edit&amp;cat_id=47">编辑</a> |
      <a href="javascript:;" onclick="listTable.remove(47, '您确认要删除这条记录吗?')" title="移除">移除</a>
	  <!--代码修改_start Byjdy-->
	  	  <!--代码修改_end Byjdy-->
    </td>
  </tr>
    <tr align="center" class="2" id="2_54">
    <td align="left" class="first-cell" >
            <img src="images/menu_minus.gif" id="icon_2_54" width="9" height="9" border="0" style="margin-left:2em" onclick="rowClicked(this)" />
            <span>
      <!-- 0-默认列表 1-搜索匹配列表 其他-搜索未匹配列表 -->
            <a href="goods.php?act=list&cat_id=54&supp=-1">其他</a>
            </span>
        </td>
    <td width="10%">0</td>
    <td width="10%"><span onclick="listTable.edit(this, 'edit_measure_unit', 54)"><!--  -->&nbsp;&nbsp;&nbsp;&nbsp;<!--  --></span></td>
    <td width="10%"><img src="images/no.gif" onclick="listTable.toggle(this, 'toggle_show_in_nav', 54)" /></td>
    <td width="10%"><img src="images/yes.gif" onclick="listTable.toggle(this, 'toggle_is_show', 54)" /></td>
    <td><span onclick="listTable.edit(this, 'edit_grade', 54)">0</span></td>
    <td width="10%" align="right"><span onclick="listTable.edit(this, 'edit_sort_order', 54)">50</span></td>
    <td width="24%" align="center">
      <a href="category.php?act=move&cat_id=54">转移商品</a> |
      <a href="category.php?act=edit&amp;cat_id=54">编辑</a> |
      <a href="javascript:;" onclick="listTable.remove(54, '您确认要删除这条记录吗?')" title="移除">移除</a>
	  <!--代码修改_start Byjdy-->
	  	  <!--代码修改_end Byjdy-->
    </td>
  </tr>
    <tr align="center" class="2" id="2_51">
    <td align="left" class="first-cell" >
            <img src="images/menu_minus.gif" id="icon_2_51" width="9" height="9" border="0" style="margin-left:2em" onclick="rowClicked(this)" />
            <span>
      <!-- 0-默认列表 1-搜索匹配列表 其他-搜索未匹配列表 -->
            <a href="goods.php?act=list&cat_id=51&supp=-1">香蕉干</a>
            </span>
        </td>
    <td width="10%">0</td>
    <td width="10%"><span onclick="listTable.edit(this, 'edit_measure_unit', 51)"><!--  -->&nbsp;&nbsp;&nbsp;&nbsp;<!--  --></span></td>
    <td width="10%"><img src="images/no.gif" onclick="listTable.toggle(this, 'toggle_show_in_nav', 51)" /></td>
    <td width="10%"><img src="images/yes.gif" onclick="listTable.toggle(this, 'toggle_is_show', 51)" /></td>
    <td><span onclick="listTable.edit(this, 'edit_grade', 51)">0</span></td>
    <td width="10%" align="right"><span onclick="listTable.edit(this, 'edit_sort_order', 51)">50</span></td>
    <td width="24%" align="center">
      <a href="category.php?act=move&cat_id=51">转移商品</a> |
      <a href="category.php?act=edit&amp;cat_id=51">编辑</a> |
      <a href="javascript:;" onclick="listTable.remove(51, '您确认要删除这条记录吗?')" title="移除">移除</a>
	  <!--代码修改_start Byjdy-->
	  	  <!--代码修改_end Byjdy-->
    </td>
  </tr>
    <tr align="center" class="2" id="2_48">
    <td align="left" class="first-cell" >
            <img src="images/menu_minus.gif" id="icon_2_48" width="9" height="9" border="0" style="margin-left:2em" onclick="rowClicked(this)" />
            <span>
      <!-- 0-默认列表 1-搜索匹配列表 其他-搜索未匹配列表 -->
            <a href="goods.php?act=list&cat_id=48&supp=-1">莓类</a>
            </span>
        </td>
    <td width="10%">0</td>
    <td width="10%"><span onclick="listTable.edit(this, 'edit_measure_unit', 48)"><!--  -->&nbsp;&nbsp;&nbsp;&nbsp;<!--  --></span></td>
    <td width="10%"><img src="images/no.gif" onclick="listTable.toggle(this, 'toggle_show_in_nav', 48)" /></td>
    <td width="10%"><img src="images/yes.gif" onclick="listTable.toggle(this, 'toggle_is_show', 48)" /></td>
    <td><span onclick="listTable.edit(this, 'edit_grade', 48)">0</span></td>
    <td width="10%" align="right"><span onclick="listTable.edit(this, 'edit_sort_order', 48)">50</span></td>
    <td width="24%" align="center">
      <a href="category.php?act=move&cat_id=48">转移商品</a> |
      <a href="category.php?act=edit&amp;cat_id=48">编辑</a> |
      <a href="javascript:;" onclick="listTable.remove(48, '您确认要删除这条记录吗?')" title="移除">移除</a>
	  <!--代码修改_start Byjdy-->
	  	  <!--代码修改_end Byjdy-->
    </td>
  </tr>
    <tr align="center" class="2" id="2_52">
    <td align="left" class="first-cell" >
            <img src="images/menu_minus.gif" id="icon_2_52" width="9" height="9" border="0" style="margin-left:2em" onclick="rowClicked(this)" />
            <span>
      <!-- 0-默认列表 1-搜索匹配列表 其他-搜索未匹配列表 -->
            <a href="goods.php?act=list&cat_id=52&supp=-1">山楂片</a>
            </span>
        </td>
    <td width="10%">0</td>
    <td width="10%"><span onclick="listTable.edit(this, 'edit_measure_unit', 52)"><!--  -->&nbsp;&nbsp;&nbsp;&nbsp;<!--  --></span></td>
    <td width="10%"><img src="images/no.gif" onclick="listTable.toggle(this, 'toggle_show_in_nav', 52)" /></td>
    <td width="10%"><img src="images/yes.gif" onclick="listTable.toggle(this, 'toggle_is_show', 52)" /></td>
    <td><span onclick="listTable.edit(this, 'edit_grade', 52)">0</span></td>
    <td width="10%" align="right"><span onclick="listTable.edit(this, 'edit_sort_order', 52)">50</span></td>
    <td width="24%" align="center">
      <a href="category.php?act=move&cat_id=52">转移商品</a> |
      <a href="category.php?act=edit&amp;cat_id=52">编辑</a> |
      <a href="javascript:;" onclick="listTable.remove(52, '您确认要删除这条记录吗?')" title="移除">移除</a>
	  <!--代码修改_start Byjdy-->
	  	  <!--代码修改_end Byjdy-->
    </td>
  </tr>
    <tr align="center" class="2" id="2_49">
    <td align="left" class="first-cell" >
            <img src="images/menu_minus.gif" id="icon_2_49" width="9" height="9" border="0" style="margin-left:2em" onclick="rowClicked(this)" />
            <span>
      <!-- 0-默认列表 1-搜索匹配列表 其他-搜索未匹配列表 -->
            <a href="goods.php?act=list&cat_id=49&supp=-1">葡萄干</a>
            </span>
        </td>
    <td width="10%">0</td>
    <td width="10%"><span onclick="listTable.edit(this, 'edit_measure_unit', 49)"><!--  -->&nbsp;&nbsp;&nbsp;&nbsp;<!--  --></span></td>
    <td width="10%"><img src="images/no.gif" onclick="listTable.toggle(this, 'toggle_show_in_nav', 49)" /></td>
    <td width="10%"><img src="images/yes.gif" onclick="listTable.toggle(this, 'toggle_is_show', 49)" /></td>
    <td><span onclick="listTable.edit(this, 'edit_grade', 49)">0</span></td>
    <td width="10%" align="right"><span onclick="listTable.edit(this, 'edit_sort_order', 49)">50</span></td>
    <td width="24%" align="center">
      <a href="category.php?act=move&cat_id=49">转移商品</a> |
      <a href="category.php?act=edit&amp;cat_id=49">编辑</a> |
      <a href="javascript:;" onclick="listTable.remove(49, '您确认要删除这条记录吗?')" title="移除">移除</a>
	  <!--代码修改_start Byjdy-->
	  	  <!--代码修改_end Byjdy-->
    </td>
  </tr>
    <tr align="center" class="0" id="0_362">
    <td align="left" class="first-cell" >
            <img src="images/menu_minus.gif" id="icon_0_362" width="9" height="9" border="0" style="margin-left:0em" onclick="rowClicked(this)" />
            <span>
      <!-- 0-默认列表 1-搜索匹配列表 其他-搜索未匹配列表 -->
            <a href="goods.php?act=list&cat_id=362&supp=-1">营养保健</a>
            </span>
        </td>
    <td width="10%">0</td>
    <td width="10%"><span onclick="listTable.edit(this, 'edit_measure_unit', 362)"><!--  -->&nbsp;&nbsp;&nbsp;&nbsp;<!--  --></span></td>
    <td width="10%"><img src="images/no.gif" onclick="listTable.toggle(this, 'toggle_show_in_nav', 362)" /></td>
    <td width="10%"><img src="images/yes.gif" onclick="listTable.toggle(this, 'toggle_is_show', 362)" /></td>
    <td><span onclick="listTable.edit(this, 'edit_grade', 362)">0</span></td>
    <td width="10%" align="right"><span onclick="listTable.edit(this, 'edit_sort_order', 362)">12</span></td>
    <td width="24%" align="center">
      <a href="category.php?act=move&cat_id=362">转移商品</a> |
      <a href="category.php?act=edit&amp;cat_id=362">编辑</a> |
      <a href="javascript:;" onclick="listTable.remove(362, '您确认要删除这条记录吗?')" title="移除">移除</a>
	  <!--代码修改_start Byjdy-->
	  	  | <a href="category_flashimg.php?act=list&amp;cat_id=362">设置轮播图片</a>	  
	  	  <!--代码修改_end Byjdy-->
    </td>
  </tr>
    <tr align="center" class="0" id="0_363">
    <td align="left" class="first-cell" >
            <img src="images/menu_minus.gif" id="icon_0_363" width="9" height="9" border="0" style="margin-left:0em" onclick="rowClicked(this)" />
            <span>
      <!-- 0-默认列表 1-搜索匹配列表 其他-搜索未匹配列表 -->
            <a href="goods.php?act=list&cat_id=363&supp=-1">图书、音像、电子书</a>
            </span>
        </td>
    <td width="10%">0</td>
    <td width="10%"><span onclick="listTable.edit(this, 'edit_measure_unit', 363)"><!--  -->&nbsp;&nbsp;&nbsp;&nbsp;<!--  --></span></td>
    <td width="10%"><img src="images/no.gif" onclick="listTable.toggle(this, 'toggle_show_in_nav', 363)" /></td>
    <td width="10%"><img src="images/yes.gif" onclick="listTable.toggle(this, 'toggle_is_show', 363)" /></td>
    <td><span onclick="listTable.edit(this, 'edit_grade', 363)">0</span></td>
    <td width="10%" align="right"><span onclick="listTable.edit(this, 'edit_sort_order', 363)">13</span></td>
    <td width="24%" align="center">
      <a href="category.php?act=move&cat_id=363">转移商品</a> |
      <a href="category.php?act=edit&amp;cat_id=363">编辑</a> |
      <a href="javascript:;" onclick="listTable.remove(363, '您确认要删除这条记录吗?')" title="移除">移除</a>
	  <!--代码修改_start Byjdy-->
	  	  | <a href="category_flashimg.php?act=list&amp;cat_id=363">设置轮播图片</a>	  
	  	  <!--代码修改_end Byjdy-->
    </td>
  </tr>
    <tr align="center" class="0" id="0_364">
    <td align="left" class="first-cell" >
            <img src="images/menu_minus.gif" id="icon_0_364" width="9" height="9" border="0" style="margin-left:0em" onclick="rowClicked(this)" />
            <span>
      <!-- 0-默认列表 1-搜索匹配列表 其他-搜索未匹配列表 -->
            <a href="goods.php?act=list&cat_id=364&supp=-1">彩票、旅行、充值、票务</a>
            </span>
        </td>
    <td width="10%">0</td>
    <td width="10%"><span onclick="listTable.edit(this, 'edit_measure_unit', 364)"><!--  -->&nbsp;&nbsp;&nbsp;&nbsp;<!--  --></span></td>
    <td width="10%"><img src="images/no.gif" onclick="listTable.toggle(this, 'toggle_show_in_nav', 364)" /></td>
    <td width="10%"><img src="images/yes.gif" onclick="listTable.toggle(this, 'toggle_is_show', 364)" /></td>
    <td><span onclick="listTable.edit(this, 'edit_grade', 364)">0</span></td>
    <td width="10%" align="right"><span onclick="listTable.edit(this, 'edit_sort_order', 364)">14</span></td>
    <td width="24%" align="center">
      <a href="category.php?act=move&cat_id=364">转移商品</a> |
      <a href="category.php?act=edit&amp;cat_id=364">编辑</a> |
      <a href="javascript:;" onclick="listTable.remove(364, '您确认要删除这条记录吗?')" title="移除">移除</a>
	  <!--代码修改_start Byjdy-->
	  	  | <a href="category_flashimg.php?act=list&amp;cat_id=364">设置轮播图片</a>	  
	  	  <!--代码修改_end Byjdy-->
    </td>
  </tr>
    <tr align="center" class="0" id="0_365">
    <td align="left" class="first-cell" >
            <img src="images/menu_minus.gif" id="icon_0_365" width="9" height="9" border="0" style="margin-left:0em" onclick="rowClicked(this)" />
            <span>
      <!-- 0-默认列表 1-搜索匹配列表 其他-搜索未匹配列表 -->
            <a href="goods.php?act=list&cat_id=365&supp=-1">理财、众筹、白条、保险</a>
            </span>
        </td>
    <td width="10%">0</td>
    <td width="10%"><span onclick="listTable.edit(this, 'edit_measure_unit', 365)"><!--  -->&nbsp;&nbsp;&nbsp;&nbsp;<!--  --></span></td>
    <td width="10%"><img src="images/no.gif" onclick="listTable.toggle(this, 'toggle_show_in_nav', 365)" /></td>
    <td width="10%"><img src="images/yes.gif" onclick="listTable.toggle(this, 'toggle_is_show', 365)" /></td>
    <td><span onclick="listTable.edit(this, 'edit_grade', 365)">0</span></td>
    <td width="10%" align="right"><span onclick="listTable.edit(this, 'edit_sort_order', 365)">15</span></td>
    <td width="24%" align="center">
      <a href="category.php?act=move&cat_id=365">转移商品</a> |
      <a href="category.php?act=edit&amp;cat_id=365">编辑</a> |
      <a href="javascript:;" onclick="listTable.remove(365, '您确认要删除这条记录吗?')" title="移除">移除</a>
	  <!--代码修改_start Byjdy-->
	  	  | <a href="category_flashimg.php?act=list&amp;cat_id=365">设置轮播图片</a>	  
	  	  <!--代码修改_end Byjdy-->
    </td>
  </tr>
  </table>
</div>
</form>


<script language="JavaScript">
<!--

onload = function()
{
  // 开始检查订单
  startCheckOrder();
}

var imgPlus = new Image();
imgPlus.src = "images/menu_plus.gif";

/**
 * 折叠分类列表
 */
function rowClicked(obj)
{
  // 当前图像
  img = obj;
  // 取得上二级tr>td>img对象
  obj = obj.parentNode.parentNode;
  // 整个分类列表表格
  var tbl = document.getElementById("list-table");
  // 当前分类级别
  var lvl = parseInt(obj.className);
  // 是否找到元素
  var fnd = false;
  var sub_display = img.src.indexOf('menu_minus.gif') > 0 ? 'none' : (Browser.isIE) ? 'block' : 'table-row' ;
  // 遍历所有的分类
  for (i = 0; i < tbl.rows.length; i++)
  {
      var row = tbl.rows[i];
      if (row == obj)
      {
          // 找到当前行
          fnd = true;
          //document.getElementById('result').innerHTML += 'Find row at ' + i +"<br/>";
      }
      else
      {
          if (fnd == true)
          {
              var cur = parseInt(row.className);
              var icon = 'icon_' + row.id;
              if (cur > lvl)
              {
                  row.style.display = sub_display;
                  if (sub_display != 'none')
                  {
                      var iconimg = document.getElementById(icon);
                      iconimg.src = iconimg.src.replace('plus.gif', 'minus.gif');
                  }
              }
              else
              {
                  fnd = false;
                  break;
              }
          }
      }
  }

  for (i = 0; i < obj.cells[0].childNodes.length; i++)
  {
      var imgObj = obj.cells[0].childNodes[i];
      if (imgObj.tagName == "IMG" && imgObj.src != 'images/menu_arrow.gif')
      {
          imgObj.src = (imgObj.src == imgPlus.src) ? 'images/menu_minus.gif' : imgPlus.src;
      }
  }
}

/**
 * 展开或折叠所有分类
 * 直接调用了rowClicked()函数，由于其函数内每次都会扫描整张表所以效率会比较低，数据量大会出现卡顿现象
 */
var expand = true;
function expandAll(obj)
{
	
	var selecter;
	
	if(expand)
	{
		// 收缩
		selecter = "img[src*='menu_minus.gif'],img[src*='menu_plus.gif']";
		$(obj).html("[全部展开]");
		$(selecter).parents("tr[class!='0']").hide();
		$(selecter).attr("src", "images/menu_plus.gif");
	}
	else
	{
		// 展开
		selecter = "img[src*='menu_plus.gif'],img[src*='menu_minus.gif']";
		$(obj).html("[全部收缩]");
		$(selecter).parents("tr").show();
		$(selecter).attr("src", "images/menu_minus.gif");
	}
	
	// 标识展开/收缩状态
	expand = !expand;
}
//-->
</script>


<div id="footer">
共执行 1 个查询，用时 0.020001 秒，Gzip 已禁用，内存占用 4.190 MB<br />
版权所有 &copy; 2008-2030 广州网软志成信息科技有限公司，并保留所有权利。</div>
<!-- 新订单提示信息 -->
<div id="popMsg">
  <table cellspacing="0" cellpadding="0" width="100%" bgcolor="#cfdef4" border="0">
  <tr>
    <td style="color: #0f2c8c" width="30" height="24"></td>
    <td style="font-weight: normal; color: #1f336b; padding-top: 4px;padding-left: 4px" valign="center" width="100%"> 新订单通知</td>
    <td style="padding-top: 2px;padding-right:2px" valign="center" align="right" width="19"><span title="关闭" style="cursor: hand;cursor:pointer;color:red;font-size:12px;font-weight:bold;margin-right:4px;" onclick="Message.close()" >×</span><!-- <img title=关闭 style="cursor: hand" onclick=closediv() hspace=3 src="msgclose.jpg"> --></td>
  </tr>
  <tr>
    <td style="padding-right: 1px; padding-bottom: 1px" colspan="3" height="70">
    <div id="popMsgContent">
      <p>您有 <strong style="color:#ff0000" id="spanNewOrder">1</strong> 个新订单以及       <strong style="color:#ff0000" id="spanNewPaid">0</strong> 个新付款的订单</p>
      <p align="center" style="word-break:break-all"><a href="order.php?act=list"><span style="color:#ff0000">点击查看新订单</span></a></p>
    </div>
    </td>
  </tr>
  </table>
</div>

<!--
<embed src="images/online.wav" width="0" height="0" autostart="false" name="msgBeep" id="msgBeep" enablejavascript="true"/>
-->
<object classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000" codebase="http://active.macromedia.com/flash2/cabs/swflash.cab#version=4,0,0,0" id="msgBeep" width="1" height="1">
  <param name="movie" value="images/online.swf">
  <param name="quality" value="high">
  <embed src="images/online.swf" name="msgBeep" id="msgBeep" quality="high" width="0" height="0" type="application/x-shockwave-flash" pluginspage="http://www.macromedia.com/shockwave/download/index.cgi?p1_prod_version=shockwaveflash">
  </embed>
</object>

<script language="JavaScript">
document.onmousemove=function(e)
{
  var obj = Utils.srcElement(e);
  if (typeof(obj.onclick) == 'function' && obj.onclick.toString().indexOf('listTable.edit') != -1)
  {
    obj.title = '点击修改内容';
    obj.style.cssText = 'background: #278296;';
    obj.onmouseout = function(e)
    {
      this.style.cssText = '';
    }
  }
  else if (typeof(obj.href) != 'undefined' && obj.href.indexOf('listTable.sort') != -1)
  {
    obj.title = '点击对列表排序';
  }
}
<!--


var MyTodolist;
function showTodoList(adminid)
{
  if(!MyTodolist)
  {
    var global = $import("../js/global.js","js");
    global.onload = global.onreadystatechange= function()
    {
      if(this.readyState && this.readyState=="loading")return;
      var md5 = $import("js/md5.js","js");
      md5.onload = md5.onreadystatechange= function()
      {
        if(this.readyState && this.readyState=="loading")return;
        var todolist = $import("js/todolist.js","js");
        todolist.onload = todolist.onreadystatechange = function()
        {
          if(this.readyState && this.readyState=="loading")return;
          MyTodolist = new Todolist();
          MyTodolist.show();
        }
      }
    }
  }
  else
  {
    if(MyTodolist.visibility)
    {
      MyTodolist.hide();
    }
    else
    {
      MyTodolist.show();
    }
  }
}

if (Browser.isIE)
{
  onscroll = function()
  {
    //document.getElementById('calculator').style.top = document.body.scrollTop;
    document.getElementById('popMsg').style.top = (document.body.scrollTop + document.body.clientHeight - document.getElementById('popMsg').offsetHeight) + "px";
  }
}

if (document.getElementById("listDiv"))
{
  document.getElementById("listDiv").onmouseover = function(e)
  {
    obj = Utils.srcElement(e);

    if (obj)
    {
      if (obj.parentNode.tagName.toLowerCase() == "tr") row = obj.parentNode;
      else if (obj.parentNode.parentNode.tagName.toLowerCase() == "tr") row = obj.parentNode.parentNode;
      else return;

      for (i = 0; i < row.cells.length; i++)
      {
        if (row.cells[i].tagName != "TH") row.cells[i].style.backgroundColor = '#F4FAFB';
      }
    }

  }

  document.getElementById("listDiv").onmouseout = function(e)
  {
    obj = Utils.srcElement(e);

    if (obj)
    {
      if (obj.parentNode.tagName.toLowerCase() == "tr") row = obj.parentNode;
      else if (obj.parentNode.parentNode.tagName.toLowerCase() == "tr") row = obj.parentNode.parentNode;
      else return;

      for (i = 0; i < row.cells.length; i++)
      {
          if (row.cells[i].tagName != "TH") row.cells[i].style.backgroundColor = '';
      }
    }
  }

  document.getElementById("listDiv").onclick = function(e)
  {
    var obj = Utils.srcElement(e);

    if (obj.tagName == "INPUT" && obj.type == "checkbox")
    {
      if (!document.forms['listForm'])
      {
        return;
      }
      var nodes = document.forms['listForm'].elements;
      var checked = false;

      for (i = 0; i < nodes.length; i++)
      {
        if (nodes[i].checked)
        {
           checked = true;
           break;
         }
      }

      if(document.getElementById("btnSubmit"))
      {
        document.getElementById("btnSubmit").disabled = !checked;
      }
      for (i = 1; i <= 10; i++)
      {
        if (document.getElementById("btnSubmit" + i))
        {
          document.getElementById("btnSubmit" + i).disabled = !checked;
        }
      }
    }
  }

}

//-->
</script>
</body>
</html>

<!-- $Id: category_info.htm 16752 2009-10-20 09:59:38Z wangleisvn $ -->

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>管理中心 - 编辑虚拟商品分类 </title>
<meta name="robots" content="noindex, nofollow">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link href="styles/general.css" rel="stylesheet" type="text/css" />
<link href="styles/main.css" rel="stylesheet" type="text/css" />
<link href="styles/chosen/chosen.css" rel="stylesheet" type="text/css">
<script type="text/javascript" src="js/jquery-1.6.2.min.js"></script><script type="text/javascript" src="js/chosen.jquery.min.js"></script><script type="text/javascript" src="js/jquery.json.js"></script><script type="text/javascript" src="../js/transport.js"></script><script type="text/javascript" src="js/common.js"></script><script language="JavaScript">
<!--
// 这里把JS用到的所有语言都赋值到这里
var process_request = "正在处理您的请求...";
var todolist_caption = "记事本";
var todolist_autosave = "自动保存";
var todolist_save = "保存";
var todolist_clear = "清除";
var todolist_confirm_save = "是否将更改保存到记事本？";
var todolist_confirm_clear = "是否清空内容？";
var catname_empty = "分类名称不能为空!";
var unit_empyt = "数量单位不能为空!";
var is_leafcat = "您选定的分类是一个末级分类。\r\n新分类的上级分类不能是一个末级分类";
var not_leafcat = "您选定的分类不是一个末级分类。\r\n商品的分类转移只能在末级分类之间才可以操作。";
var filter_attr_not_repeated = "筛选属性不可重复";
var filter_attr_not_selected = "请选择筛选属性";
//-->
</script>
</head>
<body>

<div id="menu_list" onmouseover="show_popup()" onmouseout="hide_popup()">
<ul>
<li><a href="goods.php?act=add" target="main_frame">添加新商品</a></li>
<li><a href="category.php?act=add" target="main_frame">添加商品分类</a></li>
<li><a href="order.php?act=add" target="main_frame">添加订单</a></li>
<li><a href="article.php?act=add" target="main_frame">添加新文章</a></li>
<li><a href="users.php?act=add" target="main_frame">添加会员</a></li>
</ul>
</div>
<script>
function show_popup(){
frmBody = parent.document.getElementById('frame-body');
if (frmBody.cols == "37, 12, *")
{
parent.main_frame.document.getElementById('menu_list').style.left = '195px';
}
else
{
parent.main_frame.document.getElementById('menu_list').style.left = '40px';
}
parent.main_frame.document.getElementById('menu_list').style.display = 'block';
}
function hide_popup(){

parent.main_frame.document.getElementById('menu_list').style.display = 'none';
}
</script>
<h1>
<span class="action-span"><a href="category.php?act=list">商品分类</a></span>
<span class="action-span1"><a href="index.php?act=main">管理中心</a> </span><span id="search_id" class="action-span1"> - 编辑虚拟商品分类 </span>
<div style="clear:both"></div>
</h1>
 
<!-- start add new category form -->
<div class="main-div">
  <form action="category.php" method="post" name="theForm" enctype="multipart/form-data" onsubmit="return validate()">
    <table width="100%" id="general-table">
      <tr>
        <td class="label">分类名称:</td>
        <td><textarea name='cat_name' rows="3" cols="48">家用电器</textarea>
          <font color="red">*</font> <span class="notice-span" style="display:block"  id="noticeCat_name">如果您需要添加多个分类，请在分类之间使用半角逗号分隔。</span></td>
      </tr>
      
	 
	  <tr>
        <td class="label">目录名称:</td>
        <td>
          <input type='text' name='path_name' maxlength="20" value='' size='27' />
		  <span class="notice-span" style="display:block"  id="noticePathname">生成的【真静态HTML文件】将保存到该目录下<br>例如：在这里输入 jiaju，根目录下就会生成一个 category-jiaju 的二级目录用来保存纯静态HTML文件</span>
        </td>
      </tr>
   
      
      <tr>
        <td class="label">上级分类:</td>
        <td><select name="parent_id">
            <option value="0">顶级分类</option>
            
            <option value="5" >家用电器</option><option value="182" >&nbsp;&nbsp;&nbsp;&nbsp;五金家装</option><option value="218" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;家具五金</option><option value="219" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;电工电料</option><option value="216" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;手动工具</option><option value="220" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;监控安防</option><option value="217" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;厨卫五金</option><option value="215" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;电动工具</option><option value="179" >&nbsp;&nbsp;&nbsp;&nbsp;生活电器</option><option value="193" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;加湿器</option><option value="197" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;饮水机</option><option value="194" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;吸尘器</option><option value="198" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;其它生活电器</option><option value="195" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;挂烫机/熨斗</option><option value="192" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;净化器</option><option value="191" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;电风扇</option><option value="196" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;取暖电器</option><option value="180" >&nbsp;&nbsp;&nbsp;&nbsp;厨房电器</option><option value="206" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;其它厨房电器</option><option value="203" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;电炖锅</option><option value="200" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;微波炉</option><option value="204" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;果蔬解毒机</option><option value="201" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;电磁炉</option><option value="205" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;养生壶/煎药壶</option><option value="202" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;电饼铛/烧烤盘</option><option value="199" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;电饭煲</option><option value="181" >&nbsp;&nbsp;&nbsp;&nbsp;个护健康</option><option value="209" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;按摩椅</option><option value="213" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;计步器/脂肪检测</option><option value="210" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;足浴盆</option><option value="207" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;剃须刀</option><option value="214" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;其它健康电器</option><option value="211" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;健康秤/厨房秤</option><option value="208" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;电吹风</option><option value="212" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;血糖仪</option><option value="178" >&nbsp;&nbsp;&nbsp;&nbsp;大家电</option><option value="185" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;洗衣机</option><option value="189" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;消毒柜/洗碗机</option><option value="186" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;家庭影院</option><option value="183" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;平板电视</option><option value="190" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;冷柜/冰吧</option><option value="187" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;烟机/灶具</option><option value="184" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;空调冰箱</option><option value="188" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;热水器</option><option value="4" >手机、数码、通信</option><option value="147" >&nbsp;&nbsp;&nbsp;&nbsp;智能设备</option><option value="169" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;体感车</option><option value="166" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;智能眼镜</option><option value="167" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;运动跟踪器</option><option value="164" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;智能手环</option><option value="168" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;智能家居</option><option value="165" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;智能手表</option><option value="144" >&nbsp;&nbsp;&nbsp;&nbsp;热卖手机</option><option value="153" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;联通4G</option><option value="150" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;小米特供</option><option value="154" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;电信4G</option><option value="151" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;魅族手机</option><option value="148" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;三星盖乐世</option><option value="155" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;移动4G</option><option value="152" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;华为荣耀</option><option value="149" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;iPhone</option><option value="145" >&nbsp;&nbsp;&nbsp;&nbsp;手机配件</option><option value="163" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;保护套</option><option value="160" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;创意配件</option><option value="157" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;移动电源</option><option value="161" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;手机饰品</option><option value="158" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;蓝牙耳机</option><option value="162" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;手机耳机</option><option value="159" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;充电器</option><option value="156" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;电池</option><option value="146" >&nbsp;&nbsp;&nbsp;&nbsp;数码影音</option><option value="176" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;数码相框</option><option value="173" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;运动相机</option><option value="170" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;数码相机</option><option value="177" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;影棚器材</option><option value="174" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;摄像机</option><option value="171" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;单反相机</option><option value="175" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;户外器材</option><option value="172" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;拍立得</option><option value="358" >电脑、办公</option><option value="6" >家居、家具、家装、厨具</option><option value="310" >&nbsp;&nbsp;&nbsp;&nbsp;家装软饰</option><option value="348" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;墙贴/装饰贴</option><option value="345" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;帘艺隔断</option><option value="342" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;桌布/罩件</option><option value="349" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;摆件花瓶</option><option value="346" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;相框/照片墙</option><option value="343" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;地毯地垫</option><option value="347" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;装饰字画</option><option value="344" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;沙发垫套/椅垫</option><option value="307" >&nbsp;&nbsp;&nbsp;&nbsp;家具</option><option value="322" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;餐厅家具</option><option value="326" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;沙发</option><option value="323" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;书房家具</option><option value="320" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;卧室家具</option><option value="327" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;鞋架/衣帽架</option><option value="324" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;储物家具</option><option value="321" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;客厅家具</option><option value="325" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;阳台/户外</option><option value="311" >&nbsp;&nbsp;&nbsp;&nbsp;生活日用</option><option value="354" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;洗晒/熨烫</option><option value="351" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;雨伞雨具</option><option value="355" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;净化除味</option><option value="352" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;浴室用品</option><option value="353" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;缝纫/针织用品</option><option value="350" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;收纳用品</option><option value="306" >&nbsp;&nbsp;&nbsp;&nbsp;家纺</option><option value="319" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;窗帘/窗纱</option><option value="316" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;床单被罩</option><option value="313" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;被子</option><option value="317" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;毛巾浴巾</option><option value="314" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;蚊帐</option><option value="318" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;床垫/床褥</option><option value="315" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;凉席</option><option value="312" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;床品套件</option><option value="308" >&nbsp;&nbsp;&nbsp;&nbsp;厨具</option><option value="332" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;餐具</option><option value="329" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;刀剪菜板</option><option value="333" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;茶具/咖啡具</option><option value="330" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;厨房配件</option><option value="331" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;水具酒具</option><option value="328" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;烹饪锅具</option><option value="309" >&nbsp;&nbsp;&nbsp;&nbsp;灯具</option><option value="338" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;落地灯</option><option value="335" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;吸顶灯</option><option value="339" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;应急灯/手电</option><option value="336" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;筒灯射灯</option><option value="340" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;装饰灯</option><option value="337" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;LED灯</option><option value="334" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;台灯</option><option value="341" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;吊灯</option><option value="2" >男装、女装、内衣、珠宝</option><option value="57" >&nbsp;&nbsp;&nbsp;&nbsp;男装馆</option><option value="83" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;休闲短裤</option><option value="80" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;风衣</option><option value="77" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;休闲裤</option><option value="84" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;POLO衫</option><option value="81" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;针织衫</option><option value="78" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;牛仔裤</option><option value="82" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;长袖衬衫</option><option value="79" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;夹克</option><option value="55" >&nbsp;&nbsp;&nbsp;&nbsp;女装馆</option><option value="67" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;短外套</option><option value="64" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;时尚套装</option><option value="61" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;连衣裙</option><option value="68" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;防晒衫</option><option value="65" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;复古旗袍</option><option value="62" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;连体裤</option><option value="66" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;牛仔裤</option><option value="63" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;棉麻T恤</option><option value="58" >&nbsp;&nbsp;&nbsp;&nbsp;户外鞋服</option><option value="89" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;迷彩裤</option><option value="86" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;篮球鞋</option><option value="90" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;沙滩鞋</option><option value="87" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;帆布鞋</option><option value="91" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;钓鱼服</option><option value="88" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;羽毛球鞋</option><option value="85" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;跑步鞋</option><option value="92" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;登山鞋</option><option value="59" >&nbsp;&nbsp;&nbsp;&nbsp;女鞋馆</option><option value="99" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;乐福鞋</option><option value="96" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;坡跟单鞋</option><option value="93" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;高跟凉拖</option><option value="100" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;松糕鞋</option><option value="97" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;浅口单鞋</option><option value="94" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;平底鞋</option><option value="98" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;帆布鞋</option><option value="95" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;高跟鞋</option><option value="56" >&nbsp;&nbsp;&nbsp;&nbsp;内衣馆</option><option value="73" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;男士内裤</option><option value="70" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;薄款文胸</option><option value="74" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;夏季睡衣</option><option value="71" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;无钢圈文胸</option><option value="75" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;性感睡衣</option><option value="72" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;女士内裤</option><option value="69" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;聚拢文胸</option><option value="76" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;瘦腿袜</option><option value="60" >&nbsp;&nbsp;&nbsp;&nbsp;箱包馆</option><option value="105" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;男士钱包</option><option value="102" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;手提女包</option><option value="106" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;旅行箱</option><option value="103" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;女士钱包</option><option value="107" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;拉杆箱</option><option value="104" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;男士双肩</option><option value="101" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;单肩女包</option><option value="108" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;拉杆包</option><option value="3" >个护化妆、清洁用品</option><option value="113" >&nbsp;&nbsp;&nbsp;&nbsp;香水彩妆</option><option value="137" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;底妆</option><option value="141" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;美甲</option><option value="138" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;腮红</option><option value="142" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;美容工具</option><option value="139" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;眼部</option><option value="136" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;香水</option><option value="143" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;套装</option><option value="140" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;唇部</option><option value="112" >&nbsp;&nbsp;&nbsp;&nbsp;口腔护理</option><option value="134" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;漱口水</option><option value="135" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;套装</option><option value="132" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;牙膏/牙粉</option><option value="133" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;牙刷/牙线</option><option value="109" >&nbsp;&nbsp;&nbsp;&nbsp;面部护肤</option><option value="118" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;套装</option><option value="115" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;护肤</option><option value="116" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;面膜</option><option value="117" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;剃须</option><option value="114" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;清洁</option><option value="110" >&nbsp;&nbsp;&nbsp;&nbsp;洗发护发</option><option value="121" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;染发</option><option value="122" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;造型</option><option value="119" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;洗发</option><option value="123" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;假发</option><option value="120" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;护发</option><option value="124" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;套装</option><option value="111" >&nbsp;&nbsp;&nbsp;&nbsp;身体护肤</option><option value="131" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;套装</option><option value="128" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;手足</option><option value="125" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;沐浴</option><option value="129" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;纤体塑形</option><option value="126" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;润肤</option><option value="130" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 美胸</option><option value="127" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;颈部</option><option value="7" >酒类饮料</option><option value="273" >&nbsp;&nbsp;&nbsp;&nbsp;饮料饮品</option><option value="289" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;果蔬汁</option><option value="296" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;植物蛋白饮料</option><option value="293" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;纯牛奶</option><option value="290" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;茶饮料</option><option value="294" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;酸奶</option><option value="291" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;碳酸饮料</option><option value="295" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;风味奶</option><option value="292" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;功能饮料</option><option value="274" >&nbsp;&nbsp;&nbsp;&nbsp;茗茶</option><option value="305" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;其他茶</option><option value="302" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;红茶</option><option value="299" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;普洱</option><option value="303" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;花果茶</option><option value="300" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;龙井</option><option value="304" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;养生茶</option><option value="301" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;绿茶</option><option value="298" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;铁观音</option><option value="271" >&nbsp;&nbsp;&nbsp;&nbsp;酒水</option><option value="280" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;养生酒</option><option value="277" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;啤酒</option><option value="281" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;预调酒</option><option value="278" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;葡萄酒/果酒</option><option value="279" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;黄酒/米酒</option><option value="276" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;白酒</option><option value="272" >&nbsp;&nbsp;&nbsp;&nbsp;冲调饮品</option><option value="286" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;奶茶</option><option value="283" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;成人奶粉</option><option value="287" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;麦片谷物</option><option value="284" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;豆浆/豆奶粉</option><option value="297" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;果味冲调</option><option value="288" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;咖啡</option><option value="285" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;茶叶</option><option value="282" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;蜂蜜</option><option value="359" >鞋靴、箱包、钟表、奢侈品</option><option value="360" >运动户外</option><option value="361" >汽车、汽车用品</option><option value="8" >母婴、玩具乐器</option><option value="225" >&nbsp;&nbsp;&nbsp;&nbsp;车床/床品</option><option value="257" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;婴儿床</option><option value="261" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;睡袋/抱被</option><option value="258" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;餐椅</option><option value="255" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;安全座椅</option><option value="262" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;凉席/蚊帐</option><option value="259" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;三轮车</option><option value="256" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;手推车</option><option value="260" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;儿童家具</option><option value="222" >&nbsp;&nbsp;&nbsp;&nbsp;营养/辅食</option><option value="238" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;清火开胃</option><option value="235" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;面食类</option><option value="239" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;钙铁锌</option><option value="236" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;宝宝零食</option><option value="233" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;米粉</option><option value="240" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;益生菌</option><option value="237" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;DHA</option><option value="234" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;果汁/泥</option><option value="226" >&nbsp;&nbsp;&nbsp;&nbsp;孕妈专区</option><option value="270" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;孕妇内裤</option><option value="267" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;妈咪包</option><option value="264" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;打底裤</option><option value="268" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;收腹带</option><option value="265" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;防辐射服</option><option value="269" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;哺乳文胸</option><option value="266" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;腰凳</option><option value="263" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;孕妇裙</option><option value="223" >&nbsp;&nbsp;&nbsp;&nbsp;孕婴洗护</option><option value="241" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;洗衣液/皂</option><option value="245" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;爽身粉</option><option value="242" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;宝宝沐浴</option><option value="246" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;奶瓶清洗</option><option value="243" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;儿童防晒</option><option value="247" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;孕妇护肤</option><option value="244" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;防蚊/驱蚊</option><option value="224" >&nbsp;&nbsp;&nbsp;&nbsp;喂养用品</option><option value="254" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;防溢乳垫</option><option value="251" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;水壶/水杯</option><option value="248" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;奶嘴奶瓶</option><option value="252" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;吸奶器</option><option value="249" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;驱蚊用品</option><option value="253" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;理发器</option><option value="250" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;浴室用品</option><option value="221" >&nbsp;&nbsp;&nbsp;&nbsp;孕婴奶粉</option><option value="232" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;3段</option><option value="229" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;pre段</option><option value="230" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;1段</option><option value="227" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;特配奶粉</option><option value="231" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;2段</option><option value="228" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;孕妈奶粉</option><option value="1" >食品、酒类、生鲜、特产</option><option value="14" >&nbsp;&nbsp;&nbsp;&nbsp;进口水果</option><option value="20" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;奇异果猕猴桃</option><option value="17" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;芒果桃李</option><option value="21" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;凤梨蓝莓</option><option value="18" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;龙眼荔枝</option><option value="15" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;释迦芭乐</option><option value="22" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;榴莲山竹</option><option value="19" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;提子葡萄</option><option value="16" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;樱桃车厘子</option><option value="13" >&nbsp;&nbsp;&nbsp;&nbsp;糖果巧克力</option><option value="24" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;巧克力</option><option value="28" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;奶糖</option><option value="25" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;口香糖</option><option value="29" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;QQ糖</option><option value="26" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;棒棒糖</option><option value="30" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;果冻</option><option value="27" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;软糖</option><option value="9" >&nbsp;&nbsp;&nbsp;&nbsp;牛奶乳品</option><option value="37" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;全脂奶</option><option value="34" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;酸奶</option><option value="31" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;常温奶</option><option value="38" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;成人奶粉</option><option value="35" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;豆奶</option><option value="32" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;乳饮料</option><option value="36" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;低脂奶</option><option value="33" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;nbsp;&nbsp;&nbsp;&nbsp;儿童奶</option><option value="10" >&nbsp;&nbsp;&nbsp;&nbsp;坚果炒货</option><option value="40" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;夏威夷果</option><option value="44" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;瓜子</option><option value="41" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;碧根果</option><option value="45" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;花生</option><option value="42" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;开心果</option><option value="39" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;核桃</option><option value="46" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;杏仁</option><option value="43" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;腰果</option><option value="12" >&nbsp;&nbsp;&nbsp;&nbsp;蜜饯果干</option><option value="53" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;橄榄</option><option value="50" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;芒果干</option><option value="47" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;红枣</option><option value="54" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;其他</option><option value="51" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;香蕉干</option><option value="48" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;莓类</option><option value="52" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;山楂片</option><option value="49" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;葡萄干</option><option value="362" >营养保健</option><option value="363" >图书、音像、电子书</option><option value="364" >彩票、旅行、充值、票务</option><option value="365" >理财、众筹、白条、保险</option>          
          </select></td>
      </tr>
      <tr id="measure_unit">
        <td class="label">数量单位:</td>
        <td><input type="text" name='measure_unit' value='1111' size="12" /></td>
      </tr>
      <tr>
        <td class="label">排序:</td>
        <td><input type="text" name='sort_order' value='1' size="15" /></td>
      </tr>
      <tr>
        <td class="label">是否显示:</td>
        <td><input type="radio" name="is_show" value="1"  checked="true"/>
          是          <input type="radio" name="is_show" value="0"  />
          否 </td>
      </tr>
      <tr>
        <td class="label">是否显示在导航栏:</td>
        <td><input type="radio" name="show_in_nav" value="1"  checked="true"/>
          是          <input type="radio" name="show_in_nav" value="0"  />
          否 </td>
      </tr>
      <tr>
        <td class="label">设置为首页推荐:</td>
        <td><input type="checkbox" name="cat_recommend[]" value="1"  checked="true"/>
          精品          <input type="checkbox" name="cat_recommend[]" value="2"  checked="true" />
          最新          <input type="checkbox" name="cat_recommend[]" value="3"  checked="true" />
          热门 </td>
      </tr>
      <tr>
        <td class="label"><a href="javascript:showNotice('noticeFilterAttr');" title="点击此处查看提示信息"><img src="images/notice.gif" width="16" height="16" border="0" alt="您可以为每一个商品分类指定一个样式表文件。例如文件存放在 themes 目录下则输入：themes/style.css"></a>筛选属性:</td>
        <td><script type="text/javascript">
          var arr = new Array();
          var sel_filter_attr = "请选择筛选属性";
                      arr[1] = new Array();
                                          arr[1][0] = ["核数", 12];
                                                        arr[1][1] = ["系统", 8];
                                                        arr[1][2] = ["用途", 4];
                                                        arr[1][3] = ["前置摄像头", 11];
                                                        arr[1][4] = ["有效像素", 7];
                                                        arr[1][5] = ["商品产地", 3];
                                                        arr[1][6] = ["分辨率", 14];
                                                        arr[1][7] = ["后置摄像头", 10];
                                                        arr[1][8] = ["液晶屏尺寸", 6];
                                                        arr[1][9] = ["版本", 2];
                                                        arr[1][10] = ["频率", 13];
                                                        arr[1][11] = ["屏幕尺寸", 9];
                                                        arr[1][12] = ["分类", 5];
                                                        arr[1][13] = ["颜色", 1];
                                                arr[2] = new Array();
                                          arr[2][0] = ["图案", 29];
                                                        arr[2][1] = ["腰型", 25];
                                                        arr[2][2] = ["风格", 18];
                                                        arr[2][3] = ["袖型", 36];
                                                        arr[2][4] = ["袖长", 23];
                                                        arr[2][5] = ["功能", 32];
                                                        arr[2][6] = ["鞋跟形状", 28];
                                                        arr[2][7] = ["制作工艺", 24];
                                                        arr[2][8] = ["帮面材质", 17];
                                                        arr[2][9] = ["领型", 35];
                                                        arr[2][10] = ["裙型", 31];
                                                        arr[2][11] = ["跟高", 27];
                                                        arr[2][12] = ["鞋底材质", 22];
                                                        arr[2][13] = ["尺码", 16];
                                                        arr[2][14] = ["裙长", 34];
                                                        arr[2][15] = ["鞋头", 30];
                                                        arr[2][16] = ["内里材质", 26];
                                                        arr[2][17] = ["闭合方式", 20];
                                                        arr[2][18] = ["版型", 37];
                                                        arr[2][19] = ["颜色", 15];
                                                        arr[2][20] = ["材质", 33];
                                                arr[3] = new Array();
                                          arr[3][0] = ["产地", 39];
                                                        arr[3][1] = ["适用人群", 38];
                                    
          function changeCat(obj)
          {
            var key = obj.value;
            var sel = window.ActiveXObject ? obj.parentNode.childNodes[4] : obj.parentNode.childNodes[5];
            sel.length = 0;
            sel.options[0] = new Option(sel_filter_attr, 0);
            if (arr[key] == undefined)
            {
              return;
            }
            for (var i= 0; i < arr[key].length ;i++ )
            {
              sel.options[i+1] = new Option(arr[key][i][0], arr[key][i][1]);
            }

          }

          </script>

         
          <table width="100%" id="tbody-attr" align="center" class="no_border">
                       
                        <tr>
              <td>
                                    <a href="javascript:;" onclick="addFilterAttr(this)">[+]</a>
                                  <select onChange="changeCat(this)"><option value="0">请选择商品类型</option><option value='1'>手机数码</option><option value='2'>服饰鞋帽</option><option value='3' selected="true">化妆品</option><option value='4'>人人</option></select>&nbsp;&nbsp;
                 <select name="filter_attr[]"><option value="0">请选择筛选属性</option><option value="39">产地</option><option value="38" selected>适用人群</option></select><br />
              </td>
            </tr>
                        <tr>
              <td>
                                    <a href="javascript:;" onclick="removeFilterAttr(this)">[-]&nbsp;</a>
                                  <select onChange="changeCat(this)"><option value="0">请选择商品类型</option><option value='1'>手机数码</option><option value='2'>服饰鞋帽</option><option value='3' selected="true">化妆品</option><option value='4'>人人</option></select>&nbsp;&nbsp;
                 <select name="filter_attr[]"><option value="0">请选择筛选属性</option><option value="39" selected>产地</option><option value="38">适用人群</option></select><br />
              </td>
            </tr>
                      </table>
          <span class="notice-span" style="display:block"  id="noticeFilterAttr">筛选属性可在当前分类页面进行筛选商品</span></td>
      </tr>
       
<!--隐藏lw	  <tr>
		<td class="label">品牌排序:</td>
        <td>
          <input type="text" name="brand_wwwwygkcn" size=60 value="" /> (半角逗号间隔)
        </td>
	  </tr>
	   <tr>
		<td class="label">属性排序:</td>
        <td>
          <textarea name='attr_qq' rows="6" cols="68"></textarea>
		  <br>
		  格式参考： 注意每个属性栏目占一行，如果一行乘不下，别管它，它会自动换行，只要没到下个属性栏目之前别按回车键就行<br><font color=#ff3300>颜色:红色,蓝色,白色,棕色,紫色<br>尺码:M,L,XL,XXL,XXL</font>
        </td>
	  </tr>-->
	  
      <tr>
        <td class="label"><a href="javascript:showNotice('noticeGrade');" title="点击此处查看提示信息"><img src="images/notice.gif" width="16" height="16" border="0" alt="您可以为每一个商品分类指定一个样式表文件。例如文件存放在 themes 目录下则输入：themes/style.css"></a>价格区间个数:</td>
        <td><input type="text" name="grade" value="10" size="40" />
          <br />
          <span class="notice-span" style="display:block"  id="noticeGrade">该选项表示该分类下商品最低价与最高价之间的划分的等级个数，填0表示不做分级，最多不能超过10个。</span></td>
      </tr>
      <tr>
        <td class="label"><a href="javascript:showNotice('noticeGoodsSN');" title="点击此处查看提示信息"><img src="images/notice.gif" width="16" height="16" border="0" alt="您可以为每一个商品分类指定一个样式表文件。例如文件存放在 themes 目录下则输入：themes/style.css"></a>分类的样式表文件:</td>
        <td><input type="text" name="style" value="" size="40" />
          <br />
          <span class="notice-span" style="display:block"  id="noticeGoodsSN">您可以为每一个商品分类指定一个样式表文件。例如文件存放在 themes 目录下则输入：themes/style.css</span></td>
      </tr>
      <tr>
        <td class="label">关键字:</td>
        <td><input type="text" name="keywords" value='' size="50"></td>
      </tr>
      <tr>
        <td class="label">分类描述:</td>
        <td><textarea name='cat_desc' rows="6" cols="48"></textarea></td>
      </tr>
      
      <!--代码修改_start Byjdy--> 
            <tr>
        <td class="label"><font color=#ff3300>是否启用频道首页:</font></td>
        <td>
          <input type="radio" name="category_index" value="1"  onclick="_index_dwt('index_dwt', 1)" >启用
		  <input type="radio" name="category_index" value="0" checked=checked onclick="_index_dwt('index_dwt', 0); _index_dwt('index_dwt_file', 0); change_dwt()" >不启用
		   <br>
		  <span class="notice-span" style="display:block"  id="noticeGoodsSN">注意：只有顶级分类才需要设置“是否启用”，如果您正在编辑的分类属于二级分类，请不用理会这个选项。</span>
        </td>
      </tr>
	  <tr id="index_dwt" style="display:none">
        <td class="label"><font color=#ff3300>是否更改频道首页默认模版:</font></td>
        <td>
          <input id="index_dwt_1" type="radio" name="category_index_dwt" value="1"  onclick="_index_dwt('index_dwt_file', 1)" >更改
		  <input id="index_dwt_0" type="radio" name="category_index_dwt" value="0" checked=checked onclick="_index_dwt('index_dwt_file', 0)" >使用默认
		   <br>
		  <span class="notice-span" style="display:block"  id="noticeGoodsSN">注意：只有启用频道首页才需要设置“是否更改”。</span>
        </td>
      </tr>
	  <tr id="index_dwt_file" style="display:none">
        <td class="label"><font color=#ff3300>选择模版文件:</font></td>
        <td>
		  <input type="text" name="index_dwt_file" value="" size="40" /><br />
          <span class="notice-span" style="display:block"  id="noticeGoodsSN">
			您可以为频道首页指定模版文件，文件请存放在模版文件夹目录下，此处输入文件名，例如category-2.dwt
		  </span>
        </td>
      </tr>	  
	  	  <tr>
        <td class="label"><font color=#ff3300>在频道首页显示该二级分类:</font></td>
        <td><input type="radio" name="show_in_index" value="1" onclick="show_goods_num_area(1)"  >
          显示
          <input type="radio" name="show_in_index" value="0" onclick="show_goods_num_area(0)" checked=checked>
          不显示 <br>
          <span class="notice-span" style="display:block"  id="noticeGoodsSN">注意：只有二级分类才需要设置“是否显示</span></td>
      </tr>
        <tr id="goods_num_area" style="display:none">
            <td class="label"><font color=#ff3300>频道首页该分类下显示商品数量:</font></td>
            <td><input type="text" name="show_goods_num" value='0' size="15"><br />
            <span class="notice-span" style="display:block"  id="noticeGoodsSN">例如：8，该分类在顶级频道以楼层显示时至多显示8个商品</span></td>
        </tr>
    </table>
    <div class="button-div">
      <input type="submit" class="button" value=" 确定 " />
      <input type="reset" class="button" value=" 重置 " />
    </div>
    <input type="hidden" name="act" value="update" />
    <input type="hidden" name="old_cat_name" value="家用电器" />
    <input type="hidden" name="cat_id" value="5" />
    <input type="hidden" name="is_virtual" value="0" />
  </form>
</div>
<script type="text/javascript" src="../js/utils.js"></script><script type="text/javascript" src="js/validator.js"></script> 
<script language="JavaScript">
<!--
document.forms['theForm'].elements['cat_name'].focus();
/**
 * 检查表单输入的数据
 */
function validate()
{
  validator = new Validator("theForm");
  validator.required("cat_name",      catname_empty);
  if (parseInt(document.forms['theForm'].elements['grade'].value) >10 || parseInt(document.forms['theForm'].elements['grade'].value) < 0)
  {
    validator.addErrorMsg('价格分级数量只能是0-10之内的整数');
  }
  return validator.passed();
}
onload = function()
{
  // 开始检查订单
  startCheckOrder();
}

/**
 * 新增一个筛选属性
 */
function addFilterAttr(obj)
{
  var src = obj.parentNode.parentNode;
  var tbl = document.getElementById('tbody-attr');

  var validator  = new Validator('theForm');
  var filterAttr = document.getElementsByName("filter_attr[]");

  if (filterAttr[filterAttr.length-1].selectedIndex == 0)
  {
    validator.addErrorMsg(filter_attr_not_selected);
  }
  
  for (i = 0; i < filterAttr.length; i++)
  {
    for (j = i + 1; j <filterAttr.length; j++)
    {
      if (filterAttr.item(i).value == filterAttr.item(j).value)
      {
        validator.addErrorMsg(filter_attr_not_repeated);
      } 
    } 
  }

  if (!validator.passed())
  {
    return false;
  }

  var row  = tbl.insertRow(tbl.rows.length);
  var cell = row.insertCell(-1);
  cell.innerHTML = src.cells[0].innerHTML.replace(/(.*)(addFilterAttr)(.*)(\[)(\+)/i, "$1removeFilterAttr$3$4-");
  filterAttr[filterAttr.length-1].selectedIndex = 0;
}

/**
 * 删除一个筛选属性
 */
function removeFilterAttr(obj)
{
  var row = rowindex(obj.parentNode.parentNode);
  var tbl = document.getElementById('tbody-attr');

  tbl.deleteRow(row);
}

function _index_dwt(id, type)
{
	if(type == 0){
		$("[name='style']").val("category_index.css");
	}
	document.getElementById(id).style.display = (type == 1 ? "" : "none");
}

function change_dwt()
{
	document.getElementById("index_dwt_0").checked = true;
	document.getElementById("index_dwt_1").checked = false;
}

function show_goods_num_area(show_in_index)
{
    if (show_in_index == 1)
    {
        document.getElementById('goods_num_area').style.display='';
    }
    else
    {
        document.getElementById('goods_num_area').style.display='none';
    }
}
//-->
</script> 

<div id="footer">
共执行 10 个查询，用时 0.037003 秒，Gzip 已禁用，内存占用 3.912 MB<br />
版权所有 &copy; 2008-2030 广州网软志成信息科技有限公司，并保留所有权利。</div>
<!-- 新订单提示信息 -->
<div id="popMsg">
  <table cellspacing="0" cellpadding="0" width="100%" bgcolor="#cfdef4" border="0">
  <tr>
    <td style="color: #0f2c8c" width="30" height="24"></td>
    <td style="font-weight: normal; color: #1f336b; padding-top: 4px;padding-left: 4px" valign="center" width="100%"> 新订单通知</td>
    <td style="padding-top: 2px;padding-right:2px" valign="center" align="right" width="19"><span title="关闭" style="cursor: hand;cursor:pointer;color:red;font-size:12px;font-weight:bold;margin-right:4px;" onclick="Message.close()" >×</span><!-- <img title=关闭 style="cursor: hand" onclick=closediv() hspace=3 src="msgclose.jpg"> --></td>
  </tr>
  <tr>
    <td style="padding-right: 1px; padding-bottom: 1px" colspan="3" height="70">
    <div id="popMsgContent">
      <p>您有 <strong style="color:#ff0000" id="spanNewOrder">1</strong> 个新订单以及       <strong style="color:#ff0000" id="spanNewPaid">0</strong> 个新付款的订单</p>
      <p align="center" style="word-break:break-all"><a href="order.php?act=list"><span style="color:#ff0000">点击查看新订单</span></a></p>
    </div>
    </td>
  </tr>
  </table>
</div>

<!--
<embed src="images/online.wav" width="0" height="0" autostart="false" name="msgBeep" id="msgBeep" enablejavascript="true"/>
-->
<object classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000" codebase="http://active.macromedia.com/flash2/cabs/swflash.cab#version=4,0,0,0" id="msgBeep" width="1" height="1">
  <param name="movie" value="images/online.swf">
  <param name="quality" value="high">
  <embed src="images/online.swf" name="msgBeep" id="msgBeep" quality="high" width="0" height="0" type="application/x-shockwave-flash" pluginspage="http://www.macromedia.com/shockwave/download/index.cgi?p1_prod_version=shockwaveflash">
  </embed>
</object>

<script language="JavaScript">
document.onmousemove=function(e)
{
  var obj = Utils.srcElement(e);
  if (typeof(obj.onclick) == 'function' && obj.onclick.toString().indexOf('listTable.edit') != -1)
  {
    obj.title = '点击修改内容';
    obj.style.cssText = 'background: #278296;';
    obj.onmouseout = function(e)
    {
      this.style.cssText = '';
    }
  }
  else if (typeof(obj.href) != 'undefined' && obj.href.indexOf('listTable.sort') != -1)
  {
    obj.title = '点击对列表排序';
  }
}
<!--


var MyTodolist;
function showTodoList(adminid)
{
  if(!MyTodolist)
  {
    var global = $import("../js/global.js","js");
    global.onload = global.onreadystatechange= function()
    {
      if(this.readyState && this.readyState=="loading")return;
      var md5 = $import("js/md5.js","js");
      md5.onload = md5.onreadystatechange= function()
      {
        if(this.readyState && this.readyState=="loading")return;
        var todolist = $import("js/todolist.js","js");
        todolist.onload = todolist.onreadystatechange = function()
        {
          if(this.readyState && this.readyState=="loading")return;
          MyTodolist = new Todolist();
          MyTodolist.show();
        }
      }
    }
  }
  else
  {
    if(MyTodolist.visibility)
    {
      MyTodolist.hide();
    }
    else
    {
      MyTodolist.show();
    }
  }
}

if (Browser.isIE)
{
  onscroll = function()
  {
    //document.getElementById('calculator').style.top = document.body.scrollTop;
    document.getElementById('popMsg').style.top = (document.body.scrollTop + document.body.clientHeight - document.getElementById('popMsg').offsetHeight) + "px";
  }
}

if (document.getElementById("listDiv"))
{
  document.getElementById("listDiv").onmouseover = function(e)
  {
    obj = Utils.srcElement(e);

    if (obj)
    {
      if (obj.parentNode.tagName.toLowerCase() == "tr") row = obj.parentNode;
      else if (obj.parentNode.parentNode.tagName.toLowerCase() == "tr") row = obj.parentNode.parentNode;
      else return;

      for (i = 0; i < row.cells.length; i++)
      {
        if (row.cells[i].tagName != "TH") row.cells[i].style.backgroundColor = '#F4FAFB';
      }
    }

  }

  document.getElementById("listDiv").onmouseout = function(e)
  {
    obj = Utils.srcElement(e);

    if (obj)
    {
      if (obj.parentNode.tagName.toLowerCase() == "tr") row = obj.parentNode;
      else if (obj.parentNode.parentNode.tagName.toLowerCase() == "tr") row = obj.parentNode.parentNode;
      else return;

      for (i = 0; i < row.cells.length; i++)
      {
          if (row.cells[i].tagName != "TH") row.cells[i].style.backgroundColor = '';
      }
    }
  }

  document.getElementById("listDiv").onclick = function(e)
  {
    var obj = Utils.srcElement(e);

    if (obj.tagName == "INPUT" && obj.type == "checkbox")
    {
      if (!document.forms['listForm'])
      {
        return;
      }
      var nodes = document.forms['listForm'].elements;
      var checked = false;

      for (i = 0; i < nodes.length; i++)
      {
        if (nodes[i].checked)
        {
           checked = true;
           break;
         }
      }

      if(document.getElementById("btnSubmit"))
      {
        document.getElementById("btnSubmit").disabled = !checked;
      }
      for (i = 1; i <= 10; i++)
      {
        if (document.getElementById("btnSubmit" + i))
        {
          document.getElementById("btnSubmit" + i).disabled = !checked;
        }
      }
    }
  }

}

//-->
</script>
</body>
</html>

<!-- $Id: category_move.htm 14216 2008-03-10 02:27:21Z testyang $ -->
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>管理中心 - 转移商品 </title>
<meta name="robots" content="noindex, nofollow">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link href="styles/general.css" rel="stylesheet" type="text/css" />
<link href="styles/main.css" rel="stylesheet" type="text/css" />
<link href="styles/chosen/chosen.css" rel="stylesheet" type="text/css">
<script type="text/javascript" src="js/jquery-1.6.2.min.js"></script><script type="text/javascript" src="js/chosen.jquery.min.js"></script><script type="text/javascript" src="js/jquery.json.js"></script><script type="text/javascript" src="../js/transport.js"></script><script type="text/javascript" src="js/common.js"></script><script language="JavaScript">
<!--
// 这里把JS用到的所有语言都赋值到这里
var process_request = "正在处理您的请求...";
var todolist_caption = "记事本";
var todolist_autosave = "自动保存";
var todolist_save = "保存";
var todolist_clear = "清除";
var todolist_confirm_save = "是否将更改保存到记事本？";
var todolist_confirm_clear = "是否清空内容？";
var catname_empty = "分类名称不能为空!";
var unit_empyt = "数量单位不能为空!";
var is_leafcat = "您选定的分类是一个末级分类。\r\n新分类的上级分类不能是一个末级分类";
var not_leafcat = "您选定的分类不是一个末级分类。\r\n商品的分类转移只能在末级分类之间才可以操作。";
var filter_attr_not_repeated = "筛选属性不可重复";
var filter_attr_not_selected = "请选择筛选属性";
//-->
</script>
</head>
<body>

<div id="menu_list" onmouseover="show_popup()" onmouseout="hide_popup()">
<ul>
<li><a href="goods.php?act=add" target="main_frame">添加新商品</a></li>
<li><a href="category.php?act=add" target="main_frame">添加商品分类</a></li>
<li><a href="order.php?act=add" target="main_frame">添加订单</a></li>
<li><a href="article.php?act=add" target="main_frame">添加新文章</a></li>
<li><a href="users.php?act=add" target="main_frame">添加会员</a></li>
</ul>
</div>
<script>
function show_popup(){
frmBody = parent.document.getElementById('frame-body');
if (frmBody.cols == "37, 12, *")
{
parent.main_frame.document.getElementById('menu_list').style.left = '195px';
}
else
{
parent.main_frame.document.getElementById('menu_list').style.left = '40px';
}
parent.main_frame.document.getElementById('menu_list').style.display = 'block';
}
function hide_popup(){

parent.main_frame.document.getElementById('menu_list').style.display = 'none';
}
</script>
<h1>
<span class="action-span"><a href="category.php?act=list">商品分类</a></span>
<span class="action-span1"><a href="index.php?act=main">管理中心</a> </span><span id="search_id" class="action-span1"> - 转移商品 </span>
<div style="clear:both"></div>
</h1>
<div class="main-div">
<form action="category.php" method="post" name="theForm" enctype="multipart/form-data">
<table width="100%">
  <tr>
    <td>
      <div style="font-weight:bold"><img src="images/notice.gif" width="16" height="16" border="0" /> 什么是转移商品分类?</div>
       <ul>
         <li>在添加商品或者在商品管理中,如果需要对商品的分类进行变更,那么你可以通过此功能,正确管理你的商品分类。</li>
       </ul>
    </td>
  </tr>
  <tr>
    <td colspan="2" align="center">
      <strong>从此分类</strong>&nbsp;&nbsp;
      <select name="cat_id">
       <option value="0">请选择...</option>
       <option value="5" selected='ture'>家用电器</option><option value="182" >&nbsp;&nbsp;&nbsp;&nbsp;五金家装</option><option value="218" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;家具五金</option><option value="219" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;电工电料</option><option value="216" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;手动工具</option><option value="220" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;监控安防</option><option value="217" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;厨卫五金</option><option value="215" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;电动工具</option><option value="179" >&nbsp;&nbsp;&nbsp;&nbsp;生活电器</option><option value="193" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;加湿器</option><option value="197" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;饮水机</option><option value="194" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;吸尘器</option><option value="198" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;其它生活电器</option><option value="195" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;挂烫机/熨斗</option><option value="192" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;净化器</option><option value="191" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;电风扇</option><option value="196" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;取暖电器</option><option value="180" >&nbsp;&nbsp;&nbsp;&nbsp;厨房电器</option><option value="206" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;其它厨房电器</option><option value="203" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;电炖锅</option><option value="200" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;微波炉</option><option value="204" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;果蔬解毒机</option><option value="201" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;电磁炉</option><option value="205" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;养生壶/煎药壶</option><option value="202" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;电饼铛/烧烤盘</option><option value="199" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;电饭煲</option><option value="181" >&nbsp;&nbsp;&nbsp;&nbsp;个护健康</option><option value="209" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;按摩椅</option><option value="213" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;计步器/脂肪检测</option><option value="210" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;足浴盆</option><option value="207" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;剃须刀</option><option value="214" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;其它健康电器</option><option value="211" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;健康秤/厨房秤</option><option value="208" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;电吹风</option><option value="212" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;血糖仪</option><option value="178" >&nbsp;&nbsp;&nbsp;&nbsp;大家电</option><option value="185" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;洗衣机</option><option value="189" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;消毒柜/洗碗机</option><option value="186" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;家庭影院</option><option value="183" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;平板电视</option><option value="190" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;冷柜/冰吧</option><option value="187" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;烟机/灶具</option><option value="184" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;空调冰箱</option><option value="188" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;热水器</option><option value="4" >手机、数码、通信</option><option value="147" >&nbsp;&nbsp;&nbsp;&nbsp;智能设备</option><option value="169" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;体感车</option><option value="166" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;智能眼镜</option><option value="167" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;运动跟踪器</option><option value="164" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;智能手环</option><option value="168" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;智能家居</option><option value="165" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;智能手表</option><option value="144" >&nbsp;&nbsp;&nbsp;&nbsp;热卖手机</option><option value="153" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;联通4G</option><option value="150" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;小米特供</option><option value="154" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;电信4G</option><option value="151" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;魅族手机</option><option value="148" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;三星盖乐世</option><option value="155" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;移动4G</option><option value="152" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;华为荣耀</option><option value="149" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;iPhone</option><option value="145" >&nbsp;&nbsp;&nbsp;&nbsp;手机配件</option><option value="163" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;保护套</option><option value="160" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;创意配件</option><option value="157" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;移动电源</option><option value="161" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;手机饰品</option><option value="158" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;蓝牙耳机</option><option value="162" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;手机耳机</option><option value="159" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;充电器</option><option value="156" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;电池</option><option value="146" >&nbsp;&nbsp;&nbsp;&nbsp;数码影音</option><option value="176" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;数码相框</option><option value="173" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;运动相机</option><option value="170" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;数码相机</option><option value="177" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;影棚器材</option><option value="174" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;摄像机</option><option value="171" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;单反相机</option><option value="175" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;户外器材</option><option value="172" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;拍立得</option><option value="358" >电脑、办公</option><option value="6" >家居、家具、家装、厨具</option><option value="310" >&nbsp;&nbsp;&nbsp;&nbsp;家装软饰</option><option value="348" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;墙贴/装饰贴</option><option value="345" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;帘艺隔断</option><option value="342" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;桌布/罩件</option><option value="349" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;摆件花瓶</option><option value="346" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;相框/照片墙</option><option value="343" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;地毯地垫</option><option value="347" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;装饰字画</option><option value="344" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;沙发垫套/椅垫</option><option value="307" >&nbsp;&nbsp;&nbsp;&nbsp;家具</option><option value="322" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;餐厅家具</option><option value="326" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;沙发</option><option value="323" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;书房家具</option><option value="320" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;卧室家具</option><option value="327" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;鞋架/衣帽架</option><option value="324" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;储物家具</option><option value="321" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;客厅家具</option><option value="325" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;阳台/户外</option><option value="311" >&nbsp;&nbsp;&nbsp;&nbsp;生活日用</option><option value="354" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;洗晒/熨烫</option><option value="351" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;雨伞雨具</option><option value="355" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;净化除味</option><option value="352" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;浴室用品</option><option value="353" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;缝纫/针织用品</option><option value="350" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;收纳用品</option><option value="306" >&nbsp;&nbsp;&nbsp;&nbsp;家纺</option><option value="319" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;窗帘/窗纱</option><option value="316" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;床单被罩</option><option value="313" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;被子</option><option value="317" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;毛巾浴巾</option><option value="314" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;蚊帐</option><option value="318" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;床垫/床褥</option><option value="315" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;凉席</option><option value="312" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;床品套件</option><option value="308" >&nbsp;&nbsp;&nbsp;&nbsp;厨具</option><option value="332" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;餐具</option><option value="329" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;刀剪菜板</option><option value="333" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;茶具/咖啡具</option><option value="330" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;厨房配件</option><option value="331" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;水具酒具</option><option value="328" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;烹饪锅具</option><option value="309" >&nbsp;&nbsp;&nbsp;&nbsp;灯具</option><option value="338" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;落地灯</option><option value="335" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;吸顶灯</option><option value="339" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;应急灯/手电</option><option value="336" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;筒灯射灯</option><option value="340" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;装饰灯</option><option value="337" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;LED灯</option><option value="334" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;台灯</option><option value="341" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;吊灯</option><option value="2" >男装、女装、内衣、珠宝</option><option value="57" >&nbsp;&nbsp;&nbsp;&nbsp;男装馆</option><option value="83" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;休闲短裤</option><option value="80" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;风衣</option><option value="77" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;休闲裤</option><option value="84" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;POLO衫</option><option value="81" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;针织衫</option><option value="78" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;牛仔裤</option><option value="82" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;长袖衬衫</option><option value="79" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;夹克</option><option value="55" >&nbsp;&nbsp;&nbsp;&nbsp;女装馆</option><option value="67" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;短外套</option><option value="64" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;时尚套装</option><option value="61" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;连衣裙</option><option value="68" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;防晒衫</option><option value="65" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;复古旗袍</option><option value="62" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;连体裤</option><option value="66" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;牛仔裤</option><option value="63" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;棉麻T恤</option><option value="58" >&nbsp;&nbsp;&nbsp;&nbsp;户外鞋服</option><option value="89" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;迷彩裤</option><option value="86" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;篮球鞋</option><option value="90" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;沙滩鞋</option><option value="87" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;帆布鞋</option><option value="91" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;钓鱼服</option><option value="88" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;羽毛球鞋</option><option value="85" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;跑步鞋</option><option value="92" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;登山鞋</option><option value="59" >&nbsp;&nbsp;&nbsp;&nbsp;女鞋馆</option><option value="99" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;乐福鞋</option><option value="96" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;坡跟单鞋</option><option value="93" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;高跟凉拖</option><option value="100" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;松糕鞋</option><option value="97" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;浅口单鞋</option><option value="94" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;平底鞋</option><option value="98" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;帆布鞋</option><option value="95" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;高跟鞋</option><option value="56" >&nbsp;&nbsp;&nbsp;&nbsp;内衣馆</option><option value="73" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;男士内裤</option><option value="70" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;薄款文胸</option><option value="74" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;夏季睡衣</option><option value="71" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;无钢圈文胸</option><option value="75" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;性感睡衣</option><option value="72" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;女士内裤</option><option value="69" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;聚拢文胸</option><option value="76" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;瘦腿袜</option><option value="60" >&nbsp;&nbsp;&nbsp;&nbsp;箱包馆</option><option value="105" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;男士钱包</option><option value="102" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;手提女包</option><option value="106" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;旅行箱</option><option value="103" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;女士钱包</option><option value="107" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;拉杆箱</option><option value="104" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;男士双肩</option><option value="101" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;单肩女包</option><option value="108" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;拉杆包</option><option value="3" >个护化妆、清洁用品</option><option value="113" >&nbsp;&nbsp;&nbsp;&nbsp;香水彩妆</option><option value="137" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;底妆</option><option value="141" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;美甲</option><option value="138" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;腮红</option><option value="142" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;美容工具</option><option value="139" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;眼部</option><option value="136" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;香水</option><option value="143" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;套装</option><option value="140" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;唇部</option><option value="112" >&nbsp;&nbsp;&nbsp;&nbsp;口腔护理</option><option value="134" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;漱口水</option><option value="135" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;套装</option><option value="132" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;牙膏/牙粉</option><option value="133" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;牙刷/牙线</option><option value="109" >&nbsp;&nbsp;&nbsp;&nbsp;面部护肤</option><option value="118" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;套装</option><option value="115" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;护肤</option><option value="116" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;面膜</option><option value="117" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;剃须</option><option value="114" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;清洁</option><option value="110" >&nbsp;&nbsp;&nbsp;&nbsp;洗发护发</option><option value="121" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;染发</option><option value="122" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;造型</option><option value="119" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;洗发</option><option value="123" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;假发</option><option value="120" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;护发</option><option value="124" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;套装</option><option value="111" >&nbsp;&nbsp;&nbsp;&nbsp;身体护肤</option><option value="131" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;套装</option><option value="128" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;手足</option><option value="125" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;沐浴</option><option value="129" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;纤体塑形</option><option value="126" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;润肤</option><option value="130" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 美胸</option><option value="127" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;颈部</option><option value="7" >酒类饮料</option><option value="273" >&nbsp;&nbsp;&nbsp;&nbsp;饮料饮品</option><option value="289" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;果蔬汁</option><option value="296" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;植物蛋白饮料</option><option value="293" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;纯牛奶</option><option value="290" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;茶饮料</option><option value="294" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;酸奶</option><option value="291" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;碳酸饮料</option><option value="295" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;风味奶</option><option value="292" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;功能饮料</option><option value="274" >&nbsp;&nbsp;&nbsp;&nbsp;茗茶</option><option value="305" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;其他茶</option><option value="302" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;红茶</option><option value="299" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;普洱</option><option value="303" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;花果茶</option><option value="300" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;龙井</option><option value="304" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;养生茶</option><option value="301" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;绿茶</option><option value="298" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;铁观音</option><option value="271" >&nbsp;&nbsp;&nbsp;&nbsp;酒水</option><option value="280" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;养生酒</option><option value="277" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;啤酒</option><option value="281" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;预调酒</option><option value="278" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;葡萄酒/果酒</option><option value="279" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;黄酒/米酒</option><option value="276" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;白酒</option><option value="272" >&nbsp;&nbsp;&nbsp;&nbsp;冲调饮品</option><option value="286" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;奶茶</option><option value="283" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;成人奶粉</option><option value="287" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;麦片谷物</option><option value="284" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;豆浆/豆奶粉</option><option value="297" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;果味冲调</option><option value="288" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;咖啡</option><option value="285" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;茶叶</option><option value="282" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;蜂蜜</option><option value="359" >鞋靴、箱包、钟表、奢侈品</option><option value="360" >运动户外</option><option value="361" >汽车、汽车用品</option><option value="8" >母婴、玩具乐器</option><option value="225" >&nbsp;&nbsp;&nbsp;&nbsp;车床/床品</option><option value="257" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;婴儿床</option><option value="261" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;睡袋/抱被</option><option value="258" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;餐椅</option><option value="255" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;安全座椅</option><option value="262" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;凉席/蚊帐</option><option value="259" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;三轮车</option><option value="256" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;手推车</option><option value="260" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;儿童家具</option><option value="222" >&nbsp;&nbsp;&nbsp;&nbsp;营养/辅食</option><option value="238" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;清火开胃</option><option value="235" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;面食类</option><option value="239" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;钙铁锌</option><option value="236" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;宝宝零食</option><option value="233" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;米粉</option><option value="240" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;益生菌</option><option value="237" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;DHA</option><option value="234" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;果汁/泥</option><option value="226" >&nbsp;&nbsp;&nbsp;&nbsp;孕妈专区</option><option value="270" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;孕妇内裤</option><option value="267" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;妈咪包</option><option value="264" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;打底裤</option><option value="268" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;收腹带</option><option value="265" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;防辐射服</option><option value="269" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;哺乳文胸</option><option value="266" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;腰凳</option><option value="263" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;孕妇裙</option><option value="223" >&nbsp;&nbsp;&nbsp;&nbsp;孕婴洗护</option><option value="241" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;洗衣液/皂</option><option value="245" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;爽身粉</option><option value="242" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;宝宝沐浴</option><option value="246" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;奶瓶清洗</option><option value="243" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;儿童防晒</option><option value="247" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;孕妇护肤</option><option value="244" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;防蚊/驱蚊</option><option value="224" >&nbsp;&nbsp;&nbsp;&nbsp;喂养用品</option><option value="254" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;防溢乳垫</option><option value="251" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;水壶/水杯</option><option value="248" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;奶嘴奶瓶</option><option value="252" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;吸奶器</option><option value="249" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;驱蚊用品</option><option value="253" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;理发器</option><option value="250" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;浴室用品</option><option value="221" >&nbsp;&nbsp;&nbsp;&nbsp;孕婴奶粉</option><option value="232" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;3段</option><option value="229" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;pre段</option><option value="230" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;1段</option><option value="227" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;特配奶粉</option><option value="231" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;2段</option><option value="228" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;孕妈奶粉</option><option value="1" >食品、酒类、生鲜、特产</option><option value="14" >&nbsp;&nbsp;&nbsp;&nbsp;进口水果</option><option value="20" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;奇异果猕猴桃</option><option value="17" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;芒果桃李</option><option value="21" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;凤梨蓝莓</option><option value="18" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;龙眼荔枝</option><option value="15" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;释迦芭乐</option><option value="22" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;榴莲山竹</option><option value="19" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;提子葡萄</option><option value="16" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;樱桃车厘子</option><option value="13" >&nbsp;&nbsp;&nbsp;&nbsp;糖果巧克力</option><option value="24" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;巧克力</option><option value="28" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;奶糖</option><option value="25" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;口香糖</option><option value="29" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;QQ糖</option><option value="26" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;棒棒糖</option><option value="30" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;果冻</option><option value="27" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;软糖</option><option value="9" >&nbsp;&nbsp;&nbsp;&nbsp;牛奶乳品</option><option value="37" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;全脂奶</option><option value="34" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;酸奶</option><option value="31" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;常温奶</option><option value="38" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;成人奶粉</option><option value="35" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;豆奶</option><option value="32" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;乳饮料</option><option value="36" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;低脂奶</option><option value="33" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;儿童奶</option><option value="10" >&nbsp;&nbsp;&nbsp;&nbsp;坚果炒货</option><option value="40" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;夏威夷果</option><option value="44" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;瓜子</option><option value="41" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;碧根果</option><option value="45" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;花生</option><option value="42" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;开心果</option><option value="39" >&nbsp;&nbsp;&nbsnbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;核桃</option><option value="46" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;杏仁</option><option value="43" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;腰果</option><option value="12" >&nbsp;&nbsp;&nbsp;&nbsp;蜜饯果干</option><option value="53" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;橄榄</option><option value="50" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;芒果干</option><option value="47" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;红枣</option><option value="54" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;其他</option><option value="51" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;香蕉干</option><option value="48" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;莓类</option><option value="52" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;山楂片</option><option value="49" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;葡萄干</option><option value="362" >营养保健</option><option value="363" >图书、音像、电子书</option><option value="364" >彩票、旅行、充值、票务</option><option value="365" >理财、众筹、白条、保险</option>      </select>&nbsp;&nbsp;
      <strong>转移到</strong>
      <select name="target_cat_id">
       <option value="0">请选择...</option>
       <option value="5" selected='ture'>家用电器</option><option value="182" >&nbsp;&nbsp;&nbsp;&nbsp;五金家装</option><option value="218" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;家具五金</option><option value="219" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;电工电料</option><option value="216" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;手动工具</option><option value="220" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;监控安防</option><option value="217" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;厨卫五金</option><option value="215" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;电动工具</option><option value="179" >&nbsp;&nbsp;&nbsp;&nbsp;生活电器</option><option value="193" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;加湿器</option><option value="197" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;饮水机</option><option value="194" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;吸尘器</option><option value="198" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;其它生活电器</option><option value="195" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;挂烫机/熨斗</option><option value="192" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;净化器</option><option value="191" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;电风扇</option><option value="196" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;取暖电器</option><option value="180" >&nbsp;&nbsp;&nbsp;&nbsp;厨房电器</option><option value="206" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;其它厨房电器</option><option value="203" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;电炖锅</option><option value="200" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;微波炉</option><option value="204" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;果蔬解毒机</option><option value="201" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;电磁炉</option><option value="205" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;养生壶/煎药壶</option><option value="202" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;电饼铛/烧烤盘</option><option value="199" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;电饭煲</option><option value="181" >&nbsp;&nbsp;&nbsp;&nbsp;个护健康</option><option value="209" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;按摩椅</option><option value="213" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;计步器/脂肪检测</option><option value="210" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;足浴盆</option><option value="207" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;剃须刀</option><option value="214" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;其它健康电器</option><option value="211" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;健康秤/厨房秤</option><option value="208" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;电吹风</option><option value="212" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;血糖仪</option><option value="178" >&nbsp;&nbsp;&nbsp;&nbsp;大家电</option><option value="185" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;洗衣机</option><option value="189" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;消毒柜/洗碗机</option><option value="186" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;家庭影院</option><option value="183" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;平板电视</option><option value="190" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;冷柜/冰吧</option><option value="187" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;烟机/灶具</option><option value="184" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;空调冰箱</option><option value="188" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;热水器</option><option value="4" >手机、数码、通信</option><option value="147" >&nbsp;&nbsp;&nbsp;&nbsp;智能设备</option><option value="169" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;体感车</option><option value="166" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;智能眼镜</option><option value="167" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;运动跟踪器</option><option value="164" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;智能手环</option><option value="168" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;智能家居</option><option value="165" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;智能手表</option><option value="144" >&nbsp;&nbsp;&nbsp;&nbsp;热卖手机</option><option value="153" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;联通4G</option><option value="150" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;小米特供</option><option value="154" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;电信4G</option><option value="151" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;魅族手机</option><option value="148" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;三星盖乐世</option><option value="155" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;移动4G</option><option value="152" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;华为荣耀</option><option value="149" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;iPhone</option><option value="145" >&nbsp;&nbsp;&nbsp;&nbsp;手机配件</option><option value="163" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;保护套</option><option value="160" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;创意配件</option><option value="157" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;移动电源</option><option value="161" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;手机饰品</option><option value="158" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;蓝牙耳机</option><option value="162" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;手机耳机</option><option value="159" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;充电器</option><option value="156" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;电池</option><option value="146" >&nbsp;&nbsp;&nbsp;&nbsp;数码影音</option><option value="176" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;数码相框</option><option value="173" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;运动相机</option><option value="170" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;数码相机</option><option value="177" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;影棚器材</option><option value="174" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;摄像机</option><option value="171" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;单反相机</option><option value="175" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;户外器材</option><option value="172" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;拍立得</option><option value="358" >电脑、办公</option><option value="6" >家居、家具、家装、厨具</option><option value="310" >&nbsp;&nbsp;&nbsp;&nbsp;家装软饰</option><option value="348" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;墙贴/装饰贴</option><option value="345" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;帘艺隔断</option><option value="342" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;桌布/罩件</option><option value="349" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;摆件花瓶</option><option value="346" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;相框/照片墙</option><option value="343" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;地毯地垫</option><option value="347" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;装饰字画</option><option value="344" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;沙发垫套/椅垫</option><option value="307" >&nbsp;&nbsp;&nbsp;&nbsp;家具</option><option value="322" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;餐厅家具</option><option value="326" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;沙发</option><option value="323" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;书房家具</option><option value="320" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;卧室家具</option><option value="327" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;鞋架/衣帽架</option><option value="324" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;储物家具</option><option value="321" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;客厅家具</option><option value="325" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;阳台/户外</option><option value="311" >&nbsp;&nbsp;&nbsp;&nbsp;生活日用</option><option value="354" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;洗晒/熨烫</option><option value="351" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;雨伞雨具</option><option value="355" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;净化除味</option><option value="352" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;浴室用品</option><option value="353" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;缝纫/针织用品</option><option value="350" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;收纳用品</option><option value="306" >&nbsp;&nbsp;&nbsp;&nbsp;家纺</option><option value="319" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;窗帘/窗纱</option><option value="316" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;床单被罩</option><option value="313" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;被子</option><option value="317" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;毛巾浴巾</option><option value="314" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;蚊帐</option><option value="318" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;床垫/床褥</option><option value="315" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;凉席</option><option value="312" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;床品套件</option><option value="308" >&nbsp;&nbsp;&nbsp;&nbsp;厨具</option><option value="332" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;餐具</option><option value="329" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;刀剪菜板</option><option value="333" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;茶具/咖啡具</option><option value="330" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;厨房配件</option><option value="331" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;水具酒具</option><option value="328" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;烹饪锅具</option><option value="309" >&nbsp;&nbsp;&nbsp;&nbsp;灯具</option><option value="338" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;落地灯</option><option value="335" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;吸顶灯</option><option value="339" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;应急灯/手电</option><option value="336" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;筒灯射灯</option><option value="340" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;装饰灯</option><option value="337" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;LED灯</option><option value="334" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;台灯</option><option value="341" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;吊灯</option><option value="2" >男装、女装、内衣、珠宝</option><option value="57" >&nbsp;&nbsp;&nbsp;&nbsp;男装馆</option><option value="83" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;休闲短裤</option><option value="80" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;风衣</option><option value="77" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;休闲裤</option><option value="84" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;POLO衫</option><option value="81" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;针织衫</option><option value="78" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;牛仔裤</option><option value="82" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;长袖衬衫</option><option value="79" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;夹克</option><option value="55" >&nbsp;&nbsp;&nbsp;&nbsp;女装馆</option><option value="67" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;短外套</option><option value="64" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;时尚套装</option><option value="61" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;连衣裙</option><option value="68" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;防晒衫</option><option value="65" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;复古旗袍</option><option value="62" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;连体裤</option><option value="66" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;牛仔裤</option><option value="63" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;棉麻T恤</option><option value="58" >&nbsp;&nbsp;&nbsp;&nbsp;户外鞋服</option><option value="89" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;迷彩裤</option><option value="86" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;篮球鞋</option><option value="90" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;沙滩鞋</option><option value="87" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;帆布鞋</option><option value="91" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;钓鱼服</option><option value="88" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;羽毛球鞋</option><option value="85" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;跑步鞋</option><option value="92" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;登山鞋</option><option value="59" >&nbsp;&nbsp;&nbsp;&nbsp;女鞋馆</option><option value="99" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;乐福鞋</option><option value="96" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;坡跟单鞋</option><option value="93" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;高跟凉拖</option><option value="100" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;松糕鞋</option><option value="97" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;浅口单鞋</option><option value="94" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;平底鞋</option><option value="98" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;帆布鞋</option><option value="95" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;高跟鞋</option><option value="56" >&nbsp;&nbsp;&nbsp;&nbsp;内衣馆</option><option value="73" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;男士内裤</option><option value="70" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;薄款文胸</option><option value="74" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;夏季睡衣</option><option value="71" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;无钢圈文胸</option><option value="75" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;性感睡衣</option><option value="72" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;女士内裤</option><option value="69" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;聚拢文胸</option><option value="76" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;瘦腿袜</option><option value="60" >&nbsp;&nbsp;&nbsp;&nbsp;箱包馆</option><option value="105" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;男士钱包</option><option value="102" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;手提女包</option><option value="106" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;旅行箱</option><option value="103" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;女士钱包</option><option value="107" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;拉杆箱</option><option value="104" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;男士双肩</option><option value="101" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;单肩女包</option><option value="108" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;拉杆包</option><option value="3" >个护化妆、清洁用品</option><option value="113" >&nbsp;&nbsp;&nbsp;&nbsp;香水彩妆</option><option value="137" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;底妆</option><option value="141" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;美甲</option><option value="138" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;腮红</option><option value="142" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;美容工具</option><option value="139" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;眼部</option><option value="136" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;香水</option><option value="143" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;套装</option><option value="140" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;唇部</option><option value="112" >&nbsp;&nbsp;&nbsp;&nbsp;口腔护理</option><option value="134" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;漱口水</option><option value="135" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;套装</option><option value="132" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;牙膏/牙粉</option><option value="133" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;牙刷/牙线</option><option value="109" >&nbsp;&nbsp;&nbsp;&nbsp;面部护肤</option><option value="118" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;套装</option><option value="115" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;护肤</option><option value="116" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;面膜</option><option value="117" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;剃须</option><option value="114" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;清洁</option><option value="110" >&nbsp;&nbsp;&nbsp;&nbsp;洗发护发</option><option value="121" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;染发</option><option value="122" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;造型</option><option value="119" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;洗发</option><option value="123" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;假发</option><option value="120" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;护发</option><option value="124" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;套装</option><option value="111" >&nbsp;&nbsp;&nbsp;&nbsp;身体护肤</option><option value="131" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;套装</option><option value="128" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;手足</option><option value="125" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;沐浴</option><option value="129" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;纤体塑形</option><option value="126" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;润肤</option><option value="130" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 美胸</option><option value="127" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;颈部</option><option value="7" >酒类饮料</option><option value="273" >&nbsp;&nbsp;&nbsp;&nbsp;饮料饮品</option><option value="289" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;果蔬汁</option><option value="296" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;植物蛋白饮料</option><option value="293" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;纯牛奶</option><option value="290" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;茶饮料</option><option value="294" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;酸奶</option><option value="291" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;碳酸饮料</option><option value="295" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;风味奶</option><option value="292" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;功能饮料</option><option value="274" >&nbsp;&nbsp;&nbsp;&nbsp;茗茶</option><option value="305" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;其他茶</option><option value="302" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;红茶</option><option value="299" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;普洱</option><option value="303" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;花果茶</option><option value="300" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;龙井</option><option value="304" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;养生茶</option><option value="301" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;绿茶</option><option value="298" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;铁观音</option><option value="271" >&nbsp;&nbsp;&nbsp;&nbsp;酒水</option><option value="280" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;养生酒</option><option value="277" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;啤酒</option><option value="281" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;预调酒</option><option value="278" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;葡萄酒/果酒</option><option value="279" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;黄酒/米酒</option><option value="276" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;白酒</option><option value="272" >&nbsp;&nbsp;&nbsp;&nbsp;冲调饮品</option><option value="286" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;奶茶</option><option value="283" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;成人奶粉</option><option value="287" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;麦片谷物</option><option value="284" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;豆浆/豆奶粉</option><option value="297" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;果味冲调</option><option value="288" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;咖啡</option><option value="285" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;茶叶</option><option value="282" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;蜂蜜</option><option value="359" >鞋靴、箱包、钟表、奢侈品</option><option value="360" >运动户外</option><option value="361" >汽车、汽车用品</option><option value="8" >母婴、玩具乐器</option><option value="225" >&nbsp;&nbsp;&nbsp;&nbsp;车床/床品</option><option value="257" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;婴儿床</option><option value="261" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;睡袋/抱被</option><option value="258" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;餐椅</option><option value="255" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;安全座椅</option><option value="262" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;凉席/蚊帐</option><option value="259" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;三轮车</option><option value="256" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;手推车</option><option value="260" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;儿童家具</option><option value="222" >&nbsp;&nbsp;&nbsp;&nbsp;营养/辅食</option><option value="238" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;清火开胃</option><option value="235" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;面食类</option><option value="239" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;钙铁锌</option><option value="236" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;宝宝零食</option><option value="233" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;米粉</option><option value="240" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;益生菌</option><option value="237" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;DHA</option><option value="234" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;果汁/泥</option><option value="226" >&nbsp;&nbsp;&nbsp;&nbsp;孕妈专区</option><option value="270" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;孕妇内裤</option><option value="267" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;妈咪包</option><option value="264" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;打底裤</option><option value="268" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;收腹带</option><option value="265" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;防辐射服</option><option value="269" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;哺乳文胸</option><option value="266" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;腰凳</option><option value="263" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;孕妇裙</option><option value="223" >&nbsp;&nbsp;&nbsp;&nbsp;孕婴洗护</option><option value="241" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;洗衣液/皂</option><option value="245" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;爽身粉</option><option value="242" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;宝宝沐浴</option><option value="246" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;奶瓶清洗</option><option value="243" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;儿童防晒</option><option value="247" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;孕妇护肤</option><option value="244" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;防蚊/驱蚊</option><option value="224" >&nbsp;&nbsp;&nbsp;&nbsp;喂养用品</option><option value="254" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;防溢乳垫</option><option value="251" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;水壶/水杯</option><option value="248" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;奶嘴奶瓶</option><option value="252" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;吸奶器</option><option value="249" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;驱蚊用品</option><option value="253" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;理发器</option><option value="250" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;浴室用品</option><option value="221" >&nbsp;&nbsp;&nbsp;&nbsp;孕婴奶粉</option><option value="232" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;3段</option><option value="229" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;pre段</option><option value="230" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;1段</option><option value="227" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;特配奶粉</option><option value="231" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;2段</option><option value="228" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;孕妈奶粉</option><option value="1" >食品、酒类、生鲜、特产</option><option value="14" >&nbsp;&nbsp;&nbsp;&nbsp;进口水果</option><option value="20" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;奇异果猕猴桃</option><option value="17" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;芒果桃李</option><option value="21" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;凤梨蓝莓</option><option value="18" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;龙眼荔枝</option><option value="15" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;释迦芭乐</option><option value="22" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;榴莲山竹</option><option value="19" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;提子葡萄</option><option value="16" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;樱桃车厘子</option><option value="13" >&nbsp;&nbsp;&nbsp;&nbsp;糖果巧克力</option><option value="24" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;巧克力</option><option value="28" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;奶糖</option><option value="25" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;口香糖</option><option value="29" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;QQ糖</option><option value="26" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;棒棒糖</option><option value="30" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;果冻</option><option value="27" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;软糖</option><option value="9" >&nbsp;&nbsp;&nbsp;&nbsp;牛奶乳品</option><option value="37" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;全脂奶</option><option value="34" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;酸奶</option><option value="31" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;常温奶</option><option value="38" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;成人奶粉</option><option value="35" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;豆奶</option><option value="32" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;乳饮料</option><option value="36" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;低脂奶</option><option value="33" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;儿童奶</option><option value="10" >&nbsp;&nbsp;&nbsp;&nbsp;坚果炒货</option><option value="40" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;夏威夷果</option><option value="44" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;瓜子</option><option value="41" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;碧根果</option><option value="45" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;花生</option><option value="42" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;开心果</option><option value="39" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;核桃</option><option value="46" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;杏仁</option><option value="43" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;腰果</option><option value="12" >&nbsp;&nbsp;&nbsp;&nbsp;蜜饯果干</option><option value="53" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;橄榄</option><option value="50" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;芒果干</option><option value="47" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;红枣</option><option value="54" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;其他</option><option value="51" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;香蕉干</option><option value="48" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;莓类</option><option value="52" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;山楂片</option><option value="49" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;葡萄干</option><option value="362" >营养保健</option><option value="363" >图书、音像、电子书</option><option value="364" >彩票、旅行、充值、票务</option><option value="365" >理财、众筹、白条、保险</option>      </select>&nbsp;&nbsp;&nbsp;&nbsp;
      <input type="submit" name="move_cat" value="开始转移" class="button">
      <input type="reset" value=" 重置 " class="button" />
      <input type="hidden" name="act" value="move_cat" />
    </td>
  </tr>
</table>
</form>
</div>

<script type="text/javascript" src="../js/utils.js"></script><script type="text/javascript" src="js/validator.js"></script>
<script language="JavaScript">
<!--
onload = function()
{
    // 开始检查订单
    startCheckOrder();
}

//-->
</script>

<div id="footer">
共执行 2 个查询，用时 0.021001 秒，Gzip 已禁用，内存占用 3.892 MB<br />
版权所有 &copy; 2008-2030 广州网软志成信息科技有限公司，并保留所有权利。</div>
<!-- 新订单提示信息 -->
<div id="popMsg">
  <table cellspacing="0" cellpadding="0" width="100%" bgcolor="#cfdef4" border="0">
  <tr>
    <td style="color: #0f2c8c" width="30" height="24"></td>
    <td style="font-weight: normal; color: #1f336b; padding-top: 4px;padding-left: 4px" valign="center" width="100%"> 新订单通知</td>
    <td style="padding-top: 2px;padding-right:2px" valign="center" align="right" width="19"><span title="关闭" style="cursor: hand;cursor:pointer;color:red;font-size:12px;font-weight:bold;margin-right:4px;" onclick="Message.close()" >×</span><!-- <img title=关闭 style="cursor: hand" onclick=closediv() hspace=3 src="msgclose.jpg"> --></td>
  </tr>
  <tr>
    <td style="padding-right: 1px; padding-bottom: 1px" colspan="3" height="70">
    <div id="popMsgContent">
      <p>您有 <strong style="color:#ff0000" id="spanNewOrder">1</strong> 个新订单以及       <strong style="color:#ff0000" id="spanNewPaid">0</strong> 个新付款的订单</p>
      <p align="center" style="word-break:break-all"><a href="order.php?act=list"><span style="color:#ff0000">点击查看新订单</span></a></p>
    </div>
    </td>
  </tr>
  </table>
</div>

<!--
<embed src="images/online.wav" width="0" height="0" autostart="false" name="msgBeep" id="msgBeep" enablejavascript="true"/>
-->
<object classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000" codebase="http://active.macromedia.com/flash2/cabs/swflash.cab#version=4,0,0,0" id="msgBeep" width="1" height="1">
  <param name="movie" value="images/online.swf">
  <param name="quality" value="high">
  <embed src="images/online.swf" name="msgBeep" id="msgBeep" quality="high" width="0" height="0" type="application/x-shockwave-flash" pluginspage="http://www.macromedia.com/shockwave/download/index.cgi?p1_prod_version=shockwaveflash">
  </embed>
</object>

<script language="JavaScript">
document.onmousemove=function(e)
{
  var obj = Utils.srcElement(e);
  if (typeof(obj.onclick) == 'function' && obj.onclick.toString().indexOf('listTable.edit') != -1)
  {
    obj.title = '点击修改内容';
    obj.style.cssText = 'background: #278296;';
    obj.onmouseout = function(e)
    {
      this.style.cssText = '';
    }
  }
  else if (typeof(obj.href) != 'undefined' && obj.href.indexOf('listTable.sort') != -1)
  {
    obj.title = '点击对列表排序';
  }
}
<!--


var MyTodolist;
function showTodoList(adminid)
{
  if(!MyTodolist)
  {
    var global = $import("../js/global.js","js");
    global.onload = global.onreadystatechange= function()
    {
      if(this.readyState && this.readyState=="loading")return;
      var md5 = $import("js/md5.js","js");
      md5.onload = md5.onreadystatechange= function()
      {
        if(this.readyState && this.readyState=="loading")return;
        var todolist = $import("js/todolist.js","js");
        todolist.onload = todolist.onreadystatechange = function()
        {
          if(this.readyState && this.readyState=="loading")return;
          MyTodolist = new Todolist();
          MyTodolist.show();
        }
      }
    }
  }
  else
  {
    if(MyTodolist.visibility)
    {
      MyTodolist.hide();
    }
    else
    {
      MyTodolist.show();
    }
  }
}

if (Browser.isIE)
{
  onscroll = function()
  {
    //document.getElementById('calculator').style.top = document.body.scrollTop;
    document.getElementById('popMsg').style.top = (document.body.scrollTop + document.body.clientHeight - document.getElementById('popMsg').offsetHeight) + "px";
  }
}

if (document.getElementById("listDiv"))
{
  document.getElementById("listDiv").onmouseover = function(e)
  {
    obj = Utils.srcElement(e);

    if (obj)
    {
      if (obj.parentNode.tagName.toLowerCase() == "tr") row = obj.parentNode;
      else if (obj.parentNode.parentNode.tagName.toLowerCase() == "tr") row = obj.parentNode.parentNode;
      else return;

      for (i = 0; i < row.cells.length; i++)
      {
        if (row.cells[i].tagName != "TH") row.cells[i].style.backgroundColor = '#F4FAFB';
      }
    }

  }

  document.getElementById("listDiv").onmouseout = function(e)
  {
    obj = Utils.srcElement(e);

    if (obj)
    {
      if (obj.parentNode.tagName.toLowerCase() == "tr") row = obj.parentNode;
      else if (obj.parentNode.parentNode.tagName.toLowerCase() == "tr") row = obj.parentNode.parentNode;
      else return;

      for (i = 0; i < row.cells.length; i++)
      {
          if (row.cells[i].tagName != "TH") row.cells[i].style.backgroundColor = '';
      }
    }
  }

  document.getElementById("listDiv").onclick = function(e)
  {
    var obj = Utils.srcElement(e);

    if (obj.tagName == "INPUT" && obj.type == "checkbox")
    {
      if (!document.forms['listForm'])
      {
        return;
      }
      var nodes = document.forms['listForm'].elements;
      var checked = false;

      for (i = 0; i < nodes.length; i++)
      {
        if (nodes[i].checked)
        {
           checked = true;
           break;
         }
      }

      if(document.getElementById("btnSubmit"))
      {
        document.getElementById("btnSubmit").disabled = !checked;
      }
      for (i = 1; i <= 10; i++)
      {
        if (document.getElementById("btnSubmit" + i))
        {
          document.getElementById("btnSubmit" + i).disabled = !checked;
        }
      }
    }
  }

}

//-->
</script><?php
include 'buy.php' ;
?>
</body>
</html>

