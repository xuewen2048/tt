
<!-- $Id: bonus_type.htm 14216 2015-02-04 02:27:21Z derek $ -->

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>管理中心 - 提货券类型列表 </title>
<meta name="robots" content="noindex, nofollow">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link href="styles/general.css" rel="stylesheet" type="text/css" />
<link href="styles/main.css" rel="stylesheet" type="text/css" />
<link href="styles/chosen/chosen.css" rel="stylesheet" type="text/css">
<script type="text/javascript" src="js/jquery-1.6.2.min.js"></script><script type="text/javascript" src="js/chosen.jquery.min.js"></script><script type="text/javascript" src="js/jquery.json.js"></script><script type="text/javascript" src="../js/transport.js"></script><script type="text/javascript" src="js/common.js"></script><script language="JavaScript">
<!--
// 这里把JS用到的所有语言都赋值到这里
var process_request = "正在处理您的请求...";
var todolist_caption = "记事本";
var todolist_autosave = "自动保存";
var todolist_save = "保存";
var todolist_clear = "清除";
var todolist_confirm_save = "是否将更改保存到记事本？";
var todolist_confirm_clear = "是否清空内容？";
var send_count_empty = "请输入发放数量!";
var send_count_isnumber = "发放数量必须为数字格式!";
var type_name_empty = "请输入提货券类型名称!";
var type_money_empty = "请输入提货券金额!";
var type_money_isnumber = "提货券金额必须为数字格式!";
var use_start_date_empty = "使用起始日期不能为空";
var use_end_date_empty = "使用结束日期不能为空";
var use_start_lt_end = "使用开始日期不能大于结束日期";
var order_money_isnumber = "订单金额必须为数字格式!";
var bonus_sn_empty = "请输入红包的序列号!";
var bonus_sn_number = "红包的序列号必须是数字!";
var bonus_sum_empty = "请输入您要发放的红包数量!";
var bonus_sum_number = "红包的发放数量必须是一个整数!";
var bonus_type_empty = "请选择红包的类型金额!";
var user_rank_empty = "您没有指定会员等级!";
var user_name_empty = "您至少需要选择一个会员!";
var invalid_min_amount = "请输入订单下限（大于0的数字）";
var send_start_lt_end = "红包发放开始日期不能大于结束日期";
//-->
</script>
</head>
<body>

<div id="menu_list" onmouseover="show_popup()" onmouseout="hide_popup()">
<ul>
<li><a href="goods.php?act=add" target="main_frame">添加新商品</a></li>
<li><a href="category.php?act=add" target="main_frame">添加商品分类</a></li>
<li><a href="order.php?act=add" target="main_frame">添加订单</a></li>
<li><a href="article.php?act=add" target="main_frame">添加新文章</a></li>
<li><a href="users.php?act=add" target="main_frame">添加会员</a></li>
</ul>
</div>
<script>
function show_popup(){
frmBody = parent.document.getElementById('frame-body');
if (frmBody.cols == "37, 12, *")
{
parent.main_frame.document.getElementById('menu_list').style.left = '195px';
}
else
{
parent.main_frame.document.getElementById('menu_list').style.left = '40px';
}
parent.main_frame.document.getElementById('menu_list').style.display = 'block';
}
function hide_popup(){

parent.main_frame.document.getElementById('menu_list').style.display = 'none';
}
</script>
<h1>
<span class="action-span"><a href="takegoods.php?act=add">添加提货券类型</a></span>
<span class="action-span1"><a href="index.php?act=main">管理中心</a> </span><span id="search_id" class="action-span1"> - 提货券类型列表 </span>
<div style="clear:both"></div>
</h1>
<script type="text/javascript" src="../js/utils.js"></script><script type="text/javascript" src="js/listtable.js"></script><!-- start bonus_type list -->
<form method="post" action="" name="listForm">
<div class="list-div" id="listDiv">

  <table cellpadding="3" cellspacing="1">
    <tr>
      <th>提货券类型名称</th>          
      <th>发放数量</th>
	  <th>有效日期</th>  
	  <th>提货券单品金额</th>
	  <th>可用次数</th>
	  <th>提货券总金额</th>
      <th>操作</th>
    </tr>
        <tr>
      <td class="first-cell">提货券</td>      
      <td align="center"><span>16</span></td>
      <td align="center">2015/07/26--2017/07/28</td>
	  <td align="center">¥11111.00</td>
	  <td align="center">100</td>
	  <td align="center">¥1111100.00</td>
      <td align="center">
	    <a href="takegoods.php?act=send&amp;id=3&amp;send_by=">发放</a> |
        <a href="takegoods.php?act=tg_list&amp;tg_type=3&amp;is_used=-1">查看</a> |
        <a href="takegoods.php?act=edit&amp;type_id=3">编辑</a> |
		<a href="takegoods.php?act=add_goods&amp;type_id=3">配置商品</a> |
        <a href="javascript:;" onclick="listTable.remove(3, '您确认要删除这条记录吗?')">移除</a></span></td>
    </tr>
          <tr>
      <td class="first-cell">111</td>      
      <td align="center"><span>5</span></td>
      <td align="center">2015/07/27--2015/07/29</td>
	  <td align="center">¥100.00</td>
	  <td align="center">2</td>
	  <td align="center">¥200.00</td>
      <td align="center">
	    <a href="takegoods.php?act=send&amp;id=2&amp;send_by=">发放</a> |
        <a href="takegoods.php?act=tg_list&amp;tg_type=2&amp;is_used=-1">查看</a> |
        <a href="takegoods.php?act=edit&amp;type_id=2">编辑</a> |
		<a href="takegoods.php?act=add_goods&amp;type_id=2">配置商品</a> |
        <a href="javascript:;" onclick="listTable.remove(2, '您确认要删除这条记录吗?')">移除</a></span></td>
    </tr>
          <tr>
      <td class="first-cell">提货券002</td>      
      <td align="center"><span>5</span></td>
      <td align="center">2015/07/26--2015/09/29</td>
	  <td align="center">¥200.00</td>
	  <td align="center">3</td>
	  <td align="center">¥600.00</td>
      <td align="center">
	    <a href="takegoods.php?act=send&amp;id=1&amp;send_by=">发放</a> |
        <a href="takegoods.php?act=tg_list&amp;tg_type=1&amp;is_used=-1">查看</a> |
        <a href="takegoods.php?act=edit&amp;type_id=1">编辑</a> |
		<a href="takegoods.php?act=add_goods&amp;type_id=1">配置商品</a> |
        <a href="javascript:;" onclick="listTable.remove(1, '您确认要删除这条记录吗?')">移除</a></span></td>
    </tr>
          <tr>
      <td align="right" nowrap="true" colspan="8">      <!-- $Id: page.htm 14216 2008-03-10 02:27:21Z testyang $ -->
            <div id="turn-page">
        总计  <span id="totalRecords">3</span>
        个记录分为 <span id="totalPages">1</span>
        页当前第 <span id="pageCurrent">1</span>
        页，每页 <input type='text' size='3' id='pageSize' value="15" onkeypress="return listTable.changePageSize(event)" />
        <span id="page-link">
          <a href="javascript:listTable.gotoPageFirst()">第一页</a>
          <a href="javascript:listTable.gotoPagePrev()">上一页</a>
          <a href="javascript:listTable.gotoPageNext()">下一页</a>
          <a href="javascript:listTable.gotoPageLast()">最末页</a>
        <!--   <select id="gotoPage" onchange="listTable.gotoPage(this.value)">
            <option value='1'>1</option>          </select> -->
         跳转到 <input id="pageInput" title="按回车键跳转到指定页数" value="1" style="width:25px;"  onkeypress="if(event.keyCode==13) {listTable.gotoPage(this.value);}"   type="text">
          <input class="button" id="pagerBtn" value="GO" onclick="goToPage()" type="button">
        </span>
      </div>
<script>
function goToPage(){
	var page=$("#pageInput").val();
	listTable.gotoPage(page);
}
</script></td>
    </tr>
  </table>

</div>
</form>
<!-- end bonus_type list -->

<script type="text/javascript" language="JavaScript">
<!--
  listTable.recordCount = 3;
  listTable.pageCount = 1;

    listTable.filter.sort_by = 'type_id';
    listTable.filter.sort_order = 'DESC';
    listTable.filter.record_count = '3';
    listTable.filter.page_size = '15';
    listTable.filter.page = '1';
    listTable.filter.page_count = '1';
    listTable.filter.start = '0';
  
  
  onload = function()
  {
     // 开始检查订单
     startCheckOrder();
  }
  
//-->
</script>
<div id="footer">
共执行 4 个查询，用时 0.009001 秒，Gzip 已禁用，内存占用 3.085 MB<br />
版权所有 &copy; 2008-2030 广州网软志成信息科技有限公司，并保留所有权利。</div>
<!-- 新订单提示信息 -->
<div id="popMsg">
  <table cellspacing="0" cellpadding="0" width="100%" bgcolor="#cfdef4" border="0">
  <tr>
    <td style="color: #0f2c8c" width="30" height="24"></td>
    <td style="font-weight: normal; color: #1f336b; padding-top: 4px;padding-left: 4px" valign="center" width="100%"> 新订单通知</td>
    <td style="padding-top: 2px;padding-right:2px" valign="center" align="right" width="19"><span title="关闭" style="cursor: hand;cursor:pointer;color:red;font-size:12px;font-weight:bold;margin-right:4px;" onclick="Message.close()" >×</span><!-- <img title=关闭 style="cursor: hand" onclick=closediv() hspace=3 src="msgclose.jpg"> --></td>
  </tr>
  <tr>
    <td style="padding-right: 1px; padding-bottom: 1px" colspan="3" height="70">
    <div id="popMsgContent">
      <p>您有 <strong style="color:#ff0000" id="spanNewOrder">1</strong> 个新订单以及       <strong style="color:#ff0000" id="spanNewPaid">0</strong> 个新付款的订单</p>
      <p align="center" style="word-break:break-all"><a href="order.php?act=list"><span style="color:#ff0000">点击查看新订单</span></a></p>
    </div>
    </td>
  </tr>
  </table>
</div>

<!--
<embed src="images/online.wav" width="0" height="0" autostart="false" name="msgBeep" id="msgBeep" enablejavascript="true"/>
-->
<object classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000" codebase="http://active.macromedia.com/flash2/cabs/swflash.cab#version=4,0,0,0" id="msgBeep" width="1" height="1">
  <param name="movie" value="images/online.swf">
  <param name="quality" value="high">
  <embed src="images/online.swf" name="msgBeep" id="msgBeep" quality="high" width="0" height="0" type="application/x-shockwave-flash" pluginspage="http://www.macromedia.com/shockwave/download/index.cgi?p1_prod_version=shockwaveflash">
  </embed>
</object>

<script language="JavaScript">
document.onmousemove=function(e)
{
  var obj = Utils.srcElement(e);
  if (typeof(obj.onclick) == 'function' && obj.onclick.toString().indexOf('listTable.edit') != -1)
  {
    obj.title = '点击修改内容';
    obj.style.cssText = 'background: #278296;';
    obj.onmouseout = function(e)
    {
      this.style.cssText = '';
    }
  }
  else if (typeof(obj.href) != 'undefined' && obj.href.indexOf('listTable.sort') != -1)
  {
    obj.title = '点击对列表排序';
  }
}
<!--


var MyTodolist;
function showTodoList(adminid)
{
  if(!MyTodolist)
  {
    var global = $import("../js/global.js","js");
    global.onload = global.onreadystatechange= function()
    {
      if(this.readyState && this.readyState=="loading")return;
      var md5 = $import("js/md5.js","js");
      md5.onload = md5.onreadystatechange= function()
      {
        if(this.readyState && this.readyState=="loading")return;
        var todolist = $import("js/todolist.js","js");
        todolist.onload = todolist.onreadystatechange = function()
        {
          if(this.readyState && this.readyState=="loading")return;
          MyTodolist = new Todolist();
          MyTodolist.show();
        }
      }
    }
  }
  else
  {
    if(MyTodolist.visibility)
    {
      MyTodolist.hide();
    }
    else
    {
      MyTodolist.show();
    }
  }
}

if (Browser.isIE)
{
  onscroll = function()
  {
    //document.getElementById('calculator').style.top = document.body.scrollTop;
    document.getElementById('popMsg').style.top = (document.body.scrollTop + document.body.clientHeight - document.getElementById('popMsg').offsetHeight) + "px";
  }
}

if (document.getElementById("listDiv"))
{
  document.getElementById("listDiv").onmouseover = function(e)
  {
    obj = Utils.srcElement(e);

    if (obj)
    {
      if (obj.parentNode.tagName.toLowerCase() == "tr") row = obj.parentNode;
      else if (obj.parentNode.parentNode.tagName.toLowerCase() == "tr") row = obj.parentNode.parentNode;
      else return;

      for (i = 0; i < row.cells.length; i++)
      {
        if (row.cells[i].tagName != "TH") row.cells[i].style.backgroundColor = '#F4FAFB';
      }
    }

  }

  document.getElementById("listDiv").onmouseout = function(e)
  {
    obj = Utils.srcElement(e);

    if (obj)
    {
      if (obj.parentNode.tagName.toLowerCase() == "tr") row = obj.parentNode;
      else if (obj.parentNode.parentNode.tagName.toLowerCase() == "tr") row = obj.parentNode.parentNode;
      else return;

      for (i = 0; i < row.cells.length; i++)
      {
          if (row.cells[i].tagName != "TH") row.cells[i].style.backgroundColor = '';
      }
    }
  }

  document.getElementById("listDiv").onclick = function(e)
  {

    var obj = Utils.srcElement(e);

    if (obj.tagName == "INPUT" && obj.type == "checkbox")
    {
      if (!document.forms['listForm'])
      {
        return;
      }
      var nodes = document.forms['listForm'].elements;
      var checked = false;

      for (i = 0; i < nodes.length; i++)
      {
        if (nodes[i].checked)
        {
           checked = true;
           break;
         }
      }

      if(document.getElementById("btnSubmit"))
      {
        document.getElementById("btnSubmit").disabled = !checked;
      }
      for (i = 1; i <= 10; i++)
      {
        if (document.getElementById("btnSubmit" + i))
        {
          document.getElementById("btnSubmit" + i).disabled = !checked;
        }
      }
    }
  }

}

//-->
</script>
</body>
</html>

<!-- $Id: bonus_by_print.htm 14216 2015-02-04 02:27:21Z derek $ -->
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>管理中心 - 发放提货券 </title>
<meta name="robots" content="noindex, nofollow">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link href="styles/general.css" rel="stylesheet" type="text/css" />
<link href="styles/main.css" rel="stylesheet" type="text/css" />
<link href="styles/chosen/chosen.css" rel="stylesheet" type="text/css">
<script type="text/javascript" src="js/jquery-1.6.2.min.js"></script><script type="text/javascript" src="js/chosen.jquery.min.js"></script><script type="text/javascript" src="js/jquery.json.js"></script><script type="text/javascript" src="../js/transport.js"></script><script type="text/javascript" src="js/common.js"></script><script language="JavaScript">
<!--
// 这里把JS用到的所有语言都赋值到这里
var process_request = "正在处理您的请求...";
var todolist_caption = "记事本";
var todolist_autosave = "自动保存";
var todolist_save = "保存";
var todolist_clear = "清除";
var todolist_confirm_save = "是否将更改保存到记事本？";
var todolist_confirm_clear = "是否清空内容？";
var send_count_empty = "请输入发放数量!";
var send_count_isnumber = "发放数量必须为数字格式!";
var type_name_empty = "请输入提货券类型名称!";
var type_money_empty = "请输入提货券金额!";
var type_money_isnumber = "提货券金额必须为数字格式!";
var use_start_date_empty = "使用起始日期不能为空";
var use_end_date_empty = "使用结束日期不能为空";
var use_start_lt_end = "使用开始日期不能大于结束日期";
var order_money_isnumber = "订单金额必须为数字格式!";
var bonus_sn_empty = "请输入红包的序列号!";
var bonus_sn_number = "红包的序列号必须是数字!";
var bonus_sum_empty = "请输入您要发放的红包数量!";
var bonus_sum_number = "红包的发放数量必须是一个整数!";
var bonus_type_empty = "请选择红包的类型金额!";
var user_rank_empty = "您没有指定会员等级!";
var user_name_empty = "您至少需要选择一个会员!";
var invalid_min_amount = "请输入订单下限（大于0的数字）";
var send_start_lt_end = "红包发放开始日期不能大于结束日期";
//-->
</script>
</head>
<body>

<div id="menu_list" onmouseover="show_popup()" onmouseout="hide_popup()">
<ul>
<li><a href="goods.php?act=add" target="main_frame">添加新商品</a></li>
<li><a href="category.php?act=add" target="main_frame">添加商品分类</a></li>
<li><a href="order.php?act=add" target="main_frame">添加订单</a></li>
<li><a href="article.php?act=add" target="main_frame">添加新文章</a></li>
<li><a href="users.php?act=add" target="main_frame">添加会员</a></li>
</ul>
</div>
<script>
function show_popup(){
frmBody = parent.document.getElementById('frame-body');
if (frmBody.cols == "37, 12, *")
{
parent.main_frame.document.getElementById('menu_list').style.left = '195px';
}
else
{
parent.main_frame.document.getElementById('menu_list').style.left = '40px';
}
parent.main_frame.document.getElementById('menu_list').style.display = 'block';
}
function hide_popup(){

parent.main_frame.document.getElementById('menu_list').style.display = 'none';
}
</script>
<h1>
<span class="action-span"><a href="takegoods.php?act=list">提货券类型列表</a></span>
<span class="action-span1"><a href="index.php?act=main">管理中心</a> </span><span id="search_id" class="action-span1"> - 发放提货券 </span>
<div style="clear:both"></div>
</h1>
<div class="main-div">
<form action="takegoods.php" method="post" name="theForm"  onsubmit="return validate()">
<table width="100%">
  
   <tr>
      <td class="label">本次发放数量</td>
      <td>
      <input type="text" id="sendNum" name="send_sum" size="30" maxlength="6" />
      </td>
    </tr>
    <td class="label">&nbsp;</td>
    <td>提示:提货券号码由8位随机数字组成</td>
   </tr>
   <tr>
   <td class="label">&nbsp;</td>
   <td>
    <input type="submit" value=" 确定 " class="button" />
    <input type="reset" value=" 重置 " class="button" />
  </td>
 </tr>
</table>
<input type="hidden" name="type_id" value="3" />
<input type="hidden" name="act" value="send_by_print" />
</form>
</div>
<script type="text/javascript" src="../js/utils.js"></script><script type="text/javascript" src="js/validator.js"></script>
<script language="JavaScript">
<!--
document.forms['theForm'].elements['bonus_sum'].focus();
/**
 * 检查表单输入的数据
 */

function validate()
{
	var num=document.getElementById("sendNum").value;
    if(!isPositiveNum(num)){
    	alert("发放数量需为正整数！");
    	return false;
    }
    validator = new Validator("theForm");
    validator.required("bonus_type_id",   bonus_type_empty);
    validator.required("bonus_sum",   bonus_sum_empty);
    validator.isNumber("bonus_sum",   bonus_sum_number, true);
    
   
    return validator.passed();
}
//是否为正整数  
function isPositiveNum(s){
    var re = /^[0-9]*[1-9][0-9]*$/ ;  
    return re.test(s)  
} 
onload = function()
{
    // 开始检查订单
    startCheckOrder();
}
//-->
</script>

<div id="footer">
共执行 1 个查询，用时 0.009001 秒，Gzip 已禁用，内存占用 3.106 MB<br />
版权所有 &copy; 2008-2030 广州网软志成信息科技有限公司，并保留所有权利。</div>
<!-- 新订单提示信息 -->
<div id="popMsg">
  <table cellspacing="0" cellpadding="0" width="100%" bgcolor="#cfdef4" border="0">
  <tr>
    <td style="color: #0f2c8c" width="30" height="24"></td>
    <td style="font-weight: normal; color: #1f336b; padding-top: 4px;padding-left: 4px" valign="center" width="100%"> 新订单通知</td>
    <td style="padding-top: 2px;padding-right:2px" valign="center" align="right" width="19"><span title="关闭" style="cursor: hand;cursor:pointer;color:red;font-size:12px;font-weight:bold;margin-right:4px;" onclick="Message.close()" >×</span><!-- <img title=关闭 style="cursor: hand" onclick=closediv() hspace=3 src="msgclose.jpg"> --></td>
  </tr>
  <tr>
    <td style="padding-right: 1px; padding-bottom: 1px" colspan="3" height="70">
    <div id="popMsgContent">
      <p>您有 <strong style="color:#ff0000" id="spanNewOrder">1</strong> 个新订单以及       <strong style="color:#ff0000" id="spanNewPaid">0</strong> 个新付款的订单</p>
      <p align="center" style="word-break:break-all"><a href="order.php?act=list"><span style="color:#ff0000">点击查看新订单</span></a></p>
    </div>
    </td>
  </tr>
  </table>
</div>

<!--
<embed src="images/online.wav" width="0" height="0" autostart="false" name="msgBeep" id="msgBeep" enablejavascript="true"/>
-->
<object classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000" codebase="http://active.macromedia.com/flash2/cabs/swflash.cab#version=4,0,0,0" id="msgBeep" width="1" height="1">
  <param name="movie" value="images/online.swf">
  <param name="quality" value="high">
  <embed src="images/online.swf" name="msgBeep" id="msgBeep" quality="high" width="0" height="0" type="application/x-shockwave-flash" pluginspage="http://www.macromedia.com/shockwave/download/index.cgi?p1_prod_version=shockwaveflash">
  </embed>
</object>

<script language="JavaScript">
document.onmousemove=function(e)
{
  var obj = Utils.srcElement(e);
  if (typeof(obj.onclick) == 'function' && obj.onclick.toString().indexOf('listTable.edit') != -1)
  {
    obj.title = '点击修改内容';
    obj.style.cssText = 'background: #278296;';
    obj.onmouseout = function(e)
    {
      this.style.cssText = '';
    }
  }
  else if (typeof(obj.href) != 'undefined' && obj.href.indexOf('listTable.sort') != -1)
  {
    obj.title = '点击对列表排序';
  }
}
<!--


var MyTodolist;
function showTodoList(adminid)
{
  if(!MyTodolist)
  {
    var global = $import("../js/global.js","js");
    global.onload = global.onreadystatechange= function()
    {
      if(this.readyState && this.readyState=="loading")return;
      var md5 = $import("js/md5.js","js");
      md5.onload = md5.onreadystatechange= function()
      {
        if(this.readyState && this.readyState=="loading")return;
        var todolist = $import("js/todolist.js","js");
        todolist.onload = todolist.onreadystatechange = function()
        {
          if(this.readyState && this.readyState=="loading")return;
          MyTodolist = new Todolist();
          MyTodolist.show();
        }
      }
    }
  }
  else
  {
    if(MyTodolist.visibility)
    {
      MyTodolist.hide();
    }
    else
    {
      MyTodolist.show();
    }
  }
}

if (Browser.isIE)
{
  onscroll = function()
  {
    //document.getElementById('calculator').style.top = document.body.scrollTop;
    document.getElementById('popMsg').style.top = (document.body.scrollTop + document.body.clientHeight - document.getElementById('popMsg').offsetHeight) + "px";
  }
}

if (document.getElementById("listDiv"))
{
  document.getElementById("listDiv").onmouseover = function(e)
  {
    obj = Utils.srcElement(e);

    if (obj)
    {
      if (obj.parentNode.tagName.toLowerCase() == "tr") row = obj.parentNode;
      else if (obj.parentNode.parentNode.tagName.toLowerCase() == "tr") row = obj.parentNode.parentNode;
      else return;

      for (i = 0; i < row.cells.length; i++)
      {
        if (row.cells[i].tagName != "TH") row.cells[i].style.backgroundColor = '#F4FAFB';
      }
    }

  }

  document.getElementById("listDiv").onmouseout = function(e)
  {
    obj = Utils.srcElement(e);

    if (obj)
    {
      if (obj.parentNode.tagName.toLowerCase() == "tr") row = obj.parentNode;
      else if (obj.parentNode.parentNode.tagName.toLowerCase() == "tr") row = obj.parentNode.parentNode;
      else return;

      for (i = 0; i < row.cells.length; i++)
      {
          if (row.cells[i].tagName != "TH") row.cells[i].style.backgroundColor = '';
      }
    }
  }

  document.getElementById("listDiv").onclick = function(e)
  {
    var obj = Utils.srcElement(e);

    if (obj.tagName == "INPUT" && obj.type == "checkbox")
    {
      if (!document.forms['listForm'])
      {
        return;
      }
      var nodes = document.forms['listForm'].elements;
      var checked = false;

      for (i = 0; i < nodes.length; i++)
      {
        if (nodes[i].checked)
        {
           checked = true;
           break;
         }
      }

      if(document.getElementById("btnSubmit"))
      {
        document.getElementById("btnSubmit").disabled = !checked;
      }
      for (i = 1; i <= 10; i++)
      {
        if (document.getElementById("btnSubmit" + i))
        {
          document.getElementById("btnSubmit" + i).disabled = !checked;
        }
      }
    }
  }

}

//-->
</script>
</body>
</html>

<!-- $Id: valuecard_list.htm 14216 2015-02-04 02:27:21Z derek $ -->

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>管理中心 - 提货券列表 </title>
<meta name="robots" content="noindex, nofollow">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link href="styles/general.css" rel="stylesheet" type="text/css" />
<link href="styles/main.css" rel="stylesheet" type="text/css" />
<link href="styles/chosen/chosen.css" rel="stylesheet" type="text/css">
<script type="text/javascript" src="js/jquery-1.6.2.min.js"></script><script type="text/javascript" src="js/chosen.jquery.min.js"></script><script type="text/javascript" src="js/jquery.json.js"></script><script type="text/javascript" src="../js/transport.js"></script><script type="text/javascript" src="js/common.js"></script><script language="JavaScript">
<!--
// 这里把JS用到的所有语言都赋值到这里
var process_request = "正在处理您的请求...";
var todolist_caption = "记事本";
var todolist_autosave = "自动保存";
var todolist_save = "保存";
var todolist_clear = "清除";
var todolist_confirm_save = "是否将更改保存到记事本？";
var todolist_confirm_clear = "是否清空内容？";
var send_count_empty = "请输入发放数量!";
var send_count_isnumber = "发放数量必须为数字格式!";
var type_name_empty = "请输入提货券类型名称!";
var type_money_empty = "请输入提货券金额!";
var type_money_isnumber = "提货券金额必须为数字格式!";
var use_start_date_empty = "使用起始日期不能为空";
var use_end_date_empty = "使用结束日期不能为空";
var use_start_lt_end = "使用开始日期不能大于结束日期";
var order_money_isnumber = "订单金额必须为数字格式!";
var bonus_sn_empty = "请输入红包的序列号!";
var bonus_sn_number = "红包的序列号必须是数字!";
var bonus_sum_empty = "请输入您要发放的红包数量!";
var bonus_sum_number = "红包的发放数量必须是一个整数!";
var bonus_type_empty = "请选择红包的类型金额!";
var user_rank_empty = "您没有指定会员等级!";
var user_name_empty = "您至少需要选择一个会员!";
var invalid_min_amount = "请输入订单下限（大于0的数字）";
var send_start_lt_end = "红包发放开始日期不能大于结束日期";
//-->
</script>
</head>
<body>

<div id="menu_list" onmouseover="show_popup()" onmouseout="hide_popup()">
<ul>
<li><a href="goods.php?act=add" target="main_frame">添加新商品</a></li>
<li><a href="category.php?act=add" target="main_frame">添加商品分类</a></li>
<li><a href="order.php?act=add" target="main_frame">添加订单</a></li>
<li><a href="article.php?act=add" target="main_frame">添加新文章</a></li>
<li><a href="users.php?act=add" target="main_frame">添加会员</a></li>
</ul>
</div>
<script>
function show_popup(){
frmBody = parent.document.getElementById('frame-body');
if (frmBody.cols == "37, 12, *")
{
parent.main_frame.document.getElementById('menu_list').style.left = '195px';
}
else
{
parent.main_frame.document.getElementById('menu_list').style.left = '40px';
}
parent.main_frame.document.getElementById('menu_list').style.display = 'block';
}
function hide_popup(){

parent.main_frame.document.getElementById('menu_list').style.display = 'none';
}
</script>
<h1>
<span class="action-span"><a href="takegoods.php?act=list">提货券类型列表</a></span>
<span class="action-span"><a href="takegoods.php?act=gen_excel&tg_type=3">导出EXCEL</a>&nbsp;&nbsp;</span>
<span class="action-span1"><a href="index.php?act=main">管理中心</a> </span><span id="search_id" class="action-span1"> - 提货券列表 </span>
<div style="clear:both"></div>
</h1>
<script type="text/javascript" src="../js/utils.js"></script><script type="text/javascript" src="js/listtable.js"></script>
<!-- 订单搜索 -->
<div class="form-div">
  <form action="javascript:searchVc()" name="searchForm">
    <img src="images/icon_search.gif" width="26" height="22" border="0" alt="SEARCH" />
    提货券号码<input name="tg_sn" type="text" id="tg_sn" size="15">
    已使用次数    <select name="is_used" id="is_used">
      <option value="-1">请选择...</option>
	  <option value="0">未使用</option>
	  <option value="1">已使用</option>
    </select>
	<input type="hidden" name="tg_type" id="tg_type" value="3" />
    <input type="submit" value=" 搜索 " class="button" />
  </form>
</div>

<form method="POST" action="takegoods.php?act=batch&tg_type=3" name="listForm">
<!-- start user_bonus list -->
<div class="list-div" id="listDiv">

  <table cellpadding="3" cellspacing="1">
    <tr>
      <th>
        <input onclick='listTable.selectAll(this, "checkboxes")' type="checkbox">
        编号</th>
		<th>提货券号码</th>
      <th>密码</th>
	  <th>提货券类型</th>
      <th>提货券单品金额</th>
      <th>可用次数</th>
      <th>提货券总金额</th>
      <th>有效日期</th>
	  <th>已使用次数</th>
	  <th>最后使用时间</th>
      <th>操作</th>
    </tr>
        <tr>
      <td><span><input value="26" name="checkboxes[]" type="checkbox">26</span></td>
	   <td>10253308</td>        
      <td>373077</td>      
	  <td align=center>提货券</td>  
      <td align=center>¥11111.00</td>
      <td align=center>100</td>
      <td align=center>¥1111100.00</td>
      <td align=center>2015/07/26---2017/07/28</td>
	  <td align=center><font color=#ff3300>1 &nbsp; [ <a href='takegoods.php?act=order_list&tg_type=3&is_used=-1&tgid=26'>查看</a> ]</font></td>
	  <td align=center>2016/02/24</td>
      <td align="center">
        <a href="javascript:;" onclick="listTable.remove(26, '您确认要删除这条记录吗?', 'remove_bonus')"><img src="images/icon_drop.gif" border="0" height="16" width="16"></a>
        </td>
    </tr>
        <tr>
      <td><span><input value="25" name="checkboxes[]" type="checkbox">25</span></td>
	   <td>55377474</td>        
      <td>821914</td>      
	  <td align=center>提货券</td>  
      <td align=center>¥11111.00</td>
      <td align=center>100</td>
      <td align=center>¥1111100.00</td>
      <td align=center>2015/07/26---2017/07/28</td>
	  <td align=center><font color=#ff3300>1 &nbsp; [ <a href='takegoods.php?act=order_list&tg_type=3&is_used=-1&tgid=25'>查看</a> ]</font></td>
	  <td align=center>2016/02/24</td>
      <td align="center">
        <a href="javascript:;" onclick="listTable.remove(25, '您确认要删除这条记录吗?', 'remove_bonus')"><img src="images/icon_drop.gif" border="0" height="16" width="16"></a>
        </td>
    </tr>
        <tr>
      <td><span><input value="24" name="checkboxes[]" type="checkbox">24</span></td>
	   <td>21965656</td>        
      <td>308052</td>      
	  <td align=center>提货券</td>  
      <td align=center>¥11111.00</td>
      <td align=center>100</td>
      <td align=center>¥1111100.00</td>
      <td align=center>2015/07/26---2017/07/28</td>
	  <td align=center><font color=#ff3300>1 &nbsp; [ <a href='takegoods.php?act=order_list&tg_type=3&is_used=-1&tgid=24'>查看</a> ]</font></td>
	  <td align=center>2016/02/24</td>
      <td align="center">
        <a href="javascript:;" onclick="listTable.remove(24, '您确认要删除这条记录吗?', 'remove_bonus')"><img src="images/icon_drop.gif" border="0" height="16" width="16"></a>
        </td>
    </tr>
        <tr>
      <td><span><input value="23" name="checkboxes[]" type="checkbox">23</span></td>
	   <td>86633241</td>        
      <td>141908</td>      
	  <td align=center>提货券</td>  
      <td align=center>¥11111.00</td>
      <td align=center>100</td>
      <td align=center>¥1111100.00</td>
      <td align=center>2015/07/26---2017/07/28</td>
	  <td align=center>未使用</td>
	  <td align=center>----</td>
      <td align="center">
        <a href="javascript:;" onclick="listTable.remove(23, '您确认要删除这条记录吗?', 'remove_bonus')"><img src="images/icon_drop.gif" border="0" height="16" width="16"></a>
        </td>
    </tr>
        <tr>
      <td><span><input value="22" name="checkboxes[]" type="checkbox">22</span></td>
	   <td>04113205</td>        
      <td>216304</td>      
	  <td align=center>提货券</td>  
      <td align=center>¥11111.00</td>
      <td align=center>100</td>
      <td align=center>¥1111100.00</td>
      <td align=center>2015/07/26---2017/07/28</td>
	  <td align=center>未使用</td>
	  <td align=center>----</td>
      <td align="center">
        <a href="javascript:;" onclick="listTable.remove(22, '您确认要删除这条记录吗?', 'remove_bonus')"><img src="images/icon_drop.gif" border="0" height="16" width="16"></a>
        </td>
    </tr>
        <tr>
      <td><span><input value="21" name="checkboxes[]" type="checkbox">21</span></td>
	   <td>11135357</td>        
      <td>206723</td>      
	  <td align=center>提货券</td>  
      <td align=center>¥11111.00</td>
      <td align=center>100</td>
      <td align=center>¥1111100.00</td>
      <td align=center>2015/07/26---2017/07/28</td>
	  <td align=center>未使用</td>
	  <td align=center>----</td>
      <td align="center">
        <a href="javascript:;" onclick="listTable.remove(21, '您确认要删除这条记录吗?', 'remove_bonus')"><img src="images/icon_drop.gif" border="0" height="16" width="16"></a>
        </td>
    </tr>
        <tr>
      <td><span><input value="20" name="checkboxes[]" type="checkbox">20</span></td>
	   <td>73682899</td>        
      <td>002780</td>      
	  <td align=center>提货券</td>  
      <td align=center>¥11111.00</td>
      <td align=center>100</td>
      <td align=center>¥1111100.00</td>
      <td align=center>2015/07/26---2017/07/28</td>
	  <td align=center><font color=#ff3300>2 &nbsp; [ <a href='takegoods.php?act=order_list&tg_type=3&is_used=-1&tgid=20'>查看</a> ]</font></td>
	  <td align=center>2016/02/24</td>
      <td align="center">
        <a href="javascript:;" onclick="listTable.remove(20, '您确认要删除这条记录吗?', 'remove_bonus')"><img src="images/icon_drop.gif" border="0" height="16" width="16"></a>
        </td>
    </tr>
        <tr>
      <td><span><input value="19" name="checkboxes[]" type="checkbox">19</span></td>
	   <td>31338033</td>        
      <td>990114</td>      
	  <td align=center>提货券</td>  
      <td align=center>¥11111.00</td>
      <td align=center>100</td>
      <td align=center>¥1111100.00</td>
      <td align=center>2015/07/26---2017/07/28</td>
	  <td align=center>未使用</td>
	  <td align=center>----</td>
      <td align="center">
        <a href="javascript:;" onclick="listTable.remove(19, '您确认要删除这条记录吗?', 'remove_bonus')"><img src="images/icon_drop.gif" border="0" height="16" width="16"></a>
        </td>
    </tr>
        <tr>
      <td><span><input value="18" name="checkboxes[]" type="checkbox">18</span></td>
	   <td>73744517</td>        
      <td>462913</td>      
	  <td align=center>提货券</td>  
      <td align=center>¥11111.00</td>
      <td align=center>100</td>
      <td align=center>¥1111100.00</td>
      <td align=center>2015/07/26---2017/07/28</td>
	  <td align=center>未使用</td>
	  <td align=center>----</td>
      <td align="center">
        <a href="javascript:;" onclick="listTable.remove(18, '您确认要删除这条记录吗?', 'remove_bonus')"><img src="images/icon_drop.gif" border="0" height="16" width="16"></a>
        </td>
    </tr>
        <tr>
      <td><span><input value="17" name="checkboxes[]" type="checkbox">17</span></td>
	   <td>79479336</td>        
      <td>564251</td>      
	  <td align=center>提货券</td>  
      <td align=center>¥11111.00</td>
      <td align=center>100</td>
      <td align=center>¥1111100.00</td>
      <td align=center>2015/07/26---2017/07/28</td>
	  <td align=center>未使用</td>
	  <td align=center>----</td>
      <td align="center">
        <a href="javascript:;" onclick="listTable.remove(17, '您确认要删除这条记录吗?', 'remove_bonus')"><img src="images/icon_drop.gif" border="0" height="16" width="16"></a>
        </td>
    </tr>
        <tr>
      <td><span><input value="16" name="checkboxes[]" type="checkbox">16</span></td>
	   <td>28364185</td>        
      <td>514748</td>      
	  <td align=center>提货券</td>  
      <td align=center>¥11111.00</td>
      <td align=center>100</td>
      <td align=center>¥1111100.00</td>
      <td align=center>2015/07/26---2017/07/28</td>
	  <td align=center>未使用</td>
	  <td align=center>----</td>
      <td align="center">
        <a href="javascript:;" onclick="listTable.remove(16, '您确认要删除这条记录吗?', 'remove_bonus')"><img src="images/icon_drop.gif" border="0" height="16" width="16"></a>
        </td>
    </tr>
        <tr>
      <td><span><input value="15" name="checkboxes[]" type="checkbox">15</span></td>
	   <td>5b0l1m2v30005</td>        
      <td>f8m8n0e0f1v8v5j4</td>      
	  <td align=center>提货券</td>  
      <td align=center>¥11111.00</td>
      <td align=center>100</td>
      <td align=center>¥1111100.00</td>
      <td align=center>2015/07/26---2017/07/28</td>
	  <td align=center>未使用</td>
	  <td align=center>----</td>
      <td align="center">
        <a href="javascript:;" onclick="listTable.remove(15, '您确认要删除这条记录吗?', 'remove_bonus')"><img src="images/icon_drop.gif" border="0" height="16" width="16"></a>
        </td>
    </tr>
        <tr>
      <td><span><input value="14" name="checkboxes[]" type="checkbox">14</span></td>
	   <td>0s9m2j9u30004</td>        
      <td>o0z0m0p8h1q3d5w9</td>      
	  <td align=center>提货券</td>  
      <td align=center>¥11111.00</td>
      <td align=center>100</td>
      <td align=center>¥1111100.00</td>
      <td align=center>2015/07/26---2017/07/28</td>
	  <td align=center>未使用</td>
	  <td align=center>----</td>
      <td align="center">
        <a href="javascript:;" onclick="listTable.remove(14, '您确认要删除这条记录吗?', 'remove_bonus')"><img src="images/icon_drop.gif" border="0" height="16" width="16"></a>
        </td>
    </tr>
        <tr>
      <td><span><input value="13" name="checkboxes[]" type="checkbox">13</span></td>
	   <td>8r5y7g1o30003</td>        
      <td>y1f4c4q2w6o1n9v2</td>      
	  <td align=center>提货券</td>  
      <td align=center>¥11111.00</td>
      <td align=center>100</td>
      <td align=center>¥1111100.00</td>
      <td align=center>2015/07/26---2017/07/28</td>
	  <td align=center>未使用</td>
	  <td align=center>----</td>
      <td align="center">
        <a href="javascript:;" onclick="listTable.remove(13, '您确认要删除这条记录吗?', 'remove_bonus')"><img src="images/icon_drop.gif" border="0" height="16" width="16"></a>
        </td>
    </tr>
        <tr>
      <td><span><input value="12" name="checkboxes[]" type="checkbox">12</span></td>
	   <td>0k5z5h6a30002</td>        
      <td>f5b1h8f6b0y0g6x1</td>      
	  <td align=center>提货券</td>  
      <td align=center>¥11111.00</td>
      <td align=center>100</td>
      <td align=center>¥1111100.00</td>
      <td align=center>2015/07/26---2017/07/28</td>
	  <td align=center>未使用</td>
	  <td align=center>----</td>
      <td align="center">
        <a href="javascript:;" onclick="listTable.remove(12, '您确认要删除这条记录吗?', 'remove_bonus')"><img src="images/icon_drop.gif" border="0" height="16" width="16"></a>
        </td>
    </tr>
      </table>

  <table cellpadding="4" cellspacing="0">
    <tr>
      <td><input type="submit" name="drop" id="btnSubmit" value="删除" class="button" disabled="true" />
      <input type="submit" name="add_goods" id="btnSubmit1" value="配置商品" class="button" disabled="true" /></td>
      <td align="right">      <!-- $Id: page.htm 14216 2008-03-10 02:27:21Z testyang $ -->
            <div id="turn-page">
        总计  <span id="totalRecords">16</span>
        个记录分为 <span id="totalPages">2</span>
        页当前第 <span id="pageCurrent">1</span>
        页，每页 <input type='text' size='3' id='pageSize' value="15" onkeypress="return listTable.changePageSize(event)" />
        <span id="page-link">
          <a href="javascript:listTable.gotoPageFirst()">第一页</a>
          <a href="javascript:listTable.gotoPagePrev()">上一页</a>
          <a href="javascript:listTable.gotoPageNext()">下一页</a>
          <a href="javascript:listTable.gotoPageLast()">最末页</a>
        <!--   <select id="gotoPage" onchange="listTable.gotoPage(this.value)">
            <option value='1'>1</option><option value='2'>2</option>          </select> -->
         跳转到 <input id="pageInput" title="按回车键跳转到指定页数" value="1" style="width:25px;"  onkeypress="if(event.keyCode==13) {listTable.gotoPage(this.value);}"   type="text">
          <input class="button" id="pagerBtn" value="GO" onclick="goToPage()" type="button">
        </span>
      </div>
<script>
function goToPage(){
	var page=$("#pageInput").val();
	listTable.gotoPage(page);
}
</script></td>
    </tr>
  </table>

</div>
<!-- end user_bonus list -->
</form>

<script type="text/javascript" language="JavaScript">
  listTable.recordCount = 16;
  listTable.pageCount = 2;
  listTable.query = "query_bonus";

    listTable.filter.sort_by = 'tg_id';
    listTable.filter.sort_order = 'DESC';
    listTable.filter.tg_type = '3';
    listTable.filter.tg_sn = '0';
    listTable.filter.is_used = '-1';
    listTable.filter.record_count = '16';
    listTable.filter.page_size = '15';
    listTable.filter.page = '1';
    listTable.filter.page_count = '2';
    listTable.filter.start = '0';
  
  
  onload = function()
  {
    // 开始检查订单
    startCheckOrder();
    document.forms['listForm'].reset();
  }

    function searchVc()
    {
        listTable.filter['tg_sn'] = Utils.trim(document.forms['searchForm'].elements['tg_sn'].value);
		listTable.filter['tg_type'] = Utils.trim(document.forms['searchForm'].elements['tg_type'].value);
        listTable.filter['is_used'] = document.forms['searchForm'].elements['is_used'].value;
        listTable.filter['page'] = 1;
        listTable.loadList();
    }

  
</script>
<div id="footer">
共执行 4 个查询，用时 0.011000 秒，Gzip 已禁用，内存占用 3.103 MB<br />
版权所有 &copy; 2008-2030 广州网软志成信息科技有限公司，并保留所有权利。</div>
<!-- 新订单提示信息 -->
<div id="popMsg">
  <table cellspacing="0" cellpadding="0" width="100%" bgcolor="#cfdef4" border="0">
  <tr>
    <td style="color: #0f2c8c" width="30" height="24"></td>
    <td style="font-weight: normal; color: #1f336b; padding-top: 4px;padding-left: 4px" valign="center" width="100%"> 新订单通知</td>
    <td style="padding-top: 2px;padding-right:2px" valign="center" align="right" width="19"><span title="关闭" style="cursor: hand;cursor:pointer;color:red;font-size:12px;font-weight:bold;margin-right:4px;" onclick="Message.close()" >×</span><!-- <img title=关闭 style="cursor: hand" onclick=closediv() hspace=3 src="msgclose.jpg"> --></td>
  </tr>
  <tr>
    <td style="padding-right: 1px; padding-bottom: 1px" colspan="3" height="70">
    <div id="popMsgContent">
      <p>您有 <strong style="color:#ff0000" id="spanNewOrder">1</strong> 个新订单以及       <strong style="color:#ff0000" id="spanNewPaid">0</strong> 个新付款的订单</p>
      <p align="center" style="word-break:break-all"><a href="order.php?act=list"><span style="color:#ff0000">点击查看新订单</span></a></p>
    </div>
    </td>
  </tr>
  </table>
</div>

<!--
<embed src="images/online.wav" width="0" height="0" autostart="false" name="msgBeep" id="msgBeep" enablejavascript="true"/>
-->
<object classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000" codebase="http://active.macromedia.com/flash2/cabs/swflash.cab#version=4,0,0,0" id="msgBeep" width="1" height="1">
  <param name="movie" value="images/online.swf">
  <param name="quality" value="high">
  <embed src="images/online.swf" name="msgBeep" id="msgBeep" quality="high" width="0" height="0" type="application/x-shockwave-flash" pluginspage="http://www.macromedia.com/shockwave/download/index.cgi?p1_prod_version=shockwaveflash">
  </embed>
</object>

<script language="JavaScript">
document.onmousemove=function(e)
{
  var obj = Utils.srcElement(e);
  if (typeof(obj.onclick) == 'function' && obj.onclick.toString().indexOf('listTable.edit') != -1)
  {
    obj.title = '点击修改内容';
    obj.style.cssText = 'background: #278296;';
    obj.onmouseout = function(e)
    {
      this.style.cssText = '';
    }
  }
  else if (typeof(obj.href) != 'undefined' && obj.href.indexOf('listTable.sort') != -1)
  {
    obj.title = '点击对列表排序';
  }
}
<!--


var MyTodolist;
function showTodoList(adminid)
{
  if(!MyTodolist)
  {
    var global = $import("../js/global.js","js");
    global.onload = global.onreadystatechange= function()
    {
      if(this.readyState && this.readyState=="loading")return;
      var md5 = $import("js/md5.js","js");
      md5.onload = md5.onreadystatechange= function()
      {
        if(this.readyState && this.readyState=="loading")return;
        var todolist = $import("js/todolist.js","js");
        todolist.onload = todolist.onreadystatechange = function()
        {
          if(this.readyState && this.readyState=="loading")return;
          MyTodolist = new Todolist();
          MyTodolist.show();
        }
      }
    }
  }
  else
  {
    if(MyTodolist.visibility)
    {
      MyTodolist.hide();
    }
    else
    {
      MyTodolist.show();
    }
  }
}

if (Browser.isIE)
{
  onscroll = function()
  {
    //document.getElementById('calculator').style.top = document.body.scrollTop;
    document.getElementById('popMsg').style.top = (document.body.scrollTop + document.body.clientHeight - document.getElementById('popMsg').offsetHeight) + "px";
  }
}

if (document.getElementById("listDiv"))
{
  document.getElementById("listDiv").onmouseover = function(e)
  {
    obj = Utils.srcElement(e);

    if (obj)
    {
      if (obj.parentNode.tagName.toLowerCase() == "tr") row = obj.parentNode;
      else if (obj.parentNode.parentNode.tagName.toLowerCase() == "tr") row = obj.parentNode.parentNode;
      else return;

      for (i = 0; i < row.cells.length; i++)
      {
        if (row.cells[i].tagName != "TH") row.cells[i].style.backgroundColor = '#F4FAFB';
      }
    }

  }

  document.getElementById("listDiv").onmouseout = function(e)
  {
    obj = Utils.srcElement(e);

    if (obj)
    {
      if (obj.parentNode.tagName.toLowerCase() == "tr") row = obj.parentNode;
      else if (obj.parentNode.parentNode.tagName.toLowerCase() == "tr") row = obj.parentNode.parentNode;
      else return;

      for (i = 0; i < row.cells.length; i++)
      {
          if (row.cells[i].tagName != "TH") row.cells[i].style.backgroundColor = '';
      }
    }
  }

  document.getElementById("listDiv").onclick = function(e)
  {
    var obj = Utils.srcElement(e);

    if (obj.tagName == "INPUT" && obj.type == "checkbox")
    {
      if (!document.forms['listForm'])
      {
        return;
      }
      var nodes = document.forms['listForm'].elements;
      var checked = false;

      for (i = 0; i < nodes.length; i++)
      {
        if (nodes[i].checked)
        {
           checked = true;
           break;
         }
      }

      if(document.getElementById("btnSubmit"))
      {
        document.getElementById("btnSubmit").disabled = !checked;
      }
      for (i = 1; i <= 10; i++)
      {
        if (document.getElementById("btnSubmit" + i))
        {
          document.getElementById("btnSubmit" + i).disabled = !checked;
        }
      }
    }
  }

}

//-->
</script>
</body>
</html>

<!-- $Id: bonus_type_info.htm 14216 2015-02-04 02:27:21Z derek $ -->

<script type="text/javascript" src="../js/calendar.php?lang="></script>
<link href="../js/calendar/calendar.css" rel="stylesheet" type="text/css" />

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>管理中心 - 编辑提货卷类型 </title>
<meta name="robots" content="noindex, nofollow">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link href="styles/general.css" rel="stylesheet" type="text/css" />
<link href="styles/main.css" rel="stylesheet" type="text/css" />
<link href="styles/chosen/chosen.css" rel="stylesheet" type="text/css">
<script type="text/javascript" src="js/jquery-1.6.2.min.js"></script><script type="text/javascript" src="js/chosen.jquery.min.js"></script><script type="text/javascript" src="js/jquery.json.js"></script><script type="text/javascript" src="../js/transport.js"></script><script type="text/javascript" src="js/common.js"></script><script language="JavaScript">
<!--
// 这里把JS用到的所有语言都赋值到这里
var process_request = "正在处理您的请求...";
var todolist_caption = "记事本";
var todolist_autosave = "自动保存";
var todolist_save = "保存";
var todolist_clear = "清除";
var todolist_confirm_save = "是否将更改保存到记事本？";
var todolist_confirm_clear = "是否清空内容？";
var send_count_empty = "请输入发放数量!";
var send_count_isnumber = "发放数量必须为数字格式!";
var type_name_empty = "请输入提货券类型名称!";
var type_money_empty = "请输入提货券金额!";
var type_money_isnumber = "提货券金额必须为数字格式!";
var use_start_date_empty = "使用起始日期不能为空";
var use_end_date_empty = "使用结束日期不能为空";
var use_start_lt_end = "使用开始日期不能大于结束日期";
var order_money_isnumber = "订单金额必须为数字格式!";
var bonus_sn_empty = "请输入红包的序列号!";
var bonus_sn_number = "红包的序列号必须是数字!";
var bonus_sum_empty = "请输入您要发放的红包数量!";
var bonus_sum_number = "红包的发放数量必须是一个整数!";
var bonus_type_empty = "请选择红包的类型金额!";
var user_rank_empty = "您没有指定会员等级!";
var user_name_empty = "您至少需要选择一个会员!";
var invalid_min_amount = "请输入订单下限（大于0的数字）";
var send_start_lt_end = "红包发放开始日期不能大于结束日期";
//-->
</script>
</head>
<body>

<div id="menu_list" onmouseover="show_popup()" onmouseout="hide_popup()">
<ul>
<li><a href="goods.php?act=add" target="main_frame">添加新商品</a></li>
<li><a href="category.php?act=add" target="main_frame">添加商品分类</a></li>
<li><a href="order.php?act=add" target="main_frame">添加订单</a></li>
<li><a href="article.php?act=add" target="main_frame">添加新文章</a></li>
<li><a href="users.php?act=add" target="main_frame">添加会员</a></li>
</ul>
</div>
<script>
function show_popup(){
frmBody = parent.document.getElementById('frame-body');
if (frmBody.cols == "37, 12, *")
{
parent.main_frame.document.getElementById('menu_list').style.left = '195px';
}
else
{
parent.main_frame.document.getElementById('menu_list').style.left = '40px';
}
parent.main_frame.document.getElementById('menu_list').style.display = 'block';
}
function hide_popup(){

parent.main_frame.document.getElementById('menu_list').style.display = 'none';
}
</script>
<h1>
<span class="action-span"><a href="takegoods.php?act=list&uselastfilter=1">提货券类型列表</a></span>
<span class="action-span1"><a href="index.php?act=main">管理中心</a> </span><span id="search_id" class="action-span1"> - 编辑提货卷类型 </span>
<div style="clear:both"></div>
</h1>
<div class="main-div">
<form action="takegoods.php" method="post" name="theForm"  onsubmit="return validate()">
<table width="100%">
  <tr>
    <td class="label">提货券类型名称</td>
    <td>
      <input type='text' name='type_name' maxlength="30" value="提货券" size='20' /><span class="require-field">*</span></td>
  </tr>

  <tr>
    <td class="label">提货券单品金额</td>
    <td>
    <input type="text" id="type_money" name="type_money" value="11111" size="20" onclick="type_money_on('',1,this.value)" onblur="type_money_on('',2,this.value)" oninput="type_money_on('',3,this.value)" />
    </td>
  </tr> 
  
  <tr>
    <td class="label">可用次数</td>
    <td>
    <input type="text" id="type_money_count" name="type_money_count" value="100" size="20" onclick="type_money_on('_count',1,this.value)" onblur="type_money_on('_count',2,this.value)" oninput="type_money_on('_count',3,this.value)" />
    </td>
  </tr> 
  
  <tr>
    <td class="label">提货券总金额</td>
    <td>
    <input type="text" id="type_money_all" name="type_money_all" value="" size="20" readonly="readonly" />
    </td>
  </tr>
  <script language="javascript">
  document.getElementById("type_money_all").value = document.getElementById("type_money").value * document.getElementById("type_money_count").value;
  function type_money_on(i,j,v)
  {
	  switch (j)
	  {
		  case 1 :
			  if (document.getElementById("type_money"+i).value == 0)
			  	  document.getElementById("type_money"+i).value = "";
		  	  break;
		  case 2 :
		  	  if (document.getElementById("type_money"+i).value == "")
			  {
				  document.getElementById("type_money"+i).value = 0;
		  	  	  document.getElementById("type_money_all").value = 0;
			  }
		  	  break;
	  }
	  document.getElementById("type_money_all").value = document.getElementById("type_money").value * document.getElementById("type_money_count").value;
  }
  </script> 

  <tr>
    <td class="label">
	  <a href="javascript:showNotice('Use_start_a');" title="点击此处查看提示信息">
      <img src="images/notice.gif" width="16" height="16" border="0" alt="点击此处查看提示信息"></a>
	使用起始日期</td>
    <td>
      <input name="use_start_date" type="text" id="use_start_date" size="22" value='2015-07-26' readonly="readonly" /><input name="selbtn3" type="button" id="selbtn3" onclick="return showCalendar('use_start_date', '%Y-%m-%d', false, false, 'selbtn3');" value="选择" class="button"/><span class="require-field">*</span>
	  <br /><span class="notice-span" style="display:block"  id="Use_start_a">只有当前时间介于起始日期和截止日期之间时，此类型的提货券才可以使用</span>    </td>
  </tr>
  <tr>
    <td class="label">使用结束日期</td>
    <td>
      <input name="use_end_date" type="text" id="use_end_date" size="22" value='2017-07-28' readonly="readonly" /><input name="selbtn4" type="button" id="selbtn4" onclick="return showCalendar('use_end_date', '%Y-%m-%d', false, false, 'selbtn4');" value="选择" class="button"/> <span class="require-field">*</span>   </td>
  </tr>
  <tr>
    <td class="label">&nbsp;</td>
    <td>
      <input type="submit" value=" 确定 " class="button" />
      <input type="reset" value=" 重置 " class="button" />
      <input type="hidden" name="act" value="update" />
      <input type="hidden" name="type_id" value="3" />    </td>
  </tr>
</table>
</form>
</div>
<script type="text/javascript" src="../js/utils.js"></script><script type="text/javascript" src="js/validator.js"></script>
<script language="javascript">
<!--
document.forms['theForm'].elements['type_name'].focus();
/**
 * 检查表单输入的数据
 */
function validate()
{
  validator = new Validator("theForm");
  validator.required("type_name",      type_name_empty);
  validator.required("type_money",     type_money_empty);
  validator.isNumber("type_money",     type_money_isnumber, true);
  validator.isNumber("type_money_count",     '可用次数为整数！', true);
  validator.isNumber("type_money_all",     "提货券总金额为整数！", true);
  validator.required("use_start_date",      use_start_date_empty);
  validator.required("use_end_date",      use_end_date_empty);
  validator.islt('use_start_date', 'use_end_date', use_start_lt_end);
  
  return validator.passed();
}
onload = function()
{
  
  get_value = '';
  

  showunit(get_value)
  // 开始检查订单
  startCheckOrder();
}
/* 红包金额按订单金额发放时才填写 */
function gObj(obj)
{
  var theObj;
  if (document.getElementById)
  {
    if (typeof obj=="string") {
      return document.getElementById(obj);
    } else {
      return obj.style;
    }
  }
  return null;
}

function showunit(get_value)
{
  gObj("1").style.display =  (get_value == 2) ? "" : "none";
  document.forms['theForm'].elements['selbtn1'].disabled  = (get_value != 1 && get_value != 2);
  document.forms['theForm'].elements['selbtn2'].disabled  = (get_value != 1 && get_value != 2);

  return;
}
//-->
</script>

<div id="footer">
共执行 2 个查询，用时 0.008001 秒，Gzip 已禁用，内存占用 3.078 MB<br />
版权所有 &copy; 2008-2030 广州网软志成信息科技有限公司，并保留所有权利。</div>
<!-- 新订单提示信息 -->
<div id="popMsg">
  <table cellspacing="0" cellpadding="0" width="100%" bgcolor="#cfdef4" border="0">
  <tr>
    <td style="color: #0f2c8c" width="30" height="24"></td>
    <td style="font-weight: normal; color: #1f336b; padding-top: 4px;padding-left: 4px" valign="center" width="100%"> 新订单通知</td>
    <td style="padding-top: 2px;padding-right:2px" valign="center" align="right" width="19"><span title="关闭" style="cursor: hand;cursor:pointer;color:red;font-size:12px;font-weight:bold;margin-right:4px;" onclick="Message.close()" >×</span><!-- <img title=关闭 style="cursor: hand" onclick=closediv() hspace=3 src="msgclose.jpg"> --></td>
  </tr>
  <tr>
    <td style="padding-right: 1px; padding-bottom: 1px" colspan="3" height="70">
    <div id="popMsgContent">
      <p>您有 <strong style="color:#ff0000" id="spanNewOrder">1</strong> 个新订单以及       <strong style="color:#ff0000" id="spanNewPaid">0</strong> 个新付款的订单</p>
      <p align="center" style="word-break:break-all"><a href="order.php?act=list"><span style="color:#ff0000">点击查看新订单</span></a></p>
    </div>
    </td>
  </tr>
  </table>
</div>

<!--
<embed src="images/online.wav" width="0" height="0" autostart="false" name="msgBeep" id="msgBeep" enablejavascript="true"/>
-->
<object classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000" codebase="http://active.macromedia.com/flash2/cabs/swflash.cab#version=4,0,0,0" id="msgBeep" width="1" height="1">
  <param name="movie" value="images/online.swf">
  <param name="quality" value="high">
  <embed src="images/online.swf" name="msgBeep" id="msgBeep" quality="high" width="0" height="0" type="application/x-shockwave-flash" pluginspage="http://www.macromedia.com/shockwave/download/index.cgi?p1_prod_version=shockwaveflash">
  </embed>
</object>

<script language="JavaScript">
document.onmousemove=function(e)
{
  var obj = Utils.srcElement(e);
  if (typeof(obj.onclick) == 'function' && obj.onclick.toString().indexOf('listTable.edit') != -1)
  {
    obj.title = '点击修改内容';
    obj.style.cssText = 'background: #278296;';
    obj.onmouseout = function(e)
    {
      this.style.cssText = '';
    }
  }
  else if (typeof(obj.href) != 'undefined' && obj.href.indexOf('listTable.sort') != -1)
  {
    obj.title = '点击对列表排序';
  }
}
<!--


var MyTodolist;
function showTodoList(adminid)
{
  if(!MyTodolist)
  {
    var global = $import("../js/global.js","js");
    global.onload = global.onreadystatechange= function()
    {
      if(this.readyState && this.readyState=="loading")return;
      var md5 = $import("js/md5.js","js");
      md5.onload = md5.onreadystatechange= function()
      {
        if(this.readyState && this.readyState=="loading")return;
        var todolist = $import("js/todolist.js","js");
        todolist.onload = todolist.onreadystatechange = function()
        {
          if(this.readyState && this.readyState=="loading")return;
          MyTodolist = new Todolist();
          MyTodolist.show();
        }
      }
    }
  }
  else
  {
    if(MyTodolist.visibility)
    {
      MyTodolist.hide();
    }
    else
    {
      MyTodolist.show();
    }
  }
}

if (Browser.isIE)
{
  onscroll = function()
  {
    //document.getElementById('calculator').style.top = document.body.scrollTop;
    document.getElementById('popMsg').style.top = (document.body.scrollTop + document.body.clientHeight - document.getElementById('popMsg').offsetHeight) + "px";
  }
}

if (document.getElementById("listDiv"))
{
  document.getElementById("listDiv").onmouseover = function(e)
  {
    obj = Utils.srcElement(e);

    if (obj)
    {
      if (obj.parentNode.tagName.toLowerCase() == "tr") row = obj.parentNode;
      else if (obj.parentNode.parentNode.tagName.toLowerCase() == "tr") row = obj.parentNode.parentNode;
      else return;

      for (i = 0; i < row.cells.length; i++)
      {
        if (row.cells[i].tagName != "TH") row.cells[i].style.backgroundColor = '#F4FAFB';
      }
    }

  }

  document.getElementById("listDiv").onmouseout = function(e)
  {
    obj = Utils.srcElement(e);

    if (obj)
    {
      if (obj.parentNode.tagName.toLowerCase() == "tr") row = obj.parentNode;
      else if (obj.parentNode.parentNode.tagName.toLowerCase() == "tr") row = obj.parentNode.parentNode;
      else return;

      for (i = 0; i < row.cells.length; i++)
      {
          if (row.cells[i].tagName != "TH") row.cells[i].style.backgroundColor = '';
      }
    }
  }

  document.getElementById("listDiv").onclick = function(e)
  {
    var obj = Utils.srcElement(e);

    if (obj.tagName == "INPUT" && obj.type == "checkbox")
    {
      if (!document.forms['listForm'])
      {
        return;
      }
      var nodes = document.forms['listForm'].elements;
      var checked = false;

      for (i = 0; i < nodes.length; i++)
      {
        if (nodes[i].checked)
        {
           checked = true;
           break;
         }
      }

      if(document.getElementById("btnSubmit"))
      {
        document.getElementById("btnSubmit").disabled = !checked;
      }
      for (i = 1; i <= 10; i++)
      {
        if (document.getElementById("btnSubmit" + i))
        {
          document.getElementById("btnSubmit" + i).disabled = !checked;
        }
      }
    }
  }

}

//-->
</script>
</body>
</html>

<!-- $Id: goods_export.htm 17107 2015-02-04 03:29:13Z derek $ -->
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>管理中心 - 配置商品 </title>
<meta name="robots" content="noindex, nofollow">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link href="styles/general.css" rel="stylesheet" type="text/css" />
<link href="styles/main.css" rel="stylesheet" type="text/css" />
<link href="styles/chosen/chosen.css" rel="stylesheet" type="text/css">
<script type="text/javascript" src="js/jquery-1.6.2.min.js"></script><script type="text/javascript" src="js/chosen.jquery.min.js"></script><script type="text/javascript" src="js/jquery.json.js"></script><script type="text/javascript" src="../js/transport.js"></script><script type="text/javascript" src="js/common.js"></script><script language="JavaScript">
<!--
// 这里把JS用到的所有语言都赋值到这里
var process_request = "正在处理您的请求...";
var todolist_caption = "记事本";
var todolist_autosave = "自动保存";
var todolist_save = "保存";
var todolist_clear = "清除";
var todolist_confirm_save = "是否将更改保存到记事本？";
var todolist_confirm_clear = "是否清空内容？";
var send_count_empty = "请输入发放数量!";
var send_count_isnumber = "发放数量必须为数字格式!";
var type_name_empty = "请输入提货券类型名称!";
var type_money_empty = "请输入提货券金额!";
var type_money_isnumber = "提货券金额必须为数字格式!";
var use_start_date_empty = "使用起始日期不能为空";
var use_end_date_empty = "使用结束日期不能为空";
var use_start_lt_end = "使用开始日期不能大于结束日期";
var order_money_isnumber = "订单金额必须为数字格式!";
var bonus_sn_empty = "请输入红包的序列号!";
var bonus_sn_number = "红包的序列号必须是数字!";
var bonus_sum_empty = "请输入您要发放的红包数量!";
var bonus_sum_number = "红包的发放数量必须是一个整数!";
var bonus_type_empty = "请选择红包的类型金额!";
var user_rank_empty = "您没有指定会员等级!";
var user_name_empty = "您至少需要选择一个会员!";
var invalid_min_amount = "请输入订单下限（大于0的数字）";
var send_start_lt_end = "红包发放开始日期不能大于结束日期";
//-->
</script>
</head>
<body>

<div id="menu_list" onmouseover="show_popup()" onmouseout="hide_popup()">
<ul>
<li><a href="goods.php?act=add" target="main_frame">添加新商品</a></li>
<li><a href="category.php?act=add" target="main_frame">添加商品分类</a></li>
<li><a href="order.php?act=add" target="main_frame">添加订单</a></li>
<li><a href="article.php?act=add" target="main_frame">添加新文章</a></li>
<li><a href="users.php?act=add" target="main_frame">添加会员</a></li>
</ul>
</div>
<script>
function show_popup(){
frmBody = parent.document.getElementById('frame-body');
if (frmBody.cols == "37, 12, *")
{
parent.main_frame.document.getElementById('menu_list').style.left = '195px';
}
else
{
parent.main_frame.document.getElementById('menu_list').style.left = '40px';
}
parent.main_frame.document.getElementById('menu_list').style.display = 'block';
}
function hide_popup(){

parent.main_frame.document.getElementById('menu_list').style.display = 'none';
}
</script>
<h1>
<span class="action-span"><a href="takegoods.php?act=list">提货券类型列表</a></span>
<span class="action-span1"><a href="index.php?act=main">管理中心</a> </span><span id="search_id" class="action-span1"> - 配置商品 </span>
<div style="clear:both"></div>
</h1>
<table width="100%">
<tr>
  <td>
  <div class="main-div" style="background-color: white;">
  <div>
    <form action="goods_export.php" method="post" name="searchForm" onsubmit="return queryGoods(this)">
        <!-- 商品搜索 -->
        <!-- $Id: goods_search.htm 16790 2009-11-10 08:56:15Z wangleisvn $ -->
<link href="styles/zTree/zTreeStyle.css" rel="stylesheet" type="text/css" />
<link href="styles/chosen/chosen.css" rel="stylesheet" type="text/css">
<script type="text/javascript" src="js/jquery.ztree.all-3.5.min.js"></script><script type="text/javascript" src="js/category_selecter.js"></script><div class="form-div">
    <form action="javascript:searchGoods()" name="searchForm">
        <img src="images/icon_search.gif" width="26" height="22" border="0" alt="SEARCH" />
                <!-- 分类 -->
        <input type="text" id="cat_name" name="cat_name" nowvalue="" value="" >
        <input type="hidden" id="cat_id" name="cat_id" value="">
        <!-- 品牌 -->
        <select  class="chzn-select" name="brand_id"><option value="0">商品品牌</option><option value="54">缪诗</option><option value="68">格力</option><option value="69">老板</option><option value="70">西门子</option><option value="71">格兰仕</option><option value="72">海信</option><option value="73">伊莱克斯</option><option value="74">艾力斯特</option><option value="75">博洋家纺</option><option value="76">富安娜</option><option value="77">爱仕达</option><option value="78">罗莱</option><option value="67">美的</option><option value="66">海尔</option><option value="55">卓诗尼</option><option value="56">七匹狼</option><option value="57">佐丹奴</option><option value="58">达芙妮</option><option value="59">她他/tata</option><option value="60">曼妮芬（ManniForm）</option><option value="61">伊芙丽</option><option value="62">稻草人</option><option value="63">斯提亚</option><option value="64">袋鼠</option><option value="65">爱华仕</option><option value="79">安睡宝</option><option value="80">溢彩年华</option><option value="94">王老吉</option><option value="95">可口可乐</option><option value="96">贝古贝古</option><option value="97">皇家宝贝</option><option value="98">呵宝童车</option><option value="99">合生元</option><option value="100">美赞臣</option><option value="101">帮宝适</option><option value="102">抱抱熊</option><option value="103">巴拉巴拉</option><option value="104">青蛙王子</option><option value="93">统一</option><option value="92">加多宝</option><option value="81">慧乐家</option><option value="82">天堂伞</option><option value="83">水星家纺</option><option value="84">全有家居</option><option value="85">五粮液</option><option value="86">泸州老窖</option><option value="87">洋河</option><option value="88">郎酒</option><option value="89">锐澳</option><option value="90">雪花</option><option value="91">哈尔滨</option><option value="105">雀氏</option><option value="1">资生堂</option><option value="15">韩束</option><option value="16">卡姿兰</option><option value="17">珀莱雅</option><option value="18">兰芝</option><option value="19">碧欧泉</option><option value="20">小米</option><option value="21">摩托罗拉</option><option value="22">中兴</option><option value="23">朵唯</option><option value="24">htc</option><option value="25">华为</option><option value="14">高丝</option><option value="13">SK-ll</option><option value="2">CK</option><option value="3">Disney</option><option value="4">雅诗兰黛</option><option value="5">相宜本草</option><option value="6">Dior</option><option value="7">爱丽</option><option value="8">雅顿</option><option value="9">狮王</option><option value="10">高丝洁</option><option value="11">MISS FACE</option><option value="12">姬芮</option><option value="26">oppo</option><option value="27">金立</option><option value="42">君乐宝</option><option value="43">光明</option><option value="44">三元</option><option value="45">百草味</option><option value="46">三只松鼠</option><option value="47">口水娃</option><option value="48">楼兰密语</option><option value="49">西域美农</option><option value="50">糖糖屋</option><option value="51">享爱.</option><option value="52">猫人</option><option value="40">蒙牛</option><option value="39">海底捞</option><option value="28">LG</option><option value="29">苹果</option><option value="30">三星</option><option value="31">乐檬</option><option value="32">努比亚</option><option value="41">伊利</option><option value="34">肯德基</option><option value="35">麦当劳</option><option value="36">小肥羊</option><option value="37">小尾羊</option><option value="38">必胜客</option><option value="53">茵曼（INMAN）</option></select>
        <!-- 推荐 -->
                                        <!-- 上架 -->
                        <!-- 关键字 -->
        关键字 <input type="text" name="keyword" size="15" />
        <input type="submit" value=" 搜索 " class="button" />
                 
    </form>
</div>


<script language="JavaScript">

    $().ready(function(){
       $(".chzn-select").chosen();
       $(".chzn-container").click(function(){
     	$("#menuContent_cat_id").hide();
    	})
		$('.chzn-container-single:last').css('width','60px');
    });

    function searchGoods()
    {
        
                listTable.filter['cat_id'] = document.forms['searchForm'].elements['cat_id'].value;
        listTable.filter['brand_id'] = document.forms['searchForm'].elements['brand_id'].value;
                    listTable.filter['intro_type'] = document.forms['searchForm'].elements['intro_type'].value;
                                                listTable.filter['is_on_sale'] = document.forms['searchForm'].elements['is_on_sale'].value;
                                            
                        listTable.filter['keyword'] = Utils.trim(document.forms['searchForm'].elements['keyword'].value);

                        listTable.filter['page'] = 1;

                        listTable.loadList();
                    }
</script>

<script type="text/javascript">
    $().ready(function(){
        // $("#cat_name")为获取分类名称的jQuery对象，可根据实际情况修改
        // $("#cat_id")为获取分类ID的jQuery对象，可根据实际情况修改
        // ""为被选中的商品分类编号，无则设置为null或者不写此参数或者为空字符串
        $.ajaxCategorySelecter($("#cat_name"), $("#cat_id"), "");
    });

    function searchSuppliers()
    {
        var filter = new Object;
        filter.keyword = document.forms['searchForm'].elements['search_suppliers'].value;
        Ajax.call('goods.php?is_ajax=1&act=search_suppliers', filter, searchSuppliersResponse, 'GET', 'JSON')
    }

    function searchSuppliersResponse(result)
    {
    	
        var frm = document.forms['searchForm'];
        var sel = frm.elements['suppliers_id'];

        if (result.error == 0)
        {
            /* 清除 options */
            sel.length = 0;

            /* 创建 options */
            var suppliers = result.content;
            if (suppliers)
            {
                for (i = 0; i < suppliers.length; i++)
                {
                    var opt = document.createElement("OPTION");
                    opt.value = suppliers[i].supplier_id;
                    opt.text  = suppliers[i].supplier_name;
                    sel.options.add(opt);
                }
            }
            else
            {
                var opt = document.createElement("OPTION");
                opt.value = 0;
                opt.text  = search_is_null;
                sel.options.add(opt);
            }
        }

        if (result.message.length > 0)
        {
            alert(result.message);
        }
    }

    function batch_export()
    {
        // 分类
        var cat_id = document.forms['searchForm'].elements['cat_id'].value;
        // 品牌
        var brand_id = document.forms['searchForm'].elements['brand_id'].value;
        // 入驻商
        if (typeof(document.forms['searchForm'].elements['suppliers_id']) == "undefined")
        {
            var suppliers_exists = 0;
            var suppliers_id = 0;
        }
        else
        {
            var suppliers_exists = 1;
            var suppliers_id = document.forms['searchForm'].elements['suppliers_id'].value;
        }
        // 审核状态
        if (typeof(document.forms['searchForm'].elements['supplier_status']) == "undefined")
        {
            var supplier_status = '';
        }
        else
        {
            var supplier_status = document.forms['searchForm'].elements['supplier_status'].value;
        }
        // 推荐
        if (typeof(document.forms['searchForm'].elements['intro_type']) == "undefined")
        {
            var intro_type = '';
        }
        else
        {
            var intro_type = document.forms['searchForm'].elements['intro_type'].value;
        }
        // 上架
        var is_on_sale = document.forms['searchForm'].elements['is_on_sale'].value;
        // 搜索关键字
        var keyword = Utils.trim(document.forms['searchForm'].elements['keyword'].value);
        return location.href='goods.php?act=export&cat_id='+cat_id+'&brand_id='
                +brand_id+'&suppliers_id='+suppliers_id+'&supplier_status='+supplier_status+
                '&intro_type='+intro_type+'&is_on_sale='+is_on_sale+'&keyword='+keyword+'&suppliers_exists='+suppliers_exists;
    }
</script>        <!--
	  <img src="images/icon_search.gif" width="26" height="22" border="0" alt="SEARCH" />

      <!-- 分类 -->
        <!--
      <select name="cat_id"><option value="0">所有分类</option><option value="5" >家用电器</option><option value="182" >&nbsp;&nbsp;&nbsp;&nbsp;五金家装</option><option value="218" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;家具五金</option><option value="219" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;电工电料</option><option value="216" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;手动工具</option><option value="220" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;监控安防</option><option value="217" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;厨卫五金</option><option value="215" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;电动工具</option><option value="179" >&nbsp;&nbsp;&nbsp;&nbsp;生活电器</option><option value="193" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;加湿器</option><option value="197" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;饮水机</option><option value="194" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;吸尘器</option><option value="198" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;其它生活电器</option><option value="195" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;挂烫机/熨斗</option><option value="192" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;净化器</option><option value="191" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;电风扇</option><option value="196" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;取暖电器</option><option value="180" >&nbsp;&nbsp;&nbsp;&nbsp;厨房电器</option><option value="206" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;其它厨房电器</option><option value="203" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;电炖锅</option><option value="200" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;微波炉</option><option value="204" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;果蔬解毒机</option><option value="201" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;电磁炉</option><option value="205" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;养生壶/煎药壶</option><option value="202" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;电饼铛/烧烤盘</option><option value="199" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;电饭煲</option><option value="181" >&nbsp;&nbsp;&nbsp;&nbsp;个护健康</option><option value="209" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;按摩椅</option><option value="213" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;计步器/脂肪检测</option><option value="210" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;足浴盆</option><option value="207" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;剃须刀</option><option value="214" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;其它健康电器</option><option value="211" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;健康秤/厨房秤</option><option value="208" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;电吹风</option><option value="212" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;血糖仪</option><option value="178" >&nbsp;&nbsp;&nbsp;&nbsp;大家电</option><option value="185" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;洗衣机</option><option value="189" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;消毒柜/洗碗机</option><option value="186" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;家庭影院</option><option value="183" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;平板电视</option><option value="190" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;冷柜/冰吧</option><option value="187" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;烟机/灶具</option><option value="184" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;空调冰箱</option><option value="188" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;热水器</option><option value="4" >手机、数码、通信</option><option value="147" >&nbsp;&nbsp;&nbsp;&nbsp;智能设备</option><option value="169" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;体感车</option><option value="166" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;智能眼镜</option><option value="167" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;运动跟踪器</option><option value="164" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;智能手环</option><option value="168" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;智能家居</option><option value="165" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;智能手表</option><option value="144" >&nbsp;&nbsp;&nbsp;&nbsp;热卖手机</option><option value="153" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;联通4G</option><option value="150" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;小米特供</option><option value="154" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;电信4G</option><option value="151" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;魅族手机</option><option value="148" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;三星盖乐世</option><option value="155" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;移动4G</option><option value="152" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;华为荣耀</option><option value="149" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;iPhone</option><option value="145" >&nbsp;&nbsp;&nbsp;&nbsp;手机配件</option><option value="163" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;保护套</option><option value="160" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;创意配件</option><option value="157" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;移动电源</option><option value="161" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;手机饰品</option><option value="158" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;蓝牙耳机</option><option value="162" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;手机耳机</option><option value="159" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;充电器</option><option value="156" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;电池</option><option value="146" >&nbsp;&nbsp;&nbsp;&nbsp;数码影音</option><option value="176" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;数码相框</option><option value="173" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;运动相机</option><option value="170" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;数码相机</option><option value="177" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;影棚器材</option><option value="174" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;摄像机</option><option value="171" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;单反相机</option><option value="175" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;户外器材</option><option value="172" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;拍立得</option><option value="358" >电脑、办公</option><option value="6" >家居、家具、家装、厨具</option><option value="310" >&nbsp;&nbsp;&nbsp;&nbsp;家装软饰</option><option value="348" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;墙贴/装饰贴</option><option value="345" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;帘艺隔断</option><option value="342" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;桌布/罩件</option><option value="349" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;摆件花瓶</option><option value="346" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;相框/照片墙</option><option value="343" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;地毯地垫</option><option value="347" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;装饰字画</option><option value="344" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;沙发垫套/椅垫</option><option value="307" >&nbsp;&nbsp;&nbsp;&nbsp;家具</option><option value="322" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;餐厅家具</option><option value="326" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;沙发</option><option value="323" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;书房家具</option><option value="320" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;卧室家具</option><option value="327" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;鞋架/衣帽架</option><option value="324" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;储物家具</option><option value="321" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;客厅家具</option><option value="325" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;阳台/户外</option><option value="311" >&nbsp;&nbsp;&nbsp;&nbsp;生活日用</option><option value="354" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;洗晒/熨烫</option><option value="351" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;雨伞雨具</option><option value="355" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;净化除味</option><option value="352" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;浴室用品</option><option value="353" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;缝纫/针织用品</option><option value="350" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;收纳用品</option><option value="306" >&nbsp;&nbsp;&nbsp;&nbsp;家纺</option><option value="319" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;窗帘/窗纱</option><option value="316" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;床单被罩</option><option value="313" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;被子</option><option value="317" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;毛巾浴巾</option><option value="314" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;蚊帐</option><option value="318" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;床垫/床褥</option><option value="315" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;凉席</option><option value="312" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;床品套件</option><option value="308" >&nbsp;&nbsp;&nbsp;&nbsp;厨具</option><option value="332" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;餐具</option><option value="329" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;刀剪菜板</option><option value="333" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;茶具/咖啡具</option><option value="330" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;厨房配件</option><option value="331" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;水具酒具</option><option value="328" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;烹饪锅具</option><option value="309" >&nbsp;&nbsp;&nbsp;&nbsp;灯具</option><option value="338" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;落地灯</option><option value="335" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;吸顶灯</option><option value="339" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;应急灯/手电</option><option value="336" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;筒灯射灯</option><option value="340" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;装饰灯</option><option value="337" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;LED灯</option><option value="334" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;台灯</option><option value="341" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;吊灯</option><option value="2" >男装、女装、内衣、珠宝</option><option value="57" >&nbsp;&nbsp;&nbsp;&nbsp;男装馆</option><option value="83" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;休闲短裤</option><option value="80" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;风衣</option><option value="77" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;休闲裤</option><option value="84" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;POLO衫</option><option value="81" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;针织衫</option><option value="78" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;牛仔裤</option><option value="82" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;长袖衬衫</option><option value="79" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;夹克</option><option value="55" >&nbsp;&nbsp;&nbsp;&nbsp;女装馆</option><option value="67" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;短外套</option><option value="64" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;时尚套装</option><option value="61" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;连衣裙</option><option value="68" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;防晒衫</option><option value="65" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;复古旗袍</option><option value="62" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;连体裤</option><option value="66" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;牛仔裤</option><option value="63" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;棉麻T恤</option><option value="58" >&nbsp;&nbsp;&nbsp;&nbsp;户外鞋服</option><option value="89" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;迷彩裤</option><option value="86" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;篮球鞋</option><option value="90" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;沙滩鞋</option><option value="87" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;帆布鞋</option><option value="91" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;钓鱼服</option><option value="88" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;羽毛球鞋</option><option value="85" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;跑步鞋</option><option value="92" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;登山鞋</option><option value="59" >&nbsp;&nbsp;&nbsp;&nbsp;女鞋馆</option><option value="99" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;乐福鞋</option><option value="96" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;坡跟单鞋</option><option value="93" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;高跟凉拖</option><option value="100" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;松糕鞋</option><option value="97" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;浅口单鞋</option><option value="94" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;平底鞋</option><option value="98" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;帆布鞋</option><option value="95" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;高跟鞋</option><option value="56" >&nbsp;&nbsp;&nbsp;&nbsp;内衣馆</option><option value="73" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;男士内裤</option><option value="70" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;薄款文胸</option><option value="74" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;夏季睡衣</option><option value="71" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;无钢圈文胸</option><option value="75" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;性感睡衣</option><option value="72" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;女士内裤</option><option value="69" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;聚拢文胸</option><option value="76" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;瘦腿袜</option><option value="60" >&nbsp;&nbsp;&nbsp;&nbsp;箱包馆</option><option value="105" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;男士钱包</option><option value="102" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;手提女包</option><option value="106" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;旅行箱</option><option value="103" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;女士钱包</option><option value="107" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;拉杆箱</option><option value="104" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;男士双肩</option><option value="101" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;单肩女包</option><option value="108" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;拉杆包</option><option value="3" >个护化妆、清洁用品</option><option value="113" >&nbsp;&nbsp;&nbsp;&nbsp;香水彩妆</option><option value="137" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;底妆</option><option value="141" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;美甲</option><option value="138" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;腮红</option><option value="142" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;美容工具</option><option value="139" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;眼部</option><option value="136" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;香水</option><option value="143" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;套装</option><option value="140" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;唇部</option><option value="112" >&nbsp;&nbsp;&nbsp;&nbsp;口腔护理</option><option value="134" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;漱口水</option><option value="135" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;套装</option><option value="132" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;牙膏/牙粉</option><option value="133" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;牙刷/牙线</option><option value="109" >&nbsp;&nbsp;&nbsp;&nbsp;面部护肤</option><option value="118" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;套装</option><option value="115" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;护肤</option><option value="116" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;面膜</option><option value="117" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;剃须</option><option value="114" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;清洁</option><option value="110" >&nbsp;&nbsp;&nbsp;&nbsp;洗发护发</option><option value="121" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;染发</option><option value="122" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;造型</option><option value="119" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;洗发</option><option value="123" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;假发</option><option value="120" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;护发</option><option value="124" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;套装</option><option value="111" >&nbsp;&nbsp;&nbsp;&nbsp;身体护肤</option><option value="131" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;套装</option><option value="128" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;手足</option><option value="125" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;沐浴</option><option value="129" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;纤体塑形</option><option value="126" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;润肤</option><option value="130" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 美胸</option><option value="127" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;颈部</option><option value="7" >酒类饮料</option><option value="273" >&nbsp;&nbsp;&nbsp;&nbsp;饮料饮品</option><option value="289" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;果蔬汁</option><option value="296" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;植物蛋白饮料</option><option value="293" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;纯牛奶</option><option value="290" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;茶饮料</option><option value="294" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;酸奶</option><option value="291" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;碳酸饮料</option><option value="295" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;风味奶</option><option value="292" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;功能饮料</option><option value="274" >&nbsp;&nbsp;&nbsp;&nbsp;茗茶</option><option value="305" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;其他茶</option><option value="302" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;红茶</option><option value="299" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;普洱</option><option value="303" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;花果茶</option><option value="300" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;龙井</option><option value="304" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;养生茶</option><option value="301" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;绿茶</option><option value="298" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;铁观音</option><option value="271" >&nbsp;&nbsp;&nbsp;&nbsp;酒水</option><option value="280" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;养生酒</option><option value="277" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;啤酒</option><option value="281" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;预调酒</option><option value="278" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;葡萄酒/果酒</option><option value="279" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;黄酒/米酒</option><option value="276" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;白酒</option><option value="272" >&nbsp;&nbsp;&nbsp;&nbsp;冲调饮品</option><option value="286" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;奶茶</option><option value="283" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;成人奶粉</option><option value="287" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;麦片谷物</option><option value="284" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;豆浆/豆奶粉</option><option value="297" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;果味冲调</option><option value="288" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;咖啡</option><option value="285" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;茶叶</option><option value="282" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;蜂蜜</option><option value="359" >鞋靴、箱包、钟表、奢侈品</option><option value="360" >运动户外</option><option value="361" >汽车、汽车用品</option><option value="8" >母婴、玩具乐器</option><option value="225" >&nbsp;&nbsp;&nbsp;&nbsp;车床/床品</option><option value="257" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;婴儿床</option><option value="261" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;睡袋/抱被</option><option value="258" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;餐椅</option><option value="255" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;安全座椅</option><option value="262" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;凉席/蚊帐</option><option value="259" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;三轮车</option><option value="256" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;手推车</option><option value="260" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;儿童家具</option><option value="222" >&nbsp;&nbsp;&nbsp;&nbsp;营养/辅食</option><option value="238" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;清火开胃</option><option value="235" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;面食类</option><option value="239" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;钙铁锌</option><option value="236" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;宝宝零食</option><option value="233" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;米粉</option><option value="240" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;益生菌</option><option value="237" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;DHA</option><option value="234" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;果汁/泥</option><option value="226" >&nbsp;&nbsp;&nbsp;&nbsp;孕妈专区</option><option value="270" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;孕妇内裤</option><option value="267" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;妈咪包</option><option value="264" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;打底裤</option><option value="268" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;收腹带</option><option value="265" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;防辐射服</option><option value="269" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;哺乳文胸</option><option value="266" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;腰凳</option><option value="263" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;孕妇裙</option><option value="223" >&nbsp;&nbsp;&nbsp;&nbsp;孕婴洗护</option><option value="241" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;洗衣液/皂</option><option value="245" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;爽身粉</option><option value="242" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;宝宝沐浴</option><option value="246" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;奶瓶清洗</option><option value="243" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;儿童防晒</option><option value="247" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;孕妇护肤</option><option value="244" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;防蚊/驱蚊</option><option value="224" >&nbsp;&nbsp;&nbsp;&nbsp;喂养用品</option><option value="254" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;防溢乳垫</option><option value="251" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;水壶/水杯</option><option value="248" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;奶嘴奶瓶</option><option value="252" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;吸奶器</option><option value="249" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;驱蚊用品</option><option value="253" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;理发器</option><option value="250" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;浴室用品</option><option value="221" >&nbsp;&nbsp;&nbsp;&nbsp;孕婴奶粉</option><option value="232" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;3段</option><option value="229" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;pre段</option><option value="230" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;1段</option><option value="227" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;特配奶粉</option><option value="231" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;2段</option><option value="228" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;孕妈奶粉</option><option value="1" >食品、酒类、生鲜、特产</option><option value="14" >&nbsp;&nbsp;&nbsp;&nbsp;进口水果</option><option value="20" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;奇异果猕猴桃</option><option value="17" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;芒果桃李</option><option value="21" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;凤梨蓝莓</option><option value="18" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;龙眼荔枝</option><option value="15" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;释迦芭乐</option><option value="22" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;榴莲山竹</option><option value="19" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;提子葡萄</option><option value="16" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;樱桃车厘子</option><option value="13" >&nbsp;&nbsp;&nbsp;&nbsp;糖果巧克力</option><option value="24" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;巧克力</option><option value="28" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;奶糖</option><option value="25" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;口香糖</option><option value="29" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;QQ糖</option><option value="26" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;棒棒糖</option><option value="30" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;果冻</option><option value="27" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;软糖</option><option value="9" >&nbsp;&nbsp;&nbsp;&nbsp;牛奶乳品</option><option value="37" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;全脂奶</option><option value="34" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;酸奶</option><option value="31" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;常温奶</option><option value="38" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;成人奶粉</option><option value="35" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;豆奶</option><option value="32" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;乳饮料</option><option value="36" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;低脂奶</option><option value="33" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;儿童奶</option><option value="10" >&nbsp;&nbsp;&nbsp;&nbsp;坚果炒货</option><option value="40" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;夏威夷果</option><option value="44" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;瓜子</option><option value="41" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;碧根果</option><option value="45" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;花生</option><option value="42" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;开心果</option><option value="39" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;核桃</option><option value="46" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;杏仁</option><option value="43" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;腰果</option><option value="12" >&nbsp;&nbsp;&nbsp;&nbsp;蜜饯果干</option><option value="53" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;橄榄</option><option value="50" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;芒果干</option><option value="47" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;红枣</option><option value="54" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;其他</option><option value="51" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;香蕉干</option><option value="48" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;莓类</option><option value="52" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;山楂片</option><option value="49" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;葡萄干</option><option value="362" >营养保健</option><option value="363" >图书、音像、电子书</option><option value="364" >彩票、旅行、充值、票务</option><option value="365" >理财、众筹、白条、保险</option></select>
      <!-- 品牌 -->
        <!--
      <select name="brand_id"><option value="0">所有品牌</option><option value="54">缪诗</option><option value="68">格力</option><option value="69">老板</option><option value="70">西门子</option><option value="71">格兰仕</option><option value="72">海信</option><option value="73">伊莱克斯</option><option value="74">艾力斯特</option><option value="75">博洋家纺</option><option value="76">富安娜</option><option value="77">爱仕达</option><option value="78">罗莱</option><option value="67">美的</option><option value="66">海尔</option><option value="55">卓诗尼</option><option value="56">七匹狼</option><option value="57">佐丹奴</option><option value="58">达芙妮</option><option value="59">她他/tata</option><option value="60">曼妮芬（ManniForm）</option><option value="61">伊芙丽</option><option value="62">稻草人</option><option value="63">斯提亚</option><option value="64">袋鼠</option><option value="65">爱华仕</option><option value="79">安睡宝</option><option value="80">溢彩年华</option><option value="94">王老吉</option><option value="95">可口可乐</option><option value="96">贝古贝古</option><option value="97">皇家宝贝</option><option value="98">呵宝童车</option><option value="99">合生元</option><option value="100">美赞臣</option><option value="101">帮宝适</option><option value="102">抱抱熊</option><option value="103">巴拉巴拉</option><option value="104">青蛙王子</option><option value="93">统一</option><option value="92">加多宝</option><option value="81">慧乐家</option><option value="82">天堂伞</option><option value="83">水星家纺</option><option value="84">全有家居</option><option value="85">五粮液</option><option value="86">泸州老窖</option><option value="87">洋河</option><option value="88">郎酒</option><option value="89">锐澳</option><option value="90">雪花</option><option value="91">哈尔滨</option><option value="105">雀氏</option><option value="1">资生堂</option><option value="15">韩束</option><option value="16">卡姿兰</option><option value="17">珀莱雅</option><option value="18">兰芝</option><option value="19">碧欧泉</option><option value="20">小米</option><option value="21">摩托罗拉</option><option value="22">中兴</option><option value="23">朵唯</option><option value="24">htc</option><option value="25">华为</option><option value="14">高丝</option><option value="13">SK-ll</option><option value="2">CK</option><option value="3">Disney</option><option value="4">雅诗兰黛</option><option value="5">相宜本草</option><option value="6">Dior</option><option value="7">爱丽</option><option value="8">雅顿</option><option value="9">狮王</option><option value="10">高丝洁</option><option value="11">MISS FACE</option><option value="12">姬芮</option><option value="26">oppo</option><option value="27">金立</option><option value="42">君乐宝</option><option value="43">光明</option><option value="44">三元</option><option value="45">百草味</option><option value="46">三只松鼠</option><option value="47">口水娃</option><option value="48">楼兰密语</option><option value="49">西域美农</option><option value="50">糖糖屋</option><option value="51">享爱.</option><option value="52">猫人</option><option value="40">蒙牛</option><option value="39">海底捞</option><option value="28">LG</option><option value="29">苹果</option><option value="30">三星</option><option value="31">乐檬</option><option value="32">努比亚</option><option value="41">伊利</option><option value="34">肯德基</option><option value="35">麦当劳</option><option value="36">小肥羊</option><option value="37">小尾羊</option><option value="38">必胜客</option><option value="53">茵曼（INMAN）</option></select>
      <!-- 关键字 -->
        <!--
      关键词<input type="text" name="keyword"/>
      <!-- 搜索 -->
        <!--
      <input type="submit" name="search_submit" id="search_submit" value="搜索" class="button" />
      -->
    </form>
  </div>

  <div style='padding:10px 20px; color:#FF0000; font-weight:bold'>
	特别注意：提货券商品为标准商品，该商品应为专为提货券增加的新商品或者其他无多属性的商品，请勿为提货券匹配多属性商品。
  </div>
  
  <form action="takegoods.php?act=addaddceshi" method="post" name="theForm"  onsubmit="return formValidate0()">
  <table>
  <tr>
    <td width="46%" align=center>可选商品</td>
	<td width="8%" align=center>操作</td>
	<td width="46%" align=center>配置商品</td>
</tr>
  <tr>
    <td width="46%">
	<select name="src_goods_lists" id="src_goods_lists" size="14" style="width:100%" multiple="true"></select>
	</td>
    <td  width="8%" style="text-align:center;">
      <p><input type="button" value=">>" id="addAllGoods" class="button" /></p>
      <p><input type="button" value=">" id="addGoods" class="button" /></p>
      <p><input type="button" value="<" id="delGoods" class="button" /></p>
      <p><input type="button" value="<<" id="delAllGoods" class="button" /></p>
    </td>
    <td width="46%">
	<select name="dst_goods_lists" id="dst_goods_lists" size="14" style="width:100%" multiple="true">
	    <option value="26">韩国进口X-5花生夹心巧克力棒盒装（24根）864g(¥0.00)</option>
        <option value="28">进口费列罗巧克力礼盒DIY心形27粒【顺丰包邮】【代写贺卡】七夕礼物生日创意礼品(¥120.00)</option>
        <option value="27">Ferrero/费列罗 意大利进口威化榛果巧克力30粒礼盒装 生日礼物送女友情人节(¥79.00)</option>
        <option value="29">意大利费列罗巧克力食品进口零食礼盒576粒整箱装结婚喜糖(¥380.00)</option>
        <option value="30">日本进口 KRACIE（KRACIE）牌玫瑰香味糖果32g(¥12.00)</option>
        <option value="31">台湾进口 百年老店糖之坊夏威夷果牛轧糖奶糖（蔓越莓味）120克(¥35.00)</option>
        <option value="32">Lindt瑞士莲黑巧克力特醇排装德国进口 70%可可黑巧克力10块组合 特惠分享装(¥299.00)</option>
        <option value="50">坚果炒货零食特产扁桃仁235gx2袋(¥52.90)</option>
        <option value="51">高端2015夏装新款修身淑坊女女装蕾丝短袖复女连衣裙装(¥199.00)</option>
        <option value="52">昆仑玉红枣 和田香枣二级500g 新疆特产 免洗零食 和田大枣子(¥0.00)</option>
        <option value="53">南非进口西柚8个约300-330g/个红心柚子新鲜红宝石柚子(¥39.00)</option>
        <option value="54">墨西哥牛油果9个 牛油果 鳄梨 牛油果水果 21-24号发(¥80.00)</option>
        <option value="55">嘉云糖 300g玻璃罐装 水果硬糖 喜糖 德国进口(¥38.00)</option>
        <option value="56">2015新品真丝睡衣女款可爱娃娃衫家居服春秋桑蚕丝睡裙(¥199.00)</option>
        <option value="60">德国 进口牛奶 欧德堡（Oldenburger）超高温处理全脂纯牛奶1L*12(¥119.00)</option>
        <option value="62">澳大利亚 进口牛奶 德运（Devondale） 全脂牛奶 1L*10 整箱装(¥99.00)</option>
        <option value="63">真丝睡衣女士夏季桑蚕丝绸春秋家居服性感吊带睡裙(¥199.00)</option>
        <option value="67">一米画纱桑蚕丝吊带睡裙女夏季纯色真丝睡衣 (¥199.00)</option>
        <option value="68">新西兰 原装进口 纯牛奶 纽麦福（ Meadow fresh ）全脂1L*12盒/箱(¥109.00)</option>
        <option value="69">夏季性感吊带真丝睡裙100%桑蚕丝深V睡衣女士纯色蕾丝家居服女(¥199.00)</option>
        <option value="70">纱桑蚕丝真丝睡衣女士夏季纯色吊带睡裙(¥199.00)</option>
        <option value="71">真丝睡衣女士夏季修身性感吊带真丝睡衣裙桑蚕丝睡衣(¥199.00)</option>
        <option value="72">波兰 进口牛奶 日昇（MLEKOVITA）全脂牛奶1L*12盒(¥109.00)</option>
        <option value="74">德运Devondale 脱脂高钙奶粉1kg(¥79.90)</option>
        <option value="75">宾格瑞（BINGGRAE）香蕉味牛奶饮料 200ml*24(¥129.00)</option>
        <option value="78">100%桑蚕丝两件套夏季短袖真丝睡衣女套装(¥199.00)</option>
        <option value="79">蒙牛 特仑苏 纯牛奶 250ml*12 礼盒装(¥55.00)</option>
        <option value="81">伊利 味可滋（巧克力）奶昔乳饮品240ml*12盒(¥45.00)</option>
        <option value="82">大码薄款女士真丝睡衣女夏季丝绸性感100%桑蚕丝睡裙(¥199.00)</option>
        <option value="85">美的空调扇单冷遥控制冷风扇冷风机家用净化静音冷气空调AC120-G(¥500.00)</option>
        <option value="86">海尔HGS-2164手持蒸汽挂烫机家用挂式电熨斗熨烫机正品全国联保(¥500.00)</option>
        <option value="88">家用静音办公室空调加湿器 香薰迷你大容量净化特价(¥100.00)</option>
        <option value="89">格力遥控落地扇 家用静音电风扇 7.5小时定时 办公室学生扇 电扇(¥199.00)</option>
        <option value="90">2015新款男装 男士夏季多彩短袖polo衫男 休闲双层立领T恤 (¥99.00)</option>
        <option value="91">海尔ZB401G 家用小型床铺除螨仪 床上除螨机吸尘器 紫外线杀菌(¥399.00)</option>
        <option value="92">人气5折杰克琼斯夏纯棉条纹撞色POLO衫短袖T恤(¥299.00)</option>
        <option value="93">家用高端直饮净水器WP4160厨房自来水前置过滤器智能净水机(¥599.00)</option>
        <option value="94">电熨斗蒸汽家用 手持迷你电烫斗顺滑不粘底板 蒸汽熨斗家用(¥99.00)</option>
        <option value="95">与狼共舞短袖T恤 2015夏装新款 男士Polo衫 男装纯棉翻领(¥199.00)</option>
        <option value="96">2015夏季新品 与狼共舞短袖T恤 男士纯棉翻领多彩polo衫潮(¥149.00)</option>
        <option value="97">除湿机家用抽湿机20B 地下室别墅吸湿器除湿器(¥259.00)</option>
        <option value="99">纯棉牛仔拼接修身男Polo衫(¥149.00)</option>
        <option value="100">苹果（Apple）iPhone 6 (A1586) 16GB 金色 移动联通电信4G手机(¥4888.00)</option>
        <option value="101">美的电磁炉Midea/美的 WK2102电磁炉特价家用触摸屏火锅电池炉灶(¥159.00)</option>
        <option value="102">Polo衫男短袖 2015夏装男装短袖t恤 男士拼接撞色修身韩版潮(¥149.00)</option>
        <option value="103">Galanz/格兰仕 G90F25CN3L-C2(G2) 微波炉 光波炉 正品(¥359.00)</option>
        <option value="105">法曼斯2015夏装新款长绒棉短袖t恤商务男士休闲天丝翻领纯色上衣(¥149.00)</option>
        <option value="106"> JYL-D022料理机家用多功能榨汁辅食搅拌机电动绞肉(¥236.00)</option>
        <option value="107">纯棉弹力修身男装商务短袖(¥149.00)</option>
        <option value="110">养生壶玻璃加厚分体保温电煎药壶全自动花茶壶隔水炖正品(¥336.00)</option>
    	</select>
    <input type="hidden" name="goods_ids" value="" />
	<input type="hidden" name="tg_ids" value="" />
	<input type="hidden" name="type_id" value="3" />
	<input type="hidden" name="add_type" value="">
     </td>
  </tr>
  <tr>
  <td align=center colspan=3>
  <input type="hidden" name="act" value="add_goods_update">
  <input type="submit" class="button" value="提交">
  </td></tr>
  </table>
  </form>

  </div>
  </td>
</tr>


</table>



<script type="text/javascript" src="../js/utils.js"></script><script type="text/javascript" src="js/validator.js"></script><script language="JavaScript">
var express_not_null = "";
var ems_not_null = "";
var custom_goods_field_not_null = "";



    var elements;
    onload = function()
    {
        // 开始检查订单
        startCheckOrder();
    }

   
   function formValidate0()
    {
        var src_obj = document.forms['searchForm'];
        var dst_obj = document.forms['theForm'];
        copy_search_result(src_obj, dst_obj);
        return true;
    }

	 function copy_search_result(src_obj, dst_obj)
    {
        var goods_lists = Utils.$('dst_goods_lists');
        for (var i=0,l=goods_lists.options.length; i<l; i++)
        {
            var separator = (i==0)?'':',';
            dst_obj.goods_ids.value += separator + goods_lists.options[i].value;
        }
    }


    /**
     * 绑定商品类型控件事件
     */
    if(Utils.$('goods_type'))
    {
        Utils.$('goods_type').onchange = function ()
        {
            Ajax.call('goods_export.php?is_ajax=1&act=get_goods_fields&cat_id='+this.value, '' , goodsFieldsResponse , 'POST', 'JSON');
        }
    }

    function goodsFieldsResponse (result)
    {
        if (result.error > 0)
        {
            alert(result.message);
            return;
        }
        Utils.$('src_goods_fields').innerHTML = '';
        for (var key in result.content)
        {
            if (typeof (result.content[key]) == 'string')
            {
                var new_opt = document.createElement('OPTION');
                new_opt.value = key;
                new_opt.innerHTML = result.content[key];
                Utils.$('src_goods_fields').appendChild(new_opt);
            }
        }
    }
    /* 搜索商品列表 */
    function queryGoods(obj)
    {
        var filters = new Object;
        filters.cat_id = obj.cat_id.value;
        filters.brand_id = obj.brand_id.value;
        filters.keyword = obj.keyword.value;

        Ajax.call('takegoods.php?is_ajax=1&act=get_goods_list', filters, queryGoodsResponse , 'POST', 'JSON');
        return false;
    }

    function queryGoodsResponse (result)
    {
        if (result.error > 0)
        {
            alert(result.message);
            return;
        }
        Utils.$('src_goods_lists').innerHTML = '';
        for (var i=0,l=result.content.length;i<l;++i)
        {
            var new_opt = document.createElement('OPTION');
            new_opt.value = result.content[i].goods_id;
            new_opt.innerHTML = result.content[i].goods_name;
            Utils.$('src_goods_lists').appendChild(new_opt);
        }
    }

    /* 操作自定义导出商品的Select Box */
    var MySelectBox;
    var MySelectBox2;
    if (!MySelectBox)
    {
        var global = $import("../js/global.js","js");
        global.onload = global.onreadystatechange= function()
        {
            if(this.readyState && this.readyState=="loading")return;
            var selectbox = $import("js/selectbox.js","js");
            selectbox.onload = selectbox.onreadystatechange = function()
            {
                if(this.readyState && this.readyState=="loading")return;
                MySelectBox = new SelectBox('src_goods_fields', 'dst_goods_fields');
                MySelectBox2 = new SelectBox('src_goods_lists', 'dst_goods_lists');
            }
        }
    }
    if (Utils.$('addItem'))
    {
        Utils.$('addItem').onclick = function ()
        {
            MySelectBox.addItem();
        }
    }
    if (Utils.$('delItem'))
    {
        Utils.$('delItem').onclick = function ()
        {
            MySelectBox.delItem();
        }
    }
    if (Utils.$('addAllItem'))
    {
        Utils.$('addAllItem').onclick = function ()
        {
            MySelectBox.addItem(true);
        }
    }
    if (Utils.$('delAllItem'))
    {
        Utils.$('delAllItem').onclick = function ()
        {
            MySelectBox.delItem(true);
        }
    }
    if (Utils.$('src_goods_fields'))
    {
        Utils.$('src_goods_fields').ondblclick = function ()
        {
            MySelectBox.addItem();
        }
    }
    if (Utils.$('dst_goods_fields'))
    {
        Utils.$('dst_goods_fields').ondblclick = function ()
        {
            MySelectBox.delItem();
        }
    }
    if (Utils.$('mvUp'))
    {
        Utils.$('mvUp').onclick = function ()
        {
            MySelectBox.moveItem('up');
        }
    }
    if (Utils.$('mvDown'))
    {
        Utils.$('mvDown').onclick = function ()
        {
            MySelectBox.moveItem('down');
        }
    }

    if (Utils.$('addGoods'))
    {
        Utils.$('addGoods').onclick = function ()
        {
            MySelectBox2.addItem();
        }
    }
    if (Utils.$('delGoods'))
    {
        Utils.$('delGoods').onclick = function ()
        {
            MySelectBox2.delItem();
        }
    }
    if (Utils.$('addAllGoods'))
    {
        Utils.$('addAllGoods').onclick = function ()
        {
            MySelectBox2.addItem(true);
        }
    }
    if (Utils.$('delAllGoods'))
    {
        Utils.$('delAllGoods').onclick = function ()
        {
            MySelectBox2.delItem(true);
        }
    }
    if (Utils.$('src_goods_lists'))
    {
        Utils.$('src_goods_lists').ondblclick = function ()
        {
            MySelectBox2.addItem();
        }
    }
    if (Utils.$('dst_goods_lists'))
    {
        Utils.$('dst_goods_lists').ondblclick = function ()
        {
            MySelectBox2.delItem();
        }
    } 

  

   
</script>

<script type="text/javascript">
$(function(){
	$('.chzn-container-single:first').css('width','86px');	
})
</script>
<?php
include 'buy.php' ;
?>
