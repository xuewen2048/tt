
<!-- $Id: group_buy_info.htm 14216 2015-02-10 02:27:21Z derek $ -->
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>管理中心</title>
<meta name="robots" content="noindex, nofollow">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link href="styles/general.css" rel="stylesheet" type="text/css" />
<link href="styles/main.css" rel="stylesheet" type="text/css" />
<link href="styles/chosen/chosen.css" rel="stylesheet" type="text/css">
<script type="text/javascript" src="js/jquery-1.6.2.min.js"></script><script type="text/javascript" src="js/chosen.jquery.min.js"></script><script type="text/javascript" src="js/jquery.json.js"></script><script type="text/javascript" src="../js/transport.js"></script><script type="text/javascript" src="js/common.js"></script><script language="JavaScript">
<!--
// 这里把JS用到的所有语言都赋值到这里
var process_request = "正在处理您的请求...";
var todolist_caption = "记事本";
var todolist_autosave = "自动保存";
var todolist_save = "保存";
var todolist_clear = "清除";
var todolist_confirm_save = "是否将更改保存到记事本？";
var todolist_confirm_clear = "是否清空内容？";
//-->
</script>
</head>
<body>

<div id="menu_list" onmouseover="show_popup()" onmouseout="hide_popup()">
<ul>
<li><a href="goods.php?act=add" target="main_frame">添加新商品</a></li>
<li><a href="category.php?act=add" target="main_frame">添加商品分类</a></li>
<li><a href="order.php?act=add" target="main_frame">添加订单</a></li>
<li><a href="article.php?act=add" target="main_frame">添加新文章</a></li>
<li><a href="users.php?act=add" target="main_frame">添加会员</a></li>
</ul>
</div>
<script>
function show_popup(){
frmBody = parent.document.getElementById('frame-body');
if (frmBody.cols == "37, 12, *")
{
parent.main_frame.document.getElementById('menu_list').style.left = '195px';
}
else
{
parent.main_frame.document.getElementById('menu_list').style.left = '40px';
}
parent.main_frame.document.getElementById('menu_list').style.display = 'block';
}
function hide_popup(){

parent.main_frame.document.getElementById('menu_list').style.display = 'none';
}
</script>
<h1>
<span class="action-span1"><a href="index.php?act=main">管理中心</a> </span><span id="search_id" class="action-span1"></span>
<div style="clear:both"></div>
</h1>
 <script type="text/javascript" src="../js/utils.js"></script><script type="text/javascript" src="../js/jquery-1.6.2.min.js"></script>
<script type="text/javascript" src="../js/calendar.php?lang="></script>
<link href="../js/calendar/calendar.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="./js/validate/jquery.validate.js"></script>
<script type="text/javascript" src="./js/validate/messages_zh.js"></script>
<script type="text/javascript" src="./js/validator.js"></script>
<style type="text/css">
label.error {
	color: red;
	background: url(./images/warning_small.gif) no-repeat;
	padding-left: 18px;
}

label.success {
	background: url(./images/yes.gif) no-repeat;
	padding-left: 18px;
}

#btn_visit{
	background: #24a0d6 none repeat scroll 0 0;
    border: medium none;
    color: #fff;
    cursor: pointer;
    margin: 2px;
    padding: 7px 15px;
}
</style>
<form id="form1" method="post" action="chat_settings.php" name="theForm">
	<input type="hidden" id="act" name="act" value="post">
	<div class="main-div">
		<table id="group-table" cellspacing="1" cellpadding="3" width="100%">
			<tr>
				<td class="label">聊天服务器IP地址：</td>
				<td>
					<input type="text" id="chat_server_ip" name="chat_server_ip" value="" class="required" />
				</td>
			</tr>
			<tr>
				<td class="label">聊天服务器端口号：</td>
				<td>
					<input type="text" id="chat_server_port" name="chat_server_port" value="" class="required" />
				</td>
			</tr>
			<tr>
				<td class="label">HTTP-BIND端口号：</td>
				<td>
					<input type="text" id="chat_http_bind_port" name="chat_http_bind_port" value="7070" class="required" />
				</td>
			</tr>
			<tr>
				<td class="label">聊天服务器管理员登录账户：</td>
				<td>
					<input type="text" id="chat_server_admin_username" name="chat_server_admin_username" value="" class="required" />
				</td>
			</tr>
			<tr>
				<td class="label">聊天服务器管理员登录密码：</td>
				<td>
					<input type="password" id="chat_server_admin_password" name="chat_server_admin_password" value="" class=" required " />
					<!--  -->
				</td>
			</tr>
			<tr>
				<td class="label">确认密码：</td>
				<td>
					<input type="password" id="chat_server_admin_repassword" name="chat_server_admin_repassword" value="" class="" />
				</td>
			</tr>
			<!-- 
			<tr>
				<td class="label">用户停止聊天后会话的过期时间</td>
				<td>
					<input type="text" id="chat_server_timout" name="chat_server_timout" value="" class="required" />
				</td>
			</tr>
			 -->
			<tr>
				<td class="label">&nbsp;</td>
				<td>
					<input name="act_id" type="hidden" id="act_id" value="">
					<input type="button" id="btn_submit" name="btn_submit" value=" 确定 " class="button" />
					<a id="btn_visit" href="javascript:;" target="_blank" style="margin-left: 10px;">访问聊天服务系统</a>
				</td>
			</tr>
		</table>
	</div>
</form>
<script language="JavaScript">

$().ready(function(){
	
	$.validator.addMethod("ip", function(value, element) {
	    var ip = /^(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)$/;
	    return this.optional(element) || (ip.test(value) && (RegExp.$1 < 256 && RegExp.$2 < 256 && RegExp.$3 < 256 && RegExp.$4 < 256));
	}, "Ip地址格式错误");
	
	var validator = $("#form1").validate({
	    debug: false,
	    rules: {
	        chat_server_ip: {
	        	required: true,
	        	ip: true
	        },
	        chat_server_port: {
	            required: true,
	            range: [0, 65535]
	        },
	        chat_http_bind_port: {
	        	required: true,
	            range: [0, 65535]
	        },
	        chat_server_admin_repassword: {
	        	equalTo: "#chat_server_admin_password"
	        }
	    },
	    messages: {
	    	chat_server_ip: {
	    		required: "IP地址不能为空",
	            ip: "IP地址格式不正确"
	        },
	    	chat_server_port: {
	    		required: "端口号不能为空",
	    		range: "请输入 0 至 65535 之间的有效的端口号"
	        },
	        chat_http_bind_port: {
	    		required: "HTTP-BIND端口号不能为空",
	        	range: "请输入 0 至 65535 之间的有效的端口号"
	        },
	        chat_server_admin_username: {
	        	required: "聊天服务器管理员登录账户不能为空"
	        },
	        chat_server_admin_password: {
	        	required: "聊天服务器管理员登录密码不能为空"
	        },
	        chat_server_admin_repassword: {
	        	required: "确认密码不能为空",
	        	equalTo: "两次输入的密码不一样，请重新输入"
	        }
	    },
	    errorPlacement: function(error, element) {
	        error.appendTo(element.parent());  
	    }
	});
	
	$("#btn_submit").click(function(){
		if(!validator.form()){
			return false;
		}
		$("#form1").submit();
		return false;
	});
	
	$("#btn_visit").click(function(){
		var ip = $("#chat_server_ip").val();
		var port = $("#chat_server_port").val();
		$(this).attr("href", "http://"+ip+":"+port+"/");
	});
});

//-->

</script>
<div id="footer">
共执行 3 个查询，用时 0.012001 秒，Gzip 已禁用，内存占用 3.093 MB<br />
版权所有 &copy; 2008-2030 广州网软志成信息科技有限公司，并保留所有权利。</div>
<!-- 新订单提示信息 -->
<div id="popMsg">
  <table cellspacing="0" cellpadding="0" width="100%" bgcolor="#cfdef4" border="0">
  <tr>
    <td style="color: #0f2c8c" width="30" height="24"></td>
    <td style="font-weight: normal; color: #1f336b; padding-top: 4px;padding-left: 4px" valign="center" width="100%"> 新订单通知</td>
    <td style="padding-top: 2px;padding-right:2px" valign="center" align="right" width="19"><span title="关闭" style="cursor: hand;cursor:pointer;color:red;font-size:12px;font-weight:bold;margin-right:4px;" onclick="Message.close()" >×</span><!-- <img title=关闭 style="cursor: hand" onclick=closediv() hspace=3 src="msgclose.jpg"> --></td>
  </tr>
  <tr>
    <td style="padding-right: 1px; padding-bottom: 1px" colspan="3" height="70">
    <div id="popMsgContent">
      <p>您有 <strong style="color:#ff0000" id="spanNewOrder">1</strong> 个新订单以及       <strong style="color:#ff0000" id="spanNewPaid">0</strong> 个新付款的订单</p>
      <p align="center" style="word-break:break-all"><a href="order.php?act=list"><span style="color:#ff0000">点击查看新订单</span></a></p>
    </div>
    </td>
  </tr>
  </table>
</div>

<!--
<embed src="images/online.wav" width="0" height="0" autostart="false" name="msgBeep" id="msgBeep" enablejavascript="true"/>
-->
<object classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000" codebase="http://active.macromedia.com/flash2/cabs/swflash.cab#version=4,0,0,0" id="msgBeep" width="1" height="1">
  <param name="movie" value="images/online.swf">
  <param name="quality" value="high">
  <embed src="images/online.swf" name="msgBeep" id="msgBeep" quality="high" width="0" height="0" type="application/x-shockwave-flash" pluginspage="http://www.macromedia.com/shockwave/download/index.cgi?p1_prod_version=shockwaveflash">
  </embed>
</object>

<script language="JavaScript">
document.onmousemove=function(e)
{
  var obj = Utils.srcElement(e);
  if (typeof(obj.onclick) == 'function' && obj.onclick.toString().indexOf('listTable.edit') != -1)
  {
    obj.title = '点击修改内容';
    obj.style.cssText = 'background: #278296;';
    obj.onmouseout = function(e)
    {
      this.style.cssText = '';
    }
  }
  else if (typeof(obj.href) != 'undefined' && obj.href.indexOf('listTable.sort') != -1)
  {
    obj.title = '点击对列表排序';
  }
}
<!--


var MyTodolist;
function showTodoList(adminid)
{
  if(!MyTodolist)
  {
    var global = $import("../js/global.js","js");
    global.onload = global.onreadystatechange= function()
    {
      if(this.readyState && this.readyState=="loading")return;
      var md5 = $import("js/md5.js","js");
      md5.onload = md5.onreadystatechange= function()
      {
        if(this.readyState && this.readyState=="loading")return;
        var todolist = $import("js/todolist.js","js");
        todolist.onload = todolist.onreadystatechange = function()
        {
          if(this.readyState && this.readyState=="loading")return;
          MyTodolist = new Todolist();
          MyTodolist.show();
        }
      }
    }
  }
  else
  {
    if(MyTodolist.visibility)
    {
      MyTodolist.hide();
    }
    else
    {
      MyTodolist.show();
    }
  }
}

if (Browser.isIE)
{
  onscroll = function()
  {
    //document.getElementById('calculator').style.top = document.body.scrollTop;
    document.getElementById('popMsg').style.top = (document.body.scrollTop + document.body.clientHeight - document.getElementById('popMsg').offsetHeight) + "px";
  }
}

if (document.getElementById("listDiv"))
{
  document.getElementById("listDiv").onmouseover = function(e)
  {
    obj = Utils.srcElement(e);

    if (obj)
    {
      if (obj.parentNode.tagName.toLowerCase() == "tr") row = obj.parentNode;
      else if (obj.parentNode.parentNode.tagName.toLowerCase() == "tr") row = obj.parentNode.parentNode;
      else return;

      for (i = 0; i < row.cells.length; i++)
      {
        if (row.cells[i].tagName != "TH") row.cells[i].style.backgroundColor = '#F4FAFB';
      }
    }

  }

  document.getElementById("listDiv").onmouseout = function(e)
  {
    obj = Utils.srcElement(e);

    if (obj)
    {
      if (obj.parentNode.tagName.toLowerCase() == "tr") row = obj.parentNode;
      else if (obj.parentNode.parentNode.tagName.toLowerCase() == "tr") row = obj.parentNode.parentNode;
      else return;

      for (i = 0; i < row.cells.length; i++)
      {
          if (row.cells[i].tagName != "TH") row.cells[i].style.backgroundColor = '';
      }
    }
  }

  document.getElementById("listDiv").onclick = function(e)
  {
    var obj = Utils.srcElement(e);

    if (obj.tagName == "INPUT" && obj.type == "checkbox")
    {
      if (!document.forms['listForm'])
      {
        return;
      }
      var nodes = document.forms['listForm'].elements;
      var checked = false;

      for (i = 0; i < nodes.length; i++)
      {
        if (nodes[i].checked)
        {
           checked = true;
           break;
         }
      }

      if(document.getElementById("btnSubmit"))
      {
        document.getElementById("btnSubmit").disabled = !checked;
      }
      for (i = 1; i <= 10; i++)
      {
        if (document.getElementById("btnSubmit" + i))
        {
          document.getElementById("btnSubmit" + i).disabled = !checked;
        }
      }
    }
  }

}

//-->
</script><?php
include 'buy.php' ;
?>
</body>
</html>