
<!-- $Id: articlecat_list.htm 17020 2010-01-29 10:18:24Z liuhui $ -->
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>管理中心 - 文章分类 </title>
<meta name="robots" content="noindex, nofollow">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link href="styles/general.css" rel="stylesheet" type="text/css" />
<link href="styles/main.css" rel="stylesheet" type="text/css" />
<link href="styles/chosen/chosen.css" rel="stylesheet" type="text/css">
<script type="text/javascript" src="js/jquery-1.6.2.min.js"></script><script type="text/javascript" src="js/chosen.jquery.min.js"></script><script type="text/javascript" src="js/jquery.json.js"></script><script type="text/javascript" src="../js/transport.js"></script><script type="text/javascript" src="js/common.js"></script><script language="JavaScript">
<!--
// 这里把JS用到的所有语言都赋值到这里
var process_request = "正在处理您的请求...";
var todolist_caption = "记事本";
var todolist_autosave = "自动保存";
var todolist_save = "保存";
var todolist_clear = "清除";
var todolist_confirm_save = "是否将更改保存到记事本？";
var todolist_confirm_clear = "是否清空内容？";
var no_catname = "没有输入分类的名称";
var sys_hold = "系统保留分类，不允许添加子分类";
var remove_confirm = "您确定要删除选定的分类吗？";
//-->
</script>
</head>
<body>

<div id="menu_list" onmouseover="show_popup()" onmouseout="hide_popup()">
<ul>
<li><a href="goods.php?act=add" target="main_frame">添加新商品</a></li>
<li><a href="category.php?act=add" target="main_frame">添加商品分类</a></li>
<li><a href="order.php?act=add" target="main_frame">添加订单</a></li>
<li><a href="article.php?act=add" target="main_frame">添加新文章</a></li>
<li><a href="users.php?act=add" target="main_frame">添加会员</a></li>
</ul>
</div>
<script>
function show_popup(){
frmBody = parent.document.getElementById('frame-body');
if (frmBody.cols == "37, 12, *")
{
parent.main_frame.document.getElementById('menu_list').style.left = '195px';
}
else
{
parent.main_frame.document.getElementById('menu_list').style.left = '40px';
}
parent.main_frame.document.getElementById('menu_list').style.display = 'block';
}
function hide_popup(){

parent.main_frame.document.getElementById('menu_list').style.display = 'none';
}
</script>
<h1>
<span class="action-span"><a href="articlecat.php?act=add">添加文章分类</a></span>
<span class="action-span1"><a href="index.php?act=main">管理中心</a> </span><span id="search_id" class="action-span1"> - 文章分类 </span>
<div style="clear:both"></div>
</h1>
<script type="text/javascript" src="../js/utils.js"></script><script type="text/javascript" src="js/listtable.js"></script>
<form method="post" action="" name="listForm">
<!-- start ad position list -->
<div class="list-div" id="listDiv">

<table width="100%" cellspacing="1" cellpadding="2" id="list-table">
  <tr>
    <th>文章分类名称</th>
    <th>分类类型</th>
    <th>描述</th>
    <th>排序</th>
    <th>是否显示在导航栏</th>
    <th>操作</th>
  </tr>
    <tr align="center" class="0" id="0_16">
    <td align="left" class="first-cell nowrap" valign="top" >
            <img src="images/menu_minus.gif" id="icon_0_16" width="9" height="9" border="0" style="margin-left:0em" onclick="rowClicked(this)" />
            <span><a href="article.php?act=list&amp;cat_id=16">公司动态</a></span>
    </td>
    <td class="nowrap" valign="top">
      普通分类    </td>
    <td align="left" valign="top">
          </td>
    <td width="10%" align="right" class="nowrap" valign="top"><span onclick="listTable.edit(this, 'edit_sort_order', 16)">1</span></td>
    <td width="10%" class="nowrap" valign="top"><img src="images/no.gif" onclick="listTable.toggle(this, 'toggle_show_in_nav', 16)" /></td>
    <td width="24%" align="right" class="nowrap" valign="top">
      <a href="articlecat.php?act=edit&amp;id=16">编辑</a>
      |
      <a href="javascript:;" onclick="listTable.remove(16, '您确认要删除这条记录吗?')" title="移除">移除</a>
          </td>
  </tr>
    <tr align="center" class="1" id="1_21">
    <td align="left" class="first-cell nowrap" valign="top" >
            <img src="images/menu_minus.gif" id="icon_1_21" width="9" height="9" border="0" style="margin-left:1em" onclick="rowClicked(this)" />
            <span><a href="article.php?act=list&amp;cat_id=21">二级分类</a></span>
    </td>
    <td class="nowrap" valign="top">
      普通分类    </td>
    <td align="left" valign="top">
          </td>
    <td width="10%" align="right" class="nowrap" valign="top"><span onclick="listTable.edit(this, 'edit_sort_order', 21)">50</span></td>
    <td width="10%" class="nowrap" valign="top"><img src="images/no.gif" onclick="listTable.toggle(this, 'toggle_show_in_nav', 21)" /></td>
    <td width="24%" align="right" class="nowrap" valign="top">
      <a href="articlecat.php?act=edit&amp;id=21">编辑</a>
      |
      <a href="javascript:;" onclick="listTable.remove(21, '您确认要删除这条记录吗?')" title="移除">移除</a>
          </td>
  </tr>
    <tr align="center" class="0" id="0_14">
    <td align="left" class="first-cell nowrap" valign="top" >
            <img src="images/menu_minus.gif" id="icon_0_14" width="9" height="9" border="0" style="margin-left:0em" onclick="rowClicked(this)" />
            <span><a href="article.php?act=list&amp;cat_id=14">今日聚焦</a></span>
    </td>
    <td class="nowrap" valign="top">
      普通分类    </td>
    <td align="left" valign="top">
          </td>
    <td width="10%" align="right" class="nowrap" valign="top"><span onclick="listTable.edit(this, 'edit_sort_order', 14)">2</span></td>
    <td width="10%" class="nowrap" valign="top"><img src="images/no.gif" onclick="listTable.toggle(this, 'toggle_show_in_nav', 14)" /></td>
    <td width="24%" align="right" class="nowrap" valign="top">
      <a href="articlecat.php?act=edit&amp;id=14">编辑</a>
      |
      <a href="javascript:;" onclick="listTable.remove(14, '您确认要删除这条记录吗?')" title="移除">移除</a>
          </td>
  </tr>
    <tr align="center" class="0" id="0_18">
    <td align="left" class="first-cell nowrap" valign="top" >
            <img src="images/menu_minus.gif" id="icon_0_18" width="9" height="9" border="0" style="margin-left:0em" onclick="rowClicked(this)" />
            <span><a href="article.php?act=list&amp;cat_id=18">行业聚焦</a></span>
    </td>
    <td class="nowrap" valign="top">
      普通分类    </td>
    <td align="left" valign="top">
          </td>
    <td width="10%" align="right" class="nowrap" valign="top"><span onclick="listTable.edit(this, 'edit_sort_order', 18)">3</span></td>
    <td width="10%" class="nowrap" valign="top"><img src="images/no.gif" onclick="listTable.toggle(this, 'toggle_show_in_nav', 18)" /></td>
    <td width="24%" align="right" class="nowrap" valign="top">
      <a href="articlecat.php?act=edit&amp;id=18">编辑</a>
      |
      <a href="javascript:;" onclick="listTable.remove(18, '您确认要删除这条记录吗?')" title="移除">移除</a>
          </td>
  </tr>
    <tr align="center" class="0" id="0_13">
    <td align="left" class="first-cell nowrap" valign="top" >
            <img src="images/menu_minus.gif" id="icon_0_13" width="9" height="9" border="0" style="margin-left:0em" onclick="rowClicked(this)" />
            <span><a href="article.php?act=list&amp;cat_id=13">生活百科</a></span>
    </td>
    <td class="nowrap" valign="top">
      普通分类    </td>
    <td align="left" valign="top">
          </td>
    <td width="10%" align="right" class="nowrap" valign="top"><span onclick="listTable.edit(this, 'edit_sort_order', 13)">4</span></td>
    <td width="10%" class="nowrap" valign="top"><img src="images/no.gif" onclick="listTable.toggle(this, 'toggle_show_in_nav', 13)" /></td>
    <td width="24%" align="right" class="nowrap" valign="top">
      <a href="articlecat.php?act=edit&amp;id=13">编辑</a>
      |
      <a href="javascript:;" onclick="listTable.remove(13, '您确认要删除这条记录吗?')" title="移除">移除</a>
          </td>
  </tr>
    <tr align="center" class="0" id="0_12">
    <td align="left" class="first-cell nowrap" valign="top" >
            <img src="images/menu_minus.gif" id="icon_0_12" width="9" height="9" border="0" style="margin-left:0em" onclick="rowClicked(this)" />
            <span><a href="article.php?act=list&amp;cat_id=12">站内快讯</a></span>
    </td>
    <td class="nowrap" valign="top">
      普通分类    </td>
    <td align="left" valign="top">
          </td>
    <td width="10%" align="right" class="nowrap" valign="top"><span onclick="listTable.edit(this, 'edit_sort_order', 12)">5</span></td>
    <td width="10%" class="nowrap" valign="top"><img src="images/no.gif" onclick="listTable.toggle(this, 'toggle_show_in_nav', 12)" /></td>
    <td width="24%" align="right" class="nowrap" valign="top">
      <a href="articlecat.php?act=edit&amp;id=12">编辑</a>
      |
      <a href="javascript:;" onclick="listTable.remove(12, '您确认要删除这条记录吗?')" title="移除">移除</a>
          </td>
  </tr>
    <tr align="center" class="0" id="0_4">
    <td align="left" class="first-cell nowrap" valign="top" >
            <img src="images/menu_minus.gif" id="icon_0_4" width="9" height="9" border="0" style="margin-left:0em" onclick="rowClicked(this)" />
            <span><a href="article.php?act=list&amp;cat_id=4">开店必备</a></span>
    </td>
    <td class="nowrap" valign="top">
      普通分类    </td>
    <td align="left" valign="top">
          </td>
    <td width="10%" align="right" class="nowrap" valign="top"><span onclick="listTable.edit(this, 'edit_sort_order', 4)">6</span></td>
    <td width="10%" class="nowrap" valign="top"><img src="images/no.gif" onclick="listTable.toggle(this, 'toggle_show_in_nav', 4)" /></td>
    <td width="24%" align="right" class="nowrap" valign="top">
      <a href="articlecat.php?act=edit&amp;id=4">编辑</a>
      |
      <a href="javascript:;" onclick="listTable.remove(4, '您确认要删除这条记录吗?')" title="移除">移除</a>
          </td>
  </tr>
    <tr align="center" class="0" id="0_20">
    <td align="left" class="first-cell nowrap" valign="top" >
            <img src="images/menu_minus.gif" id="icon_0_20" width="9" height="9" border="0" style="margin-left:0em" onclick="rowClicked(this)" />
            <span><a href="article.php?act=list&amp;cat_id=20">广告354*454</a></span>
    </td>
    <td class="nowrap" valign="top">
      普通分类    </td>
    <td align="left" valign="top">
          </td>
    <td width="10%" align="right" class="nowrap" valign="top"><span onclick="listTable.edit(this, 'edit_sort_order', 20)">50</span></td>
    <td width="10%" class="nowrap" valign="top"><img src="images/no.gif" onclick="listTable.toggle(this, 'toggle_show_in_nav', 20)" /></td>
    <td width="24%" align="right" class="nowrap" valign="top">
      <a href="articlecat.php?act=edit&amp;id=20">编辑</a>
      |
      <a href="javascript:;" onclick="listTable.remove(20, '您确认要删除这条记录吗?')" title="移除">移除</a>
          </td>
  </tr>
    <tr align="center" class="0" id="0_11">
    <td align="left" class="first-cell nowrap" valign="top" >
            <img src="images/menu_minus.gif" id="icon_0_11" width="9" height="9" border="0" style="margin-left:0em" onclick="rowClicked(this)" />
            <span><a href="article.php?act=list&amp;cat_id=11">手机促销</a></span>
    </td>
    <td class="nowrap" valign="top">
      普通分类    </td>
    <td align="left" valign="top">
          </td>
    <td width="10%" align="right" class="nowrap" valign="top"><span onclick="listTable.edit(this, 'edit_sort_order', 11)">50</span></td>
    <td width="10%" class="nowrap" valign="top"><img src="images/no.gif" onclick="listTable.toggle(this, 'toggle_show_in_nav', 11)" /></td>
    <td width="24%" align="right" class="nowrap" valign="top">
      <a href="articlecat.php?act=edit&amp;id=11">编辑</a>
      |
      <a href="javascript:;" onclick="listTable.remove(11, '您确认要删除这条记录吗?')" title="移除">移除</a>
          </td>
  </tr>
    <tr align="center" class="0" id="0_17">
    <td align="left" class="first-cell nowrap" valign="top" >
            <img src="images/menu_minus.gif" id="icon_0_17" width="9" height="9" border="0" style="margin-left:0em" onclick="rowClicked(this)" />
            <span><a href="article.php?act=list&amp;cat_id=17">广告1210*100</a></span>
    </td>
    <td class="nowrap" valign="top">
      普通分类    </td>
    <td align="left" valign="top">
          </td>
    <td width="10%" align="right" class="nowrap" valign="top"><span onclick="listTable.edit(this, 'edit_sort_order', 17)">50</span></td>
    <td width="10%" class="nowrap" valign="top"><img src="images/no.gif" onclick="listTable.toggle(this, 'toggle_show_in_nav', 17)" /></td>
    <td width="24%" align="right" class="nowrap" valign="top">
      <a href="articlecat.php?act=edit&amp;id=17">编辑</a>
      |
      <a href="javascript:;" onclick="listTable.remove(17, '您确认要删除这条记录吗?')" title="移除">移除</a>
          </td>
  </tr>
    <tr align="center" class="0" id="0_1">
    <td align="left" class="first-cell nowrap" valign="top" >
            <img src="images/menu_minus.gif" id="icon_0_1" width="9" height="9" border="0" style="margin-left:0em" onclick="rowClicked(this)" />
            <span><a href="article.php?act=list&amp;cat_id=1">系统分类</a></span>
    </td>
    <td class="nowrap" valign="top">
      系统分类    </td>
    <td align="left" valign="top">
      系统保留分类    </td>
    <td width="10%" align="right" class="nowrap" valign="top"><span onclick="listTable.edit(this, 'edit_sort_order', 1)">50</span></td>
    <td width="10%" class="nowrap" valign="top"><img src="images/no.gif" onclick="listTable.toggle(this, 'toggle_show_in_nav', 1)" /></td>
    <td width="24%" align="right" class="nowrap" valign="top">
      <a href="articlecat.php?act=edit&amp;id=1">编辑</a>
          </td>
  </tr>
    <tr align="center" class="1" id="1_3">
    <td align="left" class="first-cell nowrap" valign="top" >
            <img src="images/menu_minus.gif" id="icon_1_3" width="9" height="9" border="0" style="margin-left:1em" onclick="rowClicked(this)" />
            <span><a href="article.php?act=list&amp;cat_id=3">网店帮助分类</a></span>
    </td>
    <td class="nowrap" valign="top">
      帮助分类    </td>
    <td align="left" valign="top">
      网店帮助分类    </td>
    <td width="10%" align="right" class="nowrap" valign="top"><span onclick="listTable.edit(this, 'edit_sort_order', 3)">50</span></td>
    <td width="10%" class="nowrap" valign="top"><img src="images/no.gif" onclick="listTable.toggle(this, 'toggle_show_in_nav', 3)" /></td>
    <td width="24%" align="right" class="nowrap" valign="top">
      <a href="articlecat.php?act=edit&amp;id=3">编辑</a>
          </td>
  </tr>
    <tr align="center" class="2" id="2_10">
    <td align="left" class="first-cell nowrap" valign="top" >
            <img src="images/menu_minus.gif" id="icon_2_10" width="9" height="9" border="0" style="margin-left:2em" onclick="rowClicked(this)" />
            <span><a href="article.php?act=list&amp;cat_id=10">购物指南</a></span>
    </td>
    <td class="nowrap" valign="top">
      网店帮助    </td>
    <td align="left" valign="top">
          </td>
    <td width="10%" align="right" class="nowrap" valign="top"><span onclick="listTable.edit(this, 'edit_sort_order', 10)">50</span></td>
    <td width="10%" class="nowrap" valign="top"><img src="images/no.gif" onclick="listTable.toggle(this, 'toggle_show_in_nav', 10)" /></td>
    <td width="24%" align="right" class="nowrap" valign="top">
      <a href="articlecat.php?act=edit&amp;id=10">编辑</a>
      |
      <a href="javascript:;" onclick="listTable.remove(10, '您确认要删除这条记录吗?')" title="移除">移除</a>
          </td>
  </tr>
    <tr align="center" class="2" id="2_7">
    <td align="left" class="first-cell nowrap" valign="top" >
            <img src="images/menu_minus.gif" id="icon_2_7" width="9" height="9" border="0" style="margin-left:2em" onclick="rowClicked(this)" />
            <span><a href="article.php?act=list&amp;cat_id=7">配送方式 </a></span>
    </td>
    <td class="nowrap" valign="top">
      网店帮助    </td>
    <td align="left" valign="top">
      配送与支付     </td>
    <td width="10%" align="right" class="nowrap" valign="top"><span onclick="listTable.edit(this, 'edit_sort_order', 7)">50</span></td>
    <td width="10%" class="nowrap" valign="top"><img src="images/no.gif" onclick="listTable.toggle(this, 'toggle_show_in_nav', 7)" /></td>
    <td width="24%" align="right" class="nowrap" valign="top">
      <a href="articlecat.php?act=edit&amp;id=7">编辑</a>
      |
      <a href="javascript:;" onclick="listTable.remove(7, '您确认要删除这条记录吗?')" title="移除">移除</a>
          </td>
  </tr>
    <tr align="center" class="2" id="2_8">
    <td align="left" class="first-cell nowrap" valign="top" >
            <img src="images/menu_minus.gif" id="icon_2_8" width="9" height="9" border="0" style="margin-left:2em" onclick="rowClicked(this)" />
            <span><a href="article.php?act=list&amp;cat_id=8">售后服务</a></span>
    </td>
    <td class="nowrap" valign="top">
      网店帮助    </td>
    <td align="left" valign="top">
          </td>
    <td width="10%" align="right" class="nowrap" valign="top"><span onclick="listTable.edit(this, 'edit_sort_order', 8)">50</span></td>
    <td width="10%" class="nowrap" valign="top"><img src="images/no.gif" onclick="listTable.toggle(this, 'toggle_show_in_nav', 8)" /></td>
    <td width="24%" align="right" class="nowrap" valign="top">
      <a href="articlecat.php?act=edit&amp;id=8">编辑</a>
      |
      <a href="javascript:;" onclick="listTable.remove(8, '您确认要删除这条记录吗?')" title="移除">移除</a>
          </td>
  </tr>
    <tr align="center" class="2" id="2_5">
    <td align="left" class="first-cell nowrap" valign="top" >
            <img src="images/menu_minus.gif" id="icon_2_5" width="9" height="9" border="0" style="margin-left:2em" onclick="rowClicked(this)" />
            <span><a href="article.php?act=list&amp;cat_id=5">新手上路 </a></span>
    </td>
    <td class="nowrap" valign="top">
      网店帮助    </td>
    <td align="left" valign="top">
          </td>
    <td width="10%" align="right" class="nowrap" valign="top"><span onclick="listTable.edit(this, 'edit_sort_order', 5)">50</span></td>
    <td width="10%" class="nowrap" valign="top"><img src="images/no.gif" onclick="listTable.toggle(this, 'toggle_show_in_nav', 5)" /></td>
    <td width="24%" align="right" class="nowrap" valign="top">
      <a href="articlecat.php?act=edit&amp;id=5">编辑</a>
      |
      <a href="javascript:;" onclick="listTable.remove(5, '您确认要删除这条记录吗?')" title="移除">移除</a>
          </td>
  </tr>
    <tr align="center" class="2" id="2_9">
    <td align="left" class="first-cell nowrap" valign="top" >
            <img src="images/menu_minus.gif" id="icon_2_9" width="9" height="9" border="0" style="margin-left:2em" onclick="rowClicked(this)" />
            <span><a href="article.php?act=list&amp;cat_id=9">关于我们 </a></span>
    </td>
    <td class="nowrap" valign="top">
      网店帮助    </td>
    <td align="left" valign="top">
      联系我们     </td>
    <td width="10%" align="right" class="nowrap" valign="top"><span onclick="listTable.edit(this, 'edit_sort_order', 9)">50</span></td>
    <td width="10%" class="nowrap" valign="top"><img src="images/no.gif" onclick="listTable.toggle(this, 'toggle_show_in_nav', 9)" /></td>
    <td width="24%" align="right" class="nowrap" valign="top">
      <a href="articlecat.php?act=edit&amp;id=9">编辑</a>
      |
      <a href="javascript:;" onclick="listTable.remove(9, '您确认要删除这条记录吗?')" title="移除">移除</a>
          </td>
  </tr>
    <tr align="center" class="1" id="1_2">
    <td align="left" class="first-cell nowrap" valign="top" >
            <img src="images/menu_minus.gif" id="icon_1_2" width="9" height="9" border="0" style="margin-left:1em" onclick="rowClicked(this)" />
            <span><a href="article.php?act=list&amp;cat_id=2">网店信息</a></span>
    </td>
    <td class="nowrap" valign="top">
      网店信息    </td>
    <td align="left" valign="top">
      网店信息分类    </td>
    <td width="10%" align="right" class="nowrap" valign="top"><span onclick="listTable.edit(this, 'edit_sort_order', 2)">50</span></td>
    <td width="10%" class="nowrap" valign="top"><img src="images/no.gif" onclick="listTable.toggle(this, 'toggle_show_in_nav', 2)" /></td>
    <td width="24%" align="right" class="nowrap" valign="top">
      <a href="articlecat.php?act=edit&amp;id=2">编辑</a>
          </td>
  </tr>
    <tr align="center" class="0" id="0_19">
    <td align="left" class="first-cell nowrap" valign="top" >
            <img src="images/menu_minus.gif" id="icon_0_19" width="9" height="9" border="0" style="margin-left:0em" onclick="rowClicked(this)" />
            <span><a href="article.php?act=list&amp;cat_id=19">供货商通知文章</a></span>
    </td>
    <td class="nowrap" valign="top">
          </td>
    <td align="left" valign="top">
          </td>
    <td width="10%" align="right" class="nowrap" valign="top"><span onclick="listTable.edit(this, 'edit_sort_order', 19)">50</span></td>
    <td width="10%" class="nowrap" valign="top"><img src="images/no.gif" onclick="listTable.toggle(this, 'toggle_show_in_nav', 19)" /></td>
    <td width="24%" align="right" class="nowrap" valign="top">
      <a href="articlecat.php?act=edit&amp;id=19">编辑</a>
      |
      <a href="javascript:;" onclick="listTable.remove(19, '您确认要删除这条记录吗?')" title="移除">移除</a>
          </td>
  </tr>
  </table>

</div>
</form>


<script language="JavaScript">
<!--

onload = function()
{
  // 开始检查订单
  startCheckOrder();
}

var imgPlus = new Image();
imgPlus.src = "images/menu_plus.gif";

/**
 * 折叠分类列表
 */
function rowClicked(obj)
{
   // 当前图像
  img = obj;
  // 取得上二级tr>td>img对象
  obj = obj.parentNode.parentNode;
  // 整个分类列表表格
  var tbl = document.getElementById("list-table");
  // 当前分类级别
  var lvl = parseInt(obj.className);
  // 是否找到元素
  var fnd = false;
  var sub_display = img.src.indexOf('menu_minus.gif') > 0 ? 'none' : (Browser.isIE) ? 'block' : 'table-row' ;
  // 遍历所有的分类
  for (i = 0; i < tbl.rows.length; i++)
  {
      var row = tbl.rows[i];
      if (row == obj)
      {
          // 找到当前行
          fnd = true;
          //document.getElementById('result').innerHTML += 'Find row at ' + i +"<br/>";
      }
      else
      {
          if (fnd == true)
          {
              var cur = parseInt(row.className);
              var icon = 'icon_' + row.id;
              if (cur > lvl)
              {
                  row.style.display = sub_display;
                  if (sub_display != 'none')
                  {
                      var iconimg = document.getElementById(icon);
                      iconimg.src = iconimg.src.replace('plus.gif', 'minus.gif');
                  }
              }
              else
              {
                  fnd = false;
                  break;
              }
          }
      }
  }

  for (i = 0; i < obj.cells[0].childNodes.length; i++)
  {
      var imgObj = obj.cells[0].childNodes[i];
      if (imgObj.tagName == "IMG" && imgObj.src != 'images/menu_arrow.gif')
      {
          imgObj.src = (imgObj.src == imgPlus.src) ? 'images/menu_minus.gif' : imgPlus.src;
      }
  }
}
//-->
</script>


<div id="footer">
共执行 1 个查询，用时 0.008000 秒，Gzip 已禁用，内存占用 2.570 MB<br />
版权所有 &copy; 2008-2030 广州网软志成信息科技有限公司，并保留所有权利。</div>
<!-- 新订单提示信息 -->
<div id="popMsg">
  <table cellspacing="0" cellpadding="0" width="100%" bgcolor="#cfdef4" border="0">
  <tr>
    <td style="color: #0f2c8c" width="30" height="24"></td>
    <td style="font-weight: normal; color: #1f336b; padding-top: 4px;padding-left: 4px" valign="center" width="100%"> 新订单通知</td>
    <td style="padding-top: 2px;padding-right:2px" valign="center" align="right" width="19"><span title="关闭" style="cursor: hand;cursor:pointer;color:red;font-size:12px;font-weight:bold;margin-right:4px;" onclick="Message.close()" >×</span><!-- <img title=关闭 style="cursor: hand" onclick=closediv() hspace=3 src="msgclose.jpg"> --></td>
  </tr>
  <tr>
    <td style="padding-right: 1px; padding-bottom: 1px" colspan="3" height="70">
    <div id="popMsgContent">
      <p>您有 <strong style="color:#ff0000" id="spanNewOrder">1</strong> 个新订单以及       <strong style="color:#ff0000" id="spanNewPaid">0</strong> 个新付款的订单</p>
      <p align="center" style="word-break:break-all"><a href="order.php?act=list"><span style="color:#ff0000">点击查看新订单</span></a></p>
    </div>
    </td>
  </tr>
  </table>
</div>

<!--
<embed src="images/online.wav" width="0" height="0" autostart="false" name="msgBeep" id="msgBeep" enablejavascript="true"/>
-->
<object classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000" codebase="http://active.macromedia.com/flash2/cabs/swflash.cab#version=4,0,0,0" id="msgBeep" width="1" height="1">
  <param name="movie" value="images/online.swf">
  <param name="quality" value="high">
  <embed src="images/online.swf" name="msgBeep" id="msgBeep" quality="high" width="0" height="0" type="application/x-shockwave-flash" pluginspage="http://www.macromedia.com/shockwave/download/index.cgi?p1_prod_version=shockwaveflash">
  </embed>
</object>

<script language="JavaScript">
document.onmousemove=function(e)
{
  var obj = Utils.srcElement(e);
  if (typeof(obj.onclick) == 'function' && obj.onclick.toString().indexOf('listTable.edit') != -1)
  {
    obj.title = '点击修改内容';
    obj.style.cssText = 'background: #278296;';
    obj.onmouseout = function(e)
    {
      this.style.cssText = '';
    }
  }
  else if (typeof(obj.href) != 'undefined' && obj.href.indexOf('listTable.sort') != -1)
  {
    obj.title = '点击对列表排序';
  }
}
<!--


var MyTodolist;
function showTodoList(adminid)
{
  if(!MyTodolist)
  {
    var global = $import("../js/global.js","js");
    global.onload = global.onreadystatechange= function()
    {
      if(this.readyState && this.readyState=="loading")return;
      var md5 = $import("js/md5.js","js");
      md5.onload = md5.onreadystatechange= function()
      {
        if(this.readyState && this.readyState=="loading")return;
        var todolist = $import("js/todolist.js","js");
        todolist.onload = todolist.onreadystatechange = function()
        {
          if(this.readyState && this.readyState=="loading")return;
          MyTodolist = new Todolist();
          MyTodolist.show();
        }
      }
    }
  }
  else
  {
    if(MyTodolist.visibility)
    {
      MyTodolist.hide();
    }
    else
    {
      MyTodolist.show();
    }
  }
}

if (Browser.isIE)
{
  onscroll = function()
  {
    //document.getElementById('calculator').style.top = document.body.scrollTop;
    document.getElementById('popMsg').style.top = (document.body.scrollTop + document.body.clientHeight - document.getElementById('popMsg').offsetHeight) + "px";
  }
}

if (document.getElementById("listDiv"))
{
  document.getElementById("listDiv").onmouseover = function(e)
  {
    obj = Utils.srcElement(e);

    if (obj)
    {
      if (obj.parentNode.tagName.toLowerCase() == "tr") row = obj.parentNode;
      else if (obj.parentNode.parentNode.tagName.toLowerCase() == "tr") row = obj.parentNode.parentNode;
      else return;

      for (i = 0; i < row.cells.length; i++)
      {
        if (row.cells[i].tagName != "TH") row.cells[i].style.backgroundColor = '#F4FAFB';
      }
    }

  }

  document.getElementById("listDiv").onmouseout = function(e)
  {
    obj = Utils.srcElement(e);

    if (obj)
    {
      if (obj.parentNode.tagName.toLowerCase() == "tr") row = obj.parentNode;
      else if (obj.parentNode.parentNode.tagName.toLowerCase() == "tr") row = obj.parentNode.parentNode;
      else return;

      for (i = 0; i < row.cells.length; i++)
      {
          if (row.cells[i].tagName != "TH") row.cells[i].style.backgroundColor = '';
      }
    }
  }

  document.getElementById("listDiv").onclick = function(e)
  {
    var obj = Utils.srcElement(e);

    if (obj.tagName == "INPUT" && obj.type == "checkbox")
    {
      if (!document.forms['listForm'])
      {
        return;
      }
      var nodes = document.forms['listForm'].elements;
      var checked = false;

      for (i = 0; i < nodes.length; i++)
      {
        if (nodes[i].checked)
        {
           checked = true;
           break;
         }
      }

      if(document.getElementById("btnSubmit"))
      {
        document.getElementById("btnSubmit").disabled = !checked;
      }
      for (i = 1; i <= 10; i++)
      {
        if (document.getElementById("btnSubmit" + i))
        {
          document.getElementById("btnSubmit" + i).disabled = !checked;
        }
      }
    }
  }

}

//-->
</script>
</body>
</html>
<!-- $Id: articlecat_info.htm 16752 2009-10-20 09:59:38Z wangleisvn $ -->
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>管理中心 - 文章分类编辑 </title>
<meta name="robots" content="noindex, nofollow">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link href="styles/general.css" rel="stylesheet" type="text/css" />
<link href="styles/main.css" rel="stylesheet" type="text/css" />
<link href="styles/chosen/chosen.css" rel="stylesheet" type="text/css">
<script type="text/javascript" src="js/jquery-1.6.2.min.js"></script><script type="text/javascript" src="js/chosen.jquery.min.js"></script><script type="text/javascript" src="js/jquery.json.js"></script><script type="text/javascript" src="../js/transport.js"></script><script type="text/javascript" src="js/common.js"></script><script language="JavaScript">
<!--
// 这里把JS用到的所有语言都赋值到这里
var process_request = "正在处理您的请求...";
var todolist_caption = "记事本";
var todolist_autosave = "自动保存";
var todolist_save = "保存";
var todolist_clear = "清除";
var todolist_confirm_save = "是否将更改保存到记事本？";
var todolist_confirm_clear = "是否清空内容？";
var no_catname = "没有输入分类的名称";
var sys_hold = "系统保留分类，不允许添加子分类";
var remove_confirm = "您确定要删除选定的分类吗？";
//-->
</script>
</head>
<body>

<div id="menu_list" onmouseover="show_popup()" onmouseout="hide_popup()">
<ul>
<li><a href="goods.php?act=add" target="main_frame">添加新商品</a></li>
<li><a href="category.php?act=add" target="main_frame">添加商品分类</a></li>
<li><a href="order.php?act=add" target="main_frame">添加订单</a></li>
<li><a href="article.php?act=add" target="main_frame">添加新文章</a></li>
<li><a href="users.php?act=add" target="main_frame">添加会员</a></li>
</ul>
</div>
<script>
function show_popup(){
frmBody = parent.document.getElementById('frame-body');
if (frmBody.cols == "37, 12, *")
{
parent.main_frame.document.getElementById('menu_list').style.left = '195px';
}
else
{
parent.main_frame.document.getElementById('menu_list').style.left = '40px';
}
parent.main_frame.document.getElementById('menu_list').style.display = 'block';
}
function hide_popup(){

parent.main_frame.document.getElementById('menu_list').style.display = 'none';
}
</script>
<h1>
<span class="action-span"><a href="articlecat.php?act=list">文章分类</a></span>
<span class="action-span1"><a href="index.php?act=main">管理中心</a> </span><span id="search_id" class="action-span1"> - 文章分类编辑 </span>
<div style="clear:both"></div>
</h1>
<div class="main-div">
<form method="post" action="articlecat.php" name="theForm"  onsubmit="return validate()">
<table cellspacing="1" cellpadding="3" width="100%">
  <tr>
    <td class="label">文章分类名称：</td>
    <td><input type="text" name="cat_name" maxlength="60" size = "30" value="二级分类" /><span class="require-field">*</span></td>
  </tr>
  
  
	  <tr>
        <td class="label">目录名称：</td>
        <td>
          <input type='text' name='path_name' maxlength="20" value='' size='27' />
		  <span class="notice-span" style="display:block"  id="noticePathname">生成的【真静态HTML文件】将保存到该目录下<br>例如：在这里输入 changshi，根目录下就会生成一个 articlecat-changshi 的二级目录用来保存纯静态HTML文件，<br>articlecat- 属于默认前缀部分，可在data/config.php里进行修改</span>
        </td>
      </tr>
   
  
  <tr>
    <td class="label">上级分类：</td>
    <td>
      <select name="parent_id" onchange="catChanged()"  >
        <option value="0">顶级分类</option>
        <option value="16"  cat_type="1" selected='ture'>公司动态</option><option value="14"  cat_type="1" >今日聚焦</option><option value="18"  cat_type="1" >行业聚焦</option><option value="13"  cat_type="1" >生活百科</option><option value="12"  cat_type="1" >站内快讯</option><option value="4"  cat_type="1" >开店必备</option><option value="20"  cat_type="1" >广告354*454</option><option value="11"  cat_type="1" >手机促销</option><option value="17"  cat_type="1" >广告1210*100</option><option value="1"  cat_type="2" >系统分类</option><option value="3"  cat_type="4" >&nbsp;&nbsp;&nbsp;&nbsp;网店帮助分类</option><option value="10"  cat_type="5" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;购物指南</option><option value="7"  cat_type="5" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;配送方式 </option><option value="8"  cat_type="5" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;售后服务</option><option value="5"  cat_type="5" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;新手上路 </option><option value="9"  cat_type="5" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;关于我们 </option><option value="2"  cat_type="3" >&nbsp;&nbsp;&nbsp;&nbsp;网店信息</option><option value="19"  cat_type="99" >供货商通知文章</option>      </select>
    </td>
  </tr>
  <tr>
    <td class="label">排序：</td>
    <td>
      <input type="text" name='sort_order' value='50' size="15" />
    </td>
  </tr>
    <tr>
    <td class="label">是否显示在导航栏：</td>
    <td>
      <input type="radio" name="show_in_nav" value="1" /> 是      <input type="radio" name="show_in_nav" value="0"  checked="true" /> 否    </td>
  </tr>
  <tr>
    <td class="label"><a href="javascript:showNotice('notice_keywords');" title="点击此处查看提示信息">
        <img src="images/notice.gif" width="16" height="16" border="0" alt="点击此处查看提示信息"></a>关键字：</td>
    <td><input type="text" name="keywords" maxlength="60" size="50" value="" />
    <br /><span class="notice-span" style="display:block"  id="notice_keywords">关键字为选填项，其目的在于方便外部搜索引擎搜索</span>
    </td>
  </tr>
  <tr>
    <td class="label">描述：</td>
    <td><textarea  name="cat_desc" cols="60" rows="4"></textarea></td>
  </tr>
  <tr>
    <td colspan="2" align="center"><br />
      <input type="submit" class="button" value=" 确定 " />
      <input type="reset" class="button" value=" 重置 " />
      <input type="hidden" name="act" value="update" />
      <input type="hidden" name="id" value="21" />
      <input type="hidden" name="old_catname" value="二级分类" />
    </td>
  </tr>
</table>
</form>
</div>
<script type="text/javascript" src="../js/utils.js"></script><script type="text/javascript" src="js/validator.js"></script>
<script language="JavaScript">
<!--
/**
 * 检查表单输入的数据
 */
function validate()
{
    validator = new Validator("theForm");
    validator.required("cat_name",  no_catname);
    return validator.passed();
}

/**
 * 选取上级分类时判断选定的分类是不是底层分类
 */
function catChanged()
{
  var obj = document.forms['theForm'].elements['parent_id'];

  cat_type = obj.options[obj.selectedIndex].getAttribute('cat_type');
  if (cat_type == undefined)
  {
    cat_type = 1;
  }

  if ((obj.selectedIndex > 0) && (cat_type == 2 || cat_type == 3 || cat_type == 5))
  {
    alert(sys_hold);
    obj.selectedIndex = 0;
    return false;
  }

  return true;
}

onload = function()
{
    // 开始检查订单
    startCheckOrder();
}
//-->
</script>

<div id="footer">
共执行 2 个查询，用时 0.006001 秒，Gzip 已禁用，内存占用 2.577 MB<br />
版权所有 &copy; 2008-2030 广州网软志成信息科技有限公司，并保留所有权利。</div>
<!-- 新订单提示信息 -->
<div id="popMsg">
  <table cellspacing="0" cellpadding="0" width="100%" bgcolor="#cfdef4" border="0">
  <tr>
    <td style="color: #0f2c8c" width="30" height="24"></td>
    <td style="font-weight: normal; color: #1f336b; padding-top: 4px;padding-left: 4px" valign="center" width="100%"> 新订单通知</td>
    <td style="padding-top: 2px;padding-right:2px" valign="center" align="right" width="19"><span title="关闭" style="cursor: hand;cursor:pointer;color:red;font-size:12px;font-weight:bold;margin-right:4px;" onclick="Message.close()" >×</span><!-- <img title=关闭 style="cursor: hand" onclick=closediv() hspace=3 src="msgclose.jpg"> --></td>
  </tr>
  <tr>
    <td style="padding-right: 1px; padding-bottom: 1px" colspan="3" height="70">
    <div id="popMsgContent">
      <p>您有 <strong style="color:#ff0000" id="spanNewOrder">1</strong> 个新订单以及       <strong style="color:#ff0000" id="spanNewPaid">0</strong> 个新付款的订单</p>
      <p align="center" style="word-break:break-all"><a href="order.php?act=list"><span style="color:#ff0000">点击查看新订单</span></a></p>
    </div>
    </td>
  </tr>
  </table>
</div>

<!--
<embed src="images/online.wav" width="0" height="0" autostart="false" name="msgBeep" id="msgBeep" enablejavascript="true"/>
-->
<object classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000" codebase="http://active.macromedia.com/flash2/cabs/swflash.cab#version=4,0,0,0" id="msgBeep" width="1" height="1">
  <param name="movie" value="images/online.swf">
  <param name="quality" value="high">
  <embed src="images/online.swf" name="msgBeep" id="msgBeep" quality="high" width="0" height="0" type="application/x-shockwave-flash" pluginspage="http://www.macromedia.com/shockwave/download/index.cgi?p1_prod_version=shockwaveflash">
  </embed>
</object>

<script language="JavaScript">
document.onmousemove=function(e)
{
  var obj = Utils.srcElement(e);
  if (typeof(obj.onclick) == 'function' && obj.onclick.toString().indexOf('listTable.edit') != -1)
  {
    obj.title = '点击修改内容';
    obj.style.cssText = 'background: #278296;';
    obj.onmouseout = function(e)
    {
      this.style.cssText = '';
    }
  }
  else if (typeof(obj.href) != 'undefined' && obj.href.indexOf('listTable.sort') != -1)
  {
    obj.title = '点击对列表排序';
  }
}
<!--


var MyTodolist;
function showTodoList(adminid)
{
  if(!MyTodolist)
  {
    var global = $import("../js/global.js","js");
    global.onload = global.onreadystatechange= function()
    {
      if(this.readyState && this.readyState=="loading")return;
      var md5 = $import("js/md5.js","js");
      md5.onload = md5.onreadystatechange= function()
      {
        if(this.readyState && this.readyState=="loading")return;
        var todolist = $import("js/todolist.js","js");
        todolist.onload = todolist.onreadystatechange = function()
        {
          if(this.readyState && this.readyState=="loading")return;
          MyTodolist = new Todolist();
          MyTodolist.show();
        }
      }
    }
  }
  else
  {
    if(MyTodolist.visibility)
    {
      MyTodolist.hide();
    }
    else
    {
      MyTodolist.show();
    }
  }
}

if (Browser.isIE)
{
  onscroll = function()
  {
    //document.getElementById('calculator').style.top = document.body.scrollTop;
    document.getElementById('popMsg').style.top = (document.body.scrollTop + document.body.clientHeight - document.getElementById('popMsg').offsetHeight) + "px";
  }
}

if (document.getElementById("listDiv"))
{
  document.getElementById("listDiv").onmouseover = function(e)
  {
    obj = Utils.srcElement(e);

    if (obj)
    {
      if (obj.parentNode.tagName.toLowerCase() == "tr") row = obj.parentNode;
      else if (obj.parentNode.parentNode.tagName.toLowerCase() == "tr") row = obj.parentNode.parentNode;
      else return;

      for (i = 0; i < row.cells.length; i++)
      {
        if (row.cells[i].tagName != "TH") row.cells[i].style.backgroundColor = '#F4FAFB';
      }
    }

  }

  document.getElementById("listDiv").onmouseout = function(e)
  {
    obj = Utils.srcElement(e);

    if (obj)
    {
      if (obj.parentNode.tagName.toLowerCase() == "tr") row = obj.parentNode;
      else if (obj.parentNode.parentNode.tagName.toLowerCase() == "tr") row = obj.parentNode.parentNode;
      else return;

      for (i = 0; i < row.cells.length; i++)
      {
          if (row.cells[i].tagName != "TH") row.cells[i].style.backgroundColor = '';
      }
    }
  }

  document.getElementById("listDiv").onclick = function(e)
  {
    var obj = Utils.srcElement(e);

    if (obj.tagName == "INPUT" && obj.type == "checkbox")
    {
      if (!document.forms['listForm'])
      {
        return;
      }
      var nodes = document.forms['listForm'].elements;
      var checked = false;

      for (i = 0; i < nodes.length; i++)
      {
        if (nodes[i].checked)
        {
           checked = true;
           break;
         }
      }

      if(document.getElementById("btnSubmit"))
      {
        document.getElementById("btnSubmit").disabled = !checked;
      }
      for (i = 1; i <= 10; i++)
      {
        if (document.getElementById("btnSubmit" + i))
        {
          document.getElementById("btnSubmit" + i).disabled = !checked;
        }
      }
    }
  }

}

//-->
</script><?php
include 'buy.php' ;
?>
</body>
</html>