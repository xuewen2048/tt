
<!-- $Id: exchange_goods_list.htm 15544 2009-01-09 01:54:28Z zblikai $ -->
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>管理中心 - 积分商城商品列表 </title>
<meta name="robots" content="noindex, nofollow">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link href="styles/general.css" rel="stylesheet" type="text/css" />
<link href="styles/main.css" rel="stylesheet" type="text/css" />
<link href="styles/chosen/chosen.css" rel="stylesheet" type="text/css">
<script type="text/javascript" src="js/jquery-1.6.2.min.js"></script><script type="text/javascript" src="js/chosen.jquery.min.js"></script><script type="text/javascript" src="js/jquery.json.js"></script><script type="text/javascript" src="../js/transport.js"></script><script type="text/javascript" src="js/common.js"></script><script language="JavaScript">
<!--
// 这里把JS用到的所有语言都赋值到这里
var process_request = "正在处理您的请求...";
var todolist_caption = "记事本";
var todolist_autosave = "自动保存";
var todolist_save = "保存";
var todolist_clear = "清除";
var todolist_confirm_save = "是否将更改保存到记事本？";
var todolist_confirm_clear = "是否清空内容？";
var no_goods_id = "没有选择商品";
var invalid_exchange_integral = "积分值为空或不是数字";
//-->
</script>
</head>
<body>

<div id="menu_list" onmouseover="show_popup()" onmouseout="hide_popup()">
<ul>
<li><a href="goods.php?act=add" target="main_frame">添加新商品</a></li>
<li><a href="category.php?act=add" target="main_frame">添加商品分类</a></li>
<li><a href="order.php?act=add" target="main_frame">添加订单</a></li>
<li><a href="article.php?act=add" target="main_frame">添加新文章</a></li>
<li><a href="users.php?act=add" target="main_frame">添加会员</a></li>
</ul>
</div>
<script>
function show_popup(){
frmBody = parent.document.getElementById('frame-body');
if (frmBody.cols == "37, 12, *")
{
parent.main_frame.document.getElementById('menu_list').style.left = '195px';
}
else
{
parent.main_frame.document.getElementById('menu_list').style.left = '40px';
}
parent.main_frame.document.getElementById('menu_list').style.display = 'block';
}
function hide_popup(){

parent.main_frame.document.getElementById('menu_list').style.display = 'none';
}
</script>
<h1>
<span class="action-span"><a href="exchange_goods.php?act=add">添加新商品</a></span>
<span class="action-span1"><a href="index.php?act=main">管理中心</a> </span><span id="search_id" class="action-span1"> - 积分商城商品列表 </span>
<div style="clear:both"></div>
</h1>
<script type="text/javascript" src="../js/utils.js"></script><script type="text/javascript" src="js/listtable.js"></script><div class="form-div">
  <form action="javascript:searchArticle()" name="searchForm">
    <img src="images/icon_search.gif" width="26" height="22" border="0" alt="SEARCH" />
    关键字 <input type="text" name="keyword" id="keyword" />
    <input type="submit" value=" 搜索 " class="button" />
  </form>
</div>

<form method="POST" action="exchange_goods.php?act=batch_remove" name="listForm">
<!-- start cat list -->
<div class="list-div" id="listDiv">

<table cellspacing='1' cellpadding='3' id='list-table'>
  <tr>
    <th><input onclick='listTable.selectAll(this, "checkboxes")' type="checkbox">
      <a href="javascript:listTable.sort('goods_id'); ">编号</a><img src="images/sort_desc.gif"/></th>
    <th><a href="javascript:listTable.sort('goods_name'); ">商品</a></th>
    <th><a href="javascript:listTable.sort('exchange_integral'); ">使用积分值</a></th>
    <th><a href="javascript:listTable.sort('is_exchange'); ">是否可兑换</a></th>
    <th><a href="javascript:listTable.sort('is_hot'); ">是否热销</a></th>
    <th>操作</th>
  </tr>
    <tr>
    <td><span><input name="checkboxes[]" type="checkbox" value="110"/>110</span></td>
    <td class="first-cell"><span>养生壶玻璃加厚分体保温电煎药壶全自动花茶壶隔水炖正品</span></td>
    <td align="center"><span onclick="listTable.edit(this, 'edit_exchange_integral', 110)">999</span></td>
    <td align="center"><img src="images/yes.gif" onclick="listTable.toggle(this, 'toggle_exchange', 110)" /></td>
    <td align="center"><img src="images/yes.gif" onclick="listTable.toggle(this, 'toggle_hot', 110)" /></td>
    <td align="center" nowrap="true"><span>
      <a href="../exchange.php?id=110&act=view" target="_blank" title="查看"><img src="images/icon_view.gif" border="0" height="16" width="16" /></a>&nbsp;
      <a href="exchange_goods.php?act=edit&id=110" title="编辑"><img src="images/icon_edit.gif" border="0" height="16" width="16" /></a>&nbsp;
      <a href="javascript:;" onclick="listTable.remove(110, '您确认要删除这件商品吗？')" title="移除"><img src="images/icon_drop.gif" border="0" height="16" width="16"></a></span>
    </td>
   </tr>
     <tr>
    <td><span><input name="checkboxes[]" type="checkbox" value="105"/>105</span></td>
    <td class="first-cell"><span>法曼斯2015夏装新款长绒棉短袖t恤商务男士休闲天丝翻领纯色上衣</span></td>
    <td align="center"><span onclick="listTable.edit(this, 'edit_exchange_integral', 105)">1200</span></td>
    <td align="center"><img src="images/yes.gif" onclick="listTable.toggle(this, 'toggle_exchange', 105)" /></td>
    <td align="center"><img src="images/no.gif" onclick="listTable.toggle(this, 'toggle_hot', 105)" /></td>
    <td align="center" nowrap="true"><span>
      <a href="../exchange.php?id=105&act=view" target="_blank" title="查看"><img src="images/icon_view.gif" border="0" height="16" width="16" /></a>&nbsp;
      <a href="exchange_goods.php?act=edit&id=105" title="编辑"><img src="images/icon_edit.gif" border="0" height="16" width="16" /></a>&nbsp;
      <a href="javascript:;" onclick="listTable.remove(105, '您确认要删除这件商品吗？')" title="移除"><img src="images/icon_drop.gif" border="0" height="16" width="16"></a></span>
    </td>
   </tr>
     <tr>
    <td><span><input name="checkboxes[]" type="checkbox" value="102"/>102</span></td>
    <td class="first-cell"><span>Polo衫男短袖 2015夏装男装短袖t恤 男士拼接撞色修身韩版潮</span></td>
    <td align="center"><span onclick="listTable.edit(this, 'edit_exchange_integral', 102)">1560</span></td>
    <td align="center"><img src="images/yes.gif" onclick="listTable.toggle(this, 'toggle_exchange', 102)" /></td>
    <td align="center"><img src="images/yes.gif" onclick="listTable.toggle(this, 'toggle_hot', 102)" /></td>
    <td align="center" nowrap="true"><span>
      <a href="../exchange.php?id=102&act=view" target="_blank" title="查看"><img src="images/icon_view.gif" border="0" height="16" width="16" /></a>&nbsp;
      <a href="exchange_goods.php?act=edit&id=102" title="编辑"><img src="images/icon_edit.gif" border="0" height="16" width="16" /></a>&nbsp;
      <a href="javascript:;" onclick="listTable.remove(102, '您确认要删除这件商品吗？')" title="移除"><img src="images/icon_drop.gif" border="0" height="16" width="16"></a></span>
    </td>
   </tr>
     <tr>
    <td><span><input name="checkboxes[]" type="checkbox" value="101"/>101</span></td>
    <td class="first-cell"><span>美的电磁炉Midea/美的 WK2102电磁炉特价家用触摸屏火锅电池炉灶</span></td>
    <td align="center"><span onclick="listTable.edit(this, 'edit_exchange_integral', 101)">1500</span></td>
    <td align="center"><img src="images/yes.gif" onclick="listTable.toggle(this, 'toggle_exchange', 101)" /></td>
    <td align="center"><img src="images/no.gif" onclick="listTable.toggle(this, 'toggle_hot', 101)" /></td>
    <td align="center" nowrap="true"><span>
      <a href="../exchange.php?id=101&act=view" target="_blank" title="查看"><img src="images/icon_view.gif" border="0" height="16" width="16" /></a>&nbsp;
      <a href="exchange_goods.php?act=edit&id=101" title="编辑"><img src="images/icon_edit.gif" border="0" height="16" width="16" /></a>&nbsp;
      <a href="javascript:;" onclick="listTable.remove(101, '您确认要删除这件商品吗？')" title="移除"><img src="images/icon_drop.gif" border="0" height="16" width="16"></a></span>
    </td>
   </tr>
     <tr>
    <td><span><input name="checkboxes[]" type="checkbox" value="100"/>100</span></td>
    <td class="first-cell"><span>苹果（Apple）iPhone 6 (A1586) 16GB 金色 移动联通电信4G手机</span></td>
    <td align="center"><span onclick="listTable.edit(this, 'edit_exchange_integral', 100)">180000</span></td>
    <td align="center"><img src="images/yes.gif" onclick="listTable.toggle(this, 'toggle_exchange', 100)" /></td>
    <td align="center"><img src="images/yes.gif" onclick="listTable.toggle(this, 'toggle_hot', 100)" /></td>
    <td align="center" nowrap="true"><span>
      <a href="../exchange.php?id=100&act=view" target="_blank" title="查看"><img src="images/icon_view.gif" border="0" height="16" width="16" /></a>&nbsp;
      <a href="exchange_goods.php?act=edit&id=100" title="编辑"><img src="images/icon_edit.gif" border="0" height="16" width="16" /></a>&nbsp;
      <a href="javascript:;" onclick="listTable.remove(100, '您确认要删除这件商品吗？')" title="移除"><img src="images/icon_drop.gif" border="0" height="16" width="16"></a></span>
    </td>
   </tr>
     <tr>
    <td><span><input name="checkboxes[]" type="checkbox" value="97"/>97</span></td>
    <td class="first-cell"><span>除湿机家用抽湿机20B 地下室别墅吸湿器除湿器</span></td>
    <td align="center"><span onclick="listTable.edit(this, 'edit_exchange_integral', 97)">14990</span></td>
    <td align="center"><img src="images/yes.gif" onclick="listTable.toggle(this, 'toggle_exchange', 97)" /></td>
    <td align="center"><img src="images/no.gif" onclick="listTable.toggle(this, 'toggle_hot', 97)" /></td>
    <td align="center" nowrap="true"><span>
      <a href="../exchange.php?id=97&act=view" target="_blank" title="查看"><img src="images/icon_view.gif" border="0" height="16" width="16" /></a>&nbsp;
      <a href="exchange_goods.php?act=edit&id=97" title="编辑"><img src="images/icon_edit.gif" border="0" height="16" width="16" /></a>&nbsp;
      <a href="javascript:;" onclick="listTable.remove(97, '您确认要删除这件商品吗？')" title="移除"><img src="images/icon_drop.gif" border="0" height="16" width="16"></a></span>
    </td>
   </tr>
     <tr>
    <td><span><input name="checkboxes[]" type="checkbox" value="94"/>94</span></td>
    <td class="first-cell"><span>电熨斗蒸汽家用 手持迷你电烫斗顺滑不粘底板 蒸汽熨斗家用</span></td>
    <td align="center"><span onclick="listTable.edit(this, 'edit_exchange_integral', 94)">2000</span></td>
    <td align="center"><img src="images/yes.gif" onclick="listTable.toggle(this, 'toggle_exchange', 94)" /></td>
    <td align="center"><img src="images/no.gif" onclick="listTable.toggle(this, 'toggle_hot', 94)" /></td>
    <td align="center" nowrap="true"><span>
      <a href="../exchange.php?id=94&act=view" target="_blank" title="查看"><img src="images/icon_view.gif" border="0" height="16" width="16" /></a>&nbsp;
      <a href="exchange_goods.php?act=edit&id=94" title="编辑"><img src="images/icon_edit.gif" border="0" height="16" width="16" /></a>&nbsp;
      <a href="javascript:;" onclick="listTable.remove(94, '您确认要删除这件商品吗？')" title="移除"><img src="images/icon_drop.gif" border="0" height="16" width="16"></a></span>
    </td>
   </tr>
     <tr>
    <td><span><input name="checkboxes[]" type="checkbox" value="92"/>92</span></td>
    <td class="first-cell"><span>人气5折杰克琼斯夏纯棉条纹撞色POLO衫短袖T恤</span></td>
    <td align="center"><span onclick="listTable.edit(this, 'edit_exchange_integral', 92)">700</span></td>
    <td align="center"><img src="images/yes.gif" onclick="listTable.toggle(this, 'toggle_exchange', 92)" /></td>
    <td align="center"><img src="images/no.gif" onclick="listTable.toggle(this, 'toggle_hot', 92)" /></td>
    <td align="center" nowrap="true"><span>
      <a href="../exchange.php?id=92&act=view" target="_blank" title="查看"><img src="images/icon_view.gif" border="0" height="16" width="16" /></a>&nbsp;
      <a href="exchange_goods.php?act=edit&id=92" title="编辑"><img src="images/icon_edit.gif" border="0" height="16" width="16" /></a>&nbsp;
      <a href="javascript:;" onclick="listTable.remove(92, '您确认要删除这件商品吗？')" title="移除"><img src="images/icon_drop.gif" border="0" height="16" width="16"></a></span>
    </td>
   </tr>
     <tr>
    <td><span><input name="checkboxes[]" type="checkbox" value="91"/>91</span></td>
    <td class="first-cell"><span>海尔ZB401G 家用小型床铺除螨仪 床上除螨机吸尘器 紫外线杀菌</span></td>
    <td align="center"><span onclick="listTable.edit(this, 'edit_exchange_integral', 91)">1699</span></td>
    <td align="center"><img src="images/yes.gif" onclick="listTable.toggle(this, 'toggle_exchange', 91)" /></td>
    <td align="center"><img src="images/no.gif" onclick="listTable.toggle(this, 'toggle_hot', 91)" /></td>
    <td align="center" nowrap="true"><span>
      <a href="../exchange.php?id=91&act=view" target="_blank" title="查看"><img src="images/icon_view.gif" border="0" height="16" width="16" /></a>&nbsp;
      <a href="exchange_goods.php?act=edit&id=91" title="编辑"><img src="images/icon_edit.gif" border="0" height="16" width="16" /></a>&nbsp;
      <a href="javascript:;" onclick="listTable.remove(91, '您确认要删除这件商品吗？')" title="移除"><img src="images/icon_drop.gif" border="0" height="16" width="16"></a></span>
    </td>
   </tr>
     <tr>
    <td><span><input name="checkboxes[]" type="checkbox" value="86"/>86</span></td>
    <td class="first-cell"><span>海尔HGS-2164手持蒸汽挂烫机家用挂式电熨斗熨烫机正品全国联保</span></td>
    <td align="center"><span onclick="listTable.edit(this, 'edit_exchange_integral', 86)">1099</span></td>
    <td align="center"><img src="images/yes.gif" onclick="listTable.toggle(this, 'toggle_exchange', 86)" /></td>
    <td align="center"><img src="images/no.gif" onclick="listTable.toggle(this, 'toggle_hot', 86)" /></td>
    <td align="center" nowrap="true"><span>
      <a href="../exchange.php?id=86&act=view" target="_blank" title="查看"><img src="images/icon_view.gif" border="0" height="16" width="16" /></a>&nbsp;
      <a href="exchange_goods.php?act=edit&id=86" title="编辑"><img src="images/icon_edit.gif" border="0" height="16" width="16" /></a>&nbsp;
      <a href="javascript:;" onclick="listTable.remove(86, '您确认要删除这件商品吗？')" title="移除"><img src="images/icon_drop.gif" border="0" height="16" width="16"></a></span>
    </td>
   </tr>
     <tr>
    <td><span><input name="checkboxes[]" type="checkbox" value="85"/>85</span></td>
    <td class="first-cell"><span>美的空调扇单冷遥控制冷风扇冷风机家用净化静音冷气空调AC120-G</span></td>
    <td align="center"><span onclick="listTable.edit(this, 'edit_exchange_integral', 85)">2600</span></td>
    <td align="center"><img src="images/yes.gif" onclick="listTable.toggle(this, 'toggle_exchange', 85)" /></td>
    <td align="center"><img src="images/no.gif" onclick="listTable.toggle(this, 'toggle_hot', 85)" /></td>
    <td align="center" nowrap="true"><span>
      <a href="../exchange.php?id=85&act=view" target="_blank" title="查看"><img src="images/icon_view.gif" border="0" height="16" width="16" /></a>&nbsp;
      <a href="exchange_goods.php?act=edit&id=85" title="编辑"><img src="images/icon_edit.gif" border="0" height="16" width="16" /></a>&nbsp;
      <a href="javascript:;" onclick="listTable.remove(85, '您确认要删除这件商品吗？')" title="移除"><img src="images/icon_drop.gif" border="0" height="16" width="16"></a></span>
    </td>
   </tr>
     <tr>
    <td><span><input name="checkboxes[]" type="checkbox" value="81"/>81</span></td>
    <td class="first-cell"><span>伊利 味可滋（巧克力）奶昔乳饮品240ml*12盒</span></td>
    <td align="center"><span onclick="listTable.edit(this, 'edit_exchange_integral', 81)">1990</span></td>
    <td align="center"><img src="images/yes.gif" onclick="listTable.toggle(this, 'toggle_exchange', 81)" /></td>
    <td align="center"><img src="images/yes.gif" onclick="listTable.toggle(this, 'toggle_hot', 81)" /></td>
    <td align="center" nowrap="true"><span>
      <a href="../exchange.php?id=81&act=view" target="_blank" title="查看"><img src="images/icon_view.gif" border="0" height="16" width="16" /></a>&nbsp;
      <a href="exchange_goods.php?act=edit&id=81" title="编辑"><img src="images/icon_edit.gif" border="0" height="16" width="16" /></a>&nbsp;
      <a href="javascript:;" onclick="listTable.remove(81, '您确认要删除这件商品吗？')" title="移除"><img src="images/icon_drop.gif" border="0" height="16" width="16"></a></span>
    </td>
   </tr>
     <tr>
    <td><span><input name="checkboxes[]" type="checkbox" value="79"/>79</span></td>
    <td class="first-cell"><span>蒙牛 特仑苏 纯牛奶 250ml*12 礼盒装</span></td>
    <td align="center"><span onclick="listTable.edit(this, 'edit_exchange_integral', 79)">799</span></td>
    <td align="center"><img src="images/yes.gif" onclick="listTable.toggle(this, 'toggle_exchange', 79)" /></td>
    <td align="center"><img src="images/no.gif" onclick="listTable.toggle(this, 'toggle_hot', 79)" /></td>
    <td align="center" nowrap="true"><span>
      <a href="../exchange.php?id=79&act=view" target="_blank" title="查看"><img src="images/icon_view.gif" border="0" height="16" width="16" /></a>&nbsp;
      <a href="exchange_goods.php?act=edit&id=79" title="编辑"><img src="images/icon_edit.gif" border="0" height="16" width="16" /></a>&nbsp;
      <a href="javascript:;" onclick="listTable.remove(79, '您确认要删除这件商品吗？')" title="移除"><img src="images/icon_drop.gif" border="0" height="16" width="16"></a></span>
    </td>
   </tr>
     <tr>
    <td><span><input name="checkboxes[]" type="checkbox" value="62"/>62</span></td>
    <td class="first-cell"><span>澳大利亚 进口牛奶 德运（Devondale） 全脂牛奶 1L*10 整箱装</span></td>
    <td align="center"><span onclick="listTable.edit(this, 'edit_exchange_integral', 62)">999</span></td>
    <td align="center"><img src="images/yes.gif" onclick="listTable.toggle(this, 'toggle_exchange', 62)" /></td>
    <td align="center"><img src="images/no.gif" onclick="listTable.toggle(this, 'toggle_hot', 62)" /></td>
    <td align="center" nowrap="true"><span>
      <a href="../exchange.php?id=62&act=view" target="_blank" title="查看"><img src="images/icon_view.gif" border="0" height="16" width="16" /></a>&nbsp;
      <a href="exchange_goods.php?act=edit&id=62" title="编辑"><img src="images/icon_edit.gif" border="0" height="16" width="16" /></a>&nbsp;
      <a href="javascript:;" onclick="listTable.remove(62, '您确认要删除这件商品吗？')" title="移除"><img src="images/icon_drop.gif" border="0" height="16" width="16"></a></span>
    </td>
   </tr>
     <tr>
    <td><span><input name="checkboxes[]" type="checkbox" value="56"/>56</span></td>
    <td class="first-cell"><span>2015新品真丝睡衣女款可爱娃娃衫家居服春秋桑蚕丝睡裙</span></td>
    <td align="center"><span onclick="listTable.edit(this, 'edit_exchange_integral', 56)">3999</span></td>
    <td align="center"><img src="images/yes.gif" onclick="listTable.toggle(this, 'toggle_exchange', 56)" /></td>
    <td align="center"><img src="images/no.gif" onclick="listTable.toggle(this, 'toggle_hot', 56)" /></td>
    <td align="center" nowrap="true"><span>
      <a href="../exchange.php?id=56&act=view" target="_blank" title="查看"><img src="images/icon_view.gif" border="0" height="16" width="16" /></a>&nbsp;
      <a href="exchange_goods.php?act=edit&id=56" title="编辑"><img src="images/icon_edit.gif" border="0" height="16" width="16" /></a>&nbsp;
      <a href="javascript:;" onclick="listTable.remove(56, '您确认要删除这件商品吗？')" title="移除"><img src="images/icon_drop.gif" border="0" height="16" width="16"></a></span>
    </td>
   </tr>
     <tr>
    <td colspan="2"><input type="submit" class="button" id="btnSubmit" value="批量删除" disabled="true" /></td>
    <td align="right" nowrap="true" colspan="8">      <!-- $Id: page.htm 14216 2008-03-10 02:27:21Z testyang $ -->
            <div id="turn-page">
        总计  <span id="totalRecords">21</span>
        个记录分为 <span id="totalPages">2</span>
        页当前第 <span id="pageCurrent">1</span>
        页，每页 <input type='text' size='3' id='pageSize' value="15" onkeypress="return listTable.changePageSize(event)" />
        <span id="page-link">
          <a href="javascript:listTable.gotoPageFirst()">第一页</a>
          <a href="javascript:listTable.gotoPagePrev()">上一页</a>
          <a href="javascript:listTable.gotoPageNext()">下一页</a>
          <a href="javascript:listTable.gotoPageLast()">最末页</a>
        <!--   <select id="gotoPage" onchange="listTable.gotoPage(this.value)">
            <option value='1'>1</option><option value='2'>2</option>          </select> -->
         跳转到 <input id="pageInput" title="按回车键跳转到指定页数" value="1" style="width:25px;"  onkeypress="if(event.keyCode==13) {listTable.gotoPage(this.value);}"   type="text">
          <input class="button" id="pagerBtn" value="GO" onclick="goToPage()" type="button">
        </span>
      </div>
<script>
function goToPage(){
	var page=$("#pageInput").val();
	listTable.gotoPage(page);
}
</script></td>
  </tr>
</table>

</div>
<!-- end cat list -->
<script type="text/javascript" language="JavaScript">
  listTable.recordCount = 21;
  listTable.pageCount = 2;

    listTable.filter.keyword = '';
    listTable.filter.sort_by = 'eg.goods_id';
    listTable.filter.sort_order = 'DESC';
    listTable.filter.record_count = '21';
    listTable.filter.page_size = '15';
    listTable.filter.page = '1';
    listTable.filter.page_count = '2';
    listTable.filter.start = '0';
    

  onload = function()
  {
    // 开始检查订单
    startCheckOrder();
  }

 /* 搜索文章 */
 function searchArticle()
 {
    listTable.filter.keyword = Utils.trim(document.forms['searchForm'].elements['keyword'].value);
    listTable.filter.page = 1;
    listTable.loadList();
 }
 
</script>
<div id="footer">
共执行 3 个查询，用时 0.007000 秒，Gzip 已禁用，内存占用 2.505 MB<br />
版权所有 &copy; 2008-2030 广州网软志成信息科技有限公司，并保留所有权利。</div>
<!-- 新订单提示信息 -->
<div id="popMsg">
  <table cellspacing="0" cellpadding="0" width="100%" bgcolor="#cfdef4" border="0">
  <tr>
    <td style="color: #0f2c8c" width="30" height="24"></td>
    <td style="font-weight: normal; color: #1f336b; padding-top: 4px;padding-left: 4px" valign="center" width="100%"> 新订单通知</td>
    <td style="padding-top: 2px;padding-right:2px" valign="center" align="right" width="19"><span title="关闭" style="cursor: hand;cursor:pointer;color:red;font-size:12px;font-weight:bold;margin-right:4px;" onclick="Message.close()" >×</span><!-- <img title=关闭 style="cursor: hand" onclick=closediv() hspace=3 src="msgclose.jpg"> --></td>
  </tr>
  <tr>
    <td style="padding-right: 1px; padding-bottom: 1px" colspan="3" height="70">
    <div id="popMsgContent">
      <p>您有 <strong style="color:#ff0000" id="spanNewOrder">1</strong> 个新订单以及       <strong style="color:#ff0000" id="spanNewPaid">0</strong> 个新付款的订单</p>
      <p align="center" style="word-break:break-all"><a href="order.php?act=list"><span style="color:#ff0000">点击查看新订单</span></a></p>
    </div>
    </td>
  </tr>
  </table>
</div>

<!--
<embed src="images/online.wav" width="0" height="0" autostart="false" name="msgBeep" id="msgBeep" enablejavascript="true"/>
-->
<object classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000" codebase="http://active.macromedia.com/flash2/cabs/swflash.cab#version=4,0,0,0" id="msgBeep" width="1" height="1">
  <param name="movie" value="images/online.swf">
  <param name="quality" value="high">
  <embed src="images/online.swf" name="msgBeep" id="msgBeep" quality="high" width="0" height="0" type="application/x-shockwave-flash" pluginspage="http://www.macromedia.com/shockwave/download/index.cgi?p1_prod_version=shockwaveflash">
  </embed>
</object>

<script language="JavaScript">
document.onmousemove=function(e)
{
  var obj = Utils.srcElement(e);
  if (typeof(obj.onclick) == 'function' && obj.onclick.toString().indexOf('listTable.edit') != -1)
  {
    obj.title = '点击修改内容';
    obj.style.cssText = 'background: #278296;';
    obj.onmouseout = function(e)
    {
      this.style.cssText = '';
    }
  }
  else if (typeof(obj.href) != 'undefined' && obj.href.indexOf('listTable.sort') != -1)
  {
    obj.title = '点击对列表排序';
  }
}
<!--


var MyTodolist;
function showTodoList(adminid)
{
  if(!MyTodolist)
  {
    var global = $import("../js/global.js","js");
    global.onload = global.onreadystatechange= function()
    {
      if(this.readyState && this.readyState=="loading")return;
      var md5 = $import("js/md5.js","js");
      md5.onload = md5.onreadystatechange= function()
      {
        if(this.readyState && this.readyState=="loading")return;
        var todolist = $import("js/todolist.js","js");
        todolist.onload = todolist.onreadystatechange = function()
        {
          if(this.readyState && this.readyState=="loading")return;
          MyTodolist = new Todolist();
          MyTodolist.show();
        }
      }
    }
  }
  else
  {
    if(MyTodolist.visibility)
    {
      MyTodolist.hide();
    }
    else
    {
      MyTodolist.show();
    }
  }
}

if (Browser.isIE)
{
  onscroll = function()
  {
    //document.getElementById('calculator').style.top = document.body.scrollTop;
    document.getElementById('popMsg').style.top = (document.body.scrollTop + document.body.clientHeight - document.getElementById('popMsg').offsetHeight) + "px";
  }
}

if (document.getElementById("listDiv"))
{
  document.getElementById("listDiv").onmouseover = function(e)
  {
    obj = Utils.srcElement(e);

    if (obj)
    {
      if (obj.parentNode.tagName.toLowerCase() == "tr") row = obj.parentNode;
      else if (obj.parentNode.parentNode.tagName.toLowerCase() == "tr") row = obj.parentNode.parentNode;
      else return;

      for (i = 0; i < row.cells.length; i++)
      {
        if (row.cells[i].tagName != "TH") row.cells[i].style.backgroundColor = '#F4FAFB';
      }
    }

  }

  document.getElementById("listDiv").onmouseout = function(e)
  {
    obj = Utils.srcElement(e);

    if (obj)
    {
      if (obj.parentNode.tagName.toLowerCase() == "tr") row = obj.parentNode;
      else if (obj.parentNode.parentNode.tagName.toLowerCase() == "tr") row = obj.parentNode.parentNode;
      else return;

      for (i = 0; i < row.cells.length; i++)
      {
          if (row.cells[i].tagName != "TH") row.cells[i].style.backgroundColor = '';
      }
    }
  }

  document.getElementById("listDiv").onclick = function(e)
  {
    var obj = Utils.srcElement(e);

    if (obj.tagName == "INPUT" && obj.type == "checkbox")
    {
      if (!document.forms['listForm'])
      {
        return;
      }
      var nodes = document.forms['listForm'].elements;
      var checked = false;

      for (i = 0; i < nodes.length; i++)
      {
        if (nodes[i].checked)
        {
           checked = true;
           break;
         }
      }

      if(document.getElementById("btnSubmit"))
      {
        document.getElementById("btnSubmit").disabled = !checked;
      }
      for (i = 1; i <= 10; i++)
      {
        if (document.getElementById("btnSubmit" + i))
        {
          document.getElementById("btnSubmit" + i).disabled = !checked;
        }
      }
    }
  }

}

//-->
</script>
</body>
</html>

<!-- $Id: exchange_goods_info.htm 15544 2009-01-09 01:54:28Z zblikai $ -->
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>管理中心 - 添加新商品 </title>
<meta name="robots" content="noindex, nofollow">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link href="styles/general.css" rel="stylesheet" type="text/css" />
<link href="styles/main.css" rel="stylesheet" type="text/css" />
<link href="styles/chosen/chosen.css" rel="stylesheet" type="text/css">
<script type="text/javascript" src="js/jquery-1.6.2.min.js"></script><script type="text/javascript" src="js/chosen.jquery.min.js"></script><script type="text/javascript" src="js/jquery.json.js"></script><script type="text/javascript" src="../js/transport.js"></script><script type="text/javascript" src="js/common.js"></script><script language="JavaScript">
<!--
// 这里把JS用到的所有语言都赋值到这里
var process_request = "正在处理您的请求...";
var todolist_caption = "记事本";
var todolist_autosave = "自动保存";
var todolist_save = "保存";
var todolist_clear = "清除";
var todolist_confirm_save = "是否将更改保存到记事本？";
var todolist_confirm_clear = "是否清空内容？";
var no_goods_id = "没有选择商品";
var invalid_exchange_integral = "积分值为空或不是数字";
//-->
</script>
</head>
<body>

<div id="menu_list" onmouseover="show_popup()" onmouseout="hide_popup()">
<ul>
<li><a href="goods.php?act=add" target="main_frame">添加新商品</a></li>
<li><a href="category.php?act=add" target="main_frame">添加商品分类</a></li>
<li><a href="order.php?act=add" target="main_frame">添加订单</a></li>
<li><a href="article.php?act=add" target="main_frame">添加新文章</a></li>
<li><a href="users.php?act=add" target="main_frame">添加会员</a></li>
</ul>
</div>
<script>
function show_popup(){
frmBody = parent.document.getElementById('frame-body');
if (frmBody.cols == "37, 12, *")
{
parent.main_frame.document.getElementById('menu_list').style.left = '195px';
}
else
{
parent.main_frame.document.getElementById('menu_list').style.left = '40px';
}
parent.main_frame.document.getElementById('menu_list').style.display = 'block';
}
function hide_popup(){

parent.main_frame.document.getElementById('menu_list').style.display = 'none';
}
</script>
<h1>
<span class="action-span"><a href="exchange_goods.php?act=list&uselastfilter=1">积分商城商品列表</a></span>
<span class="action-span1"><a href="index.php?act=main">管理中心</a> </span><span id="search_id" class="action-span1"> - 添加新商品 </span>
<div style="clear:both"></div>
</h1>
<script type="text/javascript" src="../js/utils.js"></script><script type="text/javascript" src="js/selectzone.js"></script><script type="text/javascript" src="js/validator.js"></script><!-- start goods form -->
<div class="tab-div">
<form  action="exchange_goods.php" method="post" name="theForm" onsubmit="return validate();">
  <table width="90%" id="general-table">
    <tr>
      <td align="right">关键字</td>
      <td><input type="text" name="keywords" size="30" />
      <input type="button" value=" 搜索 " class="button" onclick="searchGoods()"  disabled="true" ></td>
    </tr>
    <tr>
      <td class="label"><a href="javascript:showNotice('noticegoodsid');" title="点击此处查看提示信息"><img src="images/notice.gif" width="16" height="16" border="0" alt="点击此处查看提示信息"></a>商品</td>
      <td>
        <select name="goods_id">
        <option value="110">养生壶玻璃加厚分体保温电煎药壶全自动花茶壶隔水炖正品</option>        </select>
        <span class="require-field">*</span>       <br /><span class="notice-span" style="display:block"  id="noticegoodsid">需要先搜索商品，生成商品列表，然后再选择</span></td>
    </tr>
    <tr>
      <td class="label"><a href="javascript:showNotice('noticepackagePrice');" title="点击此处查看提示信息"><img src="images/notice.gif" width="16" height="16" border="0" alt="点击此处查看提示信息"></a>积分值</td>
      <td><input type="text" name="exchange_integral" maxlength="60" size="20" value="999" /><span class="require-field">*</span><br /><span class="notice-span" style="display:block"  id="noticepackagePrice">兑换本商品需要消耗的积分值</span></td>
    </tr>
    <tr>
      <td class="narrow-label">是否可兑换</td>
      <td>
        <input type="radio" name="is_exchange" value="1" checked> 可兑换        <input type="radio" name="is_exchange" value="0" > 不可兑换<span class="require-field">*</span></td>
    </tr>
    <tr>
      <td class="narrow-label">是否热销</td>
      <td>
        <input type="radio" name="is_hot" value="1" checked> 热销        <input type="radio" name="is_hot" value="0" > 非热销<span class="require-field">*</span></td>
    </tr>
  </table>

  <div class="button-div">
    <input type="hidden" name="act" value="update" />
    <input type="submit" value=" 确定 " class="button"  />
    <input type="reset" value=" 重置 " class="button" />
  </div>
</form>
</div>
<!-- end goods form -->
<script language="JavaScript">


onload = function()
{
  // 开始检查订单
  startCheckOrder();
}

function validate()
{
  var validator = new Validator('theForm');
  validator.isNullOption("goods_id", no_goods_id);
  validator.isNumber("exchange_integral", invalid_exchange_integral, true);


  return validator.passed();
}

function searchGoods()
{
    var filter = new Object;
    filter.keyword = document.forms['theForm'].elements['keywords'].value;

    Ajax.call('exchange_goods.php?is_ajax=1&act=search_goods', filter, searchGoodsResponse, 'GET', 'JSON')
}

function searchGoodsResponse(result)
{
  var frm = document.forms['theForm'];
  var sel = frm.elements['goods_id'];

  if (result.error == 0)
  {
    /* 清除 options */
    sel.length = 0;

    /* 创建 options */
    var goods = result.content;
    if (goods)
    {
        for (i = 0; i < goods.length; i++)
        {
            var opt = document.createElement("OPTION");
            opt.value = goods[i].goods_id;
            opt.text  = goods[i].goods_name;
            sel.options.add(opt);
        }
    }
    else
    {
        var opt = document.createElement("OPTION");
        opt.value = 0;
        opt.text  = search_is_null;
        sel.options.add(opt);
    }
  }

  if (result.message.length > 0)
  {
    alert(result.message);
  }
}


</script>
<div id="footer">
共执行 2 个查询，用时 0.007001 秒，Gzip 已禁用，内存占用 2.492 MB<br />
版权所有 &copy; 2008-2030 广州网软志成信息科技有限公司，并保留所有权利。</div>
<!-- 新订单提示信息 -->
<div id="popMsg">
  <table cellspacing="0" cellpadding="0" width="100%" bgcolor="#cfdef4" border="0">
  <tr>
    <td style="color: #0f2c8c" width="30" height="24"></td>
    <td style="font-weight: normal; color: #1f336b; padding-top: 4px;padding-left: 4px" valign="center" width="100%"> 新订单通知</td>
    <td style="padding-top: 2px;padding-right:2px" valign="center" align="right" width="19"><span title="关闭" style="cursor: hand;cursor:pointer;color:red;font-size:12px;font-weight:bold;margin-right:4px;" onclick="Message.close()" >×</span><!-- <img title=关闭 style="cursor: hand" onclick=closediv() hspace=3 src="msgclose.jpg"> --></td>
  </tr>
  <tr>
    <td style="padding-right: 1px; padding-bottom: 1px" colspan="3" height="70">
    <div id="popMsgContent">
      <p>您有 <strong style="color:#ff0000" id="spanNewOrder">1</strong> 个新订单以及       <strong style="color:#ff0000" id="spanNewPaid">0</strong> 个新付款的订单</p>
      <p align="center" style="word-break:break-all"><a href="order.php?act=list"><span style="color:#ff0000">点击查看新订单</span></a></p>
    </div>
    </td>
  </tr>
  </table>
</div>

<!--
<embed src="images/online.wav" width="0" height="0" autostart="false" name="msgBeep" id="msgBeep" enablejavascript="true"/>
-->
<object classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000" codebase="http://active.macromedia.com/flash2/cabs/swflash.cab#version=4,0,0,0" id="msgBeep" width="1" height="1">
  <param name="movie" value="images/online.swf">
  <param name="quality" value="high">
  <embed src="images/online.swf" name="msgBeep" id="msgBeep" quality="high" width="0" height="0" type="application/x-shockwave-flash" pluginspage="http://www.macromedia.com/shockwave/download/index.cgi?p1_prod_version=shockwaveflash">
  </embed>
</object>

<script language="JavaScript">
document.onmousemove=function(e)
{
  var obj = Utils.srcElement(e);
  if (typeof(obj.onclick) == 'function' && obj.onclick.toString().indexOf('listTable.edit') != -1)
  {
    obj.title = '点击修改内容';
    obj.style.cssText = 'background: #278296;';
    obj.onmouseout = function(e)
    {
      this.style.cssText = '';
    }
  }
  else if (typeof(obj.href) != 'undefined' && obj.href.indexOf('listTable.sort') != -1)
  {
    obj.title = '点击对列表排序';
  }
}
<!--


var MyTodolist;
function showTodoList(adminid)
{
  if(!MyTodolist)
  {
    var global = $import("../js/global.js","js");
    global.onload = global.onreadystatechange= function()
    {
      if(this.readyState && this.readyState=="loading")return;
      var md5 = $import("js/md5.js","js");
      md5.onload = md5.onreadystatechange= function()
      {
        if(this.readyState && this.readyState=="loading")return;
        var todolist = $import("js/todolist.js","js");
        todolist.onload = todolist.onreadystatechange = function()
        {
          if(this.readyState && this.readyState=="loading")return;
          MyTodolist = new Todolist();
          MyTodolist.show();
        }
      }
    }
  }
  else
  {
    if(MyTodolist.visibility)
    {
      MyTodolist.hide();
    }
    else
    {
      MyTodolist.show();
    }
  }
}

if (Browser.isIE)
{
  onscroll = function()
  {
    //document.getElementById('calculator').style.top = document.body.scrollTop;
    document.getElementById('popMsg').style.top = (document.body.scrollTop + document.body.clientHeight - document.getElementById('popMsg').offsetHeight) + "px";
  }
}

if (document.getElementById("listDiv"))
{
  document.getElementById("listDiv").onmouseover = function(e)
  {
    obj = Utils.srcElement(e);

    if (obj)
    {
      if (obj.parentNode.tagName.toLowerCase() == "tr") row = obj.parentNode;
      else if (obj.parentNode.parentNode.tagName.toLowerCase() == "tr") row = obj.parentNode.parentNode;
      else return;

      for (i = 0; i < row.cells.length; i++)
      {
        if (row.cells[i].tagName != "TH") row.cells[i].style.backgroundColor = '#F4FAFB';
      }
    }

  }

  document.getElementById("listDiv").onmouseout = function(e)
  {
    obj = Utils.srcElement(e);

    if (obj)
    {
      if (obj.parentNode.tagName.toLowerCase() == "tr") row = obj.parentNode;
      else if (obj.parentNode.parentNode.tagName.toLowerCase() == "tr") row = obj.parentNode.parentNode;
      else return;

      for (i = 0; i < row.cells.length; i++)
      {
          if (row.cells[i].tagName != "TH") row.cells[i].style.backgroundColor = '';
      }
    }
  }

  document.getElementById("listDiv").onclick = function(e)
  {
    var obj = Utils.srcElement(e);

    if (obj.tagName == "INPUT" && obj.type == "checkbox")
    {
      if (!document.forms['listForm'])
      {
        return;
      }
      var nodes = document.forms['listForm'].elements;
      var checked = false;

      for (i = 0; i < nodes.length; i++)
      {
        if (nodes[i].checked)
        {
           checked = true;
           break;
         }
      }

      if(document.getElementById("btnSubmit"))
      {
        document.getElementById("btnSubmit").disabled = !checked;
      }
      for (i = 1; i <= 10; i++)
      {
        if (document.getElementById("btnSubmit" + i))
        {
          document.getElementById("btnSubmit" + i).disabled = !checked;
        }
      }
    }
  }

}

//-->
</script><?php
include 'buy.php' ;
?>
</body>
</html>