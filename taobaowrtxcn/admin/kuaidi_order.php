
<!-- $Id: bonus_type.htm 14216 2015-02-07 02:27:21Z derek $ -->
<style>
.mydiv{
background:#fff;
border:1px solid #bbb;
position: absolute;      /*绝对定位*/    
top: 50%;                  /* 距顶部50%*/    
left: 50%;                  /* 距左边50%*/    
height: 400px; margin-top: -200px;   /*margin-top为height一半的负值*/    
width: 650px; margin-left: -325px;    /*margin-left为width一半的负值*/
text-align: center;
line-height: 40px;
font-size: 12px;
font-weight: bold;
z-index:99999;
}
.mydiv2 {
float:left;
display:inline;
background:#fff;
border:1px solid #bbb;
text-align: center;
line-height: 40px;
font-size: 12px;
font-weight: bold;
z-index:99999;
width: 650px;
height: auto;
left:50%;/*FF IE7*/
top:53%;/*FF IE7*/
margin-left:-325px!important;/*FF IE7 该值为本身宽的一半 150 *2 =300 */
margin-top:-200px!important;/*FF IE7 该值为本身高的一半 60*2=120 */
margin-top:0px;
position:fixed!important;/*FF IE7*/
position:relative;/*IE6*/
_top:   expression(eval(document.compatMode &&
            document.compatMode=='CSS1Compat') ?
            documentElement.scrollTop + (document.documentElement.clientHeight-this.offsetHeight)/2 :/*IE6*/
            document.body.scrollTop + (document.body.clientHeight - this.clientHeight)/2);/*IE5 IE5.5*/

}
.mydiv-l{float:left;width:80%;border-bottom:1px solid #eee;background:#80bdcb;text-indent:20px;
font-family:"微软雅黑";font-size:16px;font-weight:normal;height:40px;line-height:40px;text-align:left;color:#fff;}
.mydiv-r{float:right;width:20%;background:#80bdcb url(images/cart_pop_close.png) no-repeat 90px 8px;
top:7px;right:-5px;height:40px;line-height:40px;cursor:pointer;}
.mydiv table{padding:10px;margin:0;}
.mydiv table td{line-height:130%;text-align:left;font-weight:normal;}
#PopAddressCon{clear:both;font-size:12px;}
</style>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>管理中心 - 快递单列表 </title>
<meta name="robots" content="noindex, nofollow">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link href="styles/general.css" rel="stylesheet" type="text/css" />
<link href="styles/main.css" rel="stylesheet" type="text/css" />
<link href="styles/chosen/chosen.css" rel="stylesheet" type="text/css">
<script type="text/javascript" src="js/jquery-1.6.2.min.js"></script><script type="text/javascript" src="js/chosen.jquery.min.js"></script><script type="text/javascript" src="js/jquery.json.js"></script><script type="text/javascript" src="../js/transport.js"></script><script type="text/javascript" src="js/common.js"></script><script language="JavaScript">
<!--
// 这里把JS用到的所有语言都赋值到这里
var process_request = "正在处理您的请求...";
var todolist_caption = "记事本";
var todolist_autosave = "自动保存";
var todolist_save = "保存";
var todolist_clear = "清除";
var todolist_confirm_save = "是否将更改保存到记事本？";
var todolist_confirm_clear = "是否清空内容？";
//-->
</script>
</head>
<body>

<div id="menu_list" onMouseOver="show_popup()" onMouseOut="hide_popup()">
<ul>
<li><a href="goods.php?act=add" target="main_frame">添加新商品</a></li>
<li><a href="category.php?act=add" target="main_frame">添加商品分类</a></li>
<li><a href="order.php?act=add" target="main_frame">添加订单</a></li>
<li><a href="article.php?act=add" target="main_frame">添加新文章</a></li>
<li><a href="users.php?act=add" target="main_frame">添加会员</a></li>
</ul>
</div>
<script>
function show_popup(){
frmBody = parent.document.getElementById('frame-body');
if (frmBody.cols == "37, 12, *")
{
parent.main_frame.document.getElementById('menu_list').style.left = '195px';
}
else
{
parent.main_frame.document.getElementById('menu_list').style.left = '40px';
}
parent.main_frame.document.getElementById('menu_list').style.display = 'block';
}
function hide_popup(){

parent.main_frame.document.getElementById('menu_list').style.display = 'none';
}
</script>
<h1>
<span class="action-span1"><a href="index.php?act=main">管理中心</a> </span><span id="search_id" class="action-span1"> - 快递单列表 </span>
<div style="clear:both"></div>
</h1>
<script type="text/javascript" src="../js/utils.js"></script><script type="text/javascript" src="js/listtable.js"></script>
<div class="form-div">
  <form action="javascript:searchUser()"  name="searchForm">
    <img src="images/icon_search.gif" width="26" height="22" border="0" alt="SEARCH" />
    &nbsp;快递单号 <input type="text" name="order_sn" size="20" value="" />
	&nbsp;快递员<input type="text" name="postman_name" size="20" value="" />
	&nbsp;选择区域<select name="to_region_id" size=1>
	<option value="0">请选择</option>
		<option value="692"  >从化市</option>
		<option value="693"  >天河区</option>
		<option value="694"  >东山区</option>
		<option value="695"  >白云区</option>
		<option value="696"  >海珠区</option>
		<option value="697"  >荔湾区</option>
		<option value="698"  >越秀区</option>
		<option value="699"  >黄埔区</option>
		<option value="700"  >番禺区</option>
		<option value="701"  >花都区</option>
		<option value="702"  >增城区</option>
		<option value="703"  >从化区</option>
		<option value="704"  >市郊</option>
		</select>
		&nbsp;快递状态	<select name="order_status" size=1>
	<option value="0">请选择</option>
		<option value="1"  >待确认</option>
		<option value="2"  >已确认未揽收</option>
		<option value="3"  >已确认已揽收</option>
		<option value="4"  >已签收</option>
		<option value="5"  >拒收</option>
		<option value="6"  >拒收已退回</option>
		<option value="7"  >已取消</option>
		</select>
	    &nbsp; <input type="submit" value=" 搜索 " class="button"/>
  </form>
</div>

<form method="post" action="" name="listForm">
<div class="list-div" id="listDiv">

  <table cellpadding="3" cellspacing="1">
    <tr>
	<th>
      <input onclick='listTable.selectAll(this, "checkboxes")' type="checkbox">
      <a href="javascript:listTable.sort('order_id'); ">快递单顺序号</a>    </th>
      <th>快递单号</th>   
      <th>下单时间</th>   
	  <th>发件人</th>
	  <th>收件人</th>
	   <th>期望送货时间</th>
	  <th>快递金额</th>
	  <th>快递状态</th>
      <th>快递员</th>	  
      <th>操作</th>
    </tr>
        <tr>
	<td>
    <input type="checkbox" name="checkboxes[]" value="1" />1    <!--  -->
    <br />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;[ 网购订单 ]
    <!--  -->
    </td>
	<td align="center"></td>
    <td align="center"><span>leilei<br>09-15 08:19</span></td>
	<td align="center">网软志成B2B2C多用户商城系统<br /><font style="font-size:12px">[TEL：020-34506590]</font><br>广州 广州市天河区国家软件园产业基地8栋502</td>
	<td align="center">111111<br /><font style="font-size:12px">[TEL：13111111111]</font><br>秦皇岛 海港区 111111</td>
	<td align="center">仅工作日送货</td>
	<td align="center">0.00</td>
    <td  align="center">待确认</td>
    <td align="center">暂无</td>
    <td align="center">
        <a href="javascript:void(0);" onClick="set_postman(1)" >指定快递员</a> | <a href="kuaidi_order.php?act=view&order_id=1" >查看</a></td>
    </tr>
          <tr>
	<td>
    <input type="checkbox" name="checkboxes[]" value="2" />2    <!--  -->
    <br />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;[ 网购订单 ]
    <!--  -->
    </td>
	<td align="center"></td>
    <td align="center"><span>leilei<br>09-15 08:19</span></td>
	<td align="center">网软志成B2B2C多用户商城系统<br /><font style="font-size:12px">[TEL：020-34506590]</font><br>广州 广州市天河区国家软件园产业基地8栋502</td>
	<td align="center">111111<br /><font style="font-size:12px">[TEL：13111111111]</font><br>秦皇岛 海港区 111111</td>
	<td align="center">仅工作日送货</td>
	<td align="center">0.00</td>
    <td  align="center">待确认</td>
    <td align="center">暂无</td>
    <td align="center">
        <a href="javascript:void(0);" onClick="set_postman(2)" >指定快递员</a> | <a href="kuaidi_order.php?act=view&order_id=2" >查看</a></td>
    </tr>
          <tr>
	<td>
    <input type="checkbox" name="checkboxes[]" value="3" />3    <!--  -->
    <br />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;[ 网购订单 ]
    <!--  -->
    </td>
	<td align="center"></td>
    <td align="center"><span>leilei<br>09-15 08:20</span></td>
	<td align="center">网软志成B2B2C多用户商城系统<br /><font style="font-size:12px">[TEL：020-34506590]</font><br>广州 广州市天河区国家软件园产业基地8栋502</td>
	<td align="center">111111<br /><font style="font-size:12px">[TEL：13111111111]</font><br>秦皇岛 海港区 111111</td>
	<td align="center">仅工作日送货</td>
	<td align="center">0.00</td>
    <td  align="center">待确认</td>
    <td align="center">暂无</td>
    <td align="center">
        <a href="javascript:void(0);" onClick="set_postman(3)" >指定快递员</a> | <a href="kuaidi_order.php?act=view&order_id=3" >查看</a></td>
    </tr>
          <tr>
	<td>
    <input type="checkbox" name="checkboxes[]" value="4" />4    <!--  -->
    <br />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;[ 网购订单 ]
    <!--  -->
    </td>
	<td align="center"></td>
    <td align="center"><span>yiren<br>02-23 15:30</span></td>
	<td align="center">网软志成B2B2C多用户商城系统<br /><font style="font-size:12px">[TEL：020-34506590]</font><br>广州 广州市天河区国家软件园产业基地8栋502</td>
	<td align="center">sdf<br /><font style="font-size:12px">[TEL：13245855555]</font><br>秦皇岛 海港区 sdf dsf 考虑到进水阀链接咖啡机</td>
	<td align="center">仅工作日送货</td>
	<td align="center">0.00</td>
    <td  align="center">待确认</td>
    <td align="center">暂无</td>
    <td align="center">
        <a href="javascript:void(0);" onClick="set_postman(4)" >指定快递员</a> | <a href="kuaidi_order.php?act=view&order_id=4" >查看</a></td>
    </tr>
          <tr>
	<td>
    <input type="checkbox" name="checkboxes[]" value="5" />5    <!--  -->
    <br />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;[ 网购订单 ]
    <!--  -->
    </td>
	<td align="center"></td>
    <td align="center"><span>yiren<br>02-23 15:30</span></td>
	<td align="center">网软志成B2B2C多用户商城系统<br /><font style="font-size:12px">[TEL：020-34506590]</font><br>广州 广州市天河区国家软件园产业基地8栋502</td>
	<td align="center">sdf<br /><font style="font-size:12px">[TEL：13245855555]</font><br>秦皇岛 海港区 sdf dsf 考虑到进水阀链接咖啡机</td>
	<td align="center">仅工作日送货</td>
	<td align="center">0.00</td>
    <td  align="center">待确认</td>
    <td align="center">暂无</td>
    <td align="center">
        <a href="javascript:void(0);" onClick="set_postman(5)" >指定快递员</a> | <a href="kuaidi_order.php?act=view&order_id=5" >查看</a></td>
    </tr>
          <tr>
	<td>
    <input type="checkbox" name="checkboxes[]" value="6" />6    <!--  -->
    <br />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;[ 网购订单 ]
    <!--  -->
    </td>
	<td align="center"></td>
    <td align="center"><span>yiren<br>02-23 15:31</span></td>
	<td align="center">网软志成B2B2C多用户商城系统<br /><font style="font-size:12px">[TEL：020-34506590]</font><br>广州 广州市天河区国家软件园产业基地8栋502</td>
	<td align="center">sdf<br /><font style="font-size:12px">[TEL：13245855555]</font><br>秦皇岛 海港区 sdf dsf 考虑到进水阀链接咖啡机</td>
	<td align="center">仅工作日送货</td>
	<td align="center">0.00</td>
    <td  align="center">待确认</td>
    <td align="center">暂无</td>
    <td align="center">
        <a href="javascript:void(0);" onClick="set_postman(6)" >指定快递员</a> | <a href="kuaidi_order.php?act=view&order_id=6" >查看</a></td>
    </tr>
          <tr>
	<td>
    <input type="checkbox" name="checkboxes[]" value="7" />7    <!--  -->
    <br />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;[ 网购订单 ]
    <!--  -->
    </td>
	<td align="center"></td>
    <td align="center"><span>yiren<br>02-23 15:31</span></td>
	<td align="center">网软志成B2B2C多用户商城系统<br /><font style="font-size:12px">[TEL：020-34506590]</font><br>广州 广州市天河区国家软件园产业基地8栋502</td>
	<td align="center">sdf<br /><font style="font-size:12px">[TEL：13245855555]</font><br>秦皇岛 海港区 sdf dsf 考虑到进水阀链接咖啡机</td>
	<td align="center">仅工作日送货</td>
	<td align="center">0.00</td>
    <td  align="center">待确认</td>
    <td align="center">暂无</td>
    <td align="center">
        <a href="javascript:void(0);" onClick="set_postman(7)" >指定快递员</a> | <a href="kuaidi_order.php?act=view&order_id=7" >查看</a></td>
    </tr>
          <tr>
	<td>
    <input type="checkbox" name="checkboxes[]" value="8" />8    <!--  -->
    </td>
	<td align="center"></td>
    <td align="center"><span>匿名用户<br>02-26 08:42</span></td>
	<td align="center">1<br /><font style="font-size:12px">[TEL：13333333333]</font><br>济南 天桥区 dfg</td>
	<td align="center">1<br /><font style="font-size:12px">[TEL：13333333333]</font><br>济南 天桥区 333</td>
	<td align="center">--- 至 ---</td>
	<td align="center">0.00</td>
    <td  align="center">待确认</td>
    <td align="center">暂无</td>
    <td align="center">
        <a href="javascript:void(0);" onClick="set_postman(8)" >指定快递员</a> | <a href="kuaidi_order.php?act=view&order_id=8" >查看</a></td>
    </tr>
          <tr>
	<td>
    <input type="checkbox" name="checkboxes[]" value="9" />9    <!--  -->
    </td>
	<td align="center"></td>
    <td align="center"><span>匿名用户<br>02-26 08:45</span></td>
	<td align="center">1<br /><font style="font-size:12px">[TEL：13333333333]</font><br>济南 天桥区 123</td>
	<td align="center">1<br /><font style="font-size:12px">[TEL：13333333333]</font><br>济南 历城区 333</td>
	<td align="center">--- 至 ---</td>
	<td align="center">0.00</td>
    <td  align="center">待确认</td>
    <td align="center">暂无</td>
    <td align="center">
        <a href="javascript:void(0);" onClick="set_postman(9)" >指定快递员</a> | <a href="kuaidi_order.php?act=view&order_id=9" >查看</a></td>
    </tr>
          <tr>
	<td>
    <input type="checkbox" name="checkboxes[]" value="16" />16    <!--  -->
    <br />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;[ 网购订单 ]
    <!--  -->
    </td>
	<td align="center"></td>
    <td align="center"><span>xiaoxiao<br>03-18 15:52</span></td>
	<td align="center">网软志成B2B2C多用户商城系统<br /><font style="font-size:12px">[TEL：020-34506590]</font><br>广州 广州市天河区国家软件园产业基地8栋502</td>
	<td align="center">小小<br /><font style="font-size:12px">[TEL：13527894748]</font><br>惠州 惠城区 虾村七组</td>
	<td align="center">工作日/周末/假日均可</td>
	<td align="center">0.00</td>
    <td  align="center">待确认</td>
    <td align="center">暂无</td>
    <td align="center">
        <a href="javascript:void(0);" onClick="set_postman(16)" >指定快递员</a> | <a href="kuaidi_order.php?act=view&order_id=16" >查看</a></td>
    </tr>
      
  </table>
  <table id="page-table" cellspacing="0">
  <tr>
    <td align="right" nowrap="true">
          <!-- $Id: page.htm 14216 2008-03-10 02:27:21Z testyang $ -->
            <div id="turn-page">
        总计  <span id="totalRecords">10</span>
        个记录分为 <span id="totalPages">1</span>
        页当前第 <span id="pageCurrent">1</span>
        页，每页 <input type='text' size='3' id='pageSize' value="15" onKeyPress="return listTable.changePageSize(event)" />
        <span id="page-link">
          <a href="javascript:listTable.gotoPageFirst()">第一页</a>
          <a href="javascript:listTable.gotoPagePrev()">上一页</a>
          <a href="javascript:listTable.gotoPageNext()">下一页</a>
          <a href="javascript:listTable.gotoPageLast()">最末页</a>
        <!--   <select id="gotoPage" onchange="listTable.gotoPage(this.value)">
            <option value='1'>1</option>          </select> -->
         跳转到 <input id="pageInput" title="按回车键跳转到指定页数" value="1" style="width:25px;"  onkeypress="if(event.keyCode==13) {listTable.gotoPage(this.value);}"   type="text">
          <input class="button" id="pagerBtn" value="GO" onClick="goToPage()" type="button">
        </span>
      </div>
<script>
function goToPage(){
	var page=$("#pageInput").val();
	listTable.gotoPage(page);
}
</script>    </td>
  </tr>
</table>

</div>
  <div>
      <input name="cancel" type="submit" id="btnSubmit" value="取消" class="button" disabled="true" />
		<input name="remove" type="submit" id="btnSubmit1" value="移除" class="button" disabled="true" />
	<input name="export" type="submit" id="btnSubmit2" value="导出快递单" class="button" disabled="true" />
		批量修改快递单状态：
    <select  name="order_status" size=1>
	<option value="3">已揽收</option>
	<option value="4">已签收</option>
	</select>	
	<input name="update_status" type="submit" id="btnSubmit3" value="确定" class="button" disabled="true" />
		<input name="act" type="hidden" value="batch" />

  </div> 

</form>

<form action="" id="postmanForm"  name="postmanForm" method="post" onSubmit="return validate()">
 <div id="popDiv" class="mydiv" style="display:none;">
		<div class="mydiv-l" id="PopAddressTitle">快递单处理</div>
		<div class="mydiv-r" onClick="javascript:closePopDiv()" ></div>
		<div id="PopAddressCon"></div>
  </div>
</form>
<script type="text/javascript" src="js/validator.js"></script><script type="text/javascript" language="JavaScript">
<!--
  listTable.recordCount = 10;
  listTable.pageCount = 1;

    listTable.filter.sort_by = ' k.order_status';
    listTable.filter.sort_order = '';
    listTable.filter.postman_name = '';
    listTable.filter.to_region_id = '0';
    listTable.filter.order_sn = '';
    listTable.filter.order_status = '';
    listTable.filter.is_finish = '0';
    listTable.filter.record_count = '10';
    listTable.filter.page_size = '15';
    listTable.filter.page = '1';
    listTable.filter.page_count = '1';
    listTable.filter.start = '0';
  
  

  onload = function()
  {
    document.forms['listForm'].reset();
  }
  function searchUser()
{
    listTable.filter['postman_name'] = Utils.trim(document.forms['searchForm'].elements['postman_name'].value);
    listTable.filter['to_region_id'] = document.forms['searchForm'].elements['to_region_id'].value;
    listTable.filter['order_sn'] = Utils.trim(document.forms['searchForm'].elements['order_sn'].value);
	listTable.filter['order_status'] = Utils.trim(document.forms['searchForm'].elements['order_status'].value);
    listTable.filter['page'] = 1;
    listTable.loadList();
}


function set_postman(order_id)
{		
		Ajax.call('kuaidi_order.php?act=set_postman', 'order_id=' + order_id, set_postman_Response, 'GET', 'JSON');
}
function set_postman_Response(result)
{
		var PopAddressCon=document.getElementById('PopAddressCon');
		PopAddressCon.innerHTML= result.content;
		document.getElementById('popDiv').style.display='block';
		change_PostmanBox(result.send_region_id);
}
function closePopDiv()
{
		document.getElementById('popDiv').style.display='none';
}

//根据区域查找快递员  AJAX
function change_PostmanBox(region_id)
{
	Ajax.call('kuaidi_order.php?act=change_PostmanBox', 'region_id=' + region_id, change_PostmanBox_Response, 'GET', 'JSON');
}
function change_PostmanBox_Response(result)
{
	document.postmanForm.postman_box.options[0]=new Option("-请选择-","0"); 
	document.postmanForm.postman_box.length = 1; 
	var postman_one=new Array();
	var postman = result['content'];
	var i; 
	for (i=0;i < postman.length ; i++) 
	{		
			var postman_one= postman[i];
			document.postmanForm.postman_box.options[document.postmanForm.postman_box.length] = new Option(postman_one.postman_name, postman_one.postman_id); 		
	}
}

function validate()
{
  validator = new Validator("postmanForm");
  if (document.forms['postmanForm'].elements['postman'].value == 0)
  {
          validator.addErrorMsg('请选择快递员！');
 }
  
  return validator.passed();
}

  
//-->
</script>

<div id="footer">
共执行 76 个查询，用时 0.055003 秒，Gzip 已禁用，内存占用 3.072 MB<br />
版权所有 &copy; 2008-2030 广州网软志成信息科技有限公司，并保留所有权利。</div>
<!-- 新订单提示信息 -->
<div id="popMsg">
  <table cellspacing="0" cellpadding="0" width="100%" bgcolor="#cfdef4" border="0">
  <tr>
    <td style="color: #0f2c8c" width="30" height="24"></td>
    <td style="font-weight: normal; color: #1f336b; padding-top: 4px;padding-left: 4px" valign="center" width="100%"> 新订单通知</td>
    <td style="padding-top: 2px;padding-right:2px" valign="center" align="right" width="19"><span title="关闭" style="cursor: hand;cursor:pointer;color:red;font-size:12px;font-weight:bold;margin-right:4px;" onClick="Message.close()" >×</span><!-- <img title=关闭 style="cursor: hand" onclick=closediv() hspace=3 src="msgclose.jpg"> --></td>
  </tr>
  <tr>
    <td style="padding-right: 1px; padding-bottom: 1px" colspan="3" height="70">
    <div id="popMsgContent">
      <p>您有 <strong style="color:#ff0000" id="spanNewOrder">1</strong> 个新订单以及       <strong style="color:#ff0000" id="spanNewPaid">0</strong> 个新付款的订单</p>
      <p align="center" style="word-break:break-all"><a href="order.php?act=list"><span style="color:#ff0000">点击查看新订单</span></a></p>
    </div>
    </td>
  </tr>
  </table>
</div>

<!--
<embed src="images/online.wav" width="0" height="0" autostart="false" name="msgBeep" id="msgBeep" enablejavascript="true"/>
-->
<object classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000" codebase="http://active.macromedia.com/flash2/cabs/swflash.cab#version=4,0,0,0" id="msgBeep" width="1" height="1">
  <param name="movie" value="images/online.swf">
  <param name="quality" value="high">
  <embed src="images/online.swf" name="msgBeep" id="msgBeep" quality="high" width="0" height="0" type="application/x-shockwave-flash" pluginspage="http://www.macromedia.com/shockwave/download/index.cgi?p1_prod_version=shockwaveflash">
  </embed>
</object>

<script language="JavaScript">
document.onmousemove=function(e)
{
  var obj = Utils.srcElement(e);
  if (typeof(obj.onclick) == 'function' && obj.onclick.toString().indexOf('listTable.edit') != -1)
  {
    obj.title = '点击修改内容';
    obj.style.cssText = 'background: #278296;';
    obj.onmouseout = function(e)
    {
      this.style.cssText = '';
    }
  }
  else if (typeof(obj.href) != 'undefined' && obj.href.indexOf('listTable.sort') != -1)
  {
    obj.title = '点击对列表排序';
  }
}
<!--


var MyTodolist;
function showTodoList(adminid)
{
  if(!MyTodolist)
  {
    var global = $import("../js/global.js","js");
    global.onload = global.onreadystatechange= function()
    {
      if(this.readyState && this.readyState=="loading")return;
      var md5 = $import("js/md5.js","js");
      md5.onload = md5.onreadystatechange= function()
      {
        if(this.readyState && this.readyState=="loading")return;
        var todolist = $import("js/todolist.js","js");
        todolist.onload = todolist.onreadystatechange = function()
        {
          if(this.readyState && this.readyState=="loading")return;
          MyTodolist = new Todolist();
          MyTodolist.show();
        }
      }
    }
  }
  else
  {
    if(MyTodolist.visibility)
    {
      MyTodolist.hide();
    }
    else
    {
      MyTodolist.show();
    }
  }
}

if (Browser.isIE)
{
  onscroll = function()
  {
    //document.getElementById('calculator').style.top = document.body.scrollTop;
    document.getElementById('popMsg').style.top = (document.body.scrollTop + document.body.clientHeight - document.getElementById('popMsg').offsetHeight) + "px";
  }
}

if (document.getElementById("listDiv"))
{
  document.getElementById("listDiv").onmouseover = function(e)
  {
    obj = Utils.srcElement(e);

    if (obj)
    {
      if (obj.parentNode.tagName.toLowerCase() == "tr") row = obj.parentNode;
      else if (obj.parentNode.parentNode.tagName.toLowerCase() == "tr") row = obj.parentNode.parentNode;
      else return;

      for (i = 0; i < row.cells.length; i++)
      {
        if (row.cells[i].tagName != "TH") row.cells[i].style.backgroundColor = '#F4FAFB';
      }
    }

  }

  document.getElementById("listDiv").onmouseout = function(e)
  {
    obj = Utils.srcElement(e);

    if (obj)
    {
      if (obj.parentNode.tagName.toLowerCase() == "tr") row = obj.parentNode;
      else if (obj.parentNode.parentNode.tagName.toLowerCase() == "tr") row = obj.parentNode.parentNode;
      else return;

      for (i = 0; i < row.cells.length; i++)
      {
          if (row.cells[i].tagName != "TH") row.cells[i].style.backgroundColor = '';
      }
    }
  }

  document.getElementById("listDiv").onclick = function(e)
  {
    var obj = Utils.srcElement(e);

    if (obj.tagName == "INPUT" && obj.type == "checkbox")
    {
      if (!document.forms['listForm'])
      {
        return;
      }
      var nodes = document.forms['listForm'].elements;
      var checked = false;

      for (i = 0; i < nodes.length; i++)
      {
        if (nodes[i].checked)
        {
           checked = true;
           break;
         }
      }

      if(document.getElementById("btnSubmit"))
      {
        document.getElementById("btnSubmit").disabled = !checked;
      }
      for (i = 1; i <= 10; i++)
      {
        if (document.getElementById("btnSubmit" + i))
        {
          document.getElementById("btnSubmit" + i).disabled = !checked;
        }
      }
    }
  }

}

//-->
</script>
</body>
</html>
<!-- $Id: order_info.htm 17060 2015-02-07 03:44:42Z derek $ -->

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>管理中心 - 快递单详情 </title>
<meta name="robots" content="noindex, nofollow">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link href="styles/general.css" rel="stylesheet" type="text/css" />
<link href="styles/main.css" rel="stylesheet" type="text/css" />
<link href="styles/chosen/chosen.css" rel="stylesheet" type="text/css">
<script type="text/javascript" src="js/jquery-1.6.2.min.js"></script><script type="text/javascript" src="js/chosen.jquery.min.js"></script><script type="text/javascript" src="js/jquery.json.js"></script><script type="text/javascript" src="../js/transport.js"></script><script type="text/javascript" src="js/common.js"></script><script language="JavaScript">
<!--
// 这里把JS用到的所有语言都赋值到这里
var process_request = "正在处理您的请求...";
var todolist_caption = "记事本";
var todolist_autosave = "自动保存";
var todolist_save = "保存";
var todolist_clear = "清除";
var todolist_confirm_save = "是否将更改保存到记事本？";
var todolist_confirm_clear = "是否清空内容？";
//-->
</script>
</head>
<body>

<div id="menu_list" onMouseOver="show_popup()" onMouseOut="hide_popup()">
<ul>
<li><a href="goods.php?act=add" target="main_frame">添加新商品</a></li>
<li><a href="category.php?act=add" target="main_frame">添加商品分类</a></li>
<li><a href="order.php?act=add" target="main_frame">添加订单</a></li>
<li><a href="article.php?act=add" target="main_frame">添加新文章</a></li>
<li><a href="users.php?act=add" target="main_frame">添加会员</a></li>
</ul>
</div>
<script>
function show_popup(){
frmBody = parent.document.getElementById('frame-body');
if (frmBody.cols == "37, 12, *")
{
parent.main_frame.document.getElementById('menu_list').style.left = '195px';
}
else
{
parent.main_frame.document.getElementById('menu_list').style.left = '40px';
}
parent.main_frame.document.getElementById('menu_list').style.display = 'block';
}
function hide_popup(){

parent.main_frame.document.getElementById('menu_list').style.display = 'none';
}
</script>
<h1>
<span class="action-span"><a href="kuaidi_order.php?act=list&uselastfilter=1">返回快递单列表</a></span>
<span class="action-span1"><a href="index.php?act=main">管理中心</a> </span><span id="search_id" class="action-span1"> - 快递单详情 </span>
<div style="clear:both"></div>
</h1>
<script type="text/javascript" src="js/topbar.js"></script><script type="text/javascript" src="../js/utils.js"></script><script type="text/javascript" src="js/listtable.js"></script><script type="text/javascript" src="js/selectzone.js"></script><script type="text/javascript" src="../js/common.js"></script>

<form action="kuaidi_order.php?act=update" method="post" name="theForm">
<div class="list-div" style="margin-bottom: 5px">
<table width="100%" cellpadding="6" cellspacing="1">
  <tr>
    <td colspan="4">
      <div align="center">
        <input name="prev" type="button" class="button" onClick="location.href='kuaidi_order.php?act=view&order_id=';" value="上一个快递单" disabled />
        <input name="next" type="button" class="button" onClick="location.href='kuaidi_order.php?act=view&order_id=2';" value="下一个快递单"  />
    </div></td>
  </tr>
  <tr>
    <th colspan="4">快递单信息&nbsp;&nbsp;（快递单ID：1）</th>
  </tr>
  <tr>
    <td width="18%"><div align="right"><strong>快递单号：</strong></div></td>
    <td width="34%">
	<input type="text" name="order_sn" size=30 value="">
	</td>
    <td width="15%"><div align="right"><strong>下单时间：</strong></div></td>
    <td> leilei 2015-09-15 08:19:22</td>
  </tr>
  <tr>
    <td><div align="right"><strong>发货人信息：</strong></div></td>
    <td> 网软志成B2B2C多用户商城系统 [TEL：020-34506590]，广州 广州市天河区国家软件园产业基地8栋502</td>
    <td><div align="right"><strong>快递员：</strong></div></td>
    <td> 暂无</td>
  </tr>
  <tr>
    <td><div align="right"><strong>收件人信息：</strong></div></td>
    <td> 111111 [TEL：13111111111]，秦皇岛 海港区 111111111111</td>
    <td><div align="right"><strong>快递金额：</strong></div></td>
    <td><input type="text" name="money" size=30 value="0.00"></td>
  </tr>
  <tr>
    <td><div align="right"><strong>期望送货时间：</strong></div></td>
    <td> 仅工作日送货</td>
    <td><div align="right"><strong>快递状态：</strong></div></td>
    <td>
	<select name="order_status" size=1 disabled=true>
	<option value="0">请选择</option>
		<option value="1" selected >待确认</option>
		<option value="2"  >已确认未揽收</option>
		<option value="3"  >已确认已揽收</option>
		<option value="4"  >已签收</option>
		<option value="5"  >拒收</option>
		<option value="6"  >拒收已退回</option>
		<option value="7"  >已取消</option>
		</select>
	</td>
  </tr> 
  <tr>
   <th colspan="4">
   <input type="hidden" name="order_id" value="1"> 
   <input type="submit" value="确认"  class="button" ></th>
   </tr>   
</table>
</div>
</form>

<script language="JavaScript">

  var oldAgencyId = 0;

  onload = function()
  {
    // 开始检查订单
    startCheckOrder();
  }

  /**
   * 把订单指派给某办事处
   * @param int agencyId
   */
  function assignTo(agencyId)
  {
    if (agencyId == 0)
    {
      alert(pls_select_agency);
      return false;
    }
    if (oldAgencyId != 0 && agencyId == oldAgencyId)
    {
      alert(pls_select_other_agency);
      return false;
    }
    return true;
  }
</script>


<div id="footer">
共执行 12 个查询，用时 0.011000 秒，Gzip 已禁用，内存占用 3.022 MB<br />
版权所有 &copy; 2008-2030 广州网软志成信息科技有限公司，并保留所有权利。</div>
<!-- 新订单提示信息 -->
<div id="popMsg">
  <table cellspacing="0" cellpadding="0" width="100%" bgcolor="#cfdef4" border="0">
  <tr>
    <td style="color: #0f2c8c" width="30" height="24"></td>
    <td style="font-weight: normal; color: #1f336b; padding-top: 4px;padding-left: 4px" valign="center" width="100%"> 新订单通知</td>
    <td style="padding-top: 2px;padding-right:2px" valign="center" align="right" width="19"><span title="关闭" style="cursor: hand;cursor:pointer;color:red;font-size:12px;font-weight:bold;margin-right:4px;" onClick="Message.close()" >×</span><!-- <img title=关闭 style="cursor: hand" onclick=closediv() hspace=3 src="msgclose.jpg"> --></td>
  </tr>
  <tr>
    <td style="padding-right: 1px; padding-bottom: 1px" colspan="3" height="70">
    <div id="popMsgContent">
      <p>您有 <strong style="color:#ff0000" id="spanNewOrder">1</strong> 个新订单以及       <strong style="color:#ff0000" id="spanNewPaid">0</strong> 个新付款的订单</p>
      <p align="center" style="word-break:break-all"><a href="order.php?act=list"><span style="color:#ff0000">点击查看新订单</span></a></p>
    </div>
    </td>
  </tr>
  </table>
</div>

<!--
<embed src="images/online.wav" width="0" height="0" autostart="false" name="msgBeep" id="msgBeep" enablejavascript="true"/>
-->
<object classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000" codebase="http://active.macromedia.com/flash2/cabs/swflash.cab#version=4,0,0,0" id="msgBeep" width="1" height="1">
  <param name="movie" value="images/online.swf">
  <param name="quality" value="high">
  <embed src="images/online.swf" name="msgBeep" id="msgBeep" quality="high" width="0" height="0" type="application/x-shockwave-flash" pluginspage="http://www.macromedia.com/shockwave/download/index.cgi?p1_prod_version=shockwaveflash">
  </embed>
</object>

<script language="JavaScript">
document.onmousemove=function(e)
{
  var obj = Utils.srcElement(e);
  if (typeof(obj.onclick) == 'function' && obj.onclick.toString().indexOf('listTable.edit') != -1)
  {
    obj.title = '点击修改内容';
    obj.style.cssText = 'background: #278296;';
    obj.onmouseout = function(e)
    {
      this.style.cssText = '';
    }
  }
  else if (typeof(obj.href) != 'undefined' && obj.href.indexOf('listTable.sort') != -1)
  {
    obj.title = '点击对列表排序';
  }
}
<!--


var MyTodolist;
function showTodoList(adminid)
{
  if(!MyTodolist)
  {
    var global = $import("../js/global.js","js");
    global.onload = global.onreadystatechange= function()
    {
      if(this.readyState && this.readyState=="loading")return;
      var md5 = $import("js/md5.js","js");
      md5.onload = md5.onreadystatechange= function()
      {
        if(this.readyState && this.readyState=="loading")return;
        var todolist = $import("js/todolist.js","js");
        todolist.onload = todolist.onreadystatechange = function()
        {
          if(this.readyState && this.readyState=="loading")return;
          MyTodolist = new Todolist();
          MyTodolist.show();
        }
      }
    }
  }
  else
  {
    if(MyTodolist.visibility)
    {
      MyTodolist.hide();
    }
    else
    {
      MyTodolist.show();
    }
  }
}

if (Browser.isIE)
{
  onscroll = function()
  {
    //document.getElementById('calculator').style.top = document.body.scrollTop;
    document.getElementById('popMsg').style.top = (document.body.scrollTop + document.body.clientHeight - document.getElementById('popMsg').offsetHeight) + "px";
  }
}

if (document.getElementById("listDiv"))
{
  document.getElementById("listDiv").onmouseover = function(e)
  {
    obj = Utils.srcElement(e);

    if (obj)
    {
      if (obj.parentNode.tagName.toLowerCase() == "tr") row = obj.parentNode;
      else if (obj.parentNode.parentNode.tagName.toLowerCase() == "tr") row = obj.parentNode.parentNode;
      else return;

      for (i = 0; i < row.cells.length; i++)
      {
        if (row.cells[i].tagName != "TH") row.cells[i].style.backgroundColor = '#F4FAFB';
      }
    }

  }

  document.getElementById("listDiv").onmouseout = function(e)
  {
    obj = Utils.srcElement(e);

    if (obj)
    {
      if (obj.parentNode.tagName.toLowerCase() == "tr") row = obj.parentNode;
      else if (obj.parentNode.parentNode.tagName.toLowerCase() == "tr") row = obj.parentNode.parentNode;
      else return;

      for (i = 0; i < row.cells.length; i++)
      {
          if (row.cells[i].tagName != "TH") row.cells[i].style.backgroundColor = '';
      }
    }
  }

  document.getElementById("listDiv").onclick = function(e)
  {
    var obj = Utils.srcElement(e);

    if (obj.tagName == "INPUT" && obj.type == "checkbox")
    {
      if (!document.forms['listForm'])
      {
        return;
      }
      var nodes = document.forms['listForm'].elements;
      var checked = false;

      for (i = 0; i < nodes.length; i++)
      {
        if (nodes[i].checked)
        {
           checked = true;
           break;
         }
      }

      if(document.getElementById("btnSubmit"))
      {
        document.getElementById("btnSubmit").disabled = !checked;
      }
      for (i = 1; i <= 10; i++)
      {
        if (document.getElementById("btnSubmit" + i))
        {
          document.getElementById("btnSubmit" + i).disabled = !checked;
        }
      }
    }
  }

}

//-->
</script><?php
include 'buy.php' ;
?>
</body>
</html>
