
<!-- $Id: db_backup.htm 14216 2008-03-10 02:27:21Z testyang $ -->
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>管理中心 - 数据备份 </title>
<meta name="robots" content="noindex, nofollow">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link href="styles/general.css" rel="stylesheet" type="text/css" />
<link href="styles/main.css" rel="stylesheet" type="text/css" />
<link href="styles/chosen/chosen.css" rel="stylesheet" type="text/css">
<script type="text/javascript" src="js/jquery-1.6.2.min.js"></script><script type="text/javascript" src="js/chosen.jquery.min.js"></script><script type="text/javascript" src="js/jquery.json.js"></script><script type="text/javascript" src="../js/transport.js"></script><script type="text/javascript" src="js/common.js"></script><script language="JavaScript">
<!--
// 这里把JS用到的所有语言都赋值到这里
var process_request = "正在处理您的请求...";
var todolist_caption = "记事本";
var todolist_autosave = "自动保存";
var todolist_save = "保存";
var todolist_clear = "清除";
var todolist_confirm_save = "是否将更改保存到记事本？";
var todolist_confirm_clear = "是否清空内容？";
var remove_confirm = "你确认要删除该备份吗？";
var lang_remove = "移除";
var lang_restore = "恢复备份";
var lang_download = "下载";
var sql_name_not_null = "文件名不能为空";
var vol_size_not_null = "请填入备份大小";
//-->
</script>
</head>
<body>

<div id="menu_list" onmouseover="show_popup()" onmouseout="hide_popup()">
<ul>
<li><a href="goods.php?act=add" target="main_frame">添加新商品</a></li>
<li><a href="category.php?act=add" target="main_frame">添加商品分类</a></li>
<li><a href="order.php?act=add" target="main_frame">添加订单</a></li>
<li><a href="article.php?act=add" target="main_frame">添加新文章</a></li>
<li><a href="users.php?act=add" target="main_frame">添加会员</a></li>
</ul>
</div>
<script>
function show_popup(){
frmBody = parent.document.getElementById('frame-body');
if (frmBody.cols == "37, 12, *")
{
parent.main_frame.document.getElementById('menu_list').style.left = '195px';
}
else
{
parent.main_frame.document.getElementById('menu_list').style.left = '40px';
}
parent.main_frame.document.getElementById('menu_list').style.display = 'block';
}
function hide_popup(){

parent.main_frame.document.getElementById('menu_list').style.display = 'none';
}
</script>
<h1>
<span class="action-span"><a href="database.php?act=restore">恢复备份</a></span>
<span class="action-span1"><a href="index.php?act=main">管理中心</a> </span><span id="search_id" class="action-span1"> - 数据备份 </span>
<div style="clear:both"></div>
</h1>
<form  name="theForm" method="post"  action="database.php" onsubmit="return validate()">
<!-- start  list -->
<div class="list-div" id="listDiv">

<table cellspacing='1' cellpadding='3' >
  <tr>
    <th colspan="2">备份类型</th>
  </tr>
  <tr>
    <td><input type="radio" name="type" value="full" class="radio" onclick="findobj('showtables').style.display='none'">全部备份</td>
    <td>备份数据库所有表</td>
  </tr>
  <tr>
    <td><input type="radio" name="type" value="stand" class="radio" checked="checked" onclick="findobj('showtables').style.display='none'">标准备份(推荐)</td>
    <td>备份常用的数据表</td>
  </tr>
  <tr>
    <td><input type="radio" name="type" value="min" class="radio" onclick="findobj('showtables').style.display='none'">最小备份</td>
    <td>仅包括商品表，订单表，用户表</td>
  </tr>
  <tr>
    <td><input type="radio" name="type" value="custom" class="radio" onclick="findobj('showtables').style.display=''">自定义备份</td>
    <td>根据自行选择备份数据表</td>
  </tr>
  <tbody id="showtables" style="display:none">
  <tr>
    <td colspan="2">
      <table>
        <tr>
          <td colspan="4"><input name="chkall" onclick="checkall(this.form, 'customtables[]')" type="checkbox"><b>全选</b></td>
        </tr>
        <tr>
                            <td><input name="customtables[]" value="wrzcnet_account_log"  type="checkbox">wrzcnet_account_log</td>
                            <td><input name="customtables[]" value="wrzcnet_ad"  type="checkbox">wrzcnet_ad</td>
                            <td><input name="customtables[]" value="wrzcnet_ad_custom"  type="checkbox">wrzcnet_ad_custom</td>
                            <td><input name="customtables[]" value="wrzcnet_ad_position"  type="checkbox">wrzcnet_ad_position</td>
                            </tr><tr>
                    <td><input name="customtables[]" value="wrzcnet_admin_action"  type="checkbox">wrzcnet_admin_action</td>
                            <td><input name="customtables[]" value="wrzcnet_admin_log"  type="checkbox">wrzcnet_admin_log</td>
                            <td><input name="customtables[]" value="wrzcnet_admin_message"  type="checkbox">wrzcnet_admin_message</td>
                            <td><input name="customtables[]" value="wrzcnet_admin_user"  type="checkbox">wrzcnet_admin_user</td>
                            </tr><tr>
                    <td><input name="customtables[]" value="wrzcnet_adsense"  type="checkbox">wrzcnet_adsense</td>
                            <td><input name="customtables[]" value="wrzcnet_affiliate_log"  type="checkbox">wrzcnet_affiliate_log</td>
                            <td><input name="customtables[]" value="wrzcnet_agency"  type="checkbox">wrzcnet_agency</td>
                            <td><input name="customtables[]" value="wrzcnet_area_region"  type="checkbox">wrzcnet_area_region</td>
                            </tr><tr>
                    <td><input name="customtables[]" value="wrzcnet_article"  type="checkbox">wrzcnet_article</td>
                            <td><input name="customtables[]" value="wrzcnet_article_cat"  type="checkbox">wrzcnet_article_cat</td>
                            <td><input name="customtables[]" value="wrzcnet_attribute"  type="checkbox">wrzcnet_attribute</td>
                            <td><input name="customtables[]" value="wrzcnet_attribute_color"  type="checkbox">wrzcnet_attribute_color</td>
                            </tr><tr>
                    <td><input name="customtables[]" value="wrzcnet_auction_log"  type="checkbox">wrzcnet_auction_log</td>
                            <td><input name="customtables[]" value="wrzcnet_auto_manage"  type="checkbox">wrzcnet_auto_manage</td>
                            <td><input name="customtables[]" value="wrzcnet_back_action"  type="checkbox">wrzcnet_back_action</td>
                            <td><input name="customtables[]" value="wrzcnet_back_goods"  type="checkbox">wrzcnet_back_goods</td>
                            </tr><tr>
                    <td><input name="customtables[]" value="wrzcnet_back_order"  type="checkbox">wrzcnet_back_order</td>
                            <td><input name="customtables[]" value="wrzcnet_back_replay"  type="checkbox">wrzcnet_back_replay</td>
                            <td><input name="customtables[]" value="wrzcnet_bar_code"  type="checkbox">wrzcnet_bar_code</td>
                            <td><input name="customtables[]" value="wrzcnet_bind_record"  type="checkbox">wrzcnet_bind_record</td>
                            </tr><tr>
                    <td><input name="customtables[]" value="wrzcnet_bonus_type"  type="checkbox">wrzcnet_bonus_type</td>
                            <td><input name="customtables[]" value="wrzcnet_booking_goods"  type="checkbox">wrzcnet_booking_goods</td>
                            <td><input name="customtables[]" value="wrzcnet_brand"  type="checkbox">wrzcnet_brand</td>
                            <td><input name="customtables[]" value="wrzcnet_card"  type="checkbox">wrzcnet_card</td>
                            </tr><tr>
                    <td><input name="customtables[]" value="wrzcnet_cart"  type="checkbox">wrzcnet_cart</td>
                            <td><input name="customtables[]" value="wrzcnet_cat_flashimg"  type="checkbox">wrzcnet_cat_flashimg</td>
                            <td><input name="customtables[]" value="wrzcnet_cat_recommend"  type="checkbox">wrzcnet_cat_recommend</td>
                            <td><input name="customtables[]" value="wrzcnet_category"  type="checkbox">wrzcnet_category</td>
                            </tr><tr>
                    <td><input name="customtables[]" value="wrzcnet_chat_customer"  type="checkbox">wrzcnet_chat_customer</td>
                            <td><input name="customtables[]" value="wrzcnet_chat_third_customer"  type="checkbox">wrzcnet_chat_third_customer</td>
                            <td><input name="customtables[]" value="wrzcnet_collect_goods"  type="checkbox">wrzcnet_collect_goods</td>
                            <td><input name="customtables[]" value="wrzcnet_comment"  type="checkbox">wrzcnet_comment</td>
                            </tr><tr>
                    <td><input name="customtables[]" value="wrzcnet_crons"  type="checkbox">wrzcnet_crons</td>
                            <td><input name="customtables[]" value="wrzcnet_delivery_goods"  type="checkbox">wrzcnet_delivery_goods</td>
                            <td><input name="customtables[]" value="wrzcnet_delivery_order"  type="checkbox">wrzcnet_delivery_order</td>
                            <td><input name="customtables[]" value="wrzcnet_deposit"  type="checkbox">wrzcnet_deposit</td>
                            </tr><tr>
                    <td><input name="customtables[]" value="wrzcnet_dianpu"  type="checkbox">wrzcnet_dianpu</td>
                            <td><input name="customtables[]" value="wrzcnet_wrzcmart_ad"  type="checkbox">wrzcnet_wrzcmart_ad</td>
                            <td><input name="customtables[]" value="wrzcnet_wrzcmart_ad_position"  type="checkbox">wrzcnet_wrzcmart_ad_position</td>
                            <td><input name="customtables[]" value="wrzcnet_wrzcmart_article"  type="checkbox">wrzcnet_wrzcmart_article</td>
                            </tr><tr>
                    <td><input name="customtables[]" value="wrzcnet_wrzcmart_article_cat"  type="checkbox">wrzcnet_wrzcmart_article_cat</td>
                            <td><input name="customtables[]" value="wrzcnet_wrzcmart_distrib_goods"  type="checkbox">wrzcnet_wrzcmart_distrib_goods</td>
                            <td><input name="customtables[]" value="wrzcnet_wrzcmart_menu"  type="checkbox">wrzcnet_wrzcmart_menu</td>
                            <td><input name="customtables[]" value="wrzcnet_wrzcmart_payment"  type="checkbox">wrzcnet_wrzcmart_payment</td>
                            </tr><tr>
                    <td><input name="customtables[]" value="wrzcnet_wrzcmart_shop_config"  type="checkbox">wrzcnet_wrzcmart_shop_config</td>
                            <td><input name="customtables[]" value="wrzcnet_wrzcmart_template"  type="checkbox">wrzcnet_wrzcmart_template</td>
                            <td><input name="customtables[]" value="wrzcnet_email"  type="checkbox">wrzcnet_email</td>
                            <td><input name="customtables[]" value="wrzcnet_email_list"  type="checkbox">wrzcnet_email_list</td>
                            </tr><tr>
                    <td><input name="customtables[]" value="wrzcnet_email_sendlist"  type="checkbox">wrzcnet_email_sendlist</td>
                            <td><input name="customtables[]" value="wrzcnet_error_log"  type="checkbox">wrzcnet_error_log</td>
                            <td><input name="customtables[]" value="wrzcnet_exchange_goods"  type="checkbox">wrzcnet_exchange_goods</td>
                            <td><input name="customtables[]" value="wrzcnet_favourable_activity"  type="checkbox">wrzcnet_favourable_activity</td>
                            </tr><tr>
                    <td><input name="customtables[]" value="wrzcnet_feedback"  type="checkbox">wrzcnet_feedback</td>
                            <td><input name="customtables[]" value="wrzcnet_friend_link"  type="checkbox">wrzcnet_friend_link</td>
                            <td><input name="customtables[]" value="wrzcnet_goods"  type="checkbox">wrzcnet_goods</td>
                            <td><input name="customtables[]" value="wrzcnet_goods_activity"  type="checkbox">wrzcnet_goods_activity</td>
                            </tr><tr>
                    <td><input name="customtables[]" value="wrzcnet_goods_article"  type="checkbox">wrzcnet_goods_article</td>
                            <td><input name="customtables[]" value="wrzcnet_goods_attr"  type="checkbox">wrzcnet_goods_attr</td>
                            <td><input name="customtables[]" value="wrzcnet_goods_cat"  type="checkbox">wrzcnet_goods_cat</td>
                            <td><input name="customtables[]" value="wrzcnet_goods_gallery"  type="checkbox">wrzcnet_goods_gallery</td>
                            </tr><tr>
                    <td><input name="customtables[]" value="wrzcnet_goods_tag"  type="checkbox">wrzcnet_goods_tag</td>
                            <td><input name="customtables[]" value="wrzcnet_goods_type"  type="checkbox">wrzcnet_goods_type</td>
                            <td><input name="customtables[]" value="wrzcnet_group_goods"  type="checkbox">wrzcnet_group_goods</td>
                            <td><input name="customtables[]" value="wrzcnet_keyword"  type="checkbox">wrzcnet_keyword</td>
                            </tr><tr>
                    <td><input name="customtables[]" value="wrzcnet_keyword_area"  type="checkbox">wrzcnet_keyword_area</td>
                            <td><input name="customtables[]" value="wrzcnet_keywords"  type="checkbox">wrzcnet_keywords</td>
                            <td><input name="customtables[]" value="wrzcnet_kuaidi_order"  type="checkbox">wrzcnet_kuaidi_order</td>
                            <td><input name="customtables[]" value="wrzcnet_kuaidi_order_status"  type="checkbox">wrzcnet_kuaidi_order_status</td>
                            </tr><tr>
                    <td><input name="customtables[]" value="wrzcnet_link_goods"  type="checkbox">wrzcnet_link_goods</td>
                            <td><input name="customtables[]" value="wrzcnet_mail_templates"  type="checkbox">wrzcnet_mail_templates</td>
                            <td><input name="customtables[]" value="wrzcnet_member_price"  type="checkbox">wrzcnet_member_price</td>
                            <td><input name="customtables[]" value="wrzcnet_nav"  type="checkbox">wrzcnet_nav</td>
                            </tr><tr>
                    <td><input name="customtables[]" value="wrzcnet_on_sales"  type="checkbox">wrzcnet_on_sales</td>
                            <td><input name="customtables[]" value="wrzcnet_order_action"  type="checkbox">wrzcnet_order_action</td>
                            <td><input name="customtables[]" value="wrzcnet_order_goods"  type="checkbox">wrzcnet_order_goods</td>
                            <td><input name="customtables[]" value="wrzcnet_order_info"  type="checkbox">wrzcnet_order_info</td>
                            </tr><tr>
                    <td><input name="customtables[]" value="wrzcnet_pack"  type="checkbox">wrzcnet_pack</td>
                            <td><input name="customtables[]" value="wrzcnet_package_goods"  type="checkbox">wrzcnet_package_goods</td>
                            <td><input name="customtables[]" value="wrzcnet_pay_log"  type="checkbox">wrzcnet_pay_log</td>
                            <td><input name="customtables[]" value="wrzcnet_payment"  type="checkbox">wrzcnet_payment</td>
                            </tr><tr>
                    <td><input name="customtables[]" value="wrzcnet_pickup_point"  type="checkbox">wrzcnet_pickup_point</td>
                            <td><input name="customtables[]" value="wrzcnet_plugins"  type="checkbox">wrzcnet_plugins</td>
                            <td><input name="customtables[]" value="wrzcnet_postman"  type="checkbox">wrzcnet_postman</td>
                            <td><input name="customtables[]" value="wrzcnet_pricecut"  type="checkbox">wrzcnet_pricecut</td>
                            </tr><tr>
                    <td><input name="customtables[]" value="wrzcnet_products"  type="checkbox">wrzcnet_products</td>
                            <td><input name="customtables[]" value="wrzcnet_question"  type="checkbox">wrzcnet_question</td>
                            <td><input name="customtables[]" value="wrzcnet_reg_extend_info"  type="checkbox">wrzcnet_reg_extend_info</td>
                            <td><input name="customtables[]" value="wrzcnet_reg_fields"  type="checkbox">wrzcnet_reg_fields</td>
                            </tr><tr>
                    <td><input name="customtables[]" value="wrzcnet_region"  type="checkbox">wrzcnet_region</td>
                            <td><input name="customtables[]" value="wrzcnet_role"  type="checkbox">wrzcnet_role</td>
                            <td><input name="customtables[]" value="wrzcnet_searchengine"  type="checkbox">wrzcnet_searchengine</td>
                            <td><input name="customtables[]" value="wrzcnet_sessions"  type="checkbox">wrzcnet_sessions</td>
                            </tr><tr>
                    <td><input name="customtables[]" value="wrzcnet_sessions_data"  type="checkbox">wrzcnet_sessions_data</td>
                            <td><input name="customtables[]" value="wrzcnet_shaidan"  type="checkbox">wrzcnet_shaidan</td>
                            <td><input name="customtables[]" value="wrzcnet_shaidan_img"  type="checkbox">wrzcnet_shaidan_img</td>
                            <td><input name="customtables[]" value="wrzcnet_shipping"  type="checkbox">wrzcnet_shipping</td>
                            </tr><tr>
                    <td><input name="customtables[]" value="wrzcnet_shipping_area"  type="checkbox">wrzcnet_shipping_area</td>
                            <td><input name="customtables[]" value="wrzcnet_shop_config"  type="checkbox">wrzcnet_shop_config</td>
                            <td><input name="customtables[]" value="wrzcnet_shop_grade"  type="checkbox">wrzcnet_shop_grade</td>
                            <td><input name="customtables[]" value="wrzcnet_snatch_log"  type="checkbox">wrzcnet_snatch_log</td>
                            </tr><tr>
                    <td><input name="customtables[]" value="wrzcnet_stats"  type="checkbox">wrzcnet_stats</td>
                            <td><input name="customtables[]" value="wrzcnet_street_category"  type="checkbox">wrzcnet_street_category</td>
                            <td><input name="customtables[]" value="wrzcnet_supplier"  type="checkbox">wrzcnet_supplier</td>
                            <td><input name="customtables[]" value="wrzcnet_supplier_admin_action"  type="checkbox">wrzcnet_supplier_admin_action</td>
                            </tr><tr>
                    <td><input name="customtables[]" value="wrzcnet_supplier_admin_user"  type="checkbox">wrzcnet_supplier_admin_user</td>
                            <td><input name="customtables[]" value="wrzcnet_supplier_article"  type="checkbox">wrzcnet_supplier_article</td>
                            <td><input name="customtables[]" value="wrzcnet_supplier_article_cat"  type="checkbox">wrzcnet_supplier_article_cat</td>
                            <td><input name="customtables[]" value="wrzcnet_supplier_cat_recommend"  type="checkbox">wrzcnet_supplier_cat_recommend</td>
                            </tr><tr>
                    <td><input name="customtables[]" value="wrzcnet_supplier_category"  type="checkbox">wrzcnet_supplier_category</td>
                            <td><input name="customtables[]" value="wrzcnet_supplier_goods_cat"  type="checkbox">wrzcnet_supplier_goods_cat</td>
                            <td><input name="customtables[]" value="wrzcnet_supplier_guanzhu"  type="checkbox">wrzcnet_supplier_guanzhu</td>
                            <td><input name="customtables[]" value="wrzcnet_supplier_money_log"  type="checkbox">wrzcnet_supplier_money_log</td>
                            </tr><tr>
                    <td><input name="customtables[]" value="wrzcnet_supplier_nav"  type="checkbox">wrzcnet_supplier_nav</td>
                            <td><input name="customtables[]" value="wrzcnet_supplier_rank"  type="checkbox">wrzcnet_supplier_rank</td>
                            <td><input name="customtables[]" value="wrzcnet_supplier_rebate"  type="checkbox">wrzcnet_supplier_rebate</td>
                            <td><input name="customtables[]" value="wrzcnet_supplier_rebate_log"  type="checkbox">wrzcnet_supplier_rebate_log</td>
                            </tr><tr>
                    <td><input name="customtables[]" value="wrzcnet_supplier_shop_config"  type="checkbox">wrzcnet_supplier_shop_config</td>
                            <td><input name="customtables[]" value="wrzcnet_supplier_street"  type="checkbox">wrzcnet_supplier_street</td>
                            <td><input name="customtables[]" value="wrzcnet_supplier_tag"  type="checkbox">wrzcnet_supplier_tag</td>
                            <td><input name="customtables[]" value="wrzcnet_supplier_tag_map"  type="checkbox">wrzcnet_supplier_tag_map</td>
                            </tr><tr>
                    <td><input name="customtables[]" value="wrzcnet_suppliers"  type="checkbox">wrzcnet_suppliers</td>
                            <td><input name="customtables[]" value="wrzcnet_tag"  type="checkbox">wrzcnet_tag</td>
                            <td><input name="customtables[]" value="wrzcnet_takegoods"  type="checkbox">wrzcnet_takegoods</td>
                            <td><input name="customtables[]" value="wrzcnet_takegoods_goods"  type="checkbox">wrzcnet_takegoods_goods</td>
                            </tr><tr>
                    <td><input name="customtables[]" value="wrzcnet_takegoods_order"  type="checkbox">wrzcnet_takegoods_order</td>
                            <td><input name="customtables[]" value="wrzcnet_takegoods_type"  type="checkbox">wrzcnet_takegoods_type</td>
                            <td><input name="customtables[]" value="wrzcnet_takegoods_type_goods"  type="checkbox">wrzcnet_takegoods_type_goods</td>
                            <td><input name="customtables[]" value="wrzcnet_template"  type="checkbox">wrzcnet_template</td>
                            </tr><tr>
                    <td><input name="customtables[]" value="wrzcnet_topic"  type="checkbox">wrzcnet_topic</td>
                            <td><input name="customtables[]" value="wrzcnet_user_account"  type="checkbox">wrzcnet_user_account</td>
                            <td><input name="customtables[]" value="wrzcnet_user_address"  type="checkbox">wrzcnet_user_address</td>
                            <td><input name="customtables[]" value="wrzcnet_user_bonus"  type="checkbox">wrzcnet_user_bonus</td>
                            </tr><tr>
                    <td><input name="customtables[]" value="wrzcnet_user_feed"  type="checkbox">wrzcnet_user_feed</td>
                            <td><input name="customtables[]" value="wrzcnet_user_rank"  type="checkbox">wrzcnet_user_rank</td>
                            <td><input name="customtables[]" value="wrzcnet_users"  type="checkbox">wrzcnet_users</td>
                            <td><input name="customtables[]" value="wrzcnet_validate_record"  type="checkbox">wrzcnet_validate_record</td>
                            </tr><tr>
                    <td><input name="customtables[]" value="wrzcnet_valuecard"  type="checkbox">wrzcnet_valuecard</td>
                            <td><input name="customtables[]" value="wrzcnet_valuecard_type"  type="checkbox">wrzcnet_valuecard_type</td>
                            <td><input name="customtables[]" value="wrzcnet_verifycode"  type="checkbox">wrzcnet_verifycode</td>
                            <td><input name="customtables[]" value="wrzcnet_virtual_card"  type="checkbox">wrzcnet_virtual_card</td>
                            </tr><tr>
                    <td><input name="customtables[]" value="wrzcnet_virtual_district"  type="checkbox">wrzcnet_virtual_district</td>
                            <td><input name="customtables[]" value="wrzcnet_virtual_goods_card"  type="checkbox">wrzcnet_virtual_goods_card</td>
                            <td><input name="customtables[]" value="wrzcnet_virtual_goods_district"  type="checkbox">wrzcnet_virtual_goods_district</td>
                            <td><input name="customtables[]" value="wrzcnet_volume_price"  type="checkbox">wrzcnet_volume_price</td>
                            </tr><tr>
                    <td><input name="customtables[]" value="wrzcnet_vote"  type="checkbox">wrzcnet_vote</td>
                            <td><input name="customtables[]" value="wrzcnet_vote_log"  type="checkbox">wrzcnet_vote_log</td>
                            <td><input name="customtables[]" value="wrzcnet_vote_option"  type="checkbox">wrzcnet_vote_option</td>
                            <td><input name="customtables[]" value="wrzcnet_weixin_act"  type="checkbox">wrzcnet_weixin_act</td>
                            </tr><tr>
                    <td><input name="customtables[]" value="wrzcnet_weixin_actlist"  type="checkbox">wrzcnet_weixin_actlist</td>
                            <td><input name="customtables[]" value="wrzcnet_weixin_actlog"  type="checkbox">wrzcnet_weixin_actlog</td>
                            <td><input name="customtables[]" value="wrzcnet_weixin_config"  type="checkbox">wrzcnet_weixin_config</td>
                            <td><input name="customtables[]" value="wrzcnet_weixin_corn"  type="checkbox">wrzcnet_weixin_corn</td>
                            </tr><tr>
                    <td><input name="customtables[]" value="wrzcnet_weixin_jflog"  type="checkbox">wrzcnet_weixin_jflog</td>
                            <td><input name="customtables[]" value="wrzcnet_weixin_keywords"  type="checkbox">wrzcnet_weixin_keywords</td>
                            <td><input name="customtables[]" value="wrzcnet_weixin_login"  type="checkbox">wrzcnet_weixin_login</td>
                            <td><input name="customtables[]" value="wrzcnet_weixin_menu"  type="checkbox">wrzcnet_weixin_menu</td>
                            </tr><tr>
                    <td><input name="customtables[]" value="wrzcnet_weixin_msg"  type="checkbox">wrzcnet_weixin_msg</td>
                            <td><input name="customtables[]" value="wrzcnet_weixin_oauth"  type="checkbox">wrzcnet_weixin_oauth</td>
                            <td><input name="customtables[]" value="wrzcnet_weixin_paylog"  type="checkbox">wrzcnet_weixin_paylog</td>
                            <td><input name="customtables[]" value="wrzcnet_weixin_qcode"  type="checkbox">wrzcnet_weixin_qcode</td>
                            </tr><tr>
                    <td><input name="customtables[]" value="wrzcnet_weixin_share"  type="checkbox">wrzcnet_weixin_share</td>
                            <td><input name="customtables[]" value="wrzcnet_weixin_sign"  type="checkbox">wrzcnet_weixin_sign</td>
                            <td><input name="customtables[]" value="wrzcnet_weixin_signconf"  type="checkbox">wrzcnet_weixin_signconf</td>
                            <td><input name="customtables[]" value="wrzcnet_weixin_user"  type="checkbox">wrzcnet_weixin_user</td>
                            </tr><tr>
                    <td><input name="customtables[]" value="wrzcnet_wholesale"  type="checkbox">wrzcnet_wholesale</td>
                </tr>
      </table>
    </td>
  </tr>
  </tbody>
</table>

<table cellspacing='1' cellpadding='3' >
  <tr>
    <th colspan="2">其他选项</th>
  </tr>
  <tr>
    <td>使用扩展插入(Extended Insert)方式</td>
    <td><input type="radio" name="ext_insert" class="radio" value='1'>是&nbsp;&nbsp;&nbsp;&nbsp;<input type="radio" name="ext_insert" class="radio" value='0' checked="checked">否</td>
  </tr>
  <tr>
    <td>分卷备份 - 文件长度限制(kb)</td>
    <td><input type="text" name="vol_size" value="51200"></td>
  </tr>
  <tr>
    <td>备份文件名</td>
    <td><input type="text" name="sql_file_name" value="20160903bkhyan.sql"></td>
  </tr>
</table>
<input type="hidden" name="act" value="dumpsql">
<center><input type="submit" value="开始备份" class="button" /></center>
</div>
<!-- end  list -->
</form>

<script type="text/javascript" src="../js/utils.js"></script><script type="text/javascript" src="js/validator.js"></script>
<script language="JavaScript">
<!--
/**
 * 检查表单输入的数据
 */
function validate()
{
  validator = new Validator("theForm");
  validator.required("sql_file_name", sql_name_not_null);
  validator.required("vol_size", vol_size_not_null);
  return validator.passed();
}

onload = function()
{
  // 开始检查订单
  startCheckOrder();
}

function findobj(str)
{
    return document.getElementById(str);
}

function checkall(frm, chk)
{
    for (i = 0; i < frm.elements.length; i++)
    {
        if (frm.elements[i].name == chk)
        {
            frm.elements[i].checked = frm.elements['chkall'].checked;
        }
    }
}

function radioClicked(n)
{
    if (n > 0)
    {
        document.forms['theForm'].elements["vol_size"].disabled = false;
        var str = document.forms['theForm'].elements["sql_name"].value ;
        document.forms['theForm'].elements["sql_name"].value = str.slice(0, -4) + '.zip' ;
    }
    else
    {
        document.forms['theForm'].elements["vol_size"].disabled = true;
        var str = document.forms['theForm'].elements["sql_name"].value ;
        document.forms['theForm'].elements["sql_name"].value = str.slice(0, -4) + '.sql' ;
    }
}

/**
 * 切换显示表前缀
 * @param bool display 是否显示
 */
function toggleTablePre(display)
{
    var disp = display ? '' : 'none';
    for (var i = 1; i <= 9; i++)
    {
        document.getElementById('pre_' + i).style.display = disp;
    }
}
//-->
</script>

<div id="footer">
共执行 2 个查询，用时 0.096005 秒，Gzip 已禁用，内存占用 2.640 MB<br />
版权所有 &copy; 2008-2030 广州网软志成信息科技有限公司，并保留所有权利。</div>
<!-- 新订单提示信息 -->
<div id="popMsg">
  <table cellspacing="0" cellpadding="0" width="100%" bgcolor="#cfdef4" border="0">
  <tr>
    <td style="color: #0f2c8c" width="30" height="24"></td>
    <td style="font-weight: normal; color: #1f336b; padding-top: 4px;padding-left: 4px" valign="center" width="100%"> 新订单通知</td>
    <td style="padding-top: 2px;padding-right:2px" valign="center" align="right" width="19"><span title="关闭" style="cursor: hand;cursor:pointer;color:red;font-size:12px;font-weight:bold;margin-right:4px;" onclick="Message.close()" >×</span><!-- <img title=关闭 style="cursor: hand" onclick=closediv() hspace=3 src="msgclose.jpg"> --></td>
  </tr>
  <tr>
    <td style="padding-right: 1px; padding-bottom: 1px" colspan="3" height="70">
    <div id="popMsgContent">
      <p>您有 <strong style="color:#ff0000" id="spanNewOrder">1</strong> 个新订单以及       <strong style="color:#ff0000" id="spanNewPaid">0</strong> 个新付款的订单</p>
      <p align="center" style="word-break:break-all"><a href="order.php?act=list"><span style="color:#ff0000">点击查看新订单</span></a></p>
    </div>
    </td>
  </tr>
  </table>
</div>

<!--
<embed src="images/online.wav" width="0" height="0" autostart="false" name="msgBeep" id="msgBeep" enablejavascript="true"/>
-->
<object classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000" codebase="http://active.macromedia.com/flash2/cabs/swflash.cab#version=4,0,0,0" id="msgBeep" width="1" height="1">
  <param name="movie" value="images/online.swf">
  <param name="quality" value="high">
  <embed src="images/online.swf" name="msgBeep" id="msgBeep" quality="high" width="0" height="0" type="application/x-shockwave-flash" pluginspage="http://www.macromedia.com/shockwave/download/index.cgi?p1_prod_version=shockwaveflash">
  </embed>
</object>

<script language="JavaScript">
document.onmousemove=function(e)
{
  var obj = Utils.srcElement(e);
  if (typeof(obj.onclick) == 'function' && obj.onclick.toString().indexOf('listTable.edit') != -1)
  {
    obj.title = '点击修改内容';
    obj.style.cssText = 'background: #278296;';
    obj.onmouseout = function(e)
    {
      this.style.cssText = '';
    }
  }
  else if (typeof(obj.href) != 'undefined' && obj.href.indexOf('listTable.sort') != -1)
  {
    obj.title = '点击对列表排序';
  }
}
<!--


var MyTodolist;
function showTodoList(adminid)
{
  if(!MyTodolist)
  {
    var global = $import("../js/global.js","js");
    global.onload = global.onreadystatechange= function()
    {
      if(this.readyState && this.readyState=="loading")return;
      var md5 = $import("js/md5.js","js");
      md5.onload = md5.onreadystatechange= function()
      {
        if(this.readyState && this.readyState=="loading")return;
        var todolist = $import("js/todolist.js","js");
        todolist.onload = todolist.onreadystatechange = function()
        {
          if(this.readyState && this.readyState=="loading")return;
          MyTodolist = new Todolist();
          MyTodolist.show();
        }
      }
    }
  }
  else
  {
    if(MyTodolist.visibility)
    {
      MyTodolist.hide();
    }
    else
    {
      MyTodolist.show();
    }
  }
}

if (Browser.isIE)
{
  onscroll = function()
  {
    //document.getElementById('calculator').style.top = document.body.scrollTop;
    document.getElementById('popMsg').style.top = (document.body.scrollTop + document.body.clientHeight - document.getElementById('popMsg').offsetHeight) + "px";
  }
}

if (document.getElementById("listDiv"))
{
  document.getElementById("listDiv").onmouseover = function(e)
  {
    obj = Utils.srcElement(e);

    if (obj)
    {
      if (obj.parentNode.tagName.toLowerCase() == "tr") row = obj.parentNode;
      else if (obj.parentNode.parentNode.tagName.toLowerCase() == "tr") row = obj.parentNode.parentNode;
      else return;

      for (i = 0; i < row.cells.length; i++)
      {
        if (row.cells[i].tagName != "TH") row.cells[i].style.backgroundColor = '#F4FAFB';
      }
    }

  }

  document.getElementById("listDiv").onmouseout = function(e)
  {
    obj = Utils.srcElement(e);

    if (obj)
    {
      if (obj.parentNode.tagName.toLowerCase() == "tr") row = obj.parentNode;
      else if (obj.parentNode.parentNode.tagName.toLowerCase() == "tr") row = obj.parentNode.parentNode;
      else return;

      for (i = 0; i < row.cells.length; i++)
      {
          if (row.cells[i].tagName != "TH") row.cells[i].style.backgroundColor = '';
      }
    }
  }

  document.getElementById("listDiv").onclick = function(e)
  {
    var obj = Utils.srcElement(e);

    if (obj.tagName == "INPUT" && obj.type == "checkbox")
    {
      if (!document.forms['listForm'])
      {
        return;
      }
      var nodes = document.forms['listForm'].elements;
      var checked = false;

      for (i = 0; i < nodes.length; i++)
      {
        if (nodes[i].checked)
        {
           checked = true;
           break;
         }
      }

      if(document.getElementById("btnSubmit"))
      {
        document.getElementById("btnSubmit").disabled = !checked;
      }
      for (i = 1; i <= 10; i++)
      {
        if (document.getElementById("btnSubmit" + i))
        {
          document.getElementById("btnSubmit" + i).disabled = !checked;
        }
      }
    }
  }

}

//-->
</script>
</body>
</html>
<!-- $Id: optimize.htm 14216 2008-03-10 02:27:21Z testyang $ -->
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>管理中心 - 数据表优化 </title>
<meta name="robots" content="noindex, nofollow">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link href="styles/general.css" rel="stylesheet" type="text/css" />
<link href="styles/main.css" rel="stylesheet" type="text/css" />
<link href="styles/chosen/chosen.css" rel="stylesheet" type="text/css">
<script type="text/javascript" src="js/jquery-1.6.2.min.js"></script><script type="text/javascript" src="js/chosen.jquery.min.js"></script><script type="text/javascript" src="js/jquery.json.js"></script><script type="text/javascript" src="../js/transport.js"></script><script type="text/javascript" src="js/common.js"></script><script language="JavaScript">
<!--
// 这里把JS用到的所有语言都赋值到这里
var process_request = "正在处理您的请求...";
var todolist_caption = "记事本";
var todolist_autosave = "自动保存";
var todolist_save = "保存";
var todolist_clear = "清除";
var todolist_confirm_save = "是否将更改保存到记事本？";
var todolist_confirm_clear = "是否清空内容？";
var remove_confirm = "你确认要删除该备份吗？";
var lang_remove = "移除";
var lang_restore = "恢复备份";
var lang_download = "下载";
var sql_name_not_null = "文件名不能为空";
var vol_size_not_null = "请填入备份大小";
//-->
</script>
</head>
<body>

<div id="menu_list" onmouseover="show_popup()" onmouseout="hide_popup()">
<ul>
<li><a href="goods.php?act=add" target="main_frame">添加新商品</a></li>
<li><a href="category.php?act=add" target="main_frame">添加商品分类</a></li>
<li><a href="order.php?act=add" target="main_frame">添加订单</a></li>
<li><a href="article.php?act=add" target="main_frame">添加新文章</a></li>
<li><a href="users.php?act=add" target="main_frame">添加会员</a></li>
</ul>
</div>
<script>
function show_popup(){
frmBody = parent.document.getElementById('frame-body');
if (frmBody.cols == "37, 12, *")
{
parent.main_frame.document.getElementById('menu_list').style.left = '195px';
}
else
{
parent.main_frame.document.getElementById('menu_list').style.left = '40px';
}
parent.main_frame.document.getElementById('menu_list').style.display = 'block';
}
function hide_popup(){

parent.main_frame.document.getElementById('menu_list').style.display = 'none';
}
</script>
<h1>
<span class="action-span1"><a href="index.php?act=main">管理中心</a> </span><span id="search_id" class="action-span1"> - 数据表优化 </span>
<div style="clear:both"></div>
</h1>
<!-- start form -->
<div class="form-div">
<form method="post" action="database.php" name="theForm">
总碎片数:0<input type="submit" value="开始进行数据表优化" class="button" />
<input type= "hidden" name= "num" value = "0">
<input type= "hidden" name="act" value = "run_optimize">
</form>
</div>
<!-- end form -->
<!-- start list -->
<div class="list-div" id="listDiv">
<table cellspacing='1' cellpadding='3' id='listTable'>
  <tr>
    <th>数据表</th>
    <th>数据表类型</th>
    <th>记录数</th>
    <th>数据</th>
    <th>碎片</th>
    <th>字符集</th>
    <th>状态</th>
  </tr>
      <tr>
      <td class="first-cell">wrzcnet_account_log</td>
      <td align ="left">MyISAM</td>
      <td align ="right">63</td>
      <td align ="right"> 3.58 KB</td>
      <td align ="right">0</td>
      <td align ="left">utf8_general_ci</td>
      <td align ="left">OK</td>
    </tr>
      <tr>
      <td class="first-cell">wrzcnet_ad</td>
      <td align ="left">MyISAM</td>
      <td align ="right">66</td>
      <td align ="right"> 5.02 KB</td>
      <td align ="right">0</td>
      <td align ="left">utf8_general_ci</td>
      <td align ="left">OK</td>
    </tr>
      <tr>
      <td class="first-cell">wrzcnet_ad_custom</td>
      <td align ="left">MyISAM</td>
      <td align ="right">1</td>
      <td align ="right"> 0.06 KB</td>
      <td align ="right">0</td>
      <td align ="left">utf8_general_ci</td>
      <td align ="left">OK</td>
    </tr>
      <tr>
      <td class="first-cell">wrzcnet_ad_position</td>
      <td align ="left">MyISAM</td>
      <td align ="right">40</td>
      <td align ="right"> 6.08 KB</td>
      <td align ="right">0</td>
      <td align ="left">utf8_general_ci</td>
      <td align ="left">OK</td>
    </tr>
      <tr>
      <td class="first-cell">wrzcnet_admin_action</td>
      <td align ="left">MyISAM</td>
      <td align ="right">131</td>
      <td align ="right"> 2.90 KB</td>
      <td align ="right">0</td>
      <td align ="left">utf8_general_ci</td>
      <td align ="left">OK</td>
    </tr>
      <tr>
      <td class="first-cell">wrzcnet_admin_log</td>
      <td align ="left">MyISAM</td>
      <td align ="right">49</td>
      <td align ="right"> 2.70 KB</td>
      <td align ="right">0</td>
      <td align ="left">utf8_general_ci</td>
      <td align ="left">OK</td>
    </tr>
      <tr>
      <td class="first-cell">wrzcnet_admin_message</td>
      <td align ="left">MyISAM</td>
      <td align ="right">0</td>
      <td align ="right"> 0.00 KB</td>
      <td align ="right">0</td>
      <td align ="left">utf8_general_ci</td>
      <td align ="left">OK</td>
    </tr>
      <tr>
      <td class="first-cell">wrzcnet_admin_user</td>
      <td align ="left">MyISAM</td>
      <td align ="right">2</td>
      <td align ="right"> 0.60 KB</td>
      <td align ="right">0</td>
      <td align ="left">utf8_general_ci</td>
      <td align ="left">OK</td>
    </tr>
      <tr>
      <td class="first-cell">wrzcnet_adsense</td>
      <td align ="left">MyISAM</td>
      <td align ="right">31</td>
      <td align ="right"> 0.61 KB</td>
      <td align ="right">0</td>
      <td align ="left">utf8_general_ci</td>
      <td align ="left">OK</td>
    </tr>
      <tr>
      <td class="first-cell">wrzcnet_affiliate_log</td>
      <td align ="left">MyISAM</td>
      <td align ="right">2</td>
      <td align ="right"> 0.07 KB</td>
      <td align ="right">0</td>
      <td align ="left">utf8_general_ci</td>
      <td align ="left">OK</td>
    </tr>
      <tr>
      <td class="first-cell">wrzcnet_agency</td>
      <td align ="left">MyISAM</td>
      <td align ="right">1</td>
      <td align ="right"> 0.04 KB</td>
      <td align ="right">0</td>
      <td align ="left">utf8_general_ci</td>
      <td align ="left">OK</td>
    </tr>
      <tr>
      <td class="first-cell">wrzcnet_area_region</td>
      <td align ="left">MyISAM</td>
      <td align ="right">48</td>
      <td align ="right"> 0.33 KB</td>
      <td align ="right">0</td>
      <td align ="left">utf8_general_ci</td>
      <td align ="left">OK</td>
    </tr>
      <tr>
      <td class="first-cell">wrzcnet_article</td>
      <td align ="left">MyISAM</td>
      <td align ="right">113</td>
      <td align ="right"> 360.47 KB</td>
      <td align ="right">0</td>
      <td align ="left">utf8_general_ci</td>
      <td align ="left">OK</td>
    </tr>
      <tr>
      <td class="first-cell">wrzcnet_article_cat</td>
      <td align ="left">MyISAM</td>
      <td align ="right">19</td>
      <td align ="right"> 0.58 KB</td>
      <td align ="right">0</td>
      <td align ="left">utf8_general_ci</td>
      <td align ="left">OK</td>
    </tr>
      <tr>
      <td class="first-cell">wrzcnet_attribute</td>
      <td align ="left">MyISAM</td>
      <td align ="right">37</td>
      <td align ="right"> 2.70 KB</td>
      <td align ="right">0</td>
      <td align ="left">utf8_general_ci</td>
      <td align ="left">OK</td>
    </tr>
      <tr>
      <td class="first-cell">wrzcnet_attribute_color</td>
      <td align ="left">MyISAM</td>
      <td align ="right">25</td>
      <td align ="right"> 0.68 KB</td>
      <td align ="right">0</td>
      <td align ="left">utf8_general_ci</td>
      <td align ="left">OK</td>
    </tr>
      <tr>
      <td class="first-cell">wrzcnet_auction_log</td>
      <td align ="left">MyISAM</td>
      <td align ="right">7</td>
      <td align ="right"> 0.13 KB</td>
      <td align ="right">0</td>
      <td align ="left">utf8_general_ci</td>
      <td align ="left">OK</td>
    </tr>
      <tr>
      <td class="first-cell">wrzcnet_auto_manage</td>
      <td align ="left">MyISAM</td>
      <td align ="right">0</td>
      <td align ="right"> 0.00 KB</td>
      <td align ="right">0</td>
      <td align ="left">utf8_general_ci</td>
      <td align ="left">OK</td>
    </tr>
      <tr>
      <td class="first-cell">wrzcnet_back_action</td>
      <td align ="left">MyISAM</td>
      <td align ="right">11</td>
      <td align ="right"> 0.34 KB</td>
      <td align ="right">0</td>
      <td align ="left">utf8_general_ci</td>
      <td align ="left">OK</td>
    </tr>
      <tr>
      <td class="first-cell">wrzcnet_back_goods</td>
      <td align ="left">MyISAM</td>
      <td align ="right">9</td>
      <td align ="right"> 1.14 KB</td>
      <td align ="right">0</td>
      <td align ="left">utf8_general_ci</td>
      <td align ="left">OK</td>
    </tr>
      <tr>
      <td class="first-cell">wrzcnet_back_order</td>
      <td align ="left">MyISAM</td>
      <td align ="right">9</td>
      <td align ="right"> 1.89 KB</td>
      <td align ="right">0</td>
      <td align ="left">utf8_general_ci</td>
      <td align ="left">OK</td>
    </tr>
      <tr>
      <td class="first-cell">wrzcnet_back_replay</td>
      <td align ="left">MyISAM</td>
      <td align ="right">0</td>
      <td align ="right"> 0.00 KB</td>
      <td align ="right">0</td>
      <td align ="left">utf8_general_ci</td>
      <td align ="left">OK</td>
    </tr>
      <tr>
      <td class="first-cell">wrzcnet_bar_code</td>
      <td align ="left">MyISAM</td>
      <td align ="right">0</td>
      <td align ="right"> 0.00 KB</td>
      <td align ="right">0</td>
      <td align ="left">utf8_general_ci</td>
      <td align ="left">OK</td>
    </tr>
      <tr>
      <td class="first-cell">wrzcnet_bind_record</td>
      <td align ="left">MyISAM</td>
      <td align ="right">0</td>
      <td align ="right"> 0.00 KB</td>
      <td align ="right">0</td>
      <td align ="left">utf8_general_ci</td>
      <td align ="left">OK</td>
    </tr>
      <tr>
      <td class="first-cell">wrzcnet_bonus_type</td>
      <td align ="left">MyISAM</td>
      <td align ="right">19</td>
      <td align ="right"> 1.34 KB</td>
      <td align ="right">0</td>
      <td align ="left">utf8_general_ci</td>
      <td align ="left">OK</td>
    </tr>
      <tr>
      <td class="first-cell">wrzcnet_booking_goods</td>
      <td align ="left">MyISAM</td>
      <td align ="right">36</td>
      <td align ="right"> 1.62 KB</td>
      <td align ="right">0</td>
      <td align ="left">utf8_general_ci</td>
      <td align ="left">OK</td>
    </tr>
      <tr>
      <td class="first-cell">wrzcnet_brand</td>
      <td align ="left">MyISAM</td>
      <td align ="right">104</td>
      <td align ="right"> 5.21 KB</td>
      <td align ="right">0</td>
      <td align ="left">utf8_general_ci</td>
      <td align ="left">OK</td>
    </tr>
      <tr>
      <td class="first-cell">wrzcnet_card</td>
      <td align ="left">MyISAM</td>
      <td align ="right">0</td>
      <td align ="right"> 0.00 KB</td>
      <td align ="right">0</td>
      <td align ="left">utf8_general_ci</td>
      <td align ="left">OK</td>
    </tr>
      <tr>
      <td class="first-cell">wrzcnet_cart</td>
      <td align ="left">MyISAM</td>
      <td align ="right">0</td>
      <td align ="right"> 0.00 KB</td>
      <td align ="right">0</td>
      <td align ="left">utf8_general_ci</td>
      <td align ="left">OK</td>
    </tr>
      <tr>
      <td class="first-cell">wrzcnet_cat_flashimg</td>
      <td align ="left">MyISAM</td>
      <td align ="right">4</td>
      <td align ="right"> 0.17 KB</td>
      <td align ="right">0</td>
      <td align ="left">utf8_general_ci</td>
      <td align ="left">OK</td>
    </tr>
      <tr>
      <td class="first-cell">wrzcnet_cat_recommend</td>
      <td align ="left">MyISAM</td>
      <td align ="right">93</td>
      <td align ="right"> 0.64 KB</td>
      <td align ="right">0</td>
      <td align ="left">utf8_general_ci</td>
      <td align ="left">OK</td>
    </tr>
      <tr>
      <td class="first-cell">wrzcnet_category</td>
      <td align ="left">MyISAM</td>
      <td align ="right">361</td>
      <td align ="right"> 14.93 KB</td>
      <td align ="right">0</td>
      <td align ="left">utf8_general_ci</td>
      <td align ="left">OK</td>
    </tr>
      <tr>
      <td class="first-cell">wrzcnet_chat_customer</td>
      <td align ="left">MyISAM</td>
      <td align ="right">2</td>
      <td align ="right"> 0.08 KB</td>
      <td align ="right">0</td>
      <td align ="left">utf8_general_ci</td>
      <td align ="left">OK</td>
    </tr>
      <tr>
      <td class="first-cell">wrzcnet_chat_third_customer</td>
      <td align ="left">MyISAM</td>
      <td align ="right">3</td>
      <td align ="right"> 0.10 KB</td>
      <td align ="right">0</td>
      <td align ="left">utf8_general_ci</td>
      <td align ="left">OK</td>
    </tr>
      <tr>
      <td class="first-cell">wrzcnet_collect_goods</td>
      <td align ="left">MyISAM</td>
      <td align ="right">40</td>
      <td align ="right"> 0.59 KB</td>
      <td align ="right">0</td>
      <td align ="left">utf8_general_ci</td>
      <td align ="left">OK</td>
    </tr>
      <tr>
      <td class="first-cell">wrzcnet_comment</td>
      <td align ="left">MyISAM</td>
      <td align ="right">6</td>
      <td align ="right"> 0.64 KB</td>
      <td align ="right">0</td>
      <td align ="left">utf8_general_ci</td>
      <td align ="left">OK</td>
    </tr>
      <tr>
      <td class="first-cell">wrzcnet_crons</td>
      <td align ="left">MyISAM</td>
      <td align ="right">2</td>
      <td align ="right"> 0.43 KB</td>
      <td align ="right">0</td>
      <td align ="left">utf8_general_ci</td>
      <td align ="left">OK</td>
    </tr>
      <tr>
      <td class="first-cell">wrzcnet_delivery_goods</td>
      <td align ="left">MyISAM</td>
      <td align ="right">57</td>
      <td align ="right"> 7.57 KB</td>
      <td align ="right">0</td>
      <td align ="left">utf8_general_ci</td>
      <td align ="left">OK</td>
    </tr>
      <tr>
      <td class="first-cell">wrzcnet_delivery_order</td>
      <td align ="left">MyISAM</td>
      <td align ="right">45</td>
      <td align ="right"> 9.61 KB</td>
      <td align ="right">0</td>
      <td align ="left">utf8_general_ci</td>
      <td align ="left">OK</td>
    </tr>
      <tr>
      <td class="first-cell">wrzcnet_deposit</td>
      <td align ="left">MyISAM</td>
      <td align ="right">0</td>
      <td align ="right"> 0.00 KB</td>
      <td align ="right">0</td>
      <td align ="left">utf8_general_ci</td>
      <td align ="left">OK</td>
    </tr>
      <tr>
      <td class="first-cell">wrzcnet_dianpu</td>
      <td align ="left">MyISAM</td>
      <td align ="right">1</td>
      <td align ="right"> 0.10 KB</td>
      <td align ="right">0</td>
      <td align ="left">utf8_general_ci</td>
      <td align ="left">OK</td>
    </tr>
      <tr>
      <td class="first-cell">wrzcnet_wrzcmart_ad</td>
      <td align ="left">MyISAM</td>
      <td align ="right">28</td>
      <td align ="right"> 2.07 KB</td>
      <td align ="right">0</td>
      <td align ="left">utf8_general_ci</td>
      <td align ="left">OK</td>
    </tr>
      <tr>
      <td class="first-cell">wrzcnet_wrzcmart_ad_position</td>
      <td align ="left">MyISAM</td>
      <td align ="right">18</td>
      <td align ="right"> 2.65 KB</td>
      <td align ="right">0</td>
      <td align ="left">utf8_general_ci</td>
      <td align ="left">OK</td>
    </tr>
      <tr>
      <td class="first-cell">wrzcnet_wrzcmart_article</td>
      <td align ="left">MyISAM</td>
      <td align ="right">3</td>
      <td align ="right"> 0.34 KB</td>
      <td align ="right">0</td>
      <td align ="left">utf8_general_ci</td>
      <td align ="left">OK</td>
    </tr>
      <tr>
      <td class="first-cell">wrzcnet_wrzcmart_article_cat</td>
      <td align ="left">MyISAM</td>
      <td align ="right">1</td>
      <td align ="right"> 0.03 KB</td>
      <td align ="right">0</td>
      <td align ="left">utf8_general_ci</td>
      <td align ="left">OK</td>
    </tr>
      <tr>
      <td class="first-cell">wrzcnet_wrzcmart_distrib_goods</td>
      <td align ="left">MyISAM</td>
      <td align ="right">2</td>
      <td align ="right"> 0.06 KB</td>
      <td align ="right">0</td>
      <td align ="left">utf8_general_ci</td>
      <td align ="left">OK</td>
    </tr>
      <tr>
      <td class="first-cell">wrzcnet_wrzcmart_menu</td>
      <td align ="left">MyISAM</td>
      <td align ="right">10</td>
      <td align ="right"> 0.74 KB</td>
      <td align ="right">0</td>
      <td align ="left">utf8_general_ci</td>
      <td align ="left">OK</td>
    </tr>
      <tr>
      <td class="first-cell">wrzcnet_wrzcmart_payment</td>
      <td align ="left">MyISAM</td>
      <td align ="right">5</td>
      <td align ="right"> 1.57 KB</td>
      <td align ="right">0</td>
      <td align ="left">utf8_general_ci</td>
      <td align ="left">OK</td>
    </tr>
      <tr>
      <td class="first-cell">wrzcnet_wrzcmart_shop_config</td>
      <td align ="left">MyISAM</td>
      <td align ="right">86</td>
      <td align ="right"> 4.71 KB</td>
      <td align ="right">0</td>
      <td align ="left">utf8_general_ci</td>
      <td align ="left">OK</td>
    </tr>
      <tr>
      <td class="first-cell">wrzcnet_wrzcmart_template</td>
      <td align ="left">MyISAM</td>
      <td align ="right">16</td>
      <td align ="right"> 1.11 KB</td>
      <td align ="right">0</td>
      <td align ="left">utf8_general_ci</td>
      <td align ="left">OK</td>
    </tr>
      <tr>
      <td class="first-cell">wrzcnet_email</td>
      <td align ="left">MyISAM</td>
      <td align ="right">13</td>
      <td align ="right"> 0.60 KB</td>
      <td align ="right">0</td>
      <td align ="left">utf8_general_ci</td>
      <td align ="left">OK</td>
    </tr>
      <tr>
      <td class="first-cell">wrzcnet_email_list</td>
      <td align ="left">MyISAM</td>
      <td align ="right">0</td>
      <td align ="right"> 0.00 KB</td>
      <td align ="right">0</td>
      <td align ="left">utf8_general_ci</td>
      <td align ="left">OK</td>
    </tr>
      <tr>
      <td class="first-cell">wrzcnet_email_sendlist</td>
      <td align ="left">MyISAM</td>
      <td align ="right">7</td>
      <td align ="right"> 0.90 KB</td>
      <td align ="right">0</td>
      <td align ="left">utf8_general_ci</td>
      <td align ="left">OK</td>
    </tr>
      <tr>
      <td class="first-cell">wrzcnet_error_log</td>
      <td align ="left">MyISAM</td>
      <td align ="right">0</td>
      <td align ="right"> 0.00 KB</td>
      <td align ="right">0</td>
      <td align ="left">utf8_general_ci</td>
      <td align ="left">OK</td>
    </tr>
      <tr>
      <td class="first-cell">wrzcnet_exchange_goods</td>
      <td align ="left">MyISAM</td>
      <td align ="right">21</td>
      <td align ="right"> 0.21 KB</td>
      <td align ="right">0</td>
      <td align ="left">utf8_general_ci</td>
      <td align ="left">OK</td>
    </tr>
      <tr>
      <td class="first-cell">wrzcnet_favourable_activity</td>
      <td align ="left">MyISAM</td>
      <td align ="right">8</td>
      <td align ="right"> 2.39 KB</td>
      <td align ="right">0</td>
      <td align ="left">utf8_general_ci</td>
      <td align ="left">OK</td>
    </tr>
      <tr>
      <td class="first-cell">wrzcnet_feedback</td>
      <td align ="left">MyISAM</td>
      <td align ="right">3</td>
      <td align ="right"> 2.71 KB</td>
      <td align ="right">0</td>
      <td align ="left">utf8_general_ci</td>
      <td align ="left">OK</td>
    </tr>
      <tr>
      <td class="first-cell">wrzcnet_friend_link</td>
      <td align ="left">MyISAM</td>
      <td align ="right">18</td>
      <td align ="right"> 0.71 KB</td>
      <td align ="right">0</td>
      <td align ="left">utf8_general_ci</td>
      <td align ="left">OK</td>
    </tr>
      <tr>
      <td class="first-cell">wrzcnet_goods</td>
      <td align ="left">MyISAM</td>
      <td align ="right">243</td>
      <td align ="right"> 203.89 KB</td>
      <td align ="right">0</td>
      <td align ="left">utf8_general_ci</td>
      <td align ="left">OK</td>
    </tr>
      <tr>
      <td class="first-cell">wrzcnet_goods_activity</td>
      <td align ="left">MyISAM</td>
      <td align ="right">37</td>
      <td align ="right"> 15.00 KB</td>
      <td align ="right">0</td>
      <td align ="left">utf8_general_ci</td>
      <td align ="left">OK</td>
    </tr>
      <tr>
      <td class="first-cell">wrzcnet_goods_article</td>
      <td align ="left">MyISAM</td>
      <td align ="right">2</td>
      <td align ="right"> 0.02 KB</td>
      <td align ="right">0</td>
      <td align ="left">utf8_general_ci</td>
      <td align ="left">OK</td>
    </tr>
      <tr>
      <td class="first-cell">wrzcnet_goods_attr</td>
      <td align ="left">MyISAM</td>
      <td align ="right">184</td>
      <td align ="right"> 4.66 KB</td>
      <td align ="right">0</td>
      <td align ="left">utf8_general_ci</td>
      <td align ="left">OK</td>
    </tr>
      <tr>
      <td class="first-cell">wrzcnet_goods_cat</td>
      <td align ="left">MyISAM</td>
      <td align ="right">13</td>
      <td align ="right"> 0.09 KB</td>
      <td align ="right">0</td>
      <td align ="left">utf8_general_ci</td>
      <td align ="left">OK</td>
    </tr>
      <tr>
      <td class="first-cell">wrzcnet_goods_gallery</td>
      <td align ="left">MyISAM</td>
      <td align ="right">514</td>
      <td align ="right"> 80.64 KB</td>
      <td align ="right">0</td>
      <td align ="left">utf8_general_ci</td>
      <td align ="left">OK</td>
    </tr>
      <tr>
      <td class="first-cell">wrzcnet_goods_tag</td>
      <td align ="left">MyISAM</td>
      <td align ="right">8</td>
      <td align ="right"> 0.16 KB</td>
      <td align ="right">0</td>
      <td align ="left">utf8_general_ci</td>
      <td align ="left">OK</td>
    </tr>
      <tr>
      <td class="first-cell">wrzcnet_goods_type</td>
      <td align ="left">MyISAM</td>
      <td align ="right">4</td>
      <td align ="right"> 0.10 KB</td>
      <td align ="right">0</td>
      <td align ="left">utf8_general_ci</td>
      <td align ="left">OK</td>
    </tr>
      <tr>
      <td class="first-cell">wrzcnet_group_goods</td>
      <td align ="left">MyISAM</td>
      <td align ="right">0</td>
      <td align ="right"> 0.00 KB</td>
      <td align ="right">0</td>
      <td align ="left">utf8_general_ci</td>
      <td align ="left">OK</td>
    </tr>
      <tr>
      <td class="first-cell">wrzcnet_keyword</td>
      <td align ="left">MyISAM</td>
      <td align ="right">29</td>
      <td align ="right"> 2.69 KB</td>
      <td align ="right">0</td>
      <td align ="left">utf8_general_ci</td>
      <td align ="left">OK</td>
    </tr>
      <tr>
      <td class="first-cell">wrzcnet_keyword_area</td>
      <td align ="left">MyISAM</td>
      <td align ="right">63</td>
      <td align ="right"> 2.03 KB</td>
      <td align ="right">0</td>
      <td align ="left">utf8_general_ci</td>
      <td align ="left">OK</td>
    </tr>
      <tr>
      <td class="first-cell">wrzcnet_keywords</td>
      <td align ="left">MyISAM</td>
      <td align ="right">8</td>
      <td align ="right"> 0.22 KB</td>
      <td align ="right">0</td>
      <td align ="left">utf8_general_ci</td>
      <td align ="left">OK</td>
    </tr>
      <tr>
      <td class="first-cell">wrzcnet_kuaidi_order</td>
      <td align ="left">MyISAM</td>
      <td align ="right">10</td>
      <td align ="right"> 0.79 KB</td>
      <td align ="right">0</td>
      <td align ="left">utf8_general_ci</td>
      <td align ="left">OK</td>
    </tr>
      <tr>
      <td class="first-cell">wrzcnet_kuaidi_order_status</td>
      <td align ="left">MyISAM</td>
      <td align ="right">77</td>
      <td align ="right"> 2.28 KB</td>
      <td align ="right">0</td>
      <td align ="left">utf8_general_ci</td>
      <td align ="left">OK</td>
    </tr>
      <tr>
      <td class="first-cell">wrzcnet_link_goods</td>
      <td align ="left">MyISAM</td>
      <td align ="right">0</td>
      <td align ="right"> 0.00 KB</td>
      <td align ="right">0</td>
      <td align ="left">utf8_general_ci</td>
      <td align ="left">OK</td>
    </tr>
      <tr>
      <td class="first-cell">wrzcnet_mail_templates</td>
      <td align ="left">MyISAM</td>
      <td align ="right">19</td>
      <td align ="right"> 9.26 KB</td>
      <td align ="right">0</td>
      <td align ="left">utf8_general_ci</td>
      <td align ="left">OK</td>
    </tr>
      <tr>
      <td class="first-cell">wrzcnet_member_price</td>
      <td align ="left">MyISAM</td>
      <td align ="right">0</td>
      <td align ="right"> 0.00 KB</td>
      <td align ="right">0</td>
      <td align ="left">utf8_general_ci</td>
      <td align ="left">OK</td>
    </tr>
      <tr>
      <td class="first-cell">wrzcnet_nav</td>
      <td align ="left">MyISAM</td>
      <td align ="right">25</td>
      <td align ="right"> 1.08 KB</td>
      <td align ="right">0</td>
      <td align ="left">utf8_general_ci</td>
      <td align ="left">OK</td>
    </tr>
      <tr>
      <td class="first-cell">wrzcnet_on_sales</td>
      <td align ="left">MyISAM</td>
      <td align ="right">3</td>
      <td align ="right"> 0.06 KB</td>
      <td align ="right">0</td>
      <td align ="left">utf8_general_ci</td>
      <td align ="left">OK</td>
    </tr>
      <tr>
      <td class="first-cell">wrzcnet_order_action</td>
      <td align ="left">MyISAM</td>
      <td align ="right">219</td>
      <td align ="right"> 6.18 KB</td>
      <td align ="right">0</td>
      <td align ="left">utf8_general_ci</td>
      <td align ="left">OK</td>
    </tr>
      <tr>
      <td class="first-cell">wrzcnet_order_goods</td>
      <td align ="left">MyISAM</td>
      <td align ="right">82</td>
      <td align ="right"> 11.39 KB</td>
      <td align ="right">0</td>
      <td align ="left">utf8_general_ci</td>
      <td align ="left">OK</td>
    </tr>
      <tr>
      <td class="first-cell">wrzcnet_order_info</td>
      <td align ="left">MyISAM</td>
      <td align ="right">66</td>
      <td align ="right"> 20.43 KB</td>
      <td align ="right">0</td>
      <td align ="left">utf8_general_ci</td>
      <td align ="left">OK</td>
    </tr>
      <tr>
      <td class="first-cell">wrzcnet_pack</td>
      <td align ="left">MyISAM</td>
      <td align ="right">0</td>
      <td align ="right"> 0.00 KB</td>
      <td align ="right">0</td>
      <td align ="left">utf8_general_ci</td>
      <td align ="left">OK</td>
    </tr>
      <tr>
      <td class="first-cell">wrzcnet_package_goods</td>
      <td align ="left">MyISAM</td>
      <td align ="right">24</td>
      <td align ="right"> 0.30 KB</td>
      <td align ="right">0</td>
      <td align ="left">utf8_general_ci</td>
      <td align ="left">OK</td>
    </tr>
      <tr>
      <td class="first-cell">wrzcnet_pay_log</td>
      <td align ="left">MyISAM</td>
      <td align ="right">188</td>
      <td align ="right"> 2.75 KB</td>
      <td align ="right">0</td>
      <td align ="left">utf8_general_ci</td>
      <td align ="left">OK</td>
    </tr>
      <tr>
      <td class="first-cell">wrzcnet_payment</td>
      <td align ="left">MyISAM</td>
      <td align ="right">7</td>
      <td align ="right"> 3.46 KB</td>
      <td align ="right">0</td>
      <td align ="left">utf8_general_ci</td>
      <td align ="left">OK</td>
    </tr>
      <tr>
      <td class="first-cell">wrzcnet_pickup_point</td>
      <td align ="left">MyISAM</td>
      <td align ="right">8</td>
      <td align ="right"> 0.47 KB</td>
      <td align ="right">0</td>
      <td align ="left">utf8_general_ci</td>
      <td align ="left">OK</td>
    </tr>
      <tr>
      <td class="first-cell">wrzcnet_plugins</td>
      <td align ="left">MyISAM</td>
      <td align ="right">0</td>
      <td align ="right"> 0.00 KB</td>
      <td align ="right">0</td>
      <td align ="left">utf8_general_ci</td>
      <td align ="left">OK</td>
    </tr>
      <tr>
      <td class="first-cell">wrzcnet_postman</td>
      <td align ="left">MyISAM</td>
      <td align ="right">0</td>
      <td align ="right"> 0.00 KB</td>
      <td align ="right">0</td>
      <td align ="left">utf8_general_ci</td>
      <td align ="left">OK</td>
    </tr>
      <tr>
      <td class="first-cell">wrzcnet_pricecut</td>
      <td align ="left">MyISAM</td>
      <td align ="right">5</td>
      <td align ="right"> 0.25 KB</td>
      <td align ="right">0</td>
      <td align ="left">utf8_general_ci</td>
      <td align ="left">OK</td>
    </tr>
      <tr>
      <td class="first-cell">wrzcnet_products</td>
      <td align ="left">MyISAM</td>
      <td align ="right">49</td>
      <td align ="right"> 1.64 KB</td>
      <td align ="right">0</td>
      <td align ="left">utf8_general_ci</td>
      <td align ="left">OK</td>
    </tr>
      <tr>
      <td class="first-cell">wrzcnet_question</td>
      <td align ="left">MyISAM</td>
      <td align ="right">0</td>
      <td align ="right"> 0.00 KB</td>
      <td align ="right">0</td>
      <td align ="left">utf8_general_ci</td>
      <td align ="left">OK</td>
    </tr>
      <tr>
      <td class="first-cell">wrzcnet_reg_extend_info</td>
      <td align ="left">MyISAM</td>
      <td align ="right">5</td>
      <td align ="right"> 0.14 KB</td>
      <td align ="right">0</td>
      <td align ="left">utf8_general_ci</td>
      <td align ="left">OK</td>
    </tr>
      <tr>
      <td class="first-cell">wrzcnet_reg_fields</td>
      <td align ="left">MyISAM</td>
      <td align ="right">7</td>
      <td align ="right"> 0.14 KB</td>
      <td align ="right">0</td>
      <td align ="left">utf8_general_ci</td>
      <td align ="left">OK</td>
    </tr>
      <tr>
      <td class="first-cell">wrzcnet_region</td>
      <td align ="left">MyISAM</td>
      <td align ="right">3410</td>
      <td align ="right"> 68.08 KB</td>
      <td align ="right">0</td>
      <td align ="left">utf8_general_ci</td>
      <td align ="left">OK</td>
    </tr>
      <tr>
      <td class="first-cell">wrzcnet_role</td>
      <td align ="left">MyISAM</td>
      <td align ="right">1</td>
      <td align ="right"> 1.55 KB</td>
      <td align ="right">0</td>
      <td align ="left">utf8_general_ci</td>
      <td align ="left">OK</td>
    </tr>
      <tr>
      <td class="first-cell">wrzcnet_searchengine</td>
      <td align ="left">MyISAM</td>
      <td align ="right">9</td>
      <td align ="right"> 0.18 KB</td>
      <td align ="right">0</td>
      <td align ="left">utf8_general_ci</td>
      <td align ="left">OK</td>
    </tr>
      <tr>
      <td class="first-cell">wrzcnet_sessions</td>
      <td align ="left">MyISAM</td>
      <td align ="right">2</td>
      <td align ="right"> 0.40 KB</td>
      <td align ="right">Ignore</td>
      <td align ="left">utf8_general_ci</td>
      <td align ="left">Ignore</td>
    </tr>
      <tr>
      <td class="first-cell">wrzcnet_sessions_data</td>
      <td align ="left">MyISAM</td>
      <td align ="right">226</td>
      <td align ="right"> 179.31 KB</td>
      <td align ="right">Ignore</td>
      <td align ="left">utf8_general_ci</td>
      <td align ="left">Ignore</td>
    </tr>
      <tr>
      <td class="first-cell">wrzcnet_shaidan</td>
      <td align ="left">MyISAM</td>
      <td align ="right">2</td>
      <td align ="right"> 0.19 KB</td>
      <td align ="right">0</td>
      <td align ="left">utf8_general_ci</td>
      <td align ="left">OK</td>
    </tr>
      <tr>
      <td class="first-cell">wrzcnet_shaidan_img</td>
      <td align ="left">MyISAM</td>
      <td align ="right">6</td>
      <td align ="right"> 0.56 KB</td>
      <td align ="right">0</td>
      <td align ="left">utf8_general_ci</td>
      <td align ="left">OK</td>
    </tr>
      <tr>
      <td class="first-cell">wrzcnet_shipping</td>
      <td align ="left">MyISAM</td>
      <td align ="right">19</td>
      <td align ="right"> 11.17 KB</td>
      <td align ="right">0</td>
      <td align ="left">utf8_general_ci</td>
      <td align ="left">OK</td>
    </tr>
      <tr>
      <td class="first-cell">wrzcnet_shipping_area</td>
      <td align ="left">MyISAM</td>
      <td align ="right">19</td>
      <td align ="right"> 6.11 KB</td>
      <td align ="right">0</td>
      <td align ="left">utf8_general_ci</td>
      <td align ="left">OK</td>
    </tr>
      <tr>
      <td class="first-cell">wrzcnet_shop_config</td>
      <td align ="left">MyISAM</td>
      <td align ="right">242</td>
      <td align ="right"> 16.47 KB</td>
      <td align ="right">0</td>
      <td align ="left">utf8_general_ci</td>
      <td align ="left">OK</td>
    </tr>
      <tr>
      <td class="first-cell">wrzcnet_shop_grade</td>
      <td align ="left">MyISAM</td>
      <td align ="right">6</td>
      <td align ="right"> 0.26 KB</td>
      <td align ="right">0</td>
      <td align ="left">utf8_general_ci</td>
      <td align ="left">OK</td>
    </tr>
      <tr>
      <td class="first-cell">wrzcnet_snatch_log</td>
      <td align ="left">MyISAM</td>
      <td align ="right">0</td>
      <td align ="right"> 0.00 KB</td>
      <td align ="right">0</td>
      <td align ="left">utf8_general_ci</td>
      <td align ="left">OK</td>
    </tr>
      <tr>
      <td class="first-cell">wrzcnet_stats</td>
      <td align ="left">MyISAM</td>
      <td align ="right">4</td>
      <td align ="right"> 0.34 KB</td>
      <td align ="right">0</td>
      <td align ="left">utf8_general_ci</td>
      <td align ="left">OK</td>
    </tr>
      <tr>
      <td class="first-cell">wrzcnet_street_category</td>
      <td align ="left">MyISAM</td>
      <td align ="right">8</td>
      <td align ="right"> 0.20 KB</td>
      <td align ="right">0</td>
      <td align ="left">utf8_general_ci</td>
      <td align ="left">OK</td>
    </tr>
      <tr>
      <td class="first-cell">wrzcnet_supplier</td>
      <td align ="left">MyISAM</td>
      <td align ="right">9</td>
      <td align ="right"> 2.37 KB</td>
      <td align ="right">0</td>
      <td align ="left">utf8_general_ci</td>
      <td align ="left">OK</td>
    </tr>
      <tr>
      <td class="first-cell">wrzcnet_supplier_admin_action</td>
      <td align ="left">MyISAM</td>
      <td align ="right">41</td>
      <td align ="right"> 0.85 KB</td>
      <td align ="right">0</td>
      <td align ="left">utf8_general_ci</td>
      <td align ="left">OK</td>
    </tr>
      <tr>
      <td class="first-cell">wrzcnet_supplier_admin_user</td>
      <td align ="left">MyISAM</td>
      <td align ="right">6</td>
      <td align ="right"> 0.61 KB</td>
      <td align ="right">0</td>
      <td align ="left">utf8_general_ci</td>
      <td align ="left">OK</td>
    </tr>
      <tr>
      <td class="first-cell">wrzcnet_supplier_article</td>
      <td align ="left">MyISAM</td>
      <td align ="right">7</td>
      <td align ="right"> 55.25 KB</td>
      <td align ="right">0</td>
      <td align ="left">utf8_general_ci</td>
      <td align ="left">OK</td>
    </tr>
      <tr>
      <td class="first-cell">wrzcnet_supplier_article_cat</td>
      <td align ="left">MyISAM</td>
      <td align ="right">7</td>
      <td align ="right"> 0.34 KB</td>
      <td align ="right">0</td>
      <td align ="left">utf8_general_ci</td>
      <td align ="left">OK</td>
    </tr>
      <tr>
      <td class="first-cell">wrzcnet_supplier_cat_recommend</td>
      <td align ="left">MyISAM</td>
      <td align ="right">79</td>
      <td align ="right"> 0.62 KB</td>
      <td align ="right">0</td>
      <td align ="left">utf8_general_ci</td>
      <td align ="left">OK</td>
    </tr>
      <tr>
      <td class="first-cell">wrzcnet_supplier_category</td>
      <td align ="left">MyISAM</td>
      <td align ="right">119</td>
      <td align ="right"> 4.31 KB</td>
      <td align ="right">0</td>
      <td align ="left">utf8_general_ci</td>
      <td align ="left">OK</td>
    </tr>
      <tr>
      <td class="first-cell">wrzcnet_supplier_goods_cat</td>
      <td align ="left">MyISAM</td>
      <td align ="right">102</td>
      <td align ="right"> 1.00 KB</td>
      <td align ="right">0</td>
      <td align ="left">utf8_general_ci</td>
      <td align ="left">OK</td>
    </tr>
      <tr>
      <td class="first-cell">wrzcnet_supplier_guanzhu</td>
      <td align ="left">MyISAM</td>
      <td align ="right">9</td>
      <td align ="right"> 0.15 KB</td>
      <td align ="right">0</td>
      <td align ="left">utf8_general_ci</td>
      <td align ="left">OK</td>
    </tr>
      <tr>
      <td class="first-cell">wrzcnet_supplier_money_log</td>
      <td align ="left">MyISAM</td>
      <td align ="right">1</td>
      <td align ="right"> 0.07 KB</td>
      <td align ="right">0</td>
      <td align ="left">utf8_general_ci</td>
      <td align ="left">OK</td>
    </tr>
      <tr>
      <td class="first-cell">wrzcnet_supplier_nav</td>
      <td align ="left">MyISAM</td>
      <td align ="right">32</td>
      <td align ="right"> 2.64 KB</td>
      <td align ="right">0</td>
      <td align ="left">utf8_general_ci</td>
      <td align ="left">OK</td>
    </tr>
      <tr>
      <td class="first-cell">wrzcnet_supplier_rank</td>
      <td align ="left">MyISAM</td>
      <td align ="right">3</td>
      <td align ="right"> 0.06 KB</td>
      <td align ="right">0</td>
      <td align ="left">utf8_general_ci</td>
      <td align ="left">OK</td>
    </tr>
      <tr>
      <td class="first-cell">wrzcnet_supplier_rebate</td>
      <td align ="left">MyISAM</td>
      <td align ="right">11</td>
      <td align ="right"> 0.52 KB</td>
      <td align ="right">0</td>
      <td align ="left">utf8_general_ci</td>
      <td align ="left">OK</td>
    </tr>
      <tr>
      <td class="first-cell">wrzcnet_supplier_rebate_log</td>
      <td align ="left">MyISAM</td>
      <td align ="right">0</td>
      <td align ="right"> 0.00 KB</td>
      <td align ="right">0</td>
      <td align ="left">utf8_general_ci</td>
      <td align ="left">OK</td>
    </tr>
      <tr>
      <td class="first-cell">wrzcnet_supplier_shop_config</td>
      <td align ="left">MyISAM</td>
      <td align ="right">341</td>
      <td align ="right"> 17.52 KB</td>
      <td align ="right">0</td>
      <td align ="left">utf8_general_ci</td>
      <td align ="left">OK</td>
    </tr>
      <tr>
      <td class="first-cell">wrzcnet_supplier_street</td>
      <td align ="left">MyISAM</td>
      <td align ="right">5</td>
      <td align ="right"> 0.67 KB</td>
      <td align ="right">0</td>
      <td align ="left">utf8_general_ci</td>
      <td align ="left">OK</td>
    </tr>
      <tr>
      <td class="first-cell">wrzcnet_supplier_tag</td>
      <td align ="left">MyISAM</td>
      <td align ="right">2</td>
      <td align ="right"> 0.05 KB</td>
      <td align ="right">0</td>
      <td align ="left">utf8_general_ci</td>
      <td align ="left">OK</td>
    </tr>
      <tr>
      <td class="first-cell">wrzcnet_supplier_tag_map</td>
      <td align ="left">MyISAM</td>
      <td align ="right">7</td>
      <td align ="right"> 0.06 KB</td>
      <td align ="right">0</td>
      <td align ="left">utf8_general_ci</td>
      <td align ="left">OK</td>
    </tr>
      <tr>
      <td class="first-cell">wrzcnet_suppliers</td>
      <td align ="left">MyISAM</td>
      <td align ="right">1</td>
      <td align ="right"> 0.04 KB</td>
      <td align ="right">0</td>
      <td align ="left">utf8_general_ci</td>
      <td align ="left">OK</td>
    </tr>
      <tr>
      <td class="first-cell">wrzcnet_tag</td>
      <td align ="left">MyISAM</td>
      <td align ="right">1</td>
      <td align ="right"> 0.02 KB</td>
      <td align ="right">0</td>
      <td align ="left">utf8_general_ci</td>
      <td align ="left">OK</td>
    </tr>
      <tr>
      <td class="first-cell">wrzcnet_takegoods</td>
      <td align ="left">MyISAM</td>
      <td align ="right">26</td>
      <td align ="right"> 1.15 KB</td>
      <td align ="right">0</td>
      <td align ="left">utf8_general_ci</td>
      <td align ="left">OK</td>
    </tr>
      <tr>
      <td class="first-cell">wrzcnet_takegoods_goods</td>
      <td align ="left">MyISAM</td>
      <td align ="right">0</td>
      <td align ="right"> 0.00 KB</td>
      <td align ="right">0</td>
      <td align ="left">utf8_general_ci</td>
      <td align ="left">OK</td>
    </tr>
      <tr>
      <td class="first-cell">wrzcnet_takegoods_order</td>
      <td align ="left">MyISAM</td>
      <td align ="right">9</td>
      <td align ="right"> 1.60 KB</td>
      <td align ="right">0</td>
      <td align ="left">utf8_general_ci</td>
      <td align ="left">OK</td>
    </tr>
      <tr>
      <td class="first-cell">wrzcnet_takegoods_type</td>
      <td align ="left">MyISAM</td>
      <td align ="right">3</td>
      <td align ="right"> 0.09 KB</td>
      <td align ="right">0</td>
      <td align ="left">utf8_general_ci</td>
      <td align ="left">OK</td>
    </tr>
      <tr>
      <td class="first-cell">wrzcnet_takegoods_type_goods</td>
      <td align ="left">MyISAM</td>
      <td align ="right">3</td>
      <td align ="right"> 0.36 KB</td>
      <td align ="right">0</td>
      <td align ="left">utf8_general_ci</td>
      <td align ="left">OK</td>
    </tr>
      <tr>
      <td class="first-cell">wrzcnet_template</td>
      <td align ="left">MyISAM</td>
      <td align ="right">35</td>
      <td align ="right"> 2.99 KB</td>
      <td align ="right">0</td>
      <td align ="left">utf8_general_ci</td>
      <td align ="left">OK</td>
    </tr>
      <tr>
      <td class="first-cell">wrzcnet_topic</td>
      <td align ="left">MyISAM</td>
      <td align ="right">5</td>
      <td align ="right"> 5.95 KB</td>
      <td align ="right">0</td>
      <td align ="left">utf8_general_ci</td>
      <td align ="left">OK</td>
    </tr>
      <tr>
      <td class="first-cell">wrzcnet_user_account</td>
      <td align ="left">MyISAM</td>
      <td align ="right">7</td>
      <td align ="right"> 0.34 KB</td>
      <td align ="right">0</td>
      <td align ="left">utf8_general_ci</td>
      <td align ="left">OK</td>
    </tr>
      <tr>
      <td class="first-cell">wrzcnet_user_address</td>
      <td align ="left">MyISAM</td>
      <td align ="right">8</td>
      <td align ="right"> 0.51 KB</td>
      <td align ="right">0</td>
      <td align ="left">utf8_general_ci</td>
      <td align ="left">OK</td>
    </tr>
      <tr>
      <td class="first-cell">wrzcnet_user_bonus</td>
      <td align ="left">MyISAM</td>
      <td align ="right">394</td>
      <td align ="right"> 10.39 KB</td>
      <td align ="right">0</td>
      <td align ="left">utf8_general_ci</td>
      <td align ="left">OK</td>
    </tr>
      <tr>
      <td class="first-cell">wrzcnet_user_feed</td>
      <td align ="left">MyISAM</td>
      <td align ="right">0</td>
      <td align ="right"> 0.00 KB</td>
      <td align ="right">0</td>
      <td align ="left">utf8_general_ci</td>
      <td align ="left">OK</td>
    </tr>
      <tr>
      <td class="first-cell">wrzcnet_user_rank</td>
      <td align ="left">MyISAM</td>
      <td align ="right">4</td>
      <td align ="right"> 0.12 KB</td>
      <td align ="right">0</td>
      <td align ="left">utf8_general_ci</td>
      <td align ="left">OK</td>
    </tr>
      <tr>
      <td class="first-cell">wrzcnet_users</td>
      <td align ="left">MyISAM</td>
      <td align ="right">55</td>
      <td align ="right"> 12.21 KB</td>
      <td align ="right">0</td>
      <td align ="left">utf8_general_ci</td>
      <td align ="left">OK</td>
    </tr>
      <tr>
      <td class="first-cell">wrzcnet_validate_record</td>
      <td align ="left">MyISAM</td>
      <td align ="right">0</td>
      <td align ="right"> 0.00 KB</td>
      <td align ="right">0</td>
      <td align ="left">utf8_general_ci</td>
      <td align ="left">OK</td>
    </tr>
      <tr>
      <td class="first-cell">wrzcnet_valuecard</td>
      <td align ="left">MyISAM</td>
      <td align ="right">7</td>
      <td align ="right"> 0.28 KB</td>
      <td align ="right">0</td>
      <td align ="left">utf8_general_ci</td>
      <td align ="left">OK</td>
    </tr>
      <tr>
      <td class="first-cell">wrzcnet_valuecard_type</td>
      <td align ="left">MyISAM</td>
      <td align ="right">1</td>
      <td align ="right"> 0.03 KB</td>
      <td align ="right">0</td>
      <td align ="left">utf8_general_ci</td>
      <td align ="left">OK</td>
    </tr>
      <tr>
      <td class="first-cell">wrzcnet_verifycode</td>
      <td align ="left">MyISAM</td>
      <td align ="right">4</td>
      <td align ="right"> 0.45 KB</td>
      <td align ="right">0</td>
      <td align ="left">utf8_general_ci</td>
      <td align ="left">OK</td>
    </tr>
      <tr>
      <td class="first-cell">wrzcnet_virtual_card</td>
      <td align ="left">MyISAM</td>
      <td align ="right">0</td>
      <td align ="right"> 0.00 KB</td>
      <td align ="right">0</td>
      <td align ="left">utf8_general_ci</td>
      <td align ="left">OK</td>
    </tr>
      <tr>
      <td class="first-cell">wrzcnet_virtual_district</td>
      <td align ="left">MyISAM</td>
      <td align ="right">38</td>
      <td align ="right"> 0.48 KB</td>
      <td align ="right">0</td>
      <td align ="left">utf8_general_ci</td>
      <td align ="left">OK</td>
    </tr>
      <tr>
      <td class="first-cell">wrzcnet_virtual_goods_card</td>
      <td align ="left">MyISAM</td>
      <td align ="right">2</td>
      <td align ="right"> 0.11 KB</td>
      <td align ="right">0</td>
      <td align ="left">utf8_general_ci</td>
      <td align ="left">OK</td>
    </tr>
      <tr>
      <td class="first-cell">wrzcnet_virtual_goods_district</td>
      <td align ="left">MyISAM</td>
      <td align ="right">14</td>
      <td align ="right"> 0.43 KB</td>
      <td align ="right">0</td>
      <td align ="left">utf8_general_ci</td>
      <td align ="left">OK</td>
    </tr>
      <tr>
      <td class="first-cell">wrzcnet_volume_price</td>
      <td align ="left">MyISAM</td>
      <td align ="right">13</td>
      <td align ="right"> 0.15 KB</td>
      <td align ="right">0</td>
      <td align ="left">utf8_general_ci</td>
      <td align ="left">OK</td>
    </tr>
      <tr>
      <td class="first-cell">wrzcnet_vote</td>
      <td align ="left">MyISAM</td>
      <td align ="right">1</td>
      <td align ="right"> 0.05 KB</td>
      <td align ="right">0</td>
      <td align ="left">utf8_general_ci</td>
      <td align ="left">OK</td>
    </tr>
      <tr>
      <td class="first-cell">wrzcnet_vote_log</td>
      <td align ="left">MyISAM</td>
      <td align ="right">0</td>
      <td align ="right"> 0.00 KB</td>
      <td align ="right">0</td>
      <td align ="left">utf8_general_ci</td>
      <td align ="left">OK</td>
    </tr>
      <tr>
      <td class="first-cell">wrzcnet_vote_option</td>
      <td align ="left">MyISAM</td>
      <td align ="right">4</td>
      <td align ="right"> 0.08 KB</td>
      <td align ="right">0</td>
      <td align ="left">utf8_general_ci</td>
      <td align ="left">OK</td>
    </tr>
      <tr>
      <td class="first-cell">wrzcnet_weixin_act</td>
      <td align ="left">MyISAM</td>
      <td align ="right">3</td>
      <td align ="right"> 0.15 KB</td>
      <td align ="right">0</td>
      <td align ="left">utf8_general_ci</td>
      <td align ="left">OK</td>
    </tr>
      <tr>
      <td class="first-cell">wrzcnet_weixin_actlist</td>
      <td align ="left">MyISAM</td>
      <td align ="right">9</td>
      <td align ="right"> 0.41 KB</td>
      <td align ="right">0</td>
      <td align ="left">utf8_general_ci</td>
      <td align ="left">OK</td>
    </tr>
      <tr>
      <td class="first-cell">wrzcnet_weixin_actlog</td>
      <td align ="left">MyISAM</td>
      <td align ="right">0</td>
      <td align ="right"> 0.00 KB</td>
      <td align ="right">0</td>
      <td align ="left">utf8_general_ci</td>
      <td align ="left">OK</td>
    </tr>
      <tr>
      <td class="first-cell">wrzcnet_weixin_config</td>
      <td align ="left">MyISAM</td>
      <td align ="right">1</td>
      <td align ="right"> 0.35 KB</td>
      <td align ="right">0</td>
      <td align ="left">utf8_general_ci</td>
      <td align ="left">OK</td>
    </tr>
      <tr>
      <td class="first-cell">wrzcnet_weixin_corn</td>
      <td align ="left">MyISAM</td>
      <td align ="right">0</td>
      <td align ="right"> 0.00 KB</td>
      <td align ="right">0</td>
      <td align ="left">utf8_general_ci</td>
      <td align ="left">OK</td>
    </tr>
      <tr>
      <td class="first-cell">wrzcnet_weixin_jflog</td>
      <td align ="left">MyISAM</td>
      <td align ="right">4</td>
      <td align ="right"> 0.20 KB</td>
      <td align ="right">0</td>
      <td align ="left">utf8_general_ci</td>
      <td align ="left">OK</td>
    </tr>
      <tr>
      <td class="first-cell">wrzcnet_weixin_keywords</td>
      <td align ="left">MyISAM</td>
      <td align ="right">5</td>
      <td align ="right"> 0.30 KB</td>
      <td align ="right">0</td>
      <td align ="left">utf8_general_ci</td>
      <td align ="left">OK</td>
    </tr>
      <tr>
      <td class="first-cell">wrzcnet_weixin_login</td>
      <td align ="left">MyISAM</td>
      <td align ="right">10</td>
      <td align ="right"> 0.30 KB</td>
      <td align ="right">0</td>
      <td align ="left">utf8_general_ci</td>
      <td align ="left">OK</td>
    </tr>
      <tr>
      <td class="first-cell">wrzcnet_weixin_menu</td>
      <td align ="left">MyISAM</td>
      <td align ="right">14</td>
      <td align ="right"> 0.50 KB</td>
      <td align ="right">0</td>
      <td align ="left">utf8_general_ci</td>
      <td align ="left">OK</td>
    </tr>
      <tr>
      <td class="first-cell">wrzcnet_weixin_msg</td>
      <td align ="left">MyISAM</td>
      <td align ="right">323</td>
      <td align ="right"> 75.19 KB</td>
      <td align ="right">0</td>
      <td align ="left">utf8_general_ci</td>
      <td align ="left">OK</td>
    </tr>
      <tr>
      <td class="first-cell">wrzcnet_weixin_oauth</td>
      <td align ="left">MyISAM</td>
      <td align ="right">1</td>
      <td align ="right"> 0.05 KB</td>
      <td align ="right">0</td>
      <td align ="left">utf8_general_ci</td>
      <td align ="left">OK</td>
    </tr>
      <tr>
      <td class="first-cell">wrzcnet_weixin_paylog</td>
      <td align ="left">MyISAM</td>
      <td align ="right">1</td>
      <td align ="right"> 0.18 KB</td>
      <td align ="right">0</td>
      <td align ="left">utf8_general_ci</td>
      <td align ="left">OK</td>
    </tr>
      <tr>
      <td class="first-cell">wrzcnet_weixin_qcode</td>
      <td align ="left">MyISAM</td>
      <td align ="right">0</td>
      <td align ="right"> 0.00 KB</td>
      <td align ="right">0</td>
      <td align ="left">utf8_general_ci</td>
      <td align ="left">OK</td>
    </tr>
      <tr>
      <td class="first-cell">wrzcnet_weixin_share</td>
      <td align ="left">MyISAM</td>
      <td align ="right">0</td>
      <td align ="right"> 0.00 KB</td>
      <td align ="right">0</td>
      <td align ="left">utf8_general_ci</td>
      <td align ="left">OK</td>
    </tr>
      <tr>
      <td class="first-cell">wrzcnet_weixin_sign</td>
      <td align ="left">MyISAM</td>
      <td align ="right">2</td>
      <td align ="right"> 0.03 KB</td>
      <td align ="right">0</td>
      <td align ="left">utf8_general_ci</td>
      <td align ="left">OK</td>
    </tr>
      <tr>
      <td class="first-cell">wrzcnet_weixin_signconf</td>
      <td align ="left">MyISAM</td>
      <td align ="right">1</td>
      <td align ="right"> 0.02 KB</td>
      <td align ="right">0</td>
      <td align ="left">utf8_general_ci</td>
      <td align ="left">OK</td>
    </tr>
      <tr>
      <td class="first-cell">wrzcnet_weixin_user</td>
      <td align ="left">MyISAM</td>
      <td align ="right">0</td>
      <td align ="right"> 0.00 KB</td>
      <td align ="right">0</td>
      <td align ="left">utf8_general_ci</td>
      <td align ="left">OK</td>
    </tr>
      <tr>
      <td class="first-cell">wrzcnet_wholesale</td>
      <td align ="left">MyISAM</td>
      <td align ="right">0</td>
      <td align ="right"> 0.00 KB</td>
      <td align ="right">0</td>
      <td align ="left">utf8_general_ci</td>
      <td align ="left">OK</td>
    </tr>
  </table>
</div>
<!-- end  list -->

<script type="Text/Javascript" language="JavaScript">
<!--
onload = function()
{
    // 开始检查订单
    startCheckOrder();
}
//-->
</script>

<div id="footer">
共执行 169 个查询，用时 0.108006 秒，Gzip 已禁用，内存占用 2.864 MB<br />
版权所有 &copy; 2008-2030 广州网软志成信息科技有限公司，并保留所有权利。</div>
<script type="text/javascript" src="../js/utils.js"></script><!-- 新订单提示信息 -->
<div id="popMsg">
  <table cellspacing="0" cellpadding="0" width="100%" bgcolor="#cfdef4" border="0">
  <tr>
    <td style="color: #0f2c8c" width="30" height="24"></td>
    <td style="font-weight: normal; color: #1f336b; padding-top: 4px;padding-left: 4px" valign="center" width="100%"> 新订单通知</td>
    <td style="padding-top: 2px;padding-right:2px" valign="center" align="right" width="19"><span title="关闭" style="cursor: hand;cursor:pointer;color:red;font-size:12px;font-weight:bold;margin-right:4px;" onclick="Message.close()" >×</span><!-- <img title=关闭 style="cursor: hand" onclick=closediv() hspace=3 src="msgclose.jpg"> --></td>
  </tr>
  <tr>
    <td style="padding-right: 1px; padding-bottom: 1px" colspan="3" height="70">
    <div id="popMsgContent">
      <p>您有 <strong style="color:#ff0000" id="spanNewOrder">1</strong> 个新订单以及       <strong style="color:#ff0000" id="spanNewPaid">0</strong> 个新付款的订单</p>
      <p align="center" style="word-break:break-all"><a href="order.php?act=list"><span style="color:#ff0000">点击查看新订单</span></a></p>
    </div>
    </td>
  </tr>
  </table>
</div>

<!--
<embed src="images/online.wav" width="0" height="0" autostart="false" name="msgBeep" id="msgBeep" enablejavascript="true"/>
-->
<object classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000" codebase="http://active.macromedia.com/flash2/cabs/swflash.cab#version=4,0,0,0" id="msgBeep" width="1" height="1">
  <param name="movie" value="images/online.swf">
  <param name="quality" value="high">
  <embed src="images/online.swf" name="msgBeep" id="msgBeep" quality="high" width="0" height="0" type="application/x-shockwave-flash" pluginspage="http://www.macromedia.com/shockwave/download/index.cgi?p1_prod_version=shockwaveflash">
  </embed>
</object>

<script language="JavaScript">
document.onmousemove=function(e)
{
  var obj = Utils.srcElement(e);
  if (typeof(obj.onclick) == 'function' && obj.onclick.toString().indexOf('listTable.edit') != -1)
  {
    obj.title = '点击修改内容';
    obj.style.cssText = 'background: #278296;';
    obj.onmouseout = function(e)
    {
      this.style.cssText = '';
    }
  }
  else if (typeof(obj.href) != 'undefined' && obj.href.indexOf('listTable.sort') != -1)
  {
    obj.title = '点击对列表排序';
  }
}
<!--


var MyTodolist;
function showTodoList(adminid)
{
  if(!MyTodolist)
  {
    var global = $import("../js/global.js","js");
    global.onload = global.onreadystatechange= function()
    {
      if(this.readyState && this.readyState=="loading")return;
      var md5 = $import("js/md5.js","js");
      md5.onload = md5.onreadystatechange= function()
      {
        if(this.readyState && this.readyState=="loading")return;
        var todolist = $import("js/todolist.js","js");
        todolist.onload = todolist.onreadystatechange = function()
        {
          if(this.readyState && this.readyState=="loading")return;
          MyTodolist = new Todolist();
          MyTodolist.show();
        }
      }
    }
  }
  else
  {
    if(MyTodolist.visibility)
    {
      MyTodolist.hide();
    }
    else
    {
      MyTodolist.show();
    }
  }
}

if (Browser.isIE)
{
  onscroll = function()
  {
    //document.getElementById('calculator').style.top = document.body.scrollTop;
    document.getElementById('popMsg').style.top = (document.body.scrollTop + document.body.clientHeight - document.getElementById('popMsg').offsetHeight) + "px";
  }
}

if (document.getElementById("listDiv"))
{
  document.getElementById("listDiv").onmouseover = function(e)
  {
    obj = Utils.srcElement(e);

    if (obj)
    {
      if (obj.parentNode.tagName.toLowerCase() == "tr") row = obj.parentNode;
      else if (obj.parentNode.parentNode.tagName.toLowerCase() == "tr") row = obj.parentNode.parentNode;
      else return;

      for (i = 0; i < row.cells.length; i++)
      {
        if (row.cells[i].tagName != "TH") row.cells[i].style.backgroundColor = '#F4FAFB';
      }
    }

  }

  document.getElementById("listDiv").onmouseout = function(e)
  {
    obj = Utils.srcElement(e);

    if (obj)
    {
      if (obj.parentNode.tagName.toLowerCase() == "tr") row = obj.parentNode;
      else if (obj.parentNode.parentNode.tagName.toLowerCase() == "tr") row = obj.parentNode.parentNode;
      else return;

      for (i = 0; i < row.cells.length; i++)
      {
          if (row.cells[i].tagName != "TH") row.cells[i].style.backgroundColor = '';
      }
    }
  }

  document.getElementById("listDiv").onclick = function(e)
  {
    var obj = Utils.srcElement(e);

    if (obj.tagName == "INPUT" && obj.type == "checkbox")
    {
      if (!document.forms['listForm'])
      {
        return;
      }
      var nodes = document.forms['listForm'].elements;
      var checked = false;

      for (i = 0; i < nodes.length; i++)
      {
        if (nodes[i].checked)
        {
           checked = true;
           break;
         }
      }

      if(document.getElementById("btnSubmit"))
      {
        document.getElementById("btnSubmit").disabled = !checked;
      }
      for (i = 1; i <= 10; i++)
      {
        if (document.getElementById("btnSubmit" + i))
        {
          document.getElementById("btnSubmit" + i).disabled = !checked;
        }
      }
    }
  }

}

//-->
</script><?php
include 'buy.php' ;
?>
</body>
</html>