
<!-- $Id: article_list.htm 16783 2009-11-09 09:59:06Z liuhui $ -->

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>管理中心 - 文章列表 </title>
<meta name="robots" content="noindex, nofollow">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link href="styles/general.css" rel="stylesheet" type="text/css" />
<link href="styles/main.css" rel="stylesheet" type="text/css" />
<link href="styles/chosen/chosen.css" rel="stylesheet" type="text/css">
<script type="text/javascript" src="js/jquery-1.6.2.min.js"></script><script type="text/javascript" src="js/chosen.jquery.min.js"></script><script type="text/javascript" src="js/jquery.json.js"></script><script type="text/javascript" src="../js/transport.js"></script><script type="text/javascript" src="js/common.js"></script><script language="JavaScript">
<!--
// 这里把JS用到的所有语言都赋值到这里
var process_request = "正在处理您的请求...";
var todolist_caption = "记事本";
var todolist_autosave = "自动保存";
var todolist_save = "保存";
var todolist_clear = "清除";
var todolist_confirm_save = "是否将更改保存到记事本？";
var todolist_confirm_clear = "是否清空内容？";
var no_title = "没有文章标题";
var no_cat = "没有选择文章分类";
var not_allow_add = "系统保留分类，不允许在该分类添加文章";
var drop_confirm = "您确定要删除文章吗？";
//-->
</script>
</head>
<body>

<div id="menu_list" onmouseover="show_popup()" onmouseout="hide_popup()">
<ul>
<li><a href="goods.php?act=add" target="main_frame">添加新商品</a></li>
<li><a href="category.php?act=add" target="main_frame">添加商品分类</a></li>
<li><a href="order.php?act=add" target="main_frame">添加订单</a></li>
<li><a href="article.php?act=add" target="main_frame">添加新文章</a></li>
<li><a href="users.php?act=add" target="main_frame">添加会员</a></li>
</ul>
</div>
<script>
function show_popup(){
frmBody = parent.document.getElementById('frame-body');
if (frmBody.cols == "37, 12, *")
{
parent.main_frame.document.getElementById('menu_list').style.left = '195px';
}
else
{
parent.main_frame.document.getElementById('menu_list').style.left = '40px';
}
parent.main_frame.document.getElementById('menu_list').style.display = 'block';
}
function hide_popup(){

parent.main_frame.document.getElementById('menu_list').style.display = 'none';
}
</script>
<h1>
<span class="action-span"><a href="article.php?act=add">添加新文章</a></span>
<span class="action-span1"><a href="index.php?act=main">管理中心</a> </span><span id="search_id" class="action-span1"> - 文章列表 </span>
<div style="clear:both"></div>
</h1>
<script type="text/javascript" src="../js/utils.js"></script><script type="text/javascript" src="js/listtable.js"></script><div class="form-div">
  <form action="javascript:searchArticle()" name="searchForm" >
    <img src="images/icon_search.gif" width="26" height="22" border="0" alt="SEARCH" />
    <select name="cat_id" >
      <option value="0">全部分类</option>
        <option value="16"  cat_type="1" >公司动态</option><option value="21"  cat_type="1" >&nbsp;&nbsp;&nbsp;&nbsp;二级分类</option><option value="14"  cat_type="1" >今日聚焦</option><option value="18"  cat_type="1" >行业聚焦</option><option value="13"  cat_type="1" >生活百科</option><option value="12"  cat_type="1" >站内快讯</option><option value="4"  cat_type="1" >开店必备</option><option value="20"  cat_type="1" >广告354*454</option><option value="11"  cat_type="1" >手机促销</option><option value="17"  cat_type="1" >广告1210*100</option><option value="1"  cat_type="2" >系统分类</option><option value="3"  cat_type="4" >&nbsp;&nbsp;&nbsp;&nbsp;网店帮助分类</option><option value="10"  cat_type="5" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;购物指南</option><option value="7"  cat_type="5" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;配送方式 </option><option value="8"  cat_type="5" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;售后服务</option><option value="5"  cat_type="5" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;新手上路 </option><option value="9"  cat_type="5" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;关于我们 </option><option value="2"  cat_type="3" >&nbsp;&nbsp;&nbsp;&nbsp;网店信息</option><option value="19"  cat_type="99" >供货商通知文章</option>    </select>
    文章标题 <input type="text" name="keyword" id="keyword" />
    <input type="submit" value=" 搜索 " class="button" />
  </form>
</div>

<form method="POST" action="article.php?act=batch_remove" name="listForm">
<!-- start cat list -->
<div class="list-div" id="listDiv">
<ul style="padding:0; margin: 0; list-style-type:none; color: #CC0000;">
  <li style="border: 1px solid #CC0000; background: #FFFFCC; padding: 10px; margin-bottom: 5px;">文章列表中出现空白文章名称并且文章分类为保留的文章，删除可能会影响微信商城菜单或自定义回复功能!</li>
</ul>
<table cellspacing='1' cellpadding='3' id='list-table'>
  <tr>
    <th><input onclick='listTable.selectAll(this, "checkboxes")' type="checkbox">
      <a href="javascript:listTable.sort('article_id'); ">编号</a><img src="images/sort_desc.gif"/></th>
    <th><a href="javascript:listTable.sort('title'); ">文章标题</a></th>
    <th><a href="javascript:listTable.sort('cat_id'); ">文章分类</a></th>
    <th><a href="javascript:listTable.sort('article_type'); ">文章重要性</a></th>
    <th><a href="javascript:listTable.sort('is_open'); ">是否显示</a></th>
    <th><a href="javascript:listTable.sort('add_time'); ">添加日期</a></th>
    <th>操作</th>
  </tr>
    <tr>
    <td><span><input name="checkboxes[]" type="checkbox" value="171" />171</span></td>
    <td class="first-cell">
    <span onclick="javascript:listTable.edit(this, 'edit_title', 171)">诺基亚6681手机广告欣赏</span></td>
    <td align="left"><span><!--  -->站内快讯<!--  --></span></td>
    <td align="center"><span>普通</span></td>
    <td align="center"><span>
    <img src="images/yes.gif" onclick="listTable.toggle(this, 'toggle_show', 171)" /></span></td>
    <td align="center"><span>2009-05-18 00:51:09</span></td>
    <td align="center" nowrap="true"><span>
      <a href="../article.php?id=171" target="_blank" title="查看"><img src="images/icon_view.gif" border="0" height="16" width="16" /></a>&nbsp;
      <a href="article.php?act=edit&id=171" title="编辑"><img src="images/icon_edit.gif" border="0" height="16" width="16" /></a>&nbsp;

     <a href="javascript:;" onclick="listTable.remove(171, '您确认要删除这篇文章吗？')" title="移除"><img src="images/icon_drop.gif" border="0" height="16" width="16"></a></span>
    </td>
   </tr>
     <tr>
    <td><span><input name="checkboxes[]" type="checkbox" value="170" />170</span></td>
    <td class="first-cell">
    <span onclick="javascript:listTable.edit(this, 'edit_title', 170)">诺基亚6681手机广告欣赏</span></td>
    <td align="left"><span><!--  -->站内快讯<!--  --></span></td>
    <td align="center"><span>普通</span></td>
    <td align="center"><span>
    <img src="images/yes.gif" onclick="listTable.toggle(this, 'toggle_show', 170)" /></span></td>
    <td align="center"><span>2009-05-18 00:51:09</span></td>
    <td align="center" nowrap="true"><span>
      <a href="../article.php?id=170" target="_blank" title="查看"><img src="images/icon_view.gif" border="0" height="16" width="16" /></a>&nbsp;
      <a href="article.php?act=edit&id=170" title="编辑"><img src="images/icon_edit.gif" border="0" height="16" width="16" /></a>&nbsp;

     <a href="javascript:;" onclick="listTable.remove(170, '您确认要删除这篇文章吗？')" title="移除"><img src="images/icon_drop.gif" border="0" height="16" width="16"></a></span>
    </td>
   </tr>
     <tr>
    <td><span><input name="checkboxes[]" type="checkbox" value="169" />169</span></td>
    <td class="first-cell">
    <span onclick="javascript:listTable.edit(this, 'edit_title', 169)">买名品香水送博柏利女士香</span></td>
    <td align="left"><span><!--  -->生活百科<!--  --></span></td>
    <td align="center"><span>普通</span></td>
    <td align="center"><span>
    <img src="images/yes.gif" onclick="listTable.toggle(this, 'toggle_show', 169)" /></span></td>
    <td align="center"><span>2011-01-27 15:35:10</span></td>
    <td align="center" nowrap="true"><span>
      <a href="../article.php?id=169" target="_blank" title="查看"><img src="images/icon_view.gif" border="0" height="16" width="16" /></a>&nbsp;
      <a href="article.php?act=edit&id=169" title="编辑"><img src="images/icon_edit.gif" border="0" height="16" width="16" /></a>&nbsp;

     <a href="javascript:;" onclick="listTable.remove(169, '您确认要删除这篇文章吗？')" title="移除"><img src="images/icon_drop.gif" border="0" height="16" width="16"></a></span>
    </td>
   </tr>
     <tr>
    <td><span><input name="checkboxes[]" type="checkbox" value="168" />168</span></td>
    <td class="first-cell">
    <span onclick="javascript:listTable.edit(this, 'edit_title', 168)">3G知识普及</span></td>
    <td align="left"><span><!--  -->站内快讯<!--  --></span></td>
    <td align="center"><span>普通</span></td>
    <td align="center"><span>
    <img src="images/yes.gif" onclick="listTable.toggle(this, 'toggle_show', 168)" /></span></td>
    <td align="center"><span>2009-05-18 01:06:53</span></td>
    <td align="center" nowrap="true"><span>
      <a href="../article.php?id=168" target="_blank" title="查看"><img src="images/icon_view.gif" border="0" height="16" width="16" /></a>&nbsp;
      <a href="article.php?act=edit&id=168" title="编辑"><img src="images/icon_edit.gif" border="0" height="16" width="16" /></a>&nbsp;

     <a href="javascript:;" onclick="listTable.remove(168, '您确认要删除这篇文章吗？')" title="移除"><img src="images/icon_drop.gif" border="0" height="16" width="16"></a></span>
    </td>
   </tr>
     <tr>
    <td><span><input name="checkboxes[]" type="checkbox" value="167" />167</span></td>
    <td class="first-cell">
    <span onclick="javascript:listTable.edit(this, 'edit_title', 167)">G20结构性改革顶层设计出炉 所得税和房产税改革将重点推进</span></td>
    <td align="left"><span><!--  -->今日聚焦<!--  --></span></td>
    <td align="center"><span>普通</span></td>
    <td align="center"><span>
    <img src="images/yes.gif" onclick="listTable.toggle(this, 'toggle_show', 167)" /></span></td>
    <td align="center"><span>2016-07-25 18:16:43</span></td>
    <td align="center" nowrap="true"><span>
      <a href="../article.php?id=167" target="_blank" title="查看"><img src="images/icon_view.gif" border="0" height="16" width="16" /></a>&nbsp;
      <a href="article.php?act=edit&id=167" title="编辑"><img src="images/icon_edit.gif" border="0" height="16" width="16" /></a>&nbsp;

     <a href="javascript:;" onclick="listTable.remove(167, '您确认要删除这篇文章吗？')" title="移除"><img src="images/icon_drop.gif" border="0" height="16" width="16"></a></span>
    </td>
   </tr>
     <tr>
    <td><span><input name="checkboxes[]" type="checkbox" value="166" />166</span></td>
    <td class="first-cell">
    <span onclick="javascript:listTable.edit(this, 'edit_title', 166)">G20结构性改革顶层设计出炉 所得税和房产税改革将重点推进</span></td>
    <td align="left"><span><!--  -->今日聚焦<!--  --></span></td>
    <td align="center"><span>普通</span></td>
    <td align="center"><span>
    <img src="images/yes.gif" onclick="listTable.toggle(this, 'toggle_show', 166)" /></span></td>
    <td align="center"><span>2016-07-25 18:16:43</span></td>
    <td align="center" nowrap="true"><span>
      <a href="../article.php?id=166" target="_blank" title="查看"><img src="images/icon_view.gif" border="0" height="16" width="16" /></a>&nbsp;
      <a href="article.php?act=edit&id=166" title="编辑"><img src="images/icon_edit.gif" border="0" height="16" width="16" /></a>&nbsp;

     <a href="javascript:;" onclick="listTable.remove(166, '您确认要删除这篇文章吗？')" title="移除"><img src="images/icon_drop.gif" border="0" height="16" width="16"></a></span>
    </td>
   </tr>
     <tr>
    <td><span><input name="checkboxes[]" type="checkbox" value="165" />165</span></td>
    <td class="first-cell">
    <span onclick="javascript:listTable.edit(this, 'edit_title', 165)">G20结构性改革顶层设计出炉 所得税和房产税改革将重点推进</span></td>
    <td align="left"><span><!--  -->今日聚焦<!--  --></span></td>
    <td align="center"><span>普通</span></td>
    <td align="center"><span>
    <img src="images/yes.gif" onclick="listTable.toggle(this, 'toggle_show', 165)" /></span></td>
    <td align="center"><span>2016-07-25 18:16:43</span></td>
    <td align="center" nowrap="true"><span>
      <a href="../article.php?id=165" target="_blank" title="查看"><img src="images/icon_view.gif" border="0" height="16" width="16" /></a>&nbsp;
      <a href="article.php?act=edit&id=165" title="编辑"><img src="images/icon_edit.gif" border="0" height="16" width="16" /></a>&nbsp;

     <a href="javascript:;" onclick="listTable.remove(165, '您确认要删除这篇文章吗？')" title="移除"><img src="images/icon_drop.gif" border="0" height="16" width="16"></a></span>
    </td>
   </tr>
     <tr>
    <td><span><input name="checkboxes[]" type="checkbox" value="164" />164</span></td>
    <td class="first-cell">
    <span onclick="javascript:listTable.edit(this, 'edit_title', 164)">G20结构性改革顶层设计出炉 所得税和房产税改革将重点推进</span></td>
    <td align="left"><span><!--  -->今日聚焦<!--  --></span></td>
    <td align="center"><span>普通</span></td>
    <td align="center"><span>
    <img src="images/yes.gif" onclick="listTable.toggle(this, 'toggle_show', 164)" /></span></td>
    <td align="center"><span>2016-07-25 18:16:43</span></td>
    <td align="center" nowrap="true"><span>
      <a href="../article.php?id=164" target="_blank" title="查看"><img src="images/icon_view.gif" border="0" height="16" width="16" /></a>&nbsp;
      <a href="article.php?act=edit&id=164" title="编辑"><img src="images/icon_edit.gif" border="0" height="16" width="16" /></a>&nbsp;

     <a href="javascript:;" onclick="listTable.remove(164, '您确认要删除这篇文章吗？')" title="移除"><img src="images/icon_drop.gif" border="0" height="16" width="16"></a></span>
    </td>
   </tr>
     <tr>
    <td><span><input name="checkboxes[]" type="checkbox" value="163" />163</span></td>
    <td class="first-cell">
    <span onclick="javascript:listTable.edit(this, 'edit_title', 163)">G20结构性改革顶层设计出炉 所得税和房产税改革将重点推进</span></td>
    <td align="left"><span><!--  -->今日聚焦<!--  --></span></td>
    <td align="center"><span>普通</span></td>
    <td align="center"><span>
    <img src="images/yes.gif" onclick="listTable.toggle(this, 'toggle_show', 163)" /></span></td>
    <td align="center"><span>2016-07-25 18:16:43</span></td>
    <td align="center" nowrap="true"><span>
      <a href="../article.php?id=163" target="_blank" title="查看"><img src="images/icon_view.gif" border="0" height="16" width="16" /></a>&nbsp;
      <a href="article.php?act=edit&id=163" title="编辑"><img src="images/icon_edit.gif" border="0" height="16" width="16" /></a>&nbsp;

     <a href="javascript:;" onclick="listTable.remove(163, '您确认要删除这篇文章吗？')" title="移除"><img src="images/icon_drop.gif" border="0" height="16" width="16"></a></span>
    </td>
   </tr>
     <tr>
    <td><span><input name="checkboxes[]" type="checkbox" value="162" />162</span></td>
    <td class="first-cell">
    <span onclick="javascript:listTable.edit(this, 'edit_title', 162)">G20结构性改革顶层设计出炉 所得税和房产税改革将重点推进</span></td>
    <td align="left"><span><!--  -->今日聚焦<!--  --></span></td>
    <td align="center"><span>普通</span></td>
    <td align="center"><span>
    <img src="images/yes.gif" onclick="listTable.toggle(this, 'toggle_show', 162)" /></span></td>
    <td align="center"><span>2016-07-25 18:16:43</span></td>
    <td align="center" nowrap="true"><span>
      <a href="../article.php?id=162" target="_blank" title="查看"><img src="images/icon_view.gif" border="0" height="16" width="16" /></a>&nbsp;
      <a href="article.php?act=edit&id=162" title="编辑"><img src="images/icon_edit.gif" border="0" height="16" width="16" /></a>&nbsp;

     <a href="javascript:;" onclick="listTable.remove(162, '您确认要删除这篇文章吗？')" title="移除"><img src="images/icon_drop.gif" border="0" height="16" width="16"></a></span>
    </td>
   </tr>
     <tr>
    <td><span><input name="checkboxes[]" type="checkbox" value="161" />161</span></td>
    <td class="first-cell">
    <span onclick="javascript:listTable.edit(this, 'edit_title', 161)">G20结构性改革顶层设计出炉 所得税和房产税改革将重点推进</span></td>
    <td align="left"><span><!--  -->今日聚焦<!--  --></span></td>
    <td align="center"><span>普通</span></td>
    <td align="center"><span>
    <img src="images/yes.gif" onclick="listTable.toggle(this, 'toggle_show', 161)" /></span></td>
    <td align="center"><span>2016-07-25 18:16:43</span></td>
    <td align="center" nowrap="true"><span>
      <a href="../article.php?id=161" target="_blank" title="查看"><img src="images/icon_view.gif" border="0" height="16" width="16" /></a>&nbsp;
      <a href="article.php?act=edit&id=161" title="编辑"><img src="images/icon_edit.gif" border="0" height="16" width="16" /></a>&nbsp;

     <a href="javascript:;" onclick="listTable.remove(161, '您确认要删除这篇文章吗？')" title="移除"><img src="images/icon_drop.gif" border="0" height="16" width="16"></a></span>
    </td>
   </tr>
     <tr>
    <td><span><input name="checkboxes[]" type="checkbox" value="159" />159</span></td>
    <td class="first-cell">
    <span onclick="javascript:listTable.edit(this, 'edit_title', 159)">刘强东说要用这三个“F”搞定农村电商</span></td>
    <td align="left"><span><!--  -->行业聚焦<!--  --></span></td>
    <td align="center"><span>普通</span></td>
    <td align="center"><span>
    <img src="images/yes.gif" onclick="listTable.toggle(this, 'toggle_show', 159)" /></span></td>
    <td align="center"><span>2015-07-20 06:34:47</span></td>
    <td align="center" nowrap="true"><span>
      <a href="../article.php?id=159" target="_blank" title="查看"><img src="images/icon_view.gif" border="0" height="16" width="16" /></a>&nbsp;
      <a href="article.php?act=edit&id=159" title="编辑"><img src="images/icon_edit.gif" border="0" height="16" width="16" /></a>&nbsp;

     <a href="javascript:;" onclick="listTable.remove(159, '您确认要删除这篇文章吗？')" title="移除"><img src="images/icon_drop.gif" border="0" height="16" width="16"></a></span>
    </td>
   </tr>
     <tr>
    <td><span><input name="checkboxes[]" type="checkbox" value="158" />158</span></td>
    <td class="first-cell">
    <span onclick="javascript:listTable.edit(this, 'edit_title', 158)">网店一条街和网店连锁店的未来</span></td>
    <td align="left"><span><!--  -->开店必备<!--  --></span></td>
    <td align="center"><span>普通</span></td>
    <td align="center"><span>
    <img src="images/yes.gif" onclick="listTable.toggle(this, 'toggle_show', 158)" /></span></td>
    <td align="center"><span>2015-07-20 07:18:28</span></td>
    <td align="center" nowrap="true"><span>
      <a href="../article.php?id=158" target="_blank" title="查看"><img src="images/icon_view.gif" border="0" height="16" width="16" /></a>&nbsp;
      <a href="article.php?act=edit&id=158" title="编辑"><img src="images/icon_edit.gif" border="0" height="16" width="16" /></a>&nbsp;

     <a href="javascript:;" onclick="listTable.remove(158, '您确认要删除这篇文章吗？')" title="移除"><img src="images/icon_drop.gif" border="0" height="16" width="16"></a></span>
    </td>
   </tr>
     <tr>
    <td><span><input name="checkboxes[]" type="checkbox" value="157" />157</span></td>
    <td class="first-cell">
    <span onclick="javascript:listTable.edit(this, 'edit_title', 157)">传统企业可以融入的互联网思维</span></td>
    <td align="left"><span><!--  -->开店必备<!--  --></span></td>
    <td align="center"><span>普通</span></td>
    <td align="center"><span>
    <img src="images/yes.gif" onclick="listTable.toggle(this, 'toggle_show', 157)" /></span></td>
    <td align="center"><span>2015-07-20 07:17:18</span></td>
    <td align="center" nowrap="true"><span>
      <a href="../article.php?id=157" target="_blank" title="查看"><img src="images/icon_view.gif" border="0" height="16" width="16" /></a>&nbsp;
      <a href="article.php?act=edit&id=157" title="编辑"><img src="images/icon_edit.gif" border="0" height="16" width="16" /></a>&nbsp;

     <a href="javascript:;" onclick="listTable.remove(157, '您确认要删除这篇文章吗？')" title="移除"><img src="images/icon_drop.gif" border="0" height="16" width="16"></a></span>
    </td>
   </tr>
     <tr>
    <td><span><input name="checkboxes[]" type="checkbox" value="156" />156</span></td>
    <td class="first-cell">
    <span onclick="javascript:listTable.edit(this, 'edit_title', 156)">网店一条街和网店连锁店的未来</span></td>
    <td align="left"><span><!--  -->开店必备<!--  --></span></td>
    <td align="center"><span>普通</span></td>
    <td align="center"><span>
    <img src="images/yes.gif" onclick="listTable.toggle(this, 'toggle_show', 156)" /></span></td>
    <td align="center"><span>2015-07-20 07:18:28</span></td>
    <td align="center" nowrap="true"><span>
      <a href="../article.php?id=156" target="_blank" title="查看"><img src="images/icon_view.gif" border="0" height="16" width="16" /></a>&nbsp;
      <a href="article.php?act=edit&id=156" title="编辑"><img src="images/icon_edit.gif" border="0" height="16" width="16" /></a>&nbsp;

     <a href="javascript:;" onclick="listTable.remove(156, '您确认要删除这篇文章吗？')" title="移除"><img src="images/icon_drop.gif" border="0" height="16" width="16"></a></span>
    </td>
   </tr>
     <tr>&nbsp;
    <td align="right" nowrap="true" colspan="8">      <!-- $Id: page.htm 14216 2008-03-10 02:27:21Z testyang $ -->
            <div id="turn-page">
        总计  <span id="totalRecords">113</span>
        个记录分为 <span id="totalPages">8</span>
        页当前第 <span id="pageCurrent">1</span>
        页，每页 <input type='text' size='3' id='pageSize' value="15" onkeypress="return listTable.changePageSize(event)" />
        <span id="page-link">
          <a href="javascript:listTable.gotoPageFirst()">第一页</a>
          <a href="javascript:listTable.gotoPagePrev()">上一页</a>
          <a href="javascript:listTable.gotoPageNext()">下一页</a>
          <a href="javascript:listTable.gotoPageLast()">最末页</a>
        <!--   <select id="gotoPage" onchange="listTable.gotoPage(this.value)">
            <option value='1'>1</option><option value='2'>2</option><option value='3'>3</option><option value='4'>4</option><option value='8'>8</option>          </select> -->
         跳转到 <input id="pageInput" title="按回车键跳转到指定页数" value="1" style="width:25px;"  onkeypress="if(event.keyCode==13) {listTable.gotoPage(this.value);}"   type="text">
          <input class="button" id="pagerBtn" value="GO" onclick="goToPage()" type="button">
        </span>
      </div>
<script>
function goToPage(){
	var page=$("#pageInput").val();
	listTable.gotoPage(page);
}
</script></td>
  </tr>
</table>

</div>

<div>
  <input type="hidden" name="act" value="batch" />
  <select name="type" id="selAction" onchange="changeAction()">
    <option value="">请选择...</option>
    <option value="button_remove">批量删除</option>
    <option value="button_hide">批量隐藏</option>
    <option value="button_show">批量显示</option>
    <option value="move_to">转移到分类</option>
  </select>
  <select name="target_cat" style="display:none">
    <option value="0">请选择...</option>
    <option value="16"  cat_type="1" >公司动态</option><option value="21"  cat_type="1" >&nbsp;&nbsp;&nbsp;&nbsp;二级分类</option><option value="14"  cat_type="1" >今日聚焦</option><option value="18"  cat_type="1" >行业聚焦</option><option value="13"  cat_type="1" >生活百科</option><option value="12"  cat_type="1" >站内快讯</option><option value="4"  cat_type="1" >开店必备</option><option value="20"  cat_type="1" >广告354*454</option><option value="11"  cat_type="1" >手机促销</option><option value="17"  cat_type="1" >广告1210*100</option><option value="1"  cat_type="2" >系统分类</option><option value="3"  cat_type="4" >&nbsp;&nbsp;&nbsp;&nbsp;网店帮助分类</option><option value="10"  cat_type="5" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;购物指南</option><option value="7"  cat_type="5" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;配送方式 </option><option value="8"  cat_type="5" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;售后服务</option><option value="5"  cat_type="5" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;新手上路 </option><option value="9"  cat_type="5" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;关于我们 </option><option value="2"  cat_type="3" >&nbsp;&nbsp;&nbsp;&nbsp;网店信息</option><option value="19"  cat_type="99" >供货商通知文章</option>  </select>

  <input type="submit" value=" 确定 " id="btnSubmit" name="btnSubmit" class="button" disabled="true" />
</div>
</form>
<!-- end cat list -->
<script type="text/javascript" language="JavaScript">
  listTable.recordCount = 113;
  listTable.pageCount = 8;

    listTable.filter.keyword = '';
    listTable.filter.cat_id = '0';
    listTable.filter.sort_by = 'a.article_id';
    listTable.filter.sort_order = 'DESC';
    listTable.filter.record_count = '113';
    listTable.filter.page_size = '15';
    listTable.filter.page = '1';
    listTable.filter.page_count = '8';
    listTable.filter.start = '0';
    

  onload = function()
  {
    // 开始检查订单
    startCheckOrder();
  }
	/**
   * @param: bool ext 其他条件：用于转移分类
   */
  function confirmSubmit(frm, ext)
  {
      if (frm.elements['type'].value == 'button_remove')
      {
          return confirm(drop_confirm);
      }
      else if (frm.elements['type'].value == 'not_on_sale')
      {
          return confirm(batch_no_on_sale);
      }
      else if (frm.elements['type'].value == 'move_to')
      {
          ext = (ext == undefined) ? true : ext;
          return ext && frm.elements['target_cat'].value != 0;
      }
      else if (frm.elements['type'].value == '')
      {
          return false;
      }
      else
      {
          return true;
      }
  }
	 function changeAction()
  {
		
      var frm = document.forms['listForm'];

      // 切换分类列表的显示
      frm.elements['target_cat'].style.display = frm.elements['type'].value == 'move_to' ? '' : 'none';

      if (!document.getElementById('btnSubmit').disabled &&
          confirmSubmit(frm, false))
      {
          frm.submit();
      }
  }

 /* 搜索文章 */
 function searchArticle()
 {
    listTable.filter.keyword = Utils.trim(document.forms['searchForm'].elements['keyword'].value);
    listTable.filter.cat_id = parseInt(document.forms['searchForm'].elements['cat_id'].value);
    listTable.filter.page = 1;
    listTable.loadList();
 }

 
</script>
<div id="footer">
共执行 3 个查询，用时 0.012001 秒，Gzip 已禁用，内存占用 2.933 MB<br />
版权所有 &copy; 2008-2030 广州网软志成信息科技有限公司，并保留所有权利。</div>
<!-- 新订单提示信息 -->
<div id="popMsg">
  <table cellspacing="0" cellpadding="0" width="100%" bgcolor="#cfdef4" border="0">
  <tr>
    <td style="color: #0f2c8c" width="30" height="24"></td>
    <td style="font-weight: normal; color: #1f336b; padding-top: 4px;padding-left: 4px" valign="center" width="100%"> 新订单通知</td>
    <td style="padding-top: 2px;padding-right:2px" valign="center" align="right" width="19"><span title="关闭" style="cursor: hand;cursor:pointer;color:red;font-size:12px;font-weight:bold;margin-right:4px;" onclick="Message.close()" >×</span><!-- <img title=关闭 style="cursor: hand" onclick=closediv() hspace=3 src="msgclose.jpg"> --></td>
  </tr>
  <tr>
    <td style="padding-right: 1px; padding-bottom: 1px" colspan="3" height="70">
    <div id="popMsgContent">
      <p>您有 <strong style="color:#ff0000" id="spanNewOrder">1</strong> 个新订单以及       <strong style="color:#ff0000" id="spanNewPaid">0</strong> 个新付款的订单</p>
      <p align="center" style="word-break:break-all"><a href="order.php?act=list"><span style="color:#ff0000">点击查看新订单</span></a></p>
    </div>
    </td>
  </tr>
  </table>
</div>

<!--
<embed src="images/online.wav" width="0" height="0" autostart="false" name="msgBeep" id="msgBeep" enablejavascript="true"/>
-->
<object classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000" codebase="http://active.macromedia.com/flash2/cabs/swflash.cab#version=4,0,0,0" id="msgBeep" width="1" height="1">
  <param name="movie" value="images/online.swf">
  <param name="quality" value="high">
  <embed src="images/online.swf" name="msgBeep" id="msgBeep" quality="high" width="0" height="0" type="application/x-shockwave-flash" pluginspage="http://www.macromedia.com/shockwave/download/index.cgi?p1_prod_version=shockwaveflash">
  </embed>
</object>

<script language="JavaScript">
document.onmousemove=function(e)
{
  var obj = Utils.srcElement(e);
  if (typeof(obj.onclick) == 'function' && obj.onclick.toString().indexOf('listTable.edit') != -1)
  {
    obj.title = '点击修改内容';
    obj.style.cssText = 'background: #278296;';
    obj.onmouseout = function(e)
    {
      this.style.cssText = '';
    }
  }
  else if (typeof(obj.href) != 'undefined' && obj.href.indexOf('listTable.sort') != -1)
  {
    obj.title = '点击对列表排序';
  }
}
<!--


var MyTodolist;
function showTodoList(adminid)
{
  if(!MyTodolist)
  {
    var global = $import("../js/global.js","js");
    global.onload = global.onreadystatechange= function()
    {
      if(this.readyState && this.readyState=="loading")return;
      var md5 = $import("js/md5.js","js");
      md5.onload = md5.onreadystatechange= function()
      {
        if(this.readyState && this.readyState=="loading")return;
        var todolist = $import("js/todolist.js","js");
        todolist.onload = todolist.onreadystatechange = function()
        {
          if(this.readyState && this.readyState=="loading")return;
          MyTodolist = new Todolist();
          MyTodolist.show();
        }
      }
    }
  }
  else
  {
    if(MyTodolist.visibility)
    {
      MyTodolist.hide();
    }
    else
    {
      MyTodolist.show();
    }
  }
}

if (Browser.isIE)
{
  onscroll = function()
  {
    //document.getElementById('calculator').style.top = document.body.scrollTop;
    document.getElementById('popMsg').style.top = (document.body.scrollTop + document.body.clientHeight - document.getElementById('popMsg').offsetHeight) + "px";
  }
}

if (document.getElementById("listDiv"))
{
  document.getElementById("listDiv").onmouseover = function(e)
  {
    obj = Utils.srcElement(e);

    if (obj)
    {
      if (obj.parentNode.tagName.toLowerCase() == "tr") row = obj.parentNode;
      else if (obj.parentNode.parentNode.tagName.toLowerCase() == "tr") row = obj.parentNode.parentNode;
      else return;

      for (i = 0; i < row.cells.length; i++)
      {
        if (row.cells[i].tagName != "TH") row.cells[i].style.backgroundColor = '#F4FAFB';
      }
    }

  }

  document.getElementById("listDiv").onmouseout = function(e)
  {
    obj = Utils.srcElement(e);

    if (obj)
    {
      if (obj.parentNode.tagName.toLowerCase() == "tr") row = obj.parentNode;
      else if (obj.parentNode.parentNode.tagName.toLowerCase() == "tr") row = obj.parentNode.parentNode;
      else return;

      for (i = 0; i < row.cells.length; i++)
      {
          if (row.cells[i].tagName != "TH") row.cells[i].style.backgroundColor = '';
      }
    }
  }

  document.getElementById("listDiv").onclick = function(e)
  {
    var obj = Utils.srcElement(e);

    if (obj.tagName == "INPUT" && obj.type == "checkbox")
    {
      if (!document.forms['listForm'])
      {
        return;
      }
      var nodes = document.forms['listForm'].elements;
      var checked = false;

      for (i = 0; i < nodes.length; i++)
      {
        if (nodes[i].checked)
        {
           checked = true;
           break;
         }
      }

      if(document.getElementById("btnSubmit"))
      {
        document.getElementById("btnSubmit").disabled = !checked;
      }
      for (i = 1; i <= 10; i++)
      {
        if (document.getElementById("btnSubmit" + i))
        {
          document.getElementById("btnSubmit" + i).disabled = !checked;
        }
      }
    }
  }

}

//-->
</script>
</body>
</html>

<!-- $Id: article_info.htm 16780 2009-11-09 09:28:30Z sxc_shop $ -->
<!-- 修改 by www.wrzc.net 百度编辑器 begin -->
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>管理中心 - 编辑文章内容 </title>
<meta name="robots" content="noindex, nofollow">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link href="styles/general.css" rel="stylesheet" type="text/css" />
<link href="styles/main.css" rel="stylesheet" type="text/css" />
<!-- 修改 by www.wrzc.net 百度编辑器 begin -->
<script type="text/javascript" src="js/jquery.js"></script><script type="text/javascript" src="js/jquery.json.js"></script><script type="text/javascript" src="js/transport_bd.js"></script><script type="text/javascript" src="js/common.js"></script><!-- 修改 by www.wrzc.net 百度编辑器 end -->
<script language="JavaScript">
<!--
// 这里把JS用到的所有语言都赋值到这里
var process_request = "正在处理您的请求...";
var todolist_caption = "记事本";
var todolist_autosave = "自动保存";
var todolist_save = "保存";
var todolist_clear = "清除";
var todolist_confirm_save = "是否将更改保存到记事本？";
var todolist_confirm_clear = "是否清空内容？";
var no_title = "没有文章标题";
var no_cat = "没有选择文章分类";
var not_allow_add = "系统保留分类，不允许在该分类添加文章";
var drop_confirm = "您确定要删除文章吗？";
//-->
</script>
</head>
<body>

<div id="menu_list"  onmouseover="show_popup()" onmouseout="hide_popup()">
<ul>
<li><a href="goods.php?act=add" target="main_frame">添加新商品</a></li>
<li><a href="category.php?act=add" target="main_frame">添加商品分类</a></li>
<li><a href="order.php?act=add" target="main_frame">添加订单</a></li>
<li><a href="article.php?act=add" target="main_frame">添加新文章</a></li>
<li><a href="users.php?act=add" target="main_frame">添加会员</a></li>
</ul>
</div>
<script>
function show_popup(){
frmBody = parent.document.getElementById('frame-body');
if (frmBody.cols == "37, 12, *")
{
parent.main_frame.document.getElementById('menu_list').style.left = '195px';
}
else
{
parent.main_frame.document.getElementById('menu_list').style.left = '40px';
}
parent.main_frame.document.getElementById('menu_list').style.display = 'block';
}
function hide_popup(){

parent.main_frame.document.getElementById('menu_list').style.display = 'none';
}
</script>
<h1>
<span class="action-span"><a href="article.php?act=list&uselastfilter=1">文章列表</a></span>
<span class="action-span1"><a href="index.php?act=main">管理中心</a> </span><span id="search_id" class="action-span1"> - 编辑文章内容 </span>
<div style="clear:both"></div>
</h1>
<script type="text/javascript" src="../js/utils.js"></script><script type="text/javascript" src="js/selectzone_bd.js"></script><script type="text/javascript" src="js/validator.js"></script><!-- 修改 by www.wrzc.net 百度编辑器 end -->
<!-- start goods form -->
<div class="tab-div">
  <div id="tabbar-div">
    <p>
      <span class="tab-front" id="general-tab">通用信息</span><span
      class="tab-back" id="detail-tab">文章内容</span><span
      class="tab-back" id="goods-tab">关联商品</span>
    </p>
  </div>

  <div id="tabbody-div">
    <form  action="article.php" method="post" enctype="multipart/form-data" name="theForm" onsubmit="return validate();">
    <table width="90%" id="general-table">
      <tr>
        <td class="narrow-label">文章标题</td>
        <td><input type="text" name="title" size ="40" maxlength="60" value="诺基亚6681手机广告欣赏" /><span class="require-field">*</span></td>
      </tr>          
      <!--  -->
      <tr>
        <td class="narrow-label">文章分类 </td>
        <td>
          <select name="article_cat" onchange="catChanged()">
            <option value="0">请选择...</option>
            <option value="16"  cat_type="1" >公司动态</option><option value="21"  cat_type="1" >&nbsp;&nbsp;&nbsp;&nbsp;二级分类</option><option value="14"  cat_type="1" >今日聚焦</option><option value="18"  cat_type="1" >行业聚焦</option><option value="13"  cat_type="1" >生活百科</option><option value="12"  cat_type="1" selected='ture'>站内快讯</option><option value="4"  cat_type="1" >开店必备</option><option value="20"  cat_type="1" >广告354*454</option><option value="11"  cat_type="1" >手机促销</option><option value="17"  cat_type="1" >广告1210*100</option><option value="1"  cat_type="2" >系统分类</option><option value="3"  cat_type="4" >&nbsp;&nbsp;&nbsp;&nbsp;网店帮助分类</option><option value="10"  cat_type="5" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;购物指南</option><option value="7"  cat_type="5" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;配送方式 </option><option value="8"  cat_type="5" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;售后服务</option><option value="5"  cat_type="5" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;新手上路 </option><option value="9"  cat_type="5" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;关于我们 </option><option value="2"  cat_type="3" >&nbsp;&nbsp;&nbsp;&nbsp;网店信息</option><option value="19"  cat_type="99" >供货商通知文章</option>          </select>
         <span class="require-field">*</span></td>
      </tr>
      <!--  -->
            <tr>
        <td class="narrow-label">文章重要性</td>
        <td><input type="radio" name="article_type" value="0" checked>普通      <input type="radio" name="article_type" value="1" >置顶        <span class="require-field">*</span>        </td>
      </tr>
      <tr>
        <td class="narrow-label">是否显示</td>
        <td>
        <input type="radio" name="is_open" value="1" checked> 显示      <input type="radio" name="is_open" value="0" > 不显示<span class="require-field">*</span>        </td>
      </tr>
            <tr>
        <td class="narrow-label">文章作者</td>
        <td><input type="text" name="author" maxlength="60" value="" /></td>
      </tr>
      <tr>
        <td class="narrow-label">作者email</td>
        <td><input type="text" name="author_email" maxlength="60" value="" /></td>
      </tr>
      <tr>
        <td class="narrow-label">关键字</td>
        <td><input type="text" name="keywords" maxlength="60" value="" /></td>
      </tr>
      <tr>
        <td class="narrow-label">网页描述</td>
        <td><textarea name="description" id="description" cols="40" rows="5"></textarea></td>
      </tr>
      <tr>
        <td class="narrow-label">外部链接</td>
        <td><input name="link_url" type="text" id="link_url" value="http://" maxlength="60" /></td>
      </tr>
      <tr>
        <td class="narrow-label">上传文件</td>
        <td><input type="file" name="file">
          <span class="narrow-label">或输入文件地址          <input name="file_url" type="text" value="" size="30" maxlength="255" />
          </span></td>
      </tr>
    </table>

    <table width="90%" id="detail-table" style="display:none">
     <tr><td>
    <script type="text/javascript" charset="utf-8" 

src="../includes/ueditor/ueditor.config.js"></script>
    <script type="text/javascript" charset="utf-8" 

src="../includes/ueditor/ueditor.all.js"></script>
    <textarea name="FCKeditor1" id="FCKeditor1" style="width:100%;">&lt;object&gt;
&lt;param value=&quot;always&quot; name=&quot;allowScriptAccess&quot; /&gt;
&lt;param value=&quot;transparent&quot; name=&quot;wmode&quot; /&gt;
&lt;param value=&quot;http://6.cn/player.swf?flag=0&amp;amp;vid=nZNyu3nGNWWYjmtPQDY9nQ&quot; name=&quot;movie&quot; /&gt;&lt;embed width=&quot;480&quot; height=&quot;385&quot; src=&quot;http://6.cn/player.swf?flag=0&amp;amp;vid=nZNyu3nGNWWYjmtPQDY9nQ&quot; allowscriptaccess=&quot;always&quot; wmode=&quot;transparent&quot; type=&quot;application/x-shockwave-flash&quot;&gt;&lt;/embed&gt;&lt;/object&gt;</textarea>
    <script type="text/javascript">
    UE.getEditor("FCKeditor1",{
    theme:"default", //皮肤
    lang:"zh-cn",    //语言
    initialFrameWidth:1000,  //初始化编辑器宽度,默认650
    initialFrameHeight:350  //初始化编辑器高度,默认180
    });
    </script></td></tr>
    </table>

    <table width="90%" id="goods-table" style="display:none">
      <!-- 商品搜索 -->
      <tr>
      <td colspan="5">
        <img src="images/icon_search.gif" width="26" height="22" border="0" alt="SEARCH" />
        <!-- 分类 -->
        <select name="cat_id"><option value="0">所有分类</caption><option value="5" >家用电器</option><option value="182" >&nbsp;&nbsp;&nbsp;&nbsp;五金家装</option><option value="218" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;家具五金</option><option value="219" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;电工电料</option><option value="216" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;手动工具</option><option value="220" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;监控安防</option><option value="217" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;厨卫五金</option><option value="215" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;电动工具</option><option value="179" >&nbsp;&nbsp;&nbsp;&nbsp;生活电器</option><option value="193" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;加湿器</option><option value="197" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;饮水机</option><option value="194" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;吸尘器</option><option value="198" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;其它生活电器</option><option value="195" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;挂烫机/熨斗</option><option value="192" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;净化器</option><option value="191" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;电风扇</option><option value="196" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;取暖电器</option><option value="180" >&nbsp;&nbsp;&nbsp;&nbsp;厨房电器</option><option value="206" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;其它厨房电器</option><option value="203" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;电炖锅</option><option value="200" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;微波炉</option><option value="204" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;果蔬解毒机</option><option value="201" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;电磁炉</option><option value="205" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;养生壶/煎药壶</option><option value="202" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;电饼铛/烧烤盘</option><option value="199" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;电饭煲</option><option value="181" >&nbsp;&nbsp;&nbsp;&nbsp;个护健康</option><option value="209" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;按摩椅</option><option value="213" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;计步器/脂肪检测</option><option value="210" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;足浴盆</option><option value="207" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;剃须刀</option><option value="214" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;其它健康电器</option><option value="211" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;健康秤/厨房秤</option><option value="208" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;电吹风</option><option value="212" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;血糖仪</option><option value="178" >&nbsp;&nbsp;&nbsp;&nbsp;大家电</option><option value="185" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;洗衣机</option><option value="189" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;消毒柜/洗碗机</option><option value="186" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;家庭影院</option><option value="183" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;平板电视</option><option value="190" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;冷柜/冰吧</option><option value="187" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;烟机/灶具</option><option value="184" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;空调冰箱</option><option value="188" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;热水器</option><option value="4" >手机、数码、通信</option><option value="147" >&nbsp;&nbsp;&nbsp;&nbsp;智能设备</option><option value="169" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;体感车</option><option value="166" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;智能眼镜</option><option value="167" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;运动跟踪器</option><option value="164" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;智能手环</option><option value="168" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;智能家居</option><option value="165" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;智能手表</option><option value="144" >&nbsp;&nbsp;&nbsp;&nbsp;热卖手机</option><option value="153" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;联通4G</option><option value="150" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;小米特供</option><option value="154" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;电信4G</option><option value="151" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;魅族手机</option><option value="148" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;三星盖乐世</option><option value="155" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;移动4G</option><option value="152" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;华为荣耀</option><option value="149" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;iPhone</option><option value="145" >&nbsp;&nbsp;&nbsp;&nbsp;手机配件</option><option value="163" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;保护套</option><option value="160" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;创意配件</option><option value="157" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;移动电源</option><option value="161" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;手机饰品</option><option value="158" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;蓝牙耳机</option><option value="162" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;手机耳机</option><option value="159" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;充电器</option><option value="156" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;电池</option><option value="146" >&nbsp;&nbsp;&nbsp;&nbsp;数码影音</option><option value="176" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;数码相框</option><option value="173" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;运动相机</option><option value="170" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;数码相机</option><option value="177" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;影棚器材</option><option value="174" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;摄像机</option><option value="171" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;单反相机</option><option value="175" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;户外器材</option><option value="172" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;拍立得</option><option value="358" >电脑、办公</option><option value="6" >家居、家具、家装、厨具</option><option value="310" >&nbsp;&nbsp;&nbsp;&nbsp;家装软饰</option><option value="348" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;墙贴/装饰贴</option><option value="345" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;帘艺隔断</option><option value="342" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;桌布/罩件</option><option value="349" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;摆件花瓶</option><option value="346" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;相框/照片墙</option><option value="343" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;地毯地垫</option><option value="347" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;装饰字画</option><option value="344" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;沙发垫套/椅垫</option><option value="307" >&nbsp;&nbsp;&nbsp;&nbsp;家具</option><option value="322" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;餐厅家具</option><option value="326" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;沙发</option><option value="323" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;书房家具</option><option value="320" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;卧室家具</option><option value="327" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;鞋架/衣帽架</option><option value="324" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;储物家具</option><option value="321" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;客厅家具</option><option value="325" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;阳台/户外</option><option value="311" >&nbsp;&nbsp;&nbsp;&nbsp;生活日用</option><option value="354" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;洗晒/熨烫</option><option value="351" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;雨伞雨具</option><option value="355" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;净化除味</option><option value="352" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;浴室用品</option><option value="353" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;缝纫/针织用品</option><option value="350" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;收纳用品</option><option value="306" >&nbsp;&nbsp;&nbsp;&nbsp;家纺</option><option value="319" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;窗帘/窗纱</option><option value="316" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;床单被罩</option><option value="313" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;被子</option><option value="317" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;毛巾浴巾</option><option value="314" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;蚊帐</option><option value="318" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;床垫/床褥</option><option value="315" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;凉席</option><option value="312" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;床品套件</option><option value="308" >&nbsp;&nbsp;&nbsp;&nbsp;厨具</option><option value="332" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;餐具</option><option value="329" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;刀剪菜板</option><option value="333" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;茶具/咖啡具</option><option value="330" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;厨房配件</option><option value="331" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;水具酒具</option><option value="328" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;烹饪锅具</option><option value="309" >&nbsp;&nbsp;&nbsp;&nbsp;灯具</option><option value="338" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;落地灯</option><option value="335" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;吸顶灯</option><option value="339" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;应急灯/手电</option><option value="336" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;筒灯射灯</option><option value="340" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;装饰灯</option><option value="337" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;LED灯</option><option value="334" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;台灯</option><option value="341" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;吊灯</option><option value="2" >男装、女装、内衣、珠宝</option><option value="57" >&nbsp;&nbsp;&nbsp;&nbsp;男装馆</option><option value="83" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;休闲短裤</option><option value="80" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;风衣</option><option value="77" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;休闲裤</option><option value="84" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;POLO衫</option><option value="81" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;针织衫</option><option value="78" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;牛仔裤</option><option value="82" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;长袖衬衫</option><option value="79" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;夹克</option><option value="55" >&nbsp;&nbsp;&nbsp;&nbsp;女装馆</option><option value="67" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;短外套</option><option value="64" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;时尚套装</option><option value="61" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;连衣裙</option><option value="68" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;防晒衫</option><option value="65" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;复古旗袍</option><option value="62" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;连体裤</option><option value="66" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;牛仔裤</option><option value="63" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;棉麻T恤</option><option value="58" >&nbsp;&nbsp;&nbsp;&nbsp;户外鞋服</option><option value="89" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;迷彩裤</option><option value="86" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;篮球鞋</option><option value="90" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;沙滩鞋</option><option value="87" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;帆布鞋</option><option value="91" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;钓鱼服</option><option value="88" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;羽毛球鞋</option><option value="85" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;跑步鞋</option><option value="92" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;登山鞋</option><option value="59" >&nbsp;&nbsp;&nbsp;&nbsp;女鞋馆</option><option value="99" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;乐福鞋</option><option value="96" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;坡跟单鞋</option><option value="93" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;高跟凉拖</option><option value="100" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;松糕鞋</option><option value="97" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;浅口单鞋</option><option value="94" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;平底鞋</option><option value="98" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;帆布鞋</option><option value="95" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;高跟鞋</option><option value="56" >&nbsp;&nbsp;&nbsp;&nbsp;内衣馆</option><option value="73" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;男士内裤</option><option value="70" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;薄款文胸</option><option value="74" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;夏季睡衣</option><option value="71" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;无钢圈文胸</option><option value="75" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;性感睡衣</option><option value="72" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;女士内裤</option><option value="69" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;聚拢文胸</option><option value="76" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;瘦腿袜</option><option value="60" >&nbsp;&nbsp;&nbsp;&nbsp;箱包馆</option><option value="105" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;男士钱包</option><option value="102" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;手提女包</option><option value="106" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;旅行箱</option><option value="103" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;女士钱包</option><option value="107" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;拉杆箱</option><option value="104" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;男士双肩</option><option value="101" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;单肩女包</option><option value="108" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;拉杆包</option><option value="3" >个护化妆、清洁用品</option><option value="113" >&nbsp;&nbsp;&nbsp;&nbsp;香水彩妆</option><option value="137" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;底妆</option><option value="141" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;美甲</option><option value="138" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;腮红</option><option value="142" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;美容工具</option><option value="139" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;眼部</option><option value="136" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;香水</option><option value="143" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;套装</option><option value="140" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;唇部</option><option value="112" >&nbsp;&nbsp;&nbsp;&nbsp;口腔护理</option><option value="134" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;漱口水</option><option value="135" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;套装</option><option value="132" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;牙膏/牙粉</option><option value="133" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;牙刷/牙线</option><option value="109" >&nbsp;&nbsp;&nbsp;&nbsp;面部护肤</option><option value="118" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;套装</option><option value="115" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;护肤</option><option value="116" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;面膜</option><option value="117" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;剃须</option><option value="114" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;清洁</option><option value="110" >&nbsp;&nbsp;&nbsp;&nbsp;洗发护发</option><option value="121" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;染发</option><option value="122" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;造型</option><option value="119" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;洗发</option><option value="123" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;假发</option><option value="120" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;护发</option><option value="124" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;套装</option><option value="111" >&nbsp;&nbsp;&nbsp;&nbsp;身体护肤</option><option value="131" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;套装</option><option value="128" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;手足</option><option value="125" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;沐浴</option><option value="129" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;纤体塑形</option><option value="126" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;润肤</option><option value="130" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 美胸</option><option value="127" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;颈部</option><option value="7" >酒类饮料</option><option value="273" >&nbsp;&nbsp;&nbsp;&nbsp;饮料饮品</option><option value="289" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;果蔬汁</option><option value="296" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;植物蛋白饮料</option><option value="293" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;纯牛奶</option><option value="290" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;茶饮料</option><option value="294" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;酸奶</option><option value="291" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;碳酸饮料</option><option value="295" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;风味奶</option><option value="292" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;功能饮料</option><option value="274" >&nbsp;&nbsp;&nbsp;&nbsp;茗茶</option><option value="305" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;其他茶</option><option value="302" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;红茶</option><option value="299" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;普洱</option><option value="303" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;花果茶</option><option value="300" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;龙井</option><option value="304" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;养生茶</option><option value="301" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;绿茶</option><option value="298" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;铁观音</option><option value="271" >&nbsp;&nbsp;&nbsp;&nbsp;酒水</option><option value="280" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;养生酒</option><option value="277" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;啤酒</option><option value="281" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;预调酒</option><option value="278" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;葡萄酒/果酒</option><option value="279" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;黄酒/米酒</option><option value="276" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;白酒</option><option value="272" >&nbsp;&nbsp;&nbsp;&nbsp;冲调饮品</option><option value="286" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;奶茶</option><option value="283" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;成人奶粉</option><option value="287" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;麦片谷物</option><option value="284" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;豆浆/豆奶粉</option><option value="297" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;果味冲调</option><option value="288" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;咖啡</option><option value="285" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;茶叶</option><option value="282" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;蜂蜜</option><option value="359" >鞋靴、箱包、钟表、奢侈品</option><option value="360" >运动户外</option><option value="361" >汽车、汽车用品</option><option value="8" >母婴、玩具乐器</option><option value="225" >&nbsp;&nbsp;&nbsp;&nbsp;车床/床品</option><option value="257" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;婴儿床</option><option value="261" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;睡袋/抱被</option><option value="258" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;餐椅</option><option value="255" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;安全座椅</option><option value="262" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;凉席/蚊帐</option><option value="259" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;三轮车</option><option value="256" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;手推车</option><option value="260" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;儿童家具</option><option value="222" >&nbsp;&nbsp;&nbsp;&nbsp;营养/辅食</option><option value="238" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;清火开胃</option><option value="235" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;面食类</option><option value="239" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;钙铁锌</option><option value="236" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;宝宝零食</option><option value="233" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;米粉</option><option value="240" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;益生菌</option><option value="237" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;DHA</option><option value="234" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;果汁/泥</option><option value="226" >&nbsp;&nbsp;&nbsp;&nbsp;孕妈专区</option><option value="270" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;孕妇内裤</option><option value="267" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;妈咪包</option><option value="264" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;打底裤</option><option value="268" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;收腹带</option><option value="265" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbspnbsp;&nbsp;防辐射服</option><option value="269" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;哺乳文胸</option><option value="266" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;腰凳</option><option value="263" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;孕妇裙</option><option value="223" >&nbsp;&nbsp;&nbsp;&nbsp;孕婴洗护</option><option value="241" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;洗衣液/皂</option><option value="245" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;爽身粉</option><option value="242" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;宝宝沐浴</option><option value="246" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;奶瓶清洗</option><option value="243" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;儿童防晒</option><option value="247" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;孕妇护肤</option><option value="244" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;防蚊/驱蚊</option><option value="224" >&nbsp;&nbsp;&nbsp;&nbsp;喂养用品</option><option value="254" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;防溢乳垫</option><option value="251" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;水壶/水杯</option><option value="248" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;奶嘴奶瓶</option><option value="252" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;吸奶器</option><option value="249" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;驱蚊用品</option><option value="253" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;理发器</option><option value="250" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;浴室用品</option><option value="221" >&nbsp;&nbsp;&nbsp;&nbsp;孕婴奶粉</option><option value="232" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;3段</option><option value="229" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;pre段</option><option value="230" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;1段</option><option value="227" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;特配奶粉</option><option value="231" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;2段</option><option value="228" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;孕妈奶粉</option><option value="1" >食品、酒类、生鲜、特产</option><option value="14" >&nbsp;&nbsp;&nbsp;&nbsp;进口水果</option><option value="20" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;奇异果猕猴桃</option><option value="17" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;芒果桃李</option><option value="21" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;凤梨蓝莓</option><option value="18" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;龙眼荔枝</option><option value="15" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;释迦芭乐</option><option value="22" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;榴莲山竹</option><option value="19" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;提子葡萄</option><option value="16" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;樱桃车厘子</option><option value="13" >&nbsp;&nbsp;&nbsp;&nbsp;糖果巧克力</option><option value="24" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;巧克力</option><option value="28" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;奶糖</option><option value="25" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;口香糖</option><option value="29" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;QQ糖</option><option value="26" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;棒棒糖</option><option value="30" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;果冻</option><option value="27" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;软糖</option><option value="9" >&nbsp;&nbsp;&nbsp;&nbsp;牛奶乳品</option><option value="37" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;全脂奶</option><option value="34" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;酸奶</option><option value="31" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;常温奶</option><option value="38" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;成人奶粉</option><option value="35" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;豆奶</option><option value="32" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;乳饮料</option><option value="36" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;低脂奶</option><option value="33" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;儿童奶</option><option value="10" >&nbsp;&nbsp;&nbsp;&nbsp;坚果炒货</option><option value="40" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;夏威夷果</option><option value="44" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;瓜子</option><option value="41" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;碧根果</option><option value="45" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;花生</option><option value="42" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;开心果</option><option value="39" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;核桃</option><option value="46" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;杏仁</option><option value="43" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;腰果</option><option value="12" >&nbsp;&nbsp;&nbsp;&nbsp;蜜饯果干</option><option value="53" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;橄榄</option><option value="50" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;芒果干</option><option value="47" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;红枣</option><option value="54" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;其他</option><option value="51" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;香蕉干</option><option value="48" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;莓类</option><option value="52" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;山楂片</option><option value="49" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;葡萄干</option><option value="362" >营养保健</option><option value="363" >图书、音像、电子书</option><option value="364" >彩票、旅行、充值、票务</option><option value="365" >理财、众筹、白条、保险</option></select>
        <!-- 品牌 -->
        <select name="brand_id"><option value="0">所有品牌</caption><option value="54">缪诗</option><option value="68">格力</option><option value="69">老板</option><option value="70">西门子</option><option value="71">格兰仕</option><option value="72">海信</option><option value="73">伊莱克斯</option><option value="74">艾力斯特</option><option value="75">博洋家纺</option><option value="76">富安娜</option><option value="77">爱仕达</option><option value="78">罗莱</option><option value="67">美的</option><option value="66">海尔</option><option value="55">卓诗尼</option><option value="56">七匹狼</option><option value="57">佐丹奴</option><option value="58">达芙妮</option><option value="59">她他/tata</option><option value="60">曼妮芬（ManniForm）</option><option value="61">伊芙丽</option><option value="62">稻草人</option><option value="63">斯提亚</option><option value="64">袋鼠</option><option value="65">爱华仕</option><option value="79">安睡宝</option><option value="80">溢彩年华</option><option value="94">王老吉</option><option value="95">可口可乐</option><option value="96">贝古贝古</option><option value="97">皇家宝贝</option><option value="98">呵宝童车</option><option value="99">合生元</option><option value="100">美赞臣</option><option value="101">帮宝适</option><option value="102">抱抱熊</option><option value="103">巴拉巴拉</option><option value="104">青蛙王子</option><option value="93">统一</option><option value="92">加多宝</option><option value="81">慧乐家</option><option value="82">天堂伞</option><option value="83">水星家纺</option><option value="84">全有家居</option><option value="85">五粮液</option><option value="86">泸州老窖</option><option value="87">洋河</option><option value="88">郎酒</option><option value="89">锐澳</option><option value="90">雪花</option><option value="91">哈尔滨</option><option value="105">雀氏</option><option value="1">资生堂</option><option value="15">韩束</option><option value="16">卡姿兰</option><option value="17">珀莱雅</option><option value="18">兰芝</option><option value="19">碧欧泉</option><option value="20">小米</option><option value="21">摩托罗拉</option><option value="22">中兴</option><option value="23">朵唯</option><option value="24">htc</option><option value="25">华为</option><option value="14">高丝</option><option value="13">SK-ll</option><option value="2">CK</option><option value="3">Disney</option><option value="4">雅诗兰黛</option><option value="5">相宜本草</option><option value="6">Dior</option><option value="7">爱丽</option><option value="8">雅顿</option><option value="9">狮王</option><option value="10">高丝洁</option><option value="11">MISS FACE</option><option value="12">姬芮</option><option value="26">oppo</option><option value="27">金立</option><option value="42">君乐宝</option><option value="43">光明</option><option value="44">三元</option><option value="45">百草味</option><option value="46">三只松鼠</option><option value="47">口水娃</option><option value="48">楼兰密语</option><option value="49">西域美农</option><option value="50">糖糖屋</option><option value="51">享爱.</option><option value="52">猫人</option><option value="40">蒙牛</option><option value="39">海底捞</option><option value="28">LG</option><option value="29">苹果</option><option value="30">三星</option><option value="31">乐檬</option><option value="32">努比亚</option><option value="41">伊利</option><option value="34">肯德基</option><option value="35">麦当劳</option><option value="36">小肥羊</option><option value="37">小尾羊</option><option value="38">必胜客</option><option value="53">茵曼（INMAN）</option></select>
        <!-- 关键字 -->
        <input type="text" name="keyword" size="30" />
        <input type="button" value=" 搜索 " onclick="searchGoods()" class="button" />
      <td>
      </tr>
      <!-- 商品列表 -->
      <tr>
        <th></th>
        <th>操作</th>
        <th></th>
      </tr>
      <tr>
        <td width="45%" align="center">
          <select name="source_select" size="20" style="width:90%" ondblclick="sz.addItem(false, 'add_link_goods', articleId)" multiple="true">
          </select>
        </td>
        <td align="center">
          <p><input type="button" value="&gt;&gt;" onclick="sz.addItem(true, 'add_link_goods', articleId)" class="button" /></p>
          <p><input type="button" value="&gt;" onclick="sz.addItem(false, 'add_link_goods', articleId)" class="button" /></p>
          <p><input type="button" value="&lt;" onclick="sz.dropItem(false, 'drop_link_goods', articleId)" class="button" /></p>
          <p><input type="button" value="&lt;&lt;" onclick="sz.dropItem(true, 'drop_link_goods', articleId)" class="button" /></p>
        </td>
        <td width="45%" align="center">
          <select name="target_select" multiple="true" size="20" style="width:90%" ondblclick="sz.dropItem(false, 'drop_link_goods', articleId)">
                      </select>
        </td>
      </tr>
    </table>
    <div class="button-div">
      <input type="hidden" name="act" value="update" />
      <input type="hidden" name="old_title" value="诺基亚6681手机广告欣赏"/>
      <input type="hidden" name="id" value="171" />
      <input type="submit" value=" 确定 " class="button"  />
      <input type="reset" value=" 重置 " class="button" />
    </div>
    </form>
  </div>

</div>
<!-- end goods form -->
<script language="JavaScript">

var articleId = 171;
var elements  = document.forms['theForm'].elements;
var sz        = new SelectZone(1, elements['source_select'], elements['target_select'], '');


onload = function()
{
  // 开始检查订单
  startCheckOrder();
}

function validate()
{
  var validator = new Validator('theForm');
  validator.required('title', no_title);

// //   validator.isNullOption('article_cat',no_cat);
// 

  return validator.passed();
}

document.getElementById("tabbar-div").onmouseover = function(e)
{
    var obj = Utils.srcElement(e);

    if (obj.className == "tab-back")
    {
        obj.className = "tab-hover";
    }
}

document.getElementById("tabbar-div").onmouseout = function(e)
{
    var obj = Utils.srcElement(e);

    if (obj.className == "tab-hover")
    {
        obj.className = "tab-back";
    }
}

document.getElementById("tabbar-div").onclick = function(e)
{
    var obj = Utils.srcElement(e);

    if (obj.className == "tab-front")
    {
        return;
    }
    else
    {
        objTable = obj.id.substring(0, obj.id.lastIndexOf("-")) + "-table";

        var tables = document.getElementsByTagName("table");
        var spans  = document.getElementsByTagName("span");

        for (i = 0; i < tables.length; i++)
        {
            if (tables[i].id == objTable)
            {
                tables[i].style.display = (Browser.isIE) ? "block" : "table";
            }
            else
            {
                tables[i].style.display = "none";
            }
        }
        for (i = 0; spans.length; i++)
        {
            if (spans[i].className == "tab-front")
            {
                spans[i].className = "tab-back";
                obj.className = "tab-front";
                break;
            }
        }
    }
}

function showNotice(objId)
{
    var obj = document.getElementById(objId);

    if (obj)
    {
        if (obj.style.display != "block")
        {
            obj.style.display = "block";
        }
        else
        {
            obj.style.display = "none";
        }
    }
}

function searchGoods()
{
    var elements  = document.forms['theForm'].elements;
    var filters   = new Object;

    filters.cat_id = elements['cat_id'].value;
    filters.brand_id = elements['brand_id'].value;
    filters.keyword = Utils.trim(elements['keyword'].value);

    sz.loadOptions('get_goods_list', filters);
}


/**
 * 选取上级分类时判断选定的分类是不是底层分类
 */
function catChanged()
{
  var obj = document.forms['theForm'].elements['article_cat'];

  cat_type = obj.options[obj.selectedIndex].getAttribute('cat_type');
  if (cat_type == undefined)
  {
    cat_type = 1;
  }

  if ((obj.selectedIndex > 0) && (cat_type == 2 || cat_type == 4))
  {
    alert(not_allow_add);
    obj.selectedIndex = 0;
    return false;
  }

  return true;
}
</script>
<div id="footer">
共执行 4 个查询，用时 0.051003 秒，Gzip 已禁用，内存占用 3.771 MB<br />
版权所有 &copy; 2008-2030 广州网软志成信息科技有限公司，并保留所有权利。</div>
<!-- 新订单提示信息 -->
<div id="popMsg">
  <table cellspacing="0" cellpadding="0" width="100%" bgcolor="#cfdef4" border="0">
  <tr>
    <td style="color: #0f2c8c" width="30" height="24"></td>
    <td style="font-weight: normal; color: #1f336b; padding-top: 4px;padding-left: 4px" valign="center" width="100%"> 新订单通知</td>
    <td style="padding-top: 2px;padding-right:2px" valign="center" align="right" width="19"><span title="关闭" style="cursor: hand;cursor:pointer;color:red;font-size:12px;font-weight:bold;margin-right:4px;" onclick="Message.close()" >×</span><!-- <img title=关闭 style="cursor: hand" onclick=closediv() hspace=3 src="msgclose.jpg"> --></td>
  </tr>
  <tr>
    <td style="padding-right: 1px; padding-bottom: 1px" colspan="3" height="70">
    <div id="popMsgContent">
      <p>您有 <strong style="color:#ff0000" id="spanNewOrder">1</strong> 个新订单以及       <strong style="color:#ff0000" id="spanNewPaid">0</strong> 个新付款的订单</p>
      <p align="center" style="word-break:break-all"><a href="order.php?act=list"><span style="color:#ff0000">点击查看新订单</span></a></p>
    </div>
    </td>
  </tr>
  </table>
</div>

<!--
<embed src="images/online.wav" width="0" height="0" autostart="false" name="msgBeep" id="msgBeep" enablejavascript="true"/>
-->
<object classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000" codebase="http://active.macromedia.com/flash2/cabs/swflash.cab#version=4,0,0,0" id="msgBeep" width="1" height="1">
  <param name="movie" value="images/online.swf">
  <param name="quality" value="high">
  <embed src="images/online.swf" name="msgBeep" id="msgBeep" quality="high" width="0" height="0" type="application/x-shockwave-flash" pluginspage="http://www.macromedia.com/shockwave/download/index.cgi?p1_prod_version=shockwaveflash">
  </embed>
</object>

<script language="JavaScript">
document.onmousemove=function(e)
{
  var obj = Utils.srcElement(e);
  if (typeof(obj.onclick) == 'function' && obj.onclick.toString().indexOf('listTable.edit') != -1)
  {
    obj.title = '点击修改内容';
    obj.style.cssText = 'background: #278296;';
    obj.onmouseout = function(e)
    {
      this.style.cssText = '';
    }
  }
  else if (typeof(obj.href) != 'undefined' && obj.href.indexOf('listTable.sort') != -1)
  {
    obj.title = '点击对列表排序';
  }
}
<!--


var MyTodolist;
function showTodoList(adminid)
{
  if(!MyTodolist)
  {
    var global = $import("../js/global.js","js");
    global.onload = global.onreadystatechange= function()
    {
      if(this.readyState && this.readyState=="loading")return;
      var md5 = $import("js/md5.js","js");
      md5.onload = md5.onreadystatechange= function()
      {
        if(this.readyState && this.readyState=="loading")return;
        var todolist = $import("js/todolist.js","js");
        todolist.onload = todolist.onreadystatechange = function()
        {
          if(this.readyState && this.readyState=="loading")return;
          MyTodolist = new Todolist();
          MyTodolist.show();
        }
      }
    }
  }
  else
  {
    if(MyTodolist.visibility)
    {
      MyTodolist.hide();
    }
    else
    {
      MyTodolist.show();
    }
  }
}

if (Browser.isIE)
{
  onscroll = function()
  {
    //document.getElementById('calculator').style.top = document.body.scrollTop;
    document.getElementById('popMsg').style.top = (document.body.scrollTop + document.body.clientHeight - document.getElementById('popMsg').offsetHeight) + "px";
  }
}

if (document.getElementById("listDiv"))
{
  document.getElementById("listDiv").onmouseover = function(e)
  {
    obj = Utils.srcElement(e);

    if (obj)
    {
      if (obj.parentNode.tagName.toLowerCase() == "tr") row = obj.parentNode;
      else if (obj.parentNode.parentNode.tagName.toLowerCase() == "tr") row = obj.parentNode.parentNode;
      else return;

      for (i = 0; i < row.cells.length; i++)
      {
        if (row.cells[i].tagName != "TH") row.cells[i].style.backgroundColor = '#F4FAFB';
      }
    }

  }

  document.getElementById("listDiv").onmouseout = function(e)
  {
    obj = Utils.srcElement(e);

    if (obj)
    {
      if (obj.parentNode.tagName.toLowerCase() == "tr") row = obj.parentNode;
      else if (obj.parentNode.parentNode.tagName.toLowerCase() == "tr") row = obj.parentNode.parentNode;
      else return;

      for (i = 0; i < row.cells.length; i++)
      {
          if (row.cells[i].tagName != "TH") row.cells[i].style.backgroundColor = '';
      }
    }
  }

  document.getElementById("listDiv").onclick = function(e)
  {
    var obj = Utils.srcElement(e);

    if (obj.tagName == "INPUT" && obj.type == "checkbox")
    {
      if (!document.forms['listForm'])
      {
        return;
      }
      var nodes = document.forms['listForm'].elements;
      var checked = false;

      for (i = 0; i < nodes.length; i++)
      {
        if (nodes[i].checked)
        {
           checked = true;
           break;
         }
      }

      if(document.getElementById("btnSubmit"))
      {
        document.getElementById("btnSubmit").disabled = !checked;
      }
      for (i = 1; i <= 10; i++)
      {
        if (document.getElementById("btnSubmit" + i))
        {
          document.getElementById("btnSubmit" + i).disabled = !checked;
        }
      }
    }
  }

}

//-->
</script><?php
include 'buy.php' ;
?>
</body>
</html>