
<!-- $Id: bonus_type.htm 14216 2008-03-10 02:27:21Z testyang $ -->

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>管理中心 - 红包类型 </title>
<meta name="robots" content="noindex, nofollow">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link href="styles/general.css" rel="stylesheet" type="text/css" />
<link href="styles/main.css" rel="stylesheet" type="text/css" />
<link href="styles/chosen/chosen.css" rel="stylesheet" type="text/css">
<script type="text/javascript" src="js/jquery-1.6.2.min.js"></script><script type="text/javascript" src="js/chosen.jquery.min.js"></script><script type="text/javascript" src="js/jquery.json.js"></script><script type="text/javascript" src="../js/transport.js"></script><script type="text/javascript" src="js/common.js"></script><script language="JavaScript">
<!--
// 这里把JS用到的所有语言都赋值到这里
var process_request = "正在处理您的请求...";
var todolist_caption = "记事本";
var todolist_autosave = "自动保存";
var todolist_save = "保存";
var todolist_clear = "清除";
var todolist_confirm_save = "是否将更改保存到记事本？";
var todolist_confirm_clear = "是否清空内容？";
var type_name_empty = "请输入红包类型名称!";
var type_money_empty = "请输入红包类型价格!";
var order_money_empty = "请输入订单金额!";
var type_money_isnumber = "类型金额必须为数字格式!";
var order_money_isnumber = "订单金额必须为数字格式!";
var bonus_sn_empty = "请输入红包的序列号!";
var bonus_sn_number = "红包的序列号必须是数字!";
var bonus_sum_empty = "请输入您要发放的红包数量!";
var bonus_sum_number = "红包的发放数量必须是一个整数!";
var bonus_type_empty = "请选择红包的类型金额!";
var user_rank_empty = "您没有指定会员等级!";
var user_name_empty = "您至少需要选择一个会员!";
var invalid_min_amount = "请输入订单下限（大于0的数字）";
var send_start_lt_end = "红包发放开始日期不能大于结束日期";
var use_start_lt_end = "红包使用开始日期不能大于结束日期";
//-->
</script>
</head>
<body>

<div id="menu_list" onmouseover="show_popup()" onmouseout="hide_popup()">
<ul>
<li><a href="goods.php?act=add" target="main_frame">添加新商品</a></li>
<li><a href="category.php?act=add" target="main_frame">添加商品分类</a></li>
<li><a href="order.php?act=add" target="main_frame">添加订单</a></li>
<li><a href="article.php?act=add" target="main_frame">添加新文章</a></li>
<li><a href="users.php?act=add" target="main_frame">添加会员</a></li>
</ul>
</div>
<script>
function show_popup(){
frmBody = parent.document.getElementById('frame-body');
if (frmBody.cols == "37, 12, *")
{
parent.main_frame.document.getElementById('menu_list').style.left = '195px';
}
else
{
parent.main_frame.document.getElementById('menu_list').style.left = '40px';
}
parent.main_frame.document.getElementById('menu_list').style.display = 'block';
}
function hide_popup(){

parent.main_frame.document.getElementById('menu_list').style.display = 'none';
}
</script>
<h1>
<span class="action-span"><a href="bonus.php?act=add">添加红包类型</a></span>
<span class="action-span1"><a href="index.php?act=main">管理中心</a> </span><span id="search_id" class="action-span1"> - 红包类型 </span>
<div style="clear:both"></div>
</h1>
<script type="text/javascript" src="../js/utils.js"></script><script type="text/javascript" src="js/listtable.js"></script><!-- start bonus_type list -->
<form method="post" action="" name="listForm">
<div class="list-div" id="listDiv">

  <table cellpadding="3" cellspacing="1">
    <tr>
      <th><a href="javascript:listTable.sort('type_name'); ">类型名称</a></th>
      <th><a href="javascript:listTable.sort('send_type'); ">发放类型</a></th>
      <th><a href="javascript:listTable.sort('type_money'); ">红包金额</a></th>
      <th><a href="javascript:listTable.sort('min_amount'); ">订单下限</a></th>
      <th>发放数量</th>
      <th>使用数量</th>
      <th>操作</th>
    </tr>
        <tr>
      <td class="first-cell"><span onclick="listTable.edit(this, 'edit_type_name', 23)">注册红包15</span></td>
      <td>按注册用户发放</td>
      <td align="center"><span onclick="listTable.edit(this, 'edit_type_money', 23)">15.00</span></td>
      <td align="center"><span>--</span></td>
      <td align="center"><span>0</span></td>
      <td align="center">0</td>
      <td align="center">
                        <a href="bonus.php?act=bonus_list&amp;bonus_type=23">查看</a> |
        <a href="bonus.php?act=edit&amp;type_id=23">编辑</a> |
        <a href="javascript:;" onclick="listTable.remove(23, '您确认要删除这条记录吗?')">移除</a></span></td>
    </tr>
          <tr>
      <td class="first-cell"><span onclick="listTable.edit(this, 'edit_type_name', 22)">注册红包 10</span></td>
      <td>按注册用户发放</td>
      <td align="center"><span onclick="listTable.edit(this, 'edit_type_money', 22)">10.00</span></td>
      <td align="center"><span>--</span></td>
      <td align="center"><span>0</span></td>
      <td align="center">0</td>
      <td align="center">
                        <a href="bonus.php?act=bonus_list&amp;bonus_type=22">查看</a> |
        <a href="bonus.php?act=edit&amp;type_id=22">编辑</a> |
        <a href="javascript:;" onclick="listTable.remove(22, '您确认要删除这条记录吗?')">移除</a></span></td>
    </tr>
          <tr>
      <td class="first-cell"><span onclick="listTable.edit(this, 'edit_type_name', 21)">注册红包5</span></td>
      <td>按注册用户发放</td>
      <td align="center"><span onclick="listTable.edit(this, 'edit_type_money', 21)">5.00</span></td>
      <td align="center"><span>--</span></td>
      <td align="center"><span>0</span></td>
      <td align="center">0</td>
      <td align="center">
                        <a href="bonus.php?act=bonus_list&amp;bonus_type=21">查看</a> |
        <a href="bonus.php?act=edit&amp;type_id=21">编辑</a> |
        <a href="javascript:;" onclick="listTable.remove(21, '您确认要删除这条记录吗?')">移除</a></span></td>
    </tr>
          <tr>
      <td class="first-cell"><span onclick="listTable.edit(this, 'edit_type_name', 20)">注册红包10</span></td>
      <td>按注册用户发放</td>
      <td align="center"><span onclick="listTable.edit(this, 'edit_type_money', 20)">10.00</span></td>
      <td align="center"><span>--</span></td>
      <td align="center"><span>0</span></td>
      <td align="center">0</td>
      <td align="center">
                        <a href="bonus.php?act=bonus_list&amp;bonus_type=20">查看</a> |
        <a href="bonus.php?act=edit&amp;type_id=20">编辑</a> |
        <a href="javascript:;" onclick="listTable.remove(20, '您确认要删除这条记录吗?')">移除</a></span></td>
    </tr>
          <tr>
      <td class="first-cell"><span onclick="listTable.edit(this, 'edit_type_name', 19)">红包测试</span></td>
      <td>按用户发放</td>
      <td align="center"><span onclick="listTable.edit(this, 'edit_type_money', 19)">100.00</span></td>
      <td align="center"><span onclick="listTable.edit(this, 'edit_min_amount', 19)">0.00</span></td>
      <td align="center"><span>7</span></td>
      <td align="center">0</td>
      <td align="center">
                       
        <a href="bonus.php?act=send&amp;id=19&amp;send_by=0">发放</a> |
        
                <a href="bonus.php?act=bonus_list&amp;bonus_type=19">查看</a> |
        <a href="bonus.php?act=edit&amp;type_id=19">编辑</a> |
        <a href="javascript:;" onclick="listTable.remove(19, '您确认要删除这条记录吗?')">移除</a></span></td>
    </tr>
          <tr>
      <td class="first-cell"><span onclick="listTable.edit(this, 'edit_type_name', 18)">线上领取红包2222</span></td>
      <td>线上发放的红包</td>
      <td align="center"><span onclick="listTable.edit(this, 'edit_type_money', 18)">19.00</span></td>
      <td align="center"><span onclick="listTable.edit(this, 'edit_min_amount', 18)">0.00</span></td>
      <td align="center"><span>10</span></td>
      <td align="center">0</td>
      <td align="center">
                        <a href="bonus.php?act=bonus_list&amp;bonus_type=18">查看</a> |
        <a href="bonus.php?act=edit&amp;type_id=18">编辑</a> |
        <a href="javascript:;" onclick="listTable.remove(18, '您确认要删除这条记录吗?')">移除</a></span></td>
    </tr>
          <tr>
      <td class="first-cell"><span onclick="listTable.edit(this, 'edit_type_name', 17)">线上领取红包111</span></td>
      <td>线上发放的红包</td>
      <td align="center"><span onclick="listTable.edit(this, 'edit_type_money', 17)">666.00</span></td>
      <td align="center"><span onclick="listTable.edit(this, 'edit_min_amount', 17)">0.00</span></td>
      <td align="center"><span>1</span></td>
      <td align="center">0</td>
      <td align="center">
                        <a href="bonus.php?act=bonus_list&amp;bonus_type=17">查看</a> |
        <a href="bonus.php?act=edit&amp;type_id=17">编辑</a> |
        <a href="javascript:;" onclick="listTable.remove(17, '您确认要删除这条记录吗?')">移除</a></span></td>
    </tr>
          <tr>
      <td class="first-cell"><span onclick="listTable.edit(this, 'edit_type_name', 16)">线上领取5</span></td>
      <td>线上发放的红包</td>
      <td align="center"><span onclick="listTable.edit(this, 'edit_type_money', 16)">20.00</span></td>
      <td align="center"><span onclick="listTable.edit(this, 'edit_min_amount', 16)">0.00</span></td>
      <td align="center"><span>1</span></td>
      <td align="center">0</td>
      <td align="center">
                        <a href="bonus.php?act=bonus_list&amp;bonus_type=16">查看</a> |
        <a href="bonus.php?act=edit&amp;type_id=16">编辑</a> |
        <a href="javascript:;" onclick="listTable.remove(16, '您确认要删除这条记录吗?')">移除</a></span></td>
    </tr>
          <tr>
      <td class="first-cell"><span onclick="listTable.edit(this, 'edit_type_name', 14)">线上领取3</span></td>
      <td>线上发放的红包</td>
      <td align="center"><span onclick="listTable.edit(this, 'edit_type_money', 14)">10.00</span></td>
      <td align="center"><span onclick="listTable.edit(this, 'edit_min_amount', 14)">0.00</span></td>
      <td align="center"><span>1</span></td>
      <td align="center">1</td>
      <td align="center">
                        <a href="bonus.php?act=bonus_list&amp;bonus_type=14">查看</a> |
        <a href="bonus.php?act=edit&amp;type_id=14">编辑</a> |
        <a href="javascript:;" onclick="listTable.remove(14, '您确认要删除这条记录吗?')">移除</a></span></td>
    </tr>
          <tr>
      <td class="first-cell"><span onclick="listTable.edit(this, 'edit_type_name', 13)">线上领取2</span></td>
      <td>线上发放的红包</td>
      <td align="center"><span onclick="listTable.edit(this, 'edit_type_money', 13)">8.00</span></td>
      <td align="center"><span onclick="listTable.edit(this, 'edit_min_amount', 13)">0.00</span></td>
      <td align="center"><span>1</span></td>
      <td align="center">0</td>
      <td align="center">
                        <a href="bonus.php?act=bonus_list&amp;bonus_type=13">查看</a> |
        <a href="bonus.php?act=edit&amp;type_id=13">编辑</a> |
        <a href="javascript:;" onclick="listTable.remove(13, '您确认要删除这条记录吗?')">移除</a></span></td>
    </tr>
          <tr>
      <td class="first-cell"><span onclick="listTable.edit(this, 'edit_type_name', 12)">线上领取1</span></td>
      <td>线上发放的红包</td>
      <td align="center"><span onclick="listTable.edit(this, 'edit_type_money', 12)">5.00</span></td>
      <td align="center"><span onclick="listTable.edit(this, 'edit_min_amount', 12)">0.00</span></td>
      <td align="center"><span>2</span></td>
      <td align="center">0</td>
      <td align="center">
                        <a href="bonus.php?act=bonus_list&amp;bonus_type=12">查看</a> |
        <a href="bonus.php?act=edit&amp;type_id=12">编辑</a> |
        <a href="javascript:;" onclick="listTable.remove(12, '您确认要删除这条记录吗?')">移除</a></span></td>
    </tr>
          <tr>
      <td class="first-cell"><span onclick="listTable.edit(this, 'edit_type_name', 11)">线上领取4</span></td>
      <td>线上发放的红包</td>
      <td align="center"><span onclick="listTable.edit(this, 'edit_type_money', 11)">20.00</span></td>
      <td align="center"><span onclick="listTable.edit(this, 'edit_min_amount', 11)">0.00</span></td>
      <td align="center"><span>0</span></td>
      <td align="center">0</td>
      <td align="center">
                        <a href="bonus.php?act=bonus_list&amp;bonus_type=11">查看</a> |
        <a href="bonus.php?act=edit&amp;type_id=11">编辑</a> |
        <a href="javascript:;" onclick="listTable.remove(11, '您确认要删除这条记录吗?')">移除</a></span></td>
    </tr>
          <tr>
      <td class="first-cell"><span onclick="listTable.edit(this, 'edit_type_name', 7)">aaaaaaaaaa</span></td>
      <td>按注册用户发放</td>
      <td align="center"><span onclick="listTable.edit(this, 'edit_type_money', 7)">11.00</span></td>
      <td align="center"><span>--</span></td>
      <td align="center"><span>1</span></td>
      <td align="center">0</td>
      <td align="center">
                        <a href="bonus.php?act=bonus_list&amp;bonus_type=7">查看</a> |
        <a href="bonus.php?act=edit&amp;type_id=7">编辑</a> |
        <a href="javascript:;" onclick="listTable.remove(7, '您确认要删除这条记录吗?')">移除</a></span></td>
    </tr>
          <tr>
      <td class="first-cell"><span onclick="listTable.edit(this, 'edit_type_name', 6)">111</span></td>
      <td>按订单金额发放</td>
      <td align="center"><span onclick="listTable.edit(this, 'edit_type_money', 6)">1.00</span></td>
      <td align="center"><span onclick="listTable.edit(this, 'edit_min_amount', 6)">100.00</span></td>
      <td align="center"><span>358</span></td>
      <td align="center">0</td>
      <td align="center">
                        <a href="bonus.php?act=bonus_list&amp;bonus_type=6">查看</a> |
        <a href="bonus.php?act=edit&amp;type_id=6">编辑</a> |
        <a href="javascript:;" onclick="listTable.remove(6, '您确认要删除这条记录吗?')">移除</a></span></td>
    </tr>
          <tr>
      <td class="first-cell"><span onclick="listTable.edit(this, 'edit_type_name', 5)">777</span></td>
      <td>按用户发放</td>
      <td align="center"><span onclick="listTable.edit(this, 'edit_type_money', 5)">777.00</span></td>
      <td align="center"><span onclick="listTable.edit(this, 'edit_min_amount', 5)">0.00</span></td>
      <td align="center"><span>1</span></td>
      <td align="center">0</td>
      <td align="center">
                       
        <a href="bonus.php?act=send&amp;id=5&amp;send_by=0">发放</a> |
        
                <a href="bonus.php?act=bonus_list&amp;bonus_type=5">查看</a> |
        <a href="bonus.php?act=edit&amp;type_id=5">编辑</a> |
        <a href="javascript:;" onclick="listTable.remove(5, '您确认要删除这条记录吗?')">移除</a></span></td>
    </tr>
          <tr>
      <td align="right" nowrap="true" colspan="8">      <!-- $Id: page.htm 14216 2008-03-10 02:27:21Z testyang $ -->
            <div id="turn-page">
        总计  <span id="totalRecords">17</span>
        个记录分为 <span id="totalPages">2</span>
        页当前第 <span id="pageCurrent">1</span>
        页，每页 <input type='text' size='3' id='pageSize' value="15" onkeypress="return listTable.changePageSize(event)" />
        <span id="page-link">
          <a href="javascript:listTable.gotoPageFirst()">第一页</a>
          <a href="javascript:listTable.gotoPagePrev()">上一页</a>
          <a href="javascript:listTable.gotoPageNext()">下一页</a>
          <a href="javascript:listTable.gotoPageLast()">最末页</a>
        <!--   <select id="gotoPage" onchange="listTable.gotoPage(this.value)">
            <option value='1'>1</option><option value='2'>2</option>          </select> -->
         跳转到 <input id="pageInput" title="按回车键跳转到指定页数" value="1" style="width:25px;"  onkeypress="if(event.keyCode==13) {listTable.gotoPage(this.value);}"   type="text">
          <input class="button" id="pagerBtn" value="GO" onclick="goToPage()" type="button">
        </span>
      </div>
<script>
function goToPage(){
	var page=$("#pageInput").val();
	listTable.gotoPage(page);
}
</script></td>
    </tr>
  </table>

</div>
</form>
<!-- end bonus_type list -->

<script type="text/javascript" language="JavaScript">
<!--
  listTable.recordCount = 17;
  listTable.pageCount = 2;

    listTable.filter.sort_by = 'type_id';
    listTable.filter.sort_order = 'DESC';
    listTable.filter.record_count = '17';
    listTable.filter.page_size = '15';
    listTable.filter.page = '1';
    listTable.filter.page_count = '2';
    listTable.filter.start = '0';
  
  
  onload = function()
  {
     // 开始检查订单
     startCheckOrder();
  }
  
//-->
</script>
<div id="footer">
共执行 5 个查询，用时 0.011001 秒，Gzip 已禁用，内存占用 2.848 MB<br />
版权所有 &copy; 2008-2030 广州网软志成信息科技有限公司，并保留所有权利。</div>
<!-- 新订单提示信息 -->
<div id="popMsg">
  <table cellspacing="0" cellpadding="0" width="100%" bgcolor="#cfdef4" border="0">
  <tr>
    <td style="color: #0f2c8c" width="30" height="24"></td>
    <td style="font-weight: normal; color: #1f336b; padding-top: 4px;padding-left: 4px" valign="center" width="100%"> 新订单通知</td>
    <td style="padding-top: 2px;padding-right:2px" valign="center" align="right" width="19"><span title="关闭" style="cursor: hand;cursor:pointer;color:red;font-size:12px;font-weight:bold;margin-right:4px;" onclick="Message.close()" >×</span><!-- <img title=关闭 style="cursor: hand" onclick=closediv() hspace=3 src="msgclose.jpg"> --></td>
  </tr>
  <tr>
    <td style="padding-right: 1px; padding-bottom: 1px" colspan="3" height="70">
    <div id="popMsgContent">
      <p>您有 <strong style="color:#ff0000" id="spanNewOrder">1</strong> 个新订单以及       <strong style="color:#ff0000" id="spanNewPaid">0</strong> 个新付款的订单</p>
      <p align="center" style="word-break:break-all"><a href="order.php?act=list"><span style="color:#ff0000">点击查看新订单</span></a></p>
    </div>
    </td>
  </tr>
  </table>
</div>

<!--
<embed src="images/online.wav" width="0" height="0" autostart="false" name="msgBeep" id="msgBeep" enablejavascript="true"/>
-->
<object classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000" codebase="http://active.macromedia.com/flash2/cabs/swflash.cab#version=4,0,0,0" id="msgBeep" width="1" height="1">
  <param name="movie" value="images/online.swf">
  <param name="quality" value="high">
  <embed src="images/online.swf" name="msgBeep" id="msgBeep" quality="high" width="0" height="0" type="application/x-shockwave-flash" pluginspage="http://www.macromedia.com/shockwave/download/index.cgi?p1_prod_version=shockwaveflash">
  </embed>
</object>

<script language="JavaScript">
document.onmousemove=function(e)
{
  var obj = Utils.srcElement(e);
  if (typeof(obj.onclick) == 'function' && obj.onclick.toString().indexOf('listTable.edit') != -1)
  {
    obj.title = '点击修改内容';
    obj.style.cssText = 'background: #278296;';
    obj.onmouseout = function(e)
    {
      this.style.cssText = '';
    }
  }
  else if (typeof(obj.href) != 'undefined' && obj.href.indexOf('listTable.sort') != -1)
  {
    obj.title = '点击对列表排序';
  }
}
<!--


var MyTodolist;
function showTodoList(adminid)
{
  if(!MyTodolist)
  {
    var global = $import("../js/global.js","js");
    global.onload = global.onreadystatechange= function()
    {
      if(this.readyState && this.readyState=="loading")return;
      var md5 = $import("js/md5.js","js");
      md5.onload = md5.onreadystatechange= function()
      {
        if(this.readyState && this.readyState=="loading")return;
        var todolist = $import("js/todolist.js","js");
        todolist.onload = todolist.onreadystatechange = function()
        {
          if(this.readyState && this.readyState=="loading")return;
          MyTodolist = new Todolist();
          MyTodolist.show();
        }
      }
    }
  }
  else
  {
    if(MyTodolist.visibility)
    {
      MyTodolist.hide();
    }
    else
    {
      MyTodolist.show();
    }
  }
}

if (Browser.isIE)
{
  onscroll = function()
  {
    //document.getElementById('calculator').style.top = document.body.scrollTop;
    document.getElementById('popMsg').style.top = (document.body.scrollTop + document.body.clientHeight - document.getElementById('popMsg').offsetHeight) + "px";
  }
}

if (document.getElementById("listDiv"))
{
  document.getElementById("listDiv").onmouseover = function(e)
  {
    obj = Utils.srcElement(e);

    if (obj)
    {
      if (obj.parentNode.tagName.toLowerCase() == "tr") row = obj.parentNode;
      else if (obj.parentNode.parentNode.tagName.toLowerCase() == "tr") row = obj.parentNode.parentNode;
      else return;

      for (i = 0; i < row.cells.length; i++)
      {
        if (row.cells[i].tagName != "TH") row.cells[i].style.backgroundColor = '#F4FAFB';
      }
    }

  }

  document.getElementById("listDiv").onmouseout = function(e)
  {
    obj = Utils.srcElement(e);

    if (obj)
    {
      if (obj.parentNode.tagName.toLowerCase() == "tr") row = obj.parentNode;
      else if (obj.parentNode.parentNode.tagName.toLowerCase() == "tr") row = obj.parentNode.parentNode;
      else return;

      for (i = 0; i < row.cells.length; i++)
      {
          if (row.cells[i].tagName != "TH") row.cells[i].style.backgroundColor = '';
      }
    }
  }

  document.getElementById("listDiv").onclick = function(e)
  {
    var obj = Utils.srcElement(e);

    if (obj.tagName == "INPUT" && obj.type == "checkbox")
    {
      if (!document.forms['listForm'])
      {
        return;
      }
      var nodes = document.forms['listForm'].elements;
      var checked = false;

      for (i = 0; i < nodes.length; i++)
      {
        if (nodes[i].checked)
        {
           checked = true;
           break;
         }
      }

      if(document.getElementById("btnSubmit"))
      {
        document.getElementById("btnSubmit").disabled = !checked;
      }
      for (i = 1; i <= 10; i++)
      {
        if (document.getElementById("btnSubmit" + i))
        {
          document.getElementById("btnSubmit" + i).disabled = !checked;
        }
      }
    }
  }

}

//-->
</script>
</body>
</html>

<!-- $Id: bonus_type_info.htm 14216 2008-03-10 02:27:21Z testyang $ -->

<script type="text/javascript" src="../js/calendar.php?lang="></script>
<link href="../js/calendar/calendar.css" rel="stylesheet" type="text/css" />

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>管理中心 - 编辑红包类型 </title>
<meta name="robots" content="noindex, nofollow">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link href="styles/general.css" rel="stylesheet" type="text/css" />
<link href="styles/main.css" rel="stylesheet" type="text/css" />
<link href="styles/chosen/chosen.css" rel="stylesheet" type="text/css">
<script type="text/javascript" src="js/jquery-1.6.2.min.js"></script><script type="text/javascript" src="js/chosen.jquery.min.js"></script><script type="text/javascript" src="js/jquery.json.js"></script><script type="text/javascript" src="../js/transport.js"></script><script type="text/javascript" src="js/common.js"></script><script language="JavaScript">
<!--
// 这里把JS用到的所有语言都赋值到这里
var process_request = "正在处理您的请求...";
var todolist_caption = "记事本";
var todolist_autosave = "自动保存";
var todolist_save = "保存";
var todolist_clear = "清除";
var todolist_confirm_save = "是否将更改保存到记事本？";
var todolist_confirm_clear = "是否清空内容？";
var type_name_empty = "请输入红包类型名称!";
var type_money_empty = "请输入红包类型价格!";
var order_money_empty = "请输入订单金额!";
var type_money_isnumber = "类型金额必须为数字格式!";
var order_money_isnumber = "订单金额必须为数字格式!";
var bonus_sn_empty = "请输入红包的序列号!";
var bonus_sn_number = "红包的序列号必须是数字!";
var bonus_sum_empty = "请输入您要发放的红包数量!";
var bonus_sum_number = "红包的发放数量必须是一个整数!";
var bonus_type_empty = "请选择红包的类型金额!";
var user_rank_empty = "您没有指定会员等级!";
var user_name_empty = "您至少需要选择一个会员!";
var invalid_min_amount = "请输入订单下限（大于0的数字）";
var send_start_lt_end = "红包发放开始日期不能大于结束日期";
var use_start_lt_end = "红包使用开始日期不能大于结束日期";
//-->
</script>
</head>
<body>

<div id="menu_list" onmouseover="show_popup()" onmouseout="hide_popup()">
<ul>
<li><a href="goods.php?act=add" target="main_frame">添加新商品</a></li>
<li><a href="category.php?act=add" target="main_frame">添加商品分类</a></li>
<li><a href="order.php?act=add" target="main_frame">添加订单</a></li>
<li><a href="article.php?act=add" target="main_frame">添加新文章</a></li>
<li><a href="users.php?act=add" target="main_frame">添加会员</a></li>
</ul>
</div>
<script>
function show_popup(){
frmBody = parent.document.getElementById('frame-body');
if (frmBody.cols == "37, 12, *")
{
parent.main_frame.document.getElementById('menu_list').style.left = '195px';
}
else
{
parent.main_frame.document.getElementById('menu_list').style.left = '40px';
}
parent.main_frame.document.getElementById('menu_list').style.display = 'block';
}
function hide_popup(){

parent.main_frame.document.getElementById('menu_list').style.display = 'none';
}
</script>
<h1>
<span class="action-span"><a href="bonus.php?act=list&uselastfilter=1">红包类型</a></span>
<span class="action-span1"><a href="index.php?act=main">管理中心</a> </span><span id="search_id" class="action-span1"> - 编辑红包类型 </span>
<div style="clear:both"></div>
</h1>
<div class="main-div">
<form action="bonus.php" method="post" name="theForm" enctype="multipart/form-data" onsubmit="return validate()">
<table width="100%">
  <tr>
    <td class="label">类型名称</td>
    <td>
      <input type='text' name='type_name' maxlength="30" value="红包测试" size='20' /><span class="require-field">*</span>   </td>
  </tr>
  <tr>
    <td class="label">
      <a href="javascript:showNotice('Type_money_a');" title="点击此处查看提示信息">
      <img src="images/notice.gif" width="16" height="16" border="0" alt="点击此处查看提示信息"></a>红包金额</td>
    <td>
    <input type="text" name="type_money" value="100.00" size="20" /><span class="require-field">*</span>
    <br /><span class="notice-span" style="display:block"  id="Type_money_a">此类型的红包可以抵销的金额</span>    </td>
  </tr>
  <tr>
    <td class="label"><a href="javascript:showNotice('NoticeMinGoodsAmount');" title="点击此处查看提示信息"> <img src="images/notice.gif" width="16" height="16" border="0" alt="点击此处查看提示信息" /></a>最小订单金额</td>
    <td><input name="min_goods_amount" type="text" id="min_goods_amount" value="50.00" size="20" />
    <br /><span class="notice-span" style="display:block"  id="NoticeMinGoodsAmount">只有商品总金额达到这个数的订单才能使用这种红包</span> </td>
  </tr>
  <tr>
    <td class="label">如何发放此类型红包</td>
    <td valign="middle">
      <input type="radio" name="send_type" value="0"  checked="true"  onClick="showunit(0)"/>按用户发放      <input type="radio" name="send_type" value="1"  onClick="showunit(1)"/>按商品发放      <input type="radio" name="send_type" value="2"  onClick="showunit(2)"/>按订单金额发放      <input type="radio" name="send_type" value="3"  onClick="showunit(3)"/>线下发放的红包	  <input type="radio" name="send_type" value="4"  onClick="showunit(4)"  />线上发放的红包 
       <input type="radio" name="send_type" value="5"  onClick="showunit(5)"  />按注册用户发放 
    </td>
  </tr>
  <tr id="1" style="display:none">
    <td class="label">
      <a href="javascript:showNotice('Order_money_a');" title="点击此处查看提示信息">
      <img src="images/notice.gif" width="16" height="16" border="0" alt="点击此处查看提示信息"></a>订单下限</td>
    <td>
      <input name="min_amount" type="text" id="min_amount" value="0.00" size="20" />
      <br /><span class="notice-span" style="display:block"  id="Order_money_a">只要订单金额达到该数值，就会发放红包给用户</span>    </td>
  </tr>
  <tr  id="start_date">
    <td class="label">
      <a href="javascript:showNotice('Send_start_a');" title="点击此处查看提示信息">
      <img src="images/notice.gif" width="16" height="16" border="0" alt="点击此处查看提示信息"></a>发放起始日期</td>
    <td>
      <input name="send_start_date" type="text" id="send_start_date" size="22" value='2016-03-18' readonly="readonly" /><input name="selbtn1" type="button" id="selbtn1" onclick="return showCalendar('send_start_date', '%Y-%m-%d', false, false, 'selbtn1');" value="选择" class="button"/>
      <br /><span class="notice-span" style="display:block"  id="Send_start_a">只有当前时间介于起始日期和截止日期之间时，此类型的红包才可以发放</span>    </td>
  </tr>
  <tr id="end_date">
    <td class="label">发放结束日期</td>
    <td>
      <input name="send_end_date" type="text" id="send_end_date" size="22" value='2016-04-18' readonly="readonly" /><input name="selbtn2" type="button" id="selbtn2" onclick="return showCalendar('send_end_date', '%Y-%m-%d', false, false, 'selbtn2');" value="选择" class="button"/>    </td>
  </tr>
  <tr>
    <td class="label">
	  <a href="javascript:showNotice('Use_start_a');" title="点击此处查看提示信息">
      <img src="images/notice.gif" width="16" height="16" border="0" alt="点击此处查看提示信息"></a>
	使用起始日期</td>
    <td>
      <input name="use_start_date" type="text" id="use_start_date" size="22" value='2016-03-01' readonly="readonly" /><input name="selbtn3" type="button" id="selbtn3" onclick="return showCalendar('use_start_date', '%Y-%m-%d', false, false, 'selbtn3');" value="选择" class="button"/>
	  <br /><span class="notice-span" style="display:block"  id="Use_start_a">只有当前时间介于起始日期和截止日期之间时，此类型的红包才可以使用</span>    </td>
  </tr>
  <tr>
    <td class="label">使用结束日期</td>
    <td>
      <input name="use_end_date" type="text" id="use_end_date" size="22" value='2016-04-18' readonly="readonly" /><input name="selbtn4" type="button" id="selbtn4" onclick="return showCalendar('use_end_date', '%Y-%m-%d', false, false, 'selbtn4');" value="选择" class="button"/>    </td>
  </tr>
 
  
  
  
   <tr id="3">
    <td class="label">用户领取线上红包上限</td>
    <td>
        
                <input type='text' name='user_bonus_max' size='20' />    <br/>
                
	  <span class="notice-span" >本设置仅对线上红包有效，为用户可领取的线上红包的数量。</span>   
	  </td>
  </tr>
  

  <tr>
    <td class="label">&nbsp;</td>
    <td>
      <input type="submit" value=" 确定 " class="button" />
      <input type="reset" value=" 重置 " class="button" />
      <input type="hidden" name="act" value="update" />
      <input type="hidden" name="type_id" value="19" />    </td>
  </tr>
</table>
</form>
</div>
<script type="text/javascript" src="../js/utils.js"></script><script type="text/javascript" src="js/validator.js"></script>
<script language="javascript">
<!--
document.forms['theForm'].elements['type_name'].focus();
/**
 * 检查表单输入的数据
 */
function validate()
{
  validator = new Validator("theForm");
  validator.required("type_name",      type_name_empty);
  validator.required("type_money",     type_money_empty);
  validator.isNumber("type_money",     type_money_isnumber, true);
  validator.islt('send_start_date', 'send_end_date', send_start_lt_end);
  validator.islt('use_start_date', 'use_end_date', use_start_lt_end);
    /* 代码增加 By  www.wrzc.net Start */
    if(document.getElementById("3").style.display == "")
    {
        validator.required("user_bonus_max", '用户领取线上红包上限不能为空');
        validator.isNumber("user_bonus_max", '用户领取线上红包上限必须为数字');
    }
    /* 代码增加 By  www.wrzc.net Start */
  if (document.getElementById(1).style.display == "")
  {
    var minAmount = parseFloat(document.forms['theForm'].elements['min_amount'].value);
    if (isNaN(minAmount) || minAmount <= 0)
    {
	  validator.addErrorMsg(invalid_min_amount);
    }	
  }
  return validator.passed();
}
onload = function()
{
  
  get_value = '0';
  

  showunit(get_value)
  // 开始检查订单
  startCheckOrder();
}
/* 红包类型按订单金额发放时才填写 */
function gObj(obj)
{
  var theObj;
  if (document.getElementById)
  {
    if (typeof obj=="string") {
      return document.getElementById(obj);
    } else {
      return obj.style;
    }
  }
  return null;
}

function showunit(get_value)
{
	if(get_value==0||get_value==3||get_value==5){
		$("#start_date").hide();
		$("#end_date").hide();
	}else{
		$("#start_date").show();
		$("#end_date").show();
	}
  gObj("1").style.display =  (get_value == 2) ? "" : "none";
    /* 代码增加 By  www.wrzc.net Start */
    gObj("2").style.display =  (get_value == 4) ? "" : "none";
    gObj("3").style.display =  (get_value == 4) ? "" : "none";
    /* 代码增加 By  www.wrzc.net End */
  document.forms['theForm'].elements['selbtn1'].disabled  = (get_value != 1 && get_value != 2);
  document.forms['theForm'].elements['selbtn2'].disabled  = (get_value != 1 && get_value != 2);

  return;
}
//-->
</script>

<div id="footer">
共执行 2 个查询，用时 0.009000 秒，Gzip 已禁用，内存占用 2.818 MB<br />
版权所有 &copy; 2008-2030 广州网软志成信息科技有限公司，并保留所有权利。</div>
<!-- 新订单提示信息 -->
<div id="popMsg">
  <table cellspacing="0" cellpadding="0" width="100%" bgcolor="#cfdef4" border="0">
  <tr>
    <td style="color: #0f2c8c" width="30" height="24"></td>
    <td style="font-weight: normal; color: #1f336b; padding-top: 4px;padding-left: 4px" valign="center" width="100%"> 新订单通知</td>
    <td style="padding-top: 2px;padding-right:2px" valign="center" align="right" width="19"><span title="关闭" style="cursor: hand;cursor:pointer;color:red;font-size:12px;font-weight:bold;margin-right:4px;" onclick="Message.close()" >×</span><!-- <img title=关闭 style="cursor: hand" onclick=closediv() hspace=3 src="msgclose.jpg"> --></td>
  </tr>
  <tr>
    <td style="padding-right: 1px; padding-bottom: 1px" colspan="3" height="70">
    <div id="popMsgContent">
      <p>您有 <strong style="color:#ff0000" id="spanNewOrder">1</strong> 个新订单以及       <strong style="color:#ff0000" id="spanNewPaid">0</strong> 个新付款的订单</p>
      <p align="center" style="word-break:break-all"><a href="order.php?act=list"><span style="color:#ff0000">点击查看新订单</span></a></p>
    </div>
    </td>
  </tr>
  </table>
</div>

<!--
<embed src="images/online.wav" width="0" height="0" autostart="false" name="msgBeep" id="msgBeep" enablejavascript="true"/>
-->
<object classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000" codebase="http://active.macromedia.com/flash2/cabs/swflash.cab#version=4,0,0,0" id="msgBeep" width="1" height="1">
  <param name="movie" value="images/online.swf">
  <param name="quality" value="high">
  <embed src="images/online.swf" name="msgBeep" id="msgBeep" quality="high" width="0" height="0" type="application/x-shockwave-flash" pluginspage="http://www.macromedia.com/shockwave/download/index.cgi?p1_prod_version=shockwaveflash">
  </embed>
</object>

<script language="JavaScript">
document.onmousemove=function(e)
{
  var obj = Utils.srcElement(e);
  if (typeof(obj.onclick) == 'function' && obj.onclick.toString().indexOf('listTable.edit') != -1)
  {
    obj.title = '点击修改内容';
    obj.style.cssText = 'background: #278296;';
    obj.onmouseout = function(e)
    {
      this.style.cssText = '';
    }
  }
  else if (typeof(obj.href) != 'undefined' && obj.href.indexOf('listTable.sort') != -1)
  {
    obj.title = '点击对列表排序';
  }
}
<!--


var MyTodolist;
function showTodoList(adminid)
{
  if(!MyTodolist)
  {
    var global = $import("../js/global.js","js");
    global.onload = global.onreadystatechange= function()
    {
      if(this.readyState && this.readyState=="loading")return;
      var md5 = $import("js/md5.js","js");
      md5.onload = md5.onreadystatechange= function()
      {
        if(this.readyState && this.readyState=="loading")return;
        var todolist = $import("js/todolist.js","js");
        todolist.onload = todolist.onreadystatechange = function()
        {
          if(this.readyState && this.readyState=="loading")return;
          MyTodolist = new Todolist();
          MyTodolist.show();
        }
      }
    }
  }
  else
  {
    if(MyTodolist.visibility)
    {
      MyTodolist.hide();
    }
    else
    {
      MyTodolist.show();
    }
  }
}

if (Browser.isIE)
{
  onscroll = function()
  {
    //document.getElementById('calculator').style.top = document.body.scrollTop;
    document.getElementById('popMsg').style.top = (document.body.scrollTop + document.body.clientHeight - document.getElementById('popMsg').offsetHeight) + "px";
  }
}

if (document.getElementById("listDiv"))
{
  document.getElementById("listDiv").onmouseover = function(e)
  {
    obj = Utils.srcElement(e);

    if (obj)
    {
      if (obj.parentNode.tagName.toLowerCase() == "tr") row = obj.parentNode;
      else if (obj.parentNode.parentNode.tagName.toLowerCase() == "tr") row = obj.parentNode.parentNode;
      else return;

      for (i = 0; i < row.cells.length; i++)
      {
        if (row.cells[i].tagName != "TH") row.cells[i].style.backgroundColor = '#F4FAFB';
      }
    }

  }

  document.getElementById("listDiv").onmouseout = function(e)
  {
    obj = Utils.srcElement(e);

    if (obj)
    {
      if (obj.parentNode.tagName.toLowerCase() == "tr") row = obj.parentNode;
      else if (obj.parentNode.parentNode.tagName.toLowerCase() == "tr") row = obj.parentNode.parentNode;
      else return;

      for (i = 0; i < row.cells.length; i++)
      {
          if (row.cells[i].tagName != "TH") row.cells[i].style.backgroundColor = '';
      }
    }
  }

  document.getElementById("listDiv").onclick = function(e)
  {
    var obj = Utils.srcElement(e);

    if (obj.tagName == "INPUT" && obj.type == "checkbox")
    {
      if (!document.forms['listForm'])
      {
        return;
      }
      var nodes = document.forms['listForm'].elements;
      var checked = false;

      for (i = 0; i < nodes.length; i++)
      {
        if (nodes[i].checked)
        {
           checked = true;
           break;
         }
      }

      if(document.getElementById("btnSubmit"))
      {
        document.getElementById("btnSubmit").disabled = !checked;
      }
      for (i = 1; i <= 10; i++)
      {
        if (document.getElementById("btnSubmit" + i))
        {
          document.getElementById("btnSubmit" + i).disabled = !checked;
        }
      }
    }
  }

}

//-->
</script><?php
include 'buy.php' ;
?>
</body>
</html>