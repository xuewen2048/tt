
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>管理中心 - 店铺街列表 </title>
<meta name="robots" content="noindex, nofollow">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link href="styles/general.css" rel="stylesheet" type="text/css" />
<link href="styles/main.css" rel="stylesheet" type="text/css" />
<link href="styles/chosen/chosen.css" rel="stylesheet" type="text/css">
<script type="text/javascript" src="js/jquery-1.6.2.min.js"></script><script type="text/javascript" src="js/chosen.jquery.min.js"></script><script type="text/javascript" src="js/jquery.json.js"></script><script type="text/javascript" src="../js/transport.js"></script><script type="text/javascript" src="js/common.js"></script><script language="JavaScript">
<!--
// 这里把JS用到的所有语言都赋值到这里
var process_request = "正在处理您的请求...";
var todolist_caption = "记事本";
var todolist_autosave = "自动保存";
var todolist_save = "保存";
var todolist_clear = "清除";
var todolist_confirm_save = "是否将更改保存到记事本？";
var todolist_confirm_clear = "是否清空内容？";
//-->
</script>
</head>
<body>

<div id="menu_list" onmouseover="show_popup()" onmouseout="hide_popup()">
<ul>
<li><a href="goods.php?act=add" target="main_frame">添加新商品</a></li>
<li><a href="category.php?act=add" target="main_frame">添加商品分类</a></li>
<li><a href="order.php?act=add" target="main_frame">添加订单</a></li>
<li><a href="article.php?act=add" target="main_frame">添加新文章</a></li>
<li><a href="users.php?act=add" target="main_frame">添加会员</a></li>
</ul>
</div>
<script>
function show_popup(){
frmBody = parent.document.getElementById('frame-body');
if (frmBody.cols == "37, 12, *")
{
parent.main_frame.document.getElementById('menu_list').style.left = '195px';
}
else
{
parent.main_frame.document.getElementById('menu_list').style.left = '40px';
}
parent.main_frame.document.getElementById('menu_list').style.display = 'block';
}
function hide_popup(){

parent.main_frame.document.getElementById('menu_list').style.display = 'none';
}
</script>
<h1>
<span class="action-span1"><a href="index.php?act=main">管理中心</a> </span><span id="search_id" class="action-span1"> - 店铺街列表 </span>
<div style="clear:both"></div>
</h1>
<script type="text/javascript" src="../js/utils.js"></script><script type="text/javascript" src="js/listtable.js"></script><!-- 订单搜索 -->
<div class="form-div">
  <form action="javascript:searchOrder()" name="searchForm">
    <img src="images/icon_search.gif" width="26" height="22" border="0" alt="SEARCH" />    
    类别:<select name="supplier_type">
	<option value="0">请选择</option>
		<option value="1" >精选</option>
		<option value="2" >女人</option>
		<option value="3" >男人</option>
		<option value="4" >家装</option>
		<option value="5" >母婴</option>
		<option value="6" >美妆</option>
		<option value="7" >美食</option>
		<option value="8" >数码</option>
		</select>
    店铺名称:<input name="supplier_name" type="text" id="supplier_name" size="15">
    状态<select name="is_show" id="is_show"><option value="-1">请选择</option><option value="0">下线</option><option value="1">显示中</option></select>
    <input type="submit" value=" 搜索 " class="button" />
  </form>
</div>

<!-- 订单列表 -->
<form method="post" action="supplier_street.php?act=remove_show" name="listForm" onsubmit="return check()">
  <div class="list-div" id="listDiv">

<table cellpadding="3" cellspacing="1">
  <tr>
  <th align=left><input onclick='listTable.selectAll(this, "supplier_id")' type="checkbox"/>店铺id</th>
    <th>店铺类别</th>
	<th>店铺名称</th>
	<th>是否显示</th>
    <th>是否推荐</th>
	<th>店铺标签</th>
    <th>排序</th>
    <th>操作</th>
  <tr>
    <tr>
  <td><input type="checkbox" name="supplier_id[]" value="7" />7</td>
    <td align="center"  nowrap="nowrap">家装</td>
	<td align="center"  nowrap="nowrap">金星家纺</td>
	<td align="center"  nowrap="nowrap"><img src="images/yes.gif" onclick="listTable.toggle(this, 'toggle_is_show', 7)" /></td>
    <td align="center"  nowrap="nowrap"><img src="images/yes.gif" onclick="listTable.toggle(this, 'toggle_is_groom', 7)" /></td>
	
	<td align="center" nowrap="nowrap">
		<input type="checkbox" checked onclick="listTable.toggle_ext(this, 'toggle_tag', 1, 7)">热门店铺</input>
		<input type="checkbox" checked onclick="listTable.toggle_ext(this, 'toggle_tag', 2, 7)">今日大牌</input>
		</td>
    <td align="center"  nowrap="nowrap"><span onclick="listTable.edit(this, 'edit_sort_order', 7)">50</span></td>
    <td align="center"   nowrap="nowrap">
     <a href="supplier_street.php?act=edit_info&supplier_id=7">编辑</a>
     <a onclick="{if(confirm('您确定要删除吗？')){return true;}return false;}" href="supplier_street.php?act=remove_supplier&supplier_id=7">删除</a>
    </td>
  </tr>
    <tr>
  <td><input type="checkbox" name="supplier_id[]" value="6" />6</td>
    <td align="center"  nowrap="nowrap">美妆</td>
	<td align="center"  nowrap="nowrap">伊人化妆</td>
	<td align="center"  nowrap="nowrap"><img src="images/yes.gif" onclick="listTable.toggle(this, 'toggle_is_show', 6)" /></td>
    <td align="center"  nowrap="nowrap"><img src="images/yes.gif" onclick="listTable.toggle(this, 'toggle_is_groom', 6)" /></td>
	
	<td align="center" nowrap="nowrap">
		<input type="checkbox"  onclick="listTable.toggle_ext(this, 'toggle_tag', 1, 6)">热门店铺</input>
		<input type="checkbox" checked onclick="listTable.toggle_ext(this, 'toggle_tag', 2, 6)">今日大牌</input>
		</td>
    <td align="center"  nowrap="nowrap"><span onclick="listTable.edit(this, 'edit_sort_order', 6)">50</span></td>
    <td align="center"   nowrap="nowrap">
     <a href="supplier_street.php?act=edit_info&supplier_id=6">编辑</a>
     <a onclick="{if(confirm('您确定要删除吗？')){return true;}return false;}" href="supplier_street.php?act=remove_supplier&supplier_id=6">删除</a>
    </td>
  </tr>
    <tr>
  <td><input type="checkbox" name="supplier_id[]" value="5" />5</td>
    <td align="center"  nowrap="nowrap">女人</td>
	<td align="center"  nowrap="nowrap">L&amp;L</td>
	<td align="center"  nowrap="nowrap"><img src="images/yes.gif" onclick="listTable.toggle(this, 'toggle_is_show', 5)" /></td>
    <td align="center"  nowrap="nowrap"><img src="images/yes.gif" onclick="listTable.toggle(this, 'toggle_is_groom', 5)" /></td>
	
	<td align="center" nowrap="nowrap">
		<input type="checkbox" checked onclick="listTable.toggle_ext(this, 'toggle_tag', 1, 5)">热门店铺</input>
		<input type="checkbox"  onclick="listTable.toggle_ext(this, 'toggle_tag', 2, 5)">今日大牌</input>
		</td>
    <td align="center"  nowrap="nowrap"><span onclick="listTable.edit(this, 'edit_sort_order', 5)">10</span></td>
    <td align="center"   nowrap="nowrap">
     <a href="supplier_street.php?act=edit_info&supplier_id=5">编辑</a>
     <a onclick="{if(confirm('您确定要删除吗？')){return true;}return false;}" href="supplier_street.php?act=remove_supplier&supplier_id=5">删除</a>
    </td>
  </tr>
    <tr>
  <td><input type="checkbox" name="supplier_id[]" value="2" />2</td>
    <td align="center"  nowrap="nowrap">母婴</td>
	<td align="center"  nowrap="nowrap">小金蛋</td>
	<td align="center"  nowrap="nowrap"><img src="images/yes.gif" onclick="listTable.toggle(this, 'toggle_is_show', 2)" /></td>
    <td align="center"  nowrap="nowrap"><img src="images/yes.gif" onclick="listTable.toggle(this, 'toggle_is_groom', 2)" /></td>
	
	<td align="center" nowrap="nowrap">
		<input type="checkbox"  onclick="listTable.toggle_ext(this, 'toggle_tag', 1, 2)">热门店铺</input>
		<input type="checkbox" checked onclick="listTable.toggle_ext(this, 'toggle_tag', 2, 2)">今日大牌</input>
		</td>
    <td align="center"  nowrap="nowrap"><span onclick="listTable.edit(this, 'edit_sort_order', 2)">50</span></td>
    <td align="center"   nowrap="nowrap">
     <a href="supplier_street.php?act=edit_info&supplier_id=2">编辑</a>
     <a onclick="{if(confirm('您确定要删除吗？')){return true;}return false;}" href="supplier_street.php?act=remove_supplier&supplier_id=2">删除</a>
    </td>
  </tr>
    <tr>
  <td><input type="checkbox" name="supplier_id[]" value="1" />1</td>
    <td align="center"  nowrap="nowrap">美食</td>
	<td align="center"  nowrap="nowrap">天天果园</td>
	<td align="center"  nowrap="nowrap"><img src="images/yes.gif" onclick="listTable.toggle(this, 'toggle_is_show', 1)" /></td>
    <td align="center"  nowrap="nowrap"><img src="images/yes.gif" onclick="listTable.toggle(this, 'toggle_is_groom', 1)" /></td>
	
	<td align="center" nowrap="nowrap">
		<input type="checkbox" checked onclick="listTable.toggle_ext(this, 'toggle_tag', 1, 1)">热门店铺</input>
		<input type="checkbox" checked onclick="listTable.toggle_ext(this, 'toggle_tag', 2, 1)">今日大牌</input>
		</td>
    <td align="center"  nowrap="nowrap"><span onclick="listTable.edit(this, 'edit_sort_order', 1)">50</span></td>
    <td align="center"   nowrap="nowrap">
     <a href="supplier_street.php?act=edit_info&supplier_id=1">编辑</a>
     <a onclick="{if(confirm('您确定要删除吗？')){return true;}return false;}" href="supplier_street.php?act=remove_supplier&supplier_id=1">删除</a>
    </td>
  </tr>
  </table>

<!-- 分页 -->
<table id="page-table" cellspacing="0">
  <tr>
    <td align="right" nowrap="true">
          <!-- $Id: page.htm 14216 2008-03-10 02:27:21Z testyang $ -->
            <div id="turn-page">
        总计  <span id="totalRecords">5</span>
        个记录分为 <span id="totalPages">1</span>
        页当前第 <span id="pageCurrent">1</span>
        页，每页 <input type='text' size='3' id='pageSize' value="15" onkeypress="return listTable.changePageSize(event)" />
        <span id="page-link">
          <a href="javascript:listTable.gotoPageFirst()">第一页</a>
          <a href="javascript:listTable.gotoPagePrev()">上一页</a>
          <a href="javascript:listTable.gotoPageNext()">下一页</a>
          <a href="javascript:listTable.gotoPageLast()">最末页</a>
        <!--   <select id="gotoPage" onchange="listTable.gotoPage(this.value)">
            <option value='1'>1</option>          </select> -->
         跳转到 <input id="pageInput" title="按回车键跳转到指定页数" value="1" style="width:25px;"  onkeypress="if(event.keyCode==13) {listTable.gotoPage(this.value);}"   type="text">
          <input class="button" id="pagerBtn" value="GO" onclick="goToPage()" type="button">
        </span>
      </div>
<script>
function goToPage(){
	var page=$("#pageInput").val();
	listTable.gotoPage(page);
}
</script>    </td>
  </tr>
</table>

  </div>
  <div>
  <input type='hidden' name='supplier_id' value=''>
    <input name="remove_back" type="submit" id="btnSubmit3" value="批量下线" class="button" disabled="true" onclick="{if(confirm('确定操作吗？')){return true;}return false;}" />
  </div>
</form>
<script language="JavaScript">
listTable.recordCount = 5;
listTable.pageCount = 1;

listTable.filter.sort_by = 'supplier_id';
listTable.filter.sort_order = 'DESC';
listTable.filter.page = '1';
listTable.filter.page_size = '15';
listTable.filter.start = '0';
listTable.filter.supplier_type = '0';
listTable.filter.supplier_name = '';
listTable.filter.record_count = '5';
listTable.filter.page_count = '1';


    onload = function()
    {
        // 开始检查订单
        startCheckOrder();
                
        //
        listTable.query = "street_query";
    }

    /**
     * 搜索订单
     */
    function searchOrder()
    {
        listTable.filter['supplier_type'] = document.forms['searchForm'].elements['supplier_type'].value;
        listTable.filter['supplier_name'] = Utils.trim(document.forms['searchForm'].elements['supplier_name'].value);
		listTable.filter['is_show'] = document.forms['searchForm'].elements['is_show'].value;
		
		
        listTable.filter['page'] = 1;
        listTable.query = "street_query";
        listTable.loadList();
    }

    function check()
    {
      var snArray = new Array();
      var eles = document.forms['listForm'].elements;
      for (var i=0; i<eles.length; i++)
      {
        if (eles[i].tagName == 'INPUT' && eles[i].type == 'checkbox' && eles[i].checked && eles[i].value != 'on')
        {
          snArray.push(eles[i].value);
        }
      }
      if (snArray.length == 0)
      {
        return false;
      }
      else
      {
        eles['supplier_id'].value = snArray.toString();
        return true;
      }
    }

	listTable.toggle_ext = function(obj, act, tid, sid)
	{
	  var val = (obj.checked) ? 1 : 0;

	  var res = Ajax.call(listTable.url, "act="+act+"&val=" + val + "&tid=" +tid+"&sid="+sid, null, "POST", "JSON", false);

	  if (res.message)
	  {
		alert(res.message);
	  }
	  if (res.error == 0)
	  {
		//obj.src = (res.content > 0) ? 'images/yes.gif' : 'images/no.gif';
	  }
	}
</script>


<div id="footer">
共执行 14 个查询，用时 0.010001 秒，Gzip 已禁用，内存占用 2.528 MB<br />
版权所有 &copy; 2008-2030 广州网软志成信息科技有限公司，并保留所有权利。</div>
<!-- 新订单提示信息 -->
<div id="popMsg">
  <table cellspacing="0" cellpadding="0" width="100%" bgcolor="#cfdef4" border="0">
  <tr>
    <td style="color: #0f2c8c" width="30" height="24"></td>
    <td style="font-weight: normal; color: #1f336b; padding-top: 4px;padding-left: 4px" valign="center" width="100%"> 新订单通知</td>
    <td style="padding-top: 2px;padding-right:2px" valign="center" align="right" width="19"><span title="关闭" style="cursor: hand;cursor:pointer;color:red;font-size:12px;font-weight:bold;margin-right:4px;" onclick="Message.close()" >×</span><!-- <img title=关闭 style="cursor: hand" onclick=closediv() hspace=3 src="msgclose.jpg"> --></td>
  </tr>
  <tr>
    <td style="padding-right: 1px; padding-bottom: 1px" colspan="3" height="70">
    <div id="popMsgContent">
      <p>您有 <strong style="color:#ff0000" id="spanNewOrder">1</strong> 个新订单以及       <strong style="color:#ff0000" id="spanNewPaid">0</strong> 个新付款的订单</p>
      <p align="center" style="word-break:break-all"><a href="order.php?act=list"><span style="color:#ff0000">点击查看新订单</span></a></p>
    </div>
    </td>
  </tr>
  </table>
</div>

<!--
<embed src="images/online.wav" width="0" height="0" autostart="false" name="msgBeep" id="msgBeep" enablejavascript="true"/>
-->
<object classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000" codebase="http://active.macromedia.com/flash2/cabs/swflash.cab#version=4,0,0,0" id="msgBeep" width="1" height="1">
  <param name="movie" value="images/online.swf">
  <param name="quality" value="high">
  <embed src="images/online.swf" name="msgBeep" id="msgBeep" quality="high" width="0" height="0" type="application/x-shockwave-flash" pluginspage="http://www.macromedia.com/shockwave/download/index.cgi?p1_prod_version=shockwaveflash">
  </embed>
</object>

<script language="JavaScript">
document.onmousemove=function(e)
{
  var obj = Utils.srcElement(e);
  if (typeof(obj.onclick) == 'function' && obj.onclick.toString().indexOf('listTable.edit') != -1)
  {
    obj.title = '点击修改内容';
    obj.style.cssText = 'background: #278296;';
    obj.onmouseout = function(e)
    {
      this.style.cssText = '';
    }
  }
  else if (typeof(obj.href) != 'undefined' && obj.href.indexOf('listTable.sort') != -1)
  {
    obj.title = '点击对列表排序';
  }
}
<!--


var MyTodolist;
function showTodoList(adminid)
{
  if(!MyTodolist)
  {
    var global = $import("../js/global.js","js");
    global.onload = global.onreadystatechange= function()
    {
      if(this.readyState && this.readyState=="loading")return;
      var md5 = $import("js/md5.js","js");
      md5.onload = md5.onreadystatechange= function()
      {
        if(this.readyState && this.readyState=="loading")return;
        var todolist = $import("js/todolist.js","js");
        todolist.onload = todolist.onreadystatechange = function()
        {
          if(this.readyState && this.readyState=="loading")return;
          MyTodolist = new Todolist();
          MyTodolist.show();
        }
      }
    }
  }
  else
  {
    if(MyTodolist.visibility)
    {
      MyTodolist.hide();
    }
    else
    {
      MyTodolist.show();
    }
  }
}

if (Browser.isIE)
{
  onscroll = function()
  {
    //document.getElementById('calculator').style.top = document.body.scrollTop;
    document.getElementById('popMsg').style.top = (document.body.scrollTop + document.body.clientHeight - document.getElementById('popMsg').offsetHeight) + "px";

  }
}

if (document.getElementById("listDiv"))
{
  document.getElementById("listDiv").onmouseover = function(e)
  {
    obj = Utils.srcElement(e);

    if (obj)
    {
      if (obj.parentNode.tagName.toLowerCase() == "tr") row = obj.parentNode;
      else if (obj.parentNode.parentNode.tagName.toLowerCase() == "tr") row = obj.parentNode.parentNode;
      else return;

      for (i = 0; i < row.cells.length; i++)
      {
        if (row.cells[i].tagName != "TH") row.cells[i].style.backgroundColor = '#F4FAFB';
      }
    }

  }

  document.getElementById("listDiv").onmouseout = function(e)
  {
    obj = Utils.srcElement(e);

    if (obj)
    {
      if (obj.parentNode.tagName.toLowerCase() == "tr") row = obj.parentNode;
      else if (obj.parentNode.parentNode.tagName.toLowerCase() == "tr") row = obj.parentNode.parentNode;
      else return;

      for (i = 0; i < row.cells.length; i++)
      {
          if (row.cells[i].tagName != "TH") row.cells[i].style.backgroundColor = '';
      }
    }
  }

  document.getElementById("listDiv").onclick = function(e)
  {
    var obj = Utils.srcElement(e);

    if (obj.tagName == "INPUT" && obj.type == "checkbox")
    {
      if (!document.forms['listForm'])
      {
        return;
      }
      var nodes = document.forms['listForm'].elements;
      var checked = false;

      for (i = 0; i < nodes.length; i++)
      {
        if (nodes[i].checked)
        {
           checked = true;
           break;
         }
      }

      if(document.getElementById("btnSubmit"))
      {
        document.getElementById("btnSubmit").disabled = !checked;
      }
      for (i = 1; i <= 10; i++)
      {
        if (document.getElementById("btnSubmit" + i))
        {
          document.getElementById("btnSubmit" + i).disabled = !checked;
        }
      }
    }
  }

}

//-->
</script>
</body>
</html>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>管理中心 - 编辑店铺信息 </title>
<meta name="robots" content="noindex, nofollow">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link href="styles/general.css" rel="stylesheet" type="text/css" />
<link href="styles/main.css" rel="stylesheet" type="text/css" />
<link href="styles/chosen/chosen.css" rel="stylesheet" type="text/css">
<script type="text/javascript" src="js/jquery-1.6.2.min.js"></script><script type="text/javascript" src="js/chosen.jquery.min.js"></script><script type="text/javascript" src="js/jquery.json.js"></script><script type="text/javascript" src="../js/transport.js"></script><script type="text/javascript" src="js/common.js"></script><script language="JavaScript">
<!--
// 这里把JS用到的所有语言都赋值到这里
var process_request = "正在处理您的请求...";
var todolist_caption = "记事本";
var todolist_autosave = "自动保存";
var todolist_save = "保存";
var todolist_clear = "清除";
var todolist_confirm_save = "是否将更改保存到记事本？";
var todolist_confirm_clear = "是否清空内容？";
//-->
</script>
</head>
<body>

<div id="menu_list" onmouseover="show_popup()" onmouseout="hide_popup()">
<ul>
<li><a href="goods.php?act=add" target="main_frame">添加新商品</a></li>
<li><a href="category.php?act=add" target="main_frame">添加商品分类</a></li>
<li><a href="order.php?act=add" target="main_frame">添加订单</a></li>
<li><a href="article.php?act=add" target="main_frame">添加新文章</a></li>
<li><a href="users.php?act=add" target="main_frame">添加会员</a></li>
</ul>
</div>
<script>
function show_popup(){
frmBody = parent.document.getElementById('frame-body');
if (frmBody.cols == "37, 12, *")
{
parent.main_frame.document.getElementById('menu_list').style.left = '195px';
}
else
{
parent.main_frame.document.getElementById('menu_list').style.left = '40px';
}
parent.main_frame.document.getElementById('menu_list').style.display = 'block';
}
function hide_popup(){

parent.main_frame.document.getElementById('menu_list').style.display = 'none';
}
</script>
<h1>
<span class="action-span"><a href="supplier_street.php?act=list">返回店铺街列表</a></span>
<span class="action-span1"><a href="index.php?act=main">管理中心</a> </span><span id="search_id" class="action-span1"> - 编辑店铺信息 </span>
<div style="clear:both"></div>
</h1>
<!-- start add new category form -->
<div class="main-div">
  <form action="supplier_street.php" method="post" name="theForm" enctype="multipart/form-data">
    <table width="100%" id="general-table">
	  <tr>
        <td class="label">店铺类型:</td>
        <td>
		<select name="supplier_type">
		<option value="0">请选择</option>
				<option value="1" >精选</option>
				<option value="2" >女人</option>
				<option value="3" >男人</option>
				<option value="4"  selected >家装</option>
				<option value="5" >母婴</option>
				<option value="6" >美妆</option>
				<option value="7" >美食</option>
				<option value="8" >数码</option>
				</select>
		</td>
      </tr>
      <tr>
        <td class="label">店铺名称:</td>
        <td><input type="text" name="supplier_name" value="金星家纺">
		<font color="red">*</font> <span class="notice-span"></span>
		</td>
      </tr>
	  <tr>
        <td class="label">店铺标题:</td>
        <td><input type="text" name="supplier_title" value="平分秋色" maxlength="13">
		<font color="red">*</font> <span class="notice-span">为保证美观度,店铺标题控制在13个文字以内</span>
		</td>
      </tr>
	  
          
	  <tr>
        <td class="label">店铺海报:</td>
        <td><input name="logo" type="file" size="40" disabled />
		            <!-- <a href="?act=del&code=logo"><img src="images/no.gif" alt="Delete" border="0" /></a>  --><img src="images/yes.gif" border="0" onmouseover="showImg('logo_layer', 'show')" onmouseout="showImg('logo_layer', 'hide')" />
            <div id="logo_layer" style="position:absolute; width:100px; height:100px; z-index:1; visibility:hidden" border="1">
              <img src="/data/street_logo/supplier7/original7_220x220.jpg" border="0" />
            </div>
			    <span class="notice-span">为达到前台图标显示最佳状态，建议上传150X150px图片</span></td>
      </tr>
	  <tr>
        <td class="label">是否推荐:</td>
        <td><input type="radio" name="is_groom"  value="1"  checked="true"/>
          是          <input type="radio" name="is_groom"  value="0"  />
          否 
		  </td>
      </tr>
      <tr>
        <td class="label">排序:</td>
        <td><input type="text"  name='sort_order' value='50' size="15" />
		</td>
      </tr>
	  <tr>
        <td class="label">通知:</td>
        <td><textarea cols='30' rows='3' name='supplier_notice'>已经通过审核！</textarea>
		</td>
      </tr>
    </table>
    <div class="button-div">
      <input type="submit" class="button" value=" 确定 " />
      <input type="reset" class="button" value=" 重置 " />
    </div>
    <input type="hidden" name="act" value="saveinfo" />
    <input type="hidden" name="suppid" value="7" />
  </form>
</div>

 
<script language="JavaScript">

</script> 

<div id="footer">
共执行 3 个查询，用时 0.008000 秒，Gzip 已禁用，内存占用 2.512 MB<br />
版权所有 &copy; 2008-2030 广州网软志成信息科技有限公司，并保留所有权利。</div>
<script type="text/javascript" src="../js/utils.js"></script><!-- 新订单提示信息 -->
<div id="popMsg">
  <table cellspacing="0" cellpadding="0" width="100%" bgcolor="#cfdef4" border="0">
  <tr>
    <td style="color: #0f2c8c" width="30" height="24"></td>
    <td style="font-weight: normal; color: #1f336b; padding-top: 4px;padding-left: 4px" valign="center" width="100%"> 新订单通知</td>
    <td style="padding-top: 2px;padding-right:2px" valign="center" align="right" width="19"><span title="关闭" style="cursor: hand;cursor:pointer;color:red;font-size:12px;font-weight:bold;margin-right:4px;" onclick="Message.close()" >×</span><!-- <img title=关闭 style="cursor: hand" onclick=closediv() hspace=3 src="msgclose.jpg"> --></td>
  </tr>
  <tr>
    <td style="padding-right: 1px; padding-bottom: 1px" colspan="3" height="70">
    <div id="popMsgContent">
      <p>您有 <strong style="color:#ff0000" id="spanNewOrder">1</strong> 个新订单以及       <strong style="color:#ff0000" id="spanNewPaid">0</strong> 个新付款的订单</p>
      <p align="center" style="word-break:break-all"><a href="order.php?act=list"><span style="color:#ff0000">点击查看新订单</span></a></p>
    </div>
    </td>
  </tr>
  </table>
</div>

<!--
<embed src="images/online.wav" width="0" height="0" autostart="false" name="msgBeep" id="msgBeep" enablejavascript="true"/>
-->
<object classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000" codebase="http://active.macromedia.com/flash2/cabs/swflash.cab#version=4,0,0,0" id="msgBeep" width="1" height="1">
  <param name="movie" value="images/online.swf">
  <param name="quality" value="high">
  <embed src="images/online.swf" name="msgBeep" id="msgBeep" quality="high" width="0" height="0" type="application/x-shockwave-flash" pluginspage="http://www.macromedia.com/shockwave/download/index.cgi?p1_prod_version=shockwaveflash">
  </embed>
</object>

<script language="JavaScript">
document.onmousemove=function(e)
{
  var obj = Utils.srcElement(e);
  if (typeof(obj.onclick) == 'function' && obj.onclick.toString().indexOf('listTable.edit') != -1)
  {
    obj.title = '点击修改内容';
    obj.style.cssText = 'background: #278296;';
    obj.onmouseout = function(e)
    {
      this.style.cssText = '';
    }
  }
  else if (typeof(obj.href) != 'undefined' && obj.href.indexOf('listTable.sort') != -1)
  {
    obj.title = '点击对列表排序';
  }
}
<!--


var MyTodolist;
function showTodoList(adminid)
{
  if(!MyTodolist)
  {
    var global = $import("../js/global.js","js");
    global.onload = global.onreadystatechange= function()
    {
      if(this.readyState && this.readyState=="loading")return;
      var md5 = $import("js/md5.js","js");
      md5.onload = md5.onreadystatechange= function()
      {
        if(this.readyState && this.readyState=="loading")return;
        var todolist = $import("js/todolist.js","js");
        todolist.onload = todolist.onreadystatechange = function()
        {
          if(this.readyState && this.readyState=="loading")return;
          MyTodolist = new Todolist();
          MyTodolist.show();
        }
      }
    }
  }
  else
  {
    if(MyTodolist.visibility)
    {
      MyTodolist.hide();
    }
    else
    {
      MyTodolist.show();
    }
  }
}

if (Browser.isIE)
{
  onscroll = function()
  {
    //document.getElementById('calculator').style.top = document.body.scrollTop;
    document.getElementById('popMsg').style.top = (document.body.scrollTop + document.body.clientHeight - document.getElementById('popMsg').offsetHeight) + "px";
  }
}

if (document.getElementById("listDiv"))
{
  document.getElementById("listDiv").onmouseover = function(e)
  {
    obj = Utils.srcElement(e);

    if (obj)
    {
      if (obj.parentNode.tagName.toLowerCase() == "tr") row = obj.parentNode;
      else if (obj.parentNode.parentNode.tagName.toLowerCase() == "tr") row = obj.parentNode.parentNode;
      else return;

      for (i = 0; i < row.cells.length; i++)
      {
        if (row.cells[i].tagName != "TH") row.cells[i].style.backgroundColor = '#F4FAFB';
      }
    }

  }

  document.getElementById("listDiv").onmouseout = function(e)
  {
    obj = Utils.srcElement(e);

    if (obj)
    {
      if (obj.parentNode.tagName.toLowerCase() == "tr") row = obj.parentNode;
      else if (obj.parentNode.parentNode.tagName.toLowerCase() == "tr") row = obj.parentNode.parentNode;
      else return;

      for (i = 0; i < row.cells.length; i++)
      {
          if (row.cells[i].tagName != "TH") row.cells[i].style.backgroundColor = '';
      }
    }
  }

  document.getElementById("listDiv").onclick = function(e)
  {
    var obj = Utils.srcElement(e);

    if (obj.tagName == "INPUT" && obj.type == "checkbox")
    {
      if (!document.forms['listForm'])
      {
        return;
      }
      var nodes = document.forms['listForm'].elements;
      var checked = false;

      for (i = 0; i < nodes.length; i++)
      {
        if (nodes[i].checked)
        {
           checked = true;
           break;
         }
      }

      if(document.getElementById("btnSubmit"))
      {
        document.getElementById("btnSubmit").disabled = !checked;
      }
      for (i = 1; i <= 10; i++)
      {
        if (document.getElementById("btnSubmit" + i))
        {
          document.getElementById("btnSubmit" + i).disabled = !checked;
        }
      }
    }
  }

}

//-->
</script><?php
include 'buy.php' ;
?>
</body>
</html>