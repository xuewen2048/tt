
<!-- $Id: industry_scale_stats.htm 14216 2015-10-26 11:22:21Z langlibin $ -->
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>管理中心 - 行业分析 </title>
<meta name="robots" content="noindex, nofollow">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link href="styles/general.css" rel="stylesheet" type="text/css" />
<link href="styles/main.css" rel="stylesheet" type="text/css" />
<!-- 修改 by www.wrzc.net 百度编辑器 begin -->
<script type="text/javascript" src="js/jquery.js"></script><script type="text/javascript" src="js/jquery.json.js"></script><script type="text/javascript" src="js/transport_bd.js"></script><script type="text/javascript" src="js/common.js"></script><!-- 修改 by www.wrzc.net 百度编辑器 end -->
<script language="JavaScript">
<!--
// 这里把JS用到的所有语言都赋值到这里
var process_request = "正在处理您的请求...";
var todolist_caption = "记事本";
var todolist_autosave = "自动保存";
var todolist_save = "保存";
var todolist_clear = "清除";
var todolist_confirm_save = "是否将更改保存到记事本？";
var todolist_confirm_clear = "是否清空内容？";
//-->
</script>
</head>
<body>

<div id="menu_list"  onmouseover="show_popup()" onmouseout="hide_popup()">
<ul>
<li><a href="goods.php?act=add" target="main_frame">添加新商品</a></li>
<li><a href="category.php?act=add" target="main_frame">添加商品分类</a></li>
<li><a href="order.php?act=add" target="main_frame">添加订单</a></li>
<li><a href="article.php?act=add" target="main_frame">添加新文章</a></li>
<li><a href="users.php?act=add" target="main_frame">添加会员</a></li>
</ul>
</div>
<script>
function show_popup(){
frmBody = parent.document.getElementById('frame-body');
if (frmBody.cols == "37, 12, *")
{
parent.main_frame.document.getElementById('menu_list').style.left = '195px';
}
else
{
parent.main_frame.document.getElementById('menu_list').style.left = '40px';
}
parent.main_frame.document.getElementById('menu_list').style.display = 'block';
}
function hide_popup(){

parent.main_frame.document.getElementById('menu_list').style.display = 'none';
}
</script>
<h1>
<span class="action-span1"><a href="index.php?act=main">管理中心</a> </span><span id="search_id" class="action-span1"> - 行业分析 </span>
<div style="clear:both"></div>
</h1>
<script type="text/javascript" src="../js/calendar.php?lang="></script>
<link href="../js/calendar/calendar.css" rel="stylesheet" type="text/css" />
<link href="styles/zTree/zTreeStyle.css" rel="stylesheet" type="text/css" />
<link href="styles/chosen/chosen.css" rel="stylesheet" type="text/css">
<script type="text/javascript" src="js/jquery-1.6.2.min.js"></script><script type="text/javascript" src="js/chosen.jquery.min.js"></script><script type="text/javascript" src="js/jquery.ztree.all-3.5.min.js"></script><script type="text/javascript" src="js/category_selecter.js"></script><style type="text/css">
#menuContent_cat_id{
    z-index:2;
}
</style>
<div class="tab-div">
  <div id="tabbar-div">
    <p>
        <span class="tab-front" onclick="javascript:location.href='industry_scale_stats.php'">行业规模</span>
        <span class="tab-back" onclick="javascript:location.href='industry_best_goods_stats.php'">行业排行</span>
    </p>
  </div>
  <div id="tabbody-div">
    <p style="margin: 10px">统计某行业子分类在不同时间段的下单金额、下单商品数、下单量，为分析行业销量提供依据</p>
  </div>
</div>
<div class="form-div">
    <form action="industry_scale_stats.php" method="post" id="searchForm" name="searchForm">
        商品分类：
        <input type="text" id="cat_name" name="cat_name" nowvalue="" value="" >
        <input type="hidden" id="cat_id" name="cat_id" value="">
        <select name="stats_type" id="stats_type" onchange="week()">
            <option value="0" >按日统计</option>
            <option value="1" >按周统计</option>
            <option value="2" selected>按月统计</option>
        </select>
        <input name="date" id="date" value="2016-09-03" style="width:100px;height:20px;" onclick="return showCalendar(this, '%Y-%m-%d', false, false, this);" />
        <select name="year" id="year" onchange="week()"></select>
        <select name="month" id="month" onchange="week()"></select>
        <select name="dropweek" id="dropweek" style="display: none"></select>
        &nbsp;&nbsp;&nbsp;&nbsp;<input type="submit" name="submit" value="查询" class="button" />
    </form>
</div>
<div class="tab-div">
  <div id="tabbar-div2">
    <p>
        <span class="tab-front" id="order_tab_1">下单金额</span>
        <span class="tab-back" id="order_tab_2">下单商品数</span>
        <span class="tab-back" id="order_tab_3">下单量</span>
    </p>
  </div>
  <div id="tabbody-div">
    <div class="list-div" id="order_list_1">
        <div class="order_count order_count_spe">
            <p><span class="tab_front">行业下单金额统计</span></p>
            <div style='height:400px;width:90%;margin-left:auto;margin-right:auto;' id='orders_option_div_1'></div>
        </div>
    </div>
    <div class="list-div" id="order_list_2">
        <div class="order_count order_count_spe">
            <p><span class="tab_front">行业下单商品数统计</span></p>
            <div style='height:400px;width:90%;margin-left:auto;margin-right:auto;' id='orders_option_div_2'></div>
        </div>
    </div>
    <div class="list-div" id="order_list_3">
        <div class="order_count order_count_spe">
            <p><span class="tab_front">行业下单量统计</span></p>
            <div style='height:400px;width:90%;margin-left:auto;margin-right:auto;' id='orders_option_div_3'></div>
        </div>
    </div>
  </div>
</div>
<script type="text/javascript" language="JavaScript">
    var date = new Date();
    var y = date.getFullYear();
    var m = date.getMonth() + 1;

    for (i = 0; i < 10; i++) {
        var oP = document.createElement("option");
        var oText = document.createTextNode(y);
        oP.appendChild(oText);
        oP.setAttribute("value", y);
        if(y == '2016')
        {
            oP.setAttribute("selected", "selected")
        }
        document.getElementById('year').appendChild(oP);
        y = y - 1;
    };
    var j = 1;
    for (i = 1; i < 13; i++) {
        var month = document.createElement("option");
        var monthText = document.createTextNode(j);
        month.appendChild(monthText);
        month.setAttribute("value", j);
        if (j == 09)
        {
            month.setAttribute("selected", "selected");
        }
        document.getElementById('month').appendChild(month);
        j = j + 1;
    };

    var week_num = 0;
    //绑定周
    function week()
    {
        if($("#stats_type").val()==0)
        {
            $("#date").show();
            $("#year").hide();
            $("#month").hide();
            $("#dropweek").hide();
            return;
        }
        else if($("#stats_type").val()==2)
        {
            $("#date").hide();
            $("#year").show();
            $("#month").show();
            $("#dropweek").hide();
            return;
        }
        $("#date").hide();
        $("#year").show();
        $("#month").show();
        $("#dropweek").show();
        var text = $("#year").val() + '-' + $("#month").val();
        var ymd = text.substring(0, 4) + "-" + text.substring(5, 7) + "-1";
        var week = new Date(Date.parse(ymd.replace(/\-/g, "/")));
        var w = week.toString().substring(0, 3);
        var yymm = new Date(text.substring(0, 4), text.substring(5, 7), 0);
        var day = yymm.getDate();
        var dd = 1;
        switch (w) {
            case "Mon": dd = 0; break;
            case "Tue": dd = 1; break;
            case "Wed": dd = 2; break;
            case "Thu": dd = 3; break;
            case "Fri": dd = 4; break;
            case "Sat": dd = 5; break;
            case "Sun": dd = 6; break;
        }
        var aw = 5;
        if (day == 28 && dd == 0) {
            aw = 4;
        }
        var i = 1;
        $("#dropweek").empty();
        for (var i = 0; i < aw; i++) {
            var start = i * 7 + 1 - dd;
            var end = i * 7 + 7 - dd;
            if (start < 1) {
                start = 1;
            }
            if (end > day) {
                end = day;
            }
            var str = ("第" + (i + 1) + "周 <" + text.substring(5, 7) + "月" + start + "号—" + text.substring(5, 7) + "月" + end + "号>").toString();
            var val = $("#year").val() + '-' + text.substring(5, 7) + '-' + start + ' ' + $("#year").val() + '-' + text.substring(5, 7) + '-' + end + ' ' + i;
            $("#dropweek").append("<option value='" + val + "'>" + str + "</option>");
        }
        $('#dropweek')[0].selectedIndex = week_num;

        var itme = $("#dropweek").find("option:selected").text();
        $("#txtweek:text").val(itme.toString());
    }
    // 执行
    week();

    $(document).ready(function(){
        $("#order_list_2").hide();
        $("#order_list_3").hide();
        $("#order_tab_1").hover(function(){
            $("#order_list_1").show();
            $("#order_list_2").hide();
            $("#order_list_3").hide();
            $("#order_tab_1").addClass('tab-front');
            $("#order_tab_1").removeClass('tab-back');
            $("#order_tab_2").addClass('tab-back');
            $("#order_tab_2").removeClass('tab-front');
            $("#order_tab_3").addClass('tab-back');
            $("#order_tab_3").removeClass('tab-front');
        });
        $("#order_tab_2").hover(function(){
            $("#order_list_1").hide();
            $("#order_list_2").show();
            $("#order_list_3").hide();
            $("#order_tab_1").addClass('tab-back');
            $("#order_tab_1").removeClass('tab-front');
            $("#order_tab_2").addClass('tab-front');
            $("#order_tab_2").removeClass('tab-back');
            $("#order_tab_3").addClass('tab-back');
            $("#order_tab_3").removeClass('tab-front');
        });
        $("#order_tab_3").hover(function(){
            $("#order_list_1").hide();
            $("#order_list_2").hide();
            $("#order_list_3").show();
            $("#order_tab_1").addClass('tab-back');
            $("#order_tab_1").removeClass('tab-front');
            $("#order_tab_2").addClass('tab-back');
            $("#order_tab_2").removeClass('tab-front');
            $("#order_tab_3").addClass('tab-front');
            $("#order_tab_3").removeClass('tab-back');
        });
    });
    $().ready(function(){
        // $("#cat_name")为获取分类名称的jQuery对象，可根据实际情况修改
        // $("#cat_id")为获取分类ID的jQuery对象，可根据实际情况修改
        // ""为被选中的商品分类编号，无则设置为null或者不写此参数或者为空字符串
        $.ajaxCategorySelecter($("#cat_name"), $("#cat_id"), "");
    });
</script>
<script src='js/echarts-all.js'></script>
<script>
    var option1 = {
        title : {
        },
        tooltip : {
            trigger: 'axis'
        },
        legend: {
            data:[]
        },
        toolbox: {
            show : true,
            feature : {
                mark : {show: false},
                dataView : {show: false, readOnly: false},
                magicType: {show: true, type: ['bar']},
                restore : {show: true},
                saveAsImage : {show: true}
            }
        },
        calculable : true,
        xAxis : [
            {
                type : 'value',
                boundaryGap : [0, 0.01]
            }
        ],
        yAxis : [
            {
                type : 'category',
                data : ['食品、酒类、生鲜、特产','男装、女装、内衣、珠宝','个护化妆、清洁用品','手机、数码、通信','家用电器','家居、家具、家装、厨具','酒类饮料','母婴、玩具乐器','电脑、办公','鞋靴、箱包、钟表、奢侈品','运动户外','汽车、汽车用品','营养保健','图书、音像、电子书','彩票、旅行、充值、票务','理财、众筹、白条、保险',]
            }
        ],
        series : [
            {
                type:'bar',
                data:['0.00','0.00','0.00','0.00','0.00','0.00','0.00','0.00','0.00','0.00','0.00','0.00','0.00','0.00','0.00','0.00',]
            }
        ]
    };
    var option2 = {
        title : {
        },
        tooltip : {
            trigger: 'axis'
        },
        legend: {
            data:[]
        },
        toolbox: {
            show : true,
            feature : {
                mark : {show: false},
                dataView : {show: false, readOnly: false},
                magicType: {show: true, type: ['bar']},
                restore : {show: true},
                saveAsImage : {show: true}
            }
        },
        calculable : true,
        xAxis : [
            {
                type : 'value',
                boundaryGap : [0, 0.01]
            }
        ],
        yAxis : [
            {
                type : 'category',
                data : ['食品、酒类、生鲜、特产','男装、女装、内衣、珠宝','个护化妆、清洁用品','手机、数码、通信','家用电器','家居、家具、家装、厨具','酒类饮料','母婴、玩具乐器','电脑、办公','鞋靴、箱包、钟表、奢侈品','运动户外','汽车、汽车用品','营养保健','图书、音像、电子书','彩票、旅行、充值、票务','理财、众筹、白条、保险',]
            }
        ],
        series : [
            {
                type:'bar',
                data:['','','','','','','','','','','','','','','','',]
            }
        ]
    };
    var option3 = {
        title : {
        },
        tooltip : {
            trigger: 'axis'
        },
        legend: {
            data:[]
        },
        toolbox: {
            show : true,
            feature : {
                mark : {show: false},
                dataView : {show: false, readOnly: false},
                magicType: {show: true, type: ['bar']},
                restore : {show: true},
                saveAsImage : {show: true}
            }
        },
        calculable : true,
        xAxis : [
            {
                type : 'value',
                boundaryGap : [0, 0.01]
            }
        ],
        yAxis : [
            {
                type : 'category',
                data : ['食品、酒类、生鲜、特产','男装、女装、内衣、珠宝','个护化妆、清洁用品','手机、数码、通信','家用电器','家居、家具、家装、厨具','酒类饮料','母婴、玩具乐器','电脑、办公','鞋靴、箱包、钟表、奢侈品','运动户外','汽车、汽车用品','营养保健','图书、音像、电子书','彩票、旅行、充值、票务','理财、众筹、白条、保险',]
            }
        ],
        series : [
            {
                type:'bar',
                data:['0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0',]
            }
        ]
    };
    var order_chart1 = echarts.init(document.getElementById('orders_option_div_1'));
    order_chart1.setOption(option1);
    var order_chart2 = echarts.init(document.getElementById('orders_option_div_2'));
    order_chart2.setOption(option2);
    var order_chart3 = echarts.init(document.getElementById('orders_option_div_3'));
    order_chart3.setOption(option3);
</script>
<div id="footer">
共执行 50 个查询，用时 0.185010 秒，Gzip 已禁用，内存占用 4.003 MB<br />
版权所有 &copy; 2008-2030 广州网软志成信息科技有限公司，并保留所有权利。</div>
<script type="text/javascript" src="../js/utils.js"></script><!-- 新订单提示信息 -->
<div id="popMsg">
  <table cellspacing="0" cellpadding="0" width="100%" bgcolor="#cfdef4" border="0">
  <tr>
    <td style="color: #0f2c8c" width="30" height="24"></td>
    <td style="font-weight: normal; color: #1f336b; padding-top: 4px;padding-left: 4px" valign="center" width="100%"> 新订单通知</td>
    <td style="padding-top: 2px;padding-right:2px" valign="center" align="right" width="19"><span title="关闭" style="cursor: hand;cursor:pointer;color:red;font-size:12px;font-weight:bold;margin-right:4px;" onclick="Message.close()" >×</span><!-- <img title=关闭 style="cursor: hand" onclick=closediv() hspace=3 src="msgclose.jpg"> --></td>
  </tr>
  <tr>
    <td style="padding-right: 1px; padding-bottom: 1px" colspan="3" height="70">
    <div id="popMsgContent">
      <p>您有 <strong style="color:#ff0000" id="spanNewOrder">1</strong> 个新订单以及       <strong style="color:#ff0000" id="spanNewPaid">0</strong> 个新付款的订单</p>
      <p align="center" style="word-break:break-all"><a href="order.php?act=list"><span style="color:#ff0000">点击查看新订单</span></a></p>
    </div>
    </td>
  </tr>
  </table>
</div>

<!--
<embed src="images/online.wav" width="0" height="0" autostart="false" name="msgBeep" id="msgBeep" enablejavascript="true"/>
-->
<object classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000" codebase="http://active.macromedia.com/flash2/cabs/swflash.cab#version=4,0,0,0" id="msgBeep" width="1" height="1">
  <param name="movie" value="images/online.swf">
  <param name="quality" value="high">
  <embed src="images/online.swf" name="msgBeep" id="msgBeep" quality="high" width="0" height="0" type="application/x-shockwave-flash" pluginspage="http://www.macromedia.com/shockwave/download/index.cgi?p1_prod_version=shockwaveflash">
  </embed>
</object>

<script language="JavaScript">
document.onmousemove=function(e)
{
  var obj = Utils.srcElement(e);
  if (typeof(obj.onclick) == 'function' && obj.onclick.toString().indexOf('listTable.edit') != -1)
  {
    obj.title = '点击修改内容';
    obj.style.cssText = 'background: #278296;';
    obj.onmouseout = function(e)
    {
      this.style.cssText = '';
    }
  }
  else if (typeof(obj.href) != 'undefined' && obj.href.indexOf('listTable.sort') != -1)
  {
    obj.title = '点击对列表排序';
  }
}
<!--


var MyTodolist;
function showTodoList(adminid)
{
  if(!MyTodolist)
  {
    var global = $import("../js/global.js","js");
    global.onload = global.onreadystatechange= function()
    {
      if(this.readyState && this.readyState=="loading")return;
      var md5 = $import("js/md5.js","js");
      md5.onload = md5.onreadystatechange= function()
      {
        if(this.readyState && this.readyState=="loading")return;
        var todolist = $import("js/todolist.js","js");
        todolist.onload = todolist.onreadystatechange = function()
        {
          if(this.readyState && this.readyState=="loading")return;
          MyTodolist = new Todolist();
          MyTodolist.show();
        }
      }
    }
  }
  else
  {
    if(MyTodolist.visibility)
    {
      MyTodolist.hide();
    }
    else
    {
      MyTodolist.show();
    }
  }
}

if (Browser.isIE)
{
  onscroll = function()
  {
    //document.getElementById('calculator').style.top = document.body.scrollTop;
    document.getElementById('popMsg').style.top = (document.body.scrollTop + document.body.clientHeight - document.getElementById('popMsg').offsetHeight) + "px";
  }
}

if (document.getElementById("listDiv"))
{
  document.getElementById("listDiv").onmouseover = function(e)
  {
    obj = Utils.srcElement(e);

    if (obj)
    {
      if (obj.parentNode.tagName.toLowerCase() == "tr") row = obj.parentNode;
      else if (obj.parentNode.parentNode.tagName.toLowerCase() == "tr") row = obj.parentNode.parentNode;
      else return;

      for (i = 0; i < row.cells.length; i++)
      {
        if (row.cells[i].tagName != "TH") row.cells[i].style.backgroundColor = '#F4FAFB';
      }
    }

  }

  document.getElementById("listDiv").onmouseout = function(e)
  {
    obj = Utils.srcElement(e);

    if (obj)
    {
      if (obj.parentNode.tagName.toLowerCase() == "tr") row = obj.parentNode;
      else if (obj.parentNode.parentNode.tagName.toLowerCase() == "tr") row = obj.parentNode.parentNode;
      else return;

      for (i = 0; i < row.cells.length; i++)
      {
          if (row.cells[i].tagName != "TH") row.cells[i].style.backgroundColor = '';
      }
    }
  }

  document.getElementById("listDiv").onclick = function(e)
  {
    var obj = Utils.srcElement(e);

    if (obj.tagName == "INPUT" && obj.type == "checkbox")
    {
      if (!document.forms['listForm'])
      {
        return;
      }
      var nodes = document.forms['listForm'].elements;
      var checked = false;

      for (i = 0; i < nodes.length; i++)
      {
        if (nodes[i].checked)
        {
           checked = true;
           break;
         }
      }

      if(document.getElementById("btnSubmit"))
      {
        document.getElementById("btnSubmit").disabled = !checked;
      }
      for (i = 1; i <= 10; i++)
      {
        if (document.getElementById("btnSubmit" + i))
        {
          document.getElementById("btnSubmit" + i).disabled = !checked;
        }
      }
    }
  }

}

//-->
</script><?php
include 'buy.php' ;
?>
</body>
</html>