<?php
/** * SHOP 快钱联合注册接口
 * ============================================================================
 * 版权所有 2005-2030 广州网软志成信息科技有限公司，并保留所有权利。
 * 网站地址: http://www.wrzc.net；
 * ============================================================================
 * $Author: liuhui $
 * $Id: send.php 15013 2008-10-23 09:31:42Z liuhui $
*/

define('IN_wrzc', true);

require(dirname(__FILE__) . '/includes/init.php');
$backUrl=$wrzc->url() . ADMIN_PATH . '/receive.php';
header("location:http://cloud.wrzchop.com/payment_apply.php?mod=kuaiqian&par=$backUrl");
exit;
?>
