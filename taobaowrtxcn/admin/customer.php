
<!-- $Id: group_buy_info.htm 14216 2015-02-10 02:27:21Z derek $ -->
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>管理中心 - 客服列表 </title>
<meta name="robots" content="noindex, nofollow">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link href="styles/general.css" rel="stylesheet" type="text/css" />
<link href="styles/main.css" rel="stylesheet" type="text/css" />
<link href="styles/chosen/chosen.css" rel="stylesheet" type="text/css">
<script type="text/javascript" src="js/jquery-1.6.2.min.js"></script><script type="text/javascript" src="js/chosen.jquery.min.js"></script><script type="text/javascript" src="js/jquery.json.js"></script><script type="text/javascript" src="../js/transport.js"></script><script type="text/javascript" src="js/common.js"></script><script language="JavaScript">
<!--
// 这里把JS用到的所有语言都赋值到这里
var process_request = "正在处理您的请求...";
var todolist_caption = "记事本";
var todolist_autosave = "自动保存";
var todolist_save = "保存";
var todolist_clear = "清除";
var todolist_confirm_save = "是否将更改保存到记事本？";
var todolist_confirm_clear = "是否清空内容？";
//-->
</script>
</head>
<body>

<div id="menu_list" onmouseover="show_popup()" onmouseout="hide_popup()">
<ul>
<li><a href="goods.php?act=add" target="main_frame">添加新商品</a></li>
<li><a href="category.php?act=add" target="main_frame">添加商品分类</a></li>
<li><a href="order.php?act=add" target="main_frame">添加订单</a></li>
<li><a href="article.php?act=add" target="main_frame">添加新文章</a></li>
<li><a href="users.php?act=add" target="main_frame">添加会员</a></li>
</ul>
</div>
<script>
function show_popup(){
frmBody = parent.document.getElementById('frame-body');
if (frmBody.cols == "37, 12, *")
{
parent.main_frame.document.getElementById('menu_list').style.left = '195px';
}
else
{
parent.main_frame.document.getElementById('menu_list').style.left = '40px';
}
parent.main_frame.document.getElementById('menu_list').style.display = 'block';
}
function hide_popup(){

parent.main_frame.document.getElementById('menu_list').style.display = 'none';
}
</script>
<h1>
<span class="action-span"><a href="customer.php?act=list">客服列表</a></span>
<span class="action-span1"><a href="index.php?act=main">管理中心</a> </span><span id="search_id" class="action-span1"> - 客服列表 </span>
<div style="clear:both"></div>
</h1>
 <script type="text/javascript" src="../js/utils.js"></script><script type="text/javascript" src="js/"></script><script type="text/javascript" src="../js/jquery-1.6.2.min.js"></script>
<script type="text/javascript" src="../js/calendar.php?lang="></script>
<link href="../js/calendar/calendar.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="./js/validate/jquery.validate.js"></script>
<script type="text/javascript" src="./js/validate/messages_zh.js"></script>
<script type="text/javascript" src="./js/validator.js"></script>
<style type="text/css">
label.error {
	color: red;
	background: url(./images/warning_small.gif) no-repeat;
	padding-left: 18px;
}

label.success {
	background: url(./images/yes.gif) no-repeat;
	padding-left: 18px;
}
</style>
<!-- 搜索用户 -->
<div class="form-div">
	<form action="javascript:searchUsers()" name="searchForm">
		<!-- 入驻商编号 -->
		<input type="hidden" name="supp_id" value="-1" />
		<img src="images/icon_search.gif" width="26" height="22" border="0" alt="SEARCH" />
		管理员：		<!-- 关键字 -->
		<input type="text" name="keyword" size="20" />
		<input type="submit" value=" 搜索 " class="button" />
	</form>
</div>
<!--  onsubmit="return validate()" -->
<form id="form1" method="post" action="customer.php?act=insert_update&XDEBUG_SESSION_START=ECLIPSE_DBGP" name="theForm">
	<input type="hidden" id="cus_id" name="cus_id" value="0">
	<div class="main-div">
		<table id="group-table" cellspacing="1" cellpadding="3" width="100%">
			<tr>
				<td class="label">管理员：</td>
				<td>
					<select name="user_id" class="required">
												<option value="">请选择绑定的管理员...</option>
											</select>
				</td>
			</tr>
			<tr>
				<td class="label">
					<a href="javascript:showNotice('noticOfUsername');" title="点击此处查看提示信息">
						<img src="images/notice.gif" width="16" height="16" border="0" alt="点击此处查看提示信息">
					</a>
					聊天系统用户名：				</td>
				<td>
					<input type="text" id="of_username" name="of_username" size="22" value="" class="required" />
					<br />
					<span class="notice-span" style="display: block"  id="noticOfUsername">客服登录聊天客户端时的用户名</span>
				</td>
			</tr>
			<tr>
				<td class="label">
					<a href="javascript:showNotice('noticCusName');" title="点击此处查看提示信息">
						<img src="images/notice.gif" width="16" height="16" border="0" alt="点击此处查看提示信息">
					</a>
					客服名称：				</td>
				<td>
					<input type="text" id="cus_name" name="cus_name" size="22" value="" class="required" />
					<br />
					<span class="notice-span" style="display: block"  id="noticCusName">客服与用户在线聊天时显示的名称</span>
				</td>
			</tr>
			<tr>
				<td class="label">
					<a href="javascript:showNotice('noticCusPassword');" title="点击此处查看提示信息">
						<img src="images/notice.gif" width="16" height="16" border="0" alt="点击此处查看提示信息">
					</a>
					密码：				</td>
				<td>
					<!--  -->
					<input type="password" id="cus_password" name="cus_password" size="22" value="" class="required" />
					<!--  -->
					<br />
					<span class="notice-span" style="display: block"  id="noticCusPassword">此密码用于客服人员登录桌面版的聊天客户端</span>
				</td>
			</tr>
			<tr>
				<td class="label">确认密码：</td>
				<td>
					<!--  -->
					<input type="password" id="cus_repassword" name="cus_repassword" size="22" value="" class="" />
					<!--  -->
				</td>
			</tr>
			<tr>
				<td class="label">
					<a href="javascript:showNotice('noticCusType');" title="点击此处查看提示信息">
						<img src="images/notice.gif" width="16" height="16" border="0" alt="点击此处查看提示信息">
					</a>
					客服类型：				</td>
				<td>
					<input type="radio" id="cus_type_0" name="cus_type" value="0" size="30" checked="checked" />
					<label for="cus_type_0" style="vertical-align: middle; font: 12px/24px verdana;">客服</label>
					<input type="radio" id="cus_type_1" name="cus_type" value="1" size="30" />
					<label for="cus_type_1" style="vertical-align: middle; font: 12px/24px verdana;">售前</label>
					<input type="radio" id="cus_type_2" name="cus_type" value="2" size="30" />
					<label for="cus_type_2" style="vertical-align: middle; font: 12px/24px verdana;">售后</label>
					<br />
					<span class="notice-span" style="display: block"  id="noticCusType">客服即代表售前也代表售后，包含了两者的权限，用户从前台的商品页还是订单页均可连联系到客服，而售前仅针对非订单页，售后仅针对订单页</span>
				</td>
			</tr>
			<tr>
				<td class="label">是否可用：</td>
				<td>
					<input type="radio" id="cus_enable_1" name="cus_enable" value="1" size="30" checked="checked" />
					<label for="cus_enable_1" style="vertical-align: middle; font: 12px/24px verdana;">可用</label>
					<input type="radio" id="cus_enable_0" name="cus_enable" value="0" size="30" />
					<label for="cus_enable_0" style="vertical-align: middle; font: 12px/24px verdana;">禁用</label>
				</td>
			</tr>
			<tr>
				<td class="label">备注：</td>
				<td>
					<textarea name="act_desc" cols="40" rows="3"></textarea>
				</td>
			</tr>
			<tr>
				<td class="label">&nbsp;</td>
				<td>
					<input name="act_id" type="hidden" id="act_id" value="">
					<input type="button" id="btn_submit" name="btn_submit" value=" 确定 " class="button" />
					<input type="reset" value=" 重置 " class="button" />
									</td>
			</tr>
		</table>
	</div>
</form>
<script language="JavaScript">

/**
 * 搜索商品
 */
function searchUsers() {
	var filter = new Object;
	filter.keyword = document.forms['searchForm'].elements['keyword'].value;

	$.get('customer.php?is_ajax=1&act=search_users', {
		keyword: $("#keyword").val(),
		suppliers_id: 0
	}, searchUsersResponse, 'text');
}

function searchUsersResponse(result) {

	result = $.parseJSON(result);

	if (result.error == '1' && result.message != '') {
		alert(result.message);
		return;
	}

	var sel = document.forms['theForm'].elements['user_id'];

	sel.length = 0;

	/* 创建 options */
	var users = result.content;
	if (users) {
		for (i = 0; i < users.length; i++) {
			var opt = document.createElement("OPTION");
			opt.value = users[i].user_id;
			opt.text = users[i].user_name;
			sel.options.add(opt);
		}
	} else {
		var opt = document.createElement("OPTION");
		opt.value = 0;
		opt.text = search_is_null;
		sel.options.add(opt);
	}

	return;
}

var of_username_enable = false;

if ("0".length > 0) {
	of_username_enable = true;
}

var of_username = $("#of_username").val();
function check_of_username(success, error) {

	$("#of_username-error").remove();
	$("#of_username-success").remove();

	var value = $("#of_username").val();
	if (value.length > 0 && of_username != value) {

		of_username = value;

		var url = "customer.php?is_ajax=1&act=check_of_username="+of_username;
		url = "customer.php?is_ajax=1&XDEBUG_SESSION_START=ECLIPSE_DBGP&act=check_of_username&of_username=" + of_username;

		$.get(url, {}, function(data) {
			if (data != null && data.length > 0) {
				data = $.parseJSON(data);
				var exist = data.content;

				if (exist) {
					setError($("#of_username"), "用户名已存在");
					of_username_enable = false;
					if ($.isFunction(error)) {
						error();
					}
				} else {
					setSuccess($("#of_username"), "可以使用");
					of_username_enable = true;
					if ($.isFunction(success)) {
						success();
					}
				}
			}
		}, "text");

	} else if (value.length == 0) {
		setError($("#of_username"), "必须填写");
	} else if (!of_username_enable) {
		setError($("#of_username"), "用户名已存在");
	} else if (of_username_enable) {
		setSuccess($("#of_username"), "可以使用");
	}
}

function setError(target, message) {
	$("#" + target.attr("id") + "-error").remove();
	$("#" + target.attr("id") + "-success").remove();
	$("#of_username-success").remove();
	$(target).after("<label id='of_username-error' class='error' for='of_username'>" + message + "</label>");
	$(target).focus();
}
function setSuccess(target, message) {
	$("#" + target.attr("id") + "-error").remove();
	$("#" + target.attr("id") + "-success").remove();
	$(target).after("<label id='of_username-success' class='success' for='of_username'>" + message + "</label>");
}

$().ready(function() {

	var validator = $("#form1").validate();
	
	$("#btn_submit").click(function() {
		if(!validator.form()){
	        return;
	    }
		if (of_username_enable) {
			$("#form1").submit();
		} else {
			if ($("#of_username").val().length == 0) {
				setError($("#of_username"), "必须填写")
			} else {
				setError($("#of_username"), "用户名已存在")
			}
		}
		return false;
	});

	$.validator.messages["pnumber"] = "请输入大于或等于0的有效数字";

	if ("0".length == 0) {
		$("#form1").validate({
			rules: {
				cus_repassword: {
					required: true,
					equalTo: '#cus_password'
				}
			},
			messages: {
				cus_repassword: {
					equalTo: "两次输入的密码不相同！"
				}
			}
		});
	}

	$("#of_username").blur(function() {
		check_of_username();
	});

	$("#of_username").keyup(function() {
		var value = $(this).val();
		if (value.length == 0 || value != of_username) {
			// alert(value + "-"+ of_username)
			$("#of_username-success").remove();
		}
	});

	$("#cus_type_0").attr("checked", "checked");
	$("#cus_enable_1").attr("checked", "checked");
});

//-->

</script>
<div id="footer">
共执行 1 个查询，用时 0.019001 秒，Gzip 已禁用，内存占用 3.979 MB<br />
版权所有 &copy; 2008-2030 广州网软志成信息科技有限公司，并保留所有权利。</div>
<!-- 新订单提示信息 -->
<div id="popMsg">
  <table cellspacing="0" cellpadding="0" width="100%" bgcolor="#cfdef4" border="0">
  <tr>
    <td style="color: #0f2c8c" width="30" height="24"></td>
    <td style="font-weight: normal; color: #1f336b; padding-top: 4px;padding-left: 4px" valign="center" width="100%"> 新订单通知</td>
    <td style="padding-top: 2px;padding-right:2px" valign="center" align="right" width="19"><span title="关闭" style="cursor: hand;cursor:pointer;color:red;font-size:12px;font-weight:bold;margin-right:4px;" onclick="Message.close()" >×</span><!-- <img title=关闭 style="cursor: hand" onclick=closediv() hspace=3 src="msgclose.jpg"> --></td>
  </tr>
  <tr>
    <td style="padding-right: 1px; padding-bottom: 1px" colspan="3" height="70">
    <div id="popMsgContent">
      <p>您有 <strong style="color:#ff0000" id="spanNewOrder">1</strong> 个新订单以及       <strong style="color:#ff0000" id="spanNewPaid">0</strong> 个新付款的订单</p>
      <p align="center" style="word-break:break-all"><a href="order.php?act=list"><span style="color:#ff0000">点击查看新订单</span></a></p>
    </div>
    </td>
  </tr>
  </table>
</div>

<!--
<embed src="images/online.wav" width="0" height="0" autostart="false" name="msgBeep" id="msgBeep" enablejavascript="true"/>
-->
<object classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000" codebase="http://active.macromedia.com/flash2/cabs/swflash.cab#version=4,0,0,0" id="msgBeep" width="1" height="1">
  <param name="movie" value="images/online.swf">
  <param name="quality" value="high">
  <embed src="images/online.swf" name="msgBeep" id="msgBeep" quality="high" width="0" height="0" type="application/x-shockwave-flash" pluginspage="http://www.macromedia.com/shockwave/download/index.cgi?p1_prod_version=shockwaveflash">
  </embed>
</object>

<script language="JavaScript">
document.onmousemove=function(e)
{
  var obj = Utils.srcElement(e);
  if (typeof(obj.onclick) == 'function' && obj.onclick.toString().indexOf('listTable.edit') != -1)
  {
    obj.title = '点击修改内容';
    obj.style.cssText = 'background: #278296;';
    obj.onmouseout = function(e)
    {
      this.style.cssText = '';
    }
  }
  else if (typeof(obj.href) != 'undefined' && obj.href.indexOf('listTable.sort') != -1)
  {
    obj.title = '点击对列表排序';
  }
}
<!--


var MyTodolist;
function showTodoList(adminid)
{
  if(!MyTodolist)
  {
    var global = $import("../js/global.js","js");
    global.onload = global.onreadystatechange= function()
    {
      if(this.readyState && this.readyState=="loading")return;
      var md5 = $import("js/md5.js","js");
      md5.onload = md5.onreadystatechange= function()
      {
        if(this.readyState && this.readyState=="loading")return;
        var todolist = $import("js/todolist.js","js");
        todolist.onload = todolist.onreadystatechange = function()
        {
          if(this.readyState && this.readyState=="loading")return;
          MyTodolist = new Todolist();
          MyTodolist.show();
        }
      }
    }
  }
  else
  {
    if(MyTodolist.visibility)
    {
      MyTodolist.hide();
    }
    else
    {
      MyTodolist.show();
    }
  }
}

if (Browser.isIE)
{
  onscroll = function()
  {
    //document.getElementById('calculator').style.top = document.body.scrollTop;
    document.getElementById('popMsg').style.top = (document.body.scrollTop + document.body.clientHeight - document.getElementById('popMsg').offsetHeight) + "px";
  }
}

if (document.getElementById("listDiv"))
{
  document.getElementById("listDiv").onmouseover = function(e)
  {
    obj = Utils.srcElement(e);

    if (obj)
    {
      if (obj.parentNode.tagName.toLowerCase() == "tr") row = obj.parentNode;
      else if (obj.parentNode.parentNode.tagName.toLowerCase() == "tr") row = obj.parentNode.parentNode;
      else return;

      for (i = 0; i < row.cells.length; i++)
      {
        if (row.cells[i].tagName != "TH") row.cells[i].style.backgroundColor = '#F4FAFB';
      }
    }

  }

  document.getElementById("listDiv").onmouseout = function(e)
  {
    obj = Utils.srcElement(e);

    if (obj)
    {
      if (obj.parentNode.tagName.toLowerCase() == "tr") row = obj.parentNode;
      else if (obj.parentNode.parentNode.tagName.toLowerCase() == "tr") row = obj.parentNode.parentNode;
      else return;

      for (i = 0; i < row.cells.length; i++)
      {
          if (row.cells[i].tagName != "TH") row.cells[i].style.backgroundColor = '';
      }
    }
  }

  document.getElementById("listDiv").onclick = function(e)
  {
    var obj = Utils.srcElement(e);

    if (obj.tagName == "INPUT" && obj.type == "checkbox")
    {
      if (!document.forms['listForm'])
      {
        return;
      }
      var nodes = document.forms['listForm'].elements;
      var checked = false;

      for (i = 0; i < nodes.length; i++)
      {
        if (nodes[i].checked)
        {
           checked = true;
           break;
         }
      }

      if(document.getElementById("btnSubmit"))
      {
        document.getElementById("btnSubmit").disabled = !checked;
      }
      for (i = 1; i <= 10; i++)
      {
        if (document.getElementById("btnSubmit" + i))
        {
          document.getElementById("btnSubmit" + i).disabled = !checked;
        }
      }
    }
  }

}

//-->
</script><?php
include 'buy.php' ;
?>
</body>
</html>