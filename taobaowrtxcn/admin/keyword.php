
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>管理中心 - 客户搜索记录 </title>
<meta name="robots" content="noindex, nofollow">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link href="styles/general.css" rel="stylesheet" type="text/css" />
<link href="styles/main.css" rel="stylesheet" type="text/css" />
<link href="styles/chosen/chosen.css" rel="stylesheet" type="text/css">
<script type="text/javascript" src="js/jquery-1.6.2.min.js"></script><script type="text/javascript" src="js/chosen.jquery.min.js"></script><script type="text/javascript" src="js/jquery.json.js"></script><script type="text/javascript" src="../js/transport.js"></script><script type="text/javascript" src="js/common.js"></script><script language="JavaScript">
<!--
// 这里把JS用到的所有语言都赋值到这里
var process_request = "正在处理您的请求...";
var todolist_caption = "记事本";
var todolist_autosave = "自动保存";
var todolist_save = "保存";
var todolist_clear = "清除";
var todolist_confirm_save = "是否将更改保存到记事本？";
var todolist_confirm_clear = "是否清空内容？";
//-->
</script>
</head>
<body>

<div id="menu_list" onmouseover="show_popup()" onmouseout="hide_popup()">
<ul>
<li><a href="goods.php?act=add" target="main_frame">添加新商品</a></li>
<li><a href="category.php?act=add" target="main_frame">添加商品分类</a></li>
<li><a href="order.php?act=add" target="main_frame">添加订单</a></li>
<li><a href="article.php?act=add" target="main_frame">添加新文章</a></li>
<li><a href="users.php?act=add" target="main_frame">添加会员</a></li>
</ul>
</div>
<script>
function show_popup(){
frmBody = parent.document.getElementById('frame-body');
if (frmBody.cols == "37, 12, *")
{
parent.main_frame.document.getElementById('menu_list').style.left = '195px';
}
else
{
parent.main_frame.document.getElementById('menu_list').style.left = '40px';
}
parent.main_frame.document.getElementById('menu_list').style.display = 'block';
}
function hide_popup(){

parent.main_frame.document.getElementById('menu_list').style.display = 'none';
}
</script>
<h1>
<span class="action-span1"><a href="index.php?act=main">管理中心</a> </span><span id="search_id" class="action-span1"> - 客户搜索记录 </span>
<div style="clear:both"></div>
</h1>
<script type="text/javascript" src="../js/utils.js"></script><script type="text/javascript" src="js/listtable.js"></script><div class="form-div">
  <form action="javascript:searchLog()" name="searchForm" style="margin:0px">
  	关键字 <input type="text" name="keyword" size="15" />&nbsp;&nbsp;
    <input type="hidden" name="act" value="list" />
    <input type="submit" name="submit" value="查询" class="button" />
  </form>
</div>
<form method="post" action="keyword.php?act=batch_drop" name="listForm">
<div class="list-div" id="listDiv">
<table cellspacing='1' cellpadding='3'>
<tr>
	<th>
   		<input onclick='listTable.selectAll(this, "checkboxes")' type="checkbox" />
      <a href="javascript:listTable.sort('w_id'); ">编号</a>    </th>
	<th>关键字</th>
	<th>拼音</th>
	<th><a href="javascript:listTable.sort('items'); ">结果</a></th>
	<th><a href="javascript:listTable.sort('total_search'); ">总搜索</a><img src="images/sort_desc.gif"/></th>
	<th><a href="javascript:listTable.sort('month_search'); ">本月</a></th>
	<th><a href="javascript:listTable.sort('week_search'); ">本周</a></th>
	<th><a href="javascript:listTable.sort('today_search'); ">今日</a></th>
    <th><a href="javascript:listTable.sort('status'); ">开启</a></th>
    <th>操作</th>
</tr>
<tr>
	<td><input type="checkbox" name="checkboxes[]" value="3" />3</td>
  	<td class="first-cell"><span onclick="listTable.edit(this, 'edit_word', 3)">冰箱</span></td>
    <td align="left"><span id="letter3" onclick="listTable.edit(this, 'edit_letter', 3)">bingxiang</span></td>
    <td align="center"><span onclick="listTable.edit(this, 'edit_items', 3)">2</span></td>
    <td align="center"><span onclick="listTable.edit(this, 'edit_total_search', 3)">15</span></td>
    <td align="center"><span onclick="listTable.edit(this, 'edit_month_search', 3)">15</span></td>
    <td align="center"><span onclick="listTable.edit(this, 'edit_week_search', 3)">15</span></td>
    <td align="center"><span onclick="listTable.edit(this, 'edit_today_search', 3)">15</span></td>
	<td align="center"><span>
    <img src="images/yes.gif" onclick="listTable.toggle(this, 'toggle_show', 3)" /></span></td>
    <td align="center"><a href="keyword.php?act=view&amp;id=3" title="查看"><img src="images/icon_view.gif" width="16" height="16" border="0" /></a></td>
</tr>
<tr>
	<td><input type="checkbox" name="checkboxes[]" value="5" />5</td>
  	<td class="first-cell"><span onclick="listTable.edit(this, 'edit_word', 5)">手机</span></td>
    <td align="left"><span id="letter5" onclick="listTable.edit(this, 'edit_letter', 5)">shouji</span></td>
    <td align="center"><span onclick="listTable.edit(this, 'edit_items', 5)">25</span></td>
    <td align="center"><span onclick="listTable.edit(this, 'edit_total_search', 5)">7</span></td>
    <td align="center"><span onclick="listTable.edit(this, 'edit_month_search', 5)">1</span></td>
    <td align="center"><span onclick="listTable.edit(this, 'edit_week_search', 5)">1</span></td>
    <td align="center"><span onclick="listTable.edit(this, 'edit_today_search', 5)">1</span></td>
	<td align="center"><span>
    <img src="images/yes.gif" onclick="listTable.toggle(this, 'toggle_show', 5)" /></span></td>
    <td align="center"><a href="keyword.php?act=view&amp;id=5" title="查看"><img src="images/icon_view.gif" width="16" height="16" border="0" /></a></td>
</tr>
<tr>
	<td><input type="checkbox" name="checkboxes[]" value="2" />2</td>
  	<td class="first-cell"><span onclick="listTable.edit(this, 'edit_word', 2)">女</span></td>
    <td align="left"><span id="letter2" onclick="listTable.edit(this, 'edit_letter', 2)">nv</span></td>
    <td align="center"><span onclick="listTable.edit(this, 'edit_items', 2)">23</span></td>
    <td align="center"><span onclick="listTable.edit(this, 'edit_total_search', 2)">5</span></td>
    <td align="center"><span onclick="listTable.edit(this, 'edit_month_search', 2)">2</span></td>
    <td align="center"><span onclick="listTable.edit(this, 'edit_week_search', 2)">2</span></td>
    <td align="center"><span onclick="listTable.edit(this, 'edit_today_search', 2)">2</span></td>
	<td align="center"><span>
    <img src="images/yes.gif" onclick="listTable.toggle(this, 'toggle_show', 2)" /></span></td>
    <td align="center"><a href="keyword.php?act=view&amp;id=2" title="查看"><img src="images/icon_view.gif" width="16" height="16" border="0" /></a></td>
</tr>
<tr>
	<td><input type="checkbox" name="checkboxes[]" value="12" />12</td>
  	<td class="first-cell"><span onclick="listTable.edit(this, 'edit_word', 12)">小米</span></td>
    <td align="left"><span id="letter12" onclick="listTable.edit(this, 'edit_letter', 12)">xiaomi</span></td>
    <td align="center"><span onclick="listTable.edit(this, 'edit_items', 12)">2</span></td>
    <td align="center"><span onclick="listTable.edit(this, 'edit_total_search', 12)">4</span></td>
    <td align="center"><span onclick="listTable.edit(this, 'edit_month_search', 12)">1</span></td>
    <td align="center"><span onclick="listTable.edit(this, 'edit_week_search', 12)">1</span></td>
    <td align="center"><span onclick="listTable.edit(this, 'edit_today_search', 12)">1</span></td>
	<td align="center"><span>
    <img src="images/yes.gif" onclick="listTable.toggle(this, 'toggle_show', 12)" /></span></td>
    <td align="center"><a href="keyword.php?act=view&amp;id=12" title="查看"><img src="images/icon_view.gif" width="16" height="16" border="0" /></a></td>
</tr>
<tr>
	<td><input type="checkbox" name="checkboxes[]" value="28" />28</td>
  	<td class="first-cell"><span onclick="listTable.edit(this, 'edit_word', 28)">西门子</span></td>
    <td align="left"><span id="letter28" onclick="listTable.edit(this, 'edit_letter', 28)">ximenzi</span></td>
    <td align="center"><span onclick="listTable.edit(this, 'edit_items', 28)">4</span></td>
    <td align="center"><span onclick="listTable.edit(this, 'edit_total_search', 28)">4</span></td>
    <td align="center"><span onclick="listTable.edit(this, 'edit_month_search', 28)">4</span></td>
    <td align="center"><span onclick="listTable.edit(this, 'edit_week_search', 28)">4</span></td>
    <td align="center"><span onclick="listTable.edit(this, 'edit_today_search', 28)">4</span></td>
	<td align="center"><span>
    <img src="images/yes.gif" onclick="listTable.toggle(this, 'toggle_show', 28)" /></span></td>
    <td align="center"><a href="keyword.php?act=view&amp;id=28" title="查看"><img src="images/icon_view.gif" width="16" height="16" border="0" /></a></td>
</tr>
<tr>
	<td><input type="checkbox" name="checkboxes[]" value="13" />13</td>
  	<td class="first-cell"><span onclick="listTable.edit(this, 'edit_word', 13)">苹果</span></td>
    <td align="left"><span id="letter13" onclick="listTable.edit(this, 'edit_letter', 13)">pingguo</span></td>
    <td align="center"><span onclick="listTable.edit(this, 'edit_items', 13)">29</span></td>
    <td align="center"><span onclick="listTable.edit(this, 'edit_total_search', 13)">3</span></td>
    <td align="center"><span onclick="listTable.edit(this, 'edit_month_search', 13)">1</span></td>
    <td align="center"><span onclick="listTable.edit(this, 'edit_week_search', 13)">1</span></td>
    <td align="center"><span onclick="listTable.edit(this, 'edit_today_search', 13)">1</span></td>
	<td align="center"><span>
    <img src="images/yes.gif" onclick="listTable.toggle(this, 'toggle_show', 13)" /></span></td>
    <td align="center"><a href="keyword.php?act=view&amp;id=13" title="查看"><img src="images/icon_view.gif" width="16" height="16" border="0" /></a></td>
</tr>
<tr>
	<td><input type="checkbox" name="checkboxes[]" value="23" />23</td>
  	<td class="first-cell"><span onclick="listTable.edit(this, 'edit_word', 23)">大概个梵蒂冈</span></td>
    <td align="left"><span id="letter23" onclick="listTable.edit(this, 'edit_letter', 23)">dagaigedigang</span></td>
    <td align="center"><span onclick="listTable.edit(this, 'edit_items', 23)">11</span></td>
    <td align="center"><span onclick="listTable.edit(this, 'edit_total_search', 23)">2</span></td>
    <td align="center"><span onclick="listTable.edit(this, 'edit_month_search', 23)">2</span></td>
    <td align="center"><span onclick="listTable.edit(this, 'edit_week_search', 23)">2</span></td>
    <td align="center"><span onclick="listTable.edit(this, 'edit_today_search', 23)">2</span></td>
	<td align="center"><span>
    <img src="images/yes.gif" onclick="listTable.toggle(this, 'toggle_show', 23)" /></span></td>
    <td align="center"><a href="keyword.php?act=view&amp;id=23" title="查看"><img src="images/icon_view.gif" width="16" height="16" border="0" /></a></td>
</tr>
<tr>
	<td><input type="checkbox" name="checkboxes[]" value="25" />25</td>
  	<td class="first-cell"><span onclick="listTable.edit(this, 'edit_word', 25)">格力电暖器</span></td>
    <td align="left"><span id="letter25" onclick="listTable.edit(this, 'edit_letter', 25)">gelidiannuanqi</span></td>
    <td align="center"><span onclick="listTable.edit(this, 'edit_items', 25)">2</span></td>
    <td align="center"><span onclick="listTable.edit(this, 'edit_total_search', 25)">2</span></td>
    <td align="center"><span onclick="listTable.edit(this, 'edit_month_search', 25)">2</span></td>
    <td align="center"><span onclick="listTable.edit(this, 'edit_week_search', 25)">2</span></td>
    <td align="center"><span onclick="listTable.edit(this, 'edit_today_search', 25)">2</span></td>
	<td align="center"><span>
    <img src="images/yes.gif" onclick="listTable.toggle(this, 'toggle_show', 25)" /></span></td>
    <td align="center"><a href="keyword.php?act=view&amp;id=25" title="查看"><img src="images/icon_view.gif" width="16" height="16" border="0" /></a></td>
</tr>
<tr>
	<td><input type="checkbox" name="checkboxes[]" value="1" />1</td>
  	<td class="first-cell"><span onclick="listTable.edit(this, 'edit_word', 1)">三星</span></td>
    <td align="left"><span id="letter1" onclick="listTable.edit(this, 'edit_letter', 1)">sanxing</span></td>
    <td align="center"><span onclick="listTable.edit(this, 'edit_items', 1)">3</span></td>
    <td align="center"><span onclick="listTable.edit(this, 'edit_total_search', 1)">1</span></td>
    <td align="center"><span onclick="listTable.edit(this, 'edit_month_search', 1)">1</span></td>
    <td align="center"><span onclick="listTable.edit(this, 'edit_week_search', 1)">1</span></td>
    <td align="center"><span onclick="listTable.edit(this, 'edit_today_search', 1)">1</span></td>
	<td align="center"><span>
    <img src="images/yes.gif" onclick="listTable.toggle(this, 'toggle_show', 1)" /></span></td>
    <td align="center"><a href="keyword.php?act=view&amp;id=1" title="查看"><img src="images/icon_view.gif" width="16" height="16" border="0" /></a></td>
</tr>
<tr>
	<td><input type="checkbox" name="checkboxes[]" value="4" />4</td>
  	<td class="first-cell"><span onclick="listTable.edit(this, 'edit_word', 4)">冰箱电视</span></td>
    <td align="left"><span id="letter4" onclick="listTable.edit(this, 'edit_letter', 4)">bingxiangdianshi</span></td>
    <td align="center"><span onclick="listTable.edit(this, 'edit_items', 4)">2</span></td>
    <td align="center"><span onclick="listTable.edit(this, 'edit_total_search', 4)">1</span></td>
    <td align="center"><span onclick="listTable.edit(this, 'edit_month_search', 4)">1</span></td>
    <td align="center"><span onclick="listTable.edit(this, 'edit_week_search', 4)">1</span></td>
    <td align="center"><span onclick="listTable.edit(this, 'edit_today_search', 4)">1</span></td>
	<td align="center"><span>
    <img src="images/yes.gif" onclick="listTable.toggle(this, 'toggle_show', 4)" /></span></td>
    <td align="center"><a href="keyword.php?act=view&amp;id=4" title="查看"><img src="images/icon_view.gif" width="16" height="16" border="0" /></a></td>
</tr>
<tr>
	<td><input type="checkbox" name="checkboxes[]" value="6" />6</td>
  	<td class="first-cell"><span onclick="listTable.edit(this, 'edit_word', 6)">手机冰箱</span></td>
    <td align="left"><span id="letter6" onclick="listTable.edit(this, 'edit_letter', 6)">shoujibingxiang</span></td>
    <td align="center"><span onclick="listTable.edit(this, 'edit_items', 6)">20</span></td>
    <td align="center"><span onclick="listTable.edit(this, 'edit_total_search', 6)">1</span></td>
    <td align="center"><span onclick="listTable.edit(this, 'edit_month_search', 6)">1</span></td>
    <td align="center"><span onclick="listTable.edit(this, 'edit_week_search', 6)">1</span></td>
    <td align="center"><span onclick="listTable.edit(this, 'edit_today_search', 6)">1</span></td>
	<td align="center"><span>
    <img src="images/yes.gif" onclick="listTable.toggle(this, 'toggle_show', 6)" /></span></td>
    <td align="center"><a href="keyword.php?act=view&amp;id=6" title="查看"><img src="images/icon_view.gif" width="16" height="16" border="0" /></a></td>
</tr>
<tr>
	<td><input type="checkbox" name="checkboxes[]" value="7" />7</td>
  	<td class="first-cell"><span onclick="listTable.edit(this, 'edit_word', 7)">海鲜</span></td>
    <td align="left"><span id="letter7" onclick="listTable.edit(this, 'edit_letter', 7)">haixian</span></td>
    <td align="center"><span onclick="listTable.edit(this, 'edit_items', 7)">4</span></td>
    <td align="center"><span onclick="listTable.edit(this, 'edit_total_search', 7)">1</span></td>
    <td align="center"><span onclick="listTable.edit(this, 'edit_month_search', 7)">1</span></td>
    <td align="center"><span onclick="listTable.edit(this, 'edit_week_search', 7)">1</span></td>
    <td align="center"><span onclick="listTable.edit(this, 'edit_today_search', 7)">1</span></td>
	<td align="center"><span>
    <img src="images/yes.gif" onclick="listTable.toggle(this, 'toggle_show', 7)" /></span></td>
    <td align="center"><a href="keyword.php?act=view&amp;id=7" title="查看"><img src="images/icon_view.gif" width="16" height="16" border="0" /></a></td>
</tr>
<tr>
	<td><input type="checkbox" name="checkboxes[]" value="8" />8</td>
  	<td class="first-cell"><span onclick="listTable.edit(this, 'edit_word', 8)">半壳</span></td>
    <td align="left"><span id="letter8" onclick="listTable.edit(this, 'edit_letter', 8)">banke</span></td>
    <td align="center"><span onclick="listTable.edit(this, 'edit_items', 8)">2</span></td>
    <td align="center"><span onclick="listTable.edit(this, 'edit_total_search', 8)">1</span></td>
    <td align="center"><span onclick="listTable.edit(this, 'edit_month_search', 8)">1</span></td>
    <td align="center"><span onclick="listTable.edit(this, 'edit_week_search', 8)">1</span></td>
    <td align="center"><span onclick="listTable.edit(this, 'edit_today_search', 8)">1</span></td>
	<td align="center"><span>
    <img src="images/yes.gif" onclick="listTable.toggle(this, 'toggle_show', 8)" /></span></td>
    <td align="center"><a href="keyword.php?act=view&amp;id=8" title="查看"><img src="images/icon_view.gif" width="16" height="16" border="0" /></a></td>
</tr>
<tr>
	<td><input type="checkbox" name="checkboxes[]" value="9" />9</td>
  	<td class="first-cell"><span onclick="listTable.edit(this, 'edit_word', 9)">半</span></td>
    <td align="left"><span id="letter9" onclick="listTable.edit(this, 'edit_letter', 9)">ban</span></td>
    <td align="center"><span onclick="listTable.edit(this, 'edit_items', 9)">2</span></td>
    <td align="center"><span onclick="listTable.edit(this, 'edit_total_search', 9)">1</span></td>
    <td align="center"><span onclick="listTable.edit(this, 'edit_month_search', 9)">1</span></td>
    <td align="center"><span onclick="listTable.edit(this, 'edit_week_search', 9)">1</span></td>
    <td align="center"><span onclick="listTable.edit(this, 'edit_today_search', 9)">1</span></td>
	<td align="center"><span>
    <img src="images/yes.gif" onclick="listTable.toggle(this, 'toggle_show', 9)" /></span></td>
    <td align="center"><a href="keyword.php?act=view&amp;id=9" title="查看"><img src="images/icon_view.gif" width="16" height="16" border="0" /></a></td>
</tr>
<tr>
	<td><input type="checkbox" name="checkboxes[]" value="10" />10</td>
  	<td class="first-cell"><span onclick="listTable.edit(this, 'edit_word', 10)">联通</span></td>
    <td align="left"><span id="letter10" onclick="listTable.edit(this, 'edit_letter', 10)">liantong</span></td>
    <td align="center"><span onclick="listTable.edit(this, 'edit_items', 10)">8</span></td>
    <td align="center"><span onclick="listTable.edit(this, 'edit_total_search', 10)">1</span></td>
    <td align="center"><span onclick="listTable.edit(this, 'edit_month_search', 10)">1</span></td>
    <td align="center"><span onclick="listTable.edit(this, 'edit_week_search', 10)">1</span></td>
    <td align="center"><span onclick="listTable.edit(this, 'edit_today_search', 10)">1</span></td>
	<td align="center"><span>
    <img src="images/yes.gif" onclick="listTable.toggle(this, 'toggle_show', 10)" /></span></td>
    <td align="center"><a href="keyword.php?act=view&amp;id=10" title="查看"><img src="images/icon_view.gif" width="16" height="16" border="0" /></a></td>
</tr>
</table>
<table id="page-table" cellspacing="0">
  <tr>
    <td>
      <input name="remove" type="submit" id="btnSubmit" value="删除" class="button" disabled="true" />
    </td>
    <td align="right" nowrap="true">
          <!-- $Id: page.htm 14216 2008-03-10 02:27:21Z testyang $ -->
            <div id="turn-page">
        总计  <span id="totalRecords">29</span>
        个记录分为 <span id="totalPages">2</span>
        页当前第 <span id="pageCurrent">1</span>
        页，每页 <input type='text' size='3' id='pageSize' value="15" onkeypress="return listTable.changePageSize(event)" />
        <span id="page-link">
          <a href="javascript:listTable.gotoPageFirst()">第一页</a>
          <a href="javascript:listTable.gotoPagePrev()">上一页</a>
          <a href="javascript:listTable.gotoPageNext()">下一页</a>
          <a href="javascript:listTable.gotoPageLast()">最末页</a>
        <!--   <select id="gotoPage" onchange="listTable.gotoPage(this.value)">
            <option value='1'>1</option><option value='2'>2</option>          </select> -->
         跳转到 <input id="pageInput" title="按回车键跳转到指定页数" value="1" style="width:25px;"  onkeypress="if(event.keyCode==13) {listTable.gotoPage(this.value);}"   type="text">
          <input class="button" id="pagerBtn" value="GO" onclick="goToPage()" type="button">
        </span>
      </div>
<script>
function goToPage(){
	var page=$("#pageInput").val();
	listTable.gotoPage(page);
}
</script>    </td>
  </tr>
</table>
</div>
</form>
<script type="Text/Javascript" language="JavaScript">
listTable.recordCount = 29;
listTable.pageCount = 2;
listTable.filter.keywords = '';
listTable.filter.sort_by = 'total_search';
listTable.filter.sort_order = 'DESC';
listTable.filter.record_count = '29';
listTable.filter.page_size = '15';
listTable.filter.page = '1';
listTable.filter.page_count = '2';
listTable.filter.start = '0';
<!--
onload = function()
{
  // 开始检查订单
  startCheckOrder();
}
/**
 * 搜索
 */
function searchLog()
{
	var keyword = Utils.trim(document.forms['searchForm'].elements['keyword'].value);
	listTable.filter['keywords'] = keyword;
	listTable.filter.page = 1;
	listTable.loadList();
}
//-->
</script>
<div id="footer">
共执行 3 个查询，用时 0.010001 秒，Gzip 已禁用，内存占用 2.518 MB<br />
版权所有 &copy; 2008-2030 广州网软志成信息科技有限公司，并保留所有权利。</div>
<!-- 新订单提示信息 -->
<div id="popMsg">
  <table cellspacing="0" cellpadding="0" width="100%" bgcolor="#cfdef4" border="0">
  <tr>
    <td style="color: #0f2c8c" width="30" height="24"></td>
    <td style="font-weight: normal; color: #1f336b; padding-top: 4px;padding-left: 4px" valign="center" width="100%"> 新订单通知</td>
    <td style="padding-top: 2px;padding-right:2px" valign="center" align="right" width="19"><span title="关闭" style="cursor: hand;cursor:pointer;color:red;font-size:12px;font-weight:bold;margin-right:4px;" onclick="Message.close()" >×</span><!-- <img title=关闭 style="cursor: hand" onclick=closediv() hspace=3 src="msgclose.jpg"> --></td>
  </tr>
  <tr>
    <td style="padding-right: 1px; padding-bottom: 1px" colspan="3" height="70">
    <div id="popMsgContent">
      <p>您有 <strong style="color:#ff0000" id="spanNewOrder">1</strong> 个新订单以及       <strong style="color:#ff0000" id="spanNewPaid">0</strong> 个新付款的订单</p>
      <p align="center" style="word-break:break-all"><a href="order.php?act=list"><span style="color:#ff0000">点击查看新订单</span></a></p>
    </div>
    </td>
  </tr>
  </table>
</div>

<!--
<embed src="images/online.wav" width="0" height="0" autostart="false" name="msgBeep" id="msgBeep" enablejavascript="true"/>
-->
<object classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000" codebase="http://active.macromedia.com/flash2/cabs/swflash.cab#version=4,0,0,0" id="msgBeep" width="1" height="1">
  <param name="movie" value="images/online.swf">
  <param name="quality" value="high">
  <embed src="images/online.swf" name="msgBeep" id="msgBeep" quality="high" width="0" height="0" type="application/x-shockwave-flash" pluginspage="http://www.macromedia.com/shockwave/download/index.cgi?p1_prod_version=shockwaveflash">
  </embed>
</object>

<script language="JavaScript">
document.onmousemove=function(e)
{
  var obj = Utils.srcElement(e);
  if (typeof(obj.onclick) == 'function' && obj.onclick.toString().indexOf('listTable.edit') != -1)
  {
    obj.title = '点击修改内容';
    obj.style.cssText = 'background: #278296;';
    obj.onmouseout = function(e)
    {
      this.style.cssText = '';
    }
  }
  else if (typeof(obj.href) != 'undefined' && obj.href.indexOf('listTable.sort') != -1)
  {
    obj.title = '点击对列表排序';
  }
}
<!--


var MyTodolist;
function showTodoList(adminid)
{
  if(!MyTodolist)
  {
    var global = $import("../js/global.js","js");
    global.onload = global.onreadystatechange= function()
    {
      if(this.readyState && this.readyState=="loading")return;
      var md5 = $import("js/md5.js","js");
      md5.onload = md5.onreadystatechange= function()
      {
        if(this.readyState && this.readyState=="loading")return;
        var todolist = $import("js/todolist.js","js");
        todolist.onload = todolist.onreadystatechange = function()
        {
          if(this.readyState && this.readyState=="loading")return;
          MyTodolist = new Todolist();
          MyTodolist.show();
        }
      }
    }
  }
  else
  {
    if(MyTodolist.visibility)
    {
      MyTodolist.hide();
    }
    else
    {
      MyTodolist.show();
    }
  }
}

if (Browser.isIE)
{
  onscroll = function()
  {
    //document.getElementById('calculator').style.top = document.body.scrollTop;
    document.getElementById('popMsg').style.top = (document.body.scrollTop + document.body.clientHeight - document.getElementById('popMsg').offsetHeight) + "px";
  }
}

if (document.getElementById("listDiv"))
{
  document.getElementById("listDiv").onmouseover = function(e)
  {
    obj = Utils.srcElement(e);

    if (obj)
    {
      if (obj.parentNode.tagName.toLowerCase() == "tr") row = obj.parentNode;
      else if (obj.parentNode.parentNode.tagName.toLowerCase() == "tr") row = obj.parentNode.parentNode;
      else return;

      for (i = 0; i < row.cells.length; i++)
      {
        if (row.cells[i].tagName != "TH") row.cells[i].style.backgroundColor = '#F4FAFB';
      }
    }

  }

  document.getElementById("listDiv").onmouseout = function(e)
  {
    obj = Utils.srcElement(e);

    if (obj)
    {
      if (obj.parentNode.tagName.toLowerCase() == "tr") row = obj.parentNode;
      else if (obj.parentNode.parentNode.tagName.toLowerCase() == "tr") row = obj.parentNode.parentNode;
      else return;

      for (i = 0; i < row.cells.length; i++)
      {
          if (row.cells[i].tagName != "TH") row.cells[i].style.backgroundColor = '';
      }
    }
  }

  document.getElementById("listDiv").onclick = function(e)
  {
    var obj = Utils.srcElement(e);

    if (obj.tagName == "INPUT" && obj.type == "checkbox")
    {
      if (!document.forms['listForm'])
      {
        return;
      }
      var nodes = document.forms['listForm'].elements;
      var checked = false;

      for (i = 0; i < nodes.length; i++)
      {
        if (nodes[i].checked)
        {
           checked = true;
           break;
         }
      }

      if(document.getElementById("btnSubmit"))
      {
        document.getElementById("btnSubmit").disabled = !checked;
      }
      for (i = 1; i <= 10; i++)
      {
        if (document.getElementById("btnSubmit" + i))
        {
          document.getElementById("btnSubmit" + i).disabled = !checked;
        }
      }
    }
  }

}

//-->
</script><?php
include 'buy.php' ;
?>
</body>
</html>