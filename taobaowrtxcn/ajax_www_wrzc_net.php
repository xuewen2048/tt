<?php

/** * SHOP ajax
 * ============================================================================
 * 版权所有 2005-2010 广州网软志成信息科技有限公司，并保留所有权利。
 * 网站地址: http://www.wrzc.net；
 * ============================================================================
 * $Author: www.wrzc.net $
 * $Id: ajax.php 17063 2010-03-25 06:35:46Z qq $
*/

define('IN_wrzc', true);

require(dirname(__FILE__) . '/includes/init.php');

if ($_REQUEST['act'] == 'tipword')
{
	require(ROOT_PATH . 'includes/cls_json.php');
	$word_www_wrzc_net = json_str_iconv($_REQUEST['word']);
	$json_www_wrzc_net   = new JSON;
	$result_www_wrzc_net = array('error' => 0, 'message' => '', 'content' => '');
	
	if(!$word_www_wrzc_net || strlen($word_www_wrzc_net) < 2 || strlen($word_www_wrzc_net) > 30)
	{
        $result_www_wrzc_net['error']   = 1;
		die($json_www_wrzc_net->encode($result_www_wrzc_net));
	}
	$needle = $replace = array();
	$word_www_wrzc_net = str_replace(array(' ','*', "\'"), array('%', '%', ''), $word_www_wrzc_net);
	$needle[] = $word_www_wrzc_net;
	$replace[] = '<strong style="color:cc0000;">'.$word_www_wrzc_net.'</strong>';
	$logdb = array();
	if(preg_match("/^[a-z0-9A-Z]+$/", $word_www_wrzc_net)) {	
    	$sql_qq = "SELECT * FROM " . $wrzc->table('keyword') ." WHERE searchengine='wrzchop' AND status='1' AND letter LIKE '%$word_www_wrzc_net%' ORDER BY total_search DESC";
	} else {
    	$sql_qq = "SELECT * FROM " . $wrzc->table('keyword') ." WHERE searchengine='wrzchop' AND status='1' AND word LIKE '%$word_www_wrzc_net%' ORDER BY total_search DESC";
	}
    $res_www_wrzc_net = $db->SelectLimit($sql_qq, 10, 0);

	$iii=1; //wrzc.net
	while ($rows_www_wrzc_net = $db->fetchRow($res_www_wrzc_net))
    {
		$rows_www_wrzc_net['kword'] = str_ireplace($needle, $replace, $rows_www_wrzc_net['word']);

		/* start  By  wrzc.net */
		if($iii==1 && $rows_www_wrzc_net['keyword_cat_count'])
		{  
			$rows_www_wrzc_net['keyword_cat'] =  '<a href="' . $rows_www_wrzc_net['keyword_cat_url'] . '"><font color=#666>在<font color=#cc0000>'. $rows_www_wrzc_net['keyword_cat'] .'</font>分类中搜索</font></a>';
			$rows_www_wrzc_net['keyword_cat_count'] = intval($rows_www_wrzc_net['keyword_cat_count']);
		}
		$iii=$iii+1;  
		/* end  By  wrzc.net */

		$logdb[] = $rows_www_wrzc_net; 

		
	}
	$smarty->assign('logdb', $logdb);
	$result_www_wrzc_net['content'] = $smarty->fetch('library/search_tip.lbi');
	die($json_www_wrzc_net->encode($result_www_wrzc_net));
}
?>