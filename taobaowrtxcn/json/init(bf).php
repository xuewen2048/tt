<?php

/** * SHOP 前台公用文件
 * ============================================================================
 * * 版权所有 2005-2030 广州网软志成信息科技有限公司，并保留所有权利。
 * 网站地址: http://www.wrzc.net；
 * ============================================================================
 * $Author: liubo $
 * $Id: init.php 17217 2011-01-19 06:29:08Z liubo $
*/

require('../data/config.php');

/* 初始化数据库类 */
require('../includes/cls_mysql.php');
$db = new cls_mysql($db_host, $db_user, $db_pass, $db_name);
$db->set_disable_cache_tables(array($wrzc->table('sessions'), $wrzc->table('sessions_data'), $wrzc->table('cart')));
$db_host = $db_user = $db_pass = $db_name = NULL;


?>