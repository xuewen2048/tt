<?php
/** * SHOP 管理中心公用文件
 * ============================================================================
 * * 版权所有 2005-2030 广州网软志成信息科技有限公司，并保留所有权利。
 * 网站地址: http://www.wrzc.net；
 * ============================================================================
 * $Author: liubo $
 * $Id: init.php 17217 2011-01-19 06:29:08Z liubo $
*/

if (!defined('IN_wrzc'))
{
    die('Hacking attempt');
}

define('wrzc_ADMIN', true);

error_reporting(E_ALL);

if (__FILE__ == '')
{
    die('Fatal error code: 0');
}

/* 初始化设置 */
@ini_set('memory_limit',          '64M');
@ini_set('session.cache_expire',  180);
@ini_set('session.use_trans_sid', 0);
@ini_set('session.use_cookies',   1);
@ini_set('session.auto_start',    0);
@ini_set('display_errors',        1);
/* 代码增加 By  www.wrzc.net Start */
define('MYSQL_ERROR_DISPLAY', 0);
/* 代码增加 By  www.wrzc.net End */

if (DIRECTORY_SEPARATOR == '\\')
{
    @ini_set('include_path',      '.;' . ROOT_PATH);
}
else
{
    @ini_set('include_path',      '.:' . ROOT_PATH);
}

if (file_exists('../data/config.php'))
{
    include('../data/config.php');
}
else
{
    include('../includes/config.php');
}

/* 取得当前所在的根目录 */
if(!defined('ADMIN_PATH'))
{
    define('ADMIN_PATH','json');
}
define('ROOT_PATH', str_replace('json/includes/init.php', '', str_replace('\\', '/', __FILE__)));


if (defined('DEBUG_MODE') == false)
{
    define('DEBUG_MODE', 0);
}

if (PHP_VERSION >= '5.1' && !empty($timezone))
{
    date_default_timezone_set($timezone);
}

if (isset($_SERVER['PHP_SELF']))
{
    define('PHP_SELF', $_SERVER['PHP_SELF']);
}
else
{
    define('PHP_SELF', $_SERVER['SCRIPT_NAME']);
}

require( '../includes/inc_constant.php');
require('../includes/cls_wrzchop.php');
require('../includes/cls_error.php');
require('../includes/lib_time.php');
require('../includes/lib_base.php');
require('../includes/lib_common.php');
require('./includes/lib_main.php');
require('./includes/cls_exchange.php');



/* 创建 WRZCNET 对象 */
$wrzc = new wrzc($db_name, $prefix);
define('DATA_DIR', $wrzc->data_dir());
define('IMAGE_DIR', $wrzc->image_dir());

/* 初始化数据库类 */
require('../includes/cls_mysql.php');
$db = new cls_mysql($db_host, $db_user, $db_pass, $db_name);
$db_host = $db_user = $db_pass = $db_name = NULL;

/* 载入系统参数 */
$_CFG = load_config();

?>
